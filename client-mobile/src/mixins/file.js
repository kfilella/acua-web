/**
 * Mixin para el guardado de archivos offline
 */
import { LocalStorage } from 'quasar'

export const FileMixin = {
  data () {
    return {
      fileName: '',
      formData: undefined,
      localStorageKeyImages: undefined
    }
  },
  methods: {
    saveBlob2File (fileName, blob, formData, localStorageKeyImages) {
      // alert('saveBlob2File' + localStorageKeyImages)
      this.formData = formData
      this.fileName = fileName
      this.localStorageKeyImages = localStorageKeyImages
      var vm = this
      var folder = cordova.file.externalRootDirectory + 'Download'
      window.resolveLocalFileSystemURL(folder, function (dirEntry) {
        vm.createFile(dirEntry, fileName, blob)
      }, this.onErrorLoadFs)
    },
    createFile (dirEntry, fileName, blob) {
      var vm = this
      // Creates a new file
      dirEntry.getFile(fileName, { create: true, exclusive: false }, function (fileEntry) {
        // Aquí pide permisos al cel
        vm.writeFile(fileEntry, blob)
      }, this.onErrorCreateFile)
    },
    writeFile (fileEntry, dataObj) {
      var vm = this
      // Create a FileWriter object for our FileEntry
      fileEntry.createWriter(function (fileWriter) {
        fileWriter.onwriteend = function () {
          // vm.readFile()
          console.log('archivo guardado')
          // Guardar en localstorage el nombre del archivo subido
          let uploadedFiles = LocalStorage.getItem(vm.localStorageKeyImages)
          if (uploadedFiles && uploadedFiles.length) {
            if (vm.formData) {
              vm.formData.filename = vm.fileName
              uploadedFiles.push(vm.formData)
            } else {
              uploadedFiles.push(vm.fileName)
            }
          } else {
            if (vm.formData) {
              vm.formData.filename = vm.fileName
              uploadedFiles = [vm.formData]
            } else {
              uploadedFiles = [vm.fileName]
            }
          }
          // alert('this.localStorageKeyImages' + vm.localStorageKeyImages + uploadedFiles.length)
          LocalStorage.set(vm.localStorageKeyImages, uploadedFiles)
        }

        fileWriter.onerror = function (error) {
          alert(error)
        }
        fileWriter.write(dataObj)
      })
    },
    readFile () {
      let fileMIMEType
      switch (this.ext) {
        case 'bmp':
          fileMIMEType = 'image/bmp'
          break
        case 'gif':
          fileMIMEType = 'image/gif'
          break
        case 'jpeg':
          fileMIMEType = 'image/jpeg'
          break
        case 'jpg':
          fileMIMEType = 'image/jpeg'
          break
        case 'jpe':
          fileMIMEType = 'image/jpeg'
          break
        case 'png':
          fileMIMEType = 'image/png'
          break
        case 'xls':
          fileMIMEType = 'application/vnd.ms-excel'
          break
        case 'xlsx':
          fileMIMEType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
          break
        default:
          fileMIMEType = 'application/pdf'
      }
      cordova.plugins.fileOpener2.open(
        cordova.file.externalRootDirectory + 'Download/' + this.fileName, // You can also use a Cordova-style file uri: cdvfile://localhost/persistent/Downloads/starwars.pdf
        fileMIMEType,
        {
          error: function (e) {
            alert('Error al abrir archivo: ' + e.status + ' - Error message: ' + e.message)
          },
          success: function () {
            // alert('file opened successfully')
          }
        }
      )
    },
    onErrorLoadFs (error) {
      console.log(error)
    },
    onErrorCreateFile (error) {
      console.log(error)
      // si no dan permisoss
    }
  }
}
