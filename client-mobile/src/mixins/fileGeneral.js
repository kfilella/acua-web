/**
 * Mixin para el guardado de archivos offline, este es más genérico y no asociado a registros
 */
import { copyToClipboard } from 'quasar'
export const FileGeneralMixin = {
  data () {
    return {
      fileName: '',
      ext: ''
    }
  },
  methods: {
    saveBlob2File (fileName, blob, onlyDownload = false) {
      const ext = fileName.split('.')
      this.ext = ext[ext.length - 1]
      // alert('saveBlob2File')
      this.fileName = fileName
      var vm = this
      // Carpeta de descargas
      var folder = cordova.file.externalRootDirectory + 'Download'
      window.resolveLocalFileSystemURL(folder, function (dirEntry) {
        vm.createFile(dirEntry, fileName, blob, true)
      }, this.onErrorLoadFs)

      if (!onlyDownload) {
        // Carpeta interna del cel para hacer el respaldo del archivo guardado
        var folder2 = cordova.file.dataDirectory
        // Guardar archivo en directorio interno de la app
        window.resolveLocalFileSystemURL(folder2, function (dirEntry) {
          vm.createFile(dirEntry, fileName, blob)
        }, this.onErrorLoadFs)
      }
    },
    createFile (dirEntry, fileName, blob, open = undefined) {
      // alert('createFile')
      var vm = this
      // Creates a new file
      dirEntry.getFile(fileName, { create: true, exclusive: false }, function (fileEntry) {
        // Aquí pide permisos al cel
        vm.writeFile(fileEntry, blob, open)
      }, this.onErrorCreateFile)
    },
    writeFile (fileEntry, dataObj, open = undefined) {
      // alert('writeFile')
      var vm = this
      // Create a FileWriter object for our FileEntry
      fileEntry.createWriter(function (fileWriter) {
        fileWriter.onwriteend = function () {
          if (open) {
            vm.readFile()
          }
        }

        fileWriter.onerror = function (error) {
          alert(error)
        }
        fileWriter.write(dataObj)
      })
    },
    searchFile (fileName) {
      // alert('searchFile')
      var vm = this
      window.resolveLocalFileSystemURL(cordova.file.dataDirectory + '/' + fileName,
        function (fileEntry) {
          // alert('Hay fileEntry')
          fileEntry.file(function (file) {
            var vm2 = vm
            // alert('getFile done for ' + fileName)
            var reader = new FileReader()
            // var vm = this
            reader.onloadend = async function () {
              const vm3 = vm2
              // alert('reader done, starting fd ...')
              var blob = new Blob([new Uint8Array(this.result)], { type: file.type })
              // alert('hay blob' + blob.size)
              vm3.saveBlob2File(fileName, blob)
            }
            reader.readAsArrayBuffer(file)
          },
          function (err) {
            alert('Error getting file', err)
          })
        },
        function (err) {
          alert('Error in resolveLocalFileSystemURL ' + JSON.stringify(err))
        })
    },
    readFile () {
      // alert('readFile')
      let fileMIMEType
      switch (this.ext) {
        case 'bmp':
          fileMIMEType = 'image/bmp'
          break
        case 'gif':
          fileMIMEType = 'image/gif'
          break
        case 'jpeg':
          fileMIMEType = 'image/jpeg'
          break
        case 'jpg':
          fileMIMEType = 'image/jpeg'
          break
        case 'jpe':
          fileMIMEType = 'image/jpeg'
          break
        case 'png':
          fileMIMEType = 'image/png'
          break
        case 'xls':
          fileMIMEType = 'application/vnd.ms-excel'
          break
        case 'xlsx':
          fileMIMEType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
          break
        default:
          fileMIMEType = 'application/pdf'
      }
      cordova.plugins.fileOpener2.open(
        cordova.file.externalRootDirectory + 'Download/' + this.fileName, // You can also use a Cordova-style file uri: cdvfile://localhost/persistent/Downloads/starwars.pdf
        fileMIMEType,
        {
          error: function (e) {
            alert('Error al abrir archivo: ' + e.status + ' - Error message: ' + e.message)
          },
          success: function () {
            // alert('file opened successfully')
          }
        }
      )
    },
    onErrorLoadFs (error) {
      console.log(error)
    },
    onErrorCreateFile (error) {
      console.log(error)
      // si no dan permisoss
    },
    edit (recordId) {
      this.$router.push(`${this.$route.path}/form/${recordId}`)
    },
    mail (recordId) {
      this.$router.push(`${this.$route.path}/send_mail/${recordId}`)
    },
    download (analisysId) {
      this.$q.loading.show({ message: 'Descargando archivo...' })
      this.$api.get(`${this.downloadUrl}/${analisysId}`).then(file => {
        var vm = this
        if (file) {
          this.$api.get(`filepdf/${file}`, { responseType: 'blob' }).then(res => {
            const blob = new Blob([res])
            let ext = file.split('.')
            ext = ext[ext.length - 1]
            const fileName = `${this.downloadFilename} ${analisysId}.${ext}`
            this.fileName = fileName

            if (this.$q.platform.is.mobile) { // Si es teléfono
              console.log('es telfono')
              this.saveBlob2File(fileName, blob)
            }
            this.$q.loading.hide()
            this.$q.notify({
              type: 'positive',
              message: 'Archivo disponible en descargas. ' + fileName
            })
          }).catch(function (error) {
            console.log('error descargando', error)
            vm.$q.loading.hide()
            vm.$q.notify({
              type: 'negative',
              message: 'Error descargando archivo'
            })
          })
        } else { // Algún error de servidor
          this.$q.loading.hide()
        }
      })
    },
    getLink (analisysId) {
      this.$api.get(`${this.downloadUrl}/${analisysId}`).then(res => {
        if (res) {
          this.link = `${process.env.apiUrl}filepdf/${res}`
          this.showLink = true
        }
      })
    },
    copyClipboard () {
      if (this.$q.platform.is.mobile) {
        cordova.plugins.clipboard.copy(this.link)
      } else {
        copyToClipboard(this.link).then(() => {
          console.log('bien')
        })
          .catch((err) => {
            alert(err)
          })
      }
    }
  }
}
