import axios from 'axios'
import { Notify, LocalStorage, Loading } from 'quasar'

const axiosInstance = axios.create({
  baseURL: process.env.apiUrl, // url base cargada de archivo env.js
  timeout: 100000
})
var formData = ''
/**
 * Función para hacer peticiones GET que guarden y recuperen datos del API y de localstorage cuando no haya conexión
 *
 * @param {url} String
 * @param {params} Object
 */
async function getData (url, params = undefined) {
  Loading.show()
  const queryString = params ? '?' + Object.keys(params.params).map(key => `${key}=${params.params[key]}`).join('&') : ''
  const fullUrl = `${url}${queryString}`
  let results = LocalStorage.getItem(fullUrl)
  const res = await axiosInstance.get(url, params)
  if (res) {
    LocalStorage.set(fullUrl, res)
    results = res
  }
  Loading.hide()
  return results
}

export default async ({ store, Vue, router }) => {
  Vue.prototype.$api = axiosInstance
  Vue.prototype.$api.getData = getData

  axiosInstance.interceptors.response.use(function (response) {
    console.log('Interceptado', response)
    // Todo bien con la respuesta
    if (response.config.method === 'post') {
      if (response.status === 201) {
        if (response.data.token === undefined) { // Si no es login
          Notify.create({
            color: 'green-4',
            textColor: 'white',
            icon: 'fas fa-check-circle',
            message: 'Registro guardado con éxito!'
          })
        } else { // Es Login
          localStorage.setItem('GM_SESSION_INFO', JSON.stringify(response.data))
        }
      }
    }
    return response.data
  }, function (error) {
    // Error en la respuesta
    // console.log('debug', error.response)
    if (error.response === undefined) { // Si no hubo comunicación con el servidor
      console.log('no hay conexion con el servidor', error, formData)
      // guardar lo que se quería enviar en LocalStorage para que sea enviado luego
      if (formData.method !== 'get') { // Los métodos distintos de get se guardarán
        if (formData.url !== 'analisis_image') { // Si no es la carga de imágenes
          // Si es analisis de patologias se deben guardar tambien en la lista de patologias con un id que identifique que aun no esta guardado en server
          let isLocalData = false
          const payload = JSON.parse(formData.data)
          // Analisis de patologias
          if (formData.url.indexOf('patologies_analisys') > -1) {
            // Si es una insercion
            const pathologies = LocalStorage.getItem(`patologies_analisys?shrimp_id=${payload.shrimp_id}`)
            if (formData.method === 'post') {
              payload.provisionalId = Date.now()
              formData.data = JSON.stringify(payload)

              // Insertar en lista
              pathologies.unshift(payload)
            } else if (formData.method === 'put') { // Si es una actualizacion
              console.log('es un put')
              if (payload.provisionalId) { // Si es un dato local
                isLocalData = true
                // Se debe actualizar la lista de patologias con los nuevos cambios
                // Buscar el registro en la lista por el campo provisional
                const index = pathologies.findIndex(val => val.provisionalId === payload.provisionalId)
                pathologies[index] = payload
              }
            }
            LocalStorage.set(`patologies_analisys?shrimp_id=${payload.shrimp_id}`, pathologies)
            LocalStorage.set(`patologies_analisys/${payload.provisionalId}`, payload)
          }

          // Analisis de suelos
          if (formData.url.indexOf('floor_analysis') > -1) {
            // Si es una insercion
            const pathologies = LocalStorage.getItem(`floor_analysis?shrimp_id=${payload.shrimp_id}`)
            if (formData.method === 'post') {
              payload.provisionalId = Date.now()
              formData.data = JSON.stringify(payload)

              // Insertar en lista
              pathologies.unshift(payload)
            } else if (formData.method === 'put') { // Si es una actualizacion
              console.log('es un put')
              if (payload.provisionalId) { // Si es un dato local
                isLocalData = true
                // Se debe actualizar la lista de patologias con los nuevos cambios
                // Buscar el registro en la lista por el campo provisional
                const index = pathologies.findIndex(val => val.provisionalId === payload.provisionalId)
                pathologies[index] = payload
              }
            }
            LocalStorage.set(`floor_analysis?shrimp_id=${payload.shrimp_id}`, pathologies)
            LocalStorage.set(`floor_analysis/${payload.provisionalId}`, payload)
          }

          // Analisis de aguas
          if (formData.url.indexOf('water_analysis') > -1) {
            // Si es una insercion
            const pathologies = LocalStorage.getItem(`water_analysis?shrimp_id=${payload.shrimp_id}`)
            if (formData.method === 'post') {
              payload.provisionalId = Date.now()
              formData.data = JSON.stringify(payload)

              // Insertar en lista
              pathologies.unshift(payload)
            } else if (formData.method === 'put') { // Si es una actualizacion
              console.log('es un put')
              if (payload.provisionalId) { // Si es un dato local
                isLocalData = true
                // Se debe actualizar la lista de patologias con los nuevos cambios
                // Buscar el registro en la lista por el campo provisional
                const index = pathologies.findIndex(val => val.provisionalId === payload.provisionalId)
                pathologies[index] = payload
              }
            }
            LocalStorage.set(`water_analysis?shrimp_id=${payload.shrimp_id}`, pathologies)
            LocalStorage.set(`water_analysis/${payload.provisionalId}`, payload)
          }

          // Analisis de fitoplancton
          if (formData.url.indexOf('phytoplanktons_analysis') > -1) {
            // Si es una insercion
            const pathologies = LocalStorage.getItem(`phytoplanktons_analysis?shrimp_id=${payload.shrimp_id}`)
            if (formData.method === 'post') {
              payload.provisionalId = Date.now()
              formData.data = JSON.stringify(payload)

              // Insertar en lista
              pathologies.unshift(payload)
            } else if (formData.method === 'put') { // Si es una actualizacion
              console.log('es un put')
              if (payload.provisionalId) { // Si es un dato local
                isLocalData = true
                // Se debe actualizar la lista de patologias con los nuevos cambios
                // Buscar el registro en la lista por el campo provisional
                const index = pathologies.findIndex(val => val.provisionalId === payload.provisionalId)
                pathologies[index] = payload
              }
            }
            LocalStorage.set(`phytoplanktons_analysis?shrimp_id=${payload.shrimp_id}`, pathologies)
            LocalStorage.set(`phytoplanktons_analysis/${payload.provisionalId}`, payload)
          }

          // Analisis de compendium
          if (formData.url.indexOf('compendium') > -1) {
            // Si es una insercion
            const pathologies = LocalStorage.getItem(`compendium?shrimp_id=${payload.shrimp_id}`)
            if (formData.method === 'post') {
              payload.provisionalId = Date.now()
              formData.data = JSON.stringify(payload)

              // Insertar en lista
              pathologies.unshift(payload)
            } else if (formData.method === 'put') { // Si es una actualizacion
              console.log('es un put')
              if (payload.provisionalId) { // Si es un dato local
                isLocalData = true
                // Se debe actualizar la lista de patologias con los nuevos cambios
                // Buscar el registro en la lista por el campo provisional
                const index = pathologies.findIndex(val => val.provisionalId === payload.provisionalId)
                pathologies[index] = payload
              }
            }
            LocalStorage.set(`compendium?shrimp_id=${payload.shrimp_id}`, pathologies)
            LocalStorage.set(`compendium/${payload.provisionalId}`, payload)
          }

          // Analisis de harvest_reports
          if (formData.url.indexOf('harvest_reports') > -1) {
            // Si es una insercion
            const records = LocalStorage.getItem(`harvest_reports?shrimp_id=${payload.shrimp_id}`) || []
            console.log({ records })
            if (formData.method === 'post') {
              payload.provisionalId = Date.now()
              formData.data = JSON.stringify(payload)

              // Insertar en lista
              records.unshift(payload)
            } else if (formData.method === 'put') { // Si es una actualizacion
              console.log('es un put')
              if (payload.provisionalId) { // Si es un dato local
                isLocalData = true
                // Se debe actualizar la lista de patologias con los nuevos cambios
                // Buscar el registro en la lista por el campo provisional
                const index = records.findIndex(val => val.provisionalId === payload.provisionalId)
                records[index] = payload
              }
            }
            LocalStorage.set(`harvest_reports?shrimp_id=${payload.shrimp_id}`, records)
            LocalStorage.set(`harvest_reports/${payload.provisionalId}`, payload)
          }

          // Proyecciones de pesca
          if (formData.url.indexOf('fishing_projections') > -1) {
            // Si es una insercion
            const records = LocalStorage.getItem(`fishing_projections?shrimp_id=${payload.shrimp_id}`) || []
            console.log({ records })
            if (formData.method === 'post') {
              payload.provisionalId = Date.now()
              formData.data = JSON.stringify(payload)

              // Insertar en lista
              records.unshift(payload)
            } else if (formData.method === 'put') { // Si es una actualizacion
              console.log('es un put')
              if (payload.provisionalId) { // Si es un dato local
                isLocalData = true
                // Se debe actualizar la lista de patologias con los nuevos cambios
                // Buscar el registro en la lista por el campo provisional
                const index = records.findIndex(val => val.provisionalId === payload.provisionalId)
                records[index] = payload
              }
            }
            LocalStorage.set(`fishing_projections?shrimp_id=${payload.shrimp_id}`, records)
            LocalStorage.set(`fishing_projections/${payload.provisionalId}`, payload)
          }

          // Proyecciones de alimentación
          if (formData.url.indexOf('food_projections') > -1) {
            // Si es una insercion
            const records = LocalStorage.getItem(`food_projections_mobile?shrimp_id=${payload.shrimp_id}&page=1&recordsCount=20`) || []
            console.log({ records })
            if (formData.method === 'post') {
              payload.provisionalId = Date.now()
              formData.data = JSON.stringify(payload)

              // Insertar en lista
              records.data.unshift(payload)
            } else if (formData.method === 'put') { // Si es una actualizacion
              console.log('es un put')
              if (payload.provisionalId) { // Si es un dato local
                isLocalData = true
                // Se debe actualizar la lista de patologias con los nuevos cambios
                // Buscar el registro en la lista por el campo provisional
                const index = records.data.findIndex(val => val.provisionalId === payload.provisionalId)
                records.data[index] = payload
              }
            }
            LocalStorage.set(`food_projections_mobile?shrimp_id=${payload.shrimp_id}&page=1&recordsCount=20`, records)
            LocalStorage.set(`food_projections/${payload.provisionalId}`, payload)
          }

          let poolData = LocalStorage.getItem('poolData')
          if (isLocalData) { // Si es un dato local se debe solo actualizar en lo pendiente en vez de insertar uno nuevo
            // Buscar dentro de pooldata el que tenga el id provisional de este payload
            poolData.forEach(element => {
              let data = JSON.parse(element.data)
              if (data.provisionalId === payload.provisionalId) {
                data = payload
              }
              element.data = JSON.stringify(data)
            })
          } else {
            if (poolData) {
              poolData.push(formData)
            } else {
              poolData = [formData]
            }
          }
          LocalStorage.set('poolData', poolData)
        }
      }
      // Hay que mostrar un mensaje diciendo que los datos se subirán cuando haya conexión
      /* Notify.create({
        color: 'red-5',
        textColor: 'white',
        icon: 'fas fa-exclamation-triangle',
        message: 'No se pudo establecer conexión con el servidor' + error
      }) */
    } else {
      // console.log('401', error.response.data)
      if (error.response.status === 401) { // si los datos de login son inválidos
        let message = 'Correo y/o Contraseña Incorrectos'
        if (error.response.data[0]) {
          if (error.response.data[0].field === 'email') {
            message = 'Correo no registrado en el sistema'
          } else {
            message = 'Contraseña incorrecta'
            LocalStorage.remove('___ATAD___')
          }
        }
        Notify.create({
          message,
          color: 'black'
        })
        if (router.app._route.path !== '/') {
          router.push('/')
        }
      } else if (error.response.status === 403) {
        Notify.create({
          message: error.response.data,
          color: 'red'
        })
      } else if (error.response.status === 404) {
        Notify.create({
          message: 'Ruta no encontrada - 404',
          color: 'negative'
        })
      } else if (error.response.status === 422) {
        const message = error.response.data[0].message ? error.response.data[0].message : error.response.data
        Notify.create({
          message: 'Datos Inválidos:' + message,
          color: 'red',
          position: 'bottom'
        })
      } else if (error.response.status === 500) {
        Notify.create({
          message: 'Error de conexión con el servidor',
          color: 'red',
          position: 'bottom'
        })
      }
      var data = error.response.data
      // console.log('error.response.data.error', data)
      if (data) {
        if (data.statusCode === 403) {
          Notify.create({
            color: 'red-5',
            textColor: 'white',
            icon: 'fas fa-exclamation-triangle',
            message: data.message
          })
        }
        if (data.statusCode === 500) {
          Notify.create({
            color: 'red-5',
            textColor: 'white',
            icon: 'fas fa-exclamation-triangle',
            message: 'Error interno en servidor' + data.message
          })
          // return Promise.reject(data.response.data.error)
        }
        // Añadir mas mensajes segun codigos de error especificos y mostrar las notificaciones correspondientes

        // Notify.create(error.response.data.error.message)
        // console.log(error.response.status);
        // console.log(error.response.headers);
      } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        // console.log(error.request)
      } else {
        // Something happened in setting up the request that triggered an Error
        // console.log('Error', error.message)
      }
      // console.log(error.config)
    }

    // return Promise.reject(data)
  })

  axiosInstance.interceptors.request.use(async function (config) {
    console.log('request')
    // Antes de enviar cada petición se añade el token si existe
    const sessionInfo = LocalStorage.getItem('___ATAD___')
    const token = (sessionInfo !== null) ? sessionInfo.token : false
    if (token) {
      if (!config.headers) { config.headers = {} }
      config.headers = {
        Authorization: 'Bearer ' + token
      }
    }
    formData = config // se guarda todo el request en una variable de manera que si falla la petición por conexión se guardará en LocalStorage
    console.log('formData', formData)
    return config
  }, function (error) {
    // Do something with request error
    return Promise.reject(error)
  })
}

export { axiosInstance }
