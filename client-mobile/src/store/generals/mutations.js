import { LocalStorage } from 'quasar'
export function someMutation (/* state */) {
}
export function login (state, data) {
  LocalStorage.set('___ATAD___', data)
  state.sessionInfo = data
}
export function logout (state) {
  // localStorage.removeItem('___ATAD___')
  localStorage.clear()
  state.sessionInfo = undefined
}
