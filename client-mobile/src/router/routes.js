/*
 * Archivo de definición de rutas del FrontEnd
 * Se usa Vue Router
 * La ruta raiz es el login. Cuando se inicie sesión se cargará el menú, es decir la ruta 'menu'
 * Todas las apps que cargue el usuario se mostrarán como child en un frame del menú destinado para tal.
 * Cada ruta que se cargue una vez que el usuario esté logueado debe tener la propiedad meta.permission
 * El valor de la propiedad meta.permission es el id del permiso que debe tener el usuario para acceder a tal ruta
 */

const routes = [
  {
    path: '/menu',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '/events', component: () => import('pages/appointment_planning/Calendar.vue') },
      { path: '', component: () => import('components/Search.vue') },
      { path: '/customer_shrimps/:id', component: () => import('pages/customers/CustomerShrimps.vue') },
      { path: '/customer_shrimps/:id/shrimp/:shrimpId', component: () => import('pages/customers/ShrimpDetail.vue') },
      { path: '/technical_advisors_shrimps', component: () => import('pages/shrimps/TechnicalAdvisorShrimps.vue') },
      { path: '/pathologies/:shrimpId/list', component: () => import('pages/shrimps/pathologies/List.vue') },
      { path: '/pathologies/:shrimpId/list/form', component: () => import('pages/shrimps/pathologies/Form.vue') },
      { path: '/pathologies/:shrimpId/list/form/:id', component: () => import('pages/shrimps/pathologies/Form.vue') },
      { path: '/pathologies/:shrimpId/list/send_mail/:id', component: () => import('pages/shrimps/pathologies/SendMail.vue') },
      { path: '/waters/:shrimpId/list', component: () => import('pages/shrimps/waters/List.vue') },
      { path: '/waters/:shrimpId/list/form', component: () => import('pages/shrimps/waters/Form.vue') },
      { path: '/waters/:shrimpId/list/form/:id', component: () => import('pages/shrimps/waters/Form.vue') },
      { path: '/waters/:shrimpId/list/send_mail/:id', component: () => import('pages/shrimps/waters/SendMail.vue') },
      { path: '/floors/:shrimpId/list', component: () => import('pages/shrimps/floors/List.vue') },
      { path: '/floors/:shrimpId/list/form', component: () => import('pages/shrimps/floors/Form.vue') },
      { path: '/floors/:shrimpId/list/form/:id', component: () => import('pages/shrimps/floors/Form.vue') },
      { path: '/floors/:shrimpId/list/send_mail/:id', component: () => import('pages/shrimps/floors/SendMail.vue') },
      { path: '/phytoplankton/:shrimpId/list', component: () => import('pages/shrimps/phytoplankton/List.vue') },
      { path: '/phytoplankton/:shrimpId/list/form', component: () => import('pages/shrimps/phytoplankton/Form.vue') },
      { path: '/phytoplankton/:shrimpId/list/form/:id', component: () => import('pages/shrimps/phytoplankton/Form.vue') },
      { path: '/phytoplankton/:shrimpId/list/send_mail/:id', component: () => import('pages/shrimps/phytoplankton/SendMail.vue') },
      { path: '/cost_benefit_projection/:shrimpId/list', component: () => import('pages/shrimps/cost_benefit_projection/List.vue') },
      { path: '/cost_benefit_projection/:shrimpId/list/form', component: () => import('pages/shrimps/cost_benefit_projection/Form.vue') },
      { path: '/cost_benefit_projection/:shrimpId/list/form/:id', component: () => import('pages/shrimps/cost_benefit_projection/Form.vue') },
      { path: '/cost_benefit_projection/:shrimpId/list/send_mail/:id', component: () => import('pages/shrimps/cost_benefit_projection/SendMail.vue') },
      { path: '/growth_reports/:shrimpId/list', component: () => import('pages/shrimps/growth_reports/List.vue') },
      { path: '/growth_reports/:shrimpId/list/form', component: () => import('pages/shrimps/growth_reports/Form.vue') },
      { path: '/growth_reports/:shrimpId/list/form/:id', component: () => import('pages/shrimps/growth_reports/Form.vue') },
      { path: '/growth_reports/:shrimpId/list/send_mail/:id', component: () => import('pages/shrimps/growth_reports/SendMail.vue') },
      { path: '/harvest_reports/:shrimpId/list', component: () => import('pages/shrimps/harvest_reports/List.vue') },
      { path: '/harvest_reports/:shrimpId/list/form', component: () => import('pages/shrimps/harvest_reports/Form.vue') },
      { path: '/harvest_reports/:shrimpId/list/form/:id', component: () => import('pages/shrimps/harvest_reports/Form.vue') },
      { path: '/harvest_reports/:shrimpId/list/send_mail/:id', component: () => import('pages/shrimps/harvest_reports/SendMail.vue') },
      // { path: '/fishing_projections/:poolId/list', component: () => import('pages/fishing_projections/List.vue') },
      { path: '/fishing_projections/list/form', component: () => import('pages/fishing_projection/FishingProjection.vue') },
      { path: '/fishing_projections/:shrimpId/list/form/:id', component: () => import('pages/fishing_projection/FishingProjection.vue') },
      { path: '/fishing_projections/:shrimpId/list/send_mail/:id', component: () => import('pages/fishing_projection/SendMail.vue') },
      { path: '/fishing_projections/:shrimpId/list', component: () => import('pages/fishing_projection/List.vue') },
      { path: '/compendium/:shrimpId/list', component: () => import('pages/shrimps/compendium/List.vue') },
      { path: '/compendium/:shrimpId/list/form', component: () => import('pages/shrimps/compendium/Form.vue') },
      { path: '/compendium/:shrimpId/list/form/:id', component: () => import('pages/shrimps/compendium/Form.vue') },
      { path: '/food_projections/list/form', component: () => import('pages/food_projection/FoodProjection.vue') },
      { path: '/food_projections/:shrimpId/list', component: () => import('pages/food_projection/List.vue') },
      { path: '/food_projections/:shrimpId/list/form/:id', component: () => import('pages/food_projection/FoodProjection.vue') },
      { path: '/food_projections/:shrimpId/list/send_mail/:id', component: () => import('pages/food_projection/SendMail.vue') },
      { path: '/food_projections/:projectionId/edit_projection/:dayId', component: () => import('pages/food_projection/EditProjection.vue') }
    ]
  },
  {
    path: '/',
    component: () => import('layouts/Login.vue')
  },
  {
    path: '/change_password/:token',
    component: () => import('layouts/ResetPassword.vue')
  },
  {
    path: '*',
    redirect: '/'
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
