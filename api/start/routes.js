'use strict'

/*
|--------------------------------------------------------------------------
| Routes
  Existen 2 grupos de rutas: Las que necesitan autenticación del usuario y las que no. TOMAR MUY EN CUENTA ESTO
  Dentro del grupo de las que necesitan autenticación se debe especificar el middleware para verificar si el
  usuario(autenticado) tiene el permiso para acceder a esa ruta. Ejemplo: .middleware('verifyPermission:1')
  El número 1 indica el track del permiso que debe tener para acceder a esa ruta
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/', () => {
  return {
    greeting: 'Qué haces aquí?'
  }
})

const addPrefixToGroup = group => {
  // Grupo para rutas con prefijo /api/
  group.prefix("api");
  return group;
};


addPrefixToGroup(
  Route.group(() => {
    // Insertar rutas sin protección de autenticación aquí
    Route.post("login", "UserController.login")
    Route.post("reset_password", "UserController.resetPassword")
    Route.post("verify_reset_token", "UserController.verifyResetToken")
    Route.post("update_password", "UserController.updatePassword")
    Route.get("file2/:dir", "UploadController.getFileByDirectory")
    Route.get("fileImg/:dir", "UploadController.getFileByDirectoryImg")
    Route.get("filepdf/:dir", "UploadController.getFileByDirectoryPDF")
    Route.get("show_file/:dir", "UploadController.getFileByFilename")

    //-------------------Paises---------------------------//
    Route.get("countries", "CountryController.index")
    //-------------------Paises---------------------------//

    //-------------------Provincias---------------------------//
    Route.get("states", "StateController.index")
    //-------------------Provincias---------------------------//

    //-------------------Ciudades---------------------------//
    Route.get("cities", "CityController.index")
    //-------------------Ciudades---------------------------//

     /*-----------------------Generador de PDF------------------------*/
     Route.get('reports', 'PdfReporterGeneratorController.index')
     Route.post('reportsByID/:id', 'PdfGeneratorController.createByFormat')
     Route.get('file/:filename', 'PdfReporterGeneratorController.getFile')
     /*-----------------------Generador de PDF------------------------*/

     /*-----------------------Categorías de formatos------------------------*/
     Route.get('format_categories', 'FormatCategoryController.index')
     /*-----------------------Categorías de formatos------------------------*/

     /*  */

     // Para descargar excel de proyección de pesca
     Route.get("Print_Fishing/:id", "FishingProjectionController.printFishing")
    Route.get('hana_customers', "CustomerController.getHanaCustomers")
  })
);

addPrefixToGroup(
  Route.group(() => {
    // Insertar rutas con protección de autenticación aquí

    //-------------------Usuarios---------------------------//
    Route.get("users", "UserController.index").middleware('verifyPermission:1.1.1')
    Route.post("users", "UserController.store").middleware('verifyPermission:1.1.2')
    Route.get("users/:id", "UserController.show").middleware('verifyPermission:1.1.5')
    Route.put("users/:id", "UserController.update").middleware('verifyPermission:1.1.3')
    Route.delete("users/:id", "UserController.destroy").middleware('verifyPermission:1.1.4')
    //-------------------Usuarios---------------------------//

    //-------------------Roles---------------------------//
    Route.get("roles", "RoleController.index").middleware('verifyPermission:1.2.1')
    Route.post("roles", "RoleController.store").middleware('verifyPermission:1.2.2')
    Route.get("roles/:id", "RoleController.show").middleware('verifyPermission:1.2.3')
    Route.put("roles/:id", "RoleController.update").middleware('verifyPermission:1.2.4')
    Route.delete("roles/:id", "RoleController.destroy").middleware('verifyPermission:1.2.5')
    //-------------------Roles---------------------------//

    //-------------------Permisos---------------------------//
    Route.get("permissions", "PermissionController.index").middleware('verifyPermission:10')
    Route.post("permissions", "PermissionController.store").middleware('verifyPermission:14')
    Route.get("permissions/:id", "PermissionController.show").middleware('verifyPermission:12')
    Route.put("permissions/:id", "PermissionController.update").middleware('verifyPermission:15')
    Route.delete("permissions/:id", "PermissionController.destroy").middleware('verifyPermission:13')
    Route.get("permissionsf", "PermissionController.getFormatedPermissions")
    //-------------------Permisos---------------------------//

    //-------------------Configuración de correo---------------------------//
    Route.get("mail_configurations", "MailConfigurationController.index").middleware('verifyPermission:1.3.1')
    Route.post("mail_configurations", "MailConfigurationController.update").middleware('verifyPermission:1.3.1.1')
    //-------------------Configuración de correo---------------------------//

    //-------------------Eventos---------------------------//
    Route.get("events", "EventController.index").middleware('verifyPermission:1.3.2.1')
    Route.get("events/:id", "EventController.show").middleware('verifyPermission:1.3.2.3')
    Route.put("events/:id", "EventController.update").middleware('verifyPermission:1.3.2.2')
    //-------------------Eventos---------------------------//

    //-------------------Notificaciones---------------------------//
    Route.get("notifications", "NotificationController.index")
    Route.post("notifications_seen/:id", "NotificationController.update")
    //-------------------Notificaciones---------------------------//

    //-------------------Clientes---------------------------//
    Route.get("client", "CustomerController.getClient").middleware('verifyPermission:6.4')
    Route.get("pools_client/:id", "CustomerController.getPools").middleware('verifyPermission:6.4')
    Route.get("customers", "CustomerController.index").middleware('verifyPermission:2.1')
    Route.get("customers/:id", "CustomerController.show").middleware('verifyPermission:2.5')
    Route.post("customers", "CustomerController.store").middleware('verifyPermission:2.2')
    Route.put("customers/:id", "CustomerController.update").middleware('verifyPermission:2.3')
    Route.delete("customers/:id", "CustomerController.destroy").middleware('verifyPermission:2.4')
    Route.get("customer_profile", "CustomerController.getCustomerProfile").middleware('verifyPermission:6.1')
    Route.put("customer_profile", "CustomerController.setCustomerProfile").middleware('verifyPermission:6.1.1')
    Route.get("assigned_technical_advisors", "CustomerController.getAssignedTechnicalAdvisors").middleware('verifyPermission:6.2')
    Route.get("upcoming_events", "CustomerController.getUpcomingEvents").middleware('verifyPermission:6.3')
    //-------------------Clientes---------------------------//

    //-------------------Categorías de clientes---------------------------//
    Route.get("categories", "CategoryController.index").middleware('verifyPermission:2.6')
    Route.get("categories/:id", "CategoryController.show").middleware('verifyPermission:2.10')
    Route.post("categories", "CategoryController.store").middleware('verifyPermission:2.7')
    Route.put("categories/:id", "CategoryController.update").middleware('verifyPermission:2.7')
    Route.delete("categories/:id", "CategoryController.destroy").middleware('verifyPermission:2.9')
    //-------------------Categorías de clientes---------------------------//

    //-------------------Proveedores---------------------------//
    Route.get("providers", "ProviderController.index").middleware('verifyPermission:3.1')
    Route.get("providers/:id", "ProviderController.show").middleware('verifyPermission:3.5')
    Route.post("providers", "ProviderController.store").middleware('verifyPermission:3.2')
    Route.put("providers/:id", "ProviderController.update").middleware('verifyPermission:3.3')
    Route.delete("providers/:id", "ProviderController.destroy").middleware('verifyPermission:3.4')
    Route.post("upload_provider_certifications", "ProviderController.uploadProviderCertifications").middleware('verifyPermission:3.2')
    Route.post("upload_provider_documents", "ProviderController.uploadProviderDocuments").middleware('verifyPermission:3.2')
    //-------------------Proveedores---------------------------//


    //-------------------Productos---------------------------//
    Route.get("products", "ProductController.index").middleware('verifyPermission:4.1')
    Route.get("products/:id", "ProductController.show").middleware('verifyPermission:4.5')
    Route.post("products", "ProductController.store").middleware('verifyPermission:4.2')
    Route.put("products/:id", "ProductController.update").middleware('verifyPermission:4.3')
    Route.delete("products/:id", "ProductController.destroy").middleware('verifyPermission:4.4')
    Route.post("upload_product_img", "ProductController.uploadProductImg").middleware('verifyPermission:4.2')
    //-------------------Productos---------------------------//

//-------------------Productos---------------------------//
    Route.get("products", "ProductController.index").middleware('verifyPermission:4.1')
    Route.get("products/:id", "ProductController.show").middleware('verifyPermission:4.5')
    Route.post("products", "ProductController.store").middleware('verifyPermission:4.2')
    Route.put("products/:id", "ProductController.update").middleware('verifyPermission:4.3')
    Route.delete("products/:id", "ProductController.destroy").middleware('verifyPermission:4.4')
    //-------------------Productos---------------------------//

    //-------------------Categoria de Productos---------------------------//
    Route.get("product_categories", "ProductCategoryController.index").middleware('verifyPermission:4.6')
    Route.get("product_categories/:id", "ProductCategoryController.show").middleware('verifyPermission:4.10')
    Route.post("product_categories", "ProductCategoryController.store").middleware('verifyPermission:4.7')
    Route.put("product_categories/:id", "ProductCategoryController.update").middleware('verifyPermission:4.8')
    Route.delete("product_categories/:id", "ProductCategoryController.destroy").middleware('verifyPermission:4.9')
    //-------------------Categoria de Productos---------------------------//

    //-------------------Unidades de Medida---------------------------//
    Route.get("measure_units", "MeasureUnitController.index").middleware('verifyPermission:4.11')
    Route.get("measure_units/:id", "MeasureUnitController.show").middleware('verifyPermission:4.15')
    Route.post("measure_units", "MeasureUnitController.store").middleware('verifyPermission:4.12')
    Route.put("measure_units/:id", "MeasureUnitController.update").middleware('verifyPermission:4.13')
    Route.delete("measure_units/:id", "MeasureUnitController.destroy").middleware('verifyPermission:4.14')
    //-------------------Unidades de Medida---------------------------//

    //-------------------Unidades de Medida---------------------------//
    Route.get("colors", "ProductController.indexColors").middleware('verifyPermission:4.16')
    //-------------------Unidades de Medida---------------------------//

    //-------------------Segmentos de clientes---------------------------//
    Route.get("customer_segments", "SegmentController.index").middleware('verifyPermission:2.11')
    Route.get("customer_segments/:id", "SegmentController.show").middleware('verifyPermission:2.15')
    Route.post("customer_segments", "SegmentController.store").middleware('verifyPermission:2.12')
    Route.put("customer_segments/:id", "SegmentController.update").middleware('verifyPermission:2.13')
    Route.delete("customer_segments/:id", "SegmentController.destroy").middleware('verifyPermission:2.14')
    //-------------------Segmentos de clientes---------------------------//

    //-------------------Segmentos de proveedores---------------------------//
    Route.get("provider_segments", "SegmentController.indexProvider").middleware('verifyPermission:3.11')
    Route.get("provider_segments/:id", "SegmentController.showProvider").middleware('verifyPermission:3.15')
    Route.post("provider_segments", "SegmentController.storeProvider").middleware('verifyPermission:3.12')
    Route.put("provider_segments/:id", "SegmentController.updateProvider").middleware('verifyPermission:3.13')
    Route.delete("provider_segments/:id", "SegmentController.destroyProvider").middleware('verifyPermission:3.14')
    //-------------------Segmentos de proveedores---------------------------//

    //-------------------Empresas calificadoras---------------------------//
    Route.get("rating_companies", "RatingCompanyController.index").middleware('verifyPermission:1.5.1.1.1')
    Route.get("rating_companies/:id", "RatingCompanyController.show").middleware('verifyPermission:1.5.1.1.3')
    Route.post("rating_companies", "RatingCompanyController.store").middleware('verifyPermission:1.5.1.1.2')
    Route.put("rating_companies/:id", "RatingCompanyController.update").middleware('verifyPermission:1.5.1.1.4')
    Route.delete("rating_companies/:id", "RatingCompanyController.destroy").middleware('verifyPermission:1.5.1.1.5')
    //-------------------Empresas calificadoras---------------------------//

    //-------------------Tipos de calificación---------------------------//
    Route.get("rating_types", "RatingTypeController.index").middleware('verifyPermission:1.5.1.2.1')
    Route.get("rating_types/:id", "RatingTypeController.show").middleware('verifyPermission:1.5.1.2.3')
    Route.post("rating_types", "RatingTypeController.store").middleware('verifyPermission:1.5.1.2.2')
    Route.put("rating_types/:id", "RatingTypeController.update").middleware('verifyPermission:1.5.1.2.4')
    Route.delete("rating_types/:id", "RatingTypeController.destroy").middleware('verifyPermission:1.5.1.2.5')
    //-------------------Tipos de calificación---------------------------//

    //-------------------Categorías de productos---------------------------//
    Route.get("product_categories", "ProductCategoryController.index")
    //-------------------Categorías de productos---------------------------//

    //-------------------Empresas---------------------------//
    Route.get("companies", "CompanyController.index").middleware('verifyPermission:1.6.1')
    Route.get("companies/:id", "CompanyController.show").middleware('verifyPermission:1.6.5')
    Route.post("companies", "CompanyController.store").middleware('verifyPermission:1.6.2')
    Route.put("companies/:id", "CompanyController.update").middleware('verifyPermission:1.6.3')
    Route.delete("companies/:id", "CompanyController.destroy").middleware('verifyPermission:1.6.4')
    //-------------------Empresas---------------------------//

    //-------------------Horarios---------------------------//
    Route.get("schedules", "ScheduleController.index").middleware('verifyPermission:1.7.1')
    Route.put("schedules", "ScheduleController.update").middleware('verifyPermission:1.7.2')
    //-------------------Horarios---------------------------//

    //-------------------Áreas---------------------------//
    Route.get("areas", "AreaController.index").middleware('verifyPermission:1.8.1')
    Route.get("areas/:id", "AreaController.show").middleware('verifyPermission:1.8.5')
    Route.post("areas", "AreaController.store").middleware('verifyPermission:1.8.2')
    Route.put("areas/:id", "AreaController.update").middleware('verifyPermission:1.8.3')
    Route.delete("areas/:id", "AreaController.destroy").middleware('verifyPermission:1.8.4')
    //-------------------Áreas---------------------------//

    //-------------------Departamentos---------------------------//
    Route.get("departments", "DepartmentController.index").middleware('verifyPermission:1.9.1')
    Route.get("departments/:id", "DepartmentController.show").middleware('verifyPermission:1.9.5')
    Route.post("departments", "DepartmentController.store").middleware('verifyPermission:1.9.2')
    Route.put("departments/:id", "DepartmentController.update").middleware('verifyPermission:1.9.3')
    Route.delete("departments/:id", "DepartmentController.destroy").middleware('verifyPermission:1.9.4')
    //-------------------Departamentos---------------------------//

    //-------------------Secciones---------------------------//
    Route.get("sections", "SectionController.index").middleware('verifyPermission:1.10.1')
    Route.get("sections/:id", "SectionController.show").middleware('verifyPermission:1.10.5')
    Route.post("sections", "SectionController.store").middleware('verifyPermission:1.10.2')
    Route.put("sections/:id", "SectionController.update").middleware('verifyPermission:1.10.3')
    Route.delete("sections/:id", "SectionController.destroy").middleware('verifyPermission:1.10.4')
    //-------------------Secciones---------------------------//

    //-------------------Formatos---------------------------//
    Route.get("formats", "FormatController.index").middleware('verifyPermission:1.11.1')
    Route.get("formatStatus", "FormatController.getFormatByStatus");
    Route.get("formats/:id", "FormatController.show");
    Route.put("formatStatus/:id", "FormatController.changeActivo");
    Route.put("formats/:id", "FormatController.update");
    Route.post("formats", "FormatController.store");
    Route.delete("formats/:id", "FormatController.destroy");
    /*-------------------Formatos----------------------*/

    //-------------------Formularios---------------------------//
    Route.get("forms", "SectionController.index").middleware('verifyPermission:1.12.1')
    Route.get("forms/:id", "SectionController.show").middleware('verifyPermission:1.12.5')
    Route.post("forms", "SectionController.store").middleware('verifyPermission:1.12.2')
    Route.put("forms/:id", "SectionController.update").middleware('verifyPermission:1.12.3')
    Route.delete("forms/:id", "SectionController.destroy").middleware('verifyPermission:1.12.4')
    //-------------------Formularios---------------------------//

    //-------------------Zonas---------------------------//
    Route.get("zones/:status", "ZoneController.index").middleware('verifyPermission:5.1.1')
    Route.get("zones/:status/:id", "ZoneController.show").middleware('verifyPermission:5.1.5')
    Route.post("zones", "ZoneController.store").middleware('verifyPermission:5.1.2')
    Route.put("zones/:id", "ZoneController.update").middleware('verifyPermission:5.1.3')
    Route.delete("zones/:status/:id", "ZoneController.destroy").middleware('verifyPermission:5.1.4')
    Route.get("activate_zones/:id/:status", "ZoneController.activate").middleware('verifyPermission:5.1.6')
    //-------------------Zonas---------------------------//

    //-------------------Camaroneras---------------------------//
    Route.get("shrimps/:id", "ShrimpController.show").middleware('verifyPermission:2.1.1.1.3')
    Route.post("shrimps", "ShrimpController.store").middleware('verifyPermission:2.1.1.1.1')
    Route.put("shrimps/:id", "ShrimpController.update").middleware('verifyPermission:2.1.1.1.2')
    Route.get("shrimps", "ShrimpController.index").middleware('verifyPermission:5.5.1')
    Route.get("shrimps_by_customer/:customer_id", "ShrimpController.indexByCustomer").middleware('verifyPermission:2.1')
    Route.delete("shrimps/:id", "ShrimpController.destroy").middleware('verifyPermission:5.5.4')
    Route.get("assignable_shrimps", "ShrimpController.getAssignableShrimps").middleware('verifyPermission:5.5.1')
    //-------------------Camaroneras---------------------------//

    //-------------------Jefes Tecnicos---------------------------//
    Route.get("technical_chiefs", "TechnicalChiefController.index").middleware('verifyPermission:5.2.1')
    Route.get("technical_chiefs_status/:status", "TechnicalChiefController.activateList").middleware('verifyPermission:5.2.1')
    Route.get("technical_chiefs/:id", "TechnicalChiefController.show").middleware('verifyPermission:5.2.5')
    Route.post("technical_chiefs", "TechnicalChiefController.store").middleware('verifyPermission:5.2.2')
    Route.put("technical_chiefs/:id", "TechnicalChiefController.update").middleware('verifyPermission:5.2.3')
    Route.put("technical_chiefs_activate/:id/:status", "TechnicalChiefController.activate").middleware('verifyPermission:5.2.3')
    Route.delete("technical_chiefs/:id", "TechnicalChiefController.destroy").middleware('verifyPermission:5.2.4')
    //-------------------Jefes Tecnicos---------------------------//

    //-------------------Asesores Tecnicos---------------------------//
    Route.get("technical_advisors", "TechnicalAdvisorController.index").middleware('verifyPermission:5.3.1')
    Route.get("technical_advisors_status/:status", "TechnicalAdvisorController.activateList").middleware('verifyPermission:5.3.1')
    Route.get("technical_advisors/:id", "TechnicalAdvisorController.show").middleware('verifyPermission:5.3.5')
    Route.post("technical_advisors", "TechnicalAdvisorController.store").middleware('verifyPermission:5.3.2')
    Route.put("technical_advisors/:id", "TechnicalAdvisorController.update").middleware('verifyPermission:5.3.3')
    Route.put("technical_advisors_activate/:id/:status", "TechnicalAdvisorController.activate").middleware('verifyPermission:5.3.3')
    Route.delete("technical_advisors/:id", "TechnicalAdvisorController.destroy").middleware('verifyPermission:5.3.4')
    Route.get("my_technical_advisors", "TechnicalAdvisorController.getMyTechnicalAdvisor").middleware('verifyPermission:5.3.1')
    //-------------------Asesores Tecnicos---------------------------//

    //-------------------Equipos Tecnicos---------------------------//
    Route.get("technical_equipments", "TechnicalEquipmentController.index").middleware('verifyPermission:5.4.1')
    Route.get("technical_equipments_status/:status", "TechnicalEquipmentController.activateList").middleware('verifyPermission:5.4.1')
    Route.get("technical_equipments/:id", "TechnicalEquipmentController.show").middleware('verifyPermission:5.4.5')
    Route.post("technical_equipments", "TechnicalEquipmentController.store").middleware('verifyPermission:5.4.2')
    Route.put("technical_equipments/:id", "TechnicalEquipmentController.update").middleware('verifyPermission:5.4.3')
    Route.put("technical_equipments_activate/:id/:status", "TechnicalEquipmentController.activate").middleware('verifyPermission:5.4.6')
    Route.delete("technical_equipments/:id", "TechnicalEquipmentController.destroy").middleware('verifyPermission:5.4.4')
    //-------------------Equipos Tecnicos---------------------------//

    //-------------------Equipos Tecnicos---------------------------//
    //Route.get("technical_equipments", "TechnicalEquipmentController.index").middleware('verifyPermission:5.4.1')
    //Route.get("technical_equipments_status/:status", "TechnicalEquipmentController.activateList").middleware('verifyPermission:5.4.1')
    //Route.get("technical_equipments/:id", "TechnicalEquipmentController.show").middleware('verifyPermission:5.4.5')
    Route.post("assign_shrimps/:id", "AssignShrimpController.store").middleware('verifyPermission:5.5.2')
    Route.get("assign_shrimps", "AssignShrimpController.index").middleware('verifyPermission:5.5.1')
    Route.get("assign_shrimps/:id", "AssignShrimpController.show").middleware('verifyPermission:5.5.5')
    //Route.put("technical_equipments/:id", "TechnicalEquipmentController.update").middleware('verifyPermission:5.4.3')
    //Route.put("technical_equipments_activate/:id/:status", "TechnicalEquipmentController.activate").middleware('verifyPermission:5.4.3')
    //-------------------Equipos Tecnicos---------------------------//

    //-------------------Piscinas---------------------------//
    Route.get("pools/:id", "PoolController.show").middleware('verifyPermission:2.1.1.1.6')
    Route.get("pools_shrimps/:shrimp_id", "PoolController.index").middleware('verifyPermission:2.1.1.1.11')
    Route.post("pools", "PoolController.store").middleware('verifyPermission:2.1.1.1.5')
    Route.put("pools/:id", "PoolController.update").middleware('verifyPermission:2.1.1.1.7')
    Route.delete("pools/:id", "PoolController.destroy").middleware('verifyPermission:2.1.1.1.8')
    Route.post("pool_images", "PoolController.uploadPoolImage").middleware('verifyPermission:2.1.1.1.5')
    Route.delete("pool_images/:fileName", "PoolController.deletePoolImage").middleware('verifyPermission:2.1.1.1.5')
    Route.post("exist_pool", "PoolController.getExistPool").middleware('verifyPermission:2.1.1.1.5')
    //-------------------Piscinas---------------------------//

    //-------------------Análisis de aguas---------------------------//
    Route.get("water_analysis", "WaterAnalysisController.index").middleware('verifyPermission:2.1.1.1.15')
    Route.get("water_analysis/:id", "WaterAnalysisController.show").middleware('verifyPermission:2.1.1.1.18')
    Route.post("water_analysis", "WaterAnalysisController.store").middleware('verifyPermission:2.1.1.1.16')
    Route.put("water_analysis/:id", "WaterAnalysisController.update").middleware('verifyPermission:2.1.1.1.19')
    Route.get("water_analysis_print/:id", "PdfGeneratorController.printWaterAnalysis").middleware('verifyPermission:2.1.1.1.15')
    Route.get("water_send_mail_info/:analysisId", "WaterAnalysisController.sendMailInfo").middleware('verifyPermission:2.1.1.1.15')
    Route.post("water_send_mail_info/:analysisId", "WaterAnalysisController.sendMail").middleware('verifyPermission:2.1.1.1.15')
    //-------------------Análisis de aguas---------------------------//
    //-------------------Patologias---------------------------//
    Route.get("patologies_analisys", "PathologyController.index").middleware('verifyPermission:2.1.1.1.9')
    Route.get("patologies_analisys/:id", "PathologyController.show").middleware('verifyPermission:2.1.1.1.10')
    Route.post("patologies_analisys", "PathologyController.store").middleware('verifyPermission:2.1.1.1.11')
    Route.put("patologies_analisys/:id", "PathologyController.update").middleware('verifyPermission:2.1.1.1.12')
    Route.get("pathology_analysis_print/:id", "PdfGeneratorController.printPathologyAnalysis").middleware('verifyPermission:2.1.1.1.32')
    Route.post("analisis_image", "PathologyController.uploadPoolImage").middleware('verifyPermission:2.1.1.1.11')
    Route.post("SendImg", "PathologiesImgController.store").middleware('verifyPermission:2.1.1.1.11')
    Route.get("ImgAnalisys/:id", "PathologiesImgController.show")
    Route.post("img/:id", "PathologiesImgController.destroy").middleware('verifyPermission:2.1.1.1.11')
    // Route.delete("pathologies/:shrimp_id/:id", "PathologyController.destroy").middleware('verifyPermission:2.1.1.1.13')
    Route.post("pathologies_send_mail/:analysisId", "PathologyController.sendMail").middleware('verifyPermission:2.1.1.1.14')
    Route.get("pathologies_send_mail_info/:analysisId", "PathologyController.sendMailInfo").middleware('verifyPermission:2.1.1.1.14')
    //-------------------Patologias---------------------------//

    //-------------------Analisis Fitoplancton---------------------------//
    Route.get("phytoplanktons_analysis", "PhytoplanktonController.index").middleware('verifyPermission:2.1.1.1.18')
    Route.get("phytoplanktons_analysis/:id", "PhytoplanktonController.show").middleware('verifyPermission:2.1.1.1.34')
    Route.post("phytoplanktons_analysis", "PhytoplanktonController.store").middleware('verifyPermission:2.1.1.1.20')
    Route.put("phytoplanktons_analysis/:id", "PhytoplanktonController.update").middleware('verifyPermission:2.1.1.1.21')
    //Route.delete("phytoplanktons_analysis/:shrimp_id/:id", "PhytoplanktonController.destroy").middleware('verifyPermission:2.1.1.1.22')
    Route.post("phytoplanktons_send_mail/:analysisId", "PhytoplanktonController.sendMail").middleware('verifyPermission:2.1.1.1.23')
    Route.get("phytoplanktons_send_mail_info/:analysisId", "PhytoplanktonController.sendMailInfo").middleware('verifyPermission:2.1.1.1.23')
    Route.get("phytoplankton_analysis_print/:id", "PhytoplanktonController.printPhytoplanktonAnalysis").middleware('verifyPermission:2.1.1.1.32')
    //-------------------Analisis Fitoplancton---------------------------//

    //-------------------Análisis de suelos---------------------------//
    Route.get("floor_analysis", "FloorAnalysisController.index").middleware('verifyPermission:2.1.1.1.24')
    Route.get("floor_analysis/:id", "FloorAnalysisController.show").middleware('verifyPermission:2.1.1.1.25')
    Route.post("floor_analysis", "FloorAnalysisController.store").middleware('verifyPermission:2.1.1.1.27')
    Route.put("floor_analysis/:id", "FloorAnalysisController.update").middleware('verifyPermission:2.1.1.1.28')
    Route.get("floor_analysis_print/:id", "PdfGeneratorController.printFloorAnalysis").middleware('verifyPermission:2.1.1.1.26')
    Route.get("floor_send_mail_info/:analysisId", "FloorAnalysisController.sendMailInfo").middleware('verifyPermission:2.1.1.1.26')
    Route.post("floor_send_mail_info/:analysisId", "FloorAnalysisController.sendMail").middleware('verifyPermission:2.1.1.1.26')
    //-------------------Análisis de suelos---------------------------//

    //-------------------Planificación de visitas---------------------//
    Route.get("appointment_plannings", "VisitingPlanningController.index").middleware('verifyPermission:2.16')
    Route.post("appointment_plannings", "VisitingPlanningController.store").middleware('verifyPermission:2.17')
    Route.put("appointment_plannings/:id", "VisitingPlanningController.update").middleware('verifyPermission:2.18')
    Route.delete("appointment_plannings/:id", "VisitingPlanningController.destroy").middleware('verifyPermission:2.19')
    Route.post("approve_appointment_plannings", "VisitingPlanningController.approve").middleware('verifyPermission:2.20')
    Route.post("reject_appointment_plannings", "VisitingPlanningController.reject").middleware('verifyPermission:2.21')
    Route.post("export_calendar", "VisitingPlanningController.printCalendar").middleware('verifyPermission:2.22')
    Route.post("approve_all_appointment_plannings", "VisitingPlanningController.approveAll").middleware('verifyPermission:2.20')
    Route.post("reject_all_appointment_plannings", "VisitingPlanningController.rejectAll").middleware('verifyPermission:2.21')
    Route.post("delete_appointment_planning_requests", "VisitingPlanningController.deletingRequest").middleware('verifyPermission:2.25')
    Route.post("process_editing_appointment_request", "VisitingPlanningController.processEditingAppointmentRequest").middleware('verifyPermission:2.26')
    Route.post("process_deleting_appointment_request", "VisitingPlanningController.processDeletingAppointmentRequest").middleware('verifyPermission:2.27')
    Route.post("accept_rejection", "VisitingPlanningController.acceptRejection").middleware('verifyPermission:2.17')
    Route.post("set_seen_update_request_notification", "VisitingPlanningController.setSeenUpdateRequestNotification")
    Route.post("appointment_plannings_ubication", "VisitingPlanningController.registerUbication").middleware('verifyPermission:2.28')
    //-------------------Planificación de visitas---------------------//

    //-------------------Proyección de alimentación---------------------//
    Route.get("projections_print/:id", "PdfGeneratorController.printProjectionsFood").middleware('verifyPermission:2.1.1.1.29.18')
    Route.get("food_projections", "FoodProjectionController.index").middleware('verifyPermission:2.1.1.1.29.3')
    Route.get("food_projections_mobile", "FoodProjectionController.indexMobile").middleware('verifyPermission:2.1.1.1.29.3')
    Route.post("food_projections", "FoodProjectionController.store").middleware('verifyPermission:2.1.1.1.29.1')
    Route.get("food_projections/:id", "FoodProjectionController.show").middleware('verifyPermission:2.1.1.1.29.2')
    Route.put("food_projections/:id", "FoodProjectionController.update").middleware('verifyPermission:2.1.1.1.29.4')
    Route.get("food_projections_detail/:id", "FoodProjectionController.getProjectionDay").middleware('verifyPermission:2.1.1.1.29.5')
    Route.put("food_projections_detail/:id", "FoodProjectionController.updateProjectionDay").middleware('verifyPermission:2.1.1.1.29.5')
    Route.get("food_projections_bulkload_format", "FoodProjectionController.getBulkLoadFormat").middleware('verifyPermission:2.1.1.1.29.6')
    Route.post("food_projections_upload_bulkload_format", "FoodProjectionController.uploadBulkLoadFormat").middleware('verifyPermission:2.1.1.1.29.6')
    Route.put("food_projections_food_types/:projectionId", "FoodProjectionController.setProjectionFoodTypes").middleware('verifyPermission:2.1.1.1.29.17')
    Route.delete("food_projections_food_types/:id", "FoodProjectionController.deleteFoodTypes").middleware('verifyPermission:2.1.1.1.29.17')
    Route.get("food_send_mail_info/:projectionId", "FoodProjectionController.sendMailInfo").middleware('verifyPermission:2.1.1.1.29.2')
    Route.post("food_send_mail/:projectionId", "FoodProjectionController.sendMail").middleware('verifyPermission:2.1.1.1.29.2')
    Route.put("food_projections_chart/:id", "FoodProjectionController.updateChart")
    //-------------------Proyección de alimentación---------------------//

     //-------------------Motivos de visita---------------------------//
     Route.get("visit_reasons", "VisitReasonController.index").middleware('verifyPermission:2.23.1')
     Route.get("visit_reasons/:id", "VisitReasonController.show").middleware('verifyPermission:2.23.3')
     Route.post("visit_reasons", "VisitReasonController.store").middleware('verifyPermission:2.23.2')
     Route.put("visit_reasons/:id", "VisitReasonController.update").middleware('verifyPermission:2.23.4')
     Route.delete("visit_reasons/:id", "VisitReasonController.destroy").middleware('verifyPermission:2.23.5')
     //-------------------Motivos de visita---------------------------//

     //-------------------Tipos de alimento---------------------------//
    Route.get("food_types", "FoodTypeController.index")
    Route.get("food_types/:id", "FoodTypeController.show").middleware('verifyPermission:1.5.1.2.3')
    Route.post("food_types", "FoodTypeController.store").middleware('verifyPermission:1.5.1.2.2')
    Route.put("food_types/:id", "FoodTypeController.update").middleware('verifyPermission:1.5.1.2.4')
    Route.delete("food_types/:id", "FoodTypeController.destroy").middleware('verifyPermission:1.5.1.2.5')

    //-------------------Tipos de alimento---------------------------//

    //-------------------Proyección de pesca---------------------------//
    Route.get("Print_Fishing/:id", "FishingProjectionController.printFishing").middleware('verifyPermission:2.1.1.1.29.8')
    Route.get("fishing_projections", "FishingProjectionController.index").middleware('verifyPermission:2.1.1.1.29.7')
    Route.get("fishing_projections/:id", "FishingProjectionController.show").middleware('verifyPermission:2.1.1.1.29.9')
    Route.post("fishing_projections", "FishingProjectionController.store").middleware('verifyPermission:2.1.1.1.29.8')
    Route.put("fishing_projections/:id", "FishingProjectionController.update").middleware('verifyPermission:2.1.1.1.29.8')
    Route.get("fishing_send_mail_info/:projectionId", "FishingProjectionController.sendMailInfo").middleware('verifyPermission:2.1.1.1.29.9')
    Route.post("fishing_send_mail/:analysisId", "FishingProjectionController.sendMail").middleware('verifyPermission:2.1.1.1.29.9')
    //-------------------Proyección de pesca---------------------------//

    //-------------------Reporte de crecimiento---------------------------//
    Route.post("growth_reports", "GrowthReportController.store").middleware('verifyPermission:2.1.1.1.29.10')
    Route.put("growth_reports/:id", "GrowthReportController.update").middleware('verifyPermission:2.1.1.1.29.11')
    //-------------------Reporte de crecimiento---------------------------//

    //-------------------Proyección de camarón---------------------------//
    Route.post("shrimp_projections", "ShrimpProjectionController.store").middleware('verifyPermission:2.1.1.1.29.12')
    Route.put("shrimp_projections/:id", "ShrimpProjectionController.update").middleware('verifyPermission:2.1.1.1.29.13')
    //-------------------Proyección de camarón---------------------------//

    //-------------------Proyección de costos---------------------------//
    Route.post("cost_projections", "CostProjectionController.store").middleware('verifyPermission:2.1.1.1.29.14')
    Route.put("cost_projections/:id", "CostProjectionController.update").middleware('verifyPermission:2.1.1.1.29.15')
    //-------------------Proyección de costos---------------------------//

    //-------------------Precio de camarón---------------------------//
    Route.get("shrimp_prices", "ShrimpPriceController.index").middleware('verifyPermission:4.17')
    Route.get("shrimp_prices/:id", "ShrimpPriceController.show").middleware('verifyPermission:4.20')
    Route.post("shrimp_prices", "ShrimpPriceController.store").middleware('verifyPermission:4.18')
    Route.put("shrimp_prices/:id", "ShrimpPriceController.update").middleware('verifyPermission:4.19')
    Route.get("shrimp_prices_last", "ShrimpPriceController.getLastPrice")
    Route.get("shrimp_prices_ranges", "ShrimpPriceController.getShrimpPricesRanges")
    //-------------------Precio de camarón---------------------------//

    //-------------------Reportes de crecimiento camaroneras---------------------------//
    Route.get("shrimp_growth_reports", "ShrimpGrowthReportController.index").middleware('verifyPermission:2.1.1.1.35')
    Route.get("shrimp_growth_reports/:id", "ShrimpGrowthReportController.show").middleware('verifyPermission:2.1.1.1.37')
    Route.post("shrimp_growth_reports", "ShrimpGrowthReportController.store").middleware('verifyPermission:2.1.1.1.36')
    Route.put("shrimp_growth_reports/:id", "ShrimpGrowthReportController.update").middleware('verifyPermission:2.1.1.1.38')
    Route.post("shrimp_growth_reports_pools", "ShrimpGrowthReportController.addPool").middleware('verifyPermission:2.1.1.1.36')
    Route.put("shrimp_growth_reports_pools", "ShrimpGrowthReportController.savePoolData").middleware('verifyPermission:2.1.1.1.36')
    Route.get("shrimp_growth_reports_print/:id", "ShrimpGrowthReportController.printGrowthReports").middleware('verifyPermission:2.1.1.1.47')
    Route.get("growth_reports_send_mail_info/:id", "ShrimpGrowthReportController.sendMailInfo").middleware('verifyPermission:2.1.1.1.48')
    Route.post("growth_reports_send_mail/:id", "ShrimpGrowthReportController.sendMail").middleware('verifyPermission:2.1.1.1.48')
    Route.delete("shrimp_growth_reports_pools", "ShrimpGrowthReportController.deletePool").middleware('verifyPermission:2.1.1.1.53')
    // Route.get("shrimp_prices_last", "ShrimpPriceController.getLastPrice")
    //-------------------Reportes de crecimiento camaroneras---------------------------//

    //-------------------Proyección de costo beneficio---------------------------//
    Route.get("cost_benefit_projections", "CostBenefitProjectionController.index").middleware('verifyPermission:2.1.1.1.39')
    Route.post("cost_benefit_projections", "CostBenefitProjectionController.store").middleware('verifyPermission:2.1.1.1.40')
    Route.post("cost_benefit_projections2", "CostBenefitProjectionController.store2").middleware('verifyPermission:2.1.1.1.40')
    Route.get("cost_benefit_projections/:id", "CostBenefitProjectionController.show").middleware('verifyPermission:2.1.1.1.41')
    Route.put("cost_benefit_projections/:id", "CostBenefitProjectionController.update").middleware('verifyPermission:2.1.1.1.42')
    Route.put("cost_benefit_projections2/:id", "CostBenefitProjectionController.update2").middleware('verifyPermission:2.1.1.1.42')
    Route.get("cost_benefit_projections_print/:id", "CostBenefitProjectionController.printCostBenefitProjection").middleware('verifyPermission:2.1.1.1.49')
    Route.get("cost_benefit_projections_send_mail_info/:id", "CostBenefitProjectionController.sendMailInfo").middleware('verifyPermission:2.1.1.1.50')
    Route.post("cost_benefit_projections_send_mail/:id", "CostBenefitProjectionController.sendMail").middleware('verifyPermission:2.1.1.1.50')
    Route.get("cost_benefit_projections_print_excel", "CostBenefitProjectionController.generateExcelReport").middleware('verifyPermission:2.1.1.1.49')
    Route.get("cost_benefit_projections_shrimp_ranges", "CostBenefitProjectionController.getShrimpRanges").middleware('verifyPermission:2.1.1.1.40')
    //-------------------Proyección de costo beneficio---------------------------//

    //-------------------Reportes de cosechas---------------------------//
    Route.get("harvest_reports", "HarvestReportController.index").middleware('verifyPermission:2.1.1.1.43')
    Route.post("harvest_reports", "HarvestReportController.store").middleware('verifyPermission:2.1.1.1.44')
    Route.get("harvest_reports/:id", "HarvestReportController.show").middleware('verifyPermission:2.1.1.1.45')
    Route.put("harvest_reports/:id", "HarvestReportController.update").middleware('verifyPermission:2.1.1.1.46')
    Route.get("harvest_reports_print/:id", "HarvestReportController.printHarvestReport").middleware('verifyPermission:2.1.1.1.51')
    Route.get("harvest_reports_send_mail_info/:id", "HarvestReportController.sendMailInfo").middleware('verifyPermission:2.1.1.1.52')
    Route.post("harvest_reports_send_mail/:id", "HarvestReportController.sendMail").middleware('verifyPermission:2.1.1.1.52')
    Route.get("general_harvest_report", "HarvestReportController.printGeneralHarvestReport").middleware('verifyPermission:2.30')
    Route.get("harvest_reports_bulkload_format", "HarvestReportController.getBulkLoadFormat").middleware('verifyPermission:2.1.1.1.44')
    Route.post("harvest_reports_upload_bulkload_format", "HarvestReportController.uploadBulkLoadFormat").middleware('verifyPermission:2.1.1.1.44')
    //-------------------Reportes de cosechas---------------------------//

    //------------------- Laboratorios ---------------------------------//
    Route.get("laboratories", "LaboratoryController.index").middleware('verifyPermission:4.26')
    Route.post("laboratories", "LaboratoryController.store").middleware('verifyPermission:4.27')
    Route.get("laboratories/:id", "LaboratoryController.show").middleware('verifyPermission:4.30')
    Route.put("laboratories/:id", "LaboratoryController.update").middleware('verifyPermission:4.28')
    Route.delete("laboratories/:id", "LaboratoryController.destroy").middleware('verifyPermission:4.29')
    //------------------- Laboratorios ---------------------------------//

    //-------------------Compendio---------------------------//
    Route.get("compendium", "CompendiumController.index").middleware('verifyPermission:2.1.1.1.54')
    Route.get("compendium/:id", "CompendiumController.show").middleware('verifyPermission:2.1.1.1.56')
    Route.post("compendium", "CompendiumController.store").middleware('verifyPermission:2.1.1.1.55')
    Route.put("compendium/:id", "CompendiumController.update").middleware('verifyPermission:2.1.1.1.57')
    Route.post("compendium_growth_reports", "CompendiumController.storeGrowthReport").middleware('verifyPermission:2.1.1.1.58')
    Route.put("compendium_growth_reports/:id", "CompendiumController.updateGrowthReport").middleware('verifyPermission:2.1.1.1.59')
    Route.post("compendium_print", "CompendiumController.printReport").middleware('verifyPermission:2.1.1.1.60')
    Route.get("compendiums_by_foods", "CompendiumController.getCompendiumsByFoods")
    Route.get("compendiums_print_xls", "CompendiumController.printReportXls")
    //-------------------Compendio---------------------------//

    //------------------- Crecimiento de camaroneras ---------------------------------//
    Route.get("shrimp_growths", "ShrimpGrowthController.index")
    Route.get("shrimp_growths_years", "ShrimpGrowthController.getWeeks")
    Route.post("shrimp_growths", "ShrimpGrowthController.store")
    Route.get("shrimp_growth_report", "ShrimpGrowthController.printReport")
    // Route.put("shrimp_growths/:id", "ShrimpGrowthController.update")
    // Route.delete("shrimp_growths/:id", "ShrimpGrowthController.destroy")
    //------------------- Crecimiento de camaroneras ---------------------------------//

    //------------------- Otras configuraciones ---------------------------------//
    // Route.get("shrimp_growths", "ShrimpGrowthController.index")
    Route.get("configurations", "ConfigurationController.index")
    Route.post("configurations", "ConfigurationController.store")
    // Route.get("shrimp_growth_report", "ShrimpGrowthController.printReport")
    //------------------- Otras configuraciones ---------------------------------//

    //get_coordinates
    Route.get("get_coordinates", "ShrimpController.getCoordinates")
  }).middleware("auth")
);
