'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const User = use('App/Models/User')
class VerifyPermission {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle ({ request, auth, response }, next, properties) {
    // call next to advance the request
    let permission = properties[0]
    let user = await auth.getUser()
    let userInfo = (await User.query().with('roles.permissions').where('id' ,user.id).first()).toJSON()

    let permissions = userInfo.roles ? User.getPermission(userInfo.roles) : []
    let found = permissions.find(value => value == permission)

    if (!found) {
      response.status(403).send('Usuario no autorizado a esta funcionalidad')
      return
    }

    await next()
  }

  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async wsHandle ({ request }, next) {
    // call next to advance the request
    await next()
  }
}

module.exports = VerifyPermission
