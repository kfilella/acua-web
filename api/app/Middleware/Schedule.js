'use strict'
/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const ScheduleModel = use('App/Models/Schedule')
class Schedule {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle ({ auth, request }, next) {
    // call next to advance the request
    /* const user = await auth.getUser()
    console.log('user', user)
    var today = new Date()
    var numberDay = today.getDay() + 1
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds()

    const canUse = await ScheduleModel.query().where('id', numberDay).where('start_time', '<=', time).where('end_time', '>=', time).first()

    if (canUse || user.id === 1) {
      await next()
    } else {
      response.status(403).send('Operación no permitida en este horario')
      return
    } */
    await next()

  }

  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async wsHandle ({ request }, next) {
    // call next to advance the request
    await next()
  }
}

module.exports = Schedule
