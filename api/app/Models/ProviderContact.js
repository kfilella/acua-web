'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ProviderContact extends Model {
  phones () {
    return this.hasMany('App/Models/ProviderContactPhone', 'id', 'provider_contact_id')
  }
  emails () {
    return this.hasMany('App/Models/ProviderContactEmail', 'id', 'provider_contact_id')
  }
}

module.exports = ProviderContact
