'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Role extends Model {
  static get fillable() {
    return ['name', 'description']
  }
  static fieldValidationRules () {
    return {
      name: 'required|string|unique:roles,name',
      description: 'string'
    }
  }
  static fieldValidationRulesUpdate () {
    return {
      name: 'required|string',
      description: 'string'
    }
  }
  permissions () {
    return this.belongsToMany('App/Models/Permission', 'role_id', 'permission_track', 'id', 'track')
  }
  static boot () {
    super.boot()

    this.addTrait('@provider:Lucid/SoftDeletes')
  }
}

module.exports = Role
