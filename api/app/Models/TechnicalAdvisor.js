'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class TechnicalAdvisor extends Model {
  static get fillable () {
    return [
      'code', 'full_name', 'phone', 'company_mail', 'personal_mail', 'zone'
    ]
  }
  static fieldValidationRules () {
    return {
      code: 'string',
      full_name: 'string|required',
      phone: 'string',
      company_mail: 'string',
      personal_mail: 'string',
      zone: 'array|required',
    }
  }
  user () {
    return this.hasOne('App/Models/User', 'user_id', 'id')
  }
  assignedShrimps () {
    return this.hasMany('App/Models/AssignShrimp', 'id', 'technical_advisor_id')
  }
}

module.exports = TechnicalAdvisor
