'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Category extends Model {
  /* static boot () {
    super.boot()
    this.addTrait('@provider:Lucid/SoftDeletes')
  } */
  static get fillable () {
    return [ 'name', 'type' ]
  }
  static fieldValidationRules () {
    return {
      name: 'required|string',
      type: 'required|number'
    }
  }
}

module.exports = Category
