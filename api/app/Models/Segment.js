'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Segment extends Model {
  static get fillable () {
    return [
      'name', 'type'
    ]
  }
  static fieldValidationRules () {
    return {
      name: 'required|string',
      type: 'required|number'
    }
  }
}

module.exports = Segment
