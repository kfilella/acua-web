'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Product extends Model {
  static boot () {
    super.boot()
    this.addTrait('@provider:Lucid/SoftDeletes')
  }
  static fieldValidationRules () {
    return {
      name: 'required|string'
    }
  }

  static get fillable() {
    return [
      'name', 'product_type', 'product_category_id', 'internal_reference', 'barcode', 'last_price_purchase', 'measure_unit_id',
      'especifications', 'color_id', 'brand', 'color', 'from_weight', 'to_weight', 'price_kg'
    ]
  }

  history_product_price () {
    return this.hasMany('App/Models/ProductPriceHistory', 'id', 'product_id')
  }

  providers_product_price () {
    return this.hasMany('App/Models/ProviderProductPrice', 'id', 'product_id')
  }

  list_img () {
    return this.hasMany('App/Models/ProductImage', 'id', 'product_id')
  }

  measure_units () {
    return this.hasMany('App/Models/MeasureUnit', 'id', 'measure_unit_id')
  }
  product_category () {
    return this.hasMany('App/Models/ProductCategory', 'product_category_id', 'id')
  }
  color () {
    return this.hasMany('App/Models/Color', 'color_id', 'id')
  }
}

module.exports = Product
