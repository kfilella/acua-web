'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ProviderDocument extends Model {
}

module.exports = ProviderDocument
