'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ShrimpGrowth extends Model {
  static get fillable () {
    return ['shrimp_id', 'year', 'week', 'food_id', 'inc']
  }
  static fieldValidationRules () {
    return {
      shrimp_id: 'required|number',
      year: 'required|number',
      week: 'required|number',
      food_id: 'required|number',
      inc: 'required|number'
    }
  }
  shrimp () {
    return this.belongsTo('App/Models/Shrimp', 'shrimp_id', 'id')
  }
  food () {
    return this.belongsTo('App/Models/Product', 'food_id', 'id')
  }

}

module.exports = ShrimpGrowth
