'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ShrimpGrowthReport extends Model {
  static get fillable () {
    return ['shrimp_id', 'technical_advisor_id', 'begin_date', 'end_date']
  }
  static fieldValidationRules () {
    return {
      shrimp_id: 'required|number',
      // technical_advisor_id: 'required|number',
      begin_date: 'required|date',
      end_date: 'required|date'
    }
  }
  technical_advisor () {
    return this.belongsTo('App/Models/TechnicalAdvisor', 'technical_advisor_id', 'id')
  }
  size () {
    return this.hasMany('App/Models/ShrimpGrowthReportSize', 'id', 'shrimp_growth_report_id')
  }
  shrimp () {
    return this.belongsTo('App/Models/Shrimp', 'shrimp_id', 'id')
  }
}

module.exports = ShrimpGrowthReport
