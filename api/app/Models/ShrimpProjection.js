'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ShrimpProjection extends Model {
  static get fillable () {
    return [
      'fishing_projection_id', 'harvest_weight', 'percent_sup'
    ]
  }
  static fieldValidationRules () {
    return {
      fishing_projection_id: 'required|integer',
      harvest_weight: 'required|number',  
      percent_sup: 'required|number'
    }
  }
}

module.exports = ShrimpProjection
