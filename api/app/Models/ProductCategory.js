'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ProductCategory extends Model {
  static fieldValidationRules () {
    return {
      name: 'required|string'
    }
  }

  static get fillable() {
    return [
      'name'
    ]
  }
}

module.exports = ProductCategory
