'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Pool extends Model {
  static boot () {
    super.boot()
    this.addTrait('@provider:Lucid/SoftDeletes')
  }
  static get fillable () {
    return ['shrimp_id', 'description', 'area']
  }
  static fieldValidationRules () {
    return {
      shrimp_id: 'required|number',
      description: 'required|string',
      area: 'required|number'
    }
  }
  images () {
    return this.hasMany('App/Models/PoolImage', 'id', 'pool_id')
  }
  shrimp () {
    return this.belongsTo('App/Models/Shrimp', 'shrimp_id', 'id')
  }
  pathologyAnalisis () {
    return this.hasMany('App/Models/PatologiesAnalysisDetail', 'id', 'pool_id')
  }
}

module.exports = Pool
