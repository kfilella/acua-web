'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ProviderContactPhone extends Model {
}

module.exports = ProviderContactPhone
