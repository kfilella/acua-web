'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class FishingProjection extends Model {
  static boot () {
    super.boot()
    this.addTrait('@provider:Lucid/SoftDeletes')
  }
  static get fillable () {
    return [
      'pool_id', 'density', 'seedtime', 'report_date', 'sowing_weight', 'dry_days', 'weight', 'mp', 'kg_acum', 'last_four', 'cur_kg_ha', 'observations'
    ]
  }
  static fieldValidationRules () {
    return {
      pool_id: 'required|integer',
      density: 'required|number',
      seedtime: 'required|date',
      report_date: 'required|date',
      sowing_weight: 'required|number',
      dry_days: 'required|integer',
      weight: 'required|number',
      mp: 'required|number',
      kg_acum: 'required|number',
      last_four: 'required|number',
      cur_kg_ha: 'required|number',
      observations: 'string'

    }
  }
  projections () {
    return this.hasMany('App/Models/FishingProjectionDetail', 'id', 'fishing_projection_id')
  }
  shrimpProjection () {
    return this.hasOne('App/Models/ShrimpProjection')
  }
  pool () {
    return this.belongsTo('App/Models/Pool')
  }
  costProjection () {
    return this.hasOne('App/Models/CostProjection')
  }
  technicalAdvisor () {
    return this.belongsTo('App/Models/TechnicalAdvisor', 'technical_advisor_id', 'id')
  }
}

module.exports = FishingProjection
