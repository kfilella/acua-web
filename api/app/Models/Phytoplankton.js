'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Phytoplankton extends Model {
  static get fillable () {
    return ['pool_id', 'diatomes', 'chlorophytes', 'cyanophites', 'others', 'observation', 'dinoflagelados']
  }
  static fieldValidationRules () {
    return {
      pool_id: 'required|number',
      diatomes: 'number',
      dinoflagelados: 'number',
      chlorophytes: 'number',
      cyanophites: 'number',
      others: 'number',
      observation: 'string'
    }
  }
  pool_shrimp () {
    return this.hasOne('App/Models/Pool', 'pool_id', 'id')
  }
}

module.exports = Phytoplankton
