'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class PatologiesAnalysisDetail extends Model {
  static boot () {
    super.boot()
    this.addTrait('@provider:Lucid/SoftDeletes')
  }
  static get table () {
    return 'patologies_analysis_details'
  }
  pool () {
    return this.hasOne('App/Models/Pool', 'pool_id', 'id')
  }
  master () {
    return this.hasOne('App/Models/PatologiesAnalysisPool', 'patologies_analysis_pools_id', 'id')
  }
  images () {
    return this.hasMany('App/Models/PathologiesImg', 'id', 'patologies_analysis_detail_id')
  }
}

module.exports = PatologiesAnalysisDetail
