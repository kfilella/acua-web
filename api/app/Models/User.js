'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

/** @type {import('@adonisjs/framework/src/Hash')} */
const Hash = use('Hash')

class User extends Model {
  static get hidden () {
    return ['password']
  }
  static get fillable() {
    return ['email', 'password']
  }
  static getPermission(roles) {
    let permissions = []
    for (let i of roles) {
      for (let j of i.permissions) {
        permissions.push(j.track)
      }
    }
    return permissions
  }
  static fieldValidationRules () {
    return {
      email: 'required|email|unique:users,email',
      password: 'required'
    }
  }
  static fieldValidationRulesUpdate () {
    return {
      email: 'required|email',
      password: 'required'
    }
  }
  static boot () {
    super.boot()
    this.addTrait('@provider:Lucid/SoftDeletes')

    /**
     * A hook to hash the user password before saving
     * it to the database.
     */
    this.addHook('beforeSave', async (userInstance) => {
      if (userInstance.dirty.password) {
        userInstance.password = await Hash.make(userInstance.password)
      }
    })
  }

  /**
   * A relationship on tokens is required for auth to
   * work. Since features like `refreshTokens` or
   * `rememberToken` will be saved inside the
   * tokens table.
   *
   * @method tokens
   *
   * @return {Object}
   */
  tokens () {
    return this.hasMany('App/Models/Token')
  }
  roles () {
    return this.belongsToMany('App/Models/Role', 'user_id', 'role_id', 'id', 'id')
  }
  events () {
    return this.hasMany('App/Models/UserEvent', 'id', 'user_id')
  }
  technical_advisor () {
    return this.hasOne('App/Models/TechnicalAdvisor', 'id', 'user_id')
  }
  technical_chief () {
    return this.hasOne('App/Models/TechnicalChief', 'id', 'user_id')
  }
}

module.exports = User
