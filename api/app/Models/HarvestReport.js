'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class HarvestReport extends Model {
  static boot () {
    super.boot()
    this.addTrait('@provider:Lucid/SoftDeletes')
  }
  static get fillable () {
    return [
      'pool_id', 'seed_type', 'seed_time', 'harvest_date', 'laboratory_id', 'seed', 'seed_weight',
      'kg_food', 'us_dollar_pound', 'us_dollar_kg', 'cost_pound', 'us_dollar_day_ha', 'pounds_first_thinning', 'pounds_second_thinning',
      'pounds_third_thinning', 'pounds_fourth_thinning', 'weight_first_thinning', 'weight_second_thinning', 'weight_third_thinning',
      'weight_fourth_thinning', 'start_food_id', 'dollar_pound_first_thinning', 'dollar_pound_second_thinning', 'dollar_pound_third_thinning', 'dry_days',
      'cost_kilo_food', 'youth_cost'
    ]
  }
  static fieldValidationRules () {
    return {
      pool_id: 'required|integer',
      seed_type: 'required|integer',
      seed_time: 'required|date',
      harvest_date: 'required|date',
      laboratory_id: 'required|integer',
      seed: 'required|number',
      seed_weight: 'required|number',
      kg_food: 'required|number',
      us_dollar_pound: 'required|number',
      us_dollar_kg: 'number',
      cost_pound: 'number',
      us_dollar_day_ha: 'required|number',
      pounds_first_thinning: 'number',
      pounds_second_thinning: 'number',
      pounds_third_thinning: 'number',
      pounds_fourth_thinning: 'required|number',
      weight_first_thinning: 'number',
      weight_second_thinning: 'number',
      weight_third_thinning: 'number',
      weight_fourth_thinning: 'required|number',
      start_food_id: 'integer',
      dollar_pound_first_thinning: 'number',
      dollar_pound_second_thinning: 'number',
      dollar_pound_third_thinning: 'number',
      dry_days: 'required|integer',
      cost_kilo_food: 'required|number',
      youth_cost: 'required|number'
    }
  }

  pool () {
    return this.belongsTo('App/Models/Pool', 'pool_id', 'id')
  }
  startFoods () {
    return this.hasMany('App/Models/HarvestReportStartFood', 'id', 'harvest_report_id')
  }
  fatteningFoods () {
    return this.hasMany('App/Models/HarvestReportFatteningFood', 'id', 'harvest_report_id')
  }
  laboratory () {
    return this.belongsTo('App/Models/Laboratory', 'laboratory_id', 'id')
  }
  startFood () {
    return this.belongsTo('App/Models/Product', 'start_food_id', 'id')
  }
  technicalAdvisor () {
    return this.belongsTo('App/Models/TechnicalAdvisor', 'technical_advisor_id', 'id')
  }
}

module.exports = HarvestReport
