'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Compendium extends Model {
  static boot () {
    super.boot()
    this.addTrait('@provider:Lucid/SoftDeletes')
  }
  static get fillable () {
    return ['shrimp_id', 'pool_id', 'seed_date', 'sowing_size', 'product_id', 'cam']
  }
  static fieldValidationRules () {
    return {
      shrimp_id: 'required|number',
      pool_id: 'required|number',
      seed_date: 'required|date',
      sowing_size: 'required|number',
      product_id: 'required|number',
      cam: 'required|number'
    }
  }

  pools () {
    return this.hasMany('App/Models/CompendiumPool', 'id', 'compendium_id')
  }

  shrimp () {
    return this.belongsTo('App/Models/Shrimp', 'shrimp_id', 'id')
  }
}

module.exports = Compendium
