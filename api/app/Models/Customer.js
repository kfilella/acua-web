'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Customer extends Model {
  static boot () {
    super.boot()
    this.addTrait('@provider:Lucid/SoftDeletes')
  }
  static get fillable() {
    return [
      'type', 'foreign_company', 'legal_name', 'comercial_name', 'document_type', 'document_number', 'deliver_address', 'code',
      'electronic_documents_email', 'country_id', 'state_id', 'city_id', 'billing_address', 'phone', 'tags', 'zone', 'sector',
      'category_id'
    ]
  }
  static fieldValidationRules () {
    return {
      type: 'required|number',
      legal_name: 'required|string',
      document_type: 'required',
      document_number: 'required|number',
      country_id: 'required|number',
      category_id: 'required|number'
    }
  }

  deliver_addresses () {
    return this.hasMany('App/Models/DeliverAddress', 'id', 'customer_id')
  }
  contacts () {
    return this.hasMany('App/Models/Contact', 'id', 'customer_id')
  }
  shrimps () {
    return this.hasMany('App/Models/Shrimp', 'id', 'customer_id')
  }
  zones () {
    return this.belongsToMany('App/Models/Zone', 'customer_id', 'zone_id', 'id', 'id')
  }
  emails () {
    return this.hasMany('App/Models/CustomerEmail', 'id', 'customer_id')
  }
  phones () {
    return this.hasMany('App/Models/CustomerPhone', 'id', 'customer_id')
  }
  state () {
    return this.belongsTo('App/Models/State', 'state_id', 'id')
  }
}

module.exports = Customer
