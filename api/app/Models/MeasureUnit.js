'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class MeasureUnit extends Model {
  static fieldValidationRules () {
    return {
      full_name: 'required|string'
    }
  }

  static get fillable() {
    return [
      'full_name', 'slug'
    ]
  }
}

module.exports = MeasureUnit
