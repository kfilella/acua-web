'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class FoodType extends Model {
  static fieldValidationRules () {
    return {
      code: 'required|string',
      type: 'required|string',
      price_kg: 'required|number',
      from_weight: 'required|number',
      to_weight: 'required|number'
    }
  }
  static get fillable() {
    return [
      'code', 'type', 'price_kg', 'from_weight', 'to_weight'
    ]
  }
}

module.exports = FoodType
