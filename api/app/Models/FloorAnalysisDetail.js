'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class FloorAnalysisDetail extends Model {
  static boot () {
    super.boot()
    this.addTrait('@provider:Lucid/SoftDeletes')

    this.addHook('beforeUpdate', async (userInstance) => {
      console.log('beforeSave')
      for (let propName in userInstance) { 
        if (userInstance[propName] === '') {
          userInstance[propName] = null
        }
      }
    })
  }
  static get table () {
    return 'floor_analysis_details'
  }
  pool () {
    return this.hasOne('App/Models/Pool', 'pool_id', 'id')
  }
}

module.exports = FloorAnalysisDetail
