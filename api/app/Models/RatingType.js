'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class RatingType extends Model {
  static get fillable () {
    return [
      'name'
    ]
  }
  static fieldValidationRules () {
    return {
      name: 'required|string'
    }
  }
}

module.exports = RatingType
