'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class VisitingPlanning extends Model {
  static boot () {
    super.boot()
    this.addTrait('@provider:Lucid/SoftDeletes')
  }
  static get fillable () {
    return [
      'event_type_id', 'customer_id', 'technical_advisor_id', 'title', 'details', 'date', 'time', 'duration',
      'icon', 'days', 'shrimp_id'
    ]
  }
  static fieldValidationRules () {
    return {
      event_type_id: 'required|integer',
      customer_id: 'required|integer',
      title: 'required|string',
      date: 'required',
      time: 'required',
      shrimp_id: 'required|integer'
    }
  }
  customer () {
    return this.hasOne('App/Models/Customer', 'customer_id', 'id')
  }
  advisor () {
    return this.hasOne('App/Models/TechnicalAdvisor', 'technical_advisor_id', 'user_id')
  }
  shrimp () {
    return this.hasOne('App/Models/Shrimp', 'shrimp_id', 'id')
  }
  visitReason () {
    return this.hasOne('App/Models/VisitReason', 'event_type_id', 'id')
  }
  updateRequest () {
    return this.hasOne('App/Models/VisitingPlanningUpdateRequest', 'id', 'visiting_planning_id')
  }
  updateRequests () {
    return this.hasMany('App/Models/VisitingPlanningUpdateRequest', 'id', 'visiting_planning_id')
  }
}

module.exports = VisitingPlanning
