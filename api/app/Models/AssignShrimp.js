'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class AssignShrimp extends Model {
  static get fillable () {
    return ['shrimps']
  }
  static fieldValidationRules () {
    return {
      shrimps: 'array'
    }
  }
  shrimp () {
    return this.hasOne('App/Models/Shrimp', 'shrimp_id', 'id')
  }
  advisor () {
    return this.hasOne('App/Models/TechnicalAdvisor', 'technical_advisor_id', 'id')
  }
}

module.exports = AssignShrimp
