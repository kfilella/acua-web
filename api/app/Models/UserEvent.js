'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class UserEvent extends Model {
  event () {
    return this.hasOne('App/Models/Event', 'event_id', 'id')
  }
}

module.exports = UserEvent
