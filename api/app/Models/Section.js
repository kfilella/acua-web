'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Section extends Model {
    static get fillable () {
        return [ 'department_id', 'description' ]
      }
      static fieldValidationRules () {
        return {
          description: 'required|string'
        }
      }
    
      department () {
        return this.hasOne('App/Models/Department', 'department_id', 'id')
      }
}

module.exports = Section
