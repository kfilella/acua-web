'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class CompendiumPool extends Model {
  static boot () {
    super.boot()
    this.addTrait('@provider:Lucid/SoftDeletes')
  }

  pool () {
    return this.belongsTo('App/Models/Pool', 'pool_id', 'id')
  }
  growthReports () {
    return this.hasMany('App/Models/CompendiumGrowthReport')
  }
  product () {
    return this.belongsTo('App/Models/Product', 'product_id', 'id')
  }
}

module.exports = CompendiumPool
