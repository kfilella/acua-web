'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ShrimpPrice extends Model {
  static get fillable () {
    return [
      'price', 'date', 'range_id'
    ]
  }
  static fieldValidationRules () {
    return {
      date: 'required|date',
      range_id: 'required|number'
    }
  }
}

module.exports = ShrimpPrice
