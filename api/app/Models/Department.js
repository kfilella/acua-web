'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Department extends Model {
  static get fillable () {
    return [ 'area_id', 'name' ]
  }
  static fieldValidationRules () {
    return {
      name: 'required|string'
    }
  }

  area () {
    return this.hasOne('App/Models/Area', 'area_id', 'id')
  }
  sections () {
    return this.hasMany('App/Models/Section', 'id', 'department_id')
  }
}

module.exports = Department
