'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class VisitReason extends Model {
  static get fillable () {
    return [
      'name', 'require_approval'
    ]
  }
  static fieldValidationRules () {
    return {
      name: 'required|string',
      require_approval: 'required|boolean'
    }
  }
}

module.exports = VisitReason
