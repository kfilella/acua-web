'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class PatologiesAnalysisPool extends Model {
  static get table () {
    return 'patologies_analysis_pools'
  }
  static get fillable () {
    return [
      'shrimp_id', 'analysis_date', 'technical_advisor_id', 'zone_id', 'applicant_id', 'delivery_date', 'sampling_date',
      'observations', 'recommendations'
    ]
  }
  static fieldValidationRules () {
    return {
      shrimp_id: 'required|integer',
      technical_advisor_id: 'required|integer',

    }
  }
  user () {
    return this.hasOne('App/Models/User', 'technical_advisor_id', 'id')
  }
  zone () {
    return this.hasOne('App/Models/Zone', 'zone_id', 'id')
  }
  pools () {
    return this.hasMany('App/Models/PatologiesAnalysisDetail', 'id', 'patologies_analysis_pools_id')
  }
  shrimp () {
    return this.hasOne('App/Models/Shrimp', 'shrimp_id', 'id')
  }
  applicant () {
    return this.hasOne('App/Models/Contact', 'applicant_id', 'id')
  }
}

module.exports = PatologiesAnalysisPool
