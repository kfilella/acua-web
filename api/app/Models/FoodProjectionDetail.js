'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class FoodProjectionDetail extends Model {
  static get table () {
    return 'food_projection_detail'
  }
}

module.exports = FoodProjectionDetail
