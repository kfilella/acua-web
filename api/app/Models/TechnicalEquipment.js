'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class TechnicalEquipment extends Model {
  static get fillable () {
    return [
      'code', 'team_name', 'technical_chief', 'zone', 'technical_advisors'
    ]
  }
  static fieldValidationRules () {
    return {
      code: 'string',
      team_name: 'string|required',
      technical_chief: 'required',
      zone: 'array|required',
      technical_advisors: 'array|required'
    }
  }
  chief () {
    return this.belongsTo('App/Models/TechnicalChief', 'technical_chief', 'id')
  }
}

module.exports = TechnicalEquipment
