'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class FoodProjectionFoodType extends Model {
  foodName () {
    return this.belongsTo('App/Models/Product', 'food_type_id', 'id')
  }
}

module.exports = FoodProjectionFoodType
