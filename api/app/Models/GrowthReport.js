'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class GrowthReport extends Model {
  static get fillable () {
    return [
      'fishing_projection_id', 'date', 'size', 'percent', 'estimated', 'dist', 'mr', 'mf'
    ]
  }
  static fieldValidationRules () {
    return {
      fishing_projection_id: 'required|integer',
      date: 'required|date',  
      size: 'required|number',
      percent: 'number',
      estimated: 'number',
      dist: 'number',
      mr: 'number',
      mf: 'number'
    }
  }
}

module.exports = GrowthReport
