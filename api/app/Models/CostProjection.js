'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class CostProjection extends Model {
  static get fillable () {
    return [
      'fishing_projection_id', 'pounds_raleo', 'weight_raleo', 'cost_juv', 'kg_ha_food_day', 'kg_acum', 'price_kilo_food',
      'dry_days', 'cost_ha_days'
    ]
  }
  static fieldValidationRules () {
    return {
      fishing_projection_id: 'required|integer',
      pounds_raleo: 'required|number',  
      weight_raleo: 'required|number',
      cost_juv: 'required|number',
      kg_ha_food_day: 'required|number',
      kg_acum:  'required|number',
      price_kilo_food:  'required|number',
      dry_days: 'required|integer',
      cost_ha_days:  'required|number'
    }
  }
}

module.exports = CostProjection
