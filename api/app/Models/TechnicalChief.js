'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class TechnicalChief extends Model {
  static get fillable () {
    return [
      'code', 'full_name', 'phone', 'company_mail', 'personal_mail', 'zone'
    ]
  }
  static fieldValidationRules () {
    return {
      code: 'string',
      full_name: 'string|required',
      phone: 'string',
      company_mail: 'string',
      personal_mail: 'string',
      zone: 'array|required',
    }
  }
  teams () {
    return this.hasMany('App/Models/TechnicalEquipment', 'id', 'technical_chief')
  }
}

module.exports = TechnicalChief
