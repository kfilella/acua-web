'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Company extends Model {
  static boot () {
    super.boot()
    this.addTrait('@provider:Lucid/SoftDeletes')
  }

  static get fillable () {
    return [
      'legal_name', 'comercial_name'
    ]
  }
  static fieldValidationRules () {
    /* Solo los campos flllables pertenecen a la tabla, el resto deben ser enviados en la petición pero pertenecen a otras
    tablas */
    return {
      legal_name: 'required|string',
      comercial_name: 'required|string',
      email: 'required|email',
      password: 'required|string|min:8',
      licence_duration: 'required|number',
      licence_time: 'required|number',
      limit_users: 'required|number'
    }
  }
}

module.exports = Company
