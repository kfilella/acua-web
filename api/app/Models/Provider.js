'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Provider extends Model {
  static boot () {
    super.boot()
    this.addTrait('@provider:Lucid/SoftDeletes')
  }
  static get fillable() {
    return [
      'type', 'foreign_company', 'legal_name', 'comercial_name', 'document_type', 'document_number', 'deliver_address', 'code',
      'electronic_documents_email', 'country_id', 'state_id', 'city_id', 'billing_address', 'phone', 'tags', 'zone', 'sector',
      'category_id', 'segment_id', 'rating_type_id', 'rating_company_id', 'supplier_qualification', 'website', 'product_categories'
    ]
  }
  static fieldValidationRules () {
    return {
      type: 'required|number',
      legal_name: 'required|string',
      document_type: 'required',
      document_number: 'required|number',
      country_id: 'required|number',
      category_id: 'required|number'
    }
  }

  history_providers_product_price () {
    return this.hasMany('App/Models/ProviderProductPrice', 'id', 'provider_id')
  }

  deliver_addresses () {
    return this.hasMany('App/Models/ProviderDeliverAddress', 'id', 'provider_id')
  }
  contacts () {
    return this.hasMany('App/Models/ProviderContact', 'id', 'provider_id')
  }
  certifications () {
    return this.hasMany('App/Models/ProviderCertification', 'id', 'provider_id')
  }
  documents () {
    return this.hasMany('App/Models/ProviderDocument', 'id', 'provider_id')
  }
}

module.exports = Provider
