'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Contact extends Model {
  phones () {
    return this.hasMany('App/Models/ContactPhone', 'id', 'contact_id')
  }
  emails () {
    return this.hasMany('App/Models/ContactEmail', 'id', 'contact_id')
  }

}

module.exports = Contact
