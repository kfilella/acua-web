'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class CostBenefitProjectionDetail extends Model {
  static boot () {
    super.boot()
    this.addTrait('@provider:Lucid/SoftDeletes')
  }
  static get fillable () {
    return [
      'pool_id', 'thinning', 'planting_type', 'density', 'survival', 'size_grs',
      'size_planting', 'cent_gr', 'size_thinning', 'pounds_thinning', 'cost_per_thounsand', 'fc',
      'us_kg_balance', 'production_days', 'stopping_days', 'cost_day_ha', 'initial_biomass',
      'food_id'
    ]
  }
  static fieldValidationRules () {
    return {
      pool_id: 'required|integer',  
      thinning: 'required|integer',
      planting_type: 'required|integer',
      density: 'required|number',
      survival:  'required|number',
      size_grs:  'required|number',
      size_planting: 'required|number',
      cent_gr:  'number',
      size_thinning:  'number',
      pounds_thinning:  'number',
      cost_per_thounsand:  'required|number',
      fc:  'required|number',
      us_kg_balance:  'required|number',
      production_days:  'required|number',
      stopping_days:  'required|number',
      cost_day_ha:  'required|number',
      initial_biomass: 'number'
    }
  }
  pool () {
    return this.belongsTo('App/Models/Pool', 'pool_id', 'id')
  }
  food () {
    return this.belongsTo('App/Models/Product', 'food_id', 'id')
  }
}

module.exports = CostBenefitProjectionDetail
