'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class VisitingPlanningUpdateRequest extends Model {
  visitingPlanning () {
    return this.belongsTo('App/Models/VisitingPlanning', 'visiting_planning_id', 'id')
  }
  visitReason () {
    return this.belongsTo('App/Models/VisitReason', 'event_type_id', 'id')
  }
  pre_visitReason () {
    return this.belongsTo('App/Models/VisitReason', 'pre_event_type_id', 'id')
  }
  customer () {
    return this.hasOne('App/Models/Customer', 'customer_id', 'id')
  }
  pre_customer () {
    return this.hasOne('App/Models/Customer', 'pre_customer_id', 'id')
  }
  shrimp () {
    return this.hasOne('App/Models/Shrimp', 'shrimp_id', 'id')
  }
  pre_shrimp () {
    return this.hasOne('App/Models/Shrimp', 'pre_shrimp_id', 'id')
  }
}

module.exports = VisitingPlanningUpdateRequest
