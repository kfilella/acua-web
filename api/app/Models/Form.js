'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Form extends Model {
  static fieldValidationRules () {
    return {
      name: 'required|string'
    }
  }
}

module.exports = Form
