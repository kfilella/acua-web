'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Permission extends Model {
  static get fillable() {
    return ['name', 'description']
  }
  static fieldValidationRules () {
    return {
      name: 'required|string|unique:permissions,name',
      description: 'required|string'
    }
  }
  static fieldValidationRulesUpdate () {
    return {
      name: 'required|string',
      description: 'required|string'
    }
  }

  static boot () {
    super.boot()

    this.addTrait('@provider:Lucid/SoftDeletes')
  }
}

module.exports = Permission
