'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class PathologiesImg extends Model {
    static get fillable () {
      return ['analisys_id', 'description', 'date', 'dir']
    }
    static fieldValidationRules () {
      return {
        analisys_id: 'required|string',
        description: 'required|string',
        date: 'required|date',
        dir: 'required|string'
      }
    }
    poolDetail () {
      return this.belongsTo('App/Models/PatologiesAnalysisDetail', 'patologies_analysis_detail_id', 'id')
    }
}

module.exports = PathologiesImg
