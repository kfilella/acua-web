'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class MailConfiguration extends Model {
  static get fillable() {
    return ['host', 'port', 'user', 'pass', 'mailgun_domain', 'mailgun_apikey', 'type']
  }
  static fieldValidationRules (type = 0) {
    let rules
    if (type.type == 0) {
      rules = {
        host: 'required|string',
        port: 'required|number',
        user: 'required|string',
        pass: 'required|string'
      }
    } else {
      rules = {
        user: 'required|string',
        mailgun_domain: 'required|string',
        mailgun_apikey: 'required|string',
      }
    }
    return rules
    }
}

module.exports = MailConfiguration
