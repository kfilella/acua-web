'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Laboratory extends Model {
  static get fillable () {
    return ['name']
  }
  static fielValidateRules () {
    return {
      name: 'required|string'
    }
  }
}

module.exports = Laboratory
