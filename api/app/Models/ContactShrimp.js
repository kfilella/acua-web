'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ContactShrimp extends Model {
  contact () {
    return this.hasOne('App/Models/Contact', 'contact_id', 'id')
  }
}

module.exports = ContactShrimp
