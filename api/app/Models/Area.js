'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Area extends Model {
  static get fillable () {
    return ['description']
  }
  static fieldValidationRules () {
    return {
      description: 'required|string'
    }
  }

  departments () {
    return this.hasMany('App/Models/Department', 'id', 'area_id')
  }
}

module.exports = Area
