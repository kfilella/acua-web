'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class FloorAnalysisPool extends Model {
  static get table () {
    return 'floor_analysis_master'
  }
  static get fillable () {
    return [
      'shrimp_id', 'analysis_date', 'technical_advisor_id', 'applicant_id', 'delivery_date', 'sampling_reception_date',
      'observations', 'recommendations'
    ]
  }
  static fieldValidationRules () {
    return {
      shrimp_id: 'required|integer',
      analysis_date: 'required|date',
      technical_advisor_id: 'required|integer',
      applicant_id: 'required|integer',
      delivery_date: 'required|date',
      sampling_reception_date: 'date'
    }
  }
  user () {
    return this.hasOne('App/Models/User', 'technical_advisor_id', 'id')
  }
  zone () {
    return this.hasOne('App/Models/Zone', 'zone_id', 'id')
  }
  pools () {
    return this.hasMany('App/Models/FloorAnalysisDetail', 'id', 'floor_analysis_master_id')
  }
  shrimp () {
    return this.hasOne('App/Models/Shrimp', 'shrimp_id', 'id')
  }
  applicant () {
    return this.hasOne('App/Models/Contact', 'applicant_id', 'id')
  }  
}

module.exports = FloorAnalysisPool
