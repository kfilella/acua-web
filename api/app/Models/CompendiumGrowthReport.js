'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class CompendiumGrowthReport extends Model {
  static get fillable () {
    return ['compendium_id', 'date', 'size', 'mp']
  }
  static fieldValidationRules () {
    return {
      compendium_id: 'required|number',
      date: 'required|date',
      size: 'required|number',
      mp: 'required|number'
    }
  }
}

module.exports = CompendiumGrowthReport
