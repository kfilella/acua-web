'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class CostBenefitProjection extends Model {
  technicalAdvisor () {
    return this.belongsTo('App/Models/TechnicalAdvisor', 'technical_advisor_id', 'id')
  }
  details () {
    return this.hasMany('App/Models/CostBenefitProjectionDetail', 'id', 'cost_benefit_projection_id')
  }
  shrimp () {
    return this.belongsTo('App/Models/Shrimp', 'shrimp_id', 'id')
  }
}

module.exports = CostBenefitProjection
