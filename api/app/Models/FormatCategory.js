'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class FormatCategory extends Model {
  variables () {
    return this.hasMany('App/Models/FormatCategoryVariable', 'id', 'format_category_id')
  }
}

module.exports = FormatCategory
