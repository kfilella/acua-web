'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Shrimp extends Model {
  static boot () {
    super.boot()
    this.addTrait('@provider:Lucid/SoftDeletes')
  }
  static get table () {
    return 'shrimps'
  }
  static get fillable () {
    return ['customer_id', 'state_id', 'city_id', 'ubication', 'name', 'zone_id', 'agripac_atendets', 'extended_all']
  }
  static fieldValidationRules () {
    return {
      customer_id: 'required|number',
      state_id: 'required|number',
      city_id: 'required|number',
      ubication: 'string',
      name: 'required|string',
      zone_id: 'required|number',
      agripac_atendets: 'required|number',
      extended_all: 'required|number'
    }
  }
  contact () {
    return this.hasMany('App/Models/Contact', 'customer_id', 'customer_id')
  }
  pools () {
    return this.hasMany('App/Models/Pool', 'id', 'shrimp_id')
  }
  customer () {
    return this.hasOne('App/Models/Customer', 'customer_id', 'id')
  }
  assign () {
    return this.hasMany('App/Models/AssignShrimp', 'id', 'shrimp_id')
  }
  zone () {
    return this.hasOne('App/Models/Zone', 'zone_id', 'id')
  }
  appointments () {
    return this.hasMany('App/Models/VisitingPlanning', 'id', 'shrimp_id')
  }
  assignedContacts () {
    return this.hasMany('App/Models/ContactShrimp', 'id', 'shrimp_id')
  }
  state () {
    return this.belongsTo('App/Models/State', 'state_id', 'id')
  }
}

module.exports = Shrimp
