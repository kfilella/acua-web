'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class PhytoplanktonAnalysisDetail extends Model {
  static boot () {
    super.boot()
    this.addTrait('@provider:Lucid/SoftDeletes')
  }
  static get table () {
    return 'phytoplankton_analysis_details'
  }
  pool () {
    return this.hasOne('App/Models/Pool', 'pool_id', 'id')
  }
}

module.exports = PhytoplanktonAnalysisDetail
