'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class WaterAnalysisPool extends Model {
  static get table () {
    return 'water_analysis_master'
  }
  static get fillable () {
    return [
      'shrimp_id', 'analysis_date', 'technical_advisor_id', 'applicant_id', 'delivery_date', 'observations', 'recommendations'
    ]
  }
  static fieldValidationRules () {
    return {
      shrimp_id: 'required|integer',
      analysis_date: 'required|date',
      technical_advisor_id: 'required|integer',
      applicant_id: 'required|integer',
      delivery_date: 'required|date'
    }
  }
  user () {
    return this.hasOne('App/Models/User', 'technical_advisor_id', 'id')
  }
  zone () {
    return this.hasOne('App/Models/Zone', 'zone_id', 'id')
  }
  pools () {
    return this.hasMany('App/Models/WaterAnalysisDetail', 'id', 'water_analysis_master_id')
  }
  shrimp () {
    return this.hasOne('App/Models/Shrimp', 'shrimp_id', 'id')
  }
  applicant () {
    return this.hasOne('App/Models/Contact', 'applicant_id', 'id')
  } 
}

module.exports = WaterAnalysisPool
