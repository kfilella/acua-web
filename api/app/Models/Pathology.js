'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Pathology extends Model {
  static boot () {
    super.boot()
    this.addTrait('@provider:Lucid/SoftDeletes')
  }
  static get fillable () {
    return ['pool_id', 'pools', 'gills', 'hepatopancreas', 'intestine', 'tubules', 'observation']
  }
  static fieldValidationRules () {
    return {
      pool_id: 'required|number',
      pools: 'required|string',
      gills: 'required|string',
      hepatopancreas: 'required|string',
      intestine: 'required|string',
      tubules: 'required|number',
      observation: 'string'
    }
  }
  pool_shrimp () {
    return this.hasOne('App/Models/Pool', 'pool_id', 'id')
  }
}

module.exports = Pathology
