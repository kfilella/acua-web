'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class FoodProjectionMaster extends Model {
  static boot () {
    super.boot()
    this.addTrait('@provider:Lucid/SoftDeletes')
  }
  static get table () {
    return 'food_projection_master'
  }
  static get fillable () {
    return [
      'pool_id', 'seedtime', 'cultivation_days', 'density', 'initial_biomass', 'stock_weight', 'harvest_target_weight',
      'survival', 'survival_after30days', 'growth_rate', 'feeding_rate'
    ]
  }
  static fieldValidationRules () {
    return {
      pool_id: 'required|number',
      seedtime: 'required|date',
      cultivation_days: 'required|number',
      density: 'required|number',
      initial_biomass: 'required|boolean',
      stock_weight: 'required|number',
      harvest_target_weight: 'required|number',
      survival: 'required|number',
      survival_after30days: 'number'
    }
  }

  details () {
    return this.hasMany('App/Models/FoodProjectionDetail', 'id', 'food_projection_master_id')
  }
  pool () {
    return this.belongsTo('App/Models/Pool', 'pool_id', 'id')
  }
  foodTypes () {
    return this.hasMany('App/Models/FoodProjectionFoodType', 'id', 'food_projection_id')
  }
  technicalAdvisor () {
    return this.belongsTo('App/Models/TechnicalAdvisor', 'technical_advisor_id', 'id')
  }
  technicalChief () {
    return this.belongsTo('App/Models/TechnicalChief', 'technical_chief_id', 'id')
  }
}

module.exports = FoodProjectionMaster
