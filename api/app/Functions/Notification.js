'use strict'

const User = use('App/Models/User')
const NotificationModel = use('App/Models/Notification')
class Notification {
  static async send ({ to, event, params }) {
    try {
      const eventUserData = (await User.query().with('events', (builder) => {
        builder.where('event_id', event)
      }).where('id', to).first()).toJSON()

      var event = eventUserData.events[0]
      if (!event.bell) { // Si el usuario desactivó el envío de notificaciones para este evento, no se hace nada
        return
      }

      await NotificationModel.create({
        user_id: to,
        title: event.subject,
        message: event.content,
        params: params ? params : undefined
      })

      return true
    } catch (error) {
      // console.log('Algo chimbo pasó con el envío de notificaciones', error)
      return false
    }
  }
}

module.exports = Notification
