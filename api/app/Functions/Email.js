/**
 * Los envíos con eventos son para usuarios del sistema, si se desea enviar un correo a alguien que no sea usuario dee hacerse sin evento
 */

'use strict'
const nodemailer = require("nodemailer")
const mg = require("nodemailer-mailgun-transport")
const MailConfiguration = use('App/Models/MailConfiguration')
const User = use('App/Models/User')
class Email {
  static async send ({to, subject, content, cc, bcc, attachments, event}) {
    try {
      if (event) { // Si el envío de correo está asociado a un evento
        const eventUserData = (await User.query().with('events', (builder) => {
          builder.where('event_id', event)
        }).where('email', to).first()).toJSON()

        var event = eventUserData.events[0]
        if (!event.mail) { // Si el usuario desactivó el envío de correos para este evento, no se hace nada
          return
        }
      }
      const mailConfiguration = (await MailConfiguration.query().first()).toJSON()
      console.log('mailConfiguration', mailConfiguration)
      let transporter

      if (mailConfiguration.type == 1) { // Mailgun
        const mailgunAuth = {
          auth: {
            api_key: mailConfiguration.mailgun_apikey,
            domain: mailConfiguration.mailgun_domain
          }
        }
        transporter = nodemailer.createTransport(mg(mailgunAuth))
      } else { // SMTP
        // create reusable transporter object using the default SMTP transport
        transporter = nodemailer.createTransport({
          host: mailConfiguration.host,
          port: mailConfiguration.port,
          secure: mailConfiguration.secure, // true for 465, false for other ports
          auth: {
            user: mailConfiguration.user,
            pass: mailConfiguration.pass,
          },
        });
      }

      // send mail with defined transport object
      let info = await transporter.sendMail({
        from: `"${mailConfiguration.from}" <${mailConfiguration.user}>`, // sender address
        to, // list of receivers
        subject: event ? event.subject : subject, // Subject line
        html: event ? event.content : content, // html body,
        cc,
        bcc,
        attachments
      });

      console.log("Message sent: %s", info.messageId);
      return true
    } catch (error) {
      // console.log('Algo chimbo pasó', error)
      return false
    }

  }
}

module.exports = Email
