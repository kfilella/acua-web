class FoodProjectionFunctions {
  /**
   * Función que devuelve el peso de siembra de una proyección
   *
   *
   * @param harvest_target_weight Peso objetivo
   * @param cultivation_days Días de cultivo
   * @param day Día a buscar dentro de los pesos vivos calculados
   */
  static plantingWeigth (harvest_target_weight, cultivation_days, day, returnZero = undefined) {
    // tanteo de tasa de crecimiento para saber con cual tasa el último día le llego cerca al peso objetivo
    let testLiveweight = 0
    let testGrowthRate = 0 // Tasa de crecimiento tanteada
    while (testLiveweight < harvest_target_weight) {
      testGrowthRate++
      testLiveweight = (((0.0231 * Math.pow(cultivation_days, 1.3758)) * (testGrowthRate / 100)))
    }
    const finalWeightPre = (((0.0231 * Math.pow(cultivation_days, 1.3758)) * ((testGrowthRate - 1) / 100)))
    const finalWeightPost = (((0.0231 * Math.pow(cultivation_days, 1.3758)) * ((testGrowthRate) / 100)))

    const diffFinalWeightPre = harvest_target_weight - finalWeightPre
    const diffFinalWeightPost = finalWeightPost - harvest_target_weight

    if (diffFinalWeightPre < diffFinalWeightPost) {
      testGrowthRate-- // Si el peso de abajo es más cercano entonces usar la tasa obtenida menos 1
    }
    const liveweights = []
    for (let i = 1; i <= cultivation_days; i++) {
      // Hasta el día 12 se usa la fórmula lógica tomando datos de la misma fila
      const liveweight = ((0.0231 * Math.pow(i, 1.3758)) * (testGrowthRate / 100)).toFixed(3)
      liveweights.push({ label: liveweight, value: i })
    }
    const elseMessage = returnZero ? 0 : 'Sin biomasa inicial'
    const found = liveweights.find(val => val.value == day) ? liveweights.find(val => val.value == day).label : elseMessage
    return found
  }

  /**
   * Función que devuelve el FC de una proyección según los datos del último día
   *
   *
   * @param foodProjectioDetails Array de objetos con los días de la proyección
   * @param density Densidad de la siembra
   * @param poolArea Área de la piscina
   * @param cultivationDays Cantidad de días de la proyección
   */
  static calculateFC (foodProjectioDetails, density, poolArea, cultivationDays, harvest_target_weight) {
    let fc, kilosFood, biomass, poundsHarvest = 0
    let total_kg = 0
    let animalNumbers = density * poolArea

    foodProjectioDetails.forEach(element => {
      
      element.food_day = parseFloat(element.food_day)
      total_kg += element.food_day
      //console.log('element.food_day', element.food_day)
      biomass = Math.round((element.liveweight / 1000) * (element.survival / 100) * animalNumbers)
      
        if (element.day == cultivationDays) {
          console.log('total_kg', total_kg)
          fc = (total_kg / biomass).toFixed(2)
          kilosFood = total_kg.toFixed()
          // poundsHarvest = (biomass * 2.205).toFixed()
          //poundsHarvest = animalNumbers * (element.survival / 100) / 454
          // densidad * area * survival * peso cosecha / 454
          poundsHarvest = animalNumbers * (element.survival / 100) * harvest_target_weight / 454
        }
      })
 
      return {fc, kilosFood, poundsHarvest}
  }
}

module.exports = FoodProjectionFunctions