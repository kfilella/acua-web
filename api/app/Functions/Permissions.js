/**
 * Función que retorna los permisos disponibles en el sistema. Cualquier permiso nuevo o
 * nodificación de permiso existente debe realizarse aquí.
 * Cada permiso tiene 3 propiedades obligatorias(track, name, description) y 1 opcional(children),
 * track: es la ruta del permiso. El permiso 1.1.1 hace referencia a que es hijo del 1.1 que a su
 * vez es hijo del 1.
 * name: nombre del permiso que el usuario verá. Debe ser descriptivo para que se entienda lo que
 * puede hacer si asigna ese permiso.
 * description: un resumen más detallado del permiso.
 * children: array con permisos que descienden de un permiso
 *
 */
exports.getPermissions =  () => {
  return [
    {
      track: '1',
      name: 'Configuración',
      description: 'Sección ítem del menú',
      icon: 'settings',
      children: [
        {
          track: '1.1',
          name: 'Usuarios',
          description: 'Módulo de Usuarios',
          icon: 'person',
          children: [
            {
              track: '1.1.1',
              name: 'Lista de usuarios',
              description: 'Visualizar todos los usuarios'
            },
            {
              track: '1.1.2',
              name: 'Nuevo Usuario',
              description: 'Agregar nuevo usuario'
            },
            {
              track: '1.1.3',
              name: 'Editar Usuario',
              description: 'Editar usuario existente'
            },
            {
              track: '1.1.4',
              name: 'Eliminar Usuario',
              description: 'Eliminar usuario existente'
            },
            {
              track: '1.1.5',
              name: 'Consultar Usuario',
              description: 'Mostrar los datos de usuario específico'
            }
          ]
        },
        {
          track: '1.2',
          name: 'Roles y Permisos',
          description: 'Módulo de Roles y Permisos',
          icon: 'security',
          children: [
            {
              track: '1.2.1',
              name: 'Lista de roles',
              description: 'Visualizar todos los roles'
            },
            {
              track: '1.2.2',
              name: 'Nuevo Rol',
              description: 'Agregar nuevo rol'
            },
            {
              track: '1.2.3',
              name: 'Consultar Rol',
              description: 'Mostrar los datos de rol específico'
            },
            {
              track: '1.2.4',
              name: 'Editar Rol',
              description: 'Editar rol existente'
            },
            {
              track: '1.2.5',
              name: 'Eliminar Rol',
              description: 'Eliminar rol existente'
            }

          ]
        },
        {
          track: '1.3',
          name: 'Correo y Notificaciones',
          description: 'Configuración de parámetros de correo y notificaciones',
          icon: 'mail',
          children: [
            {
              track: '1.3.1',
              name: 'Servicio de correo',
              description: 'Configuración del servicio usado para enviar correos',
              icon: 'mail',
              children: [
                {
                  track: '1.3.1.1',
                  name: 'Actualizar servicio de correo',
                  description: 'Modificar los campos del servicio de correo',
                  icon: 'mail'
                }
              ]
            },
            {
              track: '1.3.2',
              name: 'Eventos',
              description: 'Configuración de distintos eventos que ocurren en el sistema y si se debe enviar notificación o no',
              icon: 'log',
              children: [
                {
                  track: '1.3.2.1',
                  name: 'Lista de eventos',
                  description: 'Visualizar el listado de eventos disponibles en el sistema',
                  icon: 'mail'
                },
                {
                  track: '1.3.2.2',
                  name: 'Actualizar estatus',
                  description: 'Actualizar el estatus de las notificaciones de campana y correo electrónico',
                  icon: 'mail'
                },
                {
                  track: '1.3.2.3',
                  name: 'Ver evento',
                  description: 'Visualizar los detalles del evento',
                  icon: 'mail'
                },
                {
                  track: '1.3.2.4',
                  name: 'Actualizar evento',
                  description: 'Modificar los detalles del evento',
                  icon: 'mail'
                }
              ]
            }
          ]
        },
        {
          track: '1.4',
          name: 'Segmentos',
          description: 'Módulo de Segmentos',
          icon: 'horizontal_split',
          children: [
            {
              track: '1.4.1',
              name: 'Lista de segmentos',
              description: 'Visualizar todos los segmentos'
            },
            {
              track: '1.4.2',
              name: 'Nuevo segmento',
              description: 'Agregar nuevo segmento'
            },
            {
              track: '1.4.3',
              name: 'Consultar segmento',
              description: 'Mostrar los datos de segmento específico'
            },
            {
              track: '1.4.4',
              name: 'Editar segmento',
              description: 'Editar segmento existente'
            },
            {
              track: '1.4.5',
              name: 'Eliminar segmento',
              description: 'Eliminar segmento existente'
            }
          ]
        },
        {
          track: '1.5',
          name: 'Proveedores.',
          description: 'Módulo de Proveedores',
          icon: 'horizontal_split',
          children: [
            {
              track: '1.5.1',
              name: 'Calificación de proveedores',
              description: 'Módulo de Calificación de proveedores',
              icon: 'horizontal_split',
              children: [
                {
                  track: '1.5.1.1', //1.5
                  name: 'Empresas calificadoras',
                  description: 'Módulo de Empresas calificadoras',
                  icon: 'horizontal_split',
                  children: [
                    {
                      track: '1.5.1.1.1', //1.5.1
                      name: 'Lista de Empresas calificadoras',
                      description: 'Visualizar todos los Empresas calificadoras'
                    },
                    {
                      track: '1.5.1.1.2',//1.5.2
                      name: 'Nueva empresa calificadora',
                      description: 'Agregar nuevo empresa calificadora'
                    },
                    {
                      track: '1.5.1.1.3',//1.5.3
                      name: 'Consultar empresa calificadora',
                      description: 'Mostrar los datos de empresa calificadora específica'
                    },
                    {
                      track: '1.5.1.1.4',//1.5.4
                      name: 'Editar empresa calificadora',
                      description: 'Editar empresa calificadora existente'
                    },
                    {
                      track: '1.5.1.1.5',//1.5.5
                      name: 'Eliminar empresa calificadora',
                      description: 'Eliminar empresa calificadora existente'
                    }
                  ]
                },
                {
                  track: '1.5.1.2',//1.6
                  name: 'Tipos de calificación',
                  description: 'Módulo de Tipos de calificación',
                  icon: 'assignment_turned_in',
                  children: [
                    {
                      track: '1.5.1.2.1', //1.6.1
                      name: 'Lista de Tipos de calificación',
                      description: 'Visualizar todos los Tipos de calificación'
                    },
                    {
                      track: '1.5.1.2.2',//1.6.2
                      name: 'Nueva tipo de calificación',
                      description: 'Agregar nuevo tipo de calificación'
                    },
                    {
                      track: '1.5.1.2.3',//1.6.3
                      name: 'Consultar tipo de calificación',
                      description: 'Mostrar los datos de tipo de calificación específica'
                    },
                    {
                      track: '1.5.1.2.4',//1.6.4
                      name: 'Editar tipo de calificación',
                      description: 'Editar tipo de calificación existente'
                    },
                    {
                      track: '1.5.1.2.5',//1.6.5
                      name: 'Eliminar tipo de calificación',
                      description: 'Eliminar tipo de calificación existente'
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          track: '1.6',
          name: 'Empresas',
          description: 'Sección de configuración de empresas',
          icon: 'person',
          children: [
            {
              track: '1.6.1',
              name: 'Lista de empresas',
              description: 'Visualizar todos los empresas'
            },
            {
              track: '1.6.2',
              name: 'Nueva Empresa',
              description: 'Agregar nuevo Empresa'
            },
            {
              track: '1.6.3',
              name: 'Editar Empresa',
              description: 'Editar Empresa existente'
            },
            {
              track: '1.6.4',
              name: 'Eliminar Empresa',
              description: 'Eliminar Empresa existente'
            },
            {
              track: '1.6.5',
              name: 'Consultar Empresa',
              description: 'Mostrar los datos de Empresa específico'
            }
          ]
        },
        {
          track: '1.7',
          name: 'Restricción de horarios',
          description: 'Sección de restricción de horarios',
          icon: 'person',
          children: [
            {
              track: '1.7.1',
              name: 'Consultar horario',
              description: 'Visualizar los días y su respectivos horarios'
            },
            {
              track: '1.7.2',
              name: 'Modificar horario',
              description: 'Modificar los horarios'
            }
          ]
        },
        {
          track: '1.8',
          name: 'Áreas',
          description: 'Sección de configuración de áreas',
          icon: 'person',
          children: [
            {
              track: '1.8.1',
              name: 'Lista de áreas',
              description: 'Visualizar todos los áreas'
            },
            {
              track: '1.8.2',
              name: 'Nuevo área',
              description: 'Agregar nueva área'
            },
            {
              track: '1.8.3',
              name: 'Editar área',
              description: 'Editar área existente'
            },
            {
              track: '1.8.4',
              name: 'Eliminar área',
              description: 'Eliminar área existente'
            },
            {
              track: '1.8.5',
              name: 'Consultar área',
              description: 'Mostrar los datos de área específica'
            }
          ]
        },
        {
          track: '1.9',
          name: 'Departamentos',
          description: 'Sección de configuración de Departamentos',
          icon: 'person',
          children: [
            {
              track: '1.9.1',
              name: 'Lista de Departamentos',
              description: 'Visualizar todos los Departamentos'
            },
            {
              track: '1.9.2',
              name: 'Nuevo departamento',
              description: 'Agregar nuevo departamento'
            },
            {
              track: '1.9.3',
              name: 'Editar departamento',
              description: 'Editar departamento existente'
            },
            {
              track: '1.9.4',
              name: 'Eliminar departamento',
              description: 'Eliminar departamento existente'
            },
            {
              track: '1.9.5',
              name: 'Consultar departamento',
              description: 'Mostrar los datos de departamento específico'
            }
          ]
        },
        {
          track: '1.10',
          name: 'Secciones',
          description: 'Sección de configuración de secciones',
          icon: 'person',
          children: [
            {
              track: '1.10.1',
              name: 'Lista de secciones',
              description: 'Visualizar todos los secciones'
            },
            {
              track: '1.10.2',
              name: 'Nuevo sección',
              description: 'Agregar nuevo sección'
            },
            {
              track: '1.10.3',
              name: 'Editar sección',
              description: 'Editar sección existente'
            },
            {
              track: '1.10.4',
              name: 'Eliminar sección',
              description: 'Eliminar sección existente'
            },
            {
              track: '1.10.5',
              name: 'Consultar sección',
              description: 'Mostrar los datos de sección específico'
            }
          ]
        },
        {
          track: '1.11',
          name: 'Formatos',
          description: 'Sección de configuración de formatos',
          icon: 'person',
          children: [
            {
              track: '1.11.1',
              name: 'Lista de formatos',
              description: 'Visualizar todos los formatos'
            },
            {
              track: '1.11.2',
              name: 'Nuevo formato',
              description: 'Agregar nuevo formato'
            },
            {
              track: '1.11.3',
              name: 'Editar formato',
              description: 'Editar formato existente'
            },
            {
              track: '1.11.4',
              name: 'Eliminar formato',
              description: 'Eliminar formato existente'
            },
            {
              track: '1.11.5',
              name: 'Consultar formato',
              description: 'Mostrar los datos de formato específico'
            }
          ]
        },
        {
          track: '1.12',
          name: 'Formularios',
          description: 'Sección de configuración de Formularios',
          icon: 'view_carousel',
          children: [
            {
              track: '1.12.1',
              name: 'Lista de Formularios',
              description: 'Visualizar todos los Formularios'
            },
            {
              track: '1.12.2',
              name: 'Nuevo formulario',
              description: 'Agregar nuevo formulario'
            },
            {
              track: '1.12.3',
              name: 'Editar formulario',
              description: 'Editar formulario existente'
            },
            {
              track: '1.12.4',
              name: 'Eliminar formulario',
              description: 'Eliminar formulario existente'
            },
            {
              track: '1.12.5',
              name: 'Consultar formulario',
              description: 'Mostrar los datos de formulario específico'
            }
          ]
        },
        {
          track: '1.13',
          name: 'Otros',
          description: 'Sección de configuración de otras configuraciones',
          icon: 'view_carousel'
        }
      ]
    },
    {
      track: '2',
      name: 'Clientes',
      description: 'Sección de clientes',
      icon: 'person',
      children: [
        {
          track: '2.1',
          name: 'Lista de clientes',
          description: 'Visualizar todos los clientes',
          children: [
            {
              track: '2.1.1', // '2.16'
              name: 'Gestionar Cliente',
              description: 'Mostrar la pantalla de gestión del cliente',
              children: [
                {
                  track: '2.1.1.1',// '2.16.1'
                  name: 'Gestionar Camaroneras',
                  description: 'Mostrar la pantalla de gestión de camaroneras',
                  children: [
                    {
                      track: '2.1.1.1.1',// '2.16.1.1'
                      name: 'Crear Camaronera',
                      description: 'Agregar nueva camaronera'
                    },
                    {
                      track: '2.1.1.1.2',
                      name: 'Editar camaronera',
                      description: 'Editar camaroneras'
                    },
                    {
                      track: '2.1.1.1.3',
                      name: 'Ver camaronera',
                      description: 'Ver detalles de camaronera específica'
                    },
                    {
                      track: '2.1.1.1.4',
                      name: 'Ver piscinas',
                      description: 'Ver piscinas de camaronera específica'
                    },
                    {
                      track: '2.1.1.1.5',
                      name: 'Agregar piscina',
                      description: 'Insertar piscina de camaronera específica'
                    },
                    {
                      track: '2.1.1.1.6',
                      name: 'Ver piscina',
                      description: 'Ver datos de piscina específica de camaronera específica'
                    },
                    {
                      track: '2.1.1.1.7',
                      name: 'Editar piscina',
                      description: 'Editar datos de piscina específica de camaronera específica'
                    },
                    {
                      track: '2.1.1.1.8',
                      name: 'Eliminar piscina',
                      description: 'Eliminar piscina específica de camaronera específica'
                    },
                    {
                      track: '2.1.1.1.9',
                      name: 'Listar Patologia',
                      description: 'Listar Patologia específica de camaronera específica'
                    },
                    {
                      track: '2.1.1.1.10',
                      name: 'Ver Patologia',
                      description: 'Ver Patologia específica de camaronera específica'
                    },
                    {
                      track: '2.1.1.1.11',
                      name: 'Guardar Patologia',
                      description: 'Guardar Patologia específica de camaronera específica'
                    },
                    {
                      track: '2.1.1.1.12',
                      name: 'Editar Patologia',
                      description: 'Editar Patologia específica de camaronera específica'
                    },
                    {
                      track: '2.1.1.1.13',
                      name: 'Eliminar Patologia',
                      description: 'Eliminar Patologia específica de camaronera específica'
                    },
                    {
                      track: '2.1.1.1.14',
                      name: 'Enviar Correo',
                      description: 'Enviar Correo'
                    },
                    {
                      track: '2.1.1.1.15', // antes 2.1.1.1.14
                      name: 'Ver análisis de aguas',
                      description: 'Ver lista de los análisis de aguas realizados en camaronera específica'
                    },
                    {
                      track: '2.1.1.1.16', // antes 2.1.1.1.15
                      name: 'Crear análisis de aguas',
                      description: 'Crear un nuevo análisis de aguas realizados en camaronera específica'
                    },
                    {
                      track: '2.1.1.1.17', // antes 2.1.1.1.16
                      name: 'Enviar análisis de aguas por correo',
                      description: 'Enviar documento de análisis de aguas por correo'
                    },
                    {
                      track: '2.1.1.1.18',
                      name: 'Ver análisis de aguas.',
                      description: 'Ver los detalles de un análisis de aguas específico'
                    },
                    {
                      track: '2.1.1.1.19',
                      name: 'Editar análisis de aguas',
                      description: 'Editar datos de un análisis de aguas específico'
                    },
                    {
                      track: '2.1.1.1.20',
                      name: 'Guardar Fitoplancton',
                      description: 'Guardar Fitoplancton específica de camaronera específica'
                    },
                    {
                      track: '2.1.1.1.21',
                      name: 'Editar Fitoplancton',
                      description: 'Editar Fitoplancton específica de camaronera específica'
                    },
                    {
                      track: '2.1.1.1.22',
                      name: 'Eliminar Fitoplancton',
                      description: 'Eliminar Fitoplancton específica de camaronera específica'
                    },
                    {
                      track: '2.1.1.1.23',
                      name: 'Enviar Correo Fitoplancton',
                      description: 'Enviar Correo'
                    },
                    {
                      track: '2.1.1.1.24',
                      name: 'Ver lista análisis de suelos',
                      description: 'Ver lista de los análisis de suelos realizados en camaronera específica'
                    },
                    {
                      track: '2.1.1.1.25',
                      name: 'Ver análisis específico de suelos',
                      description: 'Ver los detalles de un análisis de suelos específico'
                    },
                    {
                      track: '2.1.1.1.26',
                      name: 'Enviar análisis de suelos por correo',
                      description: 'Enviar documento de análisis de suelos por correo'
                    },
                    {
                      track: '2.1.1.1.27',
                      name: 'Crear análisis de suelos',
                      description: 'Crear un nuevo análisis de suelos realizado en camaronera específica'
                    },
                    {
                      track: '2.1.1.1.28',
                      name: 'Editar análisis de suelos',
                      description: 'Editar datos de un análisis de suelos específico'
                    },
                    {
                      track: '2.1.1.1.29',
                      name: 'Gestionar piscinas',
                      description: 'Módulos de gestión de piscinas',
                      children: [
                        {
                          track: '2.1.1.1.29.1',
                          name: 'Guardar proyección de alimentación',
                          description: 'Permiso para guardar una nueva proyección de alimentación en una piscina'
                        },
                        {
                          track: '2.1.1.1.29.2',
                          name: 'Consultar proyección de alimentación específica',
                          description: 'Permiso para ver los datos de una proyección de alimentación en una piscina'
                        },
                        {
                          track: '2.1.1.1.29.3',
                          name: 'Listar proyecciones de alimentación',
                          description: 'Permiso para ver la lista de proyecciones de alimentación realizadas a una piscina'
                        },
                        {
                          track: '2.1.1.1.29.4',
                          name: 'Modificar proyección de alimentación',
                          description: 'Permiso para modificar una proyección de alimentación realizada a una piscina'
                        },
                        {
                          track: '2.1.1.1.29.5',
                          name: 'Agregar dato real a proyección de alimentación',
                          description: 'Permiso para agregar datos reales de proyección de alimentación'
                        },
                        {
                          track: '2.1.1.1.29.6',
                          name: 'Descargar formato de carga masiva',
                          description: 'Permiso para descargar el formato que permitirá hacer una carga masiva de datos reales de la proyección'
                        },
                        {
                          track: '2.1.1.1.29.7',
                          name: 'Listar proyecciones de pesca',
                          description: 'Permiso para ver la lista de proyecciones de pesca realizadas a una piscina'
                        },
                        {
                          track: '2.1.1.1.29.8',
                          name: 'Guardar proyección de pesca',
                          description: 'Permiso para guardar una proyección de pesca realizada a una piscina'
                        },
                        {
                          track: '2.1.1.1.29.9',
                          name: 'Ver proyección de pesca',
                          description: 'Permiso para Ver una proyección de pesca realizada a una piscina'
                        },
                        {
                          track: '2.1.1.1.29.10',
                          name: 'Añadir reporte de crecimiento',
                          description: 'Permiso añadir un reporte de crecimiento a una proyección de pesca'
                        },
                        {
                          track: '2.1.1.1.29.11',
                          name: 'Editar reporte de crecimiento',
                          description: 'Permiso Editar un reporte de crecimiento a una proyección de pesca'
                        },
                        {
                          track: '2.1.1.1.29.12',
                          name: 'Añadir proyección de camarón',
                          description: 'Permiso para añadir una proyección de camarón a una proyección de pesca'
                        },
                        {
                          track: '2.1.1.1.29.13',
                          name: 'Editar proyección de camarón',
                          description: 'Permiso para Editar una proyección de camarón a una proyección de pesca'
                        },
                        {
                          track: '2.1.1.1.29.14',
                          name: 'Añadir proyección de costos',
                          description: 'Permiso para añadir una proyección de costos a una proyección de pesca'
                        },
                        {
                          track: '2.1.1.1.29.15',
                          name: 'Editar proyección de costos',
                          description: 'Permiso para Editar una proyección de camarón a una proyección de pesca'
                        },
                        {
                          track: '2.1.1.1.29.16',
                          name: 'Editar proyección de pesca',
                          description: 'Permiso para editar una proyección de pesca realizada a una piscina'
                        },
                        {
                          track: '2.1.1.1.29.17',
                          name: 'Modificar tipos de alimentos',
                          description: 'Permiso para Modificar valores tipos de alimentos relacionados con una proyección de alimentación'
                        },
                        {
                          track: '2.1.1.1.29.18',
                          name: 'Crear Pdf de Proyección de Alimentación',
                          description: 'Permiso para ver poder generar el reporte tras una Proyección de alimentación'
                        }
                      ]
                    },
                    {
                      track: '2.1.1.1.30',
                      name: 'Imprimir análisis de aguas',
                      description: 'Permiso para imprimir el reporte de un análisis de aguas específico.'
                    },
                    {
                      track: '2.1.1.1.31',
                      name: 'Imprimir análisis de suelos',
                      description: 'Permiso para imprimir el reporte de un análisis de suelos específico.'
                    },
                    {
                      track: '2.1.1.1.32',
                      name: 'Imprimir análisis de patologías',
                      description: 'Permiso para imprimir el reporte de un análisis de patologías específico.'
                    },
                    {
                      track: '2.1.1.1.33',
                      name: 'Imprimir análisis fitoplancton',
                      description: 'Permiso para imprimir el reporte de un análisis fitoplancton específico.'
                    },
                    {
                      track: '2.1.1.1.34',
                      name: 'Ver análisis fitoplancton',
                      description: 'Permiso para ver detalles de un análisis fitoplancton específico.'
                    },
                    {
                      track: '2.1.1.1.35',
                      name: 'Ver listado de reportes de crecimiento',
                      description: 'Permiso para ver la lista de reportes de crecimiento dentro una camaronera específica'
                    },
                    {
                      track: '2.1.1.1.36',
                      name: 'Añadir nuevo reporte de crecimiento',
                      description: 'Permiso para añadir un nuevo reporte de crecimiento dentro una camaronera específica'
                    },
                    {
                      track: '2.1.1.1.37',
                      name: 'Ver reporte de crecimiento',
                      description: 'Permiso para ver un reporte de crecimiento dentro una camaronera específica'
                    },
                    {
                      track: '2.1.1.1.38',
                      name: 'Editar reporte de crecimiento de camaronera',
                      description: 'Permiso para editar reporte de crecimiento dentro una camaronera específica'
                    },
                    {
                      track: '2.1.1.1.39',
                      name: 'Ver listado de proyecciones costo/beneficio',
                      description: 'Permiso para ver las las proyecciones de costo-beneficio dentro de una camaronera'
                    },
                    {
                      track: '2.1.1.1.40',
                      name: 'Agregar nueva de proyección costo/beneficio',
                      description: 'Permiso para agregar una nueva proyección de costo-beneficio dentro de una camaronera'
                    },
                    {
                      track: '2.1.1.1.41',
                      name: 'Ver una proyección costo/beneficio',
                      description: 'Permiso para ver detalles de una proyección de costo-beneficio en específico dentro de una camaronera'
                    },
                    {
                      track: '2.1.1.1.42',
                      name: 'Editar una proyección costo/beneficio',
                      description: 'Permiso para editar una proyección de costo-beneficio en específico dentro de una camaronera'
                    },
                    {
                      track: '2.1.1.1.43',
                      name: 'Ver listado de reportes de cosechas',
                      description: 'Permiso para ver los reportes de cosechas dentro de una camaronera'
                    },
                    {
                      track: '2.1.1.1.44',
                      name: 'Guardar nuevo reporte de cosecha',
                      description: 'Permiso para guardar un reporte de cosecha dentro de una camaronera'
                    },
                    {
                      track: '2.1.1.1.45',
                      name: 'Ver reporte de cosecha específico',
                      description: 'Permiso ver un reporte de cosecha dentro de una camaronera'
                    },
                    {
                      track: '2.1.1.1.46',
                      name: 'Editar reporte de cosecha específico',
                      description: 'Permiso editar un reporte de cosecha dentro de una camaronera'
                    },
                    {
                      track: '2.1.1.1.47',
                      name: 'Imprimir reporte de crecimiento de piscinas',
                      description: 'Permiso para imprimir el reporte de crecimiento de piscinas'
                    },
                    {
                      track: '2.1.1.1.48',
                      name: 'Enviar reporte de crecimiento de piscinas por correo',
                      description: 'Permiso para enviar reporte de crecimiento de piscinas por correo'
                    },
                    {
                      track: '2.1.1.1.49',
                      name: 'Imprimir reporte de proyección costo-beneficio',
                      description: 'Permiso para imprimir el reporte de proyección costo-beneficio'
                    },
                    {
                      track: '2.1.1.1.50',
                      name: 'Enviar reporte de proyección costo-beneficio por correo',
                      description: 'Permiso para enviar reporte de proyección costo-beneficio por correo'
                    },
                    {
                      track: '2.1.1.1.51',
                      name: 'Imprimir reporte de reporte de cosecha',
                      description: 'Permiso para imprimir el reporte de reporte de cosecha'
                    },
                    {
                      track: '2.1.1.1.52',
                      name: 'Enviar reporte de reporte de cosecha por correo',
                      description: 'Permiso para enviar reporte de reporte de cosecha por correo'
                    },
                    {
                      track: '2.1.1.1.53',
                      name: 'Eliminar piscina de reporte de crecimiento de camarón',
                      description: 'Permiso para eliminar piscina de reporte de crecimiento de camarón'
                    },
                    {
                      track: '2.1.1.1.54',
                      name: 'Ver listado de compendio',
                      description: 'Permiso para ver la lista de compendio dentro una camaronera específica'
                    },
                    {
                      track: '2.1.1.1.55',
                      name: 'Añadir nuevo compendio',
                      description: 'Permiso para añadir un nuevo compendio dentro una camaronera específica'
                    },
                    {
                      track: '2.1.1.1.56',
                      name: 'Ver compendio',
                      description: 'Permiso para ver un compendio dentro una camaronera específica'
                    },
                    {
                      track: '2.1.1.1.57',
                      name: 'Editar compendio de camaronera',
                      description: 'Permiso para editar compendio dentro una camaronera específica'
                    },
                    {
                      track: '2.1.1.1.58',
                      name: 'Añadir reporte de crecimiento a compendio de camaronera',
                      description: 'Permiso para añadir reporte de crecimiento a compendio dentro una camaronera específica'
                    },
                    {
                      track: '2.1.1.1.59',
                      name: 'Editar reporte de crecimiento a compendio de camaronera',
                      description: 'Permiso para editar reporte de crecimiento a compendio dentro una camaronera específica'
                    },
                    {
                      track: '2.1.1.1.60',
                      name: 'Imprimir reporte de compendio de camaronera',
                      description: 'Permiso para editar reporte de compendio dentro una camaronera específica'
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          track: '2.2',
          name: 'Nuevo Cliente',
          description: 'Agregar nuevo Cliente'
        },
        {
          track: '2.3',
          name: 'Editar Cliente',
          description: 'Editar Cliente existente'
        },
        {
          track: '2.4',
          name: 'Eliminar Cliente',
          description: 'Eliminar Cliente existente'
        },
        {
          track: '2.5',
          name: 'Consultar Cliente',
          description: 'Mostrar los datos de Cliente específico'
        },
        {
          track: '2.6',
          name: 'Lista de categorías',
          description: 'Visualizar todos los categorías relacionadas con clientes'
        },
        {
          track: '2.7',
          name: 'Nueva Categoría',
          description: 'Agregar nueva Categoría'
        },
        {
          track: '2.8',
          name: 'Editar Categoría',
          description: 'Editar Categoría existente'
        },
        {
          track: '2.9',
          name: 'Eliminar Categoría',
          description: 'Eliminar Categoría existente'
        },
        {
          track: '2.10',
          name: 'Consultar Categoría',
          description: 'Mostrar los datos de Categoría específica'
        },
        {
          track: '2.11',
          name: 'Lista de segmentos de clientes',
          description: 'Visualizar todos los segmentos relacionadas con clientes'
        },
        {
          track: '2.12',
          name: 'Nuevo segmento de clientes',
          description: 'Agregar nueva segmento'
        },
        {
          track: '2.13',
          name: 'Editar segmento de clientes',
          description: 'Editar segmento existente'
        },
        {
          track: '2.14',
          name: 'Eliminar segmento de clientes',
          description: 'Eliminar segmento existente'
        },
        {
          track: '2.15',
          name: 'Consultar segmento de clientes',
          description: 'Mostrar los datos de segmento específico'
        },
        {
          track: '2.16',
          name: 'Planificación de visitas',
          description: 'Módulo para planificar y visualizar las visitas de un técnico'
        },
        {
          track: '2.17',
          name: 'Guardar nueva planificación de visitas',
          description: 'Módulo para crear las visitas de un técnico'
        },
        {
          track: '2.18',
          name: 'Modificar planificación de visitas',
          description: 'Módulo para modificar la planificación de visitas de un técnico'
        },
        {
          track: '2.19',
          name: 'Eliminar planificación de visitas',
          description: 'Módulo para eliminar la planificación de visitas de un técnico'
        },
        {
          track: '2.20',
          name: 'Aprobar planificación de visitas',
          description: 'Permiso para Aprobar la planificación de visitas de un técnico'
        },
        {
          track: '2.21',
          name: 'Rechazar planificación de visitas',
          description: 'Permiso para Rechazar la planificación de visitas de un técnico'
        },
        {
          track: '2.22',
          name: 'Imprimir planificación de visitas',
          description: 'Permiso para imprimir en excel la planificación de visitas de un técnico'
        },
        {
          track: '2.23',
          name: 'Módulo tipos de eventos',
          description: 'Módulo para añadir conceptos de visitas de técnicos a camaroneras',
          children: [
            {
              track: '2.23.1',
              name: 'Mostrar listado de tipos de eventos',
              description: 'Permiso para visualizar la lista de tipos de eventos registrados en el sistema'
            },
            {
              track: '2.23.2',
              name: 'Guardar nuevo tipo de evento',
              description: 'Permiso para guardar un nuevo tipo de evento en el sistema'
            },
            {
              track: '2.23.3',
              name: 'Consultar tipo de evento específico',
              description: 'Permiso para visualizar los datos de un tipo de evento específico'
            },
            {
              track: '2.23.4',
              name: 'Modificar tipo de evento específico',
              description: 'Permiso para actualizar los datos de un tipo de evento específico'
            },
            {
              track: '2.23.5',
              name: 'Eliminar tipo de evento específico',
              description: 'Permiso para eliminar un tipo de evento específico'
            }
          ]
        },
        {
          track: '2.24',
          name: 'Solicitar edición de visitas',
          description: 'Permiso para solicitar el poder editar una visita ya aprobada o rechazada'
        },
        {
          track: '2.25',
          name: 'Solicitar eliminación de visitas',
          description: 'Permiso para solicitar el poder eliminar una visita ya aprobada o rechazada'
        },
        {
          track: '2.26',
          name: 'Aprobar edición de visitas',
          description: 'Permiso para Aprobar el poder editar una visita ya aprobada o rechazada'
        },
        {
          track: '2.27',
          name: 'Aprobar eliminación de visitas',
          description: 'Permiso para Aprobar el poder eliminar una visita ya aprobada o rechazada'
        },
        {
          track: '2.28',
          name: 'Registro de ubicación de evento',
          description: 'Permiso para que el asesor técnico pueda registrar si asistió a una camaronera guardando la ubicación'
        },
        {
          track: '2.29',
          name: 'Reporte de planificaciones de eventos',
          description: 'Permiso para poder visualizar un listado de las planificaciones y poder visualizar si se cumplieron o no'
        },
        {
          track: '2.30',
          name: 'Reporte general de cosechas',
          description: 'Permiso para poder visualizar un reporte de todos los reportes de cosechas generados según los filtros añadidos'
        }
      ]
    },
    {
      track: '3',
      name: 'Proveedores',
      description: 'Sección de proveedores',
      icon: 'person',
      children: [
        {
          track: '3.1',
          name: 'Lista de proveedores',
          description: 'Visualizar todos los proveedores'
        },
        {
          track: '3.2',
          name: 'Nuevo proveedor',
          description: 'Agregar nuevo proveedor'
        },
        {
          track: '3.3',
          name: 'Editar proveedor',
          description: 'Editar proveedor existente'
        },
        {
          track: '3.4',
          name: 'Eliminar proveedor',
          description: 'Eliminar proveedor existente'
        },
        {
          track: '3.5',
          name: 'Consultar proveedor',
          description: 'Mostrar los datos de proveedor específico'
        },
        {
          track: '3.6',
          name: 'Lista de categorías de proveedores',
          description: 'Visualizar todos los categorías relacionadas con proveedores'
        },
        {
          track: '3.7',
          name: 'Nueva Categoría de proveedor',
          description: 'Agregar nueva Categoría'
        },
        {
          track: '3.8',
          name: 'Editar Categoría de proveedor',
          description: 'Editar Categoría existente'
        },
        {
          track: '3.9',
          name: 'Eliminar Categoría de proveedor',
          description: 'Eliminar Categoría existente'
        },
        {
          track: '3.10',
          name: 'Consultar Categoría de proveedor',
          description: 'Mostrar los datos de Categoría específica'
        },
        {
          track: '3.11',
          name: 'Lista de segmentos de proveedores',
          description: 'Visualizar todos los segmentos relacionadas con proveedores'
        },
        {
          track: '3.12',
          name: 'Nuevo segmento de proveedores',
          description: 'Agregar nueva segmento'
        },
        {
          track: '3.13',
          name: 'Editar segmento de proveedores',
          description: 'Editar segmento existente'
        },
        {
          track: '3.14',
          name: 'Eliminar segmento de proveedores',
          description: 'Eliminar segmento existente'
        },
        {
          track: '3.15',
          name: 'Consultar segmento de proveedores',
          description: 'Mostrar los datos de segmento específico'
        }
      ]
    },
    {
      track: '4',
      name: 'Productos',
      description: 'Sección de productos',
      icon: 'fastfood',
      children: [
        {
          track: '4.1',
          name: 'Lista de productos',
          description: 'Visualizar todos los productos'
        },
        {
          track: '4.2',
          name: 'Nuevo producto',
          description: 'Agregar nuevo producto'
        },
        {
          track: '4.3',
          name: 'Editar producto',
          description: 'Editar producto existente'
        },
        {
          track: '4.4',
          name: 'Eliminar producto',
          description: 'Eliminar producto existente'
        },
        {
          track: '4.5',
          name: 'Consultar producto',
          description: 'Mostrar los datos del producto específico'
        },
        {
          track: '4.6',
          name: 'Lista de categorías de productos',
          description: 'Visualizar todas las categorías relacionadas con productos'
        },
        {
          track: '4.7',
          name: 'Nueva Categoría de producto',
          description: 'Agregar nueva Categoría'
        },
        {
          track: '4.8',
          name: 'Editar Categoría de producto',
          description: 'Editar Categoría existente'
        },
        {
          track: '4.9',
          name: 'Eliminar Categoría de producto',
          description: 'Eliminar Categoría existente'
        },
        {
          track: '4.10',
          name: 'Consultar Categoría de producto',
          description: 'Mostrar los datos de Categoría específica'
        },
        {
          track: '4.11',
          name: 'Lista de Unidades de medida',
          description: 'Visualizar todas las unidades de medida'
        },
        {
          track: '4.12',
          name: 'Nueva Unidad de medida',
          description: 'Agregar nueva Unidad de medida'
        },
        {
          track: '4.13',
          name: 'Editar Unidad de medida',
          description: 'Editar Unidad de medida existente'
        },
        {
          track: '4.14',
          name: 'Eliminar Unidad de medida',
          description: 'Eliminar Unidad de medida existente'
        },
        {
          track: '4.15',
          name: 'Consultar Unidad de medida',
          description: 'Mostrar los datos de la Unidad de medida específica'
        },
        {
          track: '4.16',
          name: 'Lista de Colores',
          description: 'Visualizar todos los colores'
        },
        {
          track: '4.17',
          name: 'Precio del camarón',
          description: 'Permiso para mostrar el módulo precios del camarón'
        },
        {
          track: '4.18',
          name: 'Guardar Precio del camarón',
          description: 'Permiso para guardar precio de camarón'
        },
        {
          track: '4.19',
          name: 'Editar Precio del camarón',
          description: 'Permiso para editar precio de camarón'
        },
        {
          track: '4.20',
          name: 'Consultar Precio del camarón',
          description: 'Permiso para consultar precio de camarón'
        },/*,
        {
          track: '4.21',
          name: 'Alimentos',
          description: 'Permiso para mostrar el modulo de Alimentos'
        },
        {
          track: '4.22',
          name: 'Guardar Alimentos',
          description: 'Permiso para guardar alimentos'
        },
        {
          track: '4.23',
          name: 'Editar Alimentos',
          description: 'Permiso para editar alimentos'
        },
        {
          track: '4.24',
          name: 'Eliminar Alimentos',
          description: 'Permiso para eeliminar alimentos'
        },
        {
          track: '4.25',
          name: 'Consultar Alimentos',
          description: 'Permiso para consultar Alimentos'
        },*/
        {
          track: '4.26',
          name: 'Laboratorios',
          description: 'Permiso para mostrar el modulo de Laboratorio'
        },
        {
          track: '4.27',
          name: 'Guardar Laboratorio',
          description: 'Permiso para guardar Laboratorio'
        },
        {
          track: '4.28',
          name: 'Editar Laboratorio',
          description: 'Permiso para editar Laboratorio'
        },
        {
          track: '4.29',
          name: 'Eliminar Laboratorio',
          description: 'Permiso para eeliminar Laboratorio'
        },
        {
          track: '4.30',
          name: 'Consultar Laboratorio',
          description: 'Permiso para consultar Laboratorio'
        }
      ]
    },
    {
      track: '5',
      name: 'Equipo Tecnico',
      description: 'Sección ítem del menú',
      icon: 'settings',
      children: [
        {
          track: '5.1',
          name: 'Zona',
          description: 'Módulo de Zonas',
          icon: 'person',
          children: [
            {
              track: '5.1.1',
              name: 'Lista de zonas',
              description: 'Visualizar todos los zonas'
            },
            {
              track: '5.1.2',
              name: 'Nuevo zona',
              description: 'Agregar nuevo zona'
            },
            {
              track: '5.1.3',
              name: 'Editar zona',
              description: 'Editar zona existente'
            },
            {
              track: '5.1.4',
              name: 'Eliminar zona',
              description: 'Eliminar zona existente'
            },
            {
              track: '5.1.5',
              name: 'Consultar zona',
              description: 'Mostrar los datos de zonas específico'
            },
            {
              track: '5.1.6',
              name: 'Activar o Desactivar',
              description: 'Activar o Desactivar Zona'
            }
          ]
        },
        {
          track: '5.2',
          name: 'Jefes Tecnicos',
          description: 'Módulo de Jefes Tecnicos',
          icon: 'person',
          children: [
            {
              track: '5.2.1',
              name: 'Lista de Jefe Tecnicos',
              description: 'Visualizar todos los Jefe Tecnicos'
            },
            {
              track: '5.2.2',
              name: 'Nuevo Jefe Tecnico',
              description: 'Agregar nuevo Jefe Tecnico'
            },
            {
              track: '5.2.3',
              name: 'Editar Jefe Tecnico',
              description: 'Editar Jefe Tecnico existente'
            },
            {
              track: '5.2.4',
              name: 'Eliminar Jefe Tecnico',
              description: 'Eliminar Jefe Tecnico existente'
            },
            {
              track: '5.2.5',
              name: 'Consultar Jefe Tecnico',
              description: 'Mostrar los datos de Jefes Tecnicos específico'
            },
            {
              track: '5.2.6',
              name: 'Activar o Desactivar Jefes Tecnicos especifico',
              description: 'Activar o Desactivar Jefe Tecnico Especifico'
            }
          ]
        },
        {
          track: '5.3',
          name: 'Asesores Tecnicos',
          description: 'Módulo de Asesores Tecnicos',
          icon: 'person',
          children: [
            {
              track: '5.3.1',
              name: 'Lista de Asesores Tecnicos',
              description: 'Visualizar todos los Asesores Tecnicos'
            },
            {
              track: '5.3.2',
              name: 'Nuevo Asesores Tecnico',
              description: 'Agregar nuevo Asesores Tecnico'
            },
            {
              track: '5.3.3',
              name: 'Editar Asesores Tecnico',
              description: 'Editar Asesores Tecnico existente'
            },
            {
              track: '5.3.4',
              name: 'Eliminar Asesores Tecnico',
              description: 'Eliminar Asesores Tecnico existente'
            },
            {
              track: '5.3.5',
              name: 'Consultar Asesores Tecnico',
              description: 'Mostrar los datos de Asesoress Tecnicos específico'
            },
            {
              track: '5.3.6',
              name: 'Activar o Desactivar Asesores Tecnicos especifico',
              description: 'Activar o Desactivar Asesores Tecnico Especifico'
            }
          ]
        },
        {
          track: '5.4',
          name: 'Equipos Tecnicos',
          description: 'Módulo de Equipos Tecnicos',
          icon: 'person',
          children: [
            {
              track: '5.4.1',
              name: 'Lista de Equipos Tecnicos',
              description: 'Visualizar todos los Equipos Tecnicos'
            },
            {
              track: '5.4.2',
              name: 'Nuevo Equipos Tecnico',
              description: 'Agregar nuevo Equipos Tecnico'
            },
            {
              track: '5.4.3',
              name: 'Editar Equipos Técnicos',
              description: 'Editar Equipos Tecnico existente'
            },
            {
              track: '5.4.4',
              name: 'Eliminar Equipos Tecnico',
              description: 'Eliminar Equipos Tecnico existente'
            },
            {
              track: '5.4.5',
              name: 'Consultar Equipos Tecnico',
              description: 'Mostrar los datos de Equiposs Tecnicos específico'
            },
            {
              track: '5.4.6',
              name: 'Activar o Desactivar Equipos Tecnicos especifico',
              description: 'Activar o Desactivar Equipos Tecnico Especifico'
            }
          ]
        },
        {
          track: '5.5',
          name: 'Asignar Camaroneras',
          description: 'Módulo de Asignacion de Camaroneras',
          icon: 'person',
          children: [
            {
              track: '5.5.1',
              name: 'Lista de Camaroneras',
              description: 'Visualizar todos los Camaroneras'
            },
            {
              track: '5.5.2',
              name: 'Nuevo Camaronera',
              description: 'Agregar nuevo Camaronera'
            },
            {
              track: '5.5.3',
              name: 'Editar Camaronera',
              description: 'Editar Camaronera existente'
            },
            {
              track: '5.5.4',
              name: 'Eliminar Camaronera',
              description: 'Eliminar Camaronera existente'
            },
            {
              track: '5.5.5',
              name: 'Consultar Camaronera',
              description: 'Mostrar los datos de Camaronera específico'
            }
          ]
        }
      ]
    },
    {
      track: '6',
      name: 'Perfil',
      description: 'Sección ítem del menú',
      icon: 'person',
      children: [
        {
          track: '6.1',
          name: 'Mis datos',
          description: 'Ítem del menú',
          icon: 'person',
          children: [
            {
              track: '6.1.1',
              name: 'Actualizar datos de perfil de cliente',
              description: 'Permiso que le permite al cliente actualizar sus propios datos',
              icon: 'person'
            }
          ]
        },
        {
          track: '6.2',
          name: 'Asesores asignados',
          description: 'Ítem del menú',
          icon: 'person'
        },
        {
          track: '6.3',
          name: 'Próximos eventos',
          description: 'Ítem del menú',
          icon: 'person'
        },
        {
          track: '6.4',
          name: 'Camaroneras',
          description: 'Visualizar camaroneras del Cliente',
          icon: 'waves'
        }
      ]
    }
  ]
}
