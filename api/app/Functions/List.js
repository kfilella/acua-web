/**
 * Función para añadir acciones a los listados
 * Desde cualquier controlador se puede llamar a esta función de esta manera
 * const List = use('App/Functions/List')
 * users = List.addRowActions(users, [{name: 'edit', permission: 4}, {name: 'deleteR', permission: 6}])
 * la función recibe 2 parámetros, el primero es un array de objetos(registros) y el segundo es un array de objetos(botones)
 * a cada botón se le debe especificar su nombre y su permiso para ser mostrado
 * Todas la futuras acciones de registros deben definirse aquí
 */
exports.edit = (id, permission, route) => {
  return {
    color: 'primary',
    icon: 'edit',
    url: route + id,
    action: '',
    title: 'Editar',
    size: 'sm',
    permission: permission
  }
}
exports.delete =  (id, permission) => {
  return {
    color: 'negative',
    icon: 'delete',
    url: '',
    action: 'delete',
    title: 'Eliminar',
    size: 'sm',
    permission: permission
  }
}
exports.show =  (id, permission, route) => {
  return {
    color: 'positive',
    icon: 'remove_red_eye',
    url: route + id,
    action: '',
    title: 'Ver',
    size: 'sm',
    permission: permission
  }
}
exports.activate =  (id, permission, route) => {
  return {
    color: 'positive',
    icon: 'remove_red_eye',
    url: route + id,
    action: 'activate',
    title: 'Activar',
    size: 'sm',
    permission: permission
  }
}

exports.activateTechnicalChief =  (id, permission, route) => {
  return {
    color: 'positive',
    icon: 'remove_red_eye',
    url: route + id,
    action: 'activateTechnicalChief',
    title: 'Activar',
    size: 'sm',
    permission: permission
  }
}

exports.activateTechnicalAdvisor =  (id, permission, route) => {
  return {
    color: 'positive',
    icon: 'remove_red_eye',
    url: route + id,
    action: 'activateTechnicalAdvisor',
    title: 'Activar',
    size: 'sm',
    permission: permission
  }
}

exports.activateTechnicalEquipment =  (id, permission, route) => {
  return {
    color: 'positive',
    icon: 'remove_red_eye',
    url: route + id,
    action: 'activateTechnicalEquipment',
    title: 'Activar',
    size: 'sm',
    permission: permission
  }
}

exports.assign = (id, permission, route) => {
  return {
    color: 'primary',
    icon: 'add',
    url: route + id,
    action: '',
    title: 'Assignar',
    size: 'sm',
    permission: permission
  }
}

exports.sendMailPathology = (id, permission, route) => {
  return {
    color: 'primary',
    icon: 'add',
    url: route + id,
    action: '',
    title: 'Enviar Mail',
    size: 'sm',
    permission: permission
  }
}
exports.sendMailFloorAnalysis = (id, permission, route) => {
  return {
    color: 'primary',
    icon: 'add',
    url: route + id,
    action: '',
    title: 'Enviar Mail',
    size: 'sm',
    permission: permission
  }
}
exports.sendMailWaterAnalysis = (id, permission, route) => {
  return {
    color: 'primary',
    icon: 'add',
    url: route + id,
    action: '',
    title: 'Enviar Mail',
    size: 'sm',
    permission: permission
  }
}
exports.sendAnalysisEmail =  (id, permission) => {
  return {
    color: 'primary',
    icon: 'mail',
    url: '',
    action: 'sendAnalysisEmail',
    title: 'Enviar por correo',
    size: 'sm',
    permission: permission
  }
}

exports.sendMailPhytoplankton = (id, permission, route) => {
  return {
    color: 'primary',
    icon: 'add',
    url: route + id,
    action: '',
    title: 'Enviar Mail',
    size: 'sm',
    permission: permission
  }
}

exports.sendFloorAnalysisEmail =  (id, permission) => {
  return {
    color: 'primary',
    icon: 'mail',
    url: '',
    action: 'sendFloorAnalysisEmail',
    title: 'Enviar por correo',
    size: 'sm',
    permission: permission
  }
}

exports.printWaterAnalysis =  (id, permission) => {
  return {
    color: 'primary',
    icon: 'printer',
    url: '',
    action: 'printWaterAnalysis',
    title: 'Imprimir',
    size: 'sm',
    permission: permission
  }
}
exports.printPathologyAnalysis =  (id, permission) => {
  return {
    color: 'primary',
    icon: 'printer',
    url: '',
    action: 'printPathologyAnalysis',
    title: 'Imprimir',
    size: 'sm',
    permission: permission
  }
}
exports.printFloorAnalysis =  (id, permission) => {
  return {
    color: 'primary',
    icon: 'printer',
    url: '',
    action: 'printFloorAnalysis',
    title: 'Imprimir',
    size: 'sm',
    permission: permission
  }
}

exports.printPhytoplanktonAnalysis =  (id, permission) => {
  return {
    color: 'primary',
    icon: 'printer',
    url: '',
    action: 'printPhytoplanktonAnalysis',
    title: 'Imprimir',
    size: 'sm',
    permission: permission
  }
}

exports.generateLinkFloorAnalysis =  (id, permission) => {
  return {
    color: 'primary',
    icon: 'printer',
    url: '',
    action: 'generateLinkFloorAnalysis',
    title: 'Enlace',
    size: 'sm',
    permission: permission
  }
}

exports.generateLinkWaterAnalysis =  (id, permission) => {
  return {
    color: 'primary',
    icon: 'printer',
    url: '',
    action: 'generateLinkWaterAnalysis',
    title: 'Enlace',
    size: 'sm',
    permission: permission
  }
}

exports.generateLinkPathologyAnalysis =  (id, permission) => {
  return {
    color: 'primary',
    icon: 'printer',
    url: '',
    action: 'generateLinkPathologyAnalysis',
    title: 'Enlace',
    size: 'sm',
    permission: permission
  }
}

exports.generateLinkPhytoplanktonAnalysis =  (id, permission) => {
  return {
    color: 'primary',
    icon: 'printer',
    url: '',
    action: 'generateLinkPhytoplanktonAnalysis',
    title: 'Enlace',
    size: 'sm',
    permission: permission
  }
}

exports.addRowActions = (dataSet, actions) => {
  for (const i of dataSet) {
    i.actions = []
    for (const j of actions) {
      if (typeof this[j.name] === 'function') {
        i.actions.push(this[j.name](i.id, j.permission, j.route))
      }
    }
  }

  return dataSet;
};
