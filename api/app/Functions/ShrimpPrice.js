class ShrimpPriceFunctions {
  /**
   * Función que devuelve los rangos de precios de camarón
   *
   *
   */
   static getRanges () {
    return [
      {
        id: 1,
        from: 100,
        to: 120
      },
      {
        id: 2,
        from: 80,
        to: 100
      },
      {
        id: 3,
        from: 70,
        to: 80
      },
      {
        id: 4,
        from: 60,
        to: 70
      },
      {
        id: 5,
        from: 50,
        to: 60
      },
      {
        id: 6,
        from: 40,
        to: 50
      },
      {
        id: 7,
        from: 30,
        to: 40
      },
      {
        id: 8,
        from: 20,
        to: 30
      }
    ]
  }
}

module.exports = ShrimpPriceFunctions