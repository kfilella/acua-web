'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with segments
 */
const Segment = use('App/Models/Segment')
const List = use('App/Functions/List')
const { validate } = use('Validator')
class SegmentController {
  /**
   * Show a list of all segments.
   * GET segments
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ response }) { // Listado de segmentos de clientes
    let segments = (await Segment.query().where('type', '0').fetch()).toJSON()
    segments = List.addRowActions(segments, [{ name: 'edit', route: 'customer_segments/form/', permission: '2.15' }, { name: 'delete', permission: '2.14' }])
    response.send(segments)
  }

  async indexProvider ({ response }) { // Listado de segmentos de proveedores
    let segments = (await Segment.query().where('type', 1).fetch()).toJSON()
    segments = List.addRowActions(segments, [{ name: 'edit', route: 'customer_segments/form/', permission: '3.15' }, { name: 'delete', permission: '3.14' }])
    response.send(segments)
  }

  /**
   * Create/save a new segment.
   * POST segments
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const validation = await validate(request.all(), Segment.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const body = request.only(Segment.fillable)
      const segment = await Segment.create(body)
      response.send(segment);
    }
  }
  async storeProvider ({ request, response }) {
    const validation = await validate(request.all(), Segment.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const body = request.only(Segment.fillable)
      const segment = await Segment.create(body)
      response.send(segment);
    }
  }

  /**
   * Display a single segment.
   * GET segments/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    const segment = (await Segment.query().where('id', params.id).first()).toJSON()
    response.send(segment)
  }
  async showProvider ({ params, request, response, view }) {
    const segment = (await Segment.query().where('id', params.id).first()).toJSON()
    response.send(segment)
  }

  /**
   * Update segment details.
   * PUT or PATCH segments/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const segment = await Segment.query().where('id', params.id).first()
    if (segment) {
      const validation = await validate(request.all(), Segment.fieldValidationRules())

      if (validation.fails()) {
        response.unprocessableEntity(validation.messages())
      } else {
        const body = request.only(Segment.fillable)

        await Segment.query().where('id', params.id).update(body)
        response.send(segment)
      }
    } else {
      response.notFound(validation.messages())
    }
  }
  async updateProvider ({ params, request, response }) {
    const segment = await Segment.query().where('id', params.id).first()
    if (segment) {
      const validation = await validate(request.all(), Segment.fieldValidationRules())

      if (validation.fails()) {
        response.unprocessableEntity(validation.messages())
      } else {
        const body = request.only(Segment.fillable)

        await Segment.query().where('id', params.id).update(body)
        response.send(segment)
      }
    } else {
      response.notFound(validation.messages())
    }
  }

  /**
   * Delete a segment with id.
   * DELETE segments/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    const segment = await Segment.find(params.id)
    segment.delete()
    response.send('success')
  }
  async destroyProvider ({ params, request, response }) {
    const segment = await Segment.find(params.id)
    segment.delete()
    response.send('success')
  }
}

module.exports = SegmentController
