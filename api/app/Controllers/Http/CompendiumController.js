'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with compendiums
 */
const Compendium = use('App/Models/Compendium')
const CompendiumPool = use('App/Models/CompendiumPool')
const CompendiumGrowthReport = use('App/Models/CompendiumGrowthReport')
const Shrimp = use('App/Models/Shrimp')
const Product = use('App/Models/Product')
const { validate } = use('Validator')
const PdfPrinter = use("pdfmake")
const Helpers = use("Helpers")
const fs = use("fs")
const moment = require('moment')
const ExcelJS = require('exceljs')

var fonts = {
  Roboto: {
    normal: 'resources/fonts/Roboto-Regular.ttf',
    bold: 'resources/fonts/Roboto-Medium.ttf',
    italics: 'resources/fonts/Roboto-Italic.ttf',
    bolditalics: 'resources/fonts/Roboto-MediumItalic.ttf'
  }
}
class CompendiumController {
  /**
   * Show a list of all compendiums.
   * GET compendiums
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    const params = request.get()
    let compendium = (await Compendium.query().with('pools.pool').where(params).orderBy('created_at', 'desc').fetch()).toJSON()
    compendium.forEach(element => {
      element.seed_date = moment(element.seed_date).format('DD-MM-YYYY')
      element.poolNames = element.pools.map(val => val.pool.description).join(',')
    })
    response.send(compendium)
  }

  /**
   * Create/save a new compendium.
   * POST compendiums
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const { pools, shrimp_id } = request.all()

    // Guardar compendio maestro
    const compendium = await Compendium.create({
      shrimp_id
    })

    for (const i of pools) { // Recorro cada piscina para guardarla en la prueba
      // const validation = await validate(request.all(), Compendium.fieldValidationRules())

        const growthReports = i.growthReports
        delete i.growthReports
        const body = i
        body.compendium_id = compendium.id
        body.products = JSON.stringify(body.products.sort((a, b) => a - b))
        const compendiumPool = await CompendiumPool.create(body)

        // Recorrer reportes de crecimiento de cada piscina
        for (const j of growthReports) {
          j.compendium_pool_id = compendiumPool.id
          await CompendiumGrowthReport.create(j)
        }
    }

    response.send(true)
  }

  /**
   * Display a single compendium.
   * GET compendiums/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    const compendium = (await Compendium.query().with('pools.pool').with('pools.growthReports', (query) => {
      query.orderBy('date', 'desc')
      // query.limit(6)
    }).where('id', params.id).first()).toJSON()

    compendium.pools.forEach(pool => {
      pool.seed_date = moment(pool.seed_date).format('YYYY-MM-DD')
      pool.growthReports.forEach(growth => {
        growth.date = moment(growth.date).format('YYYY-MM-DD')
      })
      pool.products = JSON.parse(pool.products)
    })

    response.send(compendium)
  }

  /**
   * Update compendium details.
   * PUT or PATCH compendiums/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    /* const compendium = await Compendium.query().where('id', params.id).first()
    if (compendium) {
      const validation = await validate(request.all(), Compendium.fieldValidationRules())

      if (validation.fails()) {
        response.unprocessableEntity(validation.messages())
      } else {
        const body = request.only(Compendium.fillable)

        await Compendium.query().where('id', params.id).update(body)
        response.send(compendium)
      }
    } else {
      response.notFound(validation.messages())
    } */
    const compendium_id = params.id
    const { pools } = request.all()

    const activePoolIds = []
    const activeGrowthsIds = []

    for (const i of pools) { // Recorro cada piscina para actualizar o crearla dentro de compendio
      const growthReports = i.growthReports
      delete i.growthReports
      delete i.pool
      const body = i
      body.compendium_id = compendium_id
      body.products = JSON.stringify(body.products.sort((a, b) => a - b))

      if (i.id) { // Si la piscina ya existe entonces hay que actualizarla
        await CompendiumPool.query().where('id', i.id).update(body)
        activePoolIds.push(i.id)
      } else { // Si no existe se crea
        var compendiumPool = await CompendiumPool.create(body)
        activePoolIds.push(compendiumPool.id)
      }

      // Recorrer reportes de crecimiento de cada piscina
      const compendium_pool_id = i.id ? i.id : compendiumPool.id
      for (const j of growthReports) {
        j.compendium_pool_id = i.id ? i.id : compendiumPool.id

        if (j.id) { // Si el crecimiento ya existe entonces hay que actualizarlo
          await CompendiumGrowthReport.query().where('id', j.id).update(j)
          activeGrowthsIds.push(j.id)
        } else { // Si no existe se crea
          const growthRecord = await CompendiumGrowthReport.create(j)
          activeGrowthsIds.push(growthRecord.id)
        }
      }
      // Eliminar todos los crecimientos de las piscinas del compendio que no se encuentren activos
      await CompendiumGrowthReport.query().where({compendium_pool_id}).whereNotIn('id', activeGrowthsIds).delete()

    }

    // Eliminar todas las piscinas del compendio que no se encuentren activas
    await CompendiumPool.query().where({compendium_id}).whereNotIn('id', activePoolIds).delete()
    response.send(true)
  }

  /**
   * Delete a compendium with id.
   * DELETE compendiums/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }

   /**
   * Create/save a new compendium growth reports.
   * POST compendium_growth_reports
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async storeGrowthReport ({ request, response }) {
    const validation = await validate(request.all(), CompendiumGrowthReport.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const body = request.only(CompendiumGrowthReport.fillable)
      const growthReport = await CompendiumGrowthReport.create(body)
      response.send(growthReport)
    }
  }

  /**
   * Update compendium growth report details.
   * PUT compendium_growth_reports/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async updateGrowthReport ({ params, request, response }) {
    const growthReport = await CompendiumGrowthReport.query().where('id', params.id).first()
    if (growthReport) {
      const validation = await validate(request.all(), CompendiumGrowthReport.fieldValidationRules())

      if (validation.fails()) {
        response.unprocessableEntity(validation.messages())
      } else {
        const body = request.only(CompendiumGrowthReport.fillable)

        await CompendiumGrowthReport.query().where('id', params.id).update(body)
        response.send(growthReport)
      }
    } else {
      response.notFound(validation.messages())
    }
  }

  /**
   * Generate PDF compendium report.
   * POST compendium_print
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async printReport ({ request, response, params }) {
    var printer = new PdfPrinter(fonts)
    const req = request.all()

    // Si fue seleccionada "Todas" mostrar todos los compendios independientemente la marca
    const brandCondition = req.brand != 'Todas' ? {
      brand: req.brand
    } : {}

    // Ids de Productos de la marca seleccionada
    const productIds = (await Product.query().where(brandCondition).fetch()).toJSON().map(val => val.id)

    const compendium = await Compendium.query().where('shrimp_id', req.shrimp_id).whereIn('product_id', productIds)
    .whereBetween('seed_date', [req.start_date, req.end_date]).orWhereHas('growthReports', (query) => {
      query.whereBetween('date', [req.start_date, req.end_date])
    })
    .with('growthReports', (query) => {
      query.limit(req.report_number)
    })
    .fetch()

    const widths = Array(10).fill('auto')

    // return compendium

    let title = `Prueba Camaronera AVICACOMPANY`
    let name = 'Asesor Técnico:'
    this.img = {
      image: 'logo.jpeg',
      fit: [70, 70],
      absolutePosition: {
        'x': 35,
        'y': 35
      }
    },
    this.img2 = {
      image: 'logo.jpeg',
      fit: [70, 70],
      absolutePosition: {
        'x': 20,
        'y': 10
      }
    }
    var docDefinition = {
      pageSize: 'letter',
      pageMargins: [60, 120, 40, 90],
      pageOrientation: 'landscape',
      header: [
        {
          margin: [20, 40, 20, 20],
          alignment: 'center',
          text: [
            {
              bold: true,
              text: `${title}\n`,
              fontSize: 14
            }
          ]
        },
        this.img
      ],
      content: [
        {
          table: {
            // headers are automatically repeated if the table spans over multiple pages
            // you can declare how many rows should be treated as headers
            headerRows: 1,
            widths,

            body: [
              [ 'Pisc', 'Área', 'Fecha', 'Densidad', '# Cam', 'Lbs', 'Tam. Siemb', 'Días', 'Reportes', 'Crec. Últ', 'Crec. Ant.', 'Crec. 4 Ult', 'Crec. Gral' ]
            ]
          }
        }
      ],
      /* footer: {
        columns: [
          this.img2,
          {text: 'Dpto. Técnico\n', bold: true, alignment: 'left', fontSize: 12, margin: [-25, 13, 0, 0] },
          {text: 'Division Acuacultura\nTelf. 593 4 2811-61 Ext. 214-240\nhttp://www.agripac.com.ec/\n', alignment: 'left', fontSize: 10, margin: [-144, 25, 0, 0]},
          {text: 'Guayaquil - Ecuador', bold: true, alignment: 'left', fontSize: 12, margin: [-263, 60, 0, 0] },
          {text: `${name}\n${floorAnalysis.user.technical_advisor.full_name ? floorAnalysis.user.technical_advisor.full_name : ''}\n`,bold:true, alignment: 'left', fontSize: 10, margin: [5, 30, 0, 0] },
        ]
      } */
    }

    var options = {
      // ...
    }
    //return docDefinition
    var pdfDoc = printer.createPdfKitDocument(docDefinition, options)
    const dir = Helpers.appRoot('storage/uploads/flooranalysis')
    const filename = `Analisis_de_suelos_${compendium.id}.pdf`
    if (!fs.existsSync(dir)){
      fs.mkdirSync(dir);
    }
    pdfDoc.pipe(fs.createWriteStream(`${dir}/${filename}`));
    pdfDoc.end()

    response.send(`flooranalysis-Analisis_de_suelos_${compendium.id}`)
  }

  async getCompendiumsByFoods({ request }) {
    let { foodsIds } = request.get()
    foodsIds = foodsIds ? foodsIds.map(val => parseInt(val)) : []

    let compendiums = await Compendium.query().with('pools.growthReports').with('shrimp')
    .with('pools.growthReports', (query) => {
      query.orderBy('date', 'desc')
    })
    .fetch()
    compendiums = compendiums && foodsIds.length ? compendiums.toJSON() : []


    // Se deben mostrar los compendios que tengan todos los alimentos o grupos de alimentos seleccionados
    // Recorrer cada compendio y sacar los ids de los alimentos que tiene cada piscina

    compendiums.forEach(compendium => {
      const pools = compendium.pools
      let compendiumFoodIds = []
      pools.forEach(pool => {
        const products = pool.products ? JSON.parse(pool.products) : []
        products.forEach(product => {
          compendiumFoodIds.push(product)
        })
      })

      // Eliminar repetidos y ordenar ascendentemente
      compendiumFoodIds = [...new Set(compendiumFoodIds)]
      compendiumFoodIds = compendiumFoodIds.sort((a, b) => a - b)
      compendium.compendiumFoodIds = compendiumFoodIds
      compendium.dateFirstPool = pools.length ? moment(pools[pools.length - 1].seed_date).format('DD-MM-YYYY') : ''
    })

    // Ordenar foodsIds
    foodsIds = foodsIds.sort((a, b) => a - b)
    // return foodsIds

    // Filtrar los compendios donde sus ids coincidan con foodsIds
    // compendiums = compendiums.filter(val => val.compendiumFoodIds == JSON.stringify(foodsIds))
    compendiums = compendiums.filter(val => {
      let matches = 0
      foodsIds.forEach(foodId => {
        if (val.compendiumFoodIds.indexOf(foodId) > -1) {
          matches++
        }
      })
      return matches == foodsIds.length
    })

    // Darle nombre a los alimentos de cada compendio
    for (let i of compendiums) {
      const compendiumFoodIds = i.compendiumFoodIds
      let foodNames = []

      for (let j of compendiumFoodIds) {
        foodNames.push((await Product.query().where('id', j).first()).toJSON().name)
      }
      foodNames = foodNames.join(',')
      i.foodNames = foodNames
    }




    // Debo sacar las fechas de muestreo de las piscinas

    let dates = []
    compendiums.forEach(compendium => {
      compendium.pools.forEach(pool => {
        pool.growthReports.forEach(report => {
          dates.push({
            date: report.date,
            dateF: moment(report.date).format('DD-MM-YYYY')
          })
        })
      })
      dates = dates.filter((v,i,a)=>a.findIndex(t=>(t.dateF===v.dateF))===i)
      compendium.dates = dates
      dates = []
      compendium.check = false
    })

    return compendiums
  }

  async printReportXls ({ request, response }) {
    let { compendiums } = request.get()
    compendiums = JSON.parse(compendiums)
    const compendiumIds = compendiums.map(val => val.id)
    const workbook = new ExcelJS.Workbook()



    const shrimp = {}

    const records = (await Compendium.query().with('pools.pool').with('pools.product').with('shrimp')
    .with('pools.growthReports', (query) => {
      query.orderBy('date', 'desc')
    }).whereIn('id', compendiumIds).fetch()).toJSON() // Compendios realizados

    const products = (await Product.all()).toJSON()

    let foods = []


    await workbook.xlsx.readFile('resources/templates/plantilla_compendio.xlsx').then(function() {
      const worksheet = workbook.getWorksheet(1)




      const styleRowTotals = worksheet.getCell('D10').style
      const styleRowTotalsF = worksheet.getCell('F10').style
      const styleRowTotalsH = worksheet.getCell('H10').style
      const styleRowTotalsM = worksheet.getCell('M10').style
      const styleRowTotalsW = worksheet.getCell('W10').style
      const d9Style = worksheet.getCell('D9').style
      const e9Style = worksheet.getCell('E9').style

      // Compendios
      let poolIncrement = 9
      let foodIncrement = 9


      records.forEach((element, key) => {
        // obtener los alimentos del compendio actual
        /* foods = element.pools.map(val => (
          {
            id: val.product_id,
            name: val.product.name
          }
        ))
        foods = foods.filter((v,i,a)=>a.findIndex(t=>(t.id === v.id && t.name===v.name))===i) */

        // Guardo en un array todas las combinaciones de alimentos guardadas en las piscinas
        // Obtener info de la camaronera
        worksheet.getCell('A3').value = `Prueba Camaronera ${element.shrimp.name}`


        let foods = []
        element.pools.forEach(element2 => {
          if (element2.products) {
            foods.push(element2.products)
          }
        })
        foods = [...new Set(foods)]




        // Fecha de muestreo
        const sampling_date = compendiums.find(val => val.id == element.id).sampling_date
        if (key == 0) { // Si es el primer compendio, se usa la plantilla actual

          worksheet.getCell('E6').value = new Date(sampling_date)

          let firstNumberRowFood, lastNumberRowFood
          foods.forEach((food, foodKey) => { // Recorro cada grupo de alimento
            const arrayFoodsIds = JSON.parse(food)

            const foodNames = arrayFoodsIds.map(val => products.find(val2 => val2.id == val).name).join('\n')

            firstNumberRowFood = foodKey == 0 ? foodIncrement : firstNumberRowFood
            // lastNumberRowFood = foodKey == 0 ? foodIncrement + (poolsWithFood.length - 1)  : lastNumberRowFood

            // Buscar las piscinas que usaron ese alimento
            const poolsWithFood = element.pools.filter(val => val.products == food)

            // Copiar estilo de celda
            worksheet.getCell(`B${foodIncrement}`).style = worksheet.getCell('B9').style
            // Columna Alimento
            worksheet.getCell(`B${foodIncrement}`).value = foodNames
            worksheet.getCell(`B${foodIncrement}`).alignment = { wrapText: true, vertical: 'middle' }
            worksheet.mergeCells(`B${foodIncrement}:B${foodIncrement + (poolsWithFood.length - 1)}`)

            // Recorrer las piscinas dentro de este alimento
            let firstNumberRowFoodPool, lastNumberRowFoodPool
            poolsWithFood.forEach((pool, poolIndex) => {
              // Guardar el número de la primera fila del alimento
              firstNumberRowFoodPool = poolIndex == 0 ? poolIncrement : firstNumberRowFoodPool
              lastNumberRowFoodPool = poolIndex == 0 ? poolIncrement + (poolsWithFood.length - 1)  : lastNumberRowFoodPool

              // Copiar estilos de celdas de datos
              worksheet.getCell(`C${poolIncrement}`).style = worksheet.getCell('C9').style
              worksheet.getCell(`D${poolIncrement}`).style = d9Style
              worksheet.getCell(`E${poolIncrement}`).style = e9Style
              worksheet.getCell(`F${poolIncrement}`).style = worksheet.getCell('F9').style
              worksheet.getCell(`G${poolIncrement}`).style = worksheet.getCell('G9').style
              worksheet.getCell(`H${poolIncrement}`).style = worksheet.getCell('H9').style
              worksheet.getCell(`I${poolIncrement}`).style = worksheet.getCell('I9').style
              worksheet.getCell(`J${poolIncrement}`).style = worksheet.getCell('J9').style
              worksheet.getCell(`K${poolIncrement}`).style = worksheet.getCell('K9').style
              worksheet.getCell(`L${poolIncrement}`).style = worksheet.getCell('L9').style
              worksheet.getCell(`M${poolIncrement}`).style = worksheet.getCell('M9').style
              worksheet.getCell(`N${poolIncrement}`).style = worksheet.getCell('N9').style
              worksheet.getCell(`O${poolIncrement}`).style = worksheet.getCell('O9').style
              worksheet.getCell(`P${poolIncrement}`).style = worksheet.getCell('P9').style
              worksheet.getCell(`Q${poolIncrement}`).style = worksheet.getCell('Q9').style
              worksheet.getCell(`R${poolIncrement}`).style = worksheet.getCell('R9').style
              worksheet.getCell(`S${poolIncrement}`).style = worksheet.getCell('S9').style
              worksheet.getCell(`T${poolIncrement}`).style = worksheet.getCell('T9').style
              worksheet.getCell(`U${poolIncrement}`).style = worksheet.getCell('U9').style
              worksheet.getCell(`V${poolIncrement}`).style = worksheet.getCell('V9').style
              worksheet.getCell(`W${poolIncrement}`).style = worksheet.getCell('W9').style
              worksheet.getCell(`X${poolIncrement}`).style = worksheet.getCell('X9').style
              worksheet.getCell(`Y${poolIncrement}`).style = worksheet.getCell('Y9').style
              worksheet.getCell(`Z${poolIncrement}`).style = worksheet.getCell('Z9').style
              worksheet.getCell(`AA${poolIncrement}`).style = worksheet.getCell('AA9').style
              worksheet.getCell(`AB${poolIncrement}`).style = worksheet.getCell('AB9').style
              worksheet.getCell(`AC${poolIncrement}`).style = worksheet.getCell('AC9').style
              worksheet.getCell(`AD${poolIncrement}`).style = worksheet.getCell('AD9').style

              // Columna piscina
              worksheet.getCell(`C${poolIncrement}`).value = pool.pool.description

              // Columna Área
              worksheet.getCell(`D${poolIncrement}`).value = parseFloat(pool.pool.area)

              // Columna Fecha
              worksheet.getCell(`E${poolIncrement}`).value = new Date(pool.seed_date)

              // Columna Densidad
              worksheet.getCell(`F${poolIncrement}`).value = parseFloat(pool.density)

              // Columna #Cam
              worksheet.getCell(`G${poolIncrement}`).value = { formula: `F${poolIncrement}*D${poolIncrement}` }

              // Columna MP debe llevar el mp según la fecha de muestreo. Si no hay entonces tomar de la fecha anterior
              // worksheet.getCell(`H${poolIncrement}`).value = parseFloat(pool.mp / 100)
              let mp = pool.growthReports.find(val => moment(val.date).format('YYYY-MM-DD') == moment(sampling_date).format('YYYY-MM-DD'))
              mp = mp ? mp.mp : 0
              if (mp == 0) {
                const mpValue = pool.growthReports.find(val => val.mp != 0 && moment(val.date).format('YYYY-MM-DD') < moment(sampling_date).format('YYYY-MM-DD'))
                mp = mpValue ? mpValue.mp : 0
              }

              worksheet.getCell(`H${poolIncrement}`).value = (mp / 100)

              // Columna # Cam actual
              worksheet.getCell(`I${poolIncrement}`).value = { formula: `G${poolIncrement}*H${poolIncrement}` }

              // Columna Biomasa inicial
              worksheet.getCell(`J${poolIncrement}`).value = { formula: `G${poolIncrement}*L${poolIncrement}/454` }

              // Columna lbs
              worksheet.getCell(`K${poolIncrement}`).value = { formula: `+G${poolIncrement}*H${poolIncrement}*N${poolIncrement}/454` }

              // Columna Peso siembra
              worksheet.getCell(`L${poolIncrement}`).value = parseFloat(pool.sowing_size)

              // Columna Días
              worksheet.getCell(`M${poolIncrement}`).value = { formula: `$E$6-E${poolIncrement}+1` }

              // Filtrar los crecimientos para mostrar los iguales o menores a la fecha de muestreo
              let growthReports = [...pool.growthReports.filter(val => moment(val.date).format('YYYY-MM-DD') <= moment(sampling_date).format('YYYY-MM-DD'))]


              // Columna Peso. Debe mostrarse el peso(reporte) de la fecha de muestreo
              let lastGrowth = growthReports.find(val => moment(val.date).format('YYYY-MM-DD') == moment(sampling_date).format('YYYY-MM-DD'))
              lastGrowth = lastGrowth ? lastGrowth : {} // Esto no debe ser así
              worksheet.getCell(`N${poolIncrement}`).value = parseFloat(lastGrowth ? lastGrowth.size : 0)

              // Eliminar de los crecimientos el último usado y dejar los no usados todavía
              growthReports = growthReports.filter(val => moment(val.date).format('YYYY-MM-DD') != moment(lastGrowth.date).format('YYYY-MM-DD'))



              if (growthReports.length) { // Si hay penúltimo peso
                // Columna penúltimo peso
                worksheet.getCell(`O${poolIncrement}`).value = parseFloat(growthReports[0].size)
              }

              // Eliminar de los crecimientos el último usado y dejar los no usados todavía
              // growthReports = growthReports.splice(0, 1)
              growthReports = growthReports.filter(val => moment(val.date).format('YYYY-MM-DD') != moment(growthReports[0].date).format('YYYY-MM-DD'))


              if (growthReports.length) { // Si hay antepenúltimo peso
                // Columna antepenúltimo peso
                worksheet.getCell(`P${poolIncrement}`).value = parseFloat(growthReports[0].size)
              }
              // Eliminar de los crecimientos el último usado y dejar los no usados todavía
              growthReports = growthReports.filter(val => moment(val.date).format('YYYY-MM-DD') != moment(growthReports[0].date).format('YYYY-MM-DD'))

              if (growthReports.length) { // Si hay anteantepenúltimo peso
                // Columna anteantepenúltimo peso
                worksheet.getCell(`Q${poolIncrement}`).value = parseFloat(growthReports[0].size)
              }
              // Eliminar de los crecimientos el último usado y dejar los no usados todavía
              growthReports = growthReports.filter(val => moment(val.date).format('YYYY-MM-DD') != moment(growthReports[0].date).format('YYYY-MM-DD'))

              if (growthReports.length) { // Si hay anteanteantepenúltimo peso
                // Columna anteanteantepenúltimo peso
                worksheet.getCell(`R${poolIncrement}`).value = parseFloat(growthReports[0].size)
              }
              // Eliminar de los crecimientos el último usado y dejar los no usados todavía
              growthReports = growthReports.filter(val => moment(val.date).format('YYYY-MM-DD') != moment(growthReports[0].date).format('YYYY-MM-DD'))





               // Columna Crec últ
               worksheet.getCell(`S${poolIncrement}`).value = { formula: `+N${poolIncrement}-O${poolIncrement}` }

               // Columna Crec ant
               worksheet.getCell(`T${poolIncrement}`).value = { formula: `+O${poolIncrement}-P${poolIncrement}` }

              // Columna Crec 4 ult +(N9-Q9)/28*7
              worksheet.getCell(`U${poolIncrement}`).value = { formula: `+(N${poolIncrement}-Q${poolIncrement})/28*7` }

              // Columna Crec gral
              worksheet.getCell(`V${poolIncrement}`).value = { formula: `+(N${poolIncrement}-L${poolIncrement})/(M${poolIncrement}/7)` }

              // Columna Kg Alim, deben ir los kilos de la fecha de muestreo seleccionada
              // worksheet.getCell(`W${poolIncrement}`).value = parseFloat(pool.kg_alim)

              const kgAlimValue = pool.growthReports.find(val => moment(val.date).format('YYYY-MM-DD') == moment(sampling_date).format('YYYY-MM-DD'))


              worksheet.getCell(`W${poolIncrement}`).value = kgAlimValue ? kgAlimValue.kg_alim : 0

              // Columna FC
              worksheet.getCell(`X${poolIncrement}`).value = { formula: `(W${poolIncrement}*2.2046)/(K${poolIncrement}-(G${poolIncrement}*L${poolIncrement}/454))` }

              // Columna FC prom
              worksheet.getCell(`Y${poolIncrement}`).value = { formula: `(SUM(W${firstNumberRowFoodPool}:W${lastNumberRowFoodPool})*2.2046)/((SUM(K${firstNumberRowFoodPool}:K${lastNumberRowFoodPool})-SUM(J${firstNumberRowFoodPool}:J${lastNumberRowFoodPool})))` }
              if (poolIndex == 0) { // si es la primera piscina hacer el merge de FC prom
                worksheet.mergeCells(`Y${firstNumberRowFoodPool}:Y${lastNumberRowFoodPool}`)
                worksheet.mergeCells(`Z${firstNumberRowFoodPool}:Z${lastNumberRowFoodPool}`)
                worksheet.mergeCells(`AA${firstNumberRowFoodPool}:AA${lastNumberRowFoodPool}`)
                worksheet.mergeCells(`AB${firstNumberRowFoodPool}:AB${lastNumberRowFoodPool}`)
                worksheet.mergeCells(`AC${firstNumberRowFoodPool}:AC${lastNumberRowFoodPool}`)
                worksheet.mergeCells(`AD${firstNumberRowFoodPool}:AD${lastNumberRowFoodPool}`)
              }

              // Columna Peso prom
              worksheet.getCell(`Z${poolIncrement}`).value = { formula: `(SUM(K${firstNumberRowFoodPool}:K${lastNumberRowFoodPool})*454)/SUM(I${firstNumberRowFoodPool}:I${lastNumberRowFoodPool})` }

              // Columna Crec prom
              worksheet.getCell(`AA${poolIncrement}`).value = { formula: `AVERAGE(V${firstNumberRowFoodPool}:V${lastNumberRowFoodPool})` }

              // Columna % sup
              worksheet.getCell(`AB${poolIncrement}`).value = { formula: `+SUM(AE${firstNumberRowFoodPool}:AE${lastNumberRowFoodPool})/SUM(G${firstNumberRowFoodPool}:G${lastNumberRowFoodPool})` }

              // Columna lbs ha
              worksheet.getCell(`AC${poolIncrement}`).value = { formula: `SUM(K${firstNumberRowFoodPool}:K${lastNumberRowFoodPool})/SUM(D${firstNumberRowFoodPool}:D${lastNumberRowFoodPool})` }

              // Columna dens actual
              worksheet.getCell(`AD${poolIncrement}`).value = { formula: `SUM(I${firstNumberRowFoodPool}:I${lastNumberRowFoodPool})/SUM(D${firstNumberRowFoodPool}:D${lastNumberRowFoodPool})/10000` }

              // Columna oculta para cálculos
              worksheet.getCell(`AE${poolIncrement}`).value = { formula: `+G${poolIncrement}*H${poolIncrement}` }

              worksheet.getCell('O7').model.result = undefined
              worksheet.getCell('P7').model.result = undefined
              worksheet.getCell('Q7').model.result = undefined
              worksheet.getCell('R7').model.result = undefined


              // En la última fila de piscinas del grupo debe colocarse el doble borde
              if (poolIndex == (poolsWithFood.length - 1)) {
              }
              poolIncrement++
            })


            // Incrementar según las cantidad de piscinas dentro del alimento
            // foodIncrement++
            foodIncrement += (poolsWithFood.length)
          })

          // Línea de totales
          worksheet.getCell(`D${poolIncrement}`).style = styleRowTotals
          worksheet.getCell(`F${poolIncrement}`).style = styleRowTotalsF
          worksheet.getCell(`G${poolIncrement}`).style = styleRowTotals
          worksheet.getCell(`H${poolIncrement}`).style = styleRowTotalsH
          worksheet.getCell(`I${poolIncrement}`).style = styleRowTotals
          worksheet.getCell(`K${poolIncrement}`).style = styleRowTotals
          worksheet.getCell(`L${poolIncrement}`).style = styleRowTotals
          worksheet.getCell(`M${poolIncrement}`).style = styleRowTotalsM
          worksheet.getCell(`S${poolIncrement}`).style = styleRowTotals
          worksheet.getCell(`T${poolIncrement}`).style = styleRowTotals
          worksheet.getCell(`U${poolIncrement}`).style = styleRowTotals
          worksheet.getCell(`V${poolIncrement}`).style = styleRowTotals
          worksheet.getCell(`W${poolIncrement}`).style = styleRowTotalsW
          worksheet.getCell(`X${poolIncrement}`).style = styleRowTotals

          worksheet.getCell(`D${poolIncrement}`).value = { formula: `SUM(D${firstNumberRowFood}:D${poolIncrement - 1})` }
          worksheet.getCell(`F${poolIncrement}`).value = { formula: `G${poolIncrement}/D${poolIncrement}` }
          worksheet.getCell(`G${poolIncrement}`).value = { formula: `SUM(G${firstNumberRowFood}:G${poolIncrement - 1})` }
          worksheet.getCell(`H${poolIncrement}`).value = { formula: `I${poolIncrement}/G${poolIncrement}` }
          worksheet.getCell(`I${poolIncrement}`).value = { formula: `SUM(I${firstNumberRowFood}:I${poolIncrement - 1})` }
          worksheet.getCell(`K${poolIncrement}`).value = { formula: `SUM(K${firstNumberRowFood}:K${poolIncrement - 1})` }
          worksheet.getCell(`L${poolIncrement}`).value = { formula: `AVERAGE(L${firstNumberRowFood}:L${poolIncrement - 1})` }
          worksheet.getCell(`M${poolIncrement}`).value = { formula: `AVERAGE(M${firstNumberRowFood}:M${poolIncrement - 1})` }
          worksheet.getCell(`S${poolIncrement}`).value = { formula: `AVERAGE(S${firstNumberRowFood}:S${poolIncrement - 1})` }
          worksheet.getCell(`T${poolIncrement}`).value = { formula: `AVERAGE(T${firstNumberRowFood}:T${poolIncrement - 1})` }
          worksheet.getCell(`U${poolIncrement}`).value = { formula: `AVERAGE(U${firstNumberRowFood}:U${poolIncrement - 1})` }
          worksheet.getCell(`V${poolIncrement}`).value = { formula: `AVERAGE(V${firstNumberRowFood}:V${poolIncrement - 1})` }
          worksheet.getCell(`W${poolIncrement}`).value = { formula: `SUM(W${firstNumberRowFood}:W${poolIncrement - 1})` }
          worksheet.getCell(`X${poolIncrement}`).value = { formula: `(W${poolIncrement}*2.2046)/(K${poolIncrement}-(G${poolIncrement}*L${poolIncrement}/454))` }

        } else { // Si es del segundo compendio en adelante
          // Le incremento 2 al contador de filas de piscinas para seguir con el otro compendio
          poolIncrement += 7
          foodIncrement += 7

          // Titulo
          worksheet.mergeCells(`A${poolIncrement - 5}:AD${poolIncrement - 5}`)
          worksheet.getCell(`A${poolIncrement - 5}`).style = worksheet.getCell('A3').style
          worksheet.getCell(`A${poolIncrement - 5}`).value = `Prueba Camaronera ${element.shrimp.name}`

          // Copiar estilos de primera fila del formato
          worksheet.getCell(`C${poolIncrement - 3}`).style = worksheet.getCell('C6').style
          worksheet.getCell(`C${poolIncrement - 3}`).value = 'Muestreo'
          worksheet.mergeCells(`C${poolIncrement - 3}:D${poolIncrement - 3}`)

          worksheet.getCell(`E${poolIncrement - 3}`).style = worksheet.getCell('E6').style
          worksheet.getCell(`E${poolIncrement - 3}`).value = new Date(sampling_date)

          // Copiar estilos de la segunda fila del formato
          worksheet.getCell(`C${poolIncrement - 2}`).style = worksheet.getCell('C7').style
          worksheet.getCell(`C${poolIncrement - 2}`).value = 'Pisc'
          worksheet.mergeCells(`C${poolIncrement - 2}:C${poolIncrement - 1}`)

          worksheet.getCell(`D${poolIncrement - 2}`).style = worksheet.getCell('D7').style
          worksheet.getCell(`D${poolIncrement - 2}`).value = 'Área'
          worksheet.mergeCells(`D${poolIncrement - 2}:D${poolIncrement - 1}`)

          worksheet.getCell(`E${poolIncrement - 2}`).style = worksheet.getCell('E7').style
          worksheet.getCell(`E${poolIncrement - 2}`).value = 'Fecha Siembra'
          worksheet.mergeCells(`E${poolIncrement - 2}:E${poolIncrement - 1}`)

          worksheet.getCell(`F${poolIncrement - 2}`).style = worksheet.getCell('F7').style
          worksheet.getCell(`F${poolIncrement - 2}`).value = 'Densidad'
          worksheet.mergeCells(`F${poolIncrement - 2}:F${poolIncrement - 1}`)

          worksheet.getCell(`G${poolIncrement - 2}`).style = worksheet.getCell('G7').style
          worksheet.getCell(`G${poolIncrement - 2}`).value = '#'
          worksheet.getCell(`G${poolIncrement - 1}`).style = worksheet.getCell('G8').style
          worksheet.getCell(`G${poolIncrement - 1}`).value = 'Cam'

          worksheet.getCell(`H${poolIncrement - 2}`).style = worksheet.getCell('H7').style
          worksheet.getCell(`H${poolIncrement - 2}`).value = 'Ult'
          worksheet.getCell(`H${poolIncrement - 1}`).style = worksheet.getCell('H8').style
          worksheet.getCell(`H${poolIncrement - 1}`).value = 'M/P'

          worksheet.getCell(`I${poolIncrement - 2}`).style = worksheet.getCell('I7').style
          worksheet.getCell(`I${poolIncrement - 2}`).value = '#'
          worksheet.getCell(`I${poolIncrement - 1}`).style = worksheet.getCell('I8').style
          worksheet.getCell(`I${poolIncrement - 1}`).value = 'Cam actual'

          worksheet.getCell(`J${poolIncrement - 2}`).style = worksheet.getCell('J7').style
          worksheet.getCell(`J${poolIncrement - 2}`).value = 'biomasa'
          worksheet.getCell(`J${poolIncrement - 1}`).style = worksheet.getCell('J8').style
          worksheet.getCell(`J${poolIncrement - 1}`).value = 'Inicial'

          worksheet.getCell(`K${poolIncrement - 2}`).style = worksheet.getCell('K7').style
          worksheet.getCell(`K${poolIncrement - 2}`).value = 'lbs'
          worksheet.mergeCells(`K${poolIncrement - 2}:K${poolIncrement - 1}`)

          worksheet.getCell(`L${poolIncrement - 2}`).style = worksheet.getCell('L7').style
          worksheet.getCell(`L${poolIncrement - 2}`).value = 'Peso'
          worksheet.getCell(`L${poolIncrement - 1}`).style = worksheet.getCell('L8').style
          worksheet.getCell(`L${poolIncrement - 1}`).value = 'Siembra'

          worksheet.getCell(`M${poolIncrement - 2}`).style = worksheet.getCell('M7').style
          worksheet.getCell(`M${poolIncrement - 2}`).value = 'Días'
          worksheet.mergeCells(`M${poolIncrement - 2}:M${poolIncrement - 1}`)

          worksheet.getCell(`N${poolIncrement - 2}`).style = worksheet.getCell('N7').style
          worksheet.getCell(`N${poolIncrement - 2}`).value = 'Peso'
          worksheet.mergeCells(`N${poolIncrement - 2}:N${poolIncrement - 1}`)

          worksheet.getCell(`O${poolIncrement - 2}`).style = worksheet.getCell('O7').style
          worksheet.getCell(`O${poolIncrement - 2}`).value = { formula: `+E${poolIncrement}-7` }
          worksheet.getCell(`O${poolIncrement - 1}`).style = worksheet.getCell('O8').style
          worksheet.getCell(`O${poolIncrement - 1}`).value = 'Tam'

          worksheet.getCell(`P${poolIncrement - 2}`).style = worksheet.getCell('P7').style
          worksheet.getCell(`P${poolIncrement - 2}`).value = { formula: `+O${poolIncrement - 2}-7` }
          worksheet.getCell(`P${poolIncrement - 1}`).style = worksheet.getCell('P8').style
          worksheet.getCell(`P${poolIncrement - 1}`).value = 'Tam'

          worksheet.getCell(`Q${poolIncrement - 2}`).style = worksheet.getCell('Q7').style
          worksheet.getCell(`Q${poolIncrement - 2}`).value = { formula: `+P${poolIncrement - 2}-7` }
          worksheet.getCell(`Q${poolIncrement - 1}`).style = worksheet.getCell('Q8').style
          worksheet.getCell(`Q${poolIncrement - 1}`).value = 'Tam'

          worksheet.getCell(`R${poolIncrement - 2}`).style = worksheet.getCell('R7').style
          worksheet.getCell(`R${poolIncrement - 2}`).value = { formula: `+Q${poolIncrement - 2}-7` }
          worksheet.getCell(`R${poolIncrement - 1}`).style = worksheet.getCell('R8').style
          worksheet.getCell(`R${poolIncrement - 1}`).value = 'Tam'

          worksheet.getCell(`S${poolIncrement - 2}`).style = worksheet.getCell('S7').style
          worksheet.getCell(`S${poolIncrement - 2}`).value = 'Crec'
          worksheet.getCell(`S${poolIncrement - 1}`).style = worksheet.getCell('S8').style
          worksheet.getCell(`S${poolIncrement - 1}`).value = 'Ult'

          worksheet.getCell(`T${poolIncrement - 2}`).style = worksheet.getCell('T7').style
          worksheet.getCell(`T${poolIncrement - 2}`).value = 'Crec'
          worksheet.getCell(`T${poolIncrement - 1}`).style = worksheet.getCell('T8').style
          worksheet.getCell(`T${poolIncrement - 1}`).value = 'Ant'

          worksheet.getCell(`U${poolIncrement - 2}`).style = worksheet.getCell('U7').style
          worksheet.getCell(`U${poolIncrement - 2}`).value = 'Crec'
          worksheet.getCell(`U${poolIncrement - 1}`).style = worksheet.getCell('U8').style
          worksheet.getCell(`U${poolIncrement - 1}`).value = '4 Ult'

          worksheet.getCell(`V${poolIncrement - 2}`).style = worksheet.getCell('V7').style
          worksheet.getCell(`V${poolIncrement - 2}`).value = 'Crec'
          worksheet.getCell(`V${poolIncrement - 1}`).style = worksheet.getCell('V8').style
          worksheet.getCell(`V${poolIncrement - 1}`).value = 'Gral'

          worksheet.getCell(`W${poolIncrement - 2}`).style = worksheet.getCell('W7').style
          worksheet.getCell(`W${poolIncrement - 2}`).value = 'Kg'
          worksheet.getCell(`W${poolIncrement - 1}`).style = worksheet.getCell('W8').style
          worksheet.getCell(`W${poolIncrement - 1}`).value = 'Alim'

          worksheet.getCell(`X${poolIncrement - 2}`).style = worksheet.getCell('X7').style
          worksheet.getCell(`X${poolIncrement - 2}`).value = 'FC'
          worksheet.mergeCells(`X${poolIncrement - 2}:X${poolIncrement - 1}`)

          worksheet.getCell(`Y${poolIncrement - 2}`).style = worksheet.getCell('Y7').style
          worksheet.getCell(`Y${poolIncrement - 2}`).value = 'FC'
          worksheet.getCell(`Y${poolIncrement - 1}`).style = worksheet.getCell('Y8').style
          worksheet.getCell(`Y${poolIncrement - 1}`).value = 'Prom'

          worksheet.getCell(`Z${poolIncrement - 2}`).style = worksheet.getCell('Z7').style
          worksheet.getCell(`Z${poolIncrement - 2}`).value = 'Peso'
          worksheet.getCell(`Z${poolIncrement - 1}`).style = worksheet.getCell('Z8').style
          worksheet.getCell(`Z${poolIncrement - 1}`).value = 'Prom'

          worksheet.getCell(`AA${poolIncrement - 2}`).style = worksheet.getCell('AA7').style
          worksheet.getCell(`AA${poolIncrement - 2}`).value = 'Crec'
          worksheet.getCell(`AA${poolIncrement - 1}`).style = worksheet.getCell('AA8').style
          worksheet.getCell(`AA${poolIncrement - 1}`).value = 'Prom'

          worksheet.getCell(`AB${poolIncrement - 2}`).style = worksheet.getCell('AB7').style
          worksheet.getCell(`AB${poolIncrement - 2}`).value = '%'
          worksheet.getCell(`AB${poolIncrement - 1}`).style = worksheet.getCell('AB8').style
          worksheet.getCell(`AB${poolIncrement - 1}`).value = 'Sup.'

          worksheet.getCell(`AC${poolIncrement - 2}`).style = worksheet.getCell('AC7').style
          worksheet.getCell(`AC${poolIncrement - 2}`).value = 'Lbs'
          worksheet.getCell(`AC${poolIncrement - 1}`).style = worksheet.getCell('AC8').style
          worksheet.getCell(`AC${poolIncrement - 1}`).value = 'Ha'

          worksheet.getCell(`AD${poolIncrement - 2}`).style = worksheet.getCell('AD7').style
          worksheet.getCell(`AD${poolIncrement - 2}`).value = 'Dens.'
          worksheet.getCell(`AD${poolIncrement - 1}`).style = worksheet.getCell('AD8').style
          worksheet.getCell(`AD${poolIncrement - 1}`).value = 'actual'

          //Alto de celda
          worksheet.getRow(poolIncrement - 1).height = 30

          let firstNumberRowFood, lastNumberRowFood
          foods.forEach((food, foodKey) => { // Recorro cada grupo alimento
            const arrayFoodsIds = JSON.parse(food)
            const foodNames = arrayFoodsIds.map(val => products.find(val2 => val2.id == val).name).join('\n')

            firstNumberRowFood = foodKey == 0 ? foodIncrement : firstNumberRowFood

            // Buscar las piscinas que usaron ese alimento
            const poolsWithFood = element.pools.filter(val => val.products == food)

            // Copiar estilo de celda
            worksheet.getCell(`B${foodIncrement}`).style = worksheet.getCell('B9').style
            // Columna Alimento
            worksheet.getCell(`B${foodIncrement}`).value = foodNames
            // set cell to wrap-text
            worksheet.getCell(`B${foodIncrement}`).alignment = { wrapText: true, vertical: 'middle' }
            worksheet.mergeCells(`B${foodIncrement}:B${foodIncrement + (poolsWithFood.length - 1)}`)


            // Recorrer las piscinas dentro de este alimento
            let firstNumberRowFoodPool, lastNumberRowFoodPool
            poolsWithFood.forEach((pool, poolIndex) => {
              // Guardar el número de la primera fila del alimento
              firstNumberRowFoodPool = poolIndex == 0 ? poolIncrement : firstNumberRowFoodPool
              lastNumberRowFoodPool = poolIndex == 0 ? poolIncrement + (poolsWithFood.length - 1)  : lastNumberRowFoodPool

              // Copiar estilos
              // Copiar estilos de celdas de datos
              worksheet.getCell(`C${poolIncrement}`).style = worksheet.getCell('C9').style
              worksheet.getCell(`D${poolIncrement}`).style = d9Style
              worksheet.getCell(`E${poolIncrement}`).style = e9Style
              worksheet.getCell(`F${poolIncrement}`).style = worksheet.getCell('F9').style
              worksheet.getCell(`G${poolIncrement}`).style = worksheet.getCell('G9').style
              worksheet.getCell(`H${poolIncrement}`).style = worksheet.getCell('H9').style
              worksheet.getCell(`I${poolIncrement}`).style = worksheet.getCell('I9').style
              worksheet.getCell(`J${poolIncrement}`).style = worksheet.getCell('J9').style
              worksheet.getCell(`K${poolIncrement}`).style = worksheet.getCell('K9').style
              worksheet.getCell(`L${poolIncrement}`).style = worksheet.getCell('L9').style
              worksheet.getCell(`M${poolIncrement}`).style = worksheet.getCell('M9').style
              worksheet.getCell(`N${poolIncrement}`).style = worksheet.getCell('N9').style
              worksheet.getCell(`O${poolIncrement}`).style = worksheet.getCell('O9').style
              worksheet.getCell(`P${poolIncrement}`).style = worksheet.getCell('P9').style
              worksheet.getCell(`Q${poolIncrement}`).style = worksheet.getCell('Q9').style
              worksheet.getCell(`R${poolIncrement}`).style = worksheet.getCell('R9').style
              worksheet.getCell(`S${poolIncrement}`).style = worksheet.getCell('S9').style
              worksheet.getCell(`T${poolIncrement}`).style = worksheet.getCell('T9').style
              worksheet.getCell(`U${poolIncrement}`).style = worksheet.getCell('U9').style
              worksheet.getCell(`V${poolIncrement}`).style = worksheet.getCell('V9').style
              worksheet.getCell(`W${poolIncrement}`).style = worksheet.getCell('W9').style
              worksheet.getCell(`X${poolIncrement}`).style = worksheet.getCell('X9').style
              worksheet.getCell(`Y${poolIncrement}`).style = worksheet.getCell('Y9').style
              worksheet.getCell(`Z${poolIncrement}`).style = worksheet.getCell('Z9').style
              worksheet.getCell(`AA${poolIncrement}`).style = worksheet.getCell('AA9').style
              worksheet.getCell(`AB${poolIncrement}`).style = worksheet.getCell('AB9').style
              worksheet.getCell(`AC${poolIncrement}`).style = worksheet.getCell('AC9').style
              worksheet.getCell(`AD${poolIncrement}`).style = worksheet.getCell('AD9').style

              // Columna piscina
              worksheet.getCell(`C${poolIncrement}`).value = pool.pool.description

              // Columna Área
              worksheet.getCell(`D${poolIncrement}`).value = parseFloat(pool.pool.area)

              // Columna Fecha
              worksheet.getCell(`E${poolIncrement}`).value = new Date(pool.seed_date)

              // Columna Densidad
              worksheet.getCell(`F${poolIncrement}`).value = parseFloat(pool.density)

              // Columna #Cam
              worksheet.getCell(`G${poolIncrement}`).value = { formula: `F${poolIncrement}*D${poolIncrement}` }

              // Columna MP
              // worksheet.getCell(`H${poolIncrement}`).value = parseFloat(pool.mp / 100)
              /* let mp = pool.growthReports.find(val => moment(val.date).format('YYYY-MM-DD') == moment(sampling_date).format('YYYY-MM-DD'))
              if (mp == 0) {
                const mpValue = pool.growthReports.find(val => val.mp != 0 && moment(val.date).format('YYYY-MM-DD') < moment(sampling_date).format('YYYY-MM-DD'))
                mp = mpValue ? mpValue.mp : 0
              } */

              let mp = pool.growthReports.find(val => moment(val.date).format('YYYY-MM-DD') == moment(sampling_date).format('YYYY-MM-DD'))
              mp = mp ? mp.mp : 0
              if (mp == 0) {
                const mpValue = pool.growthReports.find(val => val.mp != 0 && moment(val.date).format('YYYY-MM-DD') < moment(sampling_date).format('YYYY-MM-DD'))
                mp = mpValue ? mpValue.mp : 0
              }

              worksheet.getCell(`H${poolIncrement}`).value = (mp / 100)

              // Columna # Cam actual
              worksheet.getCell(`I${poolIncrement}`).value = { formula: `G${poolIncrement}*H${poolIncrement}` }

              // Columna Biomasa inicial
              worksheet.getCell(`J${poolIncrement}`).value = { formula: `G${poolIncrement}*L${poolIncrement}/454` }

              // Columna lbs
              worksheet.getCell(`K${poolIncrement}`).value = { formula: `+G${poolIncrement}*H${poolIncrement}*N${poolIncrement}/454` }

              // Columna Peso siembra
              worksheet.getCell(`L${poolIncrement}`).value = parseFloat(pool.sowing_size)

              // Columna Días
              worksheet.getCell(`M${poolIncrement}`).value = { formula: `$E$6-E${poolIncrement}+1` }

               // Filtrar los crecimientos para mostrar los iguales o menores a la fecha de muestreo
               let growthReports = [...pool.growthReports.filter(val => moment(val.date).format('YYYY-MM-DD') <= moment(sampling_date).format('YYYY-MM-DD'))]


               // Columna Peso. Debe mostrarse el peso(reporte) de la fecha de muestreo
               let lastGrowth = growthReports.find(val => moment(val.date).format('YYYY-MM-DD') == moment(sampling_date).format('YYYY-MM-DD'))
               lastGrowth = lastGrowth ? lastGrowth : {} // Esto no debe ser así
               worksheet.getCell(`N${poolIncrement}`).value = parseFloat(lastGrowth ? lastGrowth.size : 0)

               // Eliminar de los crecimientos el último usado y dejar los no usados todavía
               growthReports = growthReports.filter(val => moment(val.date).format('YYYY-MM-DD') != moment(lastGrowth.date).format('YYYY-MM-DD'))



               if (growthReports.length) { // Si hay penúltimo peso
                 // Columna penúltimo peso
                 worksheet.getCell(`O${poolIncrement}`).value = parseFloat(growthReports[0].size)
               }

               // Eliminar de los crecimientos el último usado y dejar los no usados todavía
               // growthReports = growthReports.splice(0, 1)
               growthReports = growthReports.filter(val => moment(val.date).format('YYYY-MM-DD') != moment(growthReports[0].date).format('YYYY-MM-DD'))


               if (growthReports.length) { // Si hay antepenúltimo peso
                 // Columna antepenúltimo peso
                 worksheet.getCell(`P${poolIncrement}`).value = parseFloat(growthReports[0].size)
               }
               // Eliminar de los crecimientos el último usado y dejar los no usados todavía
               growthReports = growthReports.filter(val => moment(val.date).format('YYYY-MM-DD') != moment(growthReports[0].date).format('YYYY-MM-DD'))

               if (growthReports.length) { // Si hay anteantepenúltimo peso
                 // Columna anteantepenúltimo peso
                 worksheet.getCell(`Q${poolIncrement}`).value = parseFloat(growthReports[0].size)
               }
               // Eliminar de los crecimientos el último usado y dejar los no usados todavía
               growthReports = growthReports.filter(val => moment(val.date).format('YYYY-MM-DD') != moment(growthReports[0].date).format('YYYY-MM-DD'))

               if (growthReports.length) { // Si hay anteanteantepenúltimo peso
                 // Columna anteanteantepenúltimo peso
                 worksheet.getCell(`R${poolIncrement}`).value = parseFloat(growthReports[0].size)
               }
               // Eliminar de los crecimientos el último usado y dejar los no usados todavía
               growthReports = growthReports.filter(val => moment(val.date).format('YYYY-MM-DD') != moment(growthReports[0].date).format('YYYY-MM-DD'))





                // Columna Crec últ
                worksheet.getCell(`S${poolIncrement}`).value = { formula: `+N${poolIncrement}-O${poolIncrement}` }

                // Columna Crec ant
                worksheet.getCell(`T${poolIncrement}`).value = { formula: `+O${poolIncrement}-P${poolIncrement}` }

               // Columna Crec 4 ult
               worksheet.getCell(`U${poolIncrement}`).value = { formula: `+(N${poolIncrement}-Q${poolIncrement})/28*7` }

               // Columna Crec gral
               worksheet.getCell(`V${poolIncrement}`).value = { formula: `+(N${poolIncrement}-L${poolIncrement})/(M${poolIncrement}/7)` }

               // Columna Kg Alim
               // worksheet.getCell(`W${poolIncrement}`).value = parseFloat(pool.kg_alim)

              const kgAlimValue = pool.growthReports.find(val => moment(val.date).format('YYYY-MM-DD') == moment(sampling_date).format('YYYY-MM-DD'))


              worksheet.getCell(`W${poolIncrement}`).value = kgAlimValue ? kgAlimValue.kg_alim : 0

               // Columna FC
               worksheet.getCell(`X${poolIncrement}`).value = { formula: `(W${poolIncrement}*2.2046)/(K${poolIncrement}-(G${poolIncrement}*L${poolIncrement}/454))` }

               // Columna FC prom
               worksheet.getCell(`Y${poolIncrement}`).value = { formula: `(SUM(W${firstNumberRowFoodPool}:W${lastNumberRowFoodPool})*2.2046)/((SUM(K${firstNumberRowFoodPool}:K${lastNumberRowFoodPool})-SUM(J${firstNumberRowFoodPool}:J${lastNumberRowFoodPool})))` }
               if (poolIndex == 0) { // si es la primera piscina hacer el merge de FC prom
                 worksheet.mergeCells(`Y${firstNumberRowFoodPool}:Y${lastNumberRowFoodPool}`)
                 worksheet.mergeCells(`Z${firstNumberRowFoodPool}:Z${lastNumberRowFoodPool}`)
                 worksheet.mergeCells(`AA${firstNumberRowFoodPool}:AA${lastNumberRowFoodPool}`)
                 worksheet.mergeCells(`AB${firstNumberRowFoodPool}:AB${lastNumberRowFoodPool}`)
                 worksheet.mergeCells(`AC${firstNumberRowFoodPool}:AC${lastNumberRowFoodPool}`)
                 worksheet.mergeCells(`AD${firstNumberRowFoodPool}:AD${lastNumberRowFoodPool}`)
               }

               // Columna Peso prom
               worksheet.getCell(`Z${poolIncrement}`).value = { formula: `(SUM(K${firstNumberRowFoodPool}:K${lastNumberRowFoodPool})*454)/SUM(I${firstNumberRowFoodPool}:I${lastNumberRowFoodPool})` }

               // Columna Crec prom
               worksheet.getCell(`AA${poolIncrement}`).value = { formula: `AVERAGE(V${firstNumberRowFoodPool}:V${lastNumberRowFoodPool})` }

               // Columna % sup
               worksheet.getCell(`AB${poolIncrement}`).value = { formula: `+SUM(AE${firstNumberRowFoodPool}:AE${lastNumberRowFoodPool})/SUM(G${firstNumberRowFoodPool}:G${lastNumberRowFoodPool})` }

               // Columna lbs ha
               worksheet.getCell(`AC${poolIncrement}`).value = { formula: `SUM(K${firstNumberRowFoodPool}:K${lastNumberRowFoodPool})/SUM(D${firstNumberRowFoodPool}:D${lastNumberRowFoodPool})` }

               // Columna dens actual
               worksheet.getCell(`AD${poolIncrement}`).value = { formula: `SUM(I${firstNumberRowFoodPool}:I${lastNumberRowFoodPool})/SUM(D${firstNumberRowFoodPool}:D${lastNumberRowFoodPool})/10000` }

               // Columna oculta para cálculos
               worksheet.getCell(`AE${poolIncrement}`).value = { formula: `+G${poolIncrement}*H${poolIncrement}` }

               worksheet.getCell('O7').model.result = undefined
               worksheet.getCell('P7').model.result = undefined
               worksheet.getCell('Q7').model.result = undefined
               worksheet.getCell('R7').model.result = undefined

               // En la última fila de piscinas del grupo debe colocarse el doble borde
              if (poolIndex == (poolsWithFood.length - 1)) {
              }

              poolIncrement++
            })

            // Incrementar según las cantidad de piscinas dentro del alimento
            // foodIncrement++
            foodIncrement += (poolsWithFood.length)
          })

          // Línea de totales
          worksheet.getCell(`D${poolIncrement}`).style = styleRowTotals
          worksheet.getCell(`F${poolIncrement}`).style = styleRowTotalsF
          worksheet.getCell(`G${poolIncrement}`).style = styleRowTotals
          worksheet.getCell(`H${poolIncrement}`).style = styleRowTotalsH
          worksheet.getCell(`I${poolIncrement}`).style = styleRowTotals
          worksheet.getCell(`K${poolIncrement}`).style = styleRowTotals
          worksheet.getCell(`L${poolIncrement}`).style = styleRowTotals
          worksheet.getCell(`M${poolIncrement}`).style = styleRowTotalsM
          worksheet.getCell(`S${poolIncrement}`).style = styleRowTotals
          worksheet.getCell(`T${poolIncrement}`).style = styleRowTotals
          worksheet.getCell(`U${poolIncrement}`).style = styleRowTotals
          worksheet.getCell(`V${poolIncrement}`).style = styleRowTotals
          worksheet.getCell(`W${poolIncrement}`).style = styleRowTotalsW
          worksheet.getCell(`X${poolIncrement}`).style = styleRowTotals

          worksheet.getCell(`D${poolIncrement}`).value = { formula: `SUM(D${firstNumberRowFood}:D${poolIncrement - 1})` }
          worksheet.getCell(`F${poolIncrement}`).value = { formula: `G${poolIncrement}/D${poolIncrement}` }
          worksheet.getCell(`G${poolIncrement}`).value = { formula: `SUM(G${firstNumberRowFood}:G${poolIncrement - 1})` }
          worksheet.getCell(`H${poolIncrement}`).value = { formula: `I${poolIncrement}/G${poolIncrement}` }
          worksheet.getCell(`I${poolIncrement}`).value = { formula: `SUM(I${firstNumberRowFood}:I${poolIncrement - 1})` }
          worksheet.getCell(`K${poolIncrement}`).value = { formula: `SUM(K${firstNumberRowFood}:K${poolIncrement - 1})` }
          worksheet.getCell(`L${poolIncrement}`).value = { formula: `AVERAGE(L${firstNumberRowFood}:L${poolIncrement - 1})` }
          worksheet.getCell(`M${poolIncrement}`).value = { formula: `AVERAGE(M${firstNumberRowFood}:M${poolIncrement - 1})` }
          worksheet.getCell(`S${poolIncrement}`).value = { formula: `AVERAGE(S${firstNumberRowFood}:S${poolIncrement - 1})` }
          worksheet.getCell(`T${poolIncrement}`).value = { formula: `AVERAGE(T${firstNumberRowFood}:T${poolIncrement - 1})` }
          worksheet.getCell(`U${poolIncrement}`).value = { formula: `AVERAGE(U${firstNumberRowFood}:U${poolIncrement - 1})` }
          worksheet.getCell(`V${poolIncrement}`).value = { formula: `AVERAGE(V${firstNumberRowFood}:V${poolIncrement - 1})` }
          worksheet.getCell(`W${poolIncrement}`).value = { formula: `SUM(W${firstNumberRowFood}:W${poolIncrement - 1})` }
          worksheet.getCell(`X${poolIncrement}`).value = { formula: `(W${poolIncrement}*2.2046)/(K${poolIncrement}-(G${poolIncrement}*L${poolIncrement}/454))` }

        }




      })




      return workbook.xlsx.writeFile(Helpers.appRoot('ReporteCompendio.xlsx'))
    })

    response.download(Helpers.appRoot('ReporteCompendio.xlsx'))
  }

}

module.exports = CompendiumController
