'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with pools
 */
const Pool = use('App/Models/Pool')
const PoolImage = use('App/Models/PoolImage')
const CostBenefitProjectionDetail = use('App/Models/CostBenefitProjectionDetail')
const FishingProjection = use('App/Models/FishingProjection')
const FloorAnalysisDetail = use('App/Models/FloorAnalysisDetail')
const FoodProjectionMaster = use('App/Models/FoodProjectionMaster')
const HarvestReport = use('App/Models/HarvestReport')
const Pathology = use('App/Models/Pathology')
const PatologiesAnalysisDetail = use('App/Models/PatologiesAnalysisDetail')
const PhytoplanktonAnalysisDetail = use('App/Models/PhytoplanktonAnalysisDetail')
const ShrimpGrowthReportSize = use('App/Models/ShrimpGrowthReportSize')
const WaterAnalysisDetail = use('App/Models/WaterAnalysisDetail')
const Compendium = use('App/Models/Compendium')
const { validate } = use('Validator')
const Helpers = use("Helpers")
const moment = require('moment')
const fs = use("fs")
class PoolController {
  /**
   * Show a list of all pools.
   * GET pools
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, params }) {
    const pools_shrimps = (await Pool.query().where('shrimp_id', params.shrimp_id).fetch()).toJSON()
    response.send(pools_shrimps)
  }

  /**
   * Create/save a new pool.
   * POST pools
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const images = request.input('images')
    const validation = await validate(request.all(), Pool.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const body = request.only(Pool.fillable)

      body.description = body.description.toUpperCase()
      const pool = await Pool.create(body)
      for (const i of images) {
        if (!Object.keys(i).length) { // Si hay algún campo vacío se ignora
          continue
        }
        await PoolImage.create({
          pool_id: pool.id,
          date: i.date,
          description: i.description,
          filename: i.filename
        })
      }
      response.send(pool)
    }
  }

  /**
   * Display a single pool.
   * GET pools/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    const pool = (await Pool.query().with('images').with(['shrimp.customer']).where('id', params.id).first()).toJSON()
    pool.images.forEach(element => {
      element.date = moment(element.date).format('YYYY-MM-DD')
    });
    response.send(pool)
  }

  /**
   * Update pool details.
   * PUT or PATCH pools/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const images = request.input('images')
    const pool = await Pool.query().where('id', params.id).first()
    if (pool) {
      const validation = await validate(request.all(), Pool.fieldValidationRules())

      if (validation.fails()) {
        response.unprocessableEntity(validation.messages())
      } else {
        const body = request.only(Pool.fillable)
        body.description = body.description.toUpperCase()
        await Pool.query().where('id', params.id).update(body)

        // Insertar nombres de imágenes en tabla
        const imageIds = []
        for (const i of images) {
          if (!Object.keys(i).length) { // Si hay algún campo vacío se ignora
            continue
          }
          if (i.id) { // Si existe el registro de la imagen se modifica
            await PoolImage.query().where('id', i.id).update(i)
            imageIds.push(i.id)
          } else {
            const image = await PoolImage.create({
              pool_id: pool.id,
              date: i.date,
              description: i.description,
              filename: i.filename
            })
            imageIds.push(image.id)
          }
        }

        // Consultar imágenes que no fueron ni modificadas ni insertadas
        const imagesToDelete = (await PoolImage.query().where('pool_id', params.id).whereNotIn('id', imageIds).fetch()).toJSON()
        console.log(imagesToDelete)
        for (const i of imagesToDelete) {
          console.log('imagesToDelete', i)
          // Eliminar imágenes fisicamente
          i.filename = Helpers.appRoot('storage/uploads/') + i.filename.split('-').join('/')
          fs.unlinkSync(i.filename)
        }
        // Eliminar imágenes que no fueron ni modificadas ni insertadas
        await PoolImage.query().where('pool_id', params.id).whereNotIn('id', imageIds).delete()

        response.send(pool)
      }
    } else {
      response.notFound(validation.messages())
    }
  }

  /**
   * Delete a pool with id.
   * DELETE pools/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    // Eliminar piscina y toda su info de la base de datos
    
    await Pool.query().where('id', params.id).delete()
    await CostBenefitProjectionDetail.query().where('pool_id', params.id).delete()
    await FishingProjection.query().where('pool_id', params.id).delete()
    await FloorAnalysisDetail.query().where('pool_id', params.id).delete()
    await FoodProjectionMaster.query().where('pool_id', params.id).delete()
    await HarvestReport.query().where('pool_id', params.id).delete()
    await Pathology.query().where('pool_id', params.id).delete()
    await PatologiesAnalysisDetail.query().where('pool_id', params.id).delete()
    await PhytoplanktonAnalysisDetail.query().where('pool_id', params.id).delete()
    await PoolImage.query().where('pool_id', params.id).delete()
    await ShrimpGrowthReportSize.query().where('pool_id', params.id).delete()
    await WaterAnalysisDetail.query().where('pool_id', params.id).delete()
    await Compendium.query().where('pool_id', params.id).delete()
    response.send(true)
  }

  async uploadPoolImage ({ request, response }) {
    const customerID = request.input('customerID')
    const shrimpId = request.input('shrimpId')
    let dir = `storage/uploads/customers/${customerID}/shrimps/${shrimpId}/pools`
    let showingDir = `customers-${customerID}-shrimps-${shrimpId}-pools`

    const poolImage = request.file('file', {
      types: ['image'],
      size: '8mb'
    })
    let fileName = poolImage.clientName.replace(/\s/g, '')
    console.log('filename', fileName)
    fileName = fileName.split('-').join('')
    console.log('filename2', fileName)
    if (poolImage) {
      await poolImage.move(Helpers.appRoot(dir), {
        name: fileName,
        overwrite: true
      })

      if (!poolImage.moved()) {
        return poolImage.error()
      }
      showingDir += '-' + fileName
      response.send(showingDir)
    }
  }
  async deletePoolImage ({ params, request, response }) {
    await PoolImage.query().where('filename', params.fileName).delete()
    response.send(true)
    // Borrar imagen fisicamente

  }

  async getExistPool ({ request }) {
    let { shrimp_id, description } = request.all()
    description = description.toUpperCase()
    const exists = await Pool.query().where({ shrimp_id, description }).first()

    if (exists) {
      return {
        status: true,
        poolId: exists.id
      }
    }
    return {
      status: false
    }
  }
}

module.exports = PoolController
