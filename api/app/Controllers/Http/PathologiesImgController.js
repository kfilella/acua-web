'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Img = use('App/Models/PathologiesImg')
const moment = use('moment')
/**
 * Resourceful controller for interacting with pathologiesimgs
 */
class PathologiesImgController {
  /**
   * Show a list of all pathologiesimgs.
   * GET pathologiesimgs
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
  }

  /**
   * Render a form to be used for creating a new pathologiesimg.
   * GET pathologiesimgs/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new pathologiesimg.
   * POST pathologiesimgs
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const body = request.only(['analisys_id', 'description', 'date', 'dir'])
    let info = (await Img.query().where({'description': body.description}).fetch()).toJSON()
    if (info.length > 0) {
     const modify = ( await Img.query().where({'description': body.description}).update(body))
     response.send(modify)
    } else {
      const save = await Img.create(body)
      response.send(save)
    }
  }

  /**
   * Display a single pathologiesimg.
   * GET pathologiesimgs/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response }) {
    const date = (await Img.query().where({'analisys_id': params.id}).fetch()).toJSON()
    response.send(date)
  }

  /**
   * Render a form to update an existing pathologiesimg.
   * GET pathologiesimgs/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update pathologiesimg details.
   * PUT or PATCH pathologiesimgs/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a pathologiesimg with id.
   * DELETE pathologiesimgs/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    const info = request.all()
    // console.log((await Img.all()).toJSON())
    const band = (await Img.query().where({'analisys_id': params.id, 'dir': info.filename, 'date': moment(info.date).format('DD-MM-YYYY')}).delete())
    console.log(band, moment(info.date).format('DD-MM-YYYY'))
  }
}

module.exports = PathologiesImgController
