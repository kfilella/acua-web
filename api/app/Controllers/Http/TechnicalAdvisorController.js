'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const TechnicalAdvisor = use('App/Models/TechnicalAdvisor')
const TechnicalChief = use('App/Models/TechnicalChief')
const TechnicalEquipment = use('App/Models/TechnicalEquipment')
const List = use('App/Functions/List')
const { validate } = use('Validator')
/**
 * Resourceful controller for interacting with technicaladvisors
 */
class TechnicalAdvisorController {

  async index ({ request, response, view }) {
    const params = request.get()
    let technical_advisors
    if (Object.keys(params).length) { // si recibe parámetros desde el front se implementa en el where
      technical_advisors = (await TechnicalAdvisor.query().where(params).orderBy('id').fetch()).toJSON()
    } else {
      technical_advisors = (await TechnicalAdvisor.query().orderBy('id').fetch()).toJSON()
    }

    technical_advisors = List.addRowActions(technical_advisors, [{name: 'edit', route: 'technical_advisors/form/', permission: '5.3.5'}, {name: 'delete', permission: '5.3.4'}, { name: 'activateTechnicalAdvisor', permission: '5.3.6' }])
    for (let j in technical_advisors) {
      if (technical_advisors[j].status) {
        technical_advisors[j].estatus = 'Activo'
        technical_advisors[j].actions[2].title = 'Inactivar'
        technical_advisors[j].actions[2].color = 'negative'
      } else {
        technical_advisors[j].estatus = 'Inactivo'
      }
    }
    response.send(technical_advisors)
  }


  async store ({ request, response }) {
    const allRequest = request.all()
    const validation = await validate(allRequest, TechnicalAdvisor.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const body = request.only(TechnicalAdvisor.fillable)
      console.log(body, 'bodyyy')
      body.status = true
      body.zone = JSON.stringify(allRequest.zone)
      const technical_advisors = await TechnicalAdvisor.create(body)
      response.send(technical_advisors);
    }
  }

  async show ({ params, request, response, view }) {
    let technical_advisors = (await TechnicalAdvisor.query().where('id', params.id).first()).toJSON()
    let zone = technical_advisors.zone
    technical_advisors.zone = JSON.parse(zone)
    response.send(technical_advisors)
  }

  async activate ({ request, response, params }) {
    let technical_advisors = await TechnicalAdvisor.query().where('id', params.id).update({
      status: params.status == 'true' ? true : false
    })
    response.send(technical_advisors)
  }

  async activateList ({ request, response, params }) {
    let technical_advisors = (await TechnicalAdvisor.query().where({
      status: params.status == 'true' ? true : false
    }).fetch()).toJSON()
    technical_advisors = List.addRowActions(technical_advisors, [{name: 'edit', route: 'technical_advisors/form/', permission: '5.3.5'}, {name: 'delete', permission: '5.3.4'}, { name: 'activateTechnicalAdvisor', permission: '5.3.6' }])
    for (let j in technical_advisors) {
      if (technical_advisors[j].status) {
        technical_advisors[j].estatus = 'Activo'
        technical_advisors[j].actions[2].title = 'Inactivar'
        technical_advisors[j].actions[2].color = 'negative'
      } else {
        technical_advisors[j].estatus = 'Inactivo'
      }
    }
    response.send(technical_advisors)
  }

  async update ({ params, request, response }) {
    const technical_advisors =  await TechnicalAdvisor.query().where('id', params.id).first()
    const allRequest = request.all()
    if (technical_advisors) {
      const validation = await validate(request.all(), TechnicalAdvisor.fieldValidationRules())

      if (validation.fails()) {
        response.unprocessableEntity(validation.messages())
      } else {
        const body = request.only(TechnicalAdvisor.fillable)
        body.zone = JSON.stringify(allRequest.zone)
        await TechnicalAdvisor.query().where('id', params.id).update(body)
        response.send(technical_advisors);
      }
    } else {
      response.unprocessableEntity(validation.messages()) // Aquí debe ir uno de not found 404
    }
  }


  async destroy ({ params, request, response }) {
    (await TechnicalAdvisor.find(params.id)).delete()
    // Hay que eliminar el asesor de otros lugares donde esté

    const result = await TechnicalEquipment.query().where('technical_advisors', 'like', `%${params.id}%`).fetch()

    const teams = result ? result.toJSON() : []

    for (const team of teams) { // Recorro los equipos donde posiblemente está el asesor eliminado
      const technical_advisors = JSON.parse(team.technical_advisors)

      const index = technical_advisors.indexOf(parseInt(params.id))
      if (index > -1) { // Si fue encontrado lo eliminamos del array y actualizamos el equipo
        technical_advisors.splice(index, 1)
        await TechnicalEquipment.query().where('id', team.id).update({
          technical_advisors: JSON.stringify(technical_advisors)
        })
      }
    }

    response.send('success')
  }
  /**
   * Mostrar los asesores técnicos asignados a determinado jefe técnico
   * GET my_technical_advisors
   *
   * @param auth
   * @param {Response} ctx.response
   */
  async getMyTechnicalAdvisor ({ auth, response }) {
    let technical_advisors
    let user = await auth.getUser()
    const advisorsIds = []
    const technicalChief = (await TechnicalChief.query().with('teams').where('user_id', user.id).first()).toJSON()
    technicalChief.teams.forEach(element => {
      JSON.parse(element.technical_advisors).forEach(element2 => {
        advisorsIds.push(element2)
      })
    })

    technical_advisors = (await TechnicalAdvisor.query().whereIn('id', advisorsIds).fetch()).toJSON()

    
    response.send(technical_advisors)
  }
}

module.exports = TechnicalAdvisorController
