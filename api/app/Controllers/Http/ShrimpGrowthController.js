'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with shrimpgrowths
 */
const ShrimpGrowth = use('App/Models/ShrimpGrowth')
const { validate } = use('Validator')
const Database = use('Database')
const moment = require('moment')
const ExcelJS = require('exceljs')
const Helpers = use('Helpers')
class ShrimpGrowthController {
  /**
   * Show a list of all shrimpgrowths.
   * GET shrimpgrowths
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    const shrimp_id = request.get('shrimp_id').shrimp_id
    const year = request.get('year').year
    console.log({ year })
    const records = await ShrimpGrowth.query().where({shrimp_id}).where('year', year).fetch()
    response.send(records)
  }

  /**
   * Create/save a new shrimpgrowth.
   * POST shrimpgrowths
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const records = request.input('records')
    const shrimpId = request.input('shrimpId')
    for (const i of records) {
      i.shrimp_id = shrimpId
      console.log({ i })
      const validation = await validate(i, ShrimpGrowth.fieldValidationRules())

      if (validation.fails()) {
        // response.unprocessableEntity(validation.messages())
        console.log('error')
        continue
      } else {

        // Guardar o modificar según sea el caso
        const activeIds = []
        if (i.id) { // Si existe modificar
          await ShrimpGrowth.query().where('id', i.id).update(i)
          activeIds.push(i.id)
        } else {
          const shrimpGrowth = await ShrimpGrowth.create(i)
          activeIds.push(shrimpGrowth.id)
        }

      }
    }
    // Eliminar los registros no activos de esa camaronera
    response.send(true)

  }

  /**
   * Display a single shrimpgrowth.
   * GET shrimpgrowths/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Update shrimpgrowth details.
   * PUT or PATCH shrimpgrowths/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a shrimpgrowth with id.
   * DELETE shrimpgrowths/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }

  async getWeeks ({ request }) {
    const shrimpId = request.get('shrimpId').shrimpId
    const year = moment().format('YYYY')
    const years = await Database.select('year')
    .table('shrimp_growths')
    .where('shrimp_id', shrimpId)
    .pluck('year')
    .groupBy('year')

    const currentWeek = moment().format('w')

    const weeks = []

    for (let value = currentWeek; value >= 1; value--) {
      const label = value == currentWeek ? `Semana actual ${value}` :  `Semana ${value}`
      weeks.push({
        label,
        value
      })
    }

    return {
      year,
      years: years.length ? years : [year],
      currentWeek,
      weeks
    }
  }
  async printReport ({ request, response }) {
    const res = request.all()
    let query = ShrimpGrowth.query().with('shrimp').with('food')

    console.log('res.year', res.year)

    if (res.year) { // Si seleccionan año
      query = query.where('year', res.year)
    }
    if (res.shrimpsIds) { // Si seleccionan camaroneras
      query = query.whereIn('shrimp_id', res.shrimpsIds)
    }


    const foods = []
    const records = (await query.orderBy('week', 'desc').fetch()).toJSON()

    // return records

    const foodsIds = [...new Set(records.map(val => val.food_id))]
    foodsIds.forEach(element => {
      console.log({ element })
      /* const food_id = element
      element = {}
      element.food_id = food_id
      element.name = records.find(val => val.food_id = element).food.name */
      foods.push({
        food_id: element,
        name: records.find(val => val.food_id == element).food.name,
        qty: [...new Set(records.filter(val => val.food_id == element).map(val => val.shrimp_id))].length,
        shrimps: [...new Set(records.filter(val => val.food_id == element).map(val => ({
          shrimp_id: val.shrimp_id,
          name: val.shrimp.name
        })).filter((value, index, self) => self.map(x => x.shrimp_id).indexOf(value.shrimp_id) == index))]
      })
    })
    // return foods

    const workbook = new ExcelJS.Workbook()
    await workbook.xlsx.readFile('resources/templates/plantilla_reporte_crecimiento.xlsx').then(function() {
      const worksheet = workbook.getWorksheet('2021')
      worksheet.name = res.year

      // Escribir semanas
      const weeks = [...new Set(records.map(val => val.week))]
      let weekCounter = 3
      console.log({ weeks })
      weeks.forEach(week => {
        worksheet.getCell(`A${weekCounter}`).value = week
        weekCounter++
      })


      let letterNum = 1
      console.log('colName(letterNum)', colName(letterNum))
      // Productos
      foods.forEach(element => {
        worksheet.getCell(`${colName(letterNum)}1`).style = worksheet.getCell('B1').style
        worksheet.getCell(`${colName(letterNum)}1`).value = element.name
        console.log(`${colName(letterNum)}1:${colName(letterNum + element.qty - 1)}1`)
        worksheet.mergeCells(`${colName(letterNum)}1:${colName(letterNum + element.qty - 1)}1`)

        // Recorrer las camaroneras que tiene ese alimento
        element.shrimps.forEach(shrimp => {
          worksheet.getCell(`${colName(letterNum)}2`).style = worksheet.getCell('B2').style
          worksheet.getCell(`${colName(letterNum)}2`).value = shrimp.name
          worksheet.getColumn(`${colName(letterNum)}`).width = shrimp.name.length + 3

          // Recorrer los valores de cada semana
          weekCounter = 3
          weeks.forEach(week => {
            worksheet.getCell(`${colName(letterNum)}${weekCounter}`).style = worksheet.getCell('B3').style
            const incRecord = records.find(val => val.food_id == element.food_id && val.shrimp_id == shrimp.shrimp_id && val.week == week)
            worksheet.getCell(`${colName(letterNum)}${weekCounter}`).value = parseFloat(incRecord ? incRecord.inc : 0)
            weekCounter++
          })

          letterNum++
        })


        // letterNum += element.qty
      })

      // Camaroneras


      return workbook.xlsx.writeFile(`ReporteDeCrecimiento.xlsx`);
    })
    response.download(Helpers.appRoot(`ReporteDeCrecimiento.xlsx`))
  }
}

function colName(n) {
  var ordA = 'a'.charCodeAt(0);
  var ordZ = 'z'.charCodeAt(0);
  var len = ordZ - ordA + 1;

  var s = "";
  while(n >= 0) {
      s = String.fromCharCode(n % len + ordA) + s;
      n = Math.floor(n / len) - 1;
  }
  return s.toLocaleUpperCase();
}

module.exports = ShrimpGrowthController
