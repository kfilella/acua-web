'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with shrimp
 */
const Shrimp = use('App/Models/Shrimp')
const User = use('App/Models/User')
const TechnicalAdvisor = use('App/Models/TechnicalAdvisor')
const Customer = use('App/Models/Customer')
const NodeGeocoder = require('node-geocoder')
const { validate } = use('Validator')
const Env = use('Env')

const options = {
  provider: 'google',
 
  // Optional depending on the providers
  // fetch: customFetchImplementation,
  apiKey: Env.get('googleApiKey'), // for Mapquest, OpenCage, Google Premier
  formatter: null // 'gpx', 'string', ...
};
 
const geocoder = NodeGeocoder(options);
class ShrimpController {
  /**
   * Mostrar lista de camaroneras
   * GET shrimps
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ auth, response, view }) {
    const userLogged = await auth.getUser()
    const user = (await User.query().with('roles').where('id', userLogged.id).first()).toJSON()
    const isTechnicalAdvisor = user.roles.find(val => val.id === 3)
    const isTechnicalChief= user.roles.find(val => val.id === 2)

    let shrimps = []

    if (isTechnicalAdvisor) { // Si es asesor técnico debe ver las camaroneras que tiene asignadas
      const technicalAdvisor = (await TechnicalAdvisor.query().with(['assignedShrimps.shrimp.customer'])
      .with(['assignedShrimps.shrimp.zone'])
      .with('assignedShrimps.shrimp.assignedContacts.contact')
      .with('assignedShrimps.shrimp.pools')
      .with('assignedShrimps.shrimp.state')
      .where('user_id', user.id).first()).toJSON()
      technicalAdvisor.assignedShrimps.forEach(element => {
        if (element.shrimp) {
          element.shrimp.contacts = element.shrimp.assignedContacts.filter(val => {
            return val.contact
          }).map(val => val.contact)
      
          element.shrimp.contacts.forEach(val => {
            val.full_name = val.full_name = `${val.name} ${val.last_name}`
          })

          shrimps.push(element.shrimp)
        }
      })
    } else { // Si es jefe técnico o admin debe verlas todas
      shrimps = (await Shrimp.query().with('customer').with('pools').with('zone').with('assignedContacts.contact').with('state').fetch()).toJSON()
      shrimps.forEach(element => {
        element.contacts = element.assignedContacts.filter(val => {
          return val.contact
        }).map(val => val.contact)

        element.contacts.forEach(val => {
          val.full_name = val.full_name = `${val.name} ${val.last_name}`
        })
      })

    }

    response.send(shrimps)
  }

  async indexByCustomer ({ auth, response, params }) {
    let shrimps = []
    if (params.customer_id == 0) {
      const logguedUSer = await auth.getUser()
      const customer = await Customer.query().where('user_id', logguedUSer.id).first()
      shrimps = (await Shrimp.query().where('customer_id', customer.id).fetch()).toJSON()
    } else {
      shrimps = (await Shrimp.query().where('customer_id', params.customer_id).fetch()).toJSON()
    }
    
    if (shrimps.length > 0) {
      var map_shrimps = shrimps.map(v => {
        return {
          customer_id: v.customer_id,
          id: v.id,
          name: v.name
        }
      })
    } else { var map_shrimps = [] }

    response.send(map_shrimps)
  }

  /**
   * Create/save a new shrimp.
   * POST shrimp
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const validation = await validate(request.all(), Shrimp.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const body = request.only(Shrimp.fillable)
      const shrimp = await Shrimp.create(body)
      response.send(shrimp);
    }
  }

  /**
   * Display a single shrimp.
   * GET shrimp/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    const shrimp = (await Shrimp.query().with('pools.images').with('zone').with('assignedContacts.contact')
    .with('pools', (query) => {
      query.orderBy('description')
    })
    .where('id', params.id).first()).toJSON()
    shrimp.pools.forEach(element => {
      element.expanded = false
    })
    shrimp.contacts = shrimp.assignedContacts.filter(val => {
      return val.contact
    }).map(val => val.contact)

    shrimp.contacts.forEach(val => {
      val.full_name = val.full_name = `${val.name} ${val.last_name}`
    })
    response.send(shrimp)
  }

  /**
   * Update shrimp details.
   * PUT or PATCH shrimp/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const shrimp = await Shrimp.query().where('id', params.id).first()
    if (shrimp) {
      const validation = await validate(request.all(), Shrimp.fieldValidationRules())

      if (validation.fails()) {
        response.unprocessableEntity(validation.messages())
      } else {
        const body = request.only(Shrimp.fillable)

        await Shrimp.query().where('id', params.id).update(body)
        response.send(shrimp)
      }
    } else {
      response.notFound(validation.messages())
    }
  }

  /**
   * Delete a shrimp with id.
   * DELETE shrimp/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    // Al elimininar camaronera se deben eliminar todas las referencias a esa camaronera en la base de datos
    // La eliminación debe ser soft
    response.send(await Shrimp.query().where('id', params.id).delete())
  }

  async getCoordinates ({ request }) {
    const { address, country } = request.get()
    const res = await geocoder.geocode({
      address: address ? address : 'Ecuador',
      country
    })
    const coordinates = {
      lat: res[0].latitude,
      lng: res[0].longitude
    }
    return coordinates
  }

  async getAssignableShrimps ({ request, response }) {
    const { text } = request.get()
    let query = Customer.query().with('shrimps')

    if (text) { // It's looking in customer and shrimps
      query = query.where(function() {
        this.whereRaw('UPPER(legal_name) like ?', [`%${text.toUpperCase()}%`])
        .orWhereRaw('UPPER(comercial_name) like ?', [`%${text.toUpperCase()}%`])
        .orWhereHas('shrimps', builder => {
          // Filter customer
          builder.whereRaw('UPPER(name) like ?', [ `%${text.toUpperCase()}%` ])
        })
      })
    }
    query = query.whereHas('shrimps')

    const allCustomers = (await query.limit(8).fetch()).toJSON()
    const customers = allCustomers

    customers.forEach(element => {
      element.legal_name_e = element.legal_name.length > 15 ? element.legal_name.substr(0, 16) + '...' : element.legal_name
    })

    response.send(customers)
  }
}

module.exports = ShrimpController
