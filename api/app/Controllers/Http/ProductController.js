'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Helpers = use('Helpers')
const fs = require('fs')

const ProductCategory = use('App/Models/ProductCategory')
const MeasureUnit = use('App/Models/MeasureUnit')
const Product = use('App/Models/Product')
const ProductImage = use('App/Models/ProductImage')
const ProviderProductPrice = use('App/Models/ProviderProductPrice')
const Provider = use('App/Models/Provider')
const ProductPriceHistory = use('App/Models/ProductPriceHistory')
const Color = use('App/Models/Color')
const List = use('App/Functions/List')
const { validate } = use('Validator')

/**
 * Resourceful controller for interacting with products
 */
class ProductController {
  /**
   * Show a list of all products.
   * GET products
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    let products = (await Product.query().orderBy('from_weight').fetch()).toJSON()
    products.forEach(element => {
      element.range = element.from_weight + ' - ' + element.to_weight
    })
    products = List.addRowActions(products, [{name: 'edit', route: 'products/form/', permission: '4.5'}, {name: 'delete', permission: '4.4'}])
    response.send(products)
  }

  async indexColors ({ request, response, view }) {
    let colors = (await Color.all()).toJSON()
    response.send(colors)
  }

  /**
   * Create/save a new product.
   * POST products
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const validation = await validate(request.all(), Product.fieldValidationRules())
    let date = request.all()
    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      let band = true
      let verify = (await Product.all()).toJSON()
      let temp = []
      for (let i in verify) { // Ingreso todos loa valores de los rangos dentro de un array
        for (let k = (parseFloat(verify[i].from_weight)); k <= parseFloat(verify[i].to_weight); k= k+ 0.001) {
          temp.push(k.toFixed(4))
        }
      }
      for (let i = (parseFloat(date.from_weight)); i <=  parseFloat(date.to_weight); i= i +0.001) { // Recorro lo traido y verifico que nungun numero este dentro de los ingresados anteriormente
        // console.log(i.toFixed(2), i)
        if (temp.includes(i.toFixed(4))) {
          console.log('Coinsiden en ' + i)
          band = false
          break
        }
      }
      if (band) { // verifica si ya estan esos rangos guardados
        const body = request.only(Product.fillable)
        const product = await Product.create(body)
        response.send({
          banderita: false,
        })
      } else {
        return response.status(200).send({
          banderita: true,
          message: 'Ya existe un alimento con esos Rangos',
          icon: 'warning',
          color: 'negative'
        })
      }
      /* const wholeRequest = request.all()
      const body = request.only(Product.fillable)
      const product = await Product.create(body)
      // se procede a crear el historico de precios de productos por proveedor
      for(let i of wholeRequest.providers_product_price) {
        await ProviderProductPrice.create({
          product_id: product.id,
          provider_id: i.id,
          price: i.price
        })
      } */
      // se crea un historico de precios de productos
      /* await ProductPriceHistory.create({
        product_id: product.id,
        price: product.last_price_purchase
      })

      response.send(product) */
    }
  }

  /**
   * Display a single product.
   * GET products/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    const productId = params.id
    const product = (await Product.query().with('history_product_price').with('providers_product_price').with('list_img').where('id', productId).first()).toJSON()
    if (product) {
      for (let i of product.providers_product_price) {
        var provider = (await Provider.query().with('history_providers_product_price').where('id', i.provider_id).first()).toJSON()
        i.legal_name = provider.legal_name
        i.history_providers_product_price = provider.history_providers_product_price
      }
    }

    response.send(product)
  }

  /**
   * Render a form to update an existing product.
   * GET products/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update product details.
   * PUT or PATCH products/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const req = request.all()
    const validation = await validate(req, Product.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const band = await Product.query().where('id', '!=', req.id)
      .where(function() {
        this.whereBetween('from_weight', [req.from_weight, req.to_weight])
        .orWhereBetween('to_weight', [req.from_weight, req.to_weight])
      }).first()
      
      if (!band) { // Si no hay choque con los rangos
        const wholeRequest = request.all()
        const body = request.only(Product.fillable)
        const productId = params.id
        const lastProduct = (await Product.query().where('id', productId).first()).toJSON()
        const product = await Product.query().where('id', productId).update(body)

        var productDeleteProviders = (await ProviderProductPrice.query().where('product_id', productId).fetch()).toJSON()
        if (wholeRequest.providers_product_price.length === 0) {
          if (productDeleteProviders.length > 0) {
            for (let i of productDeleteProviders) {
              await ProviderProductPrice.query().where('id', i.id).delete()
            }
          }
        } else {
          const noId = wholeRequest.providers_product_price.filter(ele => !ele.id)
          const hasId =  wholeRequest.providers_product_price.filter(ele => ele.id)
          // se procede a crear el historico de precios de productos por proveedor
          for (let i in noId) {
            await ProviderProductPrice.create({
              product_id: productId,
              provider_id: i.provider_id,
              price: i.price
            })
          }

          // verificar que los datos en request sean iguales que en la tabla, y marcar cuales deben de eliminarse
          for(let i of hasId) {
            productDeleteProviders.map(ele => {
              if (i.id === ele.id) {
                ele.delete = false
              } else {
                ele.delete = true
              }
              return ele
            })
          }
          // proceder a eliminar o actualizar registros
          for (let i of productDeleteProviders) {
            if (i.delete === true) {
              await ProviderProductPrice.query().where('id', i.id).delete()
            } else {
              //si ya existe un precio por proveedor, se actualiza los datos caso contrario se crea un nuevo registro
              const dataUpdate = { product_id: i.product_id, provider_id: i.provider_id, price: i.price }
              await ProviderProductPrice.query().where('id', i.id).update(dataUpdate)
            }
          }

          // se Procede a verificar imagenes del producto, si se procede a eliminar
          for (let i of wholeRequest.list_img) {
            if (i.is_delete) {
              await ProductImage.query().where('id', i.id).delete()
              let dir = i.filename.split('-').join('/')
              fs.unlinkSync(Helpers.appRoot('storage/uploads') + `/${dir}`)
            }
          }

          // si el precio es diferente, se crea un historico de precios de productos
          if (lastProduct.last_price_purchase !== body.last_price_purchase) {
            await ProductPriceHistory.create({
              product_id: productId,
              price: body.last_price_purchase
            })
          }
        }
      response.send(product);
    } else {
      return response.status(200).send({
        banderita: true,
        message: 'Ya existe un alimento con esos Rangos',
        icon: 'warning',
        color: 'negative'
      })
    }
  }
}

  /**
   * Delete a product with id.
   * DELETE products/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    response.send(await Product.query().where('id', params.id).delete())
  }

  async uploadProductImg ({ request }) {
    const producId = request.input('product_id')
    // const productImgId = request.input('product_image_id')
    let dir = `storage/uploads/products/${producId}/images`
    let showingDir = `products-${producId}-images`
    const productImgFile = request.file('file', {
      types: ['image'],
      size: '8mb'
    })
    if (productImgFile) {
      await productImgFile.move(Helpers.appRoot(dir), {
        name: productImgFile.clientName.split('-').join('_'),
        overwrite: true
      })

      if (!productImgFile.moved()) {
        return productImgFile.error()
      }

      // Después de guardado el archivo se realiza el guardado de los datos en base de datos
      /* const productImages = await ProductImage.query().where('id', productImgId).first()
      if (productImages) {
        productImages.filename = showingDir + '-' + productImgFile.fileName
      } else { */
        await ProductImage.create({
          product_id: producId,
          filename: showingDir + '-' + productImgFile.fileName
        })
      // }

    }
  }
}

module.exports = ProductController
