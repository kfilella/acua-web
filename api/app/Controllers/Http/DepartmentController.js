'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with departments
 */
const Department = use('App/Models/Department')
const Section = use('App/Models/Section')
const List = use('App/Functions/List')
const { validate } = use('Validator')
class DepartmentController {
  /**
   * Show a list of all departments.
   * GET departments
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response }) {
    const notAssigned = (request.get()).notAssigned
    let departments
    if (notAssigned) { // si se quieren los departamentos que no están asignados a ninguna área
      departments = (await Department.query().with('area').where('area_id', null).fetch()).toJSON()
    } else {
      departments = (await Department.query().with('area').fetch()).toJSON()
    }
    
    departments = List.addRowActions(departments, [{ name: 'edit', route: 'departments/form/', permission: '1.9.3' }, { name: 'delete', permission: '1.9.4' }])
    response.send(departments)
  }

  /**
   * Create/save a new department.
   * POST departments
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const allRequest = request.all()
    const validation = await validate(request.all(), Department.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const body = request.only(Department.fillable)
      const department = await Department.create(body)
      if (allRequest.sections) {
        for (const i of allRequest.sections) {
          await Section.query().where('id', i).update({
            department_id: department.id
          })
        }
      }
      response.send(department);
    }
  }

  /**
   * Display a single department.
   * GET departments/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    const department = (await Department.query().with('sections').where('id', params.id).first()).toJSON()

    department.sectionIds = department.sections.map(value => value.id)
    response.send(department)
  }

  /**
   * Update department details.
   * PUT or PATCH departments/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const department = await Department.query().where('id', params.id).first()
    const allRequest = request.all()
    if (department) {
      const validation = await validate(request.all(), Department.fieldValidationRules())

      if (validation.fails()) {
        response.unprocessableEntity(validation.messages())
      } else {
        const body = request.only(Department.fillable)

        await Department.query().where('id', params.id).update(body)
        // Eliminar secciones de ese departamento para añadir los nuevos en la actualización
        await Section.query().where('department_id', params.id).update({
          department_id: null
        })
        // Modificación de secciones en este departamento
        for (const i of allRequest.sections) {
          await Section.query().where('id', i).update({
            department_id: params.id
          })
        }
        response.send(department)
      }
    } else {
      response.notFound(validation.messages())
    }
  }

  /**
   * Delete a department with id.
   * DELETE departments/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, response }) {
    const department = await Department.find(params.id)
    department.delete()
    response.send('success')
  }
}

module.exports = DepartmentController
