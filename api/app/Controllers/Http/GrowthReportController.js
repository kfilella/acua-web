'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with growthreports
 */
const { validate } = use('Validator')
const GrowthReport = use('App/Models/GrowthReport')
class GrowthReportController {
  /**
   * Show a list of all growthreports.
   * GET growthreports
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
  }

  /**
   * Create/save a new growthreport.
   * POST growth_reports
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const validation = await validate(request.all(), GrowthReport.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const body = request.only(GrowthReport.fillable)
      const growthReport = await GrowthReport.create(body)
      response.send(growthReport)
    }
  }

  /**
   * Display a single growthreport.
   * GET growthreports/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Update growthreport details.
   * PUT or PATCH growthreports/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const growthReport = await GrowthReport.query().where('id', params.id).first()
    if (growthReport) {
      const validation = await validate(request.all(), GrowthReport.fieldValidationRules())

      if (validation.fails()) {
        response.unprocessableEntity(validation.messages())
      } else {
        const body = request.only(GrowthReport.fillable)

        await GrowthReport.query().where('id', params.id).update(body)
        response.send(growthReport)
      }
    } else {
      response.notFound(validation.messages())
    }
  }

  /**
   * Delete a growthreport with id.
   * DELETE growthreports/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = GrowthReportController
