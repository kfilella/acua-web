'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with areas
 */
const Area = use('App/Models/Area')
const Department = use('App/Models/Department')
const List = use('App/Functions/List')
const { validate } = use('Validator')
class AreaController {
  /**
   * Show a list of all areas.
   * GET areas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ response }) {
    let areas = (await Area.all()).toJSON()
    areas = List.addRowActions(areas, [{ name: 'edit', route: 'areas/form/', permission: '1.8.3' }, { name: 'delete', permission: '1.8.4' }])
    response.send(areas)
  }

  /**
   * Create/save a new area.
   * POST areas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const allRequest = request.all()
    const validation = await validate(request.all(), Area.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const body = request.only(Area.fillable)
      const area = await Area.create(body)
      for (const i of allRequest.departments) {
        await Department.query().where('id', i).update({
          area_id: area.id
        })
      }

      response.send(area);
    }
  }

  /**
   * Display a single area.
   * GET areas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, response }) {
    const area = (await Area.query().with('departments').where('id', params.id).first()).toJSON()
    area.departmentIds = area.departments.map(value => value.id)
    response.send(area)
  }
  
  /**
   * Update area details.
   * PUT or PATCH areas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const area = await Area.query().where('id', params.id).first()
    const allRequest = request.all()
    if (area) {
      const validation = await validate(request.all(), Area.fieldValidationRules())

      if (validation.fails()) {
        response.unprocessableEntity(validation.messages())
      } else {
        const body = request.only(Area.fillable)

        await Area.query().where('id', params.id).update(body)
        // Eliminar departamentos de esa área para añadir los nuevos en la actualización
        await Department.query().where('area_id', params.id).update({
          area_id: null
        })
        // Modificación de departamentos en esta área
        for (const i of allRequest.departments) {
          await Department.query().where('id', i).update({
            area_id: params.id
          })
        }

        response.send(area)
      }
    } else {
      response.notFound(validation.messages())
    }
  }

  /**
   * Delete a area with id.
   * DELETE areas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, response }) {
    const area = await Area.find(params.id)
    area.delete()
    response.send('success')
  }
}

module.exports = AreaController
