'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with ratingcompanies
 */
const RatingCompany = use('App/Models/RatingCompany')
const List = use('App/Functions/List')
const { validate } = use('Validator')
class RatingCompanyController {
  /**
   * Show a list of all ratingcompanies.
   * GET ratingcompanies
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ response }) {
    let ratingCompanies = (await RatingCompany.all()).toJSON()
    ratingCompanies = List.addRowActions(ratingCompanies, [{ name: 'edit', route: 'rating_companies/form/', permission: '1.5.1.1.3' }, { name: 'delete', permission: '1.5.1.1.5' }])
    response.send(ratingCompanies)
  }

  /**
   * Create/save a new ratingcompany.
   * POST ratingcompanies
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const validation = await validate(request.all(), RatingCompany.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const body = request.only(RatingCompany.fillable)
      const ratingCompany = await RatingCompany.create(body)
      response.send(ratingCompany);
    }
  }

  /**
   * Display a single ratingcompany.
   * GET ratingcompanies/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, response }) {
    const ratingCompany = (await RatingCompany.query().where('id', params.id).first()).toJSON()
    response.send(ratingCompany)
  }

  /**
   * Update ratingcompany details.
   * PUT or PATCH ratingcompanies/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const ratingCompany = await RatingCompany.query().where('id', params.id).first()
    if (ratingCompany) {
      const validation = await validate(request.all(), RatingCompany.fieldValidationRules())

      if (validation.fails()) {
        response.unprocessableEntity(validation.messages())
      } else {
        const body = request.only(RatingCompany.fillable)

        await RatingCompany.query().where('id', params.id).update(body)
        response.send(ratingCompany)
      }
    } else {
      response.notFound(validation.messages())
    }
  }

  /**
   * Delete a ratingcompany with id.
   * DELETE ratingcompanies/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    const ratingCompany = await RatingCompany.find(params.id)
    try {
      await ratingCompany.delete()
      response.send('success')
    } catch (error) {
      response.forbidden('La empresa calificadora está siendo usada. No se puede eliminar')
    }
  }
}

module.exports = RatingCompanyController
