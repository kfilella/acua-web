'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with wateranalyses
 */
const moment = require('moment')
const WaterAnalysisMaster = use('App/Models/WaterAnalysisPool')
const WaterAnalysisDetail = use('App/Models/WaterAnalysisDetail')
const Shrimp = use('App/Models/Shrimp')
const Contact = use('App/Models/Contact')
const List = use('App/Functions/List')
const { validate } = use('Validator')
const Email = use("App/Functions/Email")
const Helpers = use("Helpers")
class WaterAnalysisController {
  /**
   * Show a list of all wateranalyses.
   * GET wateranalyses
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    const params = request.get()
    const shrimp = await Shrimp.find(params.shrimp_id)
    let water_analysis = (await WaterAnalysisMaster.query().with('user.technical_advisor').with('zone').with('pools.pool')
    .with('shrimp.customer.contacts').where(params).orderBy('created_at', 'desc').fetch()).toJSON()
    water_analysis.forEach(element => {
      if (element.applicant_id == 0) {
        element.applicant = 'Cliente'
      } else {
        let contact =  (element.shrimp.customer.contacts.find(val => val.id === element.applicant_id))
        element.applicant = contact ? `${contact.name} ${contact.last_name}` : 'Contacto eliminado'
      }
      element.analysis_date = moment(element.analysis_date).format("DD-MM-YYYY")
      // En cada análisis mostrar en un campo las piscinas del análisis. Filtro primero para evitar errores si se han eliminado piscinas
      element.pools = element.pools.filter(val => val.pool).map(val => val.pool.description).join(',')
    })
    
    water_analysis = List.addRowActions(water_analysis, [
      { name: 'show', route: `/customers/show/${shrimp.customer_id}/shrimps/${params.shrimp_id}/water/form/`, permission: '2.1.1.1.18' },
      /* { name: 'sendAnalysisEmail', permission: '2.1.1.1.17' },*/
      { name: 'printWaterAnalysis', permission: '2.1.1.1.30' },
      { name: 'generateLinkWaterAnalysis', permission: '2.1.1.1.30' },
      { name: 'sendMailWaterAnalysis', permission: '2.1.1.1.17', route: 'water/send_mail/'}
    ])
    response.send(water_analysis)
  }

  /**
   * Create/save a new wateranalysis.
   * POST wateranalyses
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ auth, request, response }) {
    const user = await auth.getUser()
    const wholeRequest = request.all()
    const pools = wholeRequest.pools
    wholeRequest.technical_advisor_id = user.id
    const validation = await validate(wholeRequest, WaterAnalysisMaster.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const body = request.only(WaterAnalysisMaster.fillable)
      const waterAnalysis = await WaterAnalysisMaster.create(body)
      // return pools

      // Guardar detalles de piscinas
      for (const i of pools) {
        delete i.description
        delete i.expanded
        delete i.area
        i.water_analysis_master_id = waterAnalysis.id
        await WaterAnalysisDetail.create(i)
      }
      
      response.send(waterAnalysis)
    }
  }

  /**
   * Display a single wateranalysis.
   * GET wateranalyses/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    const waterAnalysis = (await WaterAnalysisMaster.query().with('pools.pool').where('id', params.id).first()).toJSON()
    waterAnalysis.delivery_date = moment(waterAnalysis.delivery_date).format("YYYY-MM-DD")
    waterAnalysis.analysis_date = moment(waterAnalysis.analysis_date).format("YYYY-MM-DD")
    waterAnalysis.pools.forEach(element => {
      element.description = element.pool.description
      element.expanded = true
      element.alkalinity = parseInt(element.alkalinity)
      element.salinity = parseInt(element.salinity)
      element.dissolved = parseInt(element.dissolved)
      element.turbidity = parseInt(element.turbidity)
    })
    response.send(waterAnalysis)
  }

  /**
   * Update wateranalysis details.
   * PUT or PATCH wateranalyses/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const wholeRequest = request.all()
    const pools = wholeRequest.pools

    const waterAnalysis = await WaterAnalysisMaster.query().where('id', params.id).first()
    if (waterAnalysis) {
      const validation = await validate(request.all(), WaterAnalysisMaster.fieldValidationRules())

      if (validation.fails()) {
        response.unprocessableEntity(validation.messages())
      } else {
        const body = request.only(WaterAnalysisMaster.fillable)

        await WaterAnalysisMaster.query().where('id', params.id).update(body)

        //Actualizar datos de piscinas
        const poolIds = []
        for (const i of pools) {
          delete i.pool
          delete i.description
          delete i.expanded
          delete i.area
          if (i.id) {
            // Eliminar valores vacíos de objeto
            for (let propName in i) { 
              if (i[propName] === '') {
                i[propName] = null
              }
            }
            await WaterAnalysisDetail.query().where('id', i.id).update(i)
            poolIds.push(i.id)
          } else {
            i.water_analysis_master_id = params.id
            let detailId = await WaterAnalysisDetail.create(i)
            poolIds.push(detailId.id)
          }
        }
        await WaterAnalysisDetail.query().where('water_analysis_master_id', params.id).whereNotIn('id', poolIds).delete()
        response.send(waterAnalysis)
      }
    } else {
      response.notFound(validation.messages())
    }
  }

  /**
   * Delete a wateranalysis with id.
   * DELETE wateranalyses/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }

  async sendMailInfo ({ request, response, params }) {
    const analysisId = params.analysisId

    const analysis = (await WaterAnalysisMaster.query().with('applicant').with('shrimp.customer.contacts').where('id', analysisId).first()).toJSON()

    const form_emails = {
      to: analysis.applicant ? analysis.applicant.email : 'elyohan14@gmail.com', // Si no hay aplicante es porque lo eliminaron
      contacts_email: analysis.shrimp.customer.contacts.map(val => (
        {
          email: val.email || 'Sin correo',
          name: `${val.name} ${val.last_name}`,
          disable: val.email == null || val.email == ''
        }
      )),
      email_customer: analysis.shrimp.customer.electronic_documents_email
    }
    response.send(form_emails)
  }

  async sendMail ({ request, response, params }) {
    const { to, cc, content } = request.all()
    const analysisId = params.analysisId
    const filename = `Analisis_de_aguas_${analysisId}.pdf`

    await Email.send({
      to,
      subject: 'Análisis de aguas',
      content,
      cc,
      attachments: [
        {
          filename,
          path: Helpers.appRoot('storage/uploads/analysis') + `/${filename}`
        }
      ]
    })
    response.send(true)
  }
}

module.exports = WaterAnalysisController
