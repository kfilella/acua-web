'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with flooranalyses
 */
const moment = require('moment')
const FloorAnalysisMaster = use('App/Models/FloorAnalysisPool')
const FloorAnalysisDetail = use('App/Models/FloorAnalysisDetail')
const Shrimp = use('App/Models/Shrimp')
const Contact = use('App/Models/Contact')
const List = use('App/Functions/List')
const { validate } = use('Validator')
const Email = use("App/Functions/Email")
const Helpers = use("Helpers")

class FloorAnalysisController {
  /**
   * Show a list of all flooranalyses.
   * GET flooranalyses
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    const params = request.get()
    const shrimp = await Shrimp.find(params.shrimp_id)
    let floor_analysis = (await FloorAnalysisMaster.query().with('user.technical_advisor').with('zone').with('pools.pool')
    .with('shrimp.customer.contacts').with('shrimp.zone').where(params).orderBy('created_at', 'desc').fetch()).toJSON()
    floor_analysis.forEach(element => {
      if (element.applicant_id == 0) {
        element.applicant = 'Cliente'
      } else {
        let contact =  (element.shrimp.customer.contacts.find(val => val.id === element.applicant_id))
        element.applicant = contact ? `${contact.name} ${contact.last_name}` : 'Contacto eliminado'
      }
      element.analysis_date = moment(element.analysis_date).format("DD-MM-YYYY")
      // En cada análisis mostrar en un campo las piscinas del análisis. Filtro primero para evitar errores si se han eliminado piscinas
      element.pools = element.pools.filter(val => val.pool).map(val => val.pool.description).join(',')
    })

    floor_analysis = List.addRowActions(floor_analysis, [
      { name: 'show', route: `/customers/show/${shrimp.customer_id}/shrimps/${params.shrimp_id}/floor/form/`, permission: '2.1.1.1.25' },
      /* { name: 'sendFloorAnalysisEmail', permission: '2.1.1.1.26' },*/
      { name: 'printFloorAnalysis', permission: '2.1.1.1.31' },
      { name: 'generateLinkFloorAnalysis', permission: '2.1.1.1.31' },
      { name: 'sendMailFloorAnalysis', permission: '2.1.1.1.26', route: 'floor/send_mail/'}
    ])
    response.send(floor_analysis)
  }

  /**
   * Create/save a new flooranalysis.
   * POST flooranalyses
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ auth, request, response }) {
    const user = await auth.getUser()
    const wholeRequest = request.all()
    const pools = wholeRequest.pools
    wholeRequest.technical_advisor_id = user.id
    const validation = await validate(wholeRequest, FloorAnalysisMaster.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const body = request.only(FloorAnalysisMaster.fillable)
      const floorAnalysis = await FloorAnalysisMaster.create(body)

      // Guardar detalles de piscinas
      for (const i of pools) {
        delete i.description
        delete i.expanded
        delete i.area
        i.floor_analysis_master_id = floorAnalysis.id
        await FloorAnalysisDetail.create(i)
      }

      response.send(floorAnalysis)
    }
  }

  /**
   * Display a single flooranalysis.
   * GET flooranalyses/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    const floorAnalysis = (await FloorAnalysisMaster.query().with('pools.pool').where('id', params.id).first()).toJSON()
    floorAnalysis.delivery_date = moment(floorAnalysis.delivery_date).format("YYYY-MM-DD")
    floorAnalysis.analysis_date = moment(floorAnalysis.analysis_date).format("YYYY-MM-DD")
    floorAnalysis.sampling_reception_date = floorAnalysis.sampling_reception_date ? moment(floorAnalysis.sampling_reception_date).format("YYYY-MM-DD") : undefined
    floorAnalysis.pools.forEach(element => {
      element.description = element.pool.description
      element.expanded = true
    })
    response.send(floorAnalysis)
  }

  /**
   * Update flooranalysis details.
   * PUT or PATCH flooranalyses/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const wholeRequest = request.all()
    const pools = wholeRequest.pools

    const floorAnalysis = await FloorAnalysisMaster.query().where('id', params.id).first()
    if (floorAnalysis) {
      const validation = await validate(request.all(), FloorAnalysisMaster.fieldValidationRules())

      if (validation.fails()) {
        response.unprocessableEntity(validation.messages())
      } else {
        const body = request.only(FloorAnalysisMaster.fillable)

        await FloorAnalysisMaster.query().where('id', params.id).update(body)

        //Actualizar datos de piscinas
        const poolIds = []
        for (const i of pools) {
          delete i.pool
          delete i.description
          delete i.expanded
          delete i.area
          if (i.id) {
            // Eliminar valores vacíos de objeto
            for (let propName in i) {
              if (i[propName] === '') {
                i[propName] = null
              }
            }
            await FloorAnalysisDetail.query().where('id', i.id).update(i)
            poolIds.push(i.id)
          } else {
            i.floor_analysis_master_id = params.id
            let detailId = await FloorAnalysisDetail.create(i)
            poolIds.push(detailId.id)
          }
        }

        await FloorAnalysisDetail.query().where('floor_analysis_master_id', params.id).whereNotIn('id', poolIds).delete()
        response.send(floorAnalysis)
      }
    } else {
      response.notFound(validation.messages())
    }
  }

  /**
   * Delete a flooranalysis with id.
   * DELETE flooranalyses/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }

  async sendMailInfo ({ request, response, params }) {
    const analysisId = params.analysisId

    const analysis = (await FloorAnalysisMaster.query().with('applicant').with('shrimp.customer.contacts').where('id', analysisId).first()).toJSON()
    const form_emails = {
      to: analysis.applicant ? analysis.applicant.email : 'elyohan14@gmail.com', // Si el contacto fue eliminado
      contacts_email: analysis.shrimp.customer.contacts.map(val => (
        {
          email: val.email || 'Sin correo',
          name: `${val.name} ${val.last_name}`,
          disable: val.email == null || val.email == ''
        }
      )),
      email_customer: analysis.shrimp.customer.electronic_documents_email
    }
    response.send(form_emails)
  }

  async sendMail ({ request, response, params }) {
    const { to, cc, content } = request.all()
    console.log('cc', cc)
    const analysisId = params.analysisId
    const filename = `Analisis_de_suelos_${analysisId}.pdf`
    await Email.send({
      to,
      subject: 'Análisis de suelo',
      content,
      cc,
      attachments: [
        {
          filename,
          path: Helpers.appRoot('storage/uploads/flooranalysis') + `/${filename}`
        }
      ]
    })
    response.send(true)
  }
}

module.exports = FloorAnalysisController
