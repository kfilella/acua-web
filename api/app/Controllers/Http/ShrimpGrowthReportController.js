'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with shrimpgrowthreports
 */
const ShrimpGrowthReport = use('App/Models/ShrimpGrowthReport')
const User = use('App/Models/User')
const ShrimpGrowthReportSize = use('App/Models/ShrimpGrowthReportSize')
const Pool = use('App/Models/Pool')
const TechnicalEquipment = use('App/Models/TechnicalEquipment')
const Product = use('App/Models/Product')
const Email = use('App/Functions/Email')
const { validate } = use('Validator')
const moment = require('moment')
const PdfPrinter = use("pdfmake")
const Helpers = use("Helpers")
const fs = use("fs")

// Define font files
var fonts = {
  Roboto: {
    normal: 'resources/fonts/Roboto-Regular.ttf',
    bold: 'resources/fonts/Roboto-Medium.ttf',
    italics: 'resources/fonts/Roboto-Italic.ttf',
    bolditalics: 'resources/fonts/Roboto-MediumItalic.ttf'
  }
}

async function getDataSizeTable (reportId, dateAtFirst = undefined) {
  // Obtener header de tabla de tamaño: Nombres de las piscinas
  const poolsData = (await ShrimpGrowthReportSize.query().with('pool').where('shrimp_growth_report_id', reportId).orderBy('id').fetch()).toJSON()

  const header = [... new Set(poolsData.map(val => val.pool.description))]

  const poolIds = [... new Set(poolsData.map(val => val.pool.id))]

  // Obtengo las fechas de los reportes, cada fecha es un registro dentro de la tabla
  // let dates = [... new Set(poolsData.map(val => val.date))] No da fechas únicas

  let dates = await ShrimpGrowthReportSize.query().select('date').where('shrimp_growth_report_id', reportId).groupBy('date').orderBy('date').pluck('date')

  // Piscinas ya agregadas
  const inPools = []

  header.forEach((element, key) => {
    inPools.push({
      id: poolIds[key],
      description: element
    })
  });


  // Recorro las fechas que son registros para crear cada fila con sus piscinas
  const records = []
  for (const i of dates) {
    const record = []
    const pools = (await ShrimpGrowthReportSize.query().where('shrimp_growth_report_id', reportId).where('date', i)
    .with('product').orderBy('id').fetch()).toJSON()
    
    pools.forEach(element => {
      const obj = {
        value: element.value,
        color: element.product ? element.product.color : 'grey'
      }
      record.push(obj)
    })
    const objectValue = {value: moment(i).format('DD-MM-YYYY'), valueOriginal: i}
    if (dateAtFirst) {
      record.unshift(objectValue)
    } else {
      record.push(objectValue)
    }
    records.push(record)
  }
  dates = dates.map(val => (
    {
      label: moment(val).format('DD-MM-YYYY'),
      value: val
    }  
    
  ))
  const res = {
    header,
    records,
    inPools,
    dates
  }

  return res
}

async function getDataGrowthTable (reportId, dateAtFirst = undefined) {
  // Obtener header de tabla de tamaño: Nombres de las piscinas
  const poolsData = (await ShrimpGrowthReportSize.query().with('pool').where('shrimp_growth_report_id', reportId).orderBy('id').fetch()).toJSON()

  const header = [... new Set(poolsData.map(val => val.pool.description))]

  const poolIds = [... new Set(poolsData.map(val => val.pool.id))]

  // Obtengo las fechas de los reportes, cada fecha es un registro dentro de la tabla
  //let dates = [... new Set(poolsData.map(val => val.date))] No muestra las fechas únicas

  let dates = await ShrimpGrowthReportSize.query().select('date').where('shrimp_growth_report_id', reportId).groupBy('date').orderBy('date').pluck('date')

  // Piscinas ya agregadas
  const inPools = []

  header.forEach((element, key) => {
    inPools.push({
      id: poolIds[key],
      description: element
    })
  });


  // Recorro las fechas que son registros para crear cada fila con sus piscinas
  const records = []
  for (const i of dates) {
    const record = []
    const pools = (await ShrimpGrowthReportSize.query().where('shrimp_growth_report_id', reportId).where('date', i)
    .orderBy('id').fetch()).toJSON()

    for (const j of pools) { // Recorro todas las piscinas en un día específico
      /* const previousValue = await ShrimpGrowthReportSize.query().where('pool_id', j.pool_id).where('id', '<', j.id)
      .where('shrimp_growth_report_id', reportId).orderBy('id', 'desc').first()
      const growth = previousValue && j.value > 0 ? (j.value - previousValue.value).toFixed(2) : 0.00 */
      // Como se guarda el crecimiento al momento de guardar un valor no hace falta consultarlo. El campo growth lo tiene
      record.push(j.growth)
    }

    if (dateAtFirst) {
      record.unshift(moment(i).format('DD-MM-YYYY'))
    } else {
      record.push(moment(i).format('DD-MM-YYYY'))
    }
    records.push(record)
  }
  dates = dates.map(val => (
    {
      label: moment(val).format('DD-MM-YYYY'),
      value: val
    }  
    
  ))
  const res = {
    header,
    records,
    dates
  }

  return res
}

async function getDataAverageTable (reportId) {
  // Obtener header de tabla de tamaño: Nombres de los productos usados al registrar tamaños
  const products = (await ShrimpGrowthReportSize.query().with('product').where('shrimp_growth_report_id', reportId).orderBy('id').fetch()).toJSON()

  const header = [... new Set(products.filter(val => val.product).map(val => `Promedio con ${val.product.name}`))]

  const productsIds = [... new Set(products.filter(val => val.product).map(val => val.product.id))]

  // Obtengo las fechas de los reportes, cada fecha es un registro dentro de la tabla
  // let dates = [... new Set(products.map(val => val.date))] No muestra fechas únicas
  let dates = await ShrimpGrowthReportSize.query().select('date').where('shrimp_growth_report_id', reportId).groupBy('date').orderBy('date').pluck('date')

  // Recorro las fechas que son registros para crear cada fila con sus promedios por producto
  const records = []
  for (const i of dates) {
    const record = []
    
    for (const k of productsIds) {
      var avgA = await ShrimpGrowthReportSize.query().where('shrimp_growth_report_id', reportId).where('date', i).where('product_id', k).avg('growth')
      let avg = avgA[0].avg ? parseFloat((avgA[0].avg)).toFixed(2) : '0.00'
      record.push(avg)
    }

    record.unshift(moment(i).format('DD-MM-YYYY'))
    records.push(record)
  }
  dates = dates.map(val => (
    {
      label: moment(val).format('DD-MM-YYYY'),
      value: val
    }
  ))
  const res = {
    header,
    records,
    dates
  }

  return res
}
class ShrimpGrowthReportController {
  /**
   * Show a list of all shrimpgrowthreports.
   * GET shrimpgrowthreports
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, auth }) {
    // Si es jefe técnico debe ver los reportes registrados por sus asesores
    const userLogged = await auth.getUser()
    const user = (await User.query().with('roles').with('technical_chief').where('id', userLogged.id).first()).toJSON()
    const isTechnicalAdvisor = user.roles.find(val => val.id === 3)
    const isTechnicalChief= user.roles.find(val => val.id === 2)
    let reports = []
    const params = request.get()

    if (isTechnicalChief) { // Si es jefe técnico
      const technicalChiefId = user.technical_chief.id
      const technicalTeam = (await TechnicalEquipment.query().where('id', technicalChiefId).fetch()).toJSON()
      const technicalAdvisorsIds = []
      technicalTeam.forEach(element => {
        const advisors = JSON.parse(element.technical_advisors)
        advisors.forEach(element2 => {
          technicalAdvisorsIds.push(element2)  
        })
      })
    reports = (await ShrimpGrowthReport.query().with('technical_advisor').with('size').whereIn('technical_advisor_id', technicalAdvisorsIds)
    .where('shrimp_id', params.shrimp_id).fetch()).toJSON()
    }

    reports = (await ShrimpGrowthReport.query().with('technical_advisor').with('size')
    .where('shrimp_id', params.shrimp_id).fetch()).toJSON()
    reports.forEach(element => {
      element.begin_date = moment(element.begin_date).format('DD-MM-YYYY')
      element.end_date = moment(element.end_date).format('DD-MM-YYYY')
      element.pools = [... new Set(element.size.map(val => val.pool_id))].length
    })
    response.send(reports)
  }

  /**
   * Create/save a new shrimpgrowthreport.
   * POST shrimpgrowthreports
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ auth, request, response }) {
    const logguedUser = await auth.getUser()
    const user = (await User.query().with('technical_advisor').where('id', logguedUser.id).first()).toJSON()
    const validation = await validate(request.all(), ShrimpGrowthReport.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const body = request.only(ShrimpGrowthReport.fillable)
      // consultar quién es el asesor logueado
      body.technical_advisor_id = user.technical_advisor.id
      
      const report = await ShrimpGrowthReport.create(body)
      response.send(report);
    }
  }

  /**
   * Display a single shrimpgrowthreport.
   * GET shrimpgrowthreports/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    const report = (await ShrimpGrowthReport.query().with('technical_advisor').with('size').where('id', params.id).first()).toJSON()
    report.begin_date = moment(report.begin_date).format('YYYY-MM-DD')
    report.end_date = moment(report.end_date).format('YYYY-MM-DD')

    const pools = [... new Set(report.size.map(val => val.pool_id))]

    // Tamaños sin ser formateados
    report.sizeOriginal = report.size

    // Detalles de tabla de tamaño
    report.size = await getDataSizeTable(params.id)
    report.growth =await getDataGrowthTable(params.id)
    report.average =await getDataAverageTable(params.id)

    report.availablePools = await Pool.query().where('shrimp_id', report.shrimp_id).whereNotIn('id', pools).fetch()
    response.send(report)
  }

  /**
   * Update shrimpgrowthreport details.
   * PUT or PATCH shrimpgrowthreports/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const reportId = params.id
    const shrimpGrowthReport = await ShrimpGrowthReport.query().where('id', reportId).first()
    if (shrimpGrowthReport) {
      const validation = await validate(request.all(), ShrimpGrowthReport.fieldValidationRules())

      if (validation.fails()) {
        response.unprocessableEntity(validation.messages())
      } else {
        const body = request.only(ShrimpGrowthReport.fillable)

        await ShrimpGrowthReport.query().where('id', reportId).update(body)

        // Respaldar los registros de tamaño
        const sizesBackup = (await ShrimpGrowthReportSize.query().where('shrimp_growth_report_id', reportId).fetch()).toJSON()
        const poolIds = [... new Set(sizesBackup.map(val => val.pool_id))]
        
        // Eliminar los registros de tamaño para crear nuevos según la nueva fecha final
        await ShrimpGrowthReportSize.query().where('shrimp_growth_report_id', reportId).delete()

        // Recorro cada piscina que fue guardada para ir generando sus fechas
        for (const pool_id of poolIds) {
          
          // Generar y guardar registros de esa piscina para tabla de tamaño
          const beginDate = body.begin_date
          const endDate = body.end_date

          const differenceDays = moment(endDate).diff(moment(beginDate), 'days')
          const weeks = (differenceDays / 7).toFixed()

          var date = beginDate
          // Se deben recrear los registros de tamaños
          for (let i = 0; i <= weeks; i++) {
            const prevRecord = sizesBackup.find(val => val.shrimp_growth_report_id == reportId && val.pool_id == pool_id && moment(val.date).format('YYYY-MM-DD') == date)
            await ShrimpGrowthReportSize.create({
              shrimp_growth_report_id: reportId,
              pool_id,
              date,
              value: prevRecord ? prevRecord.value : 0,
              product_id: prevRecord ? prevRecord.product_id : 0,
              growth: prevRecord ? prevRecord.growth : 0
            })
            date =  moment(date).add(7, 'days').format('YYYY-MM-DD')
          }
        }

        response.send(shrimpGrowthReport)
      }
    } else {
      response.notFound(validation.messages())
    }
  }

  /**
   * Delete a shrimpgrowthreport with id.
   * DELETE shrimpgrowthreports/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }

  async addPool ({ params, request, response }) {
    const req = request.all()

    const report = (await ShrimpGrowthReport.query().where('id', req.reportId).first()).toJSON()

    // Generar y guardar registros de esa piscina para tabla de tamaño
    const beginDate = report.begin_date
    const endDate = report.end_date

    const differenceDays = moment(endDate).diff(moment(beginDate), 'days')
    const weeks = (differenceDays / 7).toFixed()

    var date = beginDate
    for (let i = 0; i <= weeks; i++) {
      console.log('date', date)
      await ShrimpGrowthReportSize.create({
        shrimp_growth_report_id: report.id,
        pool_id: req.poolId,
        date
      })
      date =  moment(date).add(7, 'days').format('YYYY-MM-DD')
    }

    return await getDataSizeTable(report.id)
  }

  async savePoolData ({ request }) {
    const req = request.all()
    const date = moment(req.date).format('YYYY-MM-DD')
    // return date
    const previousRecord = await ShrimpGrowthReportSize.query().where('shrimp_growth_report_id', req.reportId).where('pool_id', req.poolId)
    .where('date', '<', req.date).orderBy('id', 'desc').first() // crecimiento anterior

    await ShrimpGrowthReportSize.query().where('pool_id', req.poolId).where('date', req.date).update({
      value: req.size,
      product_id: req.product_id,
      growth: previousRecord ? parseFloat(req.size) - parseFloat(previousRecord.value) : 0
    })
    return true
  }

  // Reporte de crecimiento
  async printGrowthReports ({ params, response }) {
    var printer = new PdfPrinter(fonts)
    const report = (await ShrimpGrowthReport.query().where('id', params.id).with('shrimp.customer')
    .with('technical_advisor').with('size').with('shrimp.zone').first()).toJSON()
    //return report.technical_advisor.full_name

    const products = (await Product.all()).toJSON()
    const productsList = products.map(val => (
      {
        bold: true,
        text: `   ${val.name}`,
        fontSize: 12,
        color: val.color
      }
    ))

    // Consulta de datos de tamaño
    const pools = [... new Set(report.size.map(val => val.pool_id))]

    // Detalles de tabla de tamaño
    report.size = await getDataSizeTable(params.id, true)
    report.growth =await getDataGrowthTable(params.id, true)
    report.average =await getDataAverageTable(params.id)

    report.availablePools = await Pool.query().where('shrimp_id', report.shrimp_id).whereNotIn('id', pools).fetch()

    
   //return report


    let title = 'REPORTES DE CRECIMIENTO'
    let subtitle = 'subtitle'
    this.img = {
      image: 'logo.jpeg',
      fit: [65, 65],
//      margin: [31, 25],
      absolutePosition: {
        'x': 31,
        'y': 25
      }
    }
    this.img2 = {
      image: 'logo.jpeg',
      fit: [65, 65],
      absolutePosition: {
        'x': 20,
        'y': 10
      }
    }
    // Datos para tabla tamaños
    const sizesTableHeader = report.size.header.map(val => ({ text: val, bold: true }))
    sizesTableHeader.unshift({ text: 'FECHA', bold: true })
    const sizesTableWidths = Array(sizesTableHeader.length).fill('*')
    const sizeTableBody = [
    ]
    report.size.records.forEach(element => {
      const cols = []
      element.forEach(element2 => {
        cols.push({text: element2.value, color: element2.color})
      })
      sizeTableBody.push(cols)
    })
    sizeTableBody.unshift(sizesTableHeader)
    ///////////////////////////////////////////////////////7

    //Datos para tabla crecimiento
    const growthsTableHeader = report.growth.header.map(val => ({ text: val, bold: true }))
    growthsTableHeader.unshift({ text: 'FECHA', bold: true })
    const growthsTableWidths = Array(growthsTableHeader.length).fill('*')
    const growthTableBody = [
    ]
    report.growth.records.forEach(element => {
      const cols = []
      
      element.forEach(element2 => {
        cols.push(element2)
        console.log('ele2', element2)
      })
      growthTableBody.push(cols)
    })
    growthTableBody.unshift(growthsTableHeader)
    ///////////////////////////////////////////////////////////////////

    //Datos para tabla promedios
    const averagesTableHeader = report.average.header.map(val => ({ text: val, bold: true }))
    averagesTableHeader.unshift({ text: 'FECHA', bold: true })
    const averagesTableWidths = Array(averagesTableHeader.length).fill('*')
    const averageTableBody = [
    ]
    report.average.records.forEach(element => {
      const cols = []
      
      element.forEach(element2 => {
        cols.push(element2)
        console.log('ele2', element2)
      })
      averageTableBody.push(cols)
    })
    averageTableBody.unshift(averagesTableHeader)
    ///////////////////////////////////////////////////////////////////
    
    var docDefinition = {
      pageSize: 'A4',
      pageMargins: [60, 120, 40, 90],
      header: [
        {
          margin: [20, 40, 20, 20],
          alignment: 'center',
          text: [
            {
              bold: true,
              text: `${title}\n`,
              fontSize: 14
            }
          ]
        },
        {
          alignment: 'center',
          margin:[0,-10,0,10],
          text: [
            { text: 'CLIENTE: ', bold: true, fontSize: 10},
            { text: report.shrimp.customer.legal_name, fontSize: 10},
            { text: '             '},
            { text: 'CAMARONERA: ', bold: true, fontSize: 10},
            { text: report.shrimp.name, fontSize: 10},
            { text: '             '},
            { text: 'ZONA: ', bold: true, fontSize: 10},
            { text: report.shrimp.zone ? report.shrimp.zone.name : 'Zona eliminada', fontSize: 10}
          ]
        },
        {
          alignment: 'center',
          text: [
            { text: 'F. INICIO: ', bold: true, fontSize: 10, margin:[0,0,50,0]},
            { text: moment(report.begin_date).format('DD-MM-YYYY') , fontSize: 10},
            { text: '                       '},
            { text: 'F. FINAL: ', bold: true, fontSize: 10},
            { text: moment(report.end_date).format('DD-MM-YYYY') , fontSize: 10}
          ]
        },
        this.img
      ],
      content: [
        {
          margin: [-17, 40, 20, 20],
          alignment: 'left',
          text: [
            {
              bold: true,
              text: 'TAMAÑOS',
              fontSize: 14
            }
          ]
        },
        {
          margin: [-17, 0, 20, 20],
          alignment: 'left',
          text: [
            {
              bold: true,
              text: 'Leyenda',
              fontSize: 12
            }
          ]
        },
        {
          alignment: 'center',
          text: productsList
          
        },
        {
          margin: [-17, 0],
          table: {
            // headers are automatically repeated if the table spans over multiple pages
            // you can declare how many rows should be treated as headers
            headerRows: 1,
            widths: sizesTableWidths,
            body: sizeTableBody
          }
        },
        {
          pageBreak: 'before',
          margin: [-17, 40, 20, 20],
          alignment: 'left',
          text: [
            {
              bold: true,
              text: 'CRECIMIENTO',
              fontSize: 14
            }
          ]
        },
        {
          margin: [-17, 0],
          table: {
            // headers are automatically repeated if the table spans over multiple pages
            // you can declare how many rows should be treated as headers
            headerRows: 1,
            widths: growthsTableWidths,
            body: growthTableBody
          }
        },
        {
          pageBreak: 'before',
          margin: [-17, 40, 20, 20],
          alignment: 'left',
          text: [
            {
              bold: true,
              text: 'PROMEDIO DE CRECIMIENTO EN BASE AL PRODUCTO',
              fontSize: 14
            }
          ]
        },
        {
          margin: [-17, 0],
          table: {
            // headers are automatically repeated if the table spans over multiple pages
            // you can declare how many rows should be treated as headers
            headerRows: 1,
            widths: averagesTableWidths,
            body: averageTableBody
          }
        }
      ],
      footer: {
        columns: [
          this.img2,
          {text: 'Dpto. Técnico\n', bold: true, alignment: 'left', fontSize: 12, margin: [-25, 13, 0, 0] },
          {text: 'División Acuacultura\nTelf. 593 4 2811-61 Ext. 214-240\nhttp://www.agripac.com.ec/\n', alignment: 'left', fontSize: 10, margin: [-144, 25, 0, 0]},
          {text: 'Guayaquil - Ecuador', bold: true, alignment: 'left', fontSize: 12, margin: [-263, 60, 0, 0] },
          {text: `Asesor técnico\n${report.technical_advisor ? report.technical_advisor.full_name : ''}\n`,bold:true, alignment: 'left', fontSize: 10, margin: [5, 30, 0, 0] },
        ]
      }
    }

    var options = {
      // ...
    }
    var pdfDoc = printer.createPdfKitDocument(docDefinition, options)
    const dir = Helpers.appRoot('storage/uploads/growth_reports')
    const filename = `Reporte_de_crecimiento_${report.id}.pdf`
    if (!fs.existsSync(dir)){
      fs.mkdirSync(dir);
    }
    pdfDoc.pipe(fs.createWriteStream(`${dir}/${filename}`));
    pdfDoc.end()

    response.send(`growth_reports-Reporte_de_crecimiento_${report.id}`)
    //response.send(report)
  }

  async sendMailInfo ({ request, response, params }) {
    const reportId = params.id

    const report = (await ShrimpGrowthReport.query().with('shrimp.customer.contacts').where('id', reportId).first()).toJSON()

    const contacts_email = report.shrimp.customer.contacts.map(val => val.email)
    const form_emails = {
      to: contacts_email.length ? contacts_email[0] : '',
      contacts_email,
      email_customer: report.shrimp.customer.electronic_documents_email
    }
    response.send(form_emails)
  }

  async sendMail ({ request, response, params }) {
    const { to, cc, content } = request.all()
    const reportId = params.id
    const filename = `Reporte_de_crecimiento_${reportId}.pdf`
    await Email.send({
      to,
      subject: 'Reporte de crecimiento',
      content,
      cc,
      attachments: [
        {
          filename,
          path: Helpers.appRoot('storage/uploads/growth_reports') + `/${filename}`
        }
      ]
    })
    response.send(true)
  }

  async deletePool ({ params, request, response }) {
    const req = request.get()

    return await ShrimpGrowthReportSize.query().where('pool_id', req.poolId)
    .where('shrimp_growth_report_id', req.reportId).delete()
  }
}

module.exports = ShrimpGrowthReportController
