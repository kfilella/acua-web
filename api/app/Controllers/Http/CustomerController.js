'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with customers
 */
const Customer = use('App/Models/Customer')
const DeliverAddress = use('App/Models/DeliverAddress')
const Contact = use('App/Models/Contact')
const ContactPhone = use('App/Models/ContactPhone')
const ContactEmail = use('App/Models/ContactEmail')
const ContactShrimp = use('App/Models/ContactShrimp')
const Shrimp = use('App/Models/Shrimp')
const User = use('App/Models/User')
const TechnicalAdvisor = use('App/Models/TechnicalAdvisor')
const VisitingPlanning = use('App/Models/VisitingPlanning')
const CustomerEmail = use('App/Models/CustomerEmail')
const CustomerPhone = use('App/Models/CustomerPhone')
const moment = require('moment')
const Env = use('Env')
const { validate } = use('Validator')
class CustomerController {
  /**
   * Mostrar lista de clientes
   * GET customers
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, auth }) {
    const userLogged = await auth.getUser()
    const user = (await User.query().with('roles').where('id', userLogged.id).first()).toJSON()
    const isTechnicalAdvisor = user.roles.find(val => val.id === 3)
    const isTechnicalChief= user.roles.find(val => val.id === 2)

    const params = request.get()
    const { page, recordsCount, text } = params
    delete params.page
    delete params.recordsCount
    delete params.text
    let customers = []
    let query = Customer.query()

    if (isTechnicalAdvisor) { // Si es asesor técnico debe ver los clientes que tienen las camaroneras que tiene asignadas
      const technicalAdvisor = (await TechnicalAdvisor.query().with(['assignedShrimps.shrimp.customer']).where('user_id', user.id).first()).toJSON()
      technicalAdvisor.assignedShrimps.forEach(element => {
        const isInserted = customers.find(val => val.id === element.shrimp.customer.id)
        if (element.shrimp && !isInserted) {
          customers.push(element.shrimp.customer)
        }
      })

      customers = customers.map(val => val.id)
      // Obtener los ids de clientes y hacer una nueva consulta para traerlos ya paginados
      query = query.whereIn('id', customers)

    } else { // Si es jefe técnico o admin debe verlos todos
    }

    if (Object.keys(params).length) { // si recibe parámetros desde el front se implementa en el where
      query = query.where(params)
    }

    // Si es un filter
    if (text) {
      // query = query.where('legal_name', 'like', `%${text}%`)

      query = query.where(function() {
        this.where('legal_name', 'like', `%${text}%`)
        .orWhere('comercial_name', 'like', `%${text}%`)
        .orWhere('electronic_documents_email', 'like', `%${text}%`)
        .orWhere('document_number', 'like', `%${text}%`)
        .orWhereRaw('UPPER(legal_name) like ?', [`%${text.toUpperCase()}%`])
        .orWhereRaw('UPPER(comercial_name) like ?', [ `%${text.toUpperCase()}%` ])
        .orWhereRaw('UPPER(electronic_documents_email) like ?', [ `%${text.toUpperCase()}%` ])
        .orWhereRaw('UPPER(document_number) like ?', [ `%${text.toUpperCase()}%` ])
        .orWhere('sap_KUNNR', 'like', `%${text}%`)
        .orWhere('tags', 'like', `%${text}%`)
      })
    }

    customers = (await query.orderBy('id').paginate(page, recordsCount)).toJSON()

// return customers
    customers.data = [...new Set(customers.data)] // Eliminar cliente repetidos
    const customersFull = (await query.orderBy('id').fetch()).toJSON()
    console.log(customersFull.filter(val => val.legal_name.indexOf(text) > -1))

    /* customers = List.addRowActions(customers, [
      {name: 'edit', route: 'customers/form/', permission: '2.5'},
      {name: 'delete', permission: '2.4'},
      {name: 'show', route: 'customers/show/', permission: '2.1.1'}
    ]) */
    response.send(customers)
  }

  /**
   * Create/save a new customer.
   * POST customers
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async getPools ({ params, request, response, view }) {
    const shrimp = (await Shrimp.query().with('pools.images').with('zone').with('assignedContacts.contact')
    .with('pools', (query) => {
      query.orderBy('description')
    })
    .where('id', params.id).first()).toJSON()
    shrimp.pools.forEach(element => {
      element.expanded = false
    })
    shrimp.contacts = shrimp.assignedContacts.filter(val => {
      return val.contact
    }).map(val => val.contact)

    shrimp.contacts.forEach(val => {
      val.full_name = val.full_name = `${val.name} ${val.last_name}`
    })
    response.send(shrimp)
  }

  /**
   * Create/save a new customer.
   * POST customers
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async getClient ({response, auth}) {
    let user2 = (await auth.getUser()).toJSON()
    let info = (await Customer.query().with('shrimps').with('contacts').where('user_id', user2.id).first()).toJSON()
    const user = (await User.query().with('roles').with('technical_chief.teams').where('id', user2.id).first()).toJSON()
    const isTechnicalAdvisor = user.roles.find(val => val.id === 3)
    const isTechnicalChief = user.roles.find(val => val.id === 2)

    let shrimpsIds = []
    if (isTechnicalAdvisor) { // Si es asesor técnico consultar su id de asesor
      const technicalAdvisorInfo = (await TechnicalAdvisor.query().with('assignedShrimps').where('user_id', user.id).first()).toJSON()
      shrimpsIds = technicalAdvisorInfo.assignedShrimps.map(val => val.shrimp_id)
    }

    if (isTechnicalChief) { // Si es jefe técnico debe ver las camaroneras de los asesores que tiene asignados
      // Buscamos los ids de los asesores que tiene en su equipo
      let technicalAdvisorIds = []
      user.technical_chief.teams.forEach(element => {
        JSON.parse(element.technical_advisors).forEach(element2 => {
          technicalAdvisorIds.push(element2)
        })
      })

      const technicalAdvisorsInfo = (await TechnicalAdvisor.query().with('assignedShrimps').whereIn('id', technicalAdvisorIds).fetch()).toJSON()
      // shrimpsIds = technicalAdvisorsInfo.assignedShrimps.map(val => val.shrimp_id)

      technicalAdvisorsInfo.forEach(element => {
        element.assignedShrimps.forEach(element2 => {
          shrimpsIds.push(element2.shrimp_id)
        })
      })
    }

    const customer = (await Customer.query().with('deliver_addresses').with('contacts.phones').with('contacts.emails')
    .with('emails').with('phones')
    .with('shrimps.pools').with('shrimps.assign.advisor').with('shrimps.appointments').with('zones').with('shrimps.assignedContacts.contact')
    .with('shrimps', (query) => {
      if (isTechnicalAdvisor || isTechnicalChief) { // Si es asesor técnico o jefe técnico
        query.whereIn('id', shrimpsIds) // Traer detalle solo de las camaroneras asignadas a asesor o asesores
      }
    })
    .with('shrimps.appointments', (query) => {
      query.where('date', '>=', moment().format('YYYY-MM-DD'))
    })
    .where('id', info.id).first()).toJSON()
    if (customer) {
      customer.deliver_addresses = customer.deliver_addresses.map(value => value.address)
      customer.zone = customer.zones.map(value => value.id)
      for (const i of customer.contacts) {
        i.phones = i.phones.map(value => value.phone)
        i.emails = i.emails.map(value => value.email)
        i.shrimps = []
        let contact_shrimps = (await ContactShrimp.query().where('contact_id', i.id).fetch()).toJSON()
        for (const j of contact_shrimps) {
          i.shrimps.push(parseInt(j.shrimp_id))
        }
        i.full_name = `${i.name} ${i.last_name}`
      }

      // Recorro cada camaronera para calcular su área total, su cantidad de piscinas y sus visitas
      for (const i of customer.shrimps) {
        if (i.pools.length) {
          i.totalArea = i.pools.reduce((a,b) => {
            return parseFloat(a) + (parseFloat(b['area']) || 0)
          }, 0)
        }

        // Obtener asesores técnicos de camaronera
        let technicalAdvisors = []
        for (const j of i.assign) {
          if (j.advisor) {
            technicalAdvisors.push(j.advisor.full_name)
          }
        }

        technicalAdvisors = [...new Set(technicalAdvisors)] // Eliminar técnicos repetidos
        i.technicalAdvisors = technicalAdvisors.join('<br>') === '' ? 'Sin asignar' : technicalAdvisors.join('<br>')

        // Obtener contactos de camaroneras
        let assignedContacts = []
        for (const j of i.assignedContacts) {
          if (j.contact) {
            assignedContacts.push(`${j.contact.name} ${j.contact.last_name} `)
          }
        }
        i.assignedContacts = assignedContacts.join('<br>') === '' ? 'Sin contacto' : assignedContacts.join('<br>')

        // Obtener fecha de la próxima visita a esa camaronera
        if (i.appointments.length) {
          i.proxAppointment = moment(i.appointments[0].date).format('DD-MM-YYYY')
        } else {
          i.proxAppointment = 'No hay visitas próximas'
        }
      }
    }
    response.send(customer)
  }

  /**
   * Create/save a new customer.
   * POST customers
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  /**
   * Create/save a new customer.
   * POST customers
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const validation = await validate(request.all(), Customer.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const wholeRequest = request.all()
      const body = request.only(Customer.fillable)

      const customer = await Customer.create(body)
      await customer.zones().attach(body.zone)
      // Después de crear el cliente se deben guardar sus direcciones de despacho y sus contactos
      // Averiguar si el attach y dettach se puede con una relación de 1 a muchos
      for (const i of wholeRequest.deliver_addresses) {
        await DeliverAddress.create({
          customer_id: customer.id,
          address: i
        })
      }

      for (const i of wholeRequest.contacts) {
        const contact =  await Contact.create({
          customer_id: customer.id,
          last_name: i.last_name,
          name: i.name,
          job: i.job,
          phone: i.phone,
          email: i.email
        })

        for (const j of i.phones) {
          await ContactPhone.create({
            contact_id: contact.id,
            phone: j
          })
        }

        for (const j of i.emails) {
          await ContactEmail.create({
            contact_id: contact.id,
            email: j
          })
        }
        if (i.shrimps) {
          for (const j of i.shrimps) {
            let c_shrimp = await ContactShrimp.create({
              contact_id: contact.id,
              shrimp_id: j
            })
            console.log(c_shrimp, 'JJJ AQUIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII')
          }
        }
        // Añadir detalles de contacto, otros números de teléfono y otros correos relacionados a un contacto
      }

      response.send(customer);
    }
  }

  /**
   * Mostrar detalles de un cliente
   * GET customers/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, auth, response, view }) {
    const customerId = params.id
    const userLogged = await auth.getUser()
    const user = (await User.query().with('roles').with('technical_chief.teams').where('id', userLogged.id).first()).toJSON()
    const isTechnicalAdvisor = user.roles.find(val => val.id === 3)
    const isTechnicalChief = user.roles.find(val => val.id === 2)

    let shrimpsIds = []
    if (isTechnicalAdvisor) { // Si es asesor técnico consultar su id de asesor
      const technicalAdvisorInfo = (await TechnicalAdvisor.query().with('assignedShrimps').where('user_id', user.id).first()).toJSON()
      shrimpsIds = technicalAdvisorInfo.assignedShrimps.map(val => val.shrimp_id)
    }

    if (isTechnicalChief) { // Si es jefe técnico debe ver las camaroneras de los asesores que tiene asignados
      // Buscamos los ids de los asesores que tiene en su equipo
      let technicalAdvisorIds = []
      user.technical_chief.teams.forEach(element => {
        JSON.parse(element.technical_advisors).forEach(element2 => {
          technicalAdvisorIds.push(element2)
        })
      })

      const technicalAdvisorsInfo = (await TechnicalAdvisor.query().with('assignedShrimps').whereIn('id', technicalAdvisorIds).fetch()).toJSON()
      // shrimpsIds = technicalAdvisorsInfo.assignedShrimps.map(val => val.shrimp_id)

      technicalAdvisorsInfo.forEach(element => {
        element.assignedShrimps.forEach(element2 => {
          shrimpsIds.push(element2.shrimp_id)
        })
      })
    }

    const customer = (await Customer.query().with('deliver_addresses').with('contacts.phones').with('contacts.emails')
    .with('emails').with('phones').with('state')
    .with('shrimps.pools').with('shrimps.assign.advisor').with('shrimps.appointments').with('zones').with('shrimps.assignedContacts.contact')
    .with('shrimps', (query) => {
      if (isTechnicalAdvisor) { // Si es asesor técnico
        query.whereIn('id', shrimpsIds) // Traer detalle solo de las camaroneras asignadas a asesor o asesores
      }
    })
    .with('shrimps.appointments', (query) => {
      query.where('date', '>=', moment().format('YYYY-MM-DD'))
    })
    .where('id', customerId).first()).toJSON()
    if (customer) {
      customer.deliver_addresses = customer.deliver_addresses.map(value => value.address)
      customer.zone = customer.zones.map(value => value.id)
      for (const i of customer.contacts) {
        i.phones = i.phones.map(value => value.phone)
        i.emails = i.emails.map(value => value.email)
        i.shrimps = []
        let contact_shrimps = (await ContactShrimp.query().where('contact_id', i.id).fetch()).toJSON()
        for (const j of contact_shrimps) {
          i.shrimps.push(parseInt(j.shrimp_id))
        }
        i.full_name = `${i.name} ${i.last_name}`
      }

      // Recorro cada camaronera para calcular su área total, su cantidad de piscinas y sus visitas
      for (const i of customer.shrimps) {
        if (i.pools.length) {
          i.totalArea = i.pools.reduce((a,b) => {
            return parseFloat(a) + (parseFloat(b['area']) || 0)
          }, 0)
        }

        // Obtener asesores técnicos de camaronera
        let technicalAdvisors = []
        for (const j of i.assign) {
          if (j.advisor) {
            technicalAdvisors.push(j.advisor.full_name)
          }
        }

        technicalAdvisors = [...new Set(technicalAdvisors)] // Eliminar técnicos repetidos
        i.technicalAdvisors = technicalAdvisors.join('<br>') === '' ? 'Sin asignar' : technicalAdvisors.join('<br>')

        // Obtener contactos de camaroneras
        let assignedContacts = []
        for (const j of i.assignedContacts) {
          if (j.contact) {
            assignedContacts.push(`${j.contact.name} ${j.contact.last_name} `)
          }
        }
        i.assignedContacts = assignedContacts.join('<br>') === '' ? 'Sin contacto' : assignedContacts.join('<br>')

        // Obtener fecha de la próxima visita a esa camaronera
        if (i.appointments.length) {
          i.proxAppointment = moment(i.appointments[0].date).format('DD-MM-YYYY')
        } else {
          i.proxAppointment = 'No hay visitas próximas'
        }
      }
    }
    response.send(customer)
  }

  /**
   * Update customer details.
   * PUT or PATCH customers/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const validation = await validate(request.all(), Customer.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const customerId = params.id
      const customer =  await Customer.query().with('zones').where('id', customerId).first()
      const wholeRequest = request.all()
      const body = request.only(Customer.fillable)
      const zone = request.only(['zone']).zone
        for (const i in body) {
          customer[i] = body[i]
        }
        customer.save()
        await customer.zones().detach()
        await customer.zones().attach(zone)
      
      // const customer = await Customer.query().where('id', customerId).update(body)

      

      // Después de actualizar el cliente se deben guardar sus direcciones de despacho y sus contactos
      // Averiguar si el attach y dettach se puede con una relación de 1 a muchos
      // Eliminar las direcciones de despacho del cliente e insertar nuevamente

      await DeliverAddress.query().where('customer_id', customerId).delete()

      for (const i of wholeRequest.deliver_addresses) {

        await DeliverAddress.create({
          customer_id: customerId,
          address: i
        })
      }

      let activeContactIds = []
      for (const i of wholeRequest.contacts) {
        let contact = {}
        if (i.id) { // Si es un contacto que ya existía, solo se debe modificar
          contact = await Contact.query().where('id', i.id).update({
            customer_id: customerId,
            last_name: i.last_name,
            name: i.name,
            job: i.job,
            phone: i.phone,
            email: i.email
          })
          activeContactIds.push(i.id)
        } else { // Si es un contacto nuevo
          contact =  await Contact.create({
            customer_id: customerId,
            last_name: i.last_name,
            name: i.name,
            job: i.job,
            phone: i.phone,
            email: i.email
          })
          activeContactIds.push(contact.id)
        }
        // Añadir detalles de contacto, otros números de teléfono y otros correos relacionados a un contacto
        for (const j of i.phones) {
          // await ContactPhone.query().where('contact_id', contact.id).delete()
          await ContactPhone.create({
            contact_id: contact.id,
            phone: j
          })
        }

        for (const j of i.emails) {
          // await ContactEmail.query().where('contact_id', contact.id).delete()
          await ContactEmail.create({
            contact_id: contact.id,
            email: j
          })
        }
        if (i.shrimps) {
          const contact_id = i.id ? i.id : contact.id

          await ContactShrimp.query().where('contact_id', contact_id).delete()
          for (const j of i.shrimps) {

            let c_shrimp = await ContactShrimp.create({
              contact_id,
              shrimp_id: j
            })
          }
        }
      }
      // Eliminar contactos del cliente que no sean los que se acaban de modificar o crear
      await Contact.query().where('customer_id', customerId).whereNotIn('id', activeContactIds).delete()

      response.send(customer);
    }
  }

  /**
   * Delete a customer with id.
   * DELETE customers/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    // Aquí se deben implementar transacciones
    const customerId = params.id

    // Eliminar cliente
    await Customer.query().where('id', customerId).delete()
    
    // Eliminar cada camaronera de ese cliente, con softdelete
    await Shrimp.query().where('customer_id', customerId).delete()

    // Eliminar cada visita planificada a ese cliente
    await VisitingPlanning.query().where('customer_id', customerId).delete()
    response.send(true)
  }

  async getCustomerProfile ({ auth, response }) {
    const user = await auth.getUser()
    const customer = (await Customer.query().with('contacts.phones').with('contacts.emails').with('emails').with('phones')
    .with('zones')
    .where('user_id', user.id).first()).toJSON()

    customer.zone = customer.zones.map(value => value.id)

    for (const i of customer.contacts) {
      i.phones = i.phones.map(value => value.phone)
      i.emails = i.emails.map(value => value.email)
      i.shrimps = []
      let contact_shrimps = (await ContactShrimp.query().where('contact_id', i.id).fetch()).toJSON()
      for (const j of contact_shrimps) {
        i.shrimps.push(parseInt(j.shrimp_id))
      }
    }
    response.send(customer)  
  }
  async setCustomerProfile ({ auth, request, response }) {
    const user = await auth.getUser()
    const res = request.all()

    //Consultar el cliente
    const customer = await Customer.query().where('user_id', user.id).first()

    // Eliminar los correos guardados anteriormente
    await CustomerEmail.query().where('customer_id', customer.id).delete()

    // Insertar los correos del cliente
    for (const i of res.emails) {
      await CustomerEmail.create({
        customer_id: customer.id,
        email: i.email
      })
    }

    // Eliminar los números de teléfono guardados anteriormente
    await CustomerPhone.query().where('customer_id', customer.id).delete()

    // Insertar los números de teléfono del cliente
    for (const i of res.phones) {
      await CustomerPhone.create({
        customer_id: customer.id,
        phone: i.phone
      })
    }





    // Actualizar contactos
    let activeContactIds = []
      for (const i of res.contacts) {
        let contact = {}
        if (i.id) { // Si es un contacto que ya existía, solo se debe modificar
          contact = await Contact.query().where('id', i.id).update({
            customer_id: customer.id,
            last_name: i.last_name,
            name: i.name,
            job: i.job,
            phone: i.phone,
            email: i.email
          })
          activeContactIds.push(i.id)
        } else { // Si es un contacto nuevo
          contact =  await Contact.create({
            customer_id: customer.id,
            last_name: i.last_name,
            name: i.name,
            job: i.job,
            phone: i.phone,
            email: i.email
          })
          activeContactIds.push(contact.id)
        }
        // Añadir detalles de contacto, otros números de teléfono y otros correos relacionados a un contacto
        for (const j of i.phones) {
          // await ContactPhone.query().where('contact_id', contact.id).delete()
          await ContactPhone.create({
            contact_id: contact.id,
            phone: j
          })
        }

        for (const j of i.emails) {
          // await ContactEmail.query().where('contact_id', contact.id).delete()
          await ContactEmail.create({
            contact_id: contact.id,
            email: j
          })
        }
        if (i.shrimps) {
          const contact_id = i.id ? i.id : contact.id

          await ContactShrimp.query().where('contact_id', contact_id).delete()
          for (const j of i.shrimps) {

            let c_shrimp = await ContactShrimp.create({
              contact_id,
              shrimp_id: j
            })
          }
        }
      }
      // Eliminar contactos del cliente que no sean los que se acaban de modificar o crear
      await Contact.query().where('customer_id', customer.id).whereNotIn('id', activeContactIds).delete()

    response.send(res)
  }
  async getAssignedTechnicalAdvisors ({ auth, response }) {
    const userLogged = await auth.getUser()
    const customer = (await Customer.query().with('shrimps.assign.advisor').where('user_id', userLogged.id).first())
    .toJSON()
    let technicalAdvisorsId = []
    const shrimpsIds = []
    customer.shrimps.forEach(element => {
      element.assign.forEach(element2 => {
        technicalAdvisorsId.push(element2.advisor.id) // Asesores asignados a camaroneras de cliente
      })
      shrimpsIds.push(element.id) // Camaroneras de cliente
    })
    const technicalAdvisors = (await TechnicalAdvisor.query().with('assignedShrimps.shrimp')
    .whereIn('id', technicalAdvisorsId)
    .with('assignedShrimps', (query) => {
      query.whereIn('shrimp_id', shrimpsIds) // Mostrar solo las camaroneras asignadas del cliente
    }).fetch()).toJSON()

    technicalAdvisors.forEach(element => {
      element.shrimps = element.assignedShrimps.map(val => val.shrimp.name).join(', ')
    })

    response.send(technicalAdvisors)
  }
  /**
   * Obtener los eventos relacionados con el cliente logueado
   * GET upcoming_events
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async getUpcomingEvents ({ auth, response }) {
    const userLogged = await auth.getUser()
    const customer = (await Customer.query().with('shrimps.assign.advisor').where('user_id', userLogged.id).first())
    const appointmentPlanning = (await VisitingPlanning.query().with('visitReason').with('advisor').with('customer')
    .with('shrimp').where('customer_id', customer.id).where('status', 1).fetch()).toJSON()
    appointmentPlanning.forEach(element => {
      element.date = moment(element.date).format("YYYY-MM-DD")
      element.bgcolor = '#21ba45'
      element.statusName = 'Aprobado'
    })
    response.send(appointmentPlanning)
  }

  async getHanaCustomers () {
    console.log('Inicio', moment())
    var hana = require('@sap/hana-client');

    var conn = hana.createConnection();

    var conn_params = {
      serverNode: `${Env.get('HANA_HOST')}:${Env.get('HANA_PORT')}`,
      uid: Env.get('HANA_USER'),
      pwd: Env.get('HANA_PASSWORD'),
      instanceNumber: Env.get('HANA_INSTANCE_NUMBER')
    };

    
    // conn.connect(conn_params);
    console.log('Connected');
    // const records = conn.exec(`select * from "_SYS_BIC"."sap.bfranco.sd.comercial/AT_KNA1_RES"`);
    // console.log(records.length);
    // conn.disconnect();

    return await Customer.all()

    for (const i of records) {
      const exist = await Customer.query().where('sap_KUNNR', i.KUNNR).first()
      if (exist) { // Si ya se ha guardado ese cliente

      } else { // Si no se ha guardado, lo guardo
        /* const document_type = i.STCD1.length ? 2 : i.STCD3.length ? 1 : 3
        const document_number = i.STCD1.length ? i.STCD1 : i.STCD3.length ? i.STCD3 : i.STCD2
        await Customer.create({
          type: 1,
          sap_KUNNR: i.KUNNR,
          legal_name: `${i.NAME1} ${i.NAME2}`,
          document_type,
          document_number,
          country_id: 1,
          phone: i.TEL_NUMBER
        }) */
      }
    }
    
    
    console.log('Disconnected');
    console.log('Fin', moment())
    // return records
    return records
  }
}

module.exports = CustomerController

/* 
var hana = require('@sap/hana-client');

var conn = hana.createConnection();

var conn_params = {
  serverNode  : '10.2.1.249:30015',
  uid         : 'jtorbay',
  pwd         : 'W0nd1ix01',
  instanceNumber : '00'
};

conn.connect(conn_params, function(err) {
  console.log('error: ',err)
  if (err) throw err;
  conn.exec(`select * from "_SYS_BIC"."sap.bfranco.sd.comercial/AT_KNA1_RES"`, function (err, result) {
    if (err) throw err;

console.log(result)
//    console.log('Name: ', result[0].Name, ', Description: ', result[0].Description);
    // output --> Name: Tee Shirt, Description: V-neck
    conn.disconnect();
  })
});


 */