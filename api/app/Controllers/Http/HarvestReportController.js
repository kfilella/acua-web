'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with harvestreports
 */
const moment = require('moment')
const HarvestReport = use('App/Models/HarvestReport')
const HarvestReportFatteningFood = use('App/Models/HarvestReportFatteningFood')
const HarvestReportStartFood = use('App/Models/HarvestReportStartFood')
const User = use('App/Models/User')
const Shrimp = use('App/Models/Shrimp')
const Laboratory = use("App/Models/Laboratory")
const Product = use("App/Models/Product")
const Pool = use("App/Models/Pool")
const Email = use('App/Functions/Email')
const { validate } = use('Validator')
const Helpers = use('Helpers')
const imageToBase64 = use("image-to-base64")
const PdfPrinter = use("pdfmake")
const fs = use("fs")
const ExcelJS = require('exceljs')

const seedTypes = [
  {
    label: 'Directa',
    value: 1
  },
  {
    label: 'Madre hija',
    value: 2
  },
  {
    label: 'Raceways - transferida',
    value: 3
  },
  {
    label: 'Transferida',
    value: 4
  }
]
class HarvestReportController {
  /**
   * Show a list of all harvestreports.
   * GET harvestreports
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    const params = request.get()
    let records = HarvestReport.query().with('pool').with('laboratory')
    if (Object.keys(params).length) { // si recibe parámetros desde el front se implementa en el where
      // Filtrar los reportes según la camaronera que lo consulta
      records = records.whereHas("pool", builder => {
        // El wherehas filtra el modelo principal según la relación
        builder.where('shrimp_id', params.shrimp_id)
      })
    }

    records = (await records.orderBy('created_at', 'desc').fetch()).toJSON()

    records.forEach(element => {
      element.seed_time = moment(element.seed_time).format('DD-MM-YYYY')
      element.harvest_date = moment(element.harvest_date).format('DD-MM-YYYY')
    })
    response.send(records)
  }

  /**
   * Create/save a new harvestreport.
   * POST harvestreports
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ auth, request, response }) {
    const logguedUser = await auth.getUser()
    const user = (await User.query().with('technical_advisor').where('id', logguedUser.id).first()).toJSON()
    const req = request.all()
    const validation = await validate(request.all(), HarvestReport.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const body = request.only(HarvestReport.fillable)
      // Guardar id del asesor técnico
      body.technical_advisor_id = user.technical_advisor.id
      const record = await HarvestReport.create(body)

      // Guardar los alimentos de inicio asociados

      for (const i of req.start_foods) {
        await HarvestReportStartFood.create({
          harvest_report_id: record.id,
          product_id: i
        })
      }

      // Guardar los alimentos de engorde asociados

      for (const i of req.fattening_foods) {
        await HarvestReportFatteningFood.create({
          harvest_report_id: record.id,
          product_id: i
        })
      }
      response.send(record)
    }
  }

  /**
   * Display a single harvestreport.
   * GET harvestreports/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    const record = (await HarvestReport.query().with('fatteningFoods').with('startFoods').where('id', params.id).first()).toJSON()
    record.start_foods = record.startFoods.map(val => val.product_id)
    record.fattening_foods = record.fatteningFoods.map(val => val.product_id)
    record.seed_time = moment(record.seed_time).format('YYYY-MM-DD')
    record.harvest_date = moment(record.harvest_date).format('YYYY-MM-DD')
    response.send(record)
  }

  /**
   * Update harvestreport details.
   * PUT or PATCH harvestreports/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const req = request.all()
    const record = await HarvestReport.query().where('id', params.id).first()
    if (record) {
      const validation = await validate(request.all(), HarvestReport.fieldValidationRules())

      if (validation.fails()) {
        response.unprocessableEntity(validation.messages())
      } else {
        const body = request.only(HarvestReport.fillable)

        await HarvestReport.query().where('id', params.id).update(body)

        // Eliminar los productos anteriores para agregar los nuevos
        await HarvestReportFatteningFood.query().where('harvest_report_id', params.id).delete()

        // Eliminar los productos anteriores para agregar los nuevos
        await HarvestReportStartFood.query().where('harvest_report_id', params.id).delete()

        // Guardar los alimentos de inicio asociados
        for (const i of req.start_foods) {
          await HarvestReportStartFood.create({
            harvest_report_id: params.id,
            product_id: i
          })
        }
        // Guardar los alimentos de engorde asociados
        for (const i of req.fattening_foods) {
          await HarvestReportFatteningFood.create({
            harvest_report_id: params.id,
            product_id: i
          })
        }
        response.send(record)
      }
    } else {
      response.notFound(validation.messages())
    }
  }

  /**
   * Delete a harvestreport with id.
   * DELETE harvestreports/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }

  async printHarvestReport({ params, request, response }) {
    const report = (await HarvestReport.query().with('fatteningFoods.product')
    .with('pool.shrimp.customer').with('pool.shrimp.zone').with('startFood').with('technicalAdvisor')
    .with('laboratory').where('id', params.id).first()).toJSON()
    //return report


    const customer = report.pool.shrimp.customer.legal_name
    const shrimp = report.pool.shrimp.name
    const zone = report.pool.shrimp.zone.name
    const laboratory = report.laboratory.name
    const startFood = report.startFood ? report.startFood.name : ''
    const fattening_foods = report.fatteningFoods.map(val => val.product ? val.product.name : null).join(',')
    const density = (report.seed / report.pool.area).toFixed()
    const grSeed = report.seed * report.seed_weight
    const days = moment(report.harvest_date).diff(moment(report.seed_time), 'days') + 1
    const gr = report.pounds_fourth_thinning * 454
    const ani = (gr / report.weight_fourth_thinning).toFixed(2)
    const sup = ((report.pounds_fourth_thinning * 454 / report.weight_fourth_thinning / report.seed) * 100).toFixed(2)
    const poundsHa = (report.pounds_fourth_thinning / report.pool.area).toFixed()
    const fca = ((report.kg_food * 2.2046) / report.pounds_fourth_thinning).toFixed(2)
    const harvested = (report.pounds_fourth_thinning * 454 / report.weight_fourth_thinning).toFixed()
    const harvest = (harvested / report.pool.area / 10000).toFixed()
    const daily = ((report.weight_fourth_thinning - report.seed_weight) / days).toFixed(2)
    const dailyWithOutThinning = ((report.weight_fourth_thinning - report.seed_weight) / days).toFixed(2)
    const weeklyWithOutThinning = ((dailyWithOutThinning * 7)).toFixed(2)
    const rofi = 1
    const _rofi = 1 / rofi
    const weekly = (daily * 7).toFixed(2)
    const roi = (((report.us_dollar_day_ha * report.pool.area * days) / (report.cost_pound * report.pounds_fourth_thinning)) * 100).toFixed(2)
    const sale = report.us_dollar_pound * report.pounds_fourth_thinning
    const utility = (report.us_dollar_day_ha * report.pool.area * days).toFixed(2)
    // return report
    // return fattening_foods

    let seedType = ''
    switch(report.seedType) {
        case 1:
          seedType = 'Directa'
          break;
        case 2:
          seedType = 'Madre hija'
          break;
        case 3:
          seedType = 'Raceways - transferida'
          break;
        default:
          seedType = 'Transferida'
    }

    const title = 'REPORTES DE COSECHAS'
    this.img = {
      image: 'logo.jpeg',
      fit: [65, 65],
      absolutePosition: {
        'x': 31,
        'y': 25
      }
    }
    this.img2 = {
      image: 'logo.jpeg',
      fit: [65, 65],
      absolutePosition: {
        'x': 31,
        'y': 10
      }
    }
    try {

      let logo = Helpers.appRoot("") + `/logo.jpeg`;
      logo = await imageToBase64(logo).then((res) => {
        return "data:image/png;base64, " + res;
      });

      var fonts = {
        Roboto: {
          normal: 'resources/fonts/Roboto-Regular.ttf',
          bold: 'resources/fonts/Roboto-Medium.ttf',
          italics: 'resources/fonts/Roboto-Italic.ttf',
          bolditalics: 'resources/fonts/Roboto-MediumItalic.ttf'
        }
      }
      var printer = new PdfPrinter(fonts);
      let docDefinition = {
        pageSize: "letter",
        pageMargins: [10, 140, 15, 100],
        header: [
          {
            margin: [20, 40, 20, 20],
            alignment: 'center',
            text: [
              {
                bold: true,
                text: `${title}\n`,
                fontSize: 14
              }
            ]
          },
          {
            alignment: 'center',
            margin:[0,-10,0,10],
            text: [
              { text: 'CLIENTE: ', bold: true, fontSize: 10},
              { text: customer, fontSize: 10},
              { text: '             '},
              { text: 'CAMARONERA: ', bold: true, fontSize: 10},
              { text: shrimp, fontSize: 10},
              { text: '             '},
              { text: 'Zona: ', bold: true, fontSize: 10},
              { text: zone, fontSize: 10}
            ]
          },
          this.img
        ],
        footer: {
          columns: [
            this.img2,
            {text: 'Dpto. Técnico\n', bold: true, alignment: 'left', fontSize: 12, margin: [-25, 13, 0, 0] },
            {text: 'División Acuacultura\nTelf. 593 4 2811-61 Ext. 214-240\nhttp://www.agripac.com.ec/\n', alignment: 'left', fontSize: 10, margin: [-144, 25, 0, 0]},
            {text: 'Guayaquil - Ecuador', bold: true, alignment: 'left', fontSize: 12, margin: [-263, 60, 0, 0] },
            {text: `Asesor técnico\n${report.technicalAdvisor ? report.technicalAdvisor.full_name : ''}\n`,bold:true, alignment: 'left', fontSize: 10, margin: [5, 30, 0, 0] },
          ]
        },
        content: [
          {
            style: "tableOneFirstPage",
            table: {
              widths: Array.apply(null, Array(3)).map((val) => "33%"),
              heights: Array.apply(null, Array(10)).map((val) => 30),
              alignment: "center",
              body: [
                ["PISCINA", "AREA", "TIPO DE SIEMBRA"].map((ele) => {
                  return { text: ele, style: "tableHeader" };
                }),
                [ report.pool.description, report.pool.area,seedType].map((ele) => {
                  return { text: ele, style: "tableSubHeader" };
                }),
                ["FECHA DE SIEMBRA", "FECHA DE COSECHA", "LABORATORIO"].map(
                  (ele) => {
                    return { text: ele, style: "tableHeader" };
                  }
                ),
                [
                  moment(report.seed_time).format('DD-MM-YYYY'), moment(report.harvest_date).format('DD-MM-YYYY'), laboratory,
                ].map((ele) => {
                  return { text: ele, style: "tableSubHeader" };
                }),
                ["SIEMBRA", "PESO DE SIEMBRA", "KG DE ALIMENTO"].map((ele) => {
                  return { text: ele, style: "tableHeader" };
                }),
                [
                  report.seed, report.seed_weight,report.kg_food,
                ].map((ele) => {
                  return { text: ele, style: "tableSubHeader" };
                }),
                ["COSTO LIBRA", "$ / DÍA / Ha", "VENTA $ / lb"].map((ele) => {
                  return { text: ele, style: "tableHeader" };
                }),
                [
                  report.cost_pound, report.us_dollar_day_ha, report.us_dollar_pound
                ].map((ele) => {
                  return { text: ele, style: "tableSubHeader" };
                }),
                ["VENTA $ / Kg", "ALIMENTO INICIO", "ALIMENTO ENGORDE"].map(
                  (ele) => {
                    return { text: ele, style: "tableHeader" };
                  }
                ),
                [
                  report.us_dollar_kg, startFood, fattening_foods
                ].map((ele) => {
                  return { text: ele, style: "tableSubHeader" };
                }),
              ],
            },
            layout: {
              hLineWidth: function (i, node) {
                return 0.1;
              },
              vLineWidth: function (i, node) {
                return 0.1;
              },
            },
          },
          {
            style: "tableSecond",
            table: {
              widths: Array.apply(null, Array(5)).map((val) => "20%"),
              heights: Array.apply(null, Array(3)).map((val) => 30),
              alignment: "center",
              body: [
                ["", "1 RALEO", "2 RALEO", "3 RALEO", "COSECHA"].map((ele) => {
                  return ele !== ""
                    ? { text: ele, style: "tableHeader" }
                    : { text: "", border: [false, false, false, false] };
                }),
                [
                  `LIBRAS`, report.pounds_first_thinning, report.pounds_second_thinning, report.pounds_third_thinning, report.pounds_fourth_thinning
                ].map((ele) => {
                  return {
                    text: ele,
                    style: ele == "LIBRAS" ? "tableHeader" : "tableSubHeader",
                  };
                }),
                [
                  "PESO",
                  report.weight_first_thinning,
                  report.weight_second_thinning,
                  report.weight_third_thinning,
                  report.weight_fourth_thinning,
                ].map((ele) => {
                  return {
                    text: ele,
                    style: ele == "PESO" ? "tableHeader" : "tableSubHeader",
                  };
                }),
              ],
            },
          },
          {
            text: "RESULTADOS",
            pageBreak: "before",
            margin: [10, 0, 0, 10],
            bold: true,
            fontSize: 18,
          },
          {
            style: "tableOneFirstPage",
            table: {
              widths: Array.apply(null, Array(3)).map((val) => "33%"),
              heights: Array.apply(null, Array(6)).map((val) => 20),
              alignment: "center",
              body: [
                ["DENSIDAD", "GR SIEMBRA", "DÍAS"].map((ele) => {
                  return { text: ele, style: "tableHeaderSecondPage" };
                }),
                [
                  density,
                  grSeed,
                  days,
                ].map((ele) => {
                  return { text: ele, style: "tableSubHeaderSecondPage" };
                }),
                ["GRAMOS", "# ANI", "%SUP"].map((ele) => {
                  return { text: ele, style: "tableHeaderSecondPage" };
                }),
                [
                  gr, ani, sup
                ].map((ele) => {
                  return { text: ele, style: "tableSubHeaderSecondPage" };
                }),
                ["LBS / Ha", "FCA", ""].map((ele) => {
                  return ele !== ""
                    ? { text: ele, style: "tableHeaderSecondPage" }
                    : { text: "", border: [false, false, false, false] };
                }),
                [poundsHa, fca, ""].map((ele) => {
                  return ele !== ""
                    ? { text: ele, style: "tableSubHeaderSecondPage" }
                    : { text: "", border: [false, false, false, false] };
                }),
              ],
            },
            layout: {
              hLineWidth: function (i, node) {
                return 0.1;
              },
              vLineWidth: function (i, node) {
                return 0.1;
              },
            },
          },
          {
            style: "tableTwoSecondPage",
            table: {
              widths: Array.apply(null, Array(3)).map((val) => "33%"),
              heights: Array.apply(null, Array(5)).map((val) => 25),
              alignment: "center",
              body: [
                ["TOTAL COSECHADO", "ANIMALES", "DENSIDAD"].map((ele) => {
                  return { text: ele, style: "tableHeaderH1SecondPage" };
                }),
                ["LIBRAS", "COSECHADOS", "COSECHA"].map((ele) => {
                  return { text: ele, style: "tableHeaderH2SecondPage" };
                }),
                [
                  report.pounds_fourth_thinning, harvested, harvest
                ].map((ele) => {
                  return { text: ele, style: "tableSubHeaderSecondPageTable" };
                }),
                ["PESO", "", ""].map((ele) => {
                  return ele !== ""
                    ? { text: ele, style: "tableHeaderSecondPage" }
                    : { text: "", border: [false, false, false, false] };
                }),
                [report.weight_fourth_thinning, "", ""].map((ele) => {
                  return ele !== ""
                    ? { text: ele, style: "tableSubHeaderSecondPage" }
                    : { text: "", border: [false, false, false, false] };
                }),
              ],
            },
            layout: {
              hLineWidth: function (i, node) {
                return 0.1;
              },
              vLineWidth: function (i, node) {
                return 0.1;
              },
            },
          },
          {
            style: "tableTwoSecondPage",
            table: {
              widths: Array.apply(null, Array(3)).map((val) => "33%"),
              heights: Array.apply(null, Array(7)).map((val) => 20),
              alignment: "center",
              body: [
                [
                  {
                    text: "CRECIMIENTO",
                    fontSize: 12,
                    bold: true,
                    colSpan: 3,
                    alignment: "center",
                    margin: [0, 3, 0, 0],
                  },
                  "",
                  "",
                ],
                ["DIARIO", "SEM SIN RALEO", "1 / ROFI"].map((ele) => {
                  return { text: ele, style: "tableHeaderSecondPage" };
                }),
                [
                  daily,
                  weeklyWithOutThinning,
                  _rofi,
                ].map((ele) => {
                  return { text: ele, style: "tableSubHeaderSecondPage" };
                }),
                ["SEM", "ROI", "VENTA"].map((ele) => {
                  return { text: ele, style: "tableHeaderSecondPage" };
                }),
                [
                  weekly,
                  roi,
                  sale,
                ].map((ele) => {
                  return { text: ele, style: "tableSubHeaderSecondPage" };
                }),
                ["DIARIO SIN RALEO", "ROFI", "UTILIDAD"].map((ele) => {
                  return { text: ele, style: "tableHeaderSecondPage" };
                }),
                [
                  dailyWithOutThinning,
                  rofi,
                  utility,
                ].map((ele) => {
                  return { text: ele, style: "tableSubHeaderSecondPage" };
                }),
              ],
            },
          },
        ],
        styles: {
          logoHeader: {
            margin: [0, 30, 0, 0],
          },
          header: {
            alignment: "center",
            fontSize: 18,
            bold: true,
            margin: [0, 10, 0, 0],
          },
          titleHeader: {
            alignment: "center",
            fontSize: 16,
            bold: true,
            margin: [-75, 50, 0, 0],
          },
          tableHeaderTitle: {
            margin: [-55, 35, 0, 0],
          },
          tableOneFirstPage: {
            margin: [10, 0, 0, 0],
            alignment: "center",
          },
          tableSecond: {
            margin: [10, 55, 5, 0],
            alignment: "center",
          },
          tableHeader: {
            bold: true,
            fontSize: 9,
            color: "black",
            margin: [0, 10, 0, 0],
          },
          tableSubHeader: {
            fontSize: 9,
            color: "black",
            margin: [0, 10, 0, 0],
          },
          tableHeaderSecondPage: {
            bold: true,
            fontSize: 9,
            color: "black",
            margin: [0, 5, 0, 0],
          },
          tableSubHeaderSecondPage: {
            fontSize: 9,
            color: "black",
            margin: [0, 5, 0, 0],
          },
          tableTwoSecondPage: {
            margin: [10, 25, 0, 0],
            alignment: "center",
          },
          tableHeaderH1SecondPage: {
            bold: true,
            fontSize: 12,
            color: "black",
            margin: [0, 5, 0, 0],
          },
          tableHeaderH2SecondPage: {
            bold: true,
            fontSize: 9,
            color: "black",
            margin: [0, 7, 0, 0],
          },
          tableSubHeaderSecondPageTable: {
            fontSize: 9,
            color: "black",
            margin: [0, 7, 0, 0],
          },
        },
      };

      var options = {}
      var pdfDoc = printer.createPdfKitDocument(docDefinition, options)
      const dir = Helpers.appRoot('storage/uploads/harvest_reports')
      const filename = `Reporte_de_cosecha_${params.id}.pdf`
      if (!fs.existsSync(dir)){
        fs.mkdirSync(dir)
      }
      pdfDoc.pipe(fs.createWriteStream(`${dir}/${filename}`))
      pdfDoc.end()

      response.send(`harvest_reports-Reporte_de_cosecha_${params.id}`)
      } catch (e) {
      console.log(e, "error")
      return e
    }
  }

  async sendMailInfo ({ request, response, params }) {
    const reportId = params.id

    const report = (await HarvestReport.query().with('pool.shrimp.customer.contacts').where('id', reportId).first()).toJSON()

    const contacts_email = report.pool.shrimp.customer.contacts.map(val => (
      {
        email: val.email || 'Sin correo',
        name: `${val.name} ${val.last_name}`,
        disable: val.email == null || val.email == ''
      }
    ))
    const form_emails = {
      to: contacts_email.length ? contacts_email[0] : '',
      contacts_email,
      email_customer: report.pool.shrimp.customer.electronic_documents_email
    }
    response.send(form_emails)
  }

  async sendMail ({ request, response, params }) {
    const { to, cc, content } = request.all()
    const reportId = params.id
    const filename = `Reporte_de_cosecha_${reportId}.pdf`
    await Email.send({
      to,
      subject: 'Reporte de cosecha',
      content,
      cc,
      attachments: [
        {
          filename,
          path: Helpers.appRoot('storage/uploads/harvest_reports') + `/${filename}`
        }
      ]
    })
    response.send(true)
  }

  async printGeneralHarvestReport ({ request, response }) {
    const res = request.all()

    /* const harvestReports = (await HarvestReport.query().with('pool').with('fatteningFoods.product').with('laboratory')
    .with('startFoods.product')
    .whereIn('pool_id', res.poolsIds)
    .whereBetween('harvest_date', [res.from, res.to])
    .where(function() {
      this.whereHas("startFoods", builder => {
        // El wherehas filtra el modelo principal según la relación
        builder.whereIn('product_id', res.foodsIds)
      })
      .orWhereHas("fatteningFoods", builder => {
        // El wherehas filtra el modelo principal según la relación
        builder.whereIn('product_id', res.foodsIds)
      })
    })
    .whereIn('seed_type', res.seedtypes)
    .fetch()).toJSON() */

    let query = HarvestReport.query().with('pool.shrimp').with('fatteningFoods.product').with('laboratory')
    .with('startFoods.product')


    if (res.poolsIds) { // Si seleccionan piscinas
      query = query.whereIn('pool_id', res.poolsIds)
    } else if (res.shrimpsIds) { // filtrar solo las piscinas de las camaroneras seleccionadas
      query = query.whereHas("pool.shrimp", builder => {
        // El wherehas filtra el modelo principal según la relación
        builder.whereIn('id', res.shrimpsIds)
      })
    }

    console.log(res.from)
    if (res.from && res.to) {
      console.log('1')
      /* query = query.where(function() {
        this.whereBetween('seed_time', [res.from, res.to])
        .orWhereBetween('harvest_date', [res.from, res.to])
      }) */
      query = query.whereBetween('harvest_date', [res.from, res.to])
    } else if (res.from) {
      query = query.where(function() {
        this.where('seed_time', '>', res.from)
        .orWhere('harvest_date', '>', res.from)
      })
    } else if (res.to) {
      query = query.where(function() {
        this.where('seed_time', '>', res.to)
        .orWhere('harvest_date', '>', res.to)
      })
    }

    if (res.foodsIds) {
      query = query.where(function() {
        this.whereHas("startFoods", builder => {
          // El wherehas filtra el modelo principal según la relación
          builder.whereIn('product_id', res.foodsIds)
        })
        .orWhereHas("fatteningFoods", builder => {
          // El wherehas filtra el modelo principal según la relación
          builder.whereIn('product_id', res.foodsIds)
        })
      })
    }

    if (res.seedtypes) {
      query = query.whereIn('seed_type', res.seedtypes)
    }

    const harvestReports = (await query.fetch()).toJSON()

    // Agrupar por camaroneras
    /* const shrimpsIds = [...new Set(harvestReports.map(val => (
      {
        shrimpId: val.pool.shrimp_id,
        name: val.pool.shrimp.name
      }
    )))] */ // Ids de camaroneras sin repetirse

    const shrimpsIds = [...new Set(harvestReports.map(val => val.pool.shrimp_id))]


    //return shrimpsIds

    const workbook = new ExcelJS.Workbook()

    await workbook.xlsx.readFile('resources/templates/plantilla_reporte_general_cosechas.xlsx').then(function() {
      console.log('cargo archivo')
      const worksheet = workbook.getWorksheet('Reporte')

      const seedTypes = [ 'Directa',  'Madre hija', 'Raceways - transferida', 'Transferida' ]
      let increment = 6
      const economicalData = res.economicalData
      let hideJ = true
      let hideK = true
      let hideL = true
      let hideM = true
      let hideN = true
      let hideO = true

      // Hacer un bucle con las camaroneras, dentro filtrar los registros que son de esa camaronera y hacer el respectivo bucle
      shrimpsIds.forEach((shrimp, shrimpKey) => {
        const shrimpName = harvestReports.find(val => val.pool.shrimp_id === shrimp).pool.shrimp.name
        if (shrimpKey > 0) {
          worksheet.getCell(`A${increment}`).style = worksheet.getCell(`A6`).style
          worksheet.getCell(`A${increment}`).value = 'Cliente:'
          worksheet.mergeCells(`A${increment}:B${increment}`)

          worksheet.getCell(`C${increment}`).style = worksheet.getCell(`C6`).style
          worksheet.mergeCells(`C${increment}:E${increment}`)

          // Primer encabezado
          worksheet.getCell(`J${increment + 1}`).style = worksheet.getCell(`J7`).style
          worksheet.getCell(`J${increment + 1}`).value = '1° Raleo'
          worksheet.mergeCells(`J${increment + 1}:K${increment + 1}`)

          worksheet.getCell(`L${increment + 1}`).style = worksheet.getCell(`L7`).style
          worksheet.getCell(`L${increment + 1}`).value = '2° Raleo'
          worksheet.mergeCells(`L${increment + 1}:M${increment + 1}`)

          worksheet.getCell(`N${increment + 1}`).style = worksheet.getCell(`N7`).style
          worksheet.getCell(`N${increment + 1}`).value = '3° Raleo'
          worksheet.mergeCells(`N${increment + 1}:O${increment + 1}`)

          worksheet.getCell(`P${increment + 1}`).style = worksheet.getCell(`P7`).style
          worksheet.getCell(`P${increment + 1}`).value = 'Total Cosechado'
          worksheet.mergeCells(`P${increment + 1}:Q${increment + 1}`)

          worksheet.getCell(`R${increment + 1}`).style = worksheet.getCell(`R7`).style
          worksheet.getCell(`R${increment + 1}`).value = 'Densidad'

          worksheet.getCell(`S${increment + 1}`).style = worksheet.getCell(`S7`).style
          worksheet.getCell(`S${increment + 1}`).value = 'Crecimiento'
          worksheet.mergeCells(`S${increment + 1}:T${increment + 1}`)

          worksheet.getCell(`Y${increment + 1}`).style = worksheet.getCell(`Y7`).style
          worksheet.getCell(`Y${increment + 1}`).value = 'COSTOS'
          worksheet.mergeCells(`Y${increment + 1}:AB${increment + 1}`)

          worksheet.getCell(`AC${increment + 1}`).style = worksheet.getCell(`AC7`).style
          worksheet.getCell(`AC${increment + 1}`).value = 'Venta'

          worksheet.getCell(`AD${increment + 1}`).style = worksheet.getCell(`AD7`).style
          worksheet.getCell(`AD${increment + 1}`).value = 'Utilidad'
          worksheet.mergeCells(`AD${increment + 1}:AD${increment + 2}`)

          worksheet.getCell(`AE${increment + 1}`).style = worksheet.getCell(`AE7`).style
          worksheet.getCell(`AE${increment + 1}`).value = 'Utilidad'

          worksheet.getCell(`AF${increment + 1}`).style = worksheet.getCell(`AF7`).style
          worksheet.getCell(`AF${increment + 1}`).value = 'ROI'
          worksheet.mergeCells(`AF${increment + 1}:AF${increment + 2}`)

          worksheet.getCell(`AG${increment + 1}`).style = worksheet.getCell(`AG7`).style
          worksheet.getCell(`AG${increment + 1}`).value = 'ROFI'
          worksheet.mergeCells(`AG${increment + 1}:AG${increment + 2}`)

          worksheet.getCell(`AH${increment + 1}`).style = worksheet.getCell(`AH7`).style
          worksheet.getCell(`AH${increment + 1}`).value = '1/ROFI'
          worksheet.mergeCells(`AH${increment + 1}:AH${increment + 2}`)

          // Segundo encabezado

          worksheet.getCell(`A${increment + 2}`).style = worksheet.getCell(`A8`).style
          worksheet.getCell(`A${increment + 2}`).value = 'Pisc'

          worksheet.getCell(`B${increment + 2}`).style = worksheet.getCell(`B8`).style
          worksheet.getCell(`B${increment + 2}`).value = 'Tipo de siembra'

          worksheet.getCell(`C${increment + 2}`).style = worksheet.getCell(`C8`).style
          worksheet.getCell(`C${increment + 2}`).value = 'Área'


          worksheet.getCell(`D${increment + 2}`).style = worksheet.getCell(`D8`).style
          worksheet.getCell(`D${increment + 2}`).value = 'Siembra'

          worksheet.getCell(`E${increment + 2}`).style = worksheet.getCell(`E8`).style
          worksheet.getCell(`E${increment + 2}`).value = 'Cosecha'

          worksheet.getCell(`F${increment + 2}`).style = worksheet.getCell(`F8`).style
          worksheet.getCell(`F${increment + 2}`).value = 'Lab'

          worksheet.getCell(`G${increment + 2}`).style = worksheet.getCell(`G8`).style
          worksheet.getCell(`G${increment + 2}`).value = 'Densidad'

          worksheet.getCell(`H${increment + 2}`).style = worksheet.getCell(`H8`).style
          worksheet.getCell(`H${increment + 2}`).value = 'Peso Inic'

          worksheet.getCell(`I${increment + 2}`).style = worksheet.getCell(`I8`).style
          worksheet.getCell(`I${increment + 2}`).value = 'Días'

          worksheet.getCell(`J${increment + 2}`).style = worksheet.getCell(`J8`).style
          worksheet.getCell(`J${increment + 2}`).value = 'Libras'

          worksheet.getCell(`K${increment + 2}`).style = worksheet.getCell(`K8`).style
          worksheet.getCell(`K${increment + 2}`).value = 'peso'

          worksheet.getCell(`L${increment + 2}`).style = worksheet.getCell(`L8`).style
          worksheet.getCell(`L${increment + 2}`).value = 'Libras'

          worksheet.getCell(`M${increment + 2}`).style = worksheet.getCell(`M8`).style
          worksheet.getCell(`M${increment + 2}`).value = 'peso'

          worksheet.getCell(`N${increment + 2}`).style = worksheet.getCell(`N8`).style
          worksheet.getCell(`N${increment + 2}`).value = 'Libras'

          worksheet.getCell(`O${increment + 2}`).style = worksheet.getCell(`O8`).style
          worksheet.getCell(`O${increment + 2}`).value = 'peso'

          worksheet.getCell(`P${increment + 2}`).style = worksheet.getCell(`P8`).style
          worksheet.getCell(`P${increment + 2}`).value = 'Libras'

          worksheet.getCell(`Q${increment + 2}`).style = worksheet.getCell(`Q8`).style
          worksheet.getCell(`Q${increment + 2}`).value = 'Peso'

          worksheet.getCell(`R${increment + 2}`).style = worksheet.getCell(`R8`).style
          worksheet.getCell(`R${increment + 2}`).value = 'Cosecha'

          worksheet.getCell(`S${increment + 2}`).style = worksheet.getCell(`S8`).style
          worksheet.getCell(`S${increment + 2}`).value = 'Diario'

          worksheet.getCell(`T${increment + 2}`).style = worksheet.getCell(`T8`).style
          worksheet.getCell(`T${increment + 2}`).value = 'Sem'

          worksheet.getCell(`U${increment + 2}`).style = worksheet.getCell(`U8`).style
          worksheet.getCell(`U${increment + 2}`).value = '%Sup'

          worksheet.getCell(`V${increment + 2}`).style = worksheet.getCell(`V8`).style
          worksheet.getCell(`V${increment + 2}`).value = 'Lbs/Ha'

          worksheet.getCell(`W${increment + 2}`).style = worksheet.getCell(`W8`).style
          worksheet.getCell(`W${increment + 2}`).value = 'FCA'

          worksheet.getCell(`X${increment + 2}`).style = worksheet.getCell(`X8`).style
          worksheet.getCell(`X${increment + 2}`).value = 'días seco'

          worksheet.getCell(`Y${increment + 2}`).style = worksheet.getCell(`Y8`).style
          worksheet.getCell(`Y${increment + 2}`).value = '$Kilo Alim'

          worksheet.getCell(`Z${increment + 2}`).style = worksheet.getCell(`Z8`).style
          worksheet.getCell(`Z${increment + 2}`).value = 'Costo lb'

          worksheet.getCell(`AA${increment + 2}`).style = worksheet.getCell(`AA8`).style
          worksheet.getCell(`AA${increment + 2}`).value = 'Cost/día/Ha'

          worksheet.getCell(`AB${increment + 2}`).style = worksheet.getCell(`AB8`).style
          worksheet.getCell(`AB${increment + 2}`).value = 'Cost Juvenil'

          worksheet.getCell(`AC${increment + 2}`).style = worksheet.getCell(`AC8`).style
          worksheet.getCell(`AC${increment + 2}`).value = '$/lb'

          worksheet.getCell(`AE${increment + 2}`).style = worksheet.getCell(`AE8`).style
          worksheet.getCell(`AE${increment + 2}`).value = '$/día/Ha'

          worksheet.getCell(`AI${increment + 2}`).style = worksheet.getCell(`AI8`).style
          worksheet.getCell(`AI${increment + 2}`).value = 'Alimento Inicio'

          worksheet.getCell(`AJ${increment + 2}`).style = worksheet.getCell(`AJ8`).style
          worksheet.getCell(`AJ${increment + 2}`).value = 'Alimento Engorde'


        }

        worksheet.getCell(`C${increment}`).value = shrimpName

        const shrimpRecords = harvestReports.filter(val => val.pool.shrimp_id === shrimp)

        let totalSeed = 0
        let totalWeightFourth = 0
        let sumKilosFood = 0
        let totalHarvestedAnimals = 0
        let totalTotalPoundsHarvested = 0



        let numFirstRow = 0
        shrimpRecords.forEach((element, key) => {
          if (key === 0) { // Guardar número de primera fila del grupo
            numFirstRow = increment + 3
          }

          // Si hay al menos un valor en la columna J no se debe ocultar
          if (element.pounds_first_thinning) {
            hideJ = false
          }

          // Si hay al menos un valor en la columna J no se debe ocultar
          if (element.weight_first_thinning) {
            hideK = false
          }

          // Si hay al menos un valor en la columna J no se debe ocultar
          if (element.pounds_second_thinning) {
            hideL = false
          }

          // Si hay al menos un valor en la columna J no se debe ocultar
          if (element.weight_second_thinning) {
            hideM = false
          }

          // Si hay al menos un valor en la columna J no se debe ocultar
          if (element.pounds_third_thinning) {
            hideN = false
          }

          // Si hay al menos un valor en la columna J no se debe ocultar
          if (element.weight_third_thinning) {
            hideO = false
          }

          totalSeed += element.seed
          totalWeightFourth += element.weight_fourth_thinning
          sumKilosFood += element.kg_food
          const density = element.seed / element.pool.area
          const days = moment(element.harvest_date).diff(moment(element.seed_time), 'days') + 1
          const weight = weightTotalHarvested(element.pounds_first_thinning, element.pounds_second_thinning, element.pounds_third_thinning, element.pounds_fourth_thinning, element.weight_first_thinning, element.weight_second_thinning, element.weight_third_thinning, element.weight_fourth_thinning)
          const totalPounds = poundsTotalHarvested(element.pounds_first_thinning, element.pounds_second_thinning, element.pounds_third_thinning, element.pounds_fourth_thinning)
          const harvested = getHarvested(totalPounds, weight)

          totalTotalPoundsHarvested += totalPounds
          totalHarvestedAnimals += harvested

          const harvestDensity = getHarvestDensity (harvested, element.pool.area)
          // const growth = weekly(daily(element.weight_fourth_thinning, element.seed_weight, days))
          const dailyGrowth = daily(element.weight_fourth_thinning, element.seed_weight, days)
          const weeklyGrowth = weekly(dailyGrowth)
          const sup = supe(totalPounds, weight, element.seed)
          const poundsHa = getpoundsHa(totalPounds, element.pool.area)
          const fca = getFca(element.kg_food, totalPounds)
          const costLarva = getCostLarva(element.seed, element.youth_cost)
          const costFood = getCostFood(element.cost_kilo_food, element.kg_food)
          const costOperative = getCostOperative(element.us_dollar_day_ha, days, element.dry_days, element.pool.area)
          const costPound = getCostPound(costLarva, costFood, costOperative, totalPounds)
          const saleSale = getSaleSale (element.pounds_first_thinning, element.pounds_second_thinning, element.pounds_third_thinning, element.pounds_fourth_thinning, element.dollar_pound_first_thinning, element.dollar_pound_second_thinning, element.dollar_pound_third_thinning, element.us_dollar_pound)
          const profit = getProfit(saleSale, costOperative, costFood, costLarva)
          const profitDayHa = getProfitDayHa(profit, days, element.pool.area)
          const roi = getRoi(profitDayHa, element.pool.area, days, costPound, totalPounds)
          const rofi = getRofi(profit, element.cost_kilo_food, element.kg_food)


          // Estilos de registros

          worksheet.getCell(`A${increment + 3}`).style = worksheet.getCell(`A9`).style
          worksheet.getCell(`B${increment + 3}`).style = worksheet.getCell(`B9`).style
          worksheet.getCell(`C${increment + 3}`).style = worksheet.getCell(`C9`).style
          worksheet.getCell(`D${increment + 3}`).style = worksheet.getCell(`D9`).style
          worksheet.getCell(`E${increment + 3}`).style = worksheet.getCell(`E9`).style
          worksheet.getCell(`F${increment + 3}`).style = worksheet.getCell(`F9`).style
          worksheet.getCell(`G${increment + 3}`).style = worksheet.getCell(`G9`).style
          worksheet.getCell(`H${increment + 3}`).style = worksheet.getCell(`H9`).style
          worksheet.getCell(`I${increment + 3}`).style = worksheet.getCell(`I9`).style
          worksheet.getCell(`J${increment + 3}`).style = worksheet.getCell(`J9`).style
          worksheet.getCell(`K${increment + 3}`).style = worksheet.getCell(`K9`).style
          worksheet.getCell(`L${increment + 3}`).style = worksheet.getCell(`L9`).style
          worksheet.getCell(`M${increment + 3}`).style = worksheet.getCell(`M9`).style
          worksheet.getCell(`N${increment + 3}`).style = worksheet.getCell(`N9`).style
          worksheet.getCell(`O${increment + 3}`).style = worksheet.getCell(`O9`).style
          worksheet.getCell(`P${increment + 3}`).style = worksheet.getCell(`P9`).style
          worksheet.getCell(`Q${increment + 3}`).style = worksheet.getCell(`Q9`).style
          worksheet.getCell(`R${increment + 3}`).style = worksheet.getCell(`R9`).style
          worksheet.getCell(`S${increment + 3}`).style = worksheet.getCell(`S9`).style
          worksheet.getCell(`T${increment + 3}`).style = worksheet.getCell(`T9`).style
          worksheet.getCell(`U${increment + 3}`).style = worksheet.getCell(`U9`).style
          worksheet.getCell(`V${increment + 3}`).style = worksheet.getCell(`V9`).style
          worksheet.getCell(`W${increment + 3}`).style = worksheet.getCell(`W9`).style
          worksheet.getCell(`X${increment + 3}`).style = worksheet.getCell(`X9`).style
          worksheet.getCell(`Y${increment + 3}`).style = worksheet.getCell(`Y9`).style
          worksheet.getCell(`Z${increment + 3}`).style = worksheet.getCell(`Z9`).style
          worksheet.getCell(`AA${increment + 3}`).style = worksheet.getCell(`AA9`).style
          worksheet.getCell(`AB${increment + 3}`).style = worksheet.getCell(`AB9`).style
          worksheet.getCell(`AC${increment + 3}`).style = worksheet.getCell(`AC9`).style
          worksheet.getCell(`AD${increment + 3}`).style = worksheet.getCell(`AD9`).style
          worksheet.getCell(`AE${increment + 3}`).style = worksheet.getCell(`AE9`).style
          worksheet.getCell(`AF${increment + 3}`).style = worksheet.getCell(`AF9`).style
          worksheet.getCell(`AG${increment + 3}`).style = worksheet.getCell(`AG9`).style
          worksheet.getCell(`AH${increment + 3}`).style = worksheet.getCell(`AH9`).style
          worksheet.getCell(`AI${increment + 3}`).style = worksheet.getCell(`AI9`).style
          worksheet.getCell(`AJ${increment + 3}`).style = worksheet.getCell(`AJ9`).style

          worksheet.getCell(`A${increment + 3}`).value = element.pool.description
          worksheet.getCell(`B${increment + 3}`).value = seedTypes[element.seed_type - 1]
          worksheet.getCell(`C${increment + 3}`).value = parseFloat(element.pool.area)
          worksheet.getCell(`D${increment + 3}`).value =  moment(element.seed_time).format('DD/M/YYYY')
          worksheet.getCell(`E${increment + 3}`).value =  moment(element.harvest_date).format('DD/M/YYYY')
          worksheet.getCell(`F${increment + 3}`).value =  element.laboratory ? element.laboratory.name : ''
          worksheet.getCell(`G${increment + 3}`).value =  parseFloat(density)
          worksheet.getCell(`H${increment + 3}`).value =  parseFloat(element.seed_weight)
          worksheet.getCell(`I${increment + 3}`).value = days ? parseFloat(days) : ''
          worksheet.getCell(`J${increment + 3}`).value = element.pounds_first_thinning ? parseFloat(element.pounds_first_thinning) : ''
          worksheet.getCell(`K${increment + 3}`).value = element.weight_first_thinning ? parseFloat(element.weight_first_thinning) : ''
          worksheet.getCell(`L${increment + 3}`).value = element.pounds_second_thinning ? parseFloat(element.pounds_second_thinning) : ''
          worksheet.getCell(`M${increment + 3}`).value = element.weight_second_thinning ? parseFloat(element.weight_second_thinning) : ''
          worksheet.getCell(`N${increment + 3}`).value = element.pounds_third_thinning ? parseFloat(element.pounds_third_thinning) : ''
          worksheet.getCell(`O${increment + 3}`).value = element.weight_third_thinning ? parseFloat(element.weight_third_thinning) : ''
          worksheet.getCell(`P${increment + 3}`).value =  parseFloat(totalPounds)
          worksheet.getCell(`Q${increment + 3}`).value =  parseFloat(weight)
          worksheet.getCell(`R${increment + 3}`).value = harvestDensity ? parseFloat(harvestDensity) : ''
          worksheet.getCell(`S${increment + 3}`).value = dailyGrowth ? parseFloat(dailyGrowth) : ''
          worksheet.getCell(`T${increment + 3}`).value = weeklyGrowth ? parseFloat(weeklyGrowth) : ''
          worksheet.getCell(`U${increment + 3}`).value = sup ? parseFloat(sup / 100) : ''
          worksheet.getCell(`V${increment + 3}`).value = poundsHa ? parseFloat(poundsHa) : ''
          worksheet.getCell(`W${increment + 3}`).value = fca ? parseFloat(fca) : ''
          worksheet.getCell(`X${increment + 3}`).value = element.dry_days ? parseFloat(element.dry_days) : ''


          if (economicalData && economicalData == 'true') {
            worksheet.getCell(`Y${increment + 3}`).value =  parseFloat(element.cost_kilo_food)
            worksheet.getCell(`Z${increment + 3}`).value =  parseFloat(costPound)
            worksheet.getCell(`AA${increment + 3}`).value =  parseFloat(element.us_dollar_day_ha)
            worksheet.getCell(`AB${increment + 3}`).value =  parseFloat(element.youth_cost)
            worksheet.getCell(`AC${increment + 3}`).value =  parseFloat(element.us_dollar_pound)
            worksheet.getCell(`AD${increment + 3}`).value =  parseFloat(profit)
            worksheet.getCell(`AE${increment + 3}`).value =  parseFloat(profitDayHa)
            worksheet.getCell(`AF${increment + 3}`).value =  parseFloat(roi)
            worksheet.getCell(`AG${increment + 3}`).value =  parseFloat(rofi)
            worksheet.getCell(`AH${increment + 3}`).value =  parseFloat(1 / rofi)
          } else {
            worksheet.getColumn('Y').hidden = true
            worksheet.getColumn('Z').hidden = true
            worksheet.getColumn('AA').hidden = true
            worksheet.getColumn('AB').hidden = true
            worksheet.getColumn('AC').hidden = true
            worksheet.getColumn('AD').hidden = true
            worksheet.getColumn('AE').hidden = true
            worksheet.getColumn('AF').hidden = true
            worksheet.getColumn('AG').hidden = true
            worksheet.getColumn('AH').hidden = true
          }

          worksheet.getCell(`AI${increment + 3}`).value = element.startFoods.length ? element.startFoods[0].product.name : ''
          worksheet.getCell(`AJ${increment + 3}`).value = element.fatteningFoods.length && element.fatteningFoods[0].product ? element.fatteningFoods[0].product.name : ''
          increment++
        })
        increment--
        worksheet.getCell('A3').value = ''

        // Totales de columnas
        // Suma de hectareas de la piscina

        worksheet.getCell(`C${increment + 4}`).value = { formula: `SUM(C${numFirstRow}:C${increment + 3})` }
        // worksheet.getCell(`C${increment + 4}`).model.result = undefined
        worksheet.getCell(`G${increment + 4}`).value = { formula: `${totalSeed}/C${increment + 4}` }
        worksheet.getCell(`H${increment + 4}`).value = { formula: `SUM(H${numFirstRow}:H${increment + 3})` }
        worksheet.getCell(`I${increment + 4}`).value = { formula: `AVERAGE(I${numFirstRow}:I${increment + 3})` }
        worksheet.getCell(`P${increment + 4}`).value = { formula: `SUM(P${numFirstRow}:P${increment + 3})` }
        worksheet.getCell(`Q${increment + 4}`).value = { formula: `(P${increment + 4}*454)/${totalHarvestedAnimals}` }
        worksheet.getCell(`R${increment + 4}`).value = { formula: `${totalHarvestedAnimals}/C${increment + 4}/10000` }
        worksheet.getCell(`S${increment + 4}`).value = { formula: `${totalWeightFourth}/I${increment + 4}` }
        worksheet.getCell(`T${increment + 4}`).value = { formula: `+S${increment + 4}*7` }
        worksheet.getCell(`U${increment + 4}`).value = { formula: `${totalHarvestedAnimals}/${totalSeed}` }
        worksheet.getCell(`V${increment + 4}`).value = { formula: `${totalTotalPoundsHarvested}/C${increment + 4}` }
        worksheet.getCell(`W${increment + 4}`).value = { formula: `(${sumKilosFood}*2.2046)/P${increment + 4}` }

        increment +=6
    })

      // worksheet.spliceColumns(3,2) // Desde la columna 3 elimina 2
      worksheet.getColumn('J').hidden = hideJ
      worksheet.getColumn('K').hidden = hideK
      worksheet.getColumn('L').hidden = hideL
      worksheet.getColumn('M').hidden = hideM
      worksheet.getColumn('N').hidden = hideN
      worksheet.getColumn('O').hidden = hideO

      worksheet.getRow(1).hidden = true
      worksheet.getRow(2).hidden = true
      worksheet.getRow(3).hidden = true
      worksheet.getRow(4).hidden = true
      worksheet.getRow(5).hidden = true

      return workbook.xlsx.writeFile(Helpers.appRoot('ReporteGeneralCosechas.xlsx'))
    })

    /*

    worksheet.insertRow(1,[
      'Pisc', 'Área', 'Tipo', 'Siembra', 'Densidad', 'Peso siembra', 'Edad', 'Cosecha', 'Peso', 'Crec', '%Sup',
      'Cam/m', 'Libras', 'Lbs/Ha', 'FC', 'Costo Kg Alim', 'Costo Lb', 'P&G', 'Utilidad/Ha', 'Utilidad/Día/Ha',
      'ROI', 'ROFI', 'Tipo de Alimento'
    ])

    // Tipos de siembra


    // Inserción de registros
    harvestReports.forEach((element, key) => {
      const density = element.seed / element.pool.area
      const days = moment(element.harvest_date).diff(moment(element.seed_time), 'days') + 1
      const weight = weightTotalHarvested(element.pounds_first_thinning, element.pounds_second_thinning, element.pounds_third_thinning, element.pounds_fourth_thinning, element.weight_first_thinning, element.weight_second_thinning, element.weight_third_thinning, element.weight_fourth_thinning)
      const growth = weekly(daily(element.weight_fourth_thinning, element.seed_weight, days))
      const totalPounds = poundsTotalHarvested(element.pounds_first_thinning, element.pounds_second_thinning, element.pounds_third_thinning, element.pounds_fourth_thinning)
      const sup = supe(totalPounds, weight, element.seed)
      const poundsHa = getpoundsHa(totalPounds, element.pool.area)
      const fca = getFca (element.kg_food, totalPounds)
      const costLarva = getCostLarva(element.seed, element.youth_cost)
      const costFood = getCostFood(element.cost_kilo_food, element.kg_food)
      const costOperative = getCostOperative(element.us_dollar_day_ha, days, element.dry_days, element.pool.area)
      const costPound = getCostPound(costLarva, costFood, costOperative, totalPounds)
      const saleSale = getSaleSale (element.pounds_first_thinning, element.pounds_second_thinning, element.pounds_third_thinning, element.pounds_fourth_thinning, element.dollar_pound_first_thinning, element.dollar_pound_second_thinning, element.dollar_pound_third_thinning, element.us_dollar_pound)
      const profit = getProfit(saleSale, costOperative, costFood, costLarva)
      const profitDayHa = getProfitDayHa(profit, days, element.pool.area)
      const roi = getRoi(profitDayHa, element.pool.area, days, costPound, totalPounds)
      const rofi = getRofi(profit, element.cost_kilo_food, element.kg_food)

      const product = element.fatteningFoods.length ? element.fatteningFoods[element.fatteningFoods.length - 1].product.name : ''
      const record = [
        element.pool.description, element.pool.area, seedTypes[element.seed_type - 1][0],
        moment(element.seed_time).format('ll'), density, element.seed_weight, days,
        moment(element.harvest_date).format('ll'), weight, growth, sup, 'calcular', totalPounds, poundsHa, fca,
        element.cost_kilo_food, costPound, profit, 'calcular2', profitDayHa, roi, rofi, element.fatteningFoods[0].name, product
      ]
      worksheet.insertRow(key + 2, record)
    })

    // res is a Stream object
    response.header(
      "Content-Type",
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    );
    response.header(
      "Content-Disposition",
      "attachment; filename=" + "ReporteGeneralCosechas.xlsx"
    );

    await workbook.xlsx.writeFile(Helpers.appRoot('ReporteGeneralCosechas.xlsx'))

 */    response.download(Helpers.appRoot('ReporteGeneralCosechas.xlsx'))
  }

  async getBulkLoadFormat ({ request, response }) {
    const { shrimpId } = request.get()

    const shrimp = (await Shrimp.query().with('pools').where('id', shrimpId).first()).toJSON()
    const laboratories = (await Laboratory.all()).toJSON()
    const products = (await Product.all()).toJSON()

    var workbook = new ExcelJS.Workbook();

    await workbook.xlsx.readFile('resources/templates/plantilla_carga_masiva_cosechas.xlsx').then(function() {
      console.log('cargo archivo')
      const worksheet = workbook.getWorksheet(1)
      const worksheetInfo = workbook.addWorksheet(`Datos`)

      worksheetInfo.getRow(1).font = { bold: true }
      worksheetInfo.getColumn('A').width = 40
      worksheetInfo.getCell('A1').value = `Piscinas de ${shrimp.name}`
      worksheetInfo.getColumn('B').width = 20
      worksheetInfo.getCell('B1').value = 'Laboratorios'
      worksheetInfo.getColumn('C').width = 20
      worksheetInfo.getCell('C1').value = 'Alimentos'
      // Piscinas
      shrimp.pools.forEach((pool, key) => {
        worksheetInfo.getCell(`A${key + 2}`).value = pool.description
      })

      // Laboratorios
      laboratories.forEach((laboratory, key) => {
        worksheetInfo.getCell(`B${key + 2}`).value = laboratory.name
      })

      // Alimentos
      products.forEach((product, key) => {
        worksheetInfo.getCell(`C${key + 2}`).value = product.name
      })

      worksheet.getCell('A3').value = `CUADRO DE COSECHAS ${shrimp.name}`

      return workbook.xlsx.writeFile(`Carga masiva camaronera ${shrimp.name}.xlsx`);
    })
    response.download(Helpers.appRoot(`Carga masiva camaronera ${shrimp.name}.xlsx`))
  }

  async uploadBulkLoadFormat ({ request, response }) {
    const shrimpId = request.input('shrimpId')
    console.log({ shrimpId })
    // Consultar proyección
    // const projection = (await FoodProjectionMaster.query().where('id', shrimpId).first()).toJSON()

    let dir = `tmp`
    let showingDir = Helpers.appRoot(dir) + '/'

    const bulkLoad = request.file('file', {
      types: ['vnd.openxmlformats-officedocument.spreadsheetml.sheet'],
      size: '40mb'
    })
    let fileName = bulkLoad.clientName.replace(/\s/g, '')
    fileName = fileName.split('-').join('')
    if (bulkLoad) {
      await bulkLoad.move(Helpers.appRoot(dir), {
        name: 'ArchivoTemporal.xlsx',
        overwrite: true
      })

      if (!bulkLoad.moved()) {
        return bulkLoad.error()
      }
      showingDir += 'ArchivoTemporal.xlsx'

      // Leer excel
      const rows = []
      const workbook = new ExcelJS.Workbook();
      await workbook.xlsx.readFile(showingDir).then(() => {
        const worksheet = workbook.getWorksheet(1)
        // Iterate over all rows that have values in a worksheet
        worksheet.eachRow(function(row, rowNumber) {
          // console.log('Row ' + rowNumber + ' = ' + JSON.stringify(row.values))
          // console.log(rowNumber,'---->',row.getCell(1).value)
          // console.log(rowNumber,'---->',row.getCell(2).value)

          if (rowNumber > 8) { // Desde la fila 9 en adelante
            const obj = {
              pool: getExcelCellValue(row.getCell(1).value),
              seedType: getExcelCellValue(row.getCell(2).value),
              seed_time: row.getCell(7).value ? row.getCell(7).value : undefined,
              harvest_date: row.getCell(8).value ? row.getCell(8).value : undefined,
              laboratory: getExcelCellValue(row.getCell(9).value),
              seed: getExcelCellValue(row.getCell(11).value),
              seed_weight: getExcelCellValue(row.getCell(12).value),
              pounds_first_thinning: getExcelCellValue(row.getCell(15).value),
              weight_first_thinning: getExcelCellValue(row.getCell(16).value),
              dollar_pound_first_thinning: getExcelCellValue(row.getCell(17).value),
              pounds_second_thinning: getExcelCellValue(row.getCell(18).value),
              weight_second_thinning: getExcelCellValue(row.getCell(19).value),
              dollar_pound_second_thinning: getExcelCellValue(row.getCell(20).value),
              pounds_third_thinning: getExcelCellValue(row.getCell(21).value),
              weight_third_thinning: getExcelCellValue(row.getCell(22).value),
              dollar_pound_third_thinning: getExcelCellValue(row.getCell(23).value),
              pounds_fourth_thinning: getExcelCellValue(row.getCell(24).value),
              weight_fourth_thinning: getExcelCellValue(row.getCell(25).value),
              kg_food: getExcelCellValue(row.getCell(36).value),
              dry_days: getExcelCellValue(row.getCell(38).value),
              cost_kilo_food: getExcelCellValue(row.getCell(39).value),
              us_dollar_day_ha: getExcelCellValue(row.getCell(41).value),
              youth_cost: getExcelCellValue(row.getCell(42).value),
              us_dollar_pound: getExcelCellValue(row.getCell(46).value),
              startFood: getExcelCellValue(row.getCell(53).value),
              endFood: getExcelCellValue(row.getCell(54).value),
            }

            rows.push(obj)
          }
        })
      })

      for (const element of rows) { // Recorro cada registro para darle el formato y valor correcto para poder insertarlo en BD
        // const record = element
        let startFoodId, endFoodId, harvestReportRecord = undefined
        if (element.pool) { // Tiene el campo piscina
          // console.log('pool', element.pool)
          const pool = await Pool.query().where('shrimp_id', shrimpId).whereRaw('UPPER(description) = ?', [element.pool.toUpperCase()]).first()
          element.pool_id = pool ? pool.toJSON().id : undefined
          delete element.pool
        }

        if (element.seedType) { // Tiene el campo tipo de siembra
          const seedType = seedTypes.find(val => val.label.toUpperCase() == element.seedType.toUpperCase())
          element.seed_type = seedType ? seedType.value : undefined
          delete element.seedType
        }
        //console.log('element.seed_time', element.seed_time)
        if (element.seed_time) {
          element.seed_time = moment(element.seed_time)
        }

        if (element.harvest_date) {
          element.harvest_date = moment(element.harvest_date)
        }

        if (element.laboratory) { // Tiene el campo laboratorio
          const laboratory = await Laboratory.query().whereRaw('UPPER(name) = ?', [element.laboratory.toUpperCase()]).first()
          element.laboratory_id = laboratory ? laboratory.toJSON().id : undefined
          delete element.laboratory
        }

        if (element.startFood) { // Tiene el campo laboratorio
          const startFood = await Product.query().whereRaw('UPPER(name) = ?', [element.startFood.toUpperCase()]).first()
          startFoodId = startFood ? startFood.toJSON().id : undefined
        }

        if (element.endFood) { // Tiene el campo laboratorio
          const endFood = await Product.query().whereRaw('UPPER(name) = ?', [element.endFood.toUpperCase()]).first()
          endFoodId = endFood ? endFood.toJSON().id : undefined
        }

        delete element.startFood
        delete element.endFood

        //console.log(element.pool_id, '-', element.seed_type, '-', element.laboratory_id)
        if (element.pool_id && element.seed_type && element.laboratory_id) {
          // console.log('creando')
          harvestReportRecord = await HarvestReport.create(element)
        }

        // Guardar los alimentos de inicio asociados

        if (harvestReportRecord && startFoodId) {
          await HarvestReportStartFood.create({
            harvest_report_id: harvestReportRecord.id,
            product_id: startFoodId
          })
        }

        // Guardar los alimentos de engorde asociados

        if (harvestReportRecord && endFoodId) {
          await HarvestReportFatteningFood.create({
            harvest_report_id: harvestReportRecord.id,
            product_id: endFoodId
          })
        }

      }
      return rows
    }
  }
}
function weightTotalHarvested (pounds_first_thinning, pounds_second_thinning, pounds_third_thinning, pounds_fourth_thinning, weight_first_thinning, weight_second_thinning, weight_third_thinning, weight_fourth_thinning) {
  const poundsFirstThinning = pounds_first_thinning && typeof parseFloat(pounds_first_thinning) === 'number' ? parseFloat(pounds_first_thinning) : 0
  const poundsSecondThinning = pounds_second_thinning && typeof parseFloat(pounds_second_thinning) === 'number' ? parseFloat(pounds_second_thinning) : 0
  const poundsThirdThinning = pounds_third_thinning && typeof parseFloat(pounds_third_thinning) === 'number' ? parseFloat(pounds_third_thinning) : 0
  const poundsFourthThinning = pounds_fourth_thinning && typeof parseFloat(pounds_fourth_thinning) === 'number' ? parseFloat(pounds_fourth_thinning) : 0
  const weightFirstThinning = weight_first_thinning && typeof parseFloat(weight_first_thinning) === 'number' ? parseFloat(weight_first_thinning) : 0
  const weightSecondThinning = weight_second_thinning && typeof parseFloat(weight_second_thinning) === 'number' ? parseFloat(weight_second_thinning) : 0
  const weightThirdThinning = weight_third_thinning && typeof parseFloat(weight_third_thinning) === 'number' ? parseFloat(weight_third_thinning) : 0
  const weightFourthThinning = weight_fourth_thinning && typeof parseFloat(weight_fourth_thinning) === 'number' ? parseFloat(weight_fourth_thinning) : 0
  const sum = (poundsFirstThinning * 454) + (poundsSecondThinning * 454) + (poundsThirdThinning * 454) + (poundsFourthThinning * 454)

  const val1 = ((poundsFirstThinning * 454) / weightFirstThinning)
  const val2 = ((poundsSecondThinning * 454) / weightSecondThinning)
  const val3 = ((poundsThirdThinning * 454) / weightThirdThinning)
  const val4 = ((poundsFourthThinning * 454) / weightFourthThinning)
  const sum2 = (!isNaN(val1) ? val1 : 0) + (!isNaN(val2) ? val2 : 0) + (!isNaN(val3) ? val3 : 0) + (!isNaN(val4) ? val4 : 0)
  const val = !isNaN(sum / sum2) ? sum / sum2 : 0
  return val.toFixed(2)
}

function daily (weight_fourth_thinning, seed_weight, days) {
  return weight_fourth_thinning && seed_weight && days ? ((weight_fourth_thinning - seed_weight) / days) : ''
}
function weekly (daily) {
  return daily ? (daily * 7).toFixed(2) : ''
}

function supe (poundsTotalHarvested, weightTotalHarvested, seed) {
  return poundsTotalHarvested > 0 && weightTotalHarvested > 0 && seed ? ((poundsTotalHarvested * 454 / weightTotalHarvested / seed) * 100).toFixed(2) : ''
}

function poundsTotalHarvested (pounds_first_thinning, pounds_second_thinning, pounds_third_thinning, pounds_fourth_thinning) {
  const poundsFirstThinning = pounds_first_thinning && typeof parseFloat(pounds_first_thinning) === 'number' ? parseFloat(pounds_first_thinning) : 0
  const poundsSecondThinning = pounds_second_thinning && typeof parseFloat(pounds_second_thinning) === 'number' ? parseFloat(pounds_second_thinning) : 0
  const poundsThirdThinning = pounds_third_thinning && typeof parseFloat(pounds_third_thinning) === 'number' ? parseFloat(pounds_third_thinning) : 0
  const poundsFourthThinning = pounds_fourth_thinning && typeof parseFloat(pounds_fourth_thinning) === 'number' ? parseFloat(pounds_fourth_thinning) : 0
  return parseFloat(poundsFirstThinning) + parseFloat(poundsSecondThinning) + parseFloat(poundsThirdThinning) + parseFloat(poundsFourthThinning)
}

function getpoundsHa (poundsTotalHarvested, poolArea) {
  return poundsTotalHarvested && poolArea ? (poundsTotalHarvested / poolArea).toFixed() : ''
}

function getFca (kg_food, poundsTotalHarvested) {
  return kg_food && poundsTotalHarvested ? ((kg_food * 2.2046) / poundsTotalHarvested).toFixed(2) : ''
}

function getCostPound (costLarva, costFood, costOperative, poundsTotalHarvested) {
  return costLarva && costFood && costOperative && poundsTotalHarvested ? (costLarva + costFood + costOperative) / poundsTotalHarvested : 0
}

function getCostLarva (seed, youth_cost) {
  return seed / 1000 * youth_cost
}
function getCostFood (cost_kilo_food, kg_food) {
  return cost_kilo_food * kg_food
}
function getCostOperative (us_dollar_day_ha, days, dry_days, poolArea) {
  return us_dollar_day_ha * (days + dry_days) * poolArea
}

function getProfit(saleSale, costOperative, costFood, costLarva) {
  return saleSale - costOperative - costFood - costLarva
}

function getSaleSale (pounds_first_thinning, pounds_second_thinning, pounds_third_thinning, pounds_fourth_thinning, dollar_pound_first_thinning, dollar_pound_second_thinning, dollar_pound_third_thinning, us_dollar_pound) {
  const poundsFirstThinning = pounds_first_thinning && typeof parseFloat(pounds_first_thinning) === 'number' ? parseFloat(pounds_first_thinning) : 0
  const poundsSecondThinning = pounds_second_thinning && typeof parseFloat(pounds_second_thinning) === 'number' ? parseFloat(pounds_second_thinning) : 0
  const poundsThirdThinning = pounds_third_thinning && typeof parseFloat(pounds_third_thinning) === 'number' ? parseFloat(pounds_third_thinning) : 0
  return (((pounds_fourth_thinning * 0.95) + ((pounds_fourth_thinning - (pounds_fourth_thinning * 0.95)) * 0.66)) * us_dollar_pound) + (((poundsFirstThinning * 0.95) + ((9 - (9 * 0.95)) * 0.66)) * dollar_pound_first_thinning) + (((poundsSecondThinning * 0.95) + ((poundsSecondThinning - (poundsSecondThinning * 0.95)) * 0.66)) * dollar_pound_second_thinning) + (((poundsThirdThinning * 0.95) + ((poundsThirdThinning - (poundsThirdThinning * 0.95)) * 0.66)) * dollar_pound_third_thinning)
}

function getProfitDayHa (profit, days, poolArea) {
  return profit / days / poolArea
}

function getRoi (profitDayHa, poolArea, days, costPound, poundsTotalHarvested) {
  return profitDayHa && poolArea && days && costPound && poundsTotalHarvested ? ((profitDayHa * poolArea * days) / (costPound * poundsTotalHarvested)) * 100 : 0
}
function getRofi (profit, cost_kilo_food, kg_food) {
  return profit / (cost_kilo_food * kg_food)
}
function getExcelCellValue (cell) {
  let value
  if (cell) {
    if (typeof cell === 'object') {
      value = cell.result
    } else {
      value = cell
    }
  } else {
    value = undefined
  }
  return value
}

function getHarvested (poundsTotalHarvested, weightTotalHarvested) {
  return poundsTotalHarvested && weightTotalHarvested ? (poundsTotalHarvested * 454 / weightTotalHarvested).toFixed() : 0
}
function getHarvestDensity (harvested, poolArea) {
  return harvested && poolArea ? (harvested / poolArea / 10000).toFixed() : 0
}

module.exports = HarvestReportController
