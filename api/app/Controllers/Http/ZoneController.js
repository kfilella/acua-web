'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Zone = use('App/Models/Zone')
const TechnicalChief = use('App/Models/TechnicalChief')
const TechnicalAdvisor = use('App/Models/TechnicalAdvisor')
const TechnicalEquipment = use('App/Models/TechnicalAdvisor')
const List = use('App/Functions/List')
const { validate } = use('Validator')
/**
 * Resourceful controller for interacting with zones
 */
class ZoneController {
  /**
   * Show a list of all zones.
   * GET zones
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view, params }) {
    if (params.status === 'true') {
      var zones = (await Zone.query().where('status', true).fetch()).toJSON()
      zones = List.addRowActions(zones, [{ name: 'edit', route: 'zones/form/', permission: '5.1.3' }, { name: 'delete', permission: '5.1.4' }, { name: 'activate', permission: '5.1.6' }])
    } else if (params.status === 'false') {
      var zones = (await Zone.query().where('status', false).fetch()).toJSON()
      zones = List.addRowActions(zones, [{ name: 'delete', permission: '5.1.4' }, { name: 'activate', permission: '5.1.6' }])
    } else {
      console.log('all')
      var zones = (await Zone.all()).toJSON()
      console.log('zones', zones)
      zones = List.addRowActions(zones, [
        { name: 'edit', route: 'zones/form/', permission: '5.1.3' },
        { name: 'delete', permission: '5.1.4' },
        { name: 'activate', permission: '5.1.6' }])
    }

    for (let j in zones) {
      if (zones[j].status) {
        zones[j].estatus = 'Activo'
        zones[j].actions[2].title = 'Inactivar'
        zones[j].actions[2].color = 'negative'
      } else {
        zones[j].estatus = 'Inactivo'
      }
    }
    response.send(zones)
  }

  async store ({ request, response }) {
    const allRequest = request.all()
    const validation = await validate(allRequest, Zone.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const body = request.only(Zone.fillable)
      body.status = true
      const zone = await Zone.create(body)
      response.send(zone);
    }
  }

  async activate ({ request, response, params }) {
    let technical_chiefs = (await TechnicalChief.all()).toJSON()
    var error = false
    for (let j in technical_chiefs) {
      let element = technical_chiefs[j]
      let technical_zones = JSON.parse(element.zone)
      for (let i in technical_zones) {
        console.log(technical_zones[i], 'technical zones', params.id, 'params id')
        if (technical_zones[i] == params.id) { error = true }
      }
    }
    let technical_advisors = (await TechnicalAdvisor.all()).toJSON()
    for (let j in technical_advisors) {
      let element = technical_advisors[j]
      let technical_zones = JSON.parse(element.zone)
      for (let i in technical_zones) {
        console.log(technical_zones[i], 'technical zones', params.id, 'params id')
        if (technical_zones[i] == params.id) { error = true }
      }
    }

    let technical_equipments = (await TechnicalEquipment.all()).toJSON()
    for (let j in technical_equipments) {
      let element = technical_equipments[j]
      let technical_zones = JSON.parse(element.zone)
      for (let i in technical_zones) {
        console.log(technical_zones[i], 'technical zones', params.id, 'params id')
        if (technical_zones[i] == params.id) { error = true }
      }
    }

    if (!error) {
      var zone = await Zone.query().where('id', params.id).update({
        status: params.status == 'true' ? true : false
      })
      response.send(zone)
    } else {
      response.unprocessableEntity([{message: 'No se puede inactivar una zona que ya esta siendo utilizada', field: 'zone', validation: 'required'}])
    }
  }


  /**
   * Display a single zone.
   * GET zones/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    const zone = (await Zone.query().where('id', params.id).first()).toJSON()
    response.send(zone)
  }

  /**
   * Render a form to update an existing zone.
   * GET zones/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update zone details.
   * PUT or PATCH zones/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const zone =  await Zone.query().where('id', params.id).first()
    if (zone) {
      const validation = await validate(request.all(), Zone.fieldValidationRules())

      if (validation.fails()) {
        response.unprocessableEntity(validation.messages())
      } else {
        const body = request.only(Zone.fillable)
        await Zone.query().where('id', params.id).update(body)
        response.send(zone);
      }
    } else {
      response.unprocessableEntity(validation.messages()) // Aquí debe ir uno de not found 404
    }
  }

  /**
   * Delete a zone with id.
   * DELETE zones/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    let technical_chiefs = (await TechnicalChief.all()).toJSON()
    var error = false
    for (let j in technical_chiefs) {
      let element = technical_chiefs[j]
      let technical_zones = JSON.parse(element.zone)
      for (let i in technical_zones) {
        console.log(technical_zones[i], 'technical zones', params.id, 'params id')
        if (technical_zones[i] == params.id) { error = true }
      }
    }
    let technical_advisors = (await TechnicalAdvisor.all()).toJSON()
    for (let j in technical_advisors) {
      let element = technical_advisors[j]
      let technical_zones = JSON.parse(element.zone)
      for (let i in technical_zones) {
        console.log(technical_zones[i], 'technical zones', params.id, 'params id')
        if (technical_zones[i] == params.id) { error = true }
      }
    }

    let technical_equipments = (await TechnicalEquipment.all()).toJSON()
    for (let j in technical_equipments) {
      let element = technical_equipments[j]
      let technical_zones = JSON.parse(element.zone)
      for (let i in technical_zones) {
        console.log(technical_zones[i], 'technical zones', params.id, 'params id')
        if (technical_zones[i] == params.id) { error = true }
      }
    }

    if (!error) {
      (await Zone.find(params.id)).delete()
      response.send('success')
    } else {
      response.unprocessableEntity([{message: 'No se puede Eliminar una zona que ya esta siendo utilizada', field: 'zone', validation: 'required'}])
    }
  }
}

module.exports = ZoneController
