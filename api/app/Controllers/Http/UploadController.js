'use strict'

const Helpers = use('Helpers')
class UploadController {
  async getFileByDirectory ({ params, response, request }) {
    let dir = params.dir.split('-').join('/')
    // dir = dir.split('%20').join(' ')
    response.download(Helpers.appRoot('storage/uploads') + `/${dir}`)
  }
  async getFileByDirectoryImg ({ params, response, request }) {
    let dir = params.dir.split('-').join('/')
    // dir = dir.split('%20').join(' ')
    response.download(Helpers.appRoot(`storage/uploads/Pathology/${dir}`))
  }
  async getFileByFilename ({ params, response, request }) {
    let dir = params.dir
    // dir = dir.split('%20').join(' ')
    response.download(Helpers.appRoot('storage') + `/temp/${dir}`)
  }
  async getFileByDirectoryPDF ({ params, response, request }) {
    let dir = params.dir + '.pdf'
    dir = dir.split('-').join('/')

    response.download(Helpers.appRoot('storage/uploads') + `/${dir}`)
  }
}

module.exports = UploadController
