'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with providers
 */
const Provider = use('App/Models/Provider')
const ProviderDeliverAddress = use('App/Models/ProviderDeliverAddress')
const ProviderContact = use('App/Models/ProviderContact')
const ProviderCertification = use('App/Models/ProviderCertification')
const ProviderContactPhone = use('App/Models/ProviderContactPhone')
const ProviderContactEmail = use('App/Models/ProviderContactEmail')
const ProviderDocument = use('App/Models/ProviderDocument')
const List = use('App/Functions/List')
const Helpers = use('Helpers')
const { validate } = use('Validator')
class ProviderController {
  /**
   * Show a list of all providers.
   * GET providers
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ response }) {
    let providers = (await Provider.all()).toJSON()
    providers = List.addRowActions(providers, [{name: 'edit', route: 'providers/form/', permission: '2.5'}, {name: 'delete', permission: '2.4'}])
    response.send(providers)
  }

  /**
   * Create/save a new provider.
   * POST providers
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ auth, request, response }) {
    const validation = await validate(request.all(), Provider.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const wholeRequest = request.all()
      const body = request.only(Provider.fillable)
      body.product_categories = JSON.stringify(body.product_categories)
      const provider = await Provider.create(body)
      // Después de crear el cliente se deben guardar sus direcciones de despacho y sus contactos
      // Averiguar si el attach y dettach se puede con una relación de 1 a muchos
      for (const i of wholeRequest.deliver_addresses) {
        await ProviderDeliverAddress.create({
          provider_id: provider.id,
          address: i
        })
      }

      for (const i of wholeRequest.contacts) {
        const contact =  await ProviderContact.create({
          provider_id: provider.id,
          last_name: i.last_name,
          name: i.name,
          job: i.job,
          phone: i.phone,
          email: i.email
        })

        for (const j of i.phones) {
          await ProviderContactPhone.create({
            provider_contact_id: contact.id,
            phone: j
          })
        }

        for (const j of i.emails) {
          await ProviderContactEmail.create({
            provider_contact_id: contact.id,
            email: j
          })
        }
        // Añadir detalles de contacto, otros números de teléfono y otros correos relacionados a un contacto
      }

      response.send(provider);
    }
  }

  /**
   * Display a single provider.
   * GET providers/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    const providerId = params.id
    const provider = (await Provider.query().with('deliver_addresses').with('contacts.phones').with('contacts.emails').with('certifications').with('documents').where('id', providerId).first()).toJSON()
    if (provider) {
      provider.deliver_addresses = provider.deliver_addresses.map(value => value.address)
      provider.product_categories = JSON.parse(provider.product_categories)
      for (const i of provider.contacts) {
        i.phones = i.phones.map(value => value.phone)
        i.emails = i.emails.map(value => value.email)
      }

    }
    response.send(provider)
  }

  /**
   * Update provider details.
   * PUT or PATCH providers/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const validation = await validate(request.all(), Provider.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const wholeRequest = request.all()
      const body = request.only(Provider.fillable)
      const providerId = params.id
      const provider = await Provider.query().where('id', providerId).update(body)

      // Después de actualizar el cliente se deben guardar sus direcciones de despacho y sus contactos
      // Averiguar si el attach y dettach se puede con una relación de 1 a muchos
      // Eliminar las direcciones de despacho del cliente e insertar nuevamente

      await ProviderDeliverAddress.query().where('provider_id', providerId).delete()

      for (const i of wholeRequest.deliver_addresses) {

        await ProviderDeliverAddress.create({
          provider_id: providerId,
          address: i
        })
      }

      for (const i of wholeRequest.contacts) {
        // const contacts = await Contact.query().where('customer_id', providerId)
        await ProviderContact.query().where('provider_id', providerId).delete()
        const contact = await ProviderContact.create({
          provider_id: providerId,
          last_name: i.last_name,
          name: i.name,
          job: i.job,
          phone: i.phone,
          email: i.email
        })
        // Añadir detalles de contacto, otros números de teléfono y otros correos relacionados a un contacto
        for (const j of i.phones) {
          // await ContactPhone.query().where('contact_id', contact.id).delete()
          await ProviderContactPhone.create({
            contact_id: contact.id,
            phone: j
          })
        }

        for (const j of i.emails) {
          // await ContactEmail.query().where('contact_id', contact.id).delete()
          await ProviderContactEmail.create({
            contact_id: contact.id,
            email: j
          })
        }
      }

      response.send(provider);
    }
  }

  /**
   * Delete a provider with id.
   * DELETE providers/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }

  async uploadProviderCertifications ({ request }) {
    // let user = await auth.getUser()
    const providerId = request.input('provider_id')
    const description = request.input('description')
    const certificationId = request.input('certification_id')
    let dir = `storage/uploads/providers/${providerId}/certifications`
    let showingDir = `providers-${providerId}-certifications`

    const certificationFile = request.file('file', {
      types: ['image'],
      size: '8mb'
    })
    if (certificationFile) {
      await certificationFile.move(Helpers.appRoot(dir), {
        name: certificationFile.clientName,
        overwrite: true
      })

      if (!certificationFile.moved()) {
        return certificationFile.error()
      }

      // Después de guardado el archivo se realiza el guardado de los datos en base de datos
      const providerCertification = await ProviderCertification.query().where('id', certificationId).first()
      if (providerCertification) {
        providerCertification.description = description
        providerCertification.filename = showingDir + '-' + certificationFile.fileName
      } else {
        await ProviderCertification.create({
          provider_id: providerId,
          description: description,
          filename: showingDir + '-' + certificationFile.fileName
        })
      }

    }

  }
  async uploadProviderDocuments ({ request }) {
    const providerId = request.input('provider_id')
    const type = request.input('type')
    const certificationId = request.input('certification_id') ? request.input('certification_id') : 0
    const dir = `storage/uploads/providers/${providerId}/documents`
    const showingDir = `providers-${providerId}-documents`

    const providerDocumentFile = request.file('file', {
      types: ['image'],
      size: '8mb'
    })

    //console.log(providerDocumentFile)

    if (providerDocumentFile) {
      await providerDocumentFile.move(Helpers.appRoot(dir), {
        name: providerDocumentFile.clientName,
        overwrite: true
      })

      if (!providerDocumentFile.moved()) {
        return providerDocumentFile.error()
      }

      // Después de guardado el archivo se realiza el guardado de los datos en base de datos
      const providerDocument = await ProviderDocument.query().where('id', certificationId).first()
      console.log('pro', providerDocument)
      if (providerDocument) {
        providerDocument.type = type
        providerDocument.filename = showingDir + '-' + providerDocument.fileName
      } else {
        console.log('aqui')
        await ProviderDocument.create({
          provider_id: providerId,
          type,
          filename: showingDir + '-' + providerDocumentFile.fileName
        })
      }

    }
  }
}

module.exports = ProviderController
