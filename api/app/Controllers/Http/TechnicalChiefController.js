'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const TechnicalChief = use('App/Models/TechnicalChief')
const List = use('App/Functions/List')
const { validate } = use('Validator')

/**
 * Resourceful controller for interacting with technicalchiefs
 */
class TechnicalChiefController {
  /**
   * Show a list of all technicalchiefs.
   * GET technicalchiefs
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    const params = request.get()
    let technical_chiefs
    if (Object.keys(params).length) { // si recibe parámetros desde el front se implementa en el where
      technical_chiefs = (await TechnicalChief.query().where(params).fetch()).toJSON()
    } else {
      technical_chiefs = (await TechnicalChief.all()).toJSON()
    }

    technical_chiefs = List.addRowActions(technical_chiefs, [{name: 'edit', route: 'technical_chiefs/form/', permission: '5.2.5'}, {name: 'delete', permission: '5.2.4'}, { name: 'activateTechnicalChief', permission: '5.2.6' }])
    for (let j in technical_chiefs) {
      if (technical_chiefs[j].status) {
        technical_chiefs[j].estatus = 'Activo'
        technical_chiefs[j].actions[2].title = 'Inactivar'
        technical_chiefs[j].actions[2].color = 'negative'
      } else {
        technical_chiefs[j].estatus = 'Inactivo'
      }
    }
    response.send(technical_chiefs)
  }

  async store ({ request, response }) {
    const allRequest = request.all()
    const validation = await validate(allRequest, TechnicalChief.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const body = request.only(TechnicalChief.fillable)
      console.log(body, 'bodyyy')
      body.status = true
      body.zone = JSON.stringify(allRequest.zone)
      const technical_chiefs = await TechnicalChief.create(body)
      response.send(technical_chiefs);
    }
  }

  async show ({ params, request, response, view }) {
    let technical_chiefs = (await TechnicalChief.query().where('id', params.id).first()).toJSON()
    console.log(technical_chiefs, 'aquii')
    let zone = technical_chiefs.zone
    technical_chiefs.zone = JSON.parse(zone)
    response.send(technical_chiefs)
  }

  async activate ({ request, response, params }) {
    let technical_chiefs = await TechnicalChief.query().where('id', params.id).update({
      status: params.status == 'true' ? true : false
    })
    response.send(technical_chiefs)
  }

  async activateList ({ request, response, params }) {
    let technical_chiefs = (await TechnicalChief.query().where({
      status: params.status == 'true' ? true : false
    }).fetch()).toJSON()
    technical_chiefs = List.addRowActions(technical_chiefs, [{name: 'edit', route: 'technical_chiefs/form/', permission: '5.2.5'}, {name: 'delete', permission: '5.2.4'}, { name: 'activateTechnicalChief', permission: '5.2.6' }])
    for (let j in technical_chiefs) {
      if (technical_chiefs[j].status) {
        technical_chiefs[j].estatus = 'Activo'
        technical_chiefs[j].actions[2].title = 'Inactivar'
        technical_chiefs[j].actions[2].color = 'negative'
      } else {
        technical_chiefs[j].estatus = 'Inactivo'
      }
    }
    response.send(technical_chiefs)
  }

  async update ({ params, request, response }) {
    const technical_chiefs =  await TechnicalChief.query().where('id', params.id).first()
    const allRequest = request.all()
    if (technical_chiefs) {
      const validation = await validate(request.all(), TechnicalChief.fieldValidationRules())

      if (validation.fails()) {
        response.unprocessableEntity(validation.messages())
      } else {
        const body = request.only(TechnicalChief.fillable)
        body.zone = JSON.stringify(allRequest.zone)
        await TechnicalChief.query().where('id', params.id).update(body)
        response.send(technical_chiefs);
      }
    } else {
      response.unprocessableEntity(validation.messages()) // Aquí debe ir uno de not found 404
    }
  }

  async destroy ({ params, request, response }) {
    (await TechnicalChief.find(params.id)).delete()
    response.send('success')
  }
}

module.exports = TechnicalChiefController
