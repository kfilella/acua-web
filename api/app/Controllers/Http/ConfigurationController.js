'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with configurations
 */
const Configuration = use('App/Models/Configuration')
const { validate } = use('Validator')
class ConfigurationController {
  /**
   * Show a list of all configurations.
   * GET configurations
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    response.send(await Configuration.query().where('id', 1).first())
  }

  /**
   * Create/save a new configuration.
   * POST configurations
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const body = request.all()
    // Ver si está el primer registro, si no está se crea
    if (await Configuration.query().where('id', 1).first()) {
      await Configuration.query().where('id', 1).update(body)
    } else {
      await Configuration.create(body)
    }
      
    response.send(true)
  }

  /**
   * Display a single configuration.
   * GET configurations/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Update configuration details.
   * PUT or PATCH configurations/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a configuration with id.
   * DELETE configurations/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = ConfigurationController
