'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with shrimpprices
 */
const { validate } = use('Validator')
const ShrimpPrice = use('App/Models/ShrimpPrice')
const ShrimpPriceFunctions = use('App/Functions/ShrimpPrice')
const moment = require('moment')
class ShrimpPriceController {
  /**
   * Show a list of all shrimpprices.
   * GET shrimpprices
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    const shrimpPrices = (await ShrimpPrice.query().orderBy('date').fetch()).toJSON()
    shrimpPrices.forEach(element => {
      element.date = moment(element.date).format('DD-MM-YYYY')
    })
    response.send(shrimpPrices)
  }

  /**
   * Create/save a new shrimpprice.
   * POST shrimpprices
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const validation = await validate(request.all(), ShrimpPrice.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const body = request.only(ShrimpPrice.fillable)
      switch (body.range_id) {
        case 1:
          body.average_size = 8.75
          break;
        case 2:
          body.average_size = 10.75
          break;
        case 3:
          body.average_size = 12.75
          break;
        case 4:
          body.average_size = 15
          break;
        case 5:
          body.average_size = 18.50
          break;
        case 6:
          body.average_size = 23
          break;
        case 7:
          body.average_size = 28.50
          break;
        case 8:
          body.average_size = 36
          break;
        default:
          body.average_size = null
      }
      const shrimpPrice = await ShrimpPrice.create(body)
      response.send(shrimpPrice)
    }
  }

  /**
   * Display a single shrimpprice.
   * GET shrimpprices/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, response }) {
    const shrimpPrice = await ShrimpPrice.query().where('id', params.id).first()
    shrimpPrice.date = moment(shrimpPrice.date).format('YYYY-MM-DD')
    response.send(shrimpPrice)
  }

  /**
   * Update shrimpprice details.
   * PUT or PATCH shrimpprices/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const shrimpPrice = await ShrimpPrice.query().where('id', params.id).first()
    if (shrimpPrice) {
      const validation = await validate(request.all(), ShrimpPrice.fieldValidationRules())

      if (validation.fails()) {
        response.unprocessableEntity(validation.messages())
      } else {
        const body = request.only(ShrimpPrice.fillable)

        await ShrimpPrice.query().where('id', params.id).update(body)
        response.send(shrimpPrice)
      }
    } else {
      response.notFound(validation.messages())
    }
  }

  /**
   * Delete a shrimpprice with id.
   * DELETE shrimpprices/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }

  /**
   * Obtener último precio de camarón
   * GET shrimp_prices_last
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async getLastPrice ({ params, request, response }) {
    const lastPrice = await ShrimpPrice.query().orderBy('date', 'desc').first()
    response.send(lastPrice)
  }

  /**
   * Show a list of all shrimpprices ranges.
   * GET shrimpprices
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
   async getShrimpPricesRanges ({ request, response, view }) {
    const shrimpPricesRanges = ShrimpPriceFunctions.getRanges()
    for (const range of shrimpPricesRanges) {
       const lastPrice = await ShrimpPrice.query().where('range_id', range.id).orderBy('date', 'desc').first()
       range.lastPrice = lastPrice ? lastPrice.price : 0
    }
    response.send(shrimpPricesRanges)
  }

}

module.exports = ShrimpPriceController
