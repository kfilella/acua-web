'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with foodtypes
 */
const FoodType = use('App/Models/FoodType')
const { validate } = use('Validator')
class FoodTypeController {
  /**
   * Show a list of all foods.
   * GET foods
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view, params }) {
    const records = (await FoodType.all()).toJSON()
    records.forEach(element => {
      element.range = element.from_weight + ' - ' + element.to_weight
    })
    response.send(records)
  }

  /**
   * Create/save a new food.
   * POST foods
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const date = request.all()
    const validation = await validate(request.all(), FoodType.fieldValidationRules())
    if (!validation.fails()) {
      let band = true
      let verify = (await FoodType.all()).toJSON()
      let ptu = []
      for (let i in verify) {
        for (let k = parseFloat(verify[i].from_weight); k<=parseFloat(verify[i].to_weight); k+= 0.1) {
          ptu.push(k.toFixed(2))
        }
      }
      for (let i = parseFloat(date.from_weight); i<= parseFloat(date.to_weight); i+=0.1) {
        if (ptu.includes(i.toFixed(2))) {
          band = false
        }
      }
      if (band) { // verifica si ya estan esos rangos guardados
        let body = request.only(FoodType.fillable)
        await FoodType.create(body)
        response.status(201).send({
          body,
          status: false,
          to: '/foods'
        })
      } else {
        return response.status(200).send({
          status: true,
          message: 'Ya existe un alimento con esos Rangos',
          icon: 'warning',
          color: 'negative'
        })
      }
    } else {
      response.unprocessableEntity(validation.messages())
    }
  }

  /**
   * Display a single food.
   * GET foods/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    /* let foods = (await Food.all()).toJSON()
    const List = []
    for (let i in foods) {
      List.push({'id': foods[i].id, 'code': foods[i].code, 'type': foods[i].type, 'price_kg': foods[i].price_kg, 'range': foods[i].from_weight + ' - ' + foods[i].to_weight, 'actions': [{name: 'edit', route: 'foods/form/', permission: '4.23'}, {name: 'delete', permission: '4.24'}]})
    }
    response.send(List) */
    response.send(await FoodType.query().where('id', params.id).first())
  }

  /**
   * Update food details.
   * PUT or PATCH foods/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const validation = await validate(request.all(), FoodType.fieldValidationRules())
    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const body = request.only(FoodType.fillable)
      const id = params.id
      response.send((await FoodType.query().where('id', id).update(body)))
    }
  }

  /**
   * Delete a food with id.
   * DELETE foods/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    response.send(await FoodType.query().where('id', params.id).delete())
  }
}

module.exports = FoodTypeController
