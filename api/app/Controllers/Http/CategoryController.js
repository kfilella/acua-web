'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with categories
 */
const { validate } = use('Validator')
const List = use('App/Functions/List')
const Category = use('App/Models/Category')
class CategoryController {
  /**
   * Show a list of all categories.
   * GET categories
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response }) {
    const query = request.get()
    if (query.type) {
      let categories = (await Category.query().where('type', query.type).fetch()).toJSON()
      categories = List.addRowActions(categories, [{ name: 'edit', route: 'customers_categories/form/', permission: '2.10' }, { name: 'delete', permission: '2.9' }])
      response.send(categories)
    } else {
      response.unprocessableEntity('Undefined type')
    }
  }

  /**
   * Create/save a new category.
   * POST categories
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const validation = await validate(request.all(), Category.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const body = request.only(Category.fillable)
      const found = await Category.query().where('name', body.name).first()
      if (found) {
        response.unprocessableEntity('Categoría ya existe')
      } else {
        const category = await Category.create(body)
        response.send(category)
      }
    }
  }

  /**
   * Display a single category.
   * GET categories/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    const category = await Category.find(params.id)
    response.send(category)
  }

  /**
   * Update category details.
   * PUT or PATCH categories/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const category = await Category.query().where('id', params.id).first()
    if (category) {
      const validation = await validate(request.all(), Category.fieldValidationRules())

      if (validation.fails()) {
        response.unprocessableEntity(validation.messages())
      } else {
        const body = request.only(Category.fillable)
        for (const i in body) {
          category[i] = body[i]
        }
        category.save()
        response.send(category);
      }
    } else {
      response.unprocessableEntity(validation.messages()) // Aquí debe ir uno de not found 404
    }
  }

  /**
   * Delete a category with id.
   * DELETE categories/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    const category = await Category.find(params.id)
    category.delete()
    response.send('success')
  }
}

module.exports = CategoryController
