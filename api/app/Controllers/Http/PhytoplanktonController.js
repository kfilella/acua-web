'use strict'

const PhytoplanktonMaster = use('App/Models/PhytoplanktonAnalysisPool')
const PhytoplanktonDetail = use('App/Models/PhytoplanktonAnalysisDetail')
const Shrimp = use('App/Models/Shrimp')
const Contact = use('App/Models/Contact')
const { validate } = use('Validator')
const List = use('App/Functions/List')
const Email = use('App/Functions/Email')
const moment = require('moment')
const Helpers = use("Helpers")
const PdfPrinter = use("pdfmake")
const fs = use("fs")

// Define font files
var fonts = {
  Roboto: {
    normal: 'resources/fonts/Roboto-Regular.ttf',
    bold: 'resources/fonts/Roboto-Medium.ttf',
    italics: 'resources/fonts/Roboto-Italic.ttf',
    bolditalics: 'resources/fonts/Roboto-MediumItalic.ttf'
  }
}
class PhytoplanktonController {

  async index ({ request, response }) {
    const params = request.get()
    const shrimp = await Shrimp.find(params.shrimp_id)
    let phytoplankton_analysis = (await PhytoplanktonMaster.query().with('user.technical_advisor').with('zone').with('pools.pool')
    .with('shrimp.customer.contacts').where(params).orderBy('created_at', 'desc').fetch()).toJSON()
    phytoplankton_analysis.forEach(element => {
      if (element.applicant_id == 0) {
        element.applicant = 'Cliente'
      } else {
        let contact =  (element.shrimp.customer.contacts.find(val => val.id === element.applicant_id))
        element.applicant = contact ? `${contact.name} ${contact.last_name}` : 'Contacto eliminado'
      }
      element.analysis_date = moment(element.analysis_date).format("DD-MM-YYYY")
      element.sampling_date = moment(element.sampling_date).format("DD-MM-YYYY")
      // En cada análisis mostrar en un campo las piscinas del análisis. Filtro primero para evitar errores si se han eliminado piscinas
      element.pools = element.pools.filter(val => val.pool).map(val => val.pool.description).join(',')
    })

    phytoplankton_analysis = List.addRowActions(phytoplankton_analysis, [
      { name: 'show', route: `/customers/show/${shrimp.customer_id}/shrimps/${params.shrimp_id}/phytoplanktons/form/`, permission: '2.1.1.1.34' },
      { name: 'printPhytoplanktonAnalysis', permission: '2.1.1.1.33' },
      { name: 'generateLinkPhytoplanktonAnalysis', permission: '2.1.1.1.33' },
      { name: 'sendMailPhytoplankton', permission: '2.1.1.1.14', route: 'phytoplanktons/send_mail/'}
    ])
    response.send(phytoplankton_analysis)
  }

  async sendMail ({ request, response, params }) {
    const { to, cc, content } = request.all()
    const analysisId = params.analysisId
    const filename = `Analisis_fitoplancton_${analysisId}.pdf`
    await Email.send({
      to,
      subject: 'Análisis fitoplancton',
      content,
      cc,
      attachments: [
        {
          filename,
          path: Helpers.appRoot('storage/uploads/phytoplanctonanalisis') + `/${filename}`
        }
      ]
    })
    response.send(true)
  }

  async sendMailInfo ({ request, response, params }) {
    const analysisId = params.analysisId

    const analysis = (await PhytoplanktonMaster.query().with('applicant').with('shrimp.customer.contacts').where('id', analysisId).first()).toJSON()

    const form_emails = {
      to: analysis.applicant.email,
      contacts_email: analysis.shrimp.customer.contacts.map(val => (
        {
          email: val.email || 'Sin correo',
          name: `${val.name} ${val.last_name}`,
          disable: val.email == null || val.email == ''
        }
      )),
      email_customer: analysis.shrimp.customer.electronic_documents_email
    }
    response.send(form_emails)
  }

  async store ({ request, response, params, auth }) {
    const user = await auth.getUser()
    const wholeRequest = request.all()
    const pools = wholeRequest.pools
    wholeRequest.technical_advisor_id = user.id
    const validation = await validate(wholeRequest, PhytoplanktonMaster.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const body = request.only(PhytoplanktonMaster.fillable)
      const phytoplankton_analysis = await PhytoplanktonMaster.create(body)

      // Guardar detalles de piscinas
      for (let i of pools) {
        delete i.description
        delete i.expanded
        i.phytoplankton_analysis_pools_id = phytoplankton_analysis.id
        // Eliminar los valores vacíos de objeto a insertar
        i = cleanBlankValues(i)
        await PhytoplanktonDetail.create(i)
      }

      response.send(phytoplankton_analysis)
    }
  }

  async show ({ params, request, response }) {
    const phytoplankton_analysis = (await PhytoplanktonMaster.query().with('pools.pool').where('id', params.id).first()).toJSON()
    phytoplankton_analysis.delivery_date = moment(phytoplankton_analysis.delivery_date).format("YYYY-MM-DD")
    phytoplankton_analysis.analysis_date = moment(phytoplankton_analysis.analysis_date).format("YYYY-MM-DD")
    phytoplankton_analysis.sampling_date = moment(phytoplankton_analysis.analysis_date).format("YYYY-MM-DD")
    phytoplankton_analysis.pools.forEach(element => {
      element.description = element.pool.description
      element.expanded = true
    })
    response.send(phytoplankton_analysis)
  }

  async update ({ params, request, response }) {
    const wholeRequest = request.all()
    const pools = wholeRequest.pools

    const phytoplanktonAnalysis = await PhytoplanktonMaster.query().where('id', params.id).first()
    if (phytoplanktonAnalysis) {
      const validation = await validate(request.all(), PhytoplanktonMaster.fieldValidationRules())

      if (validation.fails()) {
        response.unprocessableEntity(validation.messages())
      } else {
        const body = request.only(PhytoplanktonMaster.fillable)

        await PhytoplanktonMaster.query().where('id', params.id).update(body)

        //Actualizar datos de piscinas
        const poolIds = []
        for (let i of pools) {
          delete i.pool
          delete i.description
          delete i.expanded
          i = cleanBlankValues(i)
          if (i.id) {
            await PhytoplanktonDetail.query().where('id', i.id).update(i)
            poolIds.push(i.id)
          } else {
            i.phytoplankton_analysis_pools_id = params.id
            let detailId = await PhytoplanktonDetail.create(i)
            poolIds.push(detailId.id)
          }
        }
        await PhytoplanktonDetail.query().where('phytoplankton_analysis_pools_id', params.id).whereNotIn('id', poolIds).delete()
        response.send(phytoplanktonAnalysis)
      }
    } else {
      response.notFound(validation.messages())
    }
  }

  async destroy ({ params, request, response }) {
    response.send(await Phytoplankton.query().where('id', params.id).delete())
  }

  async printPhytoplanktonAnalysis ({ params, response }) {
    var printer = new PdfPrinter(fonts)
    const phytoplanctonanalisis = (await PhytoplanktonMaster.query().with('pools.pool').with('shrimp.zone').with('user.technical_advisor')
    .with('shrimp.customer.contacts').with('applicant').where('id', params.id).first()).toJSON()
    let widths = []
    let pools = phytoplanctonanalisis.pools

    
    let space1 = []
    let subtitle2 = []
    let subtitle3 = []
    let subtitle4 = []
    let subtitle5 = []
    let subtitle6 = []
    let subtitle7 = []

    let pcyclotellah = [{text: 'Cyclotella', fontSize: 10}]
    let coscinodiscus = [{text: 'Coscinodiscus', fontSize: 10}]
    let chaetoceros = [{text: 'Chaetoceros', fontSize: 10}]
    let melosira = [{text: 'Melosira', fontSize: 10}]
    let nitzschia = [{text: 'Nitzschia', fontSize: 10}]
    let skelotonema = [{text: 'Skelotonema', fontSize: 10}]
    let navicles = [{text: 'Navículas', fontSize: 10}]
    let thalassiosira = [{text: 'Thalassiosira', fontSize: 10}]
    let totalDiatomea = [{text: 'Total', fontSize: 10}]
    let percentDiatomea = [{text: '%', fontSize: 10}]

    let ceratium = [{text: 'Ceratium', fontSize: 10}]
    let peridinium =  [{text:'Peridinium', fontSize: 10}]
    let totalDino =  [{text:'Total', fontSize: 10}]

    let chlamydomonas = [{text: 'Chlamydomonas', fontSize: 10}]
    let scenedesmus = [{text: 'Scenedesmus', fontSize: 10}]
    let chlorella = [{text: 'Clorella', fontSize: 10}]
    let totalChlorella = [{text: 'Total', fontSize: 10}]
    let percentChlorella = [{text: '%', fontSize: 10}]

    let anabaena = [{text: 'Anabaena', fontSize: 10}]
    let oscillatory = [{text: 'Oscillatoria', fontSize: 10}]
    let nostoc = [{text: 'Nostoc', fontSize: 10}]
    let anacystis = [{text: 'Anacystis', fontSize: 10}]
    let totalAnacystis = [{text: 'Total', fontSize: 10}]
    let percentAnacystis = [{text: '%', fontSize: 10}]

    let protozoa = [{text: 'Protozoarios', fontSize: 10}]
    let euglena = [{text: 'Euglena', fontSize: 10}]
    let totalOthers = [{text: 'Total', fontSize: 10}]
    let totalFinal = [{text: 'CONTEO TOTAL', fontSize: 10, bold: true}]


    let observations = []
    let allObservations = []
    let recommendations = []
    let allRecommendations = []
    let tables = []
    let recomendate = []
    // return phytoplanctonanalisis
    moment.locale('es')
    phytoplanctonanalisis.delivery_date = moment(phytoplanctonanalisis.delivery_date).format("DD-MMM-YYYY")
    phytoplanctonanalisis.analysis_date = moment(phytoplanctonanalisis.analysis_date).format("DD-MMM-YYYY")
    phytoplanctonanalisis.sampling_date = moment(phytoplanctonanalisis.sampling_date).format("DD-MMM-YYYY")
    if (phytoplanctonanalisis.pools.length === 0) {
        // pools.unshift({ text: 'Piscinas', bold: true })
        /* widths.push('*')
        let subtitle1 = [{text: '-', fontSize: 10, colSpan: 6}]
        space1.push({ text: ' ', border: [false, false, false, false] })
        subtitle2.push({text: '-', fontSize: 10})
        subtitle3.push({text: '-', fontSize: 10})
        subtitle4.push({text: '-', fontSize: 10})
        subtitle5.push({text: '-', fontSize: 10})
        subtitle6.push({text: '-', fontSize: 10})
        subtitle7.push({text: '-', fontSize: 10})
        widths.push('*')
        subtitle1.push({text: '', fontSize: 10})
        subtitle6.push('')
        subtitle7.push('')
        space1.push({ text: ' ', border: [false, false, false, false] })
        subtitle2.push({text: '', colSpan: subtitle1.length-1, alignment: 'center', fontSize: 10})
        subtitle3.push({text: '', colSpan: subtitle1.length-1, alignment: 'center', fontSize: 10})
        subtitle4.push({text: '', colSpan: subtitle1.length-1, alignment: 'center', fontSize: 10})
        subtitle5.push({text: '', colSpan: subtitle1.length-1, alignment: 'center', fontSize: 10})
        pcyclotellah.push({text: '-', fontSize: 10})
        coscinodiscus.push({text: '-', fontSize: 10})
        chaetoceros.push({text: '-', fontSize: 10})
        melosira.push({text: '-', fontSize: 10})
        nitzschia.push({text: '-', fontSize: 10})
        skelotonema.push({text: '-', fontSize: 10})
        navicles.push({text: '-', fontSize: 10})
        thalassiosira.push({text: '-', fontSize: 10})
        totalDiatomea.push({text: '-', fontSize: 10})
        percentDiatomea.push({text: '-', fontSize: 10})

        ceratium.push({text: '-', fontSize: 10})
        peridinium.push({text: '-', fontSize: 10})
        totalDino.push({text: '-', fontSize: 10})

        chlamydomonas.push({text: '-', fontSize: 10})
        scenedesmus.push({text: '-', fontSize: 10})
        chlorella.push({text: '-', fontSize: 10})
        totalChlorella.push({text: '-', fontSize: 10})
        percentChlorella.push({text: '-', fontSize: 10})

        anabaena.push({text: '-', fontSize: 10})
        oscillatory.push({text: '-', fontSize: 10})
        nostoc.push({text: '-', fontSize: 10})
        anacystis.push({text: '-', fontSize: 10})
        totalAnacystis.push({text: '-', fontSize: 10})
        percentAnacystis.push({text: '-', fontSize: 10})

        protozoa.push({text: '-', fontSize: 10})
        euglena.push({text: '-', fontSize: 10})
        totalOthers.push({text: '-', fontSize: 10})

        allObservations.push({text:'No se seleccionó piscina', fontSize: 10})
        observations.push('')
        allRecommendations.push({text:'No se seleccionó piscina', fontSize: 10})
        recommendations.push('') */
    } else {
      let cont = 0
      
      let blankColumns = Array(5 - pools.length).fill('')
      pools = pools.concat(blankColumns)
//      return pools
      let subtitle1 = [{ text: 'DIATOMEAS (CEL/ML)', bold: true , fontSize: 10, colSpan: pools.length + 1}, '', '', '', '', '']
      for (const i in pools) {
        /* console.log({subtitle1})
         */
       // console.log({pcyclotellah})
        widths.push('*')
        cont = cont + 1
        // subtitle1.push({text:`${pools[i].pool ? pools[i].pool.description : ''}`, fontSize: 10, bold:true, colSpan: pools.length})
        subtitle6.push('')
        subtitle7.push('')
        pcyclotellah.push({text: formatValue(pools[i].pcyclotellah), fontSize: 10})
        coscinodiscus.push({text: formatValue(pools[i].coscinodiscus), fontSize: 10})
        chaetoceros.push({text: formatValue(pools[i].chaetoceros), fontSize: 10})
        melosira.push({text: formatValue(pools[i].melosira), fontSize: 10})
        nitzschia.push({text: formatValue(pools[i].nitzschia), fontSize: 10})
        skelotonema.push({text: formatValue(pools[i].skelotonema), fontSize: 10})
        navicles.push({text: formatValue(pools[i].navicles), fontSize: 10})
        thalassiosira.push({text: formatValue(pools[i].thalassiosira), fontSize: 10})
        totalDiatomea.push({ text: formatValue(percentDiatoms(pools[i]).sum), fontSize: 10, bold: true, background: 'yellow'})
        percentDiatomea.push({text: percentDiatoms(pools[i]).percent ? percentDiatoms(pools[i]).percent + '%' : '', fontSize: 11, bold: true})

        ceratium.push({text: formatValue(pools[i].ceratium), fontSize: 10})
        peridinium.push({text: formatValue(pools[i].peridinium), fontSize: 10})
        totalDino.push({text: formatValue(percentDinoflagellates(pools[i])), fontSize: 10, bold: true, background: 'yellow'})

        chlamydomonas.push({text: formatValue(pools[i].chlamydomonas), fontSize: 10})
        scenedesmus.push({text: formatValue(pools[i].scenedesmus), fontSize: 10})
        chlorella.push({text: formatValue(pools[i].chlorella), fontSize: 10})
        totalChlorella.push({text: formatValue(percentChlorophytes(pools[i]).sum), fontSize: 10, bold: true, background: 'yellow'})
        percentChlorella.push({text: formatValue(percentChlorophytes(pools[i]).percent) ? formatValue(percentChlorophytes(pools[i]).percent) + '%' : '', fontSize: 11, bold: true})

        anabaena.push({text: formatValue(pools[i].anabaena), fontSize: 10})
        oscillatory.push({text: formatValue(pools[i].oscillatory), fontSize: 10})
        nostoc.push({text: formatValue(pools[i].nostoc), fontSize: 10})
        anacystis.push({text: formatValue(pools[i].anacystis), fontSize: 10})
        totalAnacystis.push({text: formatValue(percentCyanophytes(pools[i]).sum), fontSize: 10, bold: true, background: 'yellow'})
        percentAnacystis.push({text: formatValue(percentCyanophytes(pools[i]).percent) ? formatValue(percentCyanophytes(pools[i]).percent) + '%' : '', fontSize: 11, bold: true})

        protozoa.push({text: formatValue(pools[i].protozoa), fontSize: 10})
        euglena.push({text: formatValue(pools[i].euglena), fontSize: 10})

        let totalPool = parseInt(percentDiatoms(pools[i]).sum) + parseInt(percentChlorophytes(pools[i]).sum) + parseInt(percentCyanophytes(pools[i]).sum)
        totalOthers.push({text: formatValue(parseInt(pools[i].protozoa) + parseInt(pools[i].euglena)), fontSize: 10, bold: true, background: 'yellow'})
        totalFinal.push({text: formatValue(totalPool), fontSize: 10, bold: true})

        
        allObservations.push({text: `${pools[i].observations ? pools[i].observations : 'Sin observaciones'}\n`, fontSize: 10})
        recomendate.push({text: `${pools[i] ? pools[i].pool.description : ''}: `, bold: true, fontSize: 10})
        allRecommendations.push({text: `${pools[i].recommendations ? pools[i].recommendations : 'Sin recomendaciones'}\n`, fontSize: 10})
        recommendations.push('')
        /* if (cont == 6) {
          widths.push('*')
          subtitle1.unshift({ text: 'DIATOMEAS (CEL/ML)', bold: true , fontSize: 10})
          space1.push({ text: '', border: [false, false, false, false] })
          subtitle2.unshift({ text: 'DINOFLAGELADOS (CEL/CAMPO) ', bold: true , fontSize: 10})
          subtitle3.unshift({ text: 'CLOROFITAS (CEL/ML) ', bold: true , fontSize: 10})
          subtitle4.unshift({ text: 'CIANOFITAS (CEL/ML) ', bold: true , fontSize: 10})
          subtitle5.unshift({ text: 'OTROS (CEL/ML)', bold: true , fontSize: 10})
          subtitle6.unshift({ text: 'Observaciones', bold: true , fontSize: 10})
          subtitle7.unshift({ text: 'Recomendaciones', bold: true , fontSize: 10})
          space1.push({ text: '', border: [false, false, false, false] })
          subtitle2.push({text: '', colSpan: subtitle1.length-1, alignment: 'center', fontSize: 10})
          subtitle3.push({text: '', colSpan: subtitle1.length-1, alignment: 'center', fontSize: 10})
          subtitle4.push({text: '', colSpan: subtitle1.length-1, alignment: 'center', fontSize: 10})
          subtitle5.push({text: '', colSpan: subtitle1.length-1, alignment: 'center', fontSize: 10})
          tables.push({
            margin:  [-10, 10, 0,0], // optional
            alignment: 'center',
            table: {
              // headers are automatically repeated if the table spans over multiple pages
              // you can declare how many rows should be treated as headers
              headerRows: 2,
              widths: ['*', '*', '*', '*', '*'],
              body: [
                subtitle1,
                pcyclotellah,
                coscinodiscus,
                chaetoceros,
                melosira,
                nitzschia,
                skelotonema,
                navicles,
                thalassiosira,
                totalDiatomea,
                percentDiatomea,
                space1,
                subtitle2,
                ceratium,
                peridinium,
                totalDino,
                subtitle3,
                chlamydomonas,
                scenedesmus,
                chlorella,
                totalChlorella,
                percentChlorella,
                subtitle4,
                anabaena,
                oscillatory,
                nostoc,
                anacystis,
                totalAnacystis,
                percentAnacystis,
                subtitle5,
                protozoa,
                euglena,
                totalOthers
              ]
            }
          })
          subtitle1 = []
          pcyclotellah = []
          coscinodiscus = []
          chaetoceros = []
          melosira = []
          nitzschia = []
          skelotonema = []
          navicles = []
          thalassiosira = []
          totalDiatomea = []
          percentDiatomea = []
          space1 = []
          subtitle2 = []
          ceratium = []
          peridinium = []
          totalDino = []
          subtitle3 = []
          chlamydomonas = []
          scenedesmus = []
          chlorella = []
          totalChlorella = []
          percentChlorella = []
          subtitle4 = []
          anabaena = []
          oscillatory = []
          nostoc = []
          anacystis = []
          totalAnacystis = []
          percentAnacystis = []
          subtitle5 = []
          protozoa = []
          euglena = []
          totalOthers = []
          cont = 0
        } */
      }
      //console.log('length', pools.length)
      if ((pools.length < 6) && (pools.length > 0)) {
        //console.log('yes')
        // Todos los campos menores a 3 registros o 6 columnas
          //subtitle1.unshift()
          let space1 = [{ text: ' ', border: [false,false,false,false], colSpan: pools.length + 1}, '', '', '', '', '']
          let subtitle2 = [{ text: 'DINOFLAGELADOS (CEL/CAMPO) ', bold: true , fontSize: 10, colSpan: pools.length + 1}, '', '', '', '', '']
          let space2 = [{ text: ' ', border: [false,false,false,false], colSpan: pools.length + 1}, '', '', '', '', '']
          let subtitle3 = [{ text: 'CLOROFITAS (CEL/ML) ', bold: true , fontSize: 10, colSpan: pools.length + 1}, '', '', '', '', '']
          let space3 = [{ text: ' ', border: [false,false,false,false], colSpan: pools.length + 1}, '', '', '', '', '']
          let subtitle4 = [{ text: 'CIANOFITAS (CEL/ML) ', bold: true , fontSize: 10, colSpan: pools.length + 1}, '', '', '', '', '']
          let space4 = [{ text: ' ', border: [false,false,false,false], colSpan: pools.length + 1}, '', '', '', '', '']
          let subtitle5 = [{ text: 'OTROS (CEL/ML)', bold: true , fontSize: 10, colSpan: pools.length + 1}, '', '', '', '', '']
          subtitle6.unshift({ text: 'Observaciones', bold: true , fontSize: 10})
          subtitle7.unshift({ text: 'Recomendaciones', bold: true , fontSize: 10})
          
          
          widths = []
          tables.push({
            margin:  [-10, 10, -5,-5], // optional
            alignment: 'center',
            table: {
              // headers are automatically repeated if the table spans over multiple pages
              // you can declare how many rows should be treated as headers
              //headerRows: 2,
              widths: Array(pools.length + 1).fill('*'),
              //heights: ['*', '*','*', '*', '*', '*', '*', '*','*', '*', '*', 1],
              body: [
                subtitle1,
                pcyclotellah,
                coscinodiscus,
                chaetoceros,
                melosira,
                nitzschia,
                skelotonema,
                navicles,
                thalassiosira,
                totalDiatomea,
                percentDiatomea,
                /* space1,
                subtitle2,
                ceratium,
                peridinium,
                totalDino,
                space2,
                subtitle3,
                chlamydomonas,
                scenedesmus,
                chlorella,
                totalChlorella,
                percentChlorella,
                space3,
                subtitle4,
                anabaena,
                oscillatory,
                nostoc,
                anacystis,
                totalAnacystis,
                percentAnacystis,
                space4,
                subtitle5,
                protozoa,
                euglena,
                totalOthers */
              ]
            }
          })
          
          tables.push({
            margin:  [-10, 10, -5, -5], // optional
            alignment: 'center',
            table: {
              // headers are automatically repeated if the table spans over multiple pages
              // you can declare how many rows should be treated as headers
              //headerRows: 2,
              widths: Array(pools.length + 1).fill('*'),
              //heights: ['*', '*','*', '*', '*', '*', '*', '*','*', '*', '*', 1],
              body: [
                /* subtitle1,
                pcyclotellah,
                coscinodiscus,
                chaetoceros,
                melosira,
                nitzschia,
                skelotonema,
                navicles,
                thalassiosira,
                totalDiatomea,
                percentDiatomea,
                space1, */
                /* subtitle2,
                ceratium,
                peridinium,
                totalDino, */
                
                subtitle3,
                chlamydomonas,
                scenedesmus,
                chlorella,
                totalChlorella,
                percentChlorella,
                
                /* subtitle4,
                anabaena,
                oscillatory,
                nostoc,
                anacystis,
                totalAnacystis,
                percentAnacystis,
                space4,
                subtitle5,
                protozoa,
                euglena,
                totalOthers */
              ]
            }
          })
          tables.push({
            margin:  [-10, 10, -5, -5], // optional
            alignment: 'center',
            table: {
              // headers are automatically repeated if the table spans over multiple pages
              // you can declare how many rows should be treated as headers
              //headerRows: 2,
              widths: Array(pools.length + 1).fill('*'),
              //heights: ['*', '*','*', '*', '*', '*', '*', '*','*', '*', '*', 1],
              body: [
                /* subtitle1,
                pcyclotellah,
                coscinodiscus,
                chaetoceros,
                melosira,
                nitzschia,
                skelotonema,
                navicles,
                thalassiosira,
                totalDiatomea,
                percentDiatomea,
                space1, */
                /* subtitle2,
                ceratium,
                peridinium,
                totalDino, */
                
                /* subtitle3,
                chlamydomonas,
                scenedesmus,
                chlorella,
                totalChlorella,
                percentChlorella,
                space3, */
                subtitle4,
                anabaena,
                oscillatory,
                nostoc,
                anacystis,
                totalAnacystis,
                percentAnacystis,
                /* space4,
                subtitle5,
                protozoa,
                euglena,
                totalOthers */
              ]
            }
          })
          tables.push({
            margin:  [-10, 10, -5, -5], // optional
            alignment: 'center',
            table: {
              // headers are automatically repeated if the table spans over multiple pages
              // you can declare how many rows should be treated as headers
              //headerRows: 2,
              widths: Array(pools.length + 1).fill('*'),
              //heights: ['*', '*','*', '*', '*', '*', '*', '*','*', '*', '*', 1],
              body: [
                /* subtitle1,
                pcyclotellah,
                coscinodiscus,
                chaetoceros,
                melosira,
                nitzschia,
                skelotonema,
                navicles,
                thalassiosira,
                totalDiatomea,
                percentDiatomea,
                space1, */
                subtitle2,
                ceratium,
                peridinium,
                totalDino,
                /* space2,
                subtitle3,
                chlamydomonas,
                scenedesmus,
                chlorella,
                totalChlorella,
                percentChlorella,
                space3,
                subtitle4,
                anabaena,
                oscillatory,
                nostoc,
                anacystis,
                totalAnacystis,
                percentAnacystis,
                space4,
                subtitle5,
                protozoa,
                euglena,
                totalOthers */
              ]
            }
          })
          tables.push({
            margin:  [-10, 10, -5, -5], // optional
            alignment: 'center',
            table: {
              // headers are automatically repeated if the table spans over multiple pages
              // you can declare how many rows should be treated as headers
              //headerRows: 2,
              widths: Array(pools.length + 1).fill('*'),
              //heights: ['*', '*','*', '*', '*', '*', '*', '*','*', '*', '*', 1],
              body: [
                /* subtitle1,
                pcyclotellah,
                coscinodiscus,
                chaetoceros,
                melosira,
                nitzschia,
                skelotonema,
                navicles,
                thalassiosira,
                totalDiatomea,
                percentDiatomea,
                space1, */
                /* subtitle2,
                ceratium,
                peridinium,
                totalDino, */
                
                /* subtitle3,
                chlamydomonas,
                scenedesmus,
                chlorella,
                totalChlorella,
                percentChlorella,
                space3, */
                /* subtitle4,
                anabaena,
                oscillatory,
                nostoc,
                anacystis,
                totalAnacystis,
                percentAnacystis, */
                subtitle5,
                protozoa,
                euglena,
                totalOthers
              ]
            }
          })
          tables.push({
            margin:  [-10, 10, -5, -5], // optional
            alignment: 'center',
            table: {
              widths: Array(pools.length + 1).fill('*'),
              body: [
                totalFinal
              ]
            }
          })
      } else if (pools.length > 4) {
        subtitle1.unshift({ text: 'DIATOMEAS (CEL/ML)', bold: true , fontSize: 10})
        space1.push({ text: '', border: [false, false, false, false] })
        subtitle2.unshift({ text: 'DINOFLAGELADOS (CEL/CAMPO) ', bold: true , fontSize: 10})
        subtitle3.unshift({ text: 'CLOROFITAS (CEL/ML) ', bold: true , fontSize: 10})
        subtitle4.unshift({ text: 'CIANOFITAS (CEL/ML) ', bold: true , fontSize: 10})
        subtitle5.unshift({ text: 'OTROS (CEL/ML)', bold: true , fontSize: 10})
        subtitle6.unshift({ text: 'Observaciones', bold: true , fontSize: 10})
        subtitle7.unshift({ text: 'Recomendaciones', bold: true , fontSize: 10})
        space1.push({ text: '', border: [false, false, false, false] })
        subtitle2.push({text: '', colSpan: subtitle1.length-1, alignment: 'center', fontSize: 10})
        subtitle3.push({text: '', colSpan: subtitle1.length-1, alignment: 'center', fontSize: 10})
        subtitle4.push({text: '', colSpan: subtitle1.length-1, alignment: 'center', fontSize: 10})
        subtitle5.push({text: '', colSpan: subtitle1.length-1, alignment: 'center', fontSize: 10})
        widths = []
        subtitle1.forEach(element => {
          widths.push('*')
        })
        pcyclotellah.unshift({text: 'Cyclotella', fontSize: 10})
        coscinodiscus.unshift({text: 'Coscinodiscus', fontSize: 10})
        chaetoceros.unshift({text: 'Chaetoceros', fontSize: 10})
        melosira.unshift({text: 'Melosira', fontSize: 10})
        nitzschia.unshift({text: 'Nitzschia', fontSize: 10})
        skelotonema.unshift({text: 'Skelotonema', fontSize: 10})
        navicles.unshift({text: 'Navículas', fontSize: 10})
        thalassiosira.unshift({text: 'Thalassiosira', fontSize: 10})
        totalDiatomea.unshift({text: 'Total', fontSize: 10})
        percentDiatomea.unshift({text: '%', fontSize: 10})
        ceratium.unshift({text: 'Ceratium', fontSize: 10})
        peridinium.unshift({text:'Peridinium', fontSize: 10})
        totalDino.unshift({text:'Total', fontSize: 10})
        chlamydomonas.unshift({text: 'Chlamydomonas', fontSize: 10})
        scenedesmus.unshift({text: 'Scenedesmus', fontSize: 10})
        chlorella.unshift({text: 'Clorella', fontSize: 10})
        totalChlorella.unshift({text: 'Total', fontSize: 10})
        percentChlorella.unshift({text: '%', fontSize: 10})
        anabaena.unshift({text: 'Anabaena', fontSize: 10})
        oscillatory.unshift({text: 'Oscillatoria', fontSize: 10})
        nostoc.unshift({text: 'Nostoc', fontSize: 10})
        anacystis.unshift({text: 'Anacystis', fontSize: 10})
        totalAnacystis.unshift({text: 'Total', fontSize: 10})
        percentAnacystis.unshift({text: '%', fontSize: 10})
        protozoa.unshift({text: 'Protozoarios', fontSize: 10})
        euglena.unshift({text: 'Euglena', fontSize: 10})
        totalOthers.unshift({text: 'Total', fontSize: 10})
        tables.push({
          margin:  [-10, 10, 0,0], // optional
          alignment: 'center',
          pageBreak: 'before',
          table: {
            // headers are automatically repeated if the table spans over multiple pages
            // you can declare how many rows should be treated as headers
            headerRows: 2,
            widths,
            body: [
              subtitle1,
              pcyclotellah,
              coscinodiscus,
              chaetoceros,
              melosira,
              nitzschia,
              skelotonema,
              navicles,
              thalassiosira,
              totalDiatomea,
              percentDiatomea,
              space1,
              subtitle2,
              ceratium,
              peridinium,
              totalDino,
              subtitle3,
              chlamydomonas,
              scenedesmus,
              chlorella,
              totalChlorella,
              percentChlorella,
              subtitle4,
              anabaena,
              oscillatory,
              nostoc,
              anacystis,
              totalAnacystis,
              percentAnacystis,
              subtitle5,
              protozoa,
              euglena,
              totalOthers
            ]
          }
        })
      }
      // console.log(pools.length)

    }
    const poolTableHeaderBody = [{ text: 'Piscina: ', bold:true, fontSize: 10 }]
    phytoplanctonanalisis.pools.forEach(element => {
//      console.log({element})
      poolTableHeaderBody.push({text: element.pool.description, bold: true, fontSize: 10})
    })

    if (poolTableHeaderBody.length < 6) {
      if (6 - poolTableHeaderBody.length == 1) {
        poolTableHeaderBody.push('')
      } else if (6 - poolTableHeaderBody.length == 2) {
        poolTableHeaderBody.push('')
        poolTableHeaderBody.push('')
      }
      else if (6 - poolTableHeaderBody.length == 3) {
        poolTableHeaderBody.push('')
        poolTableHeaderBody.push('')
        poolTableHeaderBody.push('')
      }
      else if (6 - poolTableHeaderBody.length == 4) {
        poolTableHeaderBody.push('')
        poolTableHeaderBody.push('')
        poolTableHeaderBody.push('')
        poolTableHeaderBody.push('')
      }
      else if (6 - poolTableHeaderBody.length == 5) {
        poolTableHeaderBody.push('')
        poolTableHeaderBody.push('')
        poolTableHeaderBody.push('')
        poolTableHeaderBody.push('')
        poolTableHeaderBody.push('')
      }
      else if (6 - poolTableHeaderBody.length == 6) {
        poolTableHeaderBody.push('')
        poolTableHeaderBody.push('')
        poolTableHeaderBody.push('')
        poolTableHeaderBody.push('')
        poolTableHeaderBody.push('')
        poolTableHeaderBody.push('')
      }
    }
    let poolTableHeader = phytoplanctonanalisis.pools.length ? {
      table: {
        //headerRows: 2,
        margin:  [-10, -10,-5,-5],
        widths: Array(6).fill('*'),
        alignment: 'left',
        body: [
          poolTableHeaderBody
        ]
      }
    } : undefined
    //return poolTableHeader
    

    observations.unshift(allObservations)
    /* let more = []
    let more2 = []
    for (let i in recomendate) {
      more.push(recomendate[i])
      more2.push(recomendate[i])
      more2.push(allObservations[i])
      more.push(allRecommendations[i])
    }
    let recomendation = {
      text: []
    }
    let observate = {
      text: []
    } */

    const charts = []
    phytoplanctonanalisis.pools.forEach(element => {
      const chart = element.chart_image ? {
        image: element.chart_image,
        pageBreak: 'before',
        /* width: 446,
        height: 186, */
        alignment : 'center',
        margin: [0,0]
      } : undefined
      if (chart) {
        charts.push(chart)
      }
    })


    /////////////////////////777Esto es una PRUEBA////////////////////////////////////////////////////////////////////
    let cells = []
    const bodyImagesTable = []
    let marginLeftNumber = -50
    let marginAligment = 'right'
    console.log('Hay tantas piscinas', phytoplanctonanalisis.pools.length)
    phytoplanctonanalisis.pools.forEach((element, key) => {
      // Cada celda es una imagen con sus detalles
      marginLeftNumber = marginLeftNumber === -170 ? -50 : -170
      marginAligment = marginAligment == 'left' ? 'right' : 'left'
      const chart = element.chart_image ? [{
        image: element.chart_image,
        //pageBreak: 'before',
        /* width: 446,
        height: 186, */
        alignment : 'left',
        width: 571,
        height: 200,
        //fit: [500,500],
        margin: [marginLeftNumber,0,100,0]
      },
      {
        text: element.pool.description, alignment: marginAligment, height: 500, bold: true
      }
      ] : undefined

      if (chart) {
        cells.push(chart)
      }

      /* cells.push([{
        image: 'storage/uploads/Pathology/' + element.dir,
        // fit: [100,100], // Fit es para ajustarlas proporcionalmente
        alignment: 'left',
        width: 150,
        height: 150,
        margin: [0,0,-10,0]
      }]) */
      // Cada 3 celdas/imágenes hay que crear una nueva fila
      if (cells.length === 2 || key === phytoplanctonanalisis.pools.length - 1) { // si es una celda fin de fila o la última celda/imagen
        // Si es la última imagen y no hay 3 celdas rellenar con espacios en blanco
        if (cells.length != 2) {
          if (2 - cells.length == 1) { // Si hay 2 imágenes en la última fila
            cells.push('')
          } else { // Si hay una sola imagen
            cells.push('', '')
          }
          
        }
        bodyImagesTable.push(cells)
        cells = []
      }
    })
    const imagesTable = bodyImagesTable.length ?
      {
      layout: 'noBorders',
      table: {
        headerRows: 1,
        widths: [ '*', '*', '*' ],
        body: bodyImagesTable
      }
    } : {}
    //return imagesTable
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    const recomendation = phytoplanctonanalisis.recommendations
    const observate = phytoplanctonanalisis.recommendations
    ///////////////borrar
    //return [widths, pools,thalassiosira,ceratium]
    let title ='LABORATORIO'
    let subtitle ='ANÁLISIS CUANTITATIVO DE FITOPLANCTON'
    let name = 'Asesor Técnico:'
    this.img = {
      image: 'logo.jpeg',
      fit: [70, 70],
      absolutePosition: {
        'x': 35,
        'y': 25
      }
    },
    this.img2 = {
      image: 'logo.jpeg',
      fit: [70, 70],
      absolutePosition: {
        'x': 20,
        'y': 10
      }
    }
    //return tables
    var docDefinition = {
      pageSize: 'A4',
      pageMargins: [60, 115, 40, 90],
      header: [
        {
          margin: [20, 35, 20, 15],
          alignment: 'center',
          text: [
            {
              bold: true,
              text: `${title}\n`,
              fontSize: 14
            },
            {
              text: `${subtitle}\n`,
              fontSize: 12
            }
          ]
        },
        {
          alignment: 'center',
          margin:[0,-10,0,10],
          text: [
            { text: 'CAMARONERA: ', bold: true, fontSize: 10},
            { text: phytoplanctonanalisis.shrimp.name ? phytoplanctonanalisis.shrimp.name : '', fontSize: 10},
            { text: '             '},
            { text: 'SOLICITANTE: ', bold: true, fontSize: 10},
            { text: phytoplanctonanalisis.applicant ? `${phytoplanctonanalisis.applicant.name} ${phytoplanctonanalisis.applicant.last_name}` : '', fontSize: 10}
          ]
        },
        {
          alignment: 'center',
          text: [
            { text: 'ZONA: ', bold: true, fontSize: 10, margin:[0,0,50,0]},
            { text: phytoplanctonanalisis.shrimp.zone.name ? phytoplanctonanalisis.shrimp.zone.name : '-' , fontSize: 10},
            { text: '                       '},
            { text: 'F.ANÁLISIS: ', bold: true, fontSize: 10},
            { text: phytoplanctonanalisis.analysis_date ? phytoplanctonanalisis.analysis_date : '-' , fontSize: 10},
            { text: '                       '},
            { text: 'F.ENTREGA: ', bold: true, fontSize: 10},
            { text: phytoplanctonanalisis.delivery_date ? phytoplanctonanalisis.delivery_date : '-' , fontSize: 10}
          ]
        },
        this.img
      ],
      content: [
        poolTableHeader,
        ...tables,
        imagesTable,
        {
          pageBreak: 'before',
          alignment: 'left',
          bold: true,
          text: [
            { text: '\n\nOBSERVACIONES:\n\n', fontSize: 10}
          ]
        },
        observate,
        {
          alignment: 'left',
          bold: true,
          text: [
            { text: '\n\RECOMENDACIONES:\n\n', fontSize: 10}
          ]
        },
        recomendation,
        //charts
      ],
      footer: {
        columns: [
          this.img2,
          {text: 'Dpto. Técnico\n', bold: true, alignment: 'left', fontSize: 12, margin: [-25, 13, 0, 0] },
          {text: 'División Acuacultura\n', alignment: 'left', fontSize: 10, margin: [-144, 25, 0, 0]},
          {text: 'Guayaquil - Ecuador', bold: true, alignment: 'left', fontSize: 12, margin: [-263, 37, 0, 0] },
          {text: `_______________________\n${name}\n${phytoplanctonanalisis.user.technical_advisor.full_name ? phytoplanctonanalisis.user.technical_advisor.full_name : ''}\n`,bold:true, alignment: 'center', fontSize: 10,  margin: [-70, 15, 0, 0] },
        ]
      }
    }
    //return docDefinition

    var options = {
      // ...
    }
    // return docDefinition
    var pdfDoc = printer.createPdfKitDocument(docDefinition, options)
    const dir = Helpers.appRoot('storage/uploads/phytoplanctonanalisis')
    const filename = `Analisis_fitoplancton_${phytoplanctonanalisis.id}.pdf`
    if (!fs.existsSync(dir)){
      fs.mkdirSync(dir);
    }
    pdfDoc.pipe(fs.createWriteStream(`${dir}/${filename}`));
    pdfDoc.end()


    let to = 'elyohan14@gmail.com'
    if (phytoplanctonanalisis.applicant_id === 0) { // si el solicitante es el mismo cliente hay que buscar correo del cliente
      to = phytoplanctonanalisis.shrimp.customer ? phytoplanctonanalisis.shrimp.customer.electronic_documents_email : ''
    } else {
      to = (phytoplanctonanalisis.shrimp.customer.contacts.find(val => val.id === phytoplanctonanalisis.applicant_id)).email
    }

    response.send(`phytoplanctonanalisis-Analisis_fitoplancton_${phytoplanctonanalisis.id}`)
  }
}

function formatValue(value) {
  if (value) {
    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')
  } else {
    return ''
  }
  
}
function totalPool (pool) {
  // const pool = { ...this.form.pools.find(val => val.pool_id === poolId) }
  const poolId = pool.pool_id
  delete pool.pool_id
  delete pool.description
  delete pool.expanded

  //console.log({ pool })

  const pcyclotellah = pool.pcyclotellah && typeof parseInt(pool.pcyclotellah) === 'number' ? parseInt(pool.pcyclotellah) : 0
  const coscinodiscus = pool.coscinodiscus && typeof parseInt(pool.coscinodiscus) === 'number' ? parseInt(pool.coscinodiscus) : 0
  const chaetoceros = pool.chaetoceros && typeof parseInt(pool.chaetoceros) === 'number' ? parseInt(pool.chaetoceros) : 0
  const melosira = pool.melosira && typeof parseInt(pool.melosira) === 'number' ? parseInt(pool.melosira) : 0
  const nitzschia = pool.nitzschia && typeof parseInt(pool.nitzschia) === 'number' ? parseInt(pool.nitzschia) : 0
  const skelotonema = pool.skelotonema && typeof parseInt(pool.skelotonema) === 'number' ? parseInt(pool.skelotonema) : 0
  const navicles = pool.navicles && typeof parseInt(pool.navicles) === 'number' ? parseInt(pool.navicles) : 0
  const thalassiosira = pool.thalassiosira && typeof parseInt(pool.thalassiosira) === 'number' ? parseInt(pool.thalassiosira) : 0
  const ceratium = pool.ceratium && typeof parseInt(pool.ceratium) === 'number' ? parseInt(pool.ceratium) : 0
  const peridinium = pool.peridinium && typeof parseInt(pool.peridinium) === 'number' ? parseInt(pool.peridinium) : 0
  const chlamydomonas = pool.chlamydomonas && typeof parseInt(pool.chlamydomonas) === 'number' ? parseInt(pool.chlamydomonas) : 0
  const chlorella = pool.chlorella && typeof parseInt(pool.chlorella) === 'number' ? parseInt(pool.chlorella) : 0
  const scenedesmus = pool.scenedesmus && typeof parseInt(pool.scenedesmus) === 'number' ? parseInt(pool.scenedesmus) : 0
  const anabaena = pool.anabaena && typeof parseInt(pool.anabaena) === 'number' ? parseInt(pool.anabaena) : 0
  const oscillatory = pool.oscillatory && typeof parseInt(pool.oscillatory) === 'number' ? parseInt(pool.oscillatory) : 0
  const nostoc = pool.nostoc && typeof parseInt(pool.nostoc) === 'number' ? parseInt(pool.nostoc) : 0
  const anacystis = pool.anacystis && typeof parseInt(pool.anacystis) === 'number' ? parseInt(pool.anacystis) : 0
  const sumValues = pcyclotellah + coscinodiscus + chaetoceros + melosira + nitzschia + skelotonema + navicles + thalassiosira +
  /* ceratium + peridinium + */ chlamydomonas + chlorella + scenedesmus + anabaena + oscillatory + nostoc + anacystis

  return {
    pcyclotellah,
    coscinodiscus,
    chaetoceros,
    melosira,
    nitzschia,
    skelotonema,
    navicles,
    thalassiosira,
    ceratium,
    peridinium,
    chlamydomonas,
    chlorella,
    scenedesmus,
    anabaena,
    oscillatory,
    nostoc,
    anacystis,
    sumValues
  }
}
function percentDiatoms (pool) {
  const total = totalPool(pool)
  // console.log({ pool })
  const pcyclotellah = total.pcyclotellah
  const coscinodiscus = total.coscinodiscus
  const chaetoceros = total.chaetoceros
  const melosira = total.melosira
  const nitzschia = total.nitzschia
  const skelotonema = total.skelotonema
  const navicles = total.navicles
  const thalassiosira = total.thalassiosira
  const sum = pcyclotellah + coscinodiscus + chaetoceros + melosira + nitzschia + skelotonema + navicles + thalassiosira
  // return sum > 0 ? (sum * 100 / total.sumValues).toFixed(2) : ''
  return {
    sum,
    percent: sum > 0 ? (sum * 100 / total.sumValues).toFixed(2) : ''
  }
}
function percentDinoflagellates (pool) {
  // console.log({ pool })
  const total = totalPool(pool)
  const ceratium = total.ceratium
  const peridinium = total.peridinium
  const sum = ceratium + peridinium

  return sum > 0 ? sum : ''
}
function percentChlorophytes (pool) {
  const total = totalPool(pool)
  const chlamydomonas = total.chlamydomonas
  const chlorella = total.chlorella
  const scenedesmus = total.scenedesmus
  const sum = chlamydomonas + chlorella + scenedesmus

  // return sum > 0 ? (sum * 100 / total.sumValues).toFixed(2) : ''

  return {
    sum,
    percent: sum > 0 ? (sum * 100 / total.sumValues).toFixed(2) : ''
  }
}
function percentCyanophytes (pool) {
  const total = totalPool(pool)
  const anabaena = total.anabaena
  const oscillatory = total.oscillatory
  const nostoc = total.nostoc
  const anacystis = total.anacystis
  const sum = anabaena + oscillatory + nostoc + anacystis

  // return sum > 0 ? (sum * 100 / total.sumValues).toFixed(2) : ''
  return {
    sum,
    percent: sum > 0 ? (sum * 100 / total.sumValues).toFixed(2) : ''
  }
}
/**
 * Convertir valores en blanco en valores nulos para poder guardarlos en un campo entero
 * @param {obj} obj 
 * @returns {obj}
 */
function cleanBlankValues(obj) {
  for (var propName in obj) {
    /* if (obj[propName] === null || obj[propName] === undefined || obj[propName] === '') {
      delete obj[propName];
    } */
    if (obj[propName] === '') {
      obj[propName] = null
    }
  }
  return obj
}

module.exports = PhytoplanktonController
