'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with permissions
 */
const Permission = use('App/Models/Permission')
const List = use('App/Functions/List')
const { validate } = use('Validator')
const PermissionF = use('App/Functions/Permissions')
const permissions = PermissionF.getPermissions()
class PermissionController {
  /**
   * Show a list of all permissions.
   * GET permissions
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ response }) {
    let permissions = (await Permission.all()).toJSON()
    permissions = List.addRowActions(permissions, [{name: 'edit', route: 'permissions/form/', permission: 12}, {name: 'delete', permission: 13}])
    response.send(permissions)
  }

  /**
   * Create/save a new permission.
   * POST permissions
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const validation = await validate(request.all(), Permission.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const body = request.only(Permission.fillable)
      const permission = await Permission.create(body)
      response.send(permission);
    }
  }

  /**
   * Display a single permission.
   * GET permissions/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    const permission = (await Permission.query().where('id', params.id).first()).toJSON()
    response.send(permission)
  }

  /**
   * Update permission details.
   * PUT or PATCH permissions/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const permission =  await Permission.query().where('id', params.id).first()
    if (permission) {
      const validation = await validate(request.all(), Permission.fieldValidationRulesUpdate())

      if (validation.fails()) {
        response.unprocessableEntity(validation.messages())
      } else {
        const body = request.only(Permission.fillable)
        for (const i in body) {
          permission[i] = body[i]
        }
        permission.save()
        response.send(permission);
      }
    } else {
      response.unprocessableEntity(validation.messages()) // Aquí debe ir uno de not found 404
    }
  }

  /**
   * Delete a permission with id.
   * DELETE permissions/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, response }) {
    const permission = await Permission.find(params.id)
    permission.delete()
    response.send('success')
  }

  async getFormatedPermissions ({ response }) {
    response.send(permissions)
  }
}

module.exports = PermissionController
