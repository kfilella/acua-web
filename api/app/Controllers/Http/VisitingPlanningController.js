'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with appointmentplannings
 */
const moment = require('moment')
const momentT = require('moment-timezone')
const geolib = require('geolib')
const User = use('App/Models/User')
const VisitingPlanning = use('App/Models/VisitingPlanning')
const TechnicalChief = use('App/Models/TechnicalChief')
const TechnicalAdvisor = use('App/Models/TechnicalAdvisor')
const { validate } = use('Validator')
const Email = use('App/Functions/Email')
const Notification = use('App/Functions/Notification')
const ExcelJS = require('exceljs')
const Helpers = use("Helpers")
const VisitingPlanningUpdateRequest = use('App/Models/VisitingPlanningUpdateRequest')
const TechnicalEquipment = use('App/Models/TechnicalEquipment')
const VisitReason = use('App/Models/VisitReason')
const Configuration = use('App/Models/Configuration')
class VisitingPlanningController {

  /**
   * Muestra una lista de planificaciones de visitas realizadas
   * Si es asesor técnico se visualizan sus propias planificaciones
   * Si es jefe técnico puede visualizar las de todo su equipo o la de un miembro en específico de su equipo
   * GET appointment_plannings
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ auth, response, request }) {    
    const userLogged = await auth.getUser()
    const user = (await User.query().with('roles').where('id', userLogged.id).first()).toJSON()
    const isTechnicalAdvisor = user.roles.find(val => val.id === 3)
    const isTechnicalChief= user.roles.find(val => val.id === 2)
    let appointmentplannings = []

    const config = await Configuration.query().where('id', 1).first()
    console.log({ config })
    const mts_range_marking = config ? config.toJSON().mts_range_marking : 700
    console.log({ mts_range_marking })

    if (isTechnicalAdvisor) { // Si es asesor técnico debe mostrarse sus planificaciones
      // Si es con la app móvil solo las aprobadas
      const { approved } = request.get()

      appointmentplannings = VisitingPlanning.query().with('customer').with('shrimp').with('advisor')
      .with('visitReason').with('updateRequest').with('updateRequests.visitReason').with('updateRequests.customer')
      .with('updateRequests.shrimp').where('technical_advisor_id', user.id)
      .with('updateRequests.pre_shrimp').with('updateRequests.pre_visitReason').with('updateRequests.pre_customer')
      .with('updateRequests', (query) => {
        query.orderBy('id')
      })
      
      if (approved) { // Si se deben mostrar solo los aprobados
        appointmentplannings = appointmentplannings.where('status', 1)
      }
      appointmentplannings = (await appointmentplannings.fetch()).toJSON()
      
      // return appointmentplannings
      appointmentplannings.forEach(element => {
        element.date = moment(element.date).format("YYYY-MM-DD")
        element.dateF = moment(element.date).format("DD-MM-YYYY")

        // Si el tipo de evento no requiere aprobación puede ser eliminado
        element.canDelete = element.visitReason.require_approval == false
        element.btnEdit = true // El asesor siempre podrá ver el botón de editar evento
        if (element.status === 0) {
          element.bgcolor = '#f2c037'
          element.statusName = 'En espera de aprobación'
          element.canDelete = true
        } else if (element.status === 1) {
          element.bgcolor = '#21ba45'
          element.statusName = 'Aprobado'

          element.updateRequests.forEach(element2 => {
            element2.created_at_now = moment(element2.created_at).fromNow()
            if (element2.type == 1 && element2.status == 1) {
              element.canDelete = true
            }
          });

          element.canDeleteWithAuthorization = true
        }else if (element.status === 3) { // Si fue reenviada una edición
          element.bgcolor = '#f2c037'
          element.statusName = 'En espera de aprobación de edición solicitada'
          element.canDeleteWithAuthorization = true
        }
        else {
          element.bgcolor = '#c10015'
          element.statusName = 'Rechazado'

          if (element.updateRequest && element.updateRequest.type == 1 && element.updateRequest.status == 1) {
            element.canDelete = true
          }

          element.canDeleteWithAuthorization = true
        }
        if (element.updateRequest && (element.updateRequest.status === 1 || element.updateRequest.status === 2)) { // si fue aprobada o rechazada alguna solicitud de edición o eliminación
          if (element.updateRequest.type === 0){
            element.updateRequestDescription = element.updateRequest.status === 1 ? 'Edición Aprobada' : 'Edición Rechazada'
          }
          if (element.updateRequest.type === 1){
            element.updateRequestDescription = element.updateRequest.status === 1 ? 'Eliminación Aprobada' : 'Eliminación Rechazada'
          }
        } else {
          element.icon = element.updateRequests.filter(val => val.seen === false && val.status === 2).length ? 'notifications' : undefined
        }
        // Asignarle nombre a los estados de las solicitudes de modificación o eliminación
        element.updateRequests.forEach(element2 => {
          element2.statusName = element2.status == 0 ? 'Pendiente por aprobar' : element2.status == 1 ? 'Aprobado' : 'Rechazado'
          element2.date = moment(element2.date).format('DD-MM-YYYY')
          element2.pre_date = moment(element2.pre_date).format('DD-MM-YYYY')
          element2.created_at = moment(element2.created_at).format('DD-MM-YYYY')

          if (element2.status === 1) { // Si tiene estatus de aprobada
            if (element2.type == 1) { // Si es de eliminación
              element.bgcolor = 'grey'
              element.deleted = true
            }
          }
        })
        // Mostrarle al asesor solo las solicitudes que le han aprobado o rechazado
        element.updateRequests = element.updateRequests.filter(val => (val.status == 1 || val.status == 2))
        element.icon = element.updateRequests.filter(val => val.seen === false && (val.status === 1 || val.status === 2)).length ? 'notifications' : undefined

        element.registered = element.ubication ? true : false
      })
    } else if (isTechnicalChief) { // Si es jefe técnico deben mostrarse todas las planificaciones de los asesores técnicos que tiene asignados
      const params = request.get()
      if (Object.keys(params).length) { // Si me enviaron parámetros para filtrar entonces uso ese filtro
        appointmentplannings = (await VisitingPlanning.query().with('customer').with('shrimp').with('advisor')
        .with('visitReason').with('updateRequest').with('updateRequests.visitReason').where(params)
        .with('updateRequests.customer').with('updateRequests.shrimp')
        .with('updateRequests.pre_shrimp').with('updateRequests.pre_visitReason')
        .with('updateRequests', (query) => {
          query.orderBy('id')
        })
        .with('updateRequests.pre_customer').fetch()).toJSON()  
      } else {
        const advisorsIds = []
        const technicalChief = (await TechnicalChief.query().with('teams').where('user_id', user.id).first()).toJSON()
        technicalChief.teams.forEach(element => {
          JSON.parse(element.technical_advisors).forEach(element2 => {
            advisorsIds.push(element2)
          })
        })
        // Consultar los users_ids de esos asesores para buscar sus planificaciones
        const technicalAdvisors = (await TechnicalAdvisor.query().whereIn('id', advisorsIds).fetch()).toJSON()
        const users_ids = technicalAdvisors.map(val => val.user_id)

        appointmentplannings = (await VisitingPlanning.query().with('customer').with('shrimp').with('advisor')
        .with('visitReason').with('updateRequest').with('updateRequests.visitReason')
        .with('updateRequests.customer').with('updateRequests.shrimp')
        .with('updateRequests.pre_shrimp').with('updateRequests.pre_visitReason').with('updateRequests.pre_customer')
        .with('updateRequests', (query) => {
          query.orderBy('id')
        })
        .whereIn('technical_advisor_id', users_ids).orderBy('updated_at', 'desc').fetch()).toJSON()
      }

      appointmentplannings.forEach(element => {
        element.date = moment(element.date).format("YYYY-MM-DD")
        element.dateF = moment(element.date).format("DD-MM-YYYY")
        element.real_dateF = element.real_date ? moment(element.real_date).format("DD-MM-YYYY") : ''
        element.btnEdit = element.visitReason.require_approval // El botón de editar evento se muestra si el evento requiere aprobación
        if (element.status === 0 || element.status === 3) {
          element.bgcolor = '#f2c037'
          element.statusName = 'En espera de aprobación'
          element.canApprove = true
          element.canReject = true
        } else if (element.status === 1) {
          element.bgcolor = '#21ba45'
          element.statusName = 'Aprobado'
          element.canReject = element.visitReason.require_approval // Si es un evento que requiere aprobación puede ser rechazado
        } else {
          element.bgcolor = '#c10015'
          element.statusName = 'Rechazado'
          element.canApprove = true
        }

        element.updateRequests.forEach(element2 => {
          if (element2.status === 0) {
            if (element2.type === 0) {
              element.canApproveEdition = true
            }

            if (element2.type === 1) {
              element.canApproveDeletion = true
            }

            element.icon = 'notifications'
          }
          if (element2.status === 1) { // Si tiene estatus de aprobada la solicitud
            if (element2.type == 1) { // Si es de eliminación puede ser eliminada
              element.bgcolor = 'grey'
              element.deleted = true
              element.btnEdit = false
            }
          }
          element2.statusName = element2.status == 0 ? 'Pendiente por aprobar' : element2.status == 1 ? 'Aprobado' : 'Rechazado'
          element2.date = moment(element2.date).format('DD-MM-YYYY')
          element2.pre_date = moment(element2.pre_date).format('DD-MM-YYYY')
          element2.created_at_now = moment(element2.created_at).fromNow()
          element2.created_at = moment(element2.created_at).format('DD-MM-YYYY')
        })

        // Si el evento fue marcado mostrar si fue acertado en día, hora y lugar
        element.assisted = element.shrimp_distance <= mts_range_marking
        const full_time_p = moment(element.date + ' ' + element.time)
        const full_time_real = moment(moment(element.real_date).format('YYYY-MM-DD') + ' ' + element.real_time)
        element.full_time_p = full_time_p
        element.full_time_real = full_time_real

        const differenceMinutes = full_time_p.diff(full_time_real, 'minutes') // cantidad de minutos entre la hora planificada y la hora assitida

        // Si el evento ocurrió entre 10 minutos antes y 10 minutos después del evento(duración incluida) entonces estuvo a tiempo
        if ((differenceMinutes >=0 && differenceMinutes <=0) || (differenceMinutes + element.duration) >= -10 ) {
          element.ontime = true
        }
      })
    }
    // Obtengo las fechas de los eventos
    const dates = [... new Set(appointmentplannings.map(val => val.date))]
    // Recorro cada fecha para buscar cuales chocan con ella
    for (const event of appointmentplannings) {
      const startTime = moment.utc(event.date + ' ' + event.time)
      const endTime = moment(startTime, 'hh:mm').add(event.duration, 'minutes')
      
      const found = appointmentplannings.filter(val => {    
        const startTime2 = moment.utc(val.date + ' ' + val.time)
        const endTime2 = moment(startTime2, 'hh:mm').add(val.duration, 'minutes')
          return val.date == event.date &&
          (moment(startTime).isBetween(startTime2, endTime2) || moment(endTime).isBetween(startTime2, endTime2) || moment(startTime).isSame(startTime2))
        })
        //console.log('found', found)
      //found.push(event)
        
      // Si hay algunas que chocan revisar cuantas son, recorrerlas y asignar el left
      if (found) {
        let left = 0
        let width = 100 / (found.length)
        found.forEach(element => {
          // En cada una de las que chocan buscar en qué posición del array principal están para asignarle las propiedades
          const id = element.id
          const index = appointmentplannings.findIndex(val => val.id == id)
          appointmentplannings[index].newLeft = appointmentplannings[index].newLeft ? appointmentplannings[index].newLeft : left +'%'
          appointmentplannings[index].newWidth = appointmentplannings[index].newWidth ? appointmentplannings[index].newWidth : width+'%'
          left += width
        })
      } else {
        event.newLeft = '0%'
        event.newWidth = '100%'
      }

      // Si las fechas son de hoy en adelante se pueden modificar
      event.isTodayOrAfter =  moment(event.date).isAfter(moment()) || moment(event.date).isSame(moment().format('YYYY-MM-DD'))
    }
    response.send(appointmentplannings)
  }

  /**
   * Create/save a new visitingplanning.
   * POST appointmentplannings
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ auth, request, response }) {
    const user = await auth.getUser()
    
    const validation = await validate(request.all(), VisitingPlanning.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const body = request.only(VisitingPlanning.fillable)
      body.technical_advisor_id = user.id
      //body.status = body.event_type_id == 3 ? 1 : 0

      // Antes de crear se debe verificar si el evento requiere aprobación de jefe o no
      const visitReason = (await VisitReason.query().where('id', body.event_type_id).first()).toJSON()
      body.status = visitReason.require_approval ? 0 : 1
      const visitingPlanning = await VisitingPlanning.create(body)
      response.send(visitingPlanning)
    }
  }

  /**
   * Display a single visitingplanning.
   * GET appointmentplannings/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Update visitingplanning details.
   * PUT or PATCH appointmentplannings/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ auth, params, request, response }) {
    const userLogged = await auth.getUser()
    const user = (await User.query().with('roles').where('id', userLogged.id).first()).toJSON()
    const isTechnicalAdvisor = user.roles.find(val => val.id === 3)
    const isTechnicalChief= user.roles.find(val => val.id === 2)
    // Si el que edita es el asesor se debe colocar estatus 0: En espera de aprobación
    const visitingPlanning = (await VisitingPlanning.query().with('advisor').with('visitReason')
    .where('id', params.id).first()).toJSON()
    if (visitingPlanning) {
      const validation = await validate(request.all(), VisitingPlanning.fieldValidationRules())

      if (validation.fails()) {
        response.unprocessableEntity(validation.messages())
      } else {
        const body = request.only(VisitingPlanning.fillable)

        if (isTechnicalAdvisor && visitingPlanning.visitReason.require_approval) { // Si es asesor técnico y es un evento que requiere aprobación
          // Si la la planificación ya ha sido aprobada debe permanecer el estatus y guardarse esos datos en una tabla de solicitudes de planificación
          if (visitingPlanning.status == 1) {
            // Consultar si tiene una solicitud de edición pendiente
            const editingRequest = await VisitingPlanningUpdateRequest.query()
            .where('visiting_planning_id', visitingPlanning.id).where('type', 0).where('status', 0).first()

            if (editingRequest) { // si tiene una solicitud pendiente se debe sobreescribir
              // Datos a guardar
              editingRequest.event_type_id = body.event_type_id
              editingRequest.customer_id = body.customer_id
              editingRequest.shrimp_id = body.shrimp_id
              editingRequest.date = body.date
              editingRequest.time = body.time
              await editingRequest.save()
            } else { // Si no tiene solicitud de edición pendiente se debe crear una
              // Guardar datos actuales y pendientes por aprobar
              await VisitingPlanningUpdateRequest.create({
                visiting_planning_id: visitingPlanning.id,
                type: 0,
                status: 0,
                pre_event_type_id: visitingPlanning.event_type_id,
                pre_customer_id: visitingPlanning.customer_id,
                pre_shrimp_id: visitingPlanning.shrimp_id,
                pre_date: visitingPlanning.date,
                pre_time: visitingPlanning.time,
                event_type_id: body.event_type_id,
                customer_id: body.customer_id,
                shrimp_id: body.shrimp_id,
                date: body.date,
                time: body.time
              })
            }
            // Enviar notificación general al jefe
            const technicalTeams = (await TechnicalEquipment.query().with('chief').fetch()).toJSON()
            for (const element of technicalTeams) {
              const technicalAdvisorsIds = JSON.parse(element.technical_advisors)
              const ad = technicalAdvisorsIds.find(val => val == visitingPlanning.advisor.id)
              if (ad) {
                var technicalChiefId = element.chief.user_id
                break
              }
            }
            Notification.send({ // Envío de notificación de campana al usuario con el evento 4
              to: technicalChiefId,
              event: 4
            })
            visitingPlanning.require_approval = true // Notificar al front que requiere aprobación
          } else { // Si está en espera o rechazada se coloca en estado de espera de aprobación
            body.status = 0
            await VisitingPlanning.query().where('id', params.id).update(body)
          }
          
        } else { // Si no es asesor o si no requiere aprobación se modifica normalmente el evento
          await VisitingPlanning.query().where('id', params.id).update(body)
        }

        response.send(visitingPlanning)
      }
    } else {
      response.notFound(validation.messages())
    }
  }

  /**
   * Delete a visitingplanning with id.
   * DELETE appointmentplannings/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, response }) {
    const visitingPlanning = await VisitingPlanning.find(params.id)
    visitingPlanning.delete()
    response.send('success')
  }

  /**
   * Aprobar planificación de visitas
   * POST approve_appointment_plannings
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async approve ({ request, response }) {
    const body = request.all()
    let visitinPlanning = (await VisitingPlanning.query().with(['advisor.user']).where('id', body.id).first()).toJSON()
    await VisitingPlanning.query().where('id', body.id).update({
      status: 1
    })
    // Enviar correo y notificación de Campana
    Email.send({ // Envío de correo con el evento 2
      to: visitinPlanning.advisor.user.email,
      event: 2
    })

    Notification.send({ // Envío de notificación de campana al usuario con el evento 2
      to: visitinPlanning.advisor.user.id,
      event: 2
    })
    response.send('El evento se ha aprobado exitosamente')
  }
  /**
   * Aprobar planificación de visitas
   * POST reject_appointment_plannings
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async reject ({ request, response }) {
    const body = request.all()
    let visitinPlanning = (await VisitingPlanning.query().with(['advisor.user']).where('id', body.id).first()).toJSON()
    await VisitingPlanning.query().where('id', body.id).update({
      status: 2,
      reject_reason: body.reject_reason
    })
    // Enviar correo y notificación de Campana
    Email.send({ // Envío de correo con el evento 3
      to: visitinPlanning.advisor.user.email,
      event: 3
    })

    Notification.send({ // Envío de notificación de campana al usuario con el evento 3
      to: visitinPlanning.advisor.user.id,
      event: 3
    })
    response.send('El evento se ha rechazado exitosamente')
  }

  /**
   * Imprimir planificación de visitas según los registros enviados
   * Y según el formato recibido. Por defecto excel
   * POST calendar_excel
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async printCalendar ({ auth, request, response }) {
    const userLogged = await auth.getUser()
    const data = request.all()

    let startDate, endDate
    let appointmentPlannings = []
    if (data.calendarView == 'month') {
      startDate = moment(data.date).startOf('month').format('YYYY-MM-DD')
      endDate = moment(data.date).endOf('month').format('YYYY-MM-DD')
    } else if (data.calendarView == 'week') {
      startDate = moment(data.date).startOf('week').format('YYYY-MM-DD')
      endDate = moment(data.date).endOf('week').format('YYYY-MM-DD')
    } else {
      startDate = moment(data.date).format('YYYY-MM-DD')
      endDate = moment(data.date).format('YYYY-MM-DD')
    }

    const user = (await User.query().with('roles').where('id', userLogged.id).first()).toJSON()
    const isTechnicalAdvisor = user.roles.find(val => val.id === 3)
    const isTechnicalChief= user.roles.find(val => val.id === 2)

    if (isTechnicalAdvisor) { // Si es asesor técnico deben mostrarse las planificaciones propias aprobadas para los proximos 30 días
      const today = moment().format('YYYY-MM-DD')
      const dateAfter30days = moment().add(30, 'days').format('YYYY-MM-DD')
      appointmentPlannings = (await VisitingPlanning.query().with('customer').with('shrimp').with('visitReason')
      .where('technical_advisor_id', user.id).where('status', 1)
      .whereBetween('date', [startDate, endDate]).orderBy('date').fetch()).toJSON()
    } else if (isTechnicalChief) { // Si es jefe técnico deben mostrarse las planificaciones de sus asesores asignados e incluir el campo asesor en el excel
      const advisorsIds = []
      const technicalChief = (await TechnicalChief.query().with('teams').where('user_id', user.id).first()).toJSON()
      technicalChief.teams.forEach(element => {
        JSON.parse(element.technical_advisors).forEach(element2 => {
          advisorsIds.push(element2)
        })
      })
      // Consultar los users_ids de esos asesores para buscar sus planificaciones
      const technicalAdvisors = (await TechnicalAdvisor.query().whereIn('id', advisorsIds).fetch()).toJSON()
      const users_ids = technicalAdvisors.map(val => val.user_id)
      appointmentPlannings = (await VisitingPlanning.query().with('customer').with('shrimp').with('advisor')
      .with('visitReason') .whereIn('technical_advisor_id', users_ids).whereBetween('date', [startDate, endDate]).
      orderBy('date').fetch()).toJSON()
    }
    appointmentPlannings.forEach(element => {
      element.date = moment(element.date).format('DD-MM-YYYY')
      element.event_type_id = element.visitReason ? element.visitReason.name : 'Tipo de planificación eliminada'
      element.customer_name = element.customer ? element.customer.legal_name : ''
      if (element.shrimp) {
        element.location = element.shrimp.ubication ?  `${element.shrimp.ubication.lat}, ${element.shrimp.ubication.lng}` : 'Ubicación no guardada'
        element.shrimp_name = element.shrimp.name
      } else {
        element.location = 'Camaronera eliminada'
        element.shrimp_name = 'Camaronera eliminada'
      }
      if (isTechnicalChief) {
        element.technicalAdvisor = element.advisor.full_name
      }
    })
      
    const workbook = new ExcelJS.Workbook();
    const worksheet = workbook.addWorksheet("Visitas")
    const columns = [
      { header: "Fecha de visita", key: "date", width: 15 },
      { header: "Hora", key: "time", width: 15 },
      { header: "Camaronera a Visitar", key: "shrimp_name", width: 25 },
      { header: "Tipo de Planificación", key: "event_type_id", width: 20 },
      { header: "Cliente", key: `customer_name`, width: 20 },
      { header: "Ubicación", key: "location", width: 40 }
    ]
     // Add Array Rows
     if (isTechnicalChief) {
      columns.unshift({ header: "Asesor técnico", key: "technicalAdvisor", width: 15 })
    }

    worksheet.columns = columns

    worksheet.addRows(appointmentPlannings)
    worksheet.getRow(1).font = { bold: true };  

    // res is a Stream object
    response.header(
      "Content-Type",
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    );
    response.header(
      "Content-Disposition",
      "attachment; filename=" + "Planificacion.xlsx"
    );

    await workbook.xlsx.writeFile(Helpers.appRoot('Planificacion.xlsx'))

    response.download(Helpers.appRoot('Planificacion.xlsx'))
  }
  async approveAll ({ request, auth, response }) {
    const data = request.all()
    const user = await auth.getUser()
    let users_ids = []
    if (data.technical_advisor_id === -1) {// Si la aprobación es para todos los asesores técnicos de ese jefe técnico
      const advisorsIds = []
      const technicalChief = (await TechnicalChief.query().with('teams').where('user_id', user.id).first()).toJSON()
      technicalChief.teams.forEach(element => {
        JSON.parse(element.technical_advisors).forEach(element2 => {
          advisorsIds.push(element2)
        })
      })
      // Consultar los users_ids de esos asesores para buscar sus planificaciones
      const technicalAdvisors = (await TechnicalAdvisor.query().whereIn('id', advisorsIds).fetch()).toJSON()
      users_ids = technicalAdvisors.map(val => val.user_id)
    } else { // Si la aprobación es para un asesor técnico específico
      users_ids = [data.technical_advisor_id]
    }
    await VisitingPlanning.query().whereIn('technical_advisor_id', users_ids).whereIn('status', [0])
    .update({
      status: 1
    })
    response.send('Los eventos se han aprobado exitosamente')
  }

  async rejectAll ({ request, auth, response }) {
    const data = request.all()
    const user = await auth.getUser()
    let users_ids = []
    if (data.technical_advisor_id === -1) {// Si el rechazo es para todos los asesores técnicos de ese jefe técnico
      const advisorsIds = []
      const technicalChief = (await TechnicalChief.query().with('teams').where('user_id', user.id).first()).toJSON()
      technicalChief.teams.forEach(element => {
        JSON.parse(element.technical_advisors).forEach(element2 => {
          advisorsIds.push(element2)
        })
      })
      // Consultar los users_ids de esos asesores para buscar sus planificaciones
      const technicalAdvisors = (await TechnicalAdvisor.query().whereIn('id', advisorsIds).fetch()).toJSON()
      users_ids = technicalAdvisors.map(val => val.user_id)
    } else { // Si la aprobación es para un asesor técnico específico
      users_ids = [data.technical_advisor_id]
    }
    await VisitingPlanning.query().whereIn('technical_advisor_id', users_ids).whereIn('status', [0])
    .update({
      status: 2
    })
    response.send('Los eventos se han aprobado exitosamente')
  }

  async deletingRequest ({ request, response }) {
    const { visiting_planning_id, reason } = request.all()
    let res = {}

    // Consultar si ya tiene una solicitud de eliminación pendiente
    let editRequest = await VisitingPlanningUpdateRequest.query().with('visitingPlanning.advisor')
    .where({visiting_planning_id}).where('type', 1).where('status', 0).first()
    if (editRequest) {
      res.title = 'Solicitud en espera'
      res.message = 'Ya tienes una solicitud de eliminación en espera'
    } else {
      const visitingPlanning = (await VisitingPlanning.query().with('advisor').where('id', visiting_planning_id).first()).toJSON()
      const technical_advisor_id =  visitingPlanning.advisor.id

      // Consultar cual es su jefe técnico
      let technical_equipments = (await TechnicalEquipment.all()).toJSON()
      console.log('technical_advisor_id', technical_advisor_id)
      

      let technicalChiefId
      for (const element of technical_equipments) {
        const technical_advisors = JSON.parse(element.technical_advisors).find(val => val === technical_advisor_id)
        if (technical_advisors) {
          technicalChiefId = element.technical_chief
          var technicalChief = await TechnicalChief.query().where('id', technicalChiefId).first()
          break
        }
      }
      const visitingPlanningUpdateRequest = await VisitingPlanningUpdateRequest.create({
        visiting_planning_id,
        type: 1,
        reason
      })
      console.log('technicalChief', technicalChief)
      const technicalAdvisor = await TechnicalAdvisor.query().where('id', technical_advisor_id).first()
      const notificationParams = {
        id: visitingPlanningUpdateRequest.id,
        title: 'Solicitud de eliminación',
        technicalAdvisor: technicalAdvisor.full_name,
        reason,
        event: 5
      }
      Notification.send({ // Envío de notificación de campana al usuario con el evento 2
        to: technicalChief.user_id,
        event: 5,
        params: JSON.stringify(notificationParams)
      })
      res.title = 'Solicitud enviada'
      res.message = 'Tu solicitud ha sido enviada al jefe técnico, cuando sea aprobada podrás eliminar la planificación seleccionada.'
    }

    response.send(res)
  }
  async processEditingAppointmentRequest ({ request, response }) {
    const req = request.all()
    console.log(req.status)
    const editingRequest = (await VisitingPlanningUpdateRequest.query().where('id', req.id).first()).toJSON()

    await VisitingPlanningUpdateRequest.query().where('id', req.id).update({status: req.status})

    // Si se aprobó se debe actualizar el registro con los datos que solicitó el asesor
    if (req.status == 1) {
      // Se debe actualizar el registro con los datos que solicitó el asesor
      await VisitingPlanning.query().where('id', editingRequest.visiting_planning_id).update({
        event_type_id: editingRequest.event_type_id,
        customer_id: editingRequest.customer_id,
        shrimp_id: editingRequest.shrimp_id,
        customer_id: editingRequest.customer_id,
        date: editingRequest.date,
        time: editingRequest.time
      })
      const visitingPlanning = (await VisitingPlanning.query().with('advisor.user')
      .where('id', editingRequest.visiting_planning_id).first()).toJSON()
      // Enviar notificación a asesor acerca de la aprobación o rechazo de su solicitud
      Notification.send({ // Envío de notificación de campana al usuario con el evento 2
        to: visitingPlanning.advisor.user.id,
        event: 2
      })
    }

    return response.send(true)
  }
  async processDeletingAppointmentRequest ({ request, response }) {
    const req = request.all()
    console.log(req.status)
    await VisitingPlanningUpdateRequest.query().where('id', req.id).update({status: req.status})
    return response.send(true)
  }
  async acceptRejection ({ request }) {
    const res = request.all()
    await VisitingPlanningUpdateRequest.query().where('visiting_planning_id', res.id).where('type', res.type)
    .where('status', res.status).delete()
  }
  async setSeenUpdateRequestNotification ({ auth, request }) {
    const userLogged = await auth.getUser()
    const user = (await User.query().with('roles').where('id', userLogged.id).first()).toJSON()
    const isTechnicalAdvisor = user.roles.find(val => val.id === 3)

    const notificationId = request.input('notificationId')
    // Si es asesor técnico
    if (isTechnicalAdvisor) {
      await VisitingPlanningUpdateRequest.query().where('id', notificationId).update({ seen: true })
    }
    return true
    
  }
  async registerUbication ({ request, response }) {
    const req = request.all()
    const real_date = moment(req.timestamp).format('YYYY-MM-DD')
    const real_time = momentT(req.timestamp).tz("America/Guayaquil").format('HH:mm:ss')
    const appointmentPlanning = await VisitingPlanning.query().with('shrimp').where('id', req.id).first()
    const shrimpUbicationInfo = appointmentPlanning.toJSON().shrimp.ubication
    const shrimpUbication = typeof shrimpUbicationInfo === 'object' ? shrimpUbicationInfo : JSON.parse(shrimpUbicationInfo)
    // Calcular la distancia(línea recta) entre el sitio de marcaje y la camaronera
    const distance = geolib.getDistance(shrimpUbication, req.ubication)

    appointmentPlanning.ubication = req.ubication
    appointmentPlanning.real_date = real_date
    appointmentPlanning.real_time = real_time
    appointmentPlanning.shrimp_distance = distance
    await appointmentPlanning.save()
    
    response.send(distance)
  }
}

module.exports = VisitingPlanningController
