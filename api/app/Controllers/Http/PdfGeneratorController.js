"use strict";
const Env = use('Env')
const Details = use("App/Models/FoodProjectionDetail")
const Projections = use ("App/Models/FoodProjectionMaster")
const ShrimpPrice = use('App/Models/ShrimpPrice')
const appSecret = Env.get('URLBASE')
const Format = use("App/Models/Format");
const User = use("App/Models/User");
const PdfPrinter = use("pdfmake");
const mkdirp = use("mkdirp");
const b64 = use("image-to-base64");
const Helpers = use("Helpers")
const fs = use("fs")
const Shrimp = use("App/Models/Shrimp")
const FishingProjection = use("App/Models/FishingProjection")
const moment = use("moment")
const WaterAnalysisMaster = use('App/Models/WaterAnalysisPool')
const FloorAnalysisMaster = use('App/Models/FloorAnalysisPool')
const PathologyAnalysisMaster = use('App/Models/PatologiesAnalysisPool')
const PhytoplanktonAnalysisMaster = use('App/Models/PhytoplanktonAnalysisPool')
const FoodProjectionFunctions = use("App/Functions/FoodProjection")
const Img = use('App/Models/PathologiesImg')
const typeOptions = [
  {
    id: 1,
    name: 'Directa'
  },
  {
    id: 2,
    name: 'Transferida'
  },
  {
    id: 3,
    name: 'Bifásica'
  },
  {
    id: 4,
    name: 'Trifásica'
  }
]
function replace (params) {
  var value = params.content;
  if (value === undefined) value = "";
  value = value.replace(/\[([a-z].*?)\]/gi, function (val) {
    var variable = val.replace(/(\[|\])/g, "");
    return params.variables[variable];
  });
  value =
    value !== "" &&
      (Number(value) || Number(value) === 0) &&
      value.trim().split("")[0] !== "0"
      ? Number(value)
      : value;
  if (value === undefined || value === "undefined") value = "";
  return value;
}
async function urlBase64 (url) {
  return new Promise(resolve => {
    var request = require("request").defaults({
      encoding: null
    });
    request.get(url, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        data =
          "data:" +
          response.headers["content-type"] +
          ";base64," +
          Buffer.from(body).toString("base64");
        resolve(data);
      } else console.log(error);
    });
  });
}

async function fileSearch (url) {
  return new Promise(resolve => {
    setTimeout(function () {
      resolve(Helpers.appRoot('storage/files/cotizaciones') + `/${url}`);
    }, 1000)
  });
}


// Define font files
var fonts = {
  Roboto: {
    normal: 'resources/fonts/Roboto-Regular.ttf',
    bold: 'resources/fonts/Roboto-Medium.ttf',
    italics: 'resources/fonts/Roboto-Italic.ttf',
    bolditalics: 'resources/fonts/Roboto-MediumItalic.ttf'
  }
}
class PdfGeneratorController {
  async index ({ request, response, view }) {
    let imagen = {};
    let espacios = "",
      espaciosAtentamente = "";
    var fonts = {
      Roboto: {
        normal: "storage/fonts/Roboto-Regular.ttf",
        bold: "storage/fonts/Roboto-Medium.ttf",
        italics: "storage/fonts/Roboto-Italic.ttf",
        bolditalics: "storage/fonts/Roboto-MediumItalic.ttf"
      }
    };
    var printer = new PdfPrinter(fonts);
    let datos = request._all.datos;
    let reporte = request._all.reporte;
    let img_firma = {},
      dd = {},
      firma = [],
      firmas = { uno: "", dos: "", tres: "", cuatro: "" },
      firmaDig = [],
      atentamente = {},
      ems = [],
      soporte = "";
    let formato = await Format.where({ reporte, activo: true }).fetch();
    formato = formato.toJSON();
    formato = formato[0];
    let dpto = (await Company.all()).toJSON();
    dpto = dpto[0];
    if (formato.hasOwnProperty("titulo"))
      // var titulo = await replace({ variables: datos, content: formato.titulo })
      var titulo = formato.titulo ? formato.titulo : "";
    let cadena = await replace({
      variables: datos,
      content: formato.contenido
    });
    let cadena_email = cadena;
    /* Crea la estructura al formato necesario */
    var bext = /\s<b></gi;
    var bext_text = " |{'bold': true,<";
    var cext = /\s<i></gi;
    var cext_text = " |{'italics': true,<";
    var bint = /,<b>/gi;
    var bint_text = ",'bold': true, 'text':'";
    var cint = /,<i>/gi;
    var cint_text = ",'italics': true, 'text':'";
    var rest = /(?:<[^>]+>)/gi;
    var rest_text = "'}|";
    var cerr = /('}\|[$'])/gi;
    var cerr_text = "'";
    var ent = /<br>/gi;
    var ent_text = "\n\n";
    var eli = /<div>|<\/div>/gi;
    var todo = /(?:<[^>]+>)/gi;
    cadena = cadena.replace(ent, ent_text);
    cadena = cadena.replace(eli, "");
    if (/&nbsp;/gi.test(cadena)) {
      cadena = cadena.replace(/&nbsp;/gi, " ");
    }
    if (/\s<b><i>/gi.test(cadena)) {
      cadena = cadena.replace(bext, bext_text);
      cadena = cadena.replace(cint, cint_text);
    }
    if (/\s<i><b>/gi.test(cadena)) {
      cadena = cadena.replace(cext, cext_text);
      cadena = cadena.replace(bint, bint_text);
    }
    if (/(:?\s<b>|\s<b style\="">)/gi.test(cadena)) {
      cadena = cadena.replace(
        /(:?\s<b>|\s<b style\="">)/gi,
        " |{'bold': true,'text':'"
      );
    }
    if (/(:?\s<i>|\s<i style\="">)/gi.test(cadena)) {
      cadena = cadena.replace(
        /(:?\s<i>|\s<i style\="">)/gi,
        " |{'italics': true,'text':'"
      );
    }
    cadena = cadena.replace(rest, rest_text);
    cadena = cadena.replace(cerr, cerr_text);
    cadena = cadena.replace(todo, "");
    cadena = cadena.split("|");
    //return cadena
    for (var x in cadena) {
      if (cadena[x].charAt(0) == "{") {
        cadena[x] = cadena[x].replace(/'/gi, '"');
        cadena[x] = JSON.parse(cadena[x]);
      }
    }
    /* Fin a Crear la estructura al formato necesario */
    /* Agregar logo */
    if (formato.config.encabezado.logo) {
      let logo = Helpers.appRoot("storage") + `/${dpto.logo}`;
      logo = await b64(logo).then(res => {
        return "data:image/png;base64, " + res;
      });
      imagen = {
        image: logo,
        width: formato.config.encabezado.logo_dimension,
        fit: [
          formato.config.encabezado.logo_dimension * 28,
          formato.config.encabezado.logo_dimension * 28
        ],
        //height: formato.config.encabezado.logo_dimension,
        alignment: formato.config.encabezado.logo_alineacion,
        margin: [
          formato.config.encabezado.l_derecho * 28,
          formato.config.encabezado.l_superior * 28,
          formato.config.encabezado.l_izquierdo * 28,
          formato.config.encabezado.l_inferior * 28
        ]
      };
    }
    /* Espacio entre titulo y contenido */
    for (let x = 0; x < formato.config.encabezado.espacio; x++) {
      espacios += "\n";
    }

    /* Firmas
       4 Firmas
       Firma Empleado */
    let fcliente = async nro => {
      firmas[nro] = `${datos.nombre_cliente}\n${datos.empresa_cliente}`;
      if (formato.config.contenido.img_uno) {
        img_firma[nro] = (
          await DigitalSignature.where({ employee: datos.empleado })
            .limit(1)
            .fetch()
        ).toJSON();
        if (img_firma[nro].length) {
          img_firma[nro] =
            Helpers.appRoot("storage/DigitalSignature") +
            `/${img_firma[nro][0].file}`;
          img_firma[nro] = await b64(img_firma[nro]).then(res => {
            return "data:image/png;base64, " + res;
          });
        } else img_firma[nro] = null;
      }
    };
    let fventa = async nro => {
      let data = (
        await User.where({ _id: "5dfa409bb01b552c592b05ff" })
          .limit(1)
          .fetch()
      ).toJSON();
      data = data[0];
      firmas[nro] = `${data.name} ${data.last_name}`;
      if (formato.config.contenido.img_uno) {
        img_firma[nro] = (
          await DigitalSignature.where({ employee: datos.empleado })
            .limit(1)
            .fetch()
        ).toJSON();
        if (img_firma[nro].length) {
          img_firma[nro] =
            Helpers.appRoot("storage/DigitalSignature") +
            `/${img_firma[nro][0].file}`;
          img_firma[nro] = await b64(img_firma[nro]).then(res => {
            return "data:image/png;base64, " + res;
          });
        } else img_firma[nro] = null;
      }
    };
    let fgerente = async nro => {
      let data = (await Company.all()).toJSON();
      data = data[0];
      firmas[nro] = `${data.boss_name} ${data.boss_last_name}\n${data.name}`;
      if (formato.config.contenido.img_uno) {
        img_firma[nro] = (
          await DigitalSignature.where({ employee: "5dfa409ab01b552c592b05fe" })
            .limit(1)
            .fetch()
        ).toJSON();
        if (img_firma[nro].length) {
          img_firma[nro] =
            Helpers.appRoot("storage/DigitalSignature") +
            `/${img_firma[nro][0].file}`;
          img_firma[nro] = await b64(img_firma[nro]).then(res => {
            return "data:image/png;base64, " + res;
          });
        } else img_firma[nro] = null;
      }
    };
    if (formato.config.contenido.firma_uno == "f-cliente") {
      await fcliente("uno");
    }
    if (formato.config.contenido.firma_dos == "f-cliente") {
      await fcliente("dos");
    }
    if (formato.config.contenido.firma_tres == "f-cliente") {
      await fcliente("tres");
    }
    if (formato.config.contenido.firma_cuatro == "f-cliente") {
      await fcliente("cuatro");
    }
    /* Firma Departamento */
    if (formato.config.contenido.firma_uno == "f-venta") {
      await fventa("uno");
    }
    if (formato.config.contenido.firma_dos == "f-venta") {
      await fventa("dos");
    }
    if (formato.config.contenido.firma_tres == "f-venta") {
      await fventa("tres");
    }
    if (formato.config.contenido.firma_cuatro == "f-venta") {
      await fventa("cuatro");
    }
    /* Firma Jefe Recursos Humanos*/
    if (formato.config.contenido.firma_uno == "f-gerente") {
      await fgerente("uno");
    }
    if (formato.config.contenido.firma_dos == "f-gerente") {
      await fgerente("dos");
    }
    if (formato.config.contenido.firma_tres == "f-gerente") {
      await fgerente("tres");
    }
    if (formato.config.contenido.firma_cuatro == "f-gerente") {
      await fgerente("cuatro");
    }
    if (
      formato.config.contenido.firma_uno &&
      formato.config.contenido.firma_dos &&
      formato.config.contenido.firma_tres &&
      formato.config.contenido.firma_cuatro
    ) {
      if (img_firma["uno"])
        firmaDig.push({
          image: img_firma["uno"],
          width: 150,
          absolutePosition: {
            x: 100,
            y: 540
          }
        });
      if (img_firma["dos"])
        firmaDig.push({
          image: img_firma["dos"],
          width: 150,
          absolutePosition: {
            x: 340,
            y: 540
          }
        });
      if (img_firma["tres"])
        firmaDig.push({
          image: img_firma["tres"],
          width: 150,
          absolutePosition: {
            x: 100,
            y: 627
          }
        });
      if (img_firma["cuatro"])
        firmaDig.push({
          image: img_firma["cuatro"],
          width: 150,
          absolutePosition: {
            x: 340,
            y: 627
          }
        });
      firma = [
        firmaDig,
        {
          text: firmas.uno,
          absolutePosition: {
            x: -160,
            y: 630
          },
          alignment: "center"
        },
        {
          text: firmas.dos,
          absolutePosition: {
            x: 320,
            y: 630
          },
          alignment: "center"
        },
        {
          text: firmas.tres,
          absolutePosition: {
            x: -160,
            y: 717
          },
          alignment: "center"
        },
        {
          text: firmas.cuatro,
          absolutePosition: {
            x: 320,
            y: 717
          },
          alignment: "center"
        }
      ];
    } else if (
      /* 3 Firmas */
      formato.config.contenido.firma_uno &&
      formato.config.contenido.firma_dos &&
      formato.config.contenido.firma_tres
    ) {
      if (img_firma["uno"])
        firmaDig.push({
          image: img_firma["uno"],
          width: 150,
          absolutePosition: {
            x: 100,
            y: 560
          }
        });
      if (img_firma["dos"])
        firmaDig.push({
          image: img_firma["dos"],
          width: 150,
          absolutePosition: {
            x: 222,
            y: 627
          }
        });
      if (img_firma["tres"])
        firmaDig.push({
          image: img_firma["tres"],
          width: 150,
          absolutePosition: {
            x: 340,
            y: 560
          }
        });
      firma = [
        firmaDig,
        {
          text: firmas.uno,
          absolutePosition: {
            x: -160,
            y: 650
          },
          alignment: "center"
        },
        {
          text: firmas.dos,
          absolutePosition: {
            x: 85,
            y: 717
          },
          alignment: "center"
        },
        {
          text: firmas.tres,
          absolutePosition: {
            x: 320,
            y: 650
          },
          alignment: "center"
        }
      ];
    } else if (
      /* 2 Firmas */
      formato.config.contenido.firma_uno &&
      formato.config.contenido.firma_dos
    ) {
      if (img_firma["uno"])
        firmaDig.push({
          image: img_firma["uno"],
          width: 150,
          absolutePosition: {
            x: 100,
            y: 600
          }
        });
      if (img_firma["dos"])
        firmaDig.push({
          image: img_firma["dos"],
          width: 150,
          absolutePosition: {
            x: 340,
            y: 600
          }
        });
      firma = [
        firmaDig,
        {
          text: firmas.uno,
          absolutePosition: {
            x: -160,
            y: 700
          },
          alignment: "center"
        },
        {
          text: firmas.dos,
          absolutePosition: {
            x: 320,
            y: 700
          },
          alignment: "center"
        }
      ];
    } else if (formato.config.contenido.firma_uno) {
      if (img_firma["uno"])
        firmaDig.push({
          image: img_firma["uno"],
          width: 150,
          absolutePosition: {
            x:
              formato.config.contenido.firma_alineacion == "center"
                ? 220
                : formato.config.contenido.firma_alineacion == "left"
                  ? 70
                  : 340,
            y: 590
          }
        });
      if (formato.config.contenido.firma_alineacion == "left") {
        firma = [
          firmaDig,
          {
            text: firmas.uno,
            absolutePosition: {
              x: -220,
              y: 680
            },
            alignment: "center"
          }
        ];
      }
      if (formato.config.contenido.firma_alineacion == "center") {
        firma = [
          firmaDig,
          {
            text: firmas.uno,
            absolutePosition: {
              x: 85,
              y: 680
            },
            alignment: "center"
          }
        ];
      }
      if (formato.config.contenido.firma_alineacion == "right") {
        firma = [
          firmaDig,
          {
            text: firmas.uno,
            absolutePosition: {
              x: 320,
              y: 680
            },
            alignment: "center"
          }
        ];
      }
    }

    /* Fecha */
    // fecha = formato.config.contenido.fecha_activo ? formato.fecha : "";
    /* Atentamente */
    for (let x = 0; x < formato.config.contenido.atentamente_espacio; x++) {
      espaciosAtentamente += "\n";
    }
    atentamente = formato.config.contenido.atentamente
      ? {
        text: `${espaciosAtentamente}\nAtentamente,`,
        alignment: formato.config.contenido.atentamente_alineacion
      }
      : "";

    /* Imagen de Soporte */
    if (formato.config.pie.soporte && datos.file) {
      let img = Helpers.appRoot("storage") + `/${datos.file}`;
      img = await b64(img).then(res => {
        return "data:image/png;base64, " + res;
      });
      soporte = [
        {
          text: "ANEXO\n\n\n",
          bold: true,
          alignment: "center",
          pageBreak: "before"
        },
        {
          image: img,
          fit: [400, 400],
          alignment: "center"
        }
      ];
    }
    let imgHeader = await b64(
      Helpers.appRoot("storage") + `/encabezado.jpg`
    ).then(res => {
      return "data:image/jpg;base64, " + res;
    });
    let imgFooter = await b64(Helpers.appRoot("storage") + `/pie.png`).then(
      res => {
        return "data:image/png;base64, " + res;
      }
    );
    /* Estructura para el PDF */
    dd = {
      pageSize: formato.config.general.hoja,
      pageOrientation: formato.config.general.orientacion,
      pageMargins: [
        formato.config.general.m_derecho * 28,
        formato.config.general.m_superior * 28,
        formato.config.general.m_izquierdo * 28,
        formato.config.general.m_inferior * 28
      ],
      header: {
        image: imgHeader,
        width: 595
      },
      content: [
        //imagen,
        {
          text: titulo,
          alignment: formato.config.encabezado.alineacion,
          fontSize: formato.config.encabezado.letra,
          bold: formato.config.encabezado.negrita,
          italics: formato.config.encabezado.cursiva
        },
        espacios,
        {
          alignment: formato.config.contenido.alineacion,
          text: cadena
        },
        {
          text: formato.config.contenido.fecha_activo
            ? `\n\n\n${formato.fecha}`
            : "",
          alignment: formato.config.contenido.fecha_alineacion
        },
        atentamente,
        firma,
        soporte
      ],
      /* [
          "\n",
          {
            text: formato.pie,
            fontSize: formato.config.pie.letra,
            color: "gray",
            alignment: formato.config.pie.alineacion
          }

        ] */
      footer: {
        image: imgFooter,
        width: 595,
        absolutePosition: {
          x: 0,
          y: formato.config.general.m_inferior * 28 + -92.2
        }
      },
      defaultStyle: {
        fontSize: formato.config.general.letra
      }
    };
    var loc = `${Helpers.appRoot("storage")}/files/${reporte}`;
    mkdirp.sync(loc);
    var pdfDoc = printer.createPdfKitDocument(dd);
    var path = `${loc}/${reporte}.pdf`;
    pdfDoc.pipe(fs.createWriteStream(path));
    pdfDoc.end();
    /* let correo = await get("usuarios", {
        conditions: {
          _id: datos.empleado
        }
      });
      if (
        formato.config.general.correo &&
        datos.hasOwnProperty("correo") &&
        datos.correo
      ) {
        ems.push({
          email: correo[0].email,
          subject: titulo,
          isHtml: true,
          content: cadena_email,
          cc: formato.config.general.correo_copia ?
            formato.config.general.correo_enviar : "",
          attachment: path
        });
      }


      setTimeout(async () => {
        if (
          formato.config.general.correo &&
          datos.hasOwnProperty("correo") &&
          datos.correo
        )
          //console.log("send-email: ", await functions("send-mail", ems));
        response.send({
          pdf: dd,
          reporte: `${reporte} - ${datos.last_name} ${datos.first_name}.pdf`,
          path
        });

      }, 1000);*/

    response.send({
      pdf: dd,
      reporte: `${reporte}.pdf`,
      path
    });
  }

  async createByFormat ({ params, request, response, view }) {
    let imagen = {}
    let espacios = "",
      espaciosAtentamente = "";
    var fonts = {
      Roboto: {
        normal: "storage/fonts/Roboto-Regular.ttf",
        bold: "storage/fonts/Roboto-Medium.ttf",
        italics: "storage/fonts/Roboto-Italic.ttf",
        bolditalics: "storage/fonts/Roboto-MediumItalic.ttf"
      }
    };
    var printer = new PdfPrinter(fonts);
    let datos = request._all.datos;
    let reporte = request._all.reporte;
    let img_firma = {},
      dd = {},
      firma = [],
      firmas = { uno: "", dos: "", tres: "", cuatro: "" },
      firmaDig = [],
      atentamente = {},
      ems = [],
      soporte = "";
    let formato = await Format.find(params.id);
    // return params.id
    formato = formato.toJSON();
    formato.config = JSON.parse(formato.config)
    /* let dpto = (await Company.all()).toJSON();
    dpto = dpto[0]; */
    let dpto = {
      "name": "Wondrix",
      "position": "Gerente General",
      "boss_name": "Juan",
      "boss_last_name": "Torbay",
      "boss_mail": "suarez9ster@gmail.com",
      "direction": "Carretera Panamericana Via SAlom, Sector LAs Lagunas, Venezuela",
      "phone": "04127520012"
    }
    if (formato.hasOwnProperty("title"))
    // return formato
      // var titulo = await replace({ variables: datos, content: formato.titulo })
      var titulo = formato.title ? formato.title : "";
    let cadena = await replace({
      variables: datos,
      content: formato.content
    });
    let cadena_email = cadena;
    /* Crea la estructura al formato necesario */
    var bext = /\s<b></gi;
    var bext_text = " |{'bold': true,<";
    var cext = /\s<i></gi;
    var cext_text = " |{'italics': true,<";
    var bint = /,<b>/gi;
    var bint_text = ",'bold': true, 'text':'";
    var cint = /,<i>/gi;
    var cint_text = ",'italics': true, 'text':'";
    var rest = /(?:<[^>]+>)/gi;
    var rest_text = "'}|";
    var cerr = /('}\|[$'])/gi;
    var cerr_text = "'";
    var ent = /<br>/gi;
    var ent_text = "\n\n";
    var eli = /<div>|<\/div>/gi;
    var todo = /(?:<[^>]+>)/gi;
    cadena = cadena.replace(ent, ent_text);
    cadena = cadena.replace(eli, "");
    if (/&nbsp;/gi.test(cadena)) {
      cadena = cadena.replace(/&nbsp;/gi, " ");
    }
    if (/\s<b><i>/gi.test(cadena)) {
      cadena = cadena.replace(bext, bext_text);
      cadena = cadena.replace(cint, cint_text);
    }
    if (/\s<i><b>/gi.test(cadena)) {
      cadena = cadena.replace(cext, cext_text);
      cadena = cadena.replace(bint, bint_text);
    }
    if (/(:?\s<b>|\s<b style\="">)/gi.test(cadena)) {
      cadena = cadena.replace(
        /(:?\s<b>|\s<b style\="">)/gi,
        " |{'bold': true,'text':'"
      );
    }
    if (/(:?\s<i>|\s<i style\="">)/gi.test(cadena)) {
      cadena = cadena.replace(
        /(:?\s<i>|\s<i style\="">)/gi,
        " |{'italics': true,'text':'"
      );
    }
    cadena = cadena.replace(rest, rest_text);
    cadena = cadena.replace(cerr, cerr_text);
    cadena = cadena.replace(todo, "");
    cadena = cadena.split("|");
    //return cadena
    for (var x in cadena) {
      if (cadena[x].charAt(0) == "{") {
        cadena[x] = cadena[x].replace(/'/gi, '"');
        cadena[x] = JSON.parse(cadena[x]);
      }
    }
    /* Fin a Crear la estructura al formato necesario */
    /* Agregar logo */
    // return formato
    if (formato.config.encabezado.logo) {

      let logo = Helpers.appRoot("storage") + `/${dpto.logo}`;
      /* return logo
      logo = await b64(logo).then(res => {
        return "data:image/png;base64, " + res;
      }); */

      imagen = {
        image: logo,
        width: formato.config.encabezado.logo_dimension,
        fit: [
          formato.config.encabezado.logo_dimension * 28,
          formato.config.encabezado.logo_dimension * 28
        ],
        //height: formato.config.encabezado.logo_dimension,
        alignment: formato.config.encabezado.logo_alineacion,
        margin: [
          formato.config.encabezado.l_derecho * 28,
          formato.config.encabezado.l_superior * 28,
          formato.config.encabezado.l_izquierdo * 28,
          formato.config.encabezado.l_inferior * 28
        ]
      };
    }
    // return 'bien'
    /* Espacio entre titulo y contenido */
    for (let x = 0; x < formato.config.encabezado.espacio; x++) {
      espacios += "\n";
    }

    /* Firmas
       4 Firmas
       Firma Empleado */
    let fcliente = async nro => {
      firmas[nro] = `${datos.nombre_cliente}\n${datos.empresa_cliente}`;
      if (formato.config.contenido.img_uno) {
        img_firma[nro] = (
          await DigitalSignature.where({ employee: datos.empleado })
            .limit(1)
            .fetch()
        ).toJSON();
        if (img_firma[nro].length) {
          img_firma[nro] =
            Helpers.appRoot("storage/DigitalSignature") +
            `/${img_firma[nro][0].file}`;
          img_firma[nro] = await b64(img_firma[nro]).then(res => {
            return "data:image/png;base64, " + res;
          });
        } else img_firma[nro] = null;
      }
    };
    let fventa = async nro => {
      let data = (
        await User.where({ _id: "5dfa409bb01b552c592b05ff" })
          .limit(1)
          .fetch()
      ).toJSON();
      data = data[0];
      firmas[nro] = `${data.name} ${data.last_name}`;
      if (formato.config.contenido.img_uno) {
        img_firma[nro] = (
          await DigitalSignature.where({ employee: datos.empleado })
            .limit(1)
            .fetch()
        ).toJSON();
        if (img_firma[nro].length) {
          img_firma[nro] =
            Helpers.appRoot("storage/DigitalSignature") +
            `/${img_firma[nro][0].file}`;
          img_firma[nro] = await b64(img_firma[nro]).then(res => {
            return "data:image/png;base64, " + res;
          });
        } else img_firma[nro] = null;
      }
    };
    let fgerente = async nro => {
      /* let data = (await Company.all()).toJSON();
      data = data[0]; */
      let data = {
        "name": "Wondrix",
        "position": "Gerente General",
        "boss_name": "Juan",
        "boss_last_name": "Torbay",
        "boss_mail": "suarez9ster@gmail.com",
        "direction": "Carretera Panamericana Via SAlom, Sector LAs Lagunas, Venezuela",
        "phone": "04127520012"
      }

      firmas[nro] = `${data.boss_name} ${data.boss_last_name}\n${data.name}`;
      if (formato.config.contenido.img_uno) {
        img_firma[nro] = (
          await DigitalSignature.where({ employee: "5dfa409ab01b552c592b05fe" })
            .limit(1)
            .fetch()
        ).toJSON();
        if (img_firma[nro].length) {
          img_firma[nro] =
            Helpers.appRoot("storage/DigitalSignature") +
            `/${img_firma[nro][0].file}`;
          img_firma[nro] = await b64(img_firma[nro]).then(res => {
            return "data:image/png;base64, " + res;
          });
        } else img_firma[nro] = null;
      }
    };
    if (formato.config.contenido.firma_uno == "f-cliente") {
      await fcliente("uno");
    }
    if (formato.config.contenido.firma_dos == "f-cliente") {
      await fcliente("dos");
    }
    if (formato.config.contenido.firma_tres == "f-cliente") {
      await fcliente("tres");
    }
    if (formato.config.contenido.firma_cuatro == "f-cliente") {
      await fcliente("cuatro");
    }
    /* Firma Departamento */
    if (formato.config.contenido.firma_uno == "f-venta") {
      await fventa("uno");
    }
    if (formato.config.contenido.firma_dos == "f-venta") {
      await fventa("dos");
    }
    if (formato.config.contenido.firma_tres == "f-venta") {
      await fventa("tres");
    }
    if (formato.config.contenido.firma_cuatro == "f-venta") {
      await fventa("cuatro");
    }
    /* Firma Jefe Recursos Humanos*/
    if (formato.config.contenido.firma_uno == "f-gerente") {
      await fgerente("uno");
    }
    if (formato.config.contenido.firma_dos == "f-gerente") {
      await fgerente("dos");
    }
    if (formato.config.contenido.firma_tres == "f-gerente") {
      await fgerente("tres");
    }
    if (formato.config.contenido.firma_cuatro == "f-gerente") {
      await fgerente("cuatro");
    }
    if (
      formato.config.contenido.firma_uno &&
      formato.config.contenido.firma_dos &&
      formato.config.contenido.firma_tres &&
      formato.config.contenido.firma_cuatro
    ) {
      if (img_firma["uno"])
        firmaDig.push({
          image: img_firma["uno"],
          width: 150,
          absolutePosition: {
            x: 100,
            y: 540
          }
        });
      if (img_firma["dos"])
        firmaDig.push({
          image: img_firma["dos"],
          width: 150,
          absolutePosition: {
            x: 340,
            y: 540
          }
        });
      if (img_firma["tres"])
        firmaDig.push({
          image: img_firma["tres"],
          width: 150,
          absolutePosition: {
            x: 100,
            y: 627
          }
        });
      if (img_firma["cuatro"])
        firmaDig.push({
          image: img_firma["cuatro"],
          width: 150,
          absolutePosition: {
            x: 340,
            y: 627
          }
        });
      firma = [
        firmaDig,
        {
          text: firmas.uno,
          absolutePosition: {
            x: -160,
            y: 630
          },
          alignment: "center"
        },
        {
          text: firmas.dos,
          absolutePosition: {
            x: 320,
            y: 630
          },
          alignment: "center"
        },
        {
          text: firmas.tres,
          absolutePosition: {
            x: -160,
            y: 717
          },
          alignment: "center"
        },
        {
          text: firmas.cuatro,
          absolutePosition: {
            x: 320,
            y: 717
          },
          alignment: "center"
        }
      ];
    } else if (
      /* 3 Firmas */
      formato.config.contenido.firma_uno &&
      formato.config.contenido.firma_dos &&
      formato.config.contenido.firma_tres
    ) {
      if (img_firma["uno"])
        firmaDig.push({
          image: img_firma["uno"],
          width: 150,
          absolutePosition: {
            x: 100,
            y: 560
          }
        });
      if (img_firma["dos"])
        firmaDig.push({
          image: img_firma["dos"],
          width: 150,
          absolutePosition: {
            x: 222,
            y: 627
          }
        });
      if (img_firma["tres"])
        firmaDig.push({
          image: img_firma["tres"],
          width: 150,
          absolutePosition: {
            x: 340,
            y: 560
          }
        });
      firma = [
        firmaDig,
        {
          text: firmas.uno,
          absolutePosition: {
            x: -160,
            y: 650
          },
          alignment: "center"
        },
        {
          text: firmas.dos,
          absolutePosition: {
            x: 85,
            y: 717
          },
          alignment: "center"
        },
        {
          text: firmas.tres,
          absolutePosition: {
            x: 320,
            y: 650
          },
          alignment: "center"
        }
      ];
    } else if (
      /* 2 Firmas */
      formato.config.contenido.firma_uno &&
      formato.config.contenido.firma_dos
    ) {
      if (img_firma["uno"])
        firmaDig.push({
          image: img_firma["uno"],
          width: 150,
          absolutePosition: {
            x: 100,
            y: 600
          }
        });
      if (img_firma["dos"])
        firmaDig.push({
          image: img_firma["dos"],
          width: 150,
          absolutePosition: {
            x: 340,
            y: 600
          }
        });
      firma = [
        firmaDig,
        {
          text: firmas.uno,
          absolutePosition: {
            x: -160,
            y: 700
          },
          alignment: "center"
        },
        {
          text: firmas.dos,
          absolutePosition: {
            x: 320,
            y: 700
          },
          alignment: "center"
        }
      ];
    } else if (formato.config.contenido.firma_uno) {
      if (img_firma["uno"])
        firmaDig.push({
          image: img_firma["uno"],
          width: 150,
          absolutePosition: {
            x:
              formato.config.contenido.firma_alineacion == "center"
                ? 220
                : formato.config.contenido.firma_alineacion == "left"
                  ? 70
                  : 340,
            y: 590
          }
        });
      if (formato.config.contenido.firma_alineacion == "left") {
        firma = [
          firmaDig,
          {
            text: firmas.uno,
            absolutePosition: {
              x: -220,
              y: 680
            },
            alignment: "center"
          }
        ];
      }
      if (formato.config.contenido.firma_alineacion == "center") {
        firma = [
          firmaDig,
          {
            text: firmas.uno,
            absolutePosition: {
              x: 85,
              y: 680
            },
            alignment: "center"
          }
        ];
      }
      if (formato.config.contenido.firma_alineacion == "right") {
        firma = [
          firmaDig,
          {
            text: firmas.uno,
            absolutePosition: {
              x: 320,
              y: 680
            },
            alignment: "center"
          }
        ];
      }
    }

    /* Fecha */
    // fecha = formato.config.contenido.fecha_activo ? formato.fecha : "";
    /* Atentamente */
    for (let x = 0; x < formato.config.contenido.atentamente_espacio; x++) {
      espaciosAtentamente += "\n";
    }
    atentamente = formato.config.contenido.atentamente
      ? {
        text: `${espaciosAtentamente}\nAtentamente,`,
        alignment: formato.config.contenido.atentamente_alineacion
      }
      : "";

    /* Imagen de Soporte */
    if (formato.config.pie.soporte && datos.file) {
      let img = Helpers.appRoot("storage") + `/${datos.file}`;
      img = await b64(img).then(res => {
        return "data:image/png;base64, " + res;
      });
      soporte = [
        {
          text: "ANEXO\n\n\n",
          bold: true,
          alignment: "center",
          pageBreak: "before"
        },
        {
          image: img,
          fit: [400, 400],
          alignment: "center"
        }
      ];
    }
    let imgHeader = await b64(
      Helpers.appRoot("storage") + `/encabezado.jpg`
    ).then(res => {
      return "data:image/jpg;base64, " + res;
    });
    let imgFooter = await b64(Helpers.appRoot("storage") + `/pie.png`).then(
      res => {
        return "data:image/png;base64, " + res;
      }
    );
    /* Estructura para el PDF */
    dd = {
      pageSize: formato.config.general.hoja,
      pageOrientation: formato.config.general.orientacion,
      pageMargins: [
        formato.config.general.m_derecho * 28,
        formato.config.general.m_superior * 28,
        formato.config.general.m_izquierdo * 28,
        formato.config.general.m_inferior * 28
      ],
      header: {
        image: imgHeader,
        width: 595
      },
      content: [
        //imagen,
        {
          text: titulo,
          alignment: formato.config.encabezado.alineacion,
          fontSize: formato.config.encabezado.letra,
          bold: formato.config.encabezado.negrita,
          italics: formato.config.encabezado.cursiva
        },
        espacios,
        {
          alignment: formato.config.contenido.alineacion,
          text: cadena
        },
        {
          text: formato.config.contenido.fecha_activo
            ? `\n\n\n${formato.string_date}`
            : "",
          alignment: formato.config.contenido.fecha_alineacion
        },
        atentamente,
        firma,
        soporte
      ],
      /* [
          "\n",
          {
            text: formato.pie,
            fontSize: formato.config.pie.letra,
            color: "gray",
            alignment: formato.config.pie.alineacion
          }

        ] */
      footer: {
        image: imgFooter,
        width: 595,
        absolutePosition: {
          x: 0,
          y: formato.config.general.m_inferior * 28 + -92.2
        }
      },
      defaultStyle: {
        fontSize: formato.config.general.letra
      }
    };
  // return dd.content
    var loc = `${Helpers.appRoot("storage")}/temp`;
    mkdirp.sync(loc);
    var fileName = `${reporte}-${moment.utc().format('DD-MM-YYYY-mm-ss')}`

    var pdfDoc = printer.createPdfKitDocument(dd);
    var path = `${loc}/${fileName}.pdf`;
    pdfDoc.pipe(fs.createWriteStream(path));
    pdfDoc.end();

    /* let correo = await get("usuarios", {
        conditions: {
          _id: datos.empleado
        }
      });
      if (
        formato.config.general.correo &&
        datos.hasOwnProperty("correo") &&
        datos.correo
      ) {
        ems.push({
          email: correo[0].email,
          subject: titulo,
          isHtml: true,
          content: cadena_email,
          cc: formato.config.general.correo_copia ?
            formato.config.general.correo_enviar : "",
          attachment: path
        });
      }


      setTimeout(async () => {
        if (
          formato.config.general.correo &&
          datos.hasOwnProperty("correo") &&
          datos.correo
        )
          //console.log("send-email: ", await functions("send-mail", ems));
        response.send({
          pdf: dd,
          reporte: `${reporte} - ${datos.last_name} ${datos.first_name}.pdf`,
          path
        });

      }, 1000);*/

    /* setTimeout(async () => {
      const mss = {
        from: 'vera8german@gmail.com',
        to: listEmails,
        subject: 'Hello',
        text: pathPdf,
        attachment: path
      };
      mg.messages().send(mss, function (error, body) {
      });
    }, 2000); */
    // var emails = request._all.emails.toString();

    response.send({
      /* emails: emails, */
      reporte: `${fileName}.pdf`
    })
  }

  async getFile ({ params, request, response }) {
    var quote = {}, event = {}
    const fileName = await fileSearch(params.filename)
    let pathPdf = appSecret + '/api/file/' + `${params.filename}`
    let newPath = `${Helpers.appRoot("storage")}/files/cotizaciones/${params.filename}`

    if (request._all.key) {
      quote = (await Quote.find(request._all.key)).toJSON()
      if (quote) {
        event = (await EmailEvent.findBy('saleAgentId', quote.author)).toJSON()
        if (event.status) {
          if (request._all.sendEmail == 'true' && event) {
            setTimeout(async () => {
              const mss = {
                from: 'crm@wondrix.com',
                to: request._all.emails,
                subject: event.subject,
                html: event.mailContent + '<br> <div style="text-align: center;">' + pathPdf + '</div>',
                attachment: newPath
              };
              mg.messages().send(mss, function (error, body) {
                console.log('error', error, body)
              });
            }, 2000);
          }
        }
      }
    }

    response.download(fileName)
  }
   // Analisis de aguas
  async printWaterAnalysis ({ params, response }) {
    var printer = new PdfPrinter(fonts)
    const waterAnalysis = (await WaterAnalysisMaster.query().with('pools.pool').with('shrimp.zone').with('user.technical_advisor')
    .with('shrimp.customer.contacts').with('applicant').where('id', params.id).first()).toJSON()
    let number = []
    moment.locale('es')
    waterAnalysis.delivery_date = moment(waterAnalysis.delivery_date).format("DD-MMM-YYYY")
    waterAnalysis.analysis_date = moment(waterAnalysis.analysis_date).format("DD-MMM-YYYY")
    let pools = waterAnalysis.pools
    let blankColumns = Array(4 - pools.length).fill('')
    pools = pools.concat(blankColumns)
    number.push({text: 'Piscinas', alignment: 'center', fontSize: 10, bold: true})
    let water_type = []
    let average_weight = []
    let planting_density = []
    let climatology = []
    let alkalinity = []
    let ammonium = []
    let calcium = []
    let dissolved = []
    let magnesium = []
    let nitrate = []
    let nitrite = []
    let oxygen = []
    let ph = []
    let phosphate = []
    let potassium = []
    let salinity = []
    let turbidity = []
    let temperature = []
    let observations = []
    let recommendations = []
    let allRecommendations = []
    let widths = []
    let subtitle1 = []
    let subtitle2 = []
    let subtitle3 = []
    let tables = [] // Dentro iran las validaciones de cada tabla
    let cont = 0
    let band = false
    let recomendate = []
    water_type.unshift({text: 'Tipo de agua', fontSize: 10 })
    average_weight.unshift({text: 'Peso promedio', fontSize: 10 })
    planting_density.unshift({text: 'Densidad de siembra', fontSize: 10 })
    climatology.unshift({text: 'Clima', fontSize: 10 })
    alkalinity.unshift({ text: 'Alcalinidad CO3Ca', fontSize: 10 })
    ammonium.unshift({text: 'Amonio NH4 (mg/l )', fontSize: 10 })
    calcium.unshift({text: 'Calcio ( mg/l )', fontSize: 10 })
    dissolved.unshift({text: 'S.Disueltos T. (mg/Lt)', fontSize: 10 })
    magnesium.unshift({text: 'Magnesio ( mg/l )', fontSize: 10 })
    nitrate.unshift({text: 'Nitrato NO3 ( mg/l )', fontSize: 10 })
    nitrite.unshift({text: 'Nitrito NO2 ( mg/l )', fontSize: 10 })
    oxygen.unshift({text: 'Oxígeno', fontSize: 10 })
    ph.unshift({text: 'pH', fontSize: 10 })
    phosphate.unshift({text: 'Fosfato PO4 ( mg/l )', fontSize: 10 })
    potassium.unshift({text: 'Potasio ( mg/l )', fontSize: 10 })
    salinity.unshift({text: 'Salinidad (ppl)', fontSize: 10 })
    turbidity.unshift({text: 'Turbiedad', fontSize: 10 })
    temperature.unshift({text: 'Temperatura (°C)', fontSize: 10 })

      for (const i in pools) {
        cont = cont + 1
        if (pools[i].climatology && pools[i].climatology == 1) {
          pools[i].climatology = 'Soleado'
        } else if (pools[i].climatology && pools[i].climatology == 2) {
          pools[i].climatology = 'Nublado'
        } else if (pools[i].climatology) {
          pools[i].climatology = pools[i].climatology ? 'Lluvia' : ''
        }
        let waterTypeName = ''
        if (pools[i]) {
          waterTypeName = pools[i].water_type ? 'Dulce' : 'Salada'
        } else {
          waterTypeName = ''
        }
        water_type.push({text: waterTypeName, alignment: 'center', fontSize: 10})
        average_weight.push({text: pools[i].average_weight ? pools[i].average_weight : '-', alignment: 'center', fontSize: 10})
        planting_density.push({ text: pools[i].planting_density ?  numberWithThousandSeparator(pools[i].planting_density) : '-', alignment: 'center', fontSize: 10})
        climatology.push({text: pools[i].climatology ? pools[i].climatology : '-', alignment: 'center', fontSize: 10})
        alkalinity.push({text: pools[i].alkalinity ? parseInt(pools[i].alkalinity) :'-', fillColor: colorAlkalinity (pools[i].water_type, pools[i].alkalinity), alignment: 'center', fontSize: 10})
        ammonium.push({text: pools[i].ammonium ? pools[i].ammonium : '-', fillColor: pools[i].ammonium ? colorAmmonium (pools[i].water_type, pools[i].ammonium) : undefined, alignment: 'center', fontSize: 10})
        calcium.push({text: pools[i].calcium ? pools[i].calcium : '-', fillColor: colorCalcium (pools[i].water_type, pools[i].calcium), alignment: 'center', fontSize: 10})
        dissolved.push({text: pools[i].dissolved ? parseInt(pools[i].dissolved) : '-', fillColor: colorDissolved (pools[i].water_type, pools[i].dissolved), alignment: 'center', fontSize: 10})
        magnesium.push({text: pools[i].magnesium ? pools[i].magnesium : '-', fillColor: colorMagnesium (pools[i].water_type, pools[i].magnesium), alignment: 'center', fontSize: 10})
        nitrate.push({text: pools[i].nitrate ? pools[i].nitrate : '-', fillColor: colorNitrate (pools[i].water_type, pools[i].nitrate), alignment: 'center', fontSize: 10})
        nitrite.push({text: pools[i].nitrite ? pools[i].nitrite : '-', fillColor: colorNitrite (pools[i].water_type, pools[i].nitrite), alignment: 'center', fontSize: 10})
        oxygen.push({text: pools[i].oxygen ? pools[i].oxygen : '-', background: ((pools[i].oxygen >= 4 && pools[i].oxygen <= 8) || pools[i].oxygen == null) ? '' : 'yellow', alignment: 'center', fontSize: 10})
        ph.push({text: pools[i].ph ? pools[i].ph : '-', background: ((pools[i].ph >= 7 && pools[i].ph <= 8.5) || pools[i].ph == null) ? '' : 'yellow', alignment: 'center', fontSize: 10})
        phosphate.push({text: pools[i].phosphate ? pools[i].phosphate : '-', background: colorPhosphate (pools[i].water_type, pools[i].phosphate), alignment: 'center', fontSize: 10})
        potassium.push({text: pools[i].potassium ? pools[i].potassium : '-', background: colorPotassium (pools[i].water_type, pools[i].potassium), alignment: 'center', fontSize: 10})
        salinity.push({text: pools[i].salinity ? parseInt(pools[i].salinity): '-', alignment: 'center', fontSize: 10})
        turbidity.push({text: pools[i].turbidity ? parseInt(pools[i].turbidity) : '-', background: (pools[i].turbidity >= 0 && pools[i].turbidity <= 40 || pools[i].turbidity == null) ? '' : 'yellow', alignment: 'center', fontSize: 10})
        temperature.push({text: pools[i].temperature ? pools[i].temperature : '-', background: ((pools[i].temperature >= 25 && pools[i].temperature <= 32) || pools[i].temperature == null) ? '' : 'yellow', alignment: 'center', fontSize: 10})
        observations.push({text: `${pools[i].observations ? pools[i].observations : ''}\n`, fontSize: 10})
        recomendate.push({text: `${pools[i] ? pools[i].pool.description : ''}: `, bold: true, fontSize: 10})
        allRecommendations.push({text: `${pools[i].recommendations ? pools[i].recommendations : ''}\n`, fontSize: 10})
        recommendations.push('')
        subtitle1.push('')
        subtitle2.push('')
        subtitle3.push('')
        number.push({text: `${pools[i] ? pools[i].pool.description : ''}`, alignment: 'center',bold:true, fontSize: 10})
        if (cont == 4) {
          cont = 0
          // Columnas restantes
          subtitle1.unshift({ text: 'PARÁMETROS FÍSICOS', bold: true , fontSize: 10, colSpan: pools.length + 1})
          subtitle2.unshift({ text: 'PARÁMETROS QUÍMICOS', bold: true , fontSize: 10, colSpan: pools.length + 1})
          subtitle3.unshift({ text: 'Observaciones', bold: true , fontSize: 10})

          //Columnas adicionales//////////////////////////////////////////////////////////////
          water_type.push({text: '', colSpan: 2, alignment: 'center', fontSize: 10})
          average_weight.push({text: '', colSpan: 2, alignment: 'center', fontSize: 10})
          planting_density.push({text: '', colSpan: 2, alignment: 'center', fontSize: 10})
          climatology.push({text: '', colSpan: 2, fontSize: 10})
          alkalinity.push({text: '20 - 60', alignment: 'center', fontSize: 10})
          ammonium.push({text: '0.0 - 1.04', alignment: 'center', fontSize: 10})
          calcium.push({text: '60 -120', alignment: 'center', fontSize: 10})
          dissolved.push({text: '10 - 100', alignment: 'center', fontSize: 10})
          magnesium.push({text: '30 -120', alignment: 'center', fontSize: 10})
          nitrate.push({text: '2.2 - 4.4', alignment: 'center', fontSize: 10})
          nitrite.push({text: '0.003 - 0.33', alignment: 'center', fontSize: 10})
          oxygen.push({text: '4.0 - 8,0', colSpan: 2, alignment: 'center', fontSize: 10})
          ph.push({text: '7,0 - 8.5', colSpan: 2, alignment: 'center', fontSize: 10})
          phosphate.push({text: '0.1 - 1.0', alignment: 'center', fontSize: 10})
          potassium.push({text: '10 - 35', alignment: 'center', fontSize: 10})
          salinity.push({text: '', colSpan: 2, alignment: 'center', fontSize: 10})
          turbidity.push({text: '0 - 40', colSpan: 2, alignment: 'center', fontSize: 10})
          temperature.push({text: '25 - 32', colSpan: 2, alignment: 'center', fontSize: 10})
          observations.push('')
          recommendations.push('')

          alkalinity.push({text: '60 - 250', alignment: 'center', fontSize: 10})
          ammonium.push({text: '0.0 - 0.26', alignment: 'center', fontSize: 10})
          calcium.push({text: '80 - 450', alignment: 'center', fontSize: 10})
          dissolved.push({ text: '10 - 300', alignment: 'center', fontSize: 10})
          magnesium.push({text: '800 - 2500', alignment: 'center', fontSize: 10})
          nitrate.push({text: '1.7 - 3.10', alignment: 'center', fontSize: 10})
          nitrite.push({text: '0.003 - 0.660', alignment: 'center', fontSize: 10})
          oxygen.push('Oxígeno')
          phosphate.push({text: '0.2 - 2.0', alignment: 'center', fontSize: 10})
          potassium.push({text: '30 - 375', alignment: 'center', fontSize: 10})
          salinity.push('')
          observations.push('')
          recommendations.push('')
          // Finales
          number.push({text: '', colSpan: 2, alignment: 'center', fontSize: 10})
          number.push({text: '', alignment: 'center', fontSize: 10})
          water_type.push({text: '', alignment: 'center', fontSize: 10})
          average_weight.push({text: '', alignment: 'center', fontSize: 10})
          planting_density.push({text: '', alignment: 'center', fontSize: 10})
          climatology.push({text: '', alignment: 'center', fontSize: 10})
          ph.push({text: '', alignment: 'center', fontSize: 10})
          temperature.push({text: '', alignment: 'center', fontSize: 10})
          turbidity.push({text: '', alignment: 'center', fontSize: 10})
          subtitle1.push({text: 'A. Dulce', bold: true, alignment: 'center', fontSize: 10})
          subtitle2.push({text: 'A. Dulce', bold: true, alignment: 'center', fontSize: 10})
          subtitle3.push('')
          subtitle1.push({text: 'A. Salada', bold: true, alignment: 'center', fontSize: 10})
          subtitle2.push({text: 'A. Salada', bold: true, alignment: 'center', fontSize: 10})
          subtitle3.push('')
          //Ingresa datos a la tabla
          band = true
          tables.push({
            margin:  [-10, 20, 0,0], // optional
            alignment: 'center',
            table: {
              headerRows: 1,
              widths: [100,'*','*','*','*',65,65],
              body: [
                number,
                water_type,
                average_weight,
                planting_density,
                ['', '','','','',{text: '\nRANGO ÓPTIMO', colSpan: number.length - 5, alignment: 'center', bold: true, fontSize: 11}],
                subtitle1,
                climatology,
                oxygen,
                ph,
                temperature,
                salinity,
                dissolved,
                turbidity,
                ['', '','','','',{text: '\nRANGO ÓPTIMO', colSpan: number.length - 5, alignment: 'center', bold: true, fontSize: 11}],
                subtitle2,
                alkalinity,
                ammonium,
                nitrite,
                nitrate,
                phosphate,
                magnesium,
                calcium,
                potassium
              ]
            }
          })
          number = []
          water_type = []
          average_weight = []
          planting_density = []
          subtitle1 = []
          climatology = []
          oxygen = []
          ph = []
          temperature = []
          salinity = []
          dissolved = []
          turbidity = []
          subtitle2 = []
          alkalinity = []
          ammonium = []
          nitrite = []
          nitrate = []
          phosphate = []
          magnesium = []
          calcium = []
          potassium = []
        }
      }
      console.log(pools.length)
      if (pools.length < 4) {
        // Evalua si hay menos de cuatro registros
          widths.unshift(100)
          pools.forEach(element => {
            widths.push('*')
          })
          widths.push(60,60)

          // Columnas restantes
          subtitle1.unshift({ text: 'PARÁMETROS FÍSICOS', bold: true , fontSize: 10, colSpan: pools.length})
          subtitle2.unshift({ text: 'PARÁMETROS QUÍMICOS', bold: true , fontSize: 10})
          subtitle3.unshift({ text: 'Observaciones', bold: true , fontSize: 10})

          //Columnas adicionales//////////////////////////////////////////////////////////////
          water_type.push({text: '', colSpan:  2, alignment: 'center', fontSize: 10})
          average_weight.push({text: '', colSpan:  2, alignment: 'center', fontSize: 10})
          planting_density.push({text: '', colSpan:  2, alignment: 'center', fontSize: 10})
          climatology.push({text: '', colSpan:  2, fontSize: 10})
          alkalinity.push({text: '20 - 60', alignment: 'center', fontSize: 10})
          ammonium.push({text: '0.0 - 1.04', alignment: 'center', fontSize: 10})
          calcium.push({text: '60 -120', alignment: 'center', fontSize: 10})
          dissolved.push({text: '10 - 100', alignment: 'center', fontSize: 10})
          magnesium.push({text: '30 -120', alignment: 'center', fontSize: 10})
          nitrate.push({text: '2.2 - 4.4', alignment: 'center', fontSize: 10})
          nitrite.push({text: '0.003 - 0.33', alignment: 'center', fontSize: 10})
          oxygen.push({text: '4.0 - 8,0', colSpan:  2, alignment: 'center', fontSize: 10})
          ph.push({text: '7,0 - 8.5', colSpan:  2, alignment: 'center', fontSize: 10})
          phosphate.push({text: '0.1 - 1.0', alignment: 'center', fontSize: 10})
          potassium.push({text: '10 - 35', alignment: 'center', fontSize: 10})
          salinity.push({text: '', colSpan:  2, alignment: 'center', fontSize: 10})
          turbidity.push({text: '0 - 40', colSpan:  2, alignment: 'center', fontSize: 10})
          temperature.push({text: '25 - 32', colSpan:  2, alignment: 'center', fontSize: 10})
          observations.push('')
          recommendations.push('')

          alkalinity.push({text: '60 - 250', alignment: 'center', fontSize: 10})
          ammonium.push({text: '0.0 - 0.26', alignment: 'center', fontSize: 10})
          calcium.push({text: '80 - 450', alignment: 'center', fontSize: 10})
          dissolved.push({ text: '10 - 300', alignment: 'center', fontSize: 10})
          magnesium.push({text: '800 - 2500', alignment: 'center', fontSize: 10})
          nitrate.push({text: '1.7 - 3.10', alignment: 'center', fontSize: 10})
          nitrite.push({text: '0.003 - 0.660', alignment: 'center', fontSize: 10})
          oxygen.push('Oxígeno')
          phosphate.push({text: '0.2 - 2.0', alignment: 'center', fontSize: 10})
          potassium.push({text: '30 - 375', alignment: 'center', fontSize: 10})
          salinity.push('')
          observations.push('')
          recommendations.push('')
          // Finales
          number.push({text: '', colSpan: 2, alignment: 'center', fontSize: 10})
          number.push({text: '', alignment: 'center', fontSize: 10})
          water_type.push({text: '', alignment: 'center', fontSize: 10})
          average_weight.push({text: '', alignment: 'center', fontSize: 10})
          planting_density.push({text: '', alignment: 'center', fontSize: 10})
          climatology.push({text: '', alignment: 'center', fontSize: 10})
          ph.push({text: '', alignment: 'center', fontSize: 10})
          temperature.push({text: '', alignment: 'center', fontSize: 10})
          turbidity.push({text: '', alignment: 'center', fontSize: 10})
          subtitle1.push({text: 'A. Dulce', bold: true, alignment: 'center', fontSize: 10})
          subtitle2.push({text: 'A. Dulce', bold: true, alignment: 'center', fontSize: 10})
          subtitle3.push('')
          subtitle1.push({text: 'A. Salada', bold: true, alignment: 'center', fontSize: 10})
          subtitle2.push({text: 'A. Salada', bold: true, alignment: 'center', fontSize: 10})
          subtitle3.push('')
          let brek = ''
          if (pools.length > 4) {
            brek = 'before'
          }
          tables.push({
            margin:  [-10, 20, 0,0], // optional
            pageBreak: brek,
            alignment: 'center',
            table: {
              headerRows: 1,
              widths,
              body: [
                number,
                water_type,
                average_weight,
                planting_density,
                ['', '','','','',{text: '\nRANGO ÓPTIMO', colSpan: number.length - 5, alignment: 'center', bold: true, fontSize: 11}],
                subtitle1,
                climatology,
                oxygen,
                ph,
                temperature,
                salinity,
                dissolved,
                turbidity,
                ['', '','','','',{text: '\nRANGO ÓPTIMO', colSpan: number.length - 5, alignment: 'center', bold: true, fontSize: 11}],
                subtitle2,
                alkalinity,
                ammonium,
                nitrite,
                nitrate,
                phosphate,
                magnesium,
                calcium,
                potassium
              ]
            }
          })
      } else if (pools.length > 4) {
        widths = []
        widths.unshift(100)
        number.forEach(element => {
          widths.push('*')
        })
        widths.push(60,60)

        if (band == true){
          number.unshift({text: 'Peso promedio', fontSize: 10 })
          water_type.unshift({text: 'Tipo de agua', fontSize: 10 })
          average_weight.unshift({text: 'Peso promedio', fontSize: 10 })
          planting_density.unshift({text: 'Densidad de siembra', fontSize: 10 })
          climatology.unshift({text: 'Clima', fontSize: 10 })
          alkalinity.unshift({ text: 'Alcalinidad CO3Ca', fontSize: 10 })
          ammonium.unshift({text: 'Amonio NH4 (mg/l )', fontSize: 10 })
          calcium.unshift({text: 'Calcio ( mg/l )', fontSize: 10 })
          dissolved.unshift({text: 'S.Disueltos T. (mg/Lt)', fontSize: 10 })
          magnesium.unshift({text: 'Magnesio ( mg/l )', fontSize: 10 })
          nitrate.unshift({text: 'Nitrato NO3 ( mg/l )', fontSize: 10 })
          nitrite.unshift({text: 'Nitrito NO2 ( mg/l )', fontSize: 10 })
          oxygen.unshift({text: 'Oxígeno', fontSize: 10 })
          ph.unshift({text: 'pH', fontSize: 10 })
          phosphate.unshift({text: 'Fosfato PO4 ( mg/l )', fontSize: 10 })
          potassium.unshift({text: 'Potasio ( mg/l )', fontSize: 10 })
          salinity.unshift({text: 'Salinidad (ppl)', fontSize: 10 })
          turbidity.unshift({text: 'Turbiedad', fontSize: 10 })
          temperature.unshift({text: 'Temperatura (°C)', fontSize: 10 })
          observations.unshift('')
        }
        // Evalua si hay menos de cuatro registros

          // Columnas restantes
          let subtitle1 = [{ text: 'PARÁMETROS FÍSICOS', bold: true , fontSize: 10, colSpan: pools.length}]
          subtitle2.unshift({ text: 'PARÁMETROS QUÍMICOS', bold: true , fontSize: 10})
          subtitle3.unshift({ text: 'Observaciones', bold: true , fontSize: 10})

          //Columnas adicionales//////////////////////////////////////////////////////////////
          water_type.push({text: '', colSpan:  2, alignment: 'center', fontSize: 10})
          average_weight.push({text: '', colSpan:  2, alignment: 'center', fontSize: 10})
          planting_density.push({text: '', colSpan:  2, alignment: 'center', fontSize: 10})
          climatology.push({text: '', colSpan:  2, fontSize: 10})
          alkalinity.push({text: '20 - 60', alignment: 'center', fontSize: 10})
          ammonium.push({text: '0.0 - 1.04', alignment: 'center', fontSize: 10})
          calcium.push({text: '60 -120', alignment: 'center', fontSize: 10})
          dissolved.push({text: '10 - 100', alignment: 'center', fontSize: 10})
          magnesium.push({text: '30 -120', alignment: 'center', fontSize: 10})
          nitrate.push({text: '2.2 - 4.4', alignment: 'center', fontSize: 10})
          nitrite.push({text: '0.003 - 0.33', alignment: 'center', fontSize: 10})
          oxygen.push({text: '4.0 - 8,0', colSpan:  2, alignment: 'center', fontSize: 10})
          ph.push({text: '7,0 - 8.5', colSpan:  2, alignment: 'center', fontSize: 10})
          phosphate.push({text: '0.1 - 1.0', alignment: 'center', fontSize: 10})
          potassium.push({text: '10 - 35', alignment: 'center', fontSize: 10})
          salinity.push({text: '', colSpan:  2, alignment: 'center', fontSize: 10})
          turbidity.push({text: '0 - 40', colSpan:  2, alignment: 'center', fontSize: 10})
          temperature.push({text: '25 - 32', colSpan:  2, alignment: 'center', fontSize: 10})
          observations.push('')
          recommendations.push('')

          alkalinity.push({text: '60 - 250', alignment: 'center', fontSize: 10})
          ammonium.push({text: '0.0 - 0.26', alignment: 'center', fontSize: 10})
          calcium.push({text: '80 - 450', alignment: 'center', fontSize: 10})
          dissolved.push({ text: '10 - 300', alignment: 'center', fontSize: 10})
          magnesium.push({text: '800 - 2500', alignment: 'center', fontSize: 10})
          nitrate.push({text: '1.7 - 3.10', alignment: 'center', fontSize: 10})
          nitrite.push({text: '0.003 - 0.660', alignment: 'center', fontSize: 10})
          oxygen.push('Oxígeno')
          phosphate.push({text: '0.2 - 2.0', alignment: 'center', fontSize: 10})
          potassium.push({text: '30 - 375', alignment: 'center', fontSize: 10})
          salinity.push('')
          observations.push('')
          recommendations.push('')
          // Finales
          number.push({text: '', colSpan: 2, alignment: 'center', fontSize: 10})
          number.push({text: '', alignment: 'center', fontSize: 10})
          water_type.push({text: '', alignment: 'center', fontSize: 10})
          average_weight.push({text: '', alignment: 'center', fontSize: 10})
          planting_density.push({text: '', alignment: 'center', fontSize: 10})
          climatology.push({text: '', alignment: 'center', fontSize: 10})
          ph.push({text: '', alignment: 'center', fontSize: 10})
          temperature.push({text: '', alignment: 'center', fontSize: 10})
          turbidity.push({text: '', alignment: 'center', fontSize: 10})
          subtitle1.push({text: 'A. Dulce', bold: true, alignment: 'center', fontSize: 10})
          subtitle2.push({text: 'A. Dulce', bold: true, alignment: 'center', fontSize: 10})
          subtitle3.push('')
          subtitle1.push({text: 'A. Salada', bold: true, alignment: 'center', fontSize: 10})
          subtitle2.push({text: 'A. Salada', bold: true, alignment: 'center', fontSize: 10})
          subtitle3.push('')
          let brek = ''
          if (pools.length > 4) {
            brek = 'before'
          }
          console.log(widths, number.length)
          tables.push({
            margin:  [-10, 20, 0,0], // optional
            pageBreak: brek,
            alignment: 'center',
            table: {
              headerRows: 1,
              widths,
              body: [
                number,
                water_type,
                average_weight,
                planting_density,
                ['', '','','','',{text: '\nRANGO ÓPTIMO', colSpan: number.length - 5, alignment: 'center', bold: true, fontSize: 11}],
                subtitle1,
                climatology,
                oxygen,
                ph,
                temperature,
                salinity,
                dissolved,
                turbidity,
                ['', '','','','',{text: '\nRANGO ÓPTIMO', colSpan: number.length - 5, alignment: 'center', bold: true, fontSize: 11}],
                subtitle2,
                alkalinity,
                ammonium,
                nitrite,
                nitrate,
                phosphate,
                magnesium,
                calcium,
                potassium
              ]
            }
          })

      }
      /* let more = []
      let more2 = []
      for (let i in recomendate) {
        
        // Si hay observaciones insertar more2
      if (observations[i].text != '\n') {
        more2.push(recomendate[i])
        more2.push(observations[i])
      }

      if (allRecommendations[i].text != '\n') {
        more.push(recomendate[i])
        more.push(allRecommendations[i])
      }
      }
      let recomendation = {
        text: []
      }
      let observate = {
        text: []
      }
      recomendation.text = more
      observate.text = more2 */
    const observate = waterAnalysis.observations
    const recomendation = waterAnalysis.recommendations
      recommendations.unshift(allRecommendations)

    let title ='LABORATORIO'
    let subtitle ='ANÁLISIS FISICO Y QUIMICO DEL AGUA'
    let name = 'Asesor Técnico:'
    this.img = {
      image: 'logo.jpeg',
      fit: [70, 70],
      absolutePosition: {
        'x': 35,
        'y': 35
      }
    },
    this.img2 = {
      image: 'logo.jpeg',
      fit: [70, 70],
      absolutePosition: {
        'x': 20,
        'y': 10
      }
    }
    var docDefinition = {
      pageSize: 'A4',
      pageMargins: [60, 120, 40, 90],
      header: [
        {
          margin: [20, 40, 20, 20],
          alignment: 'center',
          text: [
            {
              bold: true,
              text: `${title}\n`,
              fontSize: 14
            },
            {
              text: `${subtitle}\n`,
              fontSize: 12
            }
          ]
        },
        {
          alignment: 'center',
          text: [
            { text: 'CAMARONERA: ', bold: true, fontSize: 10},
            { text: waterAnalysis.shrimp.name ? waterAnalysis.shrimp.name : '-', fontSize: 10},
            { text: '             '},
            { text: 'SOLICITANTE: ', bold: true, fontSize: 10},
            { text: waterAnalysis.applicant ? `${waterAnalysis.applicant.name} ${waterAnalysis.applicant.last_name}` : '', fontSize: 10}
          ]
        },
        {
          alignment: 'center',
          text: [
            { text: 'ZONA: ', bold: true, fontSize: 10, margin:[0,0,50,0]},
            { text: waterAnalysis.shrimp.zone.name ? waterAnalysis.shrimp.zone.name : '-' , fontSize: 10},
            { text: '                       '},
            { text: 'F.ANÁLISIS: ', bold: true, fontSize: 10},
            { text: waterAnalysis.analysis_date ? waterAnalysis.analysis_date : '-' , fontSize: 10},
            { text: '                       '},
            { text: 'F.ENTREGA: ', bold: true, fontSize: 10},
            { text: waterAnalysis.delivery_date ? waterAnalysis.delivery_date : '-' , fontSize: 10}
          ]
        },
        this.img
      ],
      content: [
        ...tables,
        {
          pageBreak: 'before',
          alignment: 'left',
          bold: true,
          text: [
            { text: '\n\nOBSERVACIONES:\n\n', fontSize: 10}
          ]
        },
        observate,
        {
          alignment: 'left',
          bold: true,
          text: [
            { text: '\n\RECOMENDACIONES:\n\n', fontSize: 10}
          ]
        },
        recomendation,
      ],
      footer: {
        columns: [
          this.img2,
          {text: 'Dpto. Técnico\n', bold: true, alignment: 'left', fontSize: 12, margin: [-25, 13, 0, 0] },
          {text: 'División Acuacultura\n', alignment: 'left', fontSize: 10, margin: [-144, 25, 0, 0]},
          {text: 'Guayaquil - Ecuador', bold: true, alignment: 'left', fontSize: 12, margin: [-263, 37, 0, 0] },
          {text: `_______________________\n${name}\n${waterAnalysis.user.technical_advisor.full_name ? waterAnalysis.user.technical_advisor.full_name : ''}\n`,bold:true, alignment: 'center', fontSize: 10, margin: [-70, 15, 0, 0] },
        ]
      }
    }

    var options = {
      // ...
    }
    // return docDefinition
    var pdfDoc = printer.createPdfKitDocument(docDefinition, options)
    const dir = Helpers.appRoot('storage/uploads/analysis')
    const filename = `Analisis_de_aguas_${waterAnalysis.id}.pdf`
    if (!fs.existsSync(dir)){
      fs.mkdirSync(dir);
    }
    pdfDoc.pipe(fs.createWriteStream(`${dir}/${filename}`));
    pdfDoc.end()


    let to = 'elyohan14@gmail.com'
    if (waterAnalysis.applicant_id === 0) { // si el solicitante es el mismo cliente hay que buscar correo del cliente
      to = waterAnalysis.shrimp.customer ? waterAnalysis.shrimp.customer.electronic_documents_email : ''
    } else {
      let find = (waterAnalysis.shrimp.customer.contacts.find(val => val.id === waterAnalysis.applicant_id))
      to = find ? find.email : 'elyohan14@gmail.com'
    }

    response.send(`analysis-Analisis_de_aguas_${waterAnalysis.id}`)
  }
  // Analisis de suelos
  async printFloorAnalysis ({ params, response }) {
    var printer = new PdfPrinter(fonts)
    const floorAnalysis = (await FloorAnalysisMaster.query().with('pools.pool').with('shrimp.zone').with('user.technical_advisor')
    .with('shrimp.customer.contacts').with('applicant').where('id', params.id).first()).toJSON()
    moment.locale('es')
    floorAnalysis.delivery_date = moment(floorAnalysis.delivery_date).format("DD-MMM-YYYY")
    floorAnalysis.analysis_date = moment(floorAnalysis.analysis_date).format("DD-MMM-YYYY")
    let pools = floorAnalysis.pools
    pools.unshift({ text: 'Piscinas', bold: true })
    let blankColumns = Array(5 - pools.length).fill('')
    pools = pools.concat(blankColumns)

    let ph = []
    let organic_material = []
    let redox_potential = []
    let sulfides = []
    let iron = []
    const observations = []
    const recommendations = []
    const allRecommendations = []
    const tables = []
    let widths = []
    let subtitle1 = []
    let cont = 0
    let recomendate = []

    if (floorAnalysis.pools.length > 0) { // Si el análisis tiene piscinas


      for (let i in pools) {
      cont = cont + 1

      if (i == 0) { // Si es la primera columna. Columna vacía insertada antes de las piscinas
        subtitle1.push({ text: 'PARÁMETROS FÍSICOS', bold: true , fontSize: 10})
        widths.push(60)
        } else {
          subtitle1.push({text: `${pools[i] ? pools[i].pool.description : ''}`, bold: true, alignment: 'center', fontSize: 10})
          widths.push('*')
          ph.push({text: pools[i].ph, background: (pools[i].ph && pools[i].ph >= 7.5 && pools[i].ph <= 8.5) ? '' : 'yellow', alignment: 'center', fontSize: 10})
          organic_material.push({text: pools[i].organic_material, background: (pools[i].organic_material && pools[i].organic_material >= 2 && pools[i].organic_material <= 3.5) ? '' : 'yellow', alignment: 'center', fontSize: 10})
          redox_potential.push({text: pools[i].redox_potential, background: (pools[i].redox_potential && pools[i].redox_potential > 30) ? '' : 'yellow', alignment: 'center', fontSize: 10})
          sulfides.push({text: pools[i].sulfides, background: (pools[i].sulfides && pools[i].sulfides < 0.1) ? '' : 'yellow', alignment: 'center', fontSize: 10})
          iron.push({text: pools[i].iron, background: (pools[i].iron && pools[i].iron < 0.3) ? '' : 'yellow', alignment: 'center', fontSize: 10})
          if (pools[i].pool) {
            observations.push({text: `${pools[i].observations ? pools[i].observations : 'Sin observaciones'}\n`, fontSize: 10})
            allRecommendations.push({text: `${pools[i].recommendations ? pools[i].recommendations : 'Sin recomendaciones'}\n`, fontSize: 10})
            recomendate.push({text: `${pools[i] ? pools[i].pool.description : ''}: `, bold: true, fontSize: 10})
          recommendations.push('')
          }
          if (cont == 5) {
          cont = 0


          ph.unshift({text: 'pH', fontSize: 10})
          organic_material.unshift({text: 'Materia orgánica ( % )', fontSize: 10})
          redox_potential.unshift({text: 'Potencial Redox(mv)', fontSize: 10})
          sulfides.unshift({text: 'Sulfuros SH2(mg/L)', fontSize: 10})
          iron.unshift({text: 'Hierro(mg/L)', fontSize: 10})

          //Columnas adicionales//////////////////////////////////////////////////////////////
          ph.push({text: '7.5 - 8.5', alignment: 'center', fontSize: 10})
          organic_material.push({text: '2.0 - 3.5', alignment: 'center', fontSize: 10})
          redox_potential.push({text: '>30', alignment: 'center', fontSize: 10})
          sulfides.push({text: '<0.1', alignment: 'center', fontSize: 10})
          iron.push({text: '<0.3', alignment: 'center', fontSize: 10})
          //////////////////////////////////////////////////////////////////////////7

          subtitle1.push({text: 'RANGO ÓPTIMO', bold: true, fontSize: 10})
          // Trabaja en las tablas
          const f1 = subtitle1
          const f2 = ph
          const f3 = organic_material
           tables.push({
                alignment: 'center',
                margin:  [-10, 20, 0,0],
                table: {
                  widths: ['*', '*', '*', '*', '*', '*'], // jj
                  style: 'tableExample',
                  body: [
                    f1,
                    f2,
                    f3,
                    redox_potential,
                    sulfides,
                    iron
                  ]
                }
              })
              subtitle1 = []
              ph = []
              organic_material = []
              redox_potential = []
              sulfides = []
              iron = []
              subtitle1.push({ text: 'PARÁMETROS FÍSICOS', bold: true , fontSize: 10})
          }
        }
      }
      widths = []
      let band = false
      if (subtitle1.length === 1) { // Si no hay piscinas
        band = true
        widths = ['*', '*', '*']
        observations.push({text: 'No se seleccionó piscina', fontSize: 10})
        allRecommendations.push({text: 'No se seleccionó piscina', fontSize: 10})
        recommendations.push('')

        ph.unshift({text: 'pH', fontSize: 10})
        ph.push({text: '-', fontSize: 10})
        organic_material.unshift({text: 'Materia orgánica ( % )', fontSize: 10})
        organic_material.push({text: '-', fontSize: 10})

        //Columnas adicionales//////////////////////////////////////////////////////////////
        ph.push({text: '7.5 - 8.5', alignment: 'center', fontSize: 10})
        organic_material.push({text: '2.0 - 3.5', alignment: 'center', fontSize: 10})
        //////////////////////////////////////////////////////////////////////////7

        subtitle1.push({text: 'Sin piscina', bold: true, fontSize: 10})
        subtitle1.push({text: 'RANGO ÓPTIMO', bold: true, fontSize: 10})
      } else if (subtitle1.length === 2) { // Si hay una sola piscina
        widths = [85, '*']
        band = true
        subtitle1.push({text: 'RANGO ÓPTIMO', bold: true, fontSize: 10})

        organic_material.unshift({text: 'Materia orgánica ( % )', fontSize: 10})
        organic_material.push({text: '2.0 - 3.5', alignment: 'center', fontSize: 10})

        ph.unshift({text: 'pH', fontSize: 10})
        ph.push({text: '7.5 - 8.5', alignment: 'center', fontSize: 10})
      } else if (subtitle1.length === 3) { // Si hay 2 piscinas
        widths = [85, '*', '*']
        subtitle1.push({text: 'RANGO ÓPTIMO', bold: true, fontSize: 10})

        organic_material.unshift({text: 'Materia orgánica ( % )', fontSize: 10})
        organic_material.push({text: '2.0 - 3.5', alignment: 'center', fontSize: 10})

        ph.unshift({text: 'pH', fontSize: 10})
        ph.push({text: '7.5 - 8.5', alignment: 'center', fontSize: 10})
      } else if (subtitle1.length === 4) { // Si hay 3 piscinas
        widths = [85, '*', '*', '*']
        subtitle1.push({text: 'RANGO ÓPTIMO', bold: true, fontSize: 10})

        organic_material.unshift({text: 'Materia orgánica ( % )', fontSize: 10})
        organic_material.push({text: '2.0 - 3.5', alignment: 'center', fontSize: 10})

        ph.unshift({text: 'pH', fontSize: 10})
        ph.push({text: '7.5 - 8.5', alignment: 'center', fontSize: 10})
      } else if (subtitle1.length === 5) { // Si hay 4 piscinas
        band = true
        widths = [85, '*', '*', '*', '*']
        subtitle1.push({text: 'RANGO ÓPTIMO', bold: true, fontSize: 10})

        organic_material.unshift({text: 'Materia orgánica ( % )', fontSize: 10})
        organic_material.push({text: '2.0 - 3.5', alignment: 'center', fontSize: 10})

        ph.unshift({text: 'pH', fontSize: 10})
        ph.push({text: '7.5 - 8.5', alignment: 'center', fontSize: 10})
      }
      /////////////////////////////// Lo añadí yo
      // Insertar tabla
      tables.push({
        alignment: 'center',
        pageBreak: 'before',
        margin:  [-10, 20, 0,0],
        table: {
          // headers are automatically repeated if the table spans over multiple pages
          // you can declare how many rows should be treated as headers
          widths,
          style: 'tableExample',
          body: [
            subtitle1,
            ph,
            organic_material
          ]
        }
      })
      ///////////////////////////////////////////      
      if ((band === true)) {
        tables[0].pageBreak = ''
      }

      if (pools.length < 6) { // Si hay menos de 5 piscinas
        if (((widths.length <= 4) && (widths.length >2))) {
          tables[0].pageBreak = ''
          delete tables[1]
          delete observations[4]
          delete allRecommendations[4]
        }
      }
    }
    /* let more = []
    let more2 = []
    for (let i in recomendate) {
      more.push(recomendate[i])
      more2.push(recomendate[i])
      more2.push(observations[i])
      more.push(allRecommendations[i])
    }
    let recomendation = {
      text: []
    }
    let observate = {
      text: []
    }
    recomendation.text = more
    observate.text = more2 */
    const observate = floorAnalysis.observations
    const recomendation = floorAnalysis.recommendations
    //console.log(recomendation, observate)
    recommendations.unshift(allRecommendations)


    let title ='LABORATORIO'
    let subtitle ='ANÁLISIS FISICO Y QUIMICO DEL SUELO'
    let name = 'Asesor Técnico:'
    this.img = {
      image: 'logo.jpeg',
      fit: [70, 70],
      absolutePosition: {
        'x': 35,
        'y': 35
      }
    },
    this.img2 = {
      image: 'logo.jpeg',
      fit: [70, 70],
      absolutePosition: {
        'x': 20,
        'y': 10
      }
    },
    ///////////////borrar
    widths.push(45)
    //////////////////////
    var docDefinition = {
      pageSize: 'A4',
      pageMargins: [60, 120, 40, 90],
      header: [
        {
          margin: [20, 40, 20, 20],
          alignment: 'center',
          text: [
            {
              bold: true,
              text: `${title}\n`,
              fontSize: 14
            },
            {
              text: `${subtitle}\n`,
              fontSize: 12
            }
          ]
        },
        {
          alignment: 'center',
          text: [
            { text: 'CAMARONERA: ', bold: true, fontSize: 10},
            { text: floorAnalysis.shrimp.name ? floorAnalysis.shrimp.name : '-', fontSize: 10},
            { text: '             '},
            { text: 'SOLICITANTE: ', bold: true, fontSize: 10},
            { text: floorAnalysis.applicant ? `${floorAnalysis.applicant.name} ${floorAnalysis.applicant.last_name}` : '', fontSize: 10},
            { text: '             '},
            { text: 'Zona: ', bold: true, fontSize: 10},
            { text: floorAnalysis.shrimp.zone.name ? floorAnalysis.shrimp.zone.name : '-' , fontSize: 10},
          ]
        },
        {
          alignment: 'center',
          text: [
            /* { text: 'ZONA: ', bold: true, fontSize: 10, margin:[0,0,50,0]},
            { text: floorAnalysis.shrimp.zone.name ? floorAnalysis.shrimp.zone.name : '-' , fontSize: 10},
            { text: '                       '}, */
            { text: 'F.ANÁLISIS: ', bold: true, fontSize: 10},
            { text: floorAnalysis.analysis_date ? floorAnalysis.analysis_date : '-' , fontSize: 10},
            { text: '                       '},
            { text: 'F.ENTREGA: ', bold: true, fontSize: 10},
            { text: floorAnalysis.delivery_date ? floorAnalysis.delivery_date : '-' , fontSize: 10},
            { text: '                       '},
            { text: 'F.RECEPCIÓN: ', bold: true, fontSize: 10},
            { text: floorAnalysis.sampling_reception_date ? floorAnalysis.sampling_reception_date : '-' , fontSize: 10}
          ]
        },
        this.img
      ],
      content: [
        /* {
          columns: [
            {
              width: 'auto',
              image: 'logo.jpeg',
              width: 40
            },
            {
              stack: [
                // second column consists of paragraphs
                'Laboratorio',
                'Análisis de suelos'
              ],
              fontSize: 15,
              style: { alignment: 'center' }
            }

          ]
        }, */
        ...tables,
        {
          // pageBreak: 'before',
          alignment: 'left',
          bold: true,
          text: [
            { text: '\n\nOBSERVACIONES:\n\n', fontSize: 10}
          ]
        },
        observate,
        {
          alignment: 'left',
          bold: true,
          text: [
            { text: '\n\RECOMENDACIONES:\n\n', fontSize: 10}
          ]
        },
        recomendation
      ],
      footer: {
        columns: [
          this.img2,
          {text: 'Dpto. Técnico\n', bold: true, alignment: 'left', fontSize: 12, margin: [-30, 13, 0, 0] },
          {text: 'División Acuacultura\n', alignment: 'left', fontSize: 10, margin: [-150, 25, 0, 0]},
          {text: 'Guayaquil - Ecuador', bold: true, alignment: 'left', fontSize: 12, margin: [-269, 36, 0, 0] },
          {text: `_______________________\n${name}\n${floorAnalysis.user.technical_advisor.full_name ? floorAnalysis.user.technical_advisor.full_name : ''}\n`,bold:true, alignment: 'center', fontSize: 10, margin: [-70, 17, 0, 0] },
        ]
      }
    }

    var options = {
      // ...
    }
    //return docDefinition
    var pdfDoc = printer.createPdfKitDocument(docDefinition, options)
    const dir = Helpers.appRoot('storage/uploads/flooranalysis')
    const filename = `Analisis_de_suelos_${floorAnalysis.id}.pdf`
    if (!fs.existsSync(dir)){
      fs.mkdirSync(dir);
    }
    pdfDoc.pipe(fs.createWriteStream(`${dir}/${filename}`));
    pdfDoc.end()


    let to = 'elyohan14@gmail.com'
    if (floorAnalysis.applicant_id === 0) { // si el solicitante es el mismo cliente hay que buscar correo del cliente
      to = floorAnalysis.shrimp.customer ? floorAnalysis.shrimp.customer.electronic_documents_email : ''
    } else {
      let find = (floorAnalysis.shrimp.customer.contacts.find(val => val.id === floorAnalysis.applicant_id))
      to = find ? find.email : 'elyohan14@gmail.com'
    }
    // return to
    console.log('to', to)
    /* Email.send({
      to,
      subject: 'Análisis',
      content: 'Saludos',
      attachments: [
        {
          filename,
          path: `${dir}/${filename}`
        }
      ]
    }) */
    response.send(`flooranalysis-Analisis_de_suelos_${floorAnalysis.id}`)

  }
  async printProjectionsFood ({ params, response}) {
    var printer = new PdfPrinter(fonts)
    const Projection = (await Projections.query().with('details').with('pool').with('technicalAdvisor').with('technicalChief')
    .with("details", (builder) => {
      builder.orderBy("id");
    })
    .where('id', params.id).first()).toJSON()
    // return Projection
    const shrimp = (await Shrimp.query().with('customer').where('id', Projection.pool.shrimp_id).first()).toJSON()
    Projection.seedtime = moment(Projection.seedtime).format("DD-MM-YYYY")
    if (Projection.initial_biomass) {
      Projection.initial_biomass = 'Con biomasa inicial'
    } else {
      Projection.initial_biomass = 'Sin biomasa inicial'
    }
    const file1 = []
    const file2 = []
    const file3 = []
    const file4 = []
    const heads = []
    const subtitles = []
    // Siembra
    file1.push({text: `Fecha de Siembra:\n${Projection.seedtime}`, fontSize: 10})
    file1.push({text: `Días de Cultivo:\n${Projection.cultivation_days}`, fontSize: 10})
    file1.push({text: `Densidad:\n${Projection.density}`, fontSize: 10})
    file1.push({text: `Tipo de Siembra:\n${Projection.initial_biomass}`, fontSize: 10})
    file1.push({text: `Peso de Siembra:\n${FoodProjectionFunctions.plantingWeigth(Projection.harvest_target_weight, Projection.cultivation_days, Projection.stock_weight, true)}`, fontSize: 10})
    file2.push({text: `Peso objetivo de cosecha:\n${Projection.harvest_target_weight}`, fontSize: 10})
    file2.push({text: `Supervivencia (Primeros 30 días):\n${Projection.survival}`, fontSize: 10})
    file2.push({text: `Supervivencia (despues de 30 días):\n${Projection.survival_after30days}`, fontSize: 10})
    file2.push({text: `Tasa de alimentación:\n${Projection.feeding_rate}`, fontSize: 10})
    file2.push({text: ''})
    // Segunda tabla
    heads.push({text: 'Día', fontSize: 10, rowSpan: 2})
    heads.push({text: 'Alimento/Día(Kg)', fontSize: 10, colSpan: 2})
    heads.push({text: '', fontSize: 10, rowSpan: 2})
    heads.push({text: 'Tasa de biomasa (%)', fontSize: 10, rowSpan: 2})
    heads.push({text: 'Peso vivo (%)', fontSize: 10, rowSpan: 2})
    heads.push({text: 'Tipo de alimento', fontSize: 10, rowSpan: 2})
    heads.push({text: 'Sobrevivencia (%)', fontSize: 10, rowSpan: 2})
    subtitles.push({text: ''})
    subtitles.push({text: 'Proyect.',fontSize:10})
    subtitles.push({text: 'Reales.',fontSize:10})
    subtitles.push({text: ''})
    subtitles.push({text: ''})
    subtitles.push({text: ''})
    subtitles.push({text: ''})
    let titleTab = []
    let titleTab2 = []
    let information = []
    information.push([{text: '\nDía', fontSize: 10, alignment: 'center'},{text: 'Alimento/Día(Kg)\n\nProyect. Reales' ,fontSize: 10, colSpan: 2, alignment: 'center'}, {text: ''},{text: 'Tasa de Biomasa\n\nProyect. Reales' ,fontSize: 10, colSpan: 2, alignment: 'center'}, {text: ''},{text: 'Peso vivo (Kg)  .\n\nProyect. Reales' ,fontSize: 10, colSpan: 2, alignment: 'center'}, {text: ''},{text: '\nTipo de Alimento', alignment: 'center', fontSize: 10},{text: 'Supervivencia (%)\n\nProyect. Reales' ,fontSize: 10, colSpan: 2, alignment: 'center'}, {text: ''}])

    let detal = (await Details.query().where({'food_projection_master_id': Projection.id}).orderBy('day').fetch()).toJSON()
    for (let i in detal) {
      information.push([{text: `${detal[i].day}`, fontSize: 10, alignment: 'center'},{text: `${parseInt(detal[i].food_day)}`, fontSize: 10, alignment: 'center'},{text: `${detal[i].real_food_day ? parseInt(detal[i].real_food_day) : '.       .'}`, fontSize: 10, alignment: 'center', color: 'green'},{text: `${detal[i].biomass_rate}`, fontSize: 10, alignment: 'center'},{text: `${detal[i].real_biomass_rate ? detal[i].real_biomass_rate : '.       .'}`, fontSize: 10, alignment: 'center', color: 'green'},{text: `${detal[i].liveweight}`, fontSize: 10, alignment: 'center'},{text: `${detal[i].real_liveweight ? detal[i].real_liveweight : '.       .'}`, fontSize: 10, alignment: 'center', color: 'green'},{text: `${detal[i].food_type}`, fontSize: 10, alignment: 'center'},{text: `${detal[i].survival}`, fontSize: 10, alignment: 'center'},{text: `${detal[i].real_survival ? detal[i].real_survival : '.       .'}`, fontSize: 10, alignment: 'center', color: 'green'}])
    }
    // Cosecha
    if (Projection.initial_biomass === 'Sin biomasa inicial') {
      Projection.initial_biomass = false
    } else {
      Projection.initial_biomass = true
    }

    const projectionCalculatedData = FoodProjectionFunctions.calculateFC (Projection.details, Projection.density, Projection.pool.area, Projection.cultivation_days, Projection.harvest_target_weight)
    const animals_number =  (Projection.density * parseFloat(Projection.pool_area)).toFixed()
    const weeklyGrowth = (Projection.initial_biomass ? (parseFloat(Projection.harvest_target_weight) - parseFloat(Projection.stock_weight)) / Projection.cultivation_days * 7 : (parseFloat(Projection.harvest_target_weight) / Projection.cultivation_days) * 7).toFixed(2)
    //const poundsHarvest = (animals_number * (parseFloat(Projection.survival) / 100) * parseFloat(Projection.harvest_target_weight) / 454).toFixed()

    // const kilosFood = (Projection.initial_biomass ? parseFloat(Projection.fc) * ((poundsHarvest / 2.2046) - (animals_number * parseFloat(Projection.stock_weight) / 1000)) : poundsHarvest * parseFloat(Projection.fc)).toFixed()
    const kilosFood = projectionCalculatedData.kilosFood
    const poundsHarvest = projectionCalculatedData.poundsHarvest
    const poundsHa = (poundsHarvest / parseFloat(Projection.pool_area)).toFixed()
    const harvestDensity = (poundsHarvest * 454 / parseFloat(Projection.harvest_target_weight) / parseFloat(Projection.pool_area)).toFixed()
    const harvest = moment(Projection.seedtime, 'DD-MM-YYYY').add((Projection.cultivation_days), 'days').format('DD-MM-YYYY')

    const initialBiomassMessage = Projection.initial_biomass ? 'con' : 'sin'

    file3.push({text: `Fecha de cosecha:\n ${harvest}` ,fontSize: 10})
    file3.push({text: `Número de animales:\n ${animals_number ? animals_number : ''}`, fontSize: 10})
    file3.push({text: `Crecimiento semanal (${initialBiomassMessage} biomasa inicial):\n ${weeklyGrowth > 0 ? weeklyGrowth : ''}`, fontSize: 10})
    file3.push({text: `Kg Alimento (${initialBiomassMessage} biomasa inicial):\n ${kilosFood > 0 ? kilosFood : ''}` , fontSize: 10})
    file3.push({text: `Libras a cosechar:\n ${poundsHarvest > 0 ? poundsHarvest : ''}`, fontSize: 10})
    file4.push({text: `Libras/Ha:\n ${poundsHa > 0 ? poundsHa : ''}` , fontSize: 10})
    file4.push({text: `Densidad de cosecha:\n ${harvestDensity > 0 ? harvestDensity : ''}`, fontSize: 10})
    file4.push({text: `FC:\n${projectionCalculatedData.fc}`, fontSize: 10})
    file4.push({text: '', colSpan: 2})
    let title ='LABORATORIO'
    let subtitle ='PROYECCIÒN DE ALIMENTOS'
    let name = 'Asesor Técnico:'
    this.img = {
      image: 'logo.jpeg',
      fit: [70, 70],
      absolutePosition: {
        'x': 35,
        'y': 35
      }
    },
    this.img2 = {
      image: 'logo.jpeg',
      fit: [70, 70],
      absolutePosition: {
        'x': 20,
        'y': 10
      }
    }
    const chart =Projection.chart_image ? {
      image: Projection.chart_image,
      pageBreak: 'before',
      width: 446,
      height: 186,
      alignment : 'left',
      margin: [0,0]
    } : {}
    var docDefinition = {
      pageSize: 'A4',
      fontSize: 10,
      pageMargins: [60, 120, 40, 90],
      header: [
        {
          margin: [20, 40, 20, 20],
          alignment: 'center',
          text: [
            {
              bold: true,
              text: `${title}\n`,
              fontSize: 14
            },
            {
              text: `${subtitle}\n`,
              fontSize: 12
            }
          ]
        },
        this.img
      ],
      content: [
        {
          margin: [0,0,0,15],
          alignment: 'left',
           text: [
            { text: 'CLIENTE: ', bold: true, fontSize: 10},
            { text: shrimp.customer.legal_name ? shrimp.customer.legal_name : '', fontSize: 10}
          ]
        },
        {
          columns: [
            { text: 'CAMARONERA: ', bold: true, alignment: 'left' , fontSize: 10},
            { text: shrimp.name , fontSize: 10},
            { text: 'ÁREA: ', bold: true, alignment: 'center' , fontSize: 10},
            { text: Projection.pool.area, fontSize: 10},
            { text: 'PISCINA: ', bold: true, alignment: 'right' , fontSize: 10},
            { text: Projection.pool.description , fontSize: 10}
          ]
        },
        /* {
          columns: [
            {
              width: 'auto',
              image: 'logo.jpeg',
              width: 40
            },
            {
              stack: [
                // second column consists of paragraphs
                'Laboratorio',
                'Análisis de suelos'
              ],
              fontSize: 15,
              style: { alignment: 'center' }
            }

          ]
        }, */
        { // optional
          /* margin:  [-10, 20, 0,0],
          table: {
            // headers are automatically repeated if the table spans over multiple pages
            // you can declare how many rows should be treated as headers
            headerRows: 1,
            widths,
            style: 'tableExample',
            body: [
              subtitle1,
              ph,
              organic_material/*,
              observations,
              recommendations
            ]
          } */
        },
        {
          // pageBreak: 'before',
          alignment: 'left',
          bold: true,
          text: [
            { text: '\n\nSIEMBRA\n\n', fontSize: 10}
          ]
        },
        {
          alignment: 'center',
          table: {
            // headers are automatically repeated if the table spans over multiple pages
            // you can declare how many rows should be treated as headers
            headerRows: 2,
            width: ['*','*','*','*', '*'],
            body: [
              file1,
              file2,
            ]
          }
        },
        {
          alignment: 'left',
          bold: true,
          text: [
            { text: '\n\nCOSECHA\n\n', fontSize: 10}
          ]
        },
        {
          alignment: 'center',
          table: {
            // headers are automatically repeated if the table spans over multiple pages
            // you can declare how many rows should be treated as headers
            headerRows: 2,
            width: ['*','*','*','*', '*'],
            body: [
              file3,
              file4
            ]
          }
        },
        {
          alignment: 'left',
          bold: true,
          text: [
            { text: '  ', fontSize: 10}
          ],
          pageBreak: 'before'
        },
        {
          alignment: 'center',
        margin: [10,0,0,0],
         table: {
            // headers are automatically repeated if the table spans over multiple pages
            // you can declare how many rows should be treated as headers
            width: [40,40,40,40,40,40,40,40,40,40],
            headerRows: 1,
            body: information
          }
        },
        chart
      ],
      footer: {
        columns: [
          this.img2,
          {text: 'Dpto. Técnico\n', bold: true, alignment: 'left', fontSize: 12, margin: [-25, 13, 0, 0] },
          {text: 'División Acuacultura\n', alignment: 'left', fontSize: 10, margin: [-144, 25, 0, 0]},
          {text: 'Guayaquil - Ecuador', bold: true, alignment: 'left', fontSize: 12, margin: [-263, 60, 0, 0] },
          {text: `${name}\n${Projection.technicalAdvisor ? Projection.technicalAdvisor.full_name : Projection.technicalChief.full_name}\n`,bold:true, alignment: 'left', fontSize: 10, margin: [5, 30, 0, 0] },
        ]
      },
      style: {
        fontss: {
          fontSize: 8,
          alignment: 'center'
        }
      }
    }

    var options = {
      // ...
    }
    //return docDefinition
    var pdfDoc = printer.createPdfKitDocument(docDefinition, options)
    const dir = Helpers.appRoot('storage/uploads/')
    const filename = `Proyección_Alimentos_${Projection.id}.pdf`
    if (!fs.existsSync(dir)){
      fs.mkdirSync(dir);
    }
    pdfDoc.pipe(fs.createWriteStream(`${dir}/${filename}`));
    pdfDoc.end()

    response.send(`Proyección_Alimentos_${Projection.id}`)
  }

  async printFishing ({ params, response}) {
    var printer = new PdfPrinter(fonts)
    const fishingProjection = (await FishingProjection.query().with('growthReports').with('shrimpProjection')
    .with('pool').with('costProjection').with('technicalAdvisor').where('id', params.id)
    .with('growthReports', (query) => {
      query.orderBy('id')
    })
    .first()).toJSON()

    fishingProjection.type = typeOptions.find(val => val.id === fishingProjection.type).name
    let subtitle1 = []
    let subtitle2 = []
    subtitle1.push({text: '\nFECHA', fontSize: 10, rowSpan: 2},{text: '\nTAMAÑO', fontSize: 10, rowSpan: 2},{text: '\nCRECIMIENTO', fontSize: 10, rowSpan: 2},{text: '\nCRECIMIENO GENERAL', fontSize: 10, rowSpan: 2},{text: 'MUESTREO POBLACIONAL', fontSize: 10, colSpan: 5, alignment: 'center'}, {text: ''}, {text: ''}, {text: ''}, {text: ''})
    subtitle2.push({text: ''}, {text: ''}, {text: ''}, {text: ''}, {text: '%', fontSize: 8, margin:[50,0,-40,0]}, {text: 'ESTIMADO', fontSize: 8, alignment: 'center', margin:[40,0,-30,0]}, {text: 'MUERTO ROJO', fontSize: 8, alignment: 'center', margin:[40,0,-30,0]}, {text: 'DISTRIBUCIÒN', fontSize: 8, alignment: 'center', margin:[40,0,-30,0]}, {text: 'MUERTO FRESCO', fontSize: 8, alignment: 'center', margin:[40,0,-30,0]})
    const density = fishingProjection.qty ? (fishingProjection.qty / fishingProjection.pool.area).toFixed() : 0
    const InitialBiomas = fishingProjection.qty && fishingProjection.sowing_weight ? ((fishingProjection.qty * fishingProjection.sowing_weight) / 454).toFixed() : 0
    let date = []
    console.log(fishingProjection.growthReports[0].date)
    console.log(fishingProjection.seedtime)
    const date1 = moment(fishingProjection.growthReports[fishingProjection.growthReports.length - 1].date)
    const date2 =  moment(fishingProjection.seedtime)
    // console.log(date1, date2)
    const age =  date1.diff(date2, 'days') > 0 ? date1.diff(date2, 'days') : 0
    fishingProjection.seedtime = moment(fishingProjection.seedtime).format('YYYY-MM-DD')

    // Peso de siembra
    const sowing_weight = fishingProjection.sowing_weight

    // Hectareas de la piscina
    const poolHa = fishingProjection.pool.area

    // Tamaño
    const size = fishingProjection.growthReports.length ? fishingProjection.growthReports[fishingProjection.growthReports.length - 1].size : 0 // Tamaño del último reporte registrado, ordenado por ID

    // tamaño de pre-antepenultimo reporte registrado
    const preAntepenultimate = fishingProjection.growthReports.length > 4 ? fishingProjection.growthReports[fishingProjection.growthReports.length - 5].size : 0

    // Crecimiento 4 últimos (Tamaño - ([tamaño de pre-antepenultimo reporte registrado])) / 28 * 7
    const lastFourGrowth = fishingProjection.growthReports.length > 4 ? (size - (preAntepenultimate)) / 28 * 7 : 0

    fishingProjection.lastFourGrowth = lastFourGrowth.toFixed(3)

    if (fishingProjection.shrimpProjection) {
      // Peso de cosecha
      var harvest_weight = fishingProjection.shrimpProjection.harvest_weight

      // Fecha último reporte
      var lastReportDate = moment(fishingProjection.growthReports[fishingProjection.growthReports.length - 1].date)

      // Día de cosecha = Fecha + (((Peso Cosecha - tamaño) / Crecimiento 4 ult) * 7)
      var daysDifference = (((harvest_weight - size) / lastFourGrowth) * 7) // Días a sumar con decimales
      daysDifference = daysDifference - Math.floor(daysDifference) // Guardo los decimales porque la fórmula loca del excel los necesita para que una resta de día de decimales
      var harvestDay = moment(lastReportDate).add((((harvest_weight - size) / lastFourGrowth) * 7), 'days')

      // Días faltan Dia Cosecha - Fecha + 1
      const daysLeft = moment(harvestDay).diff(moment(lastReportDate), 'days') + 1

      // Debe crecer (Peso Cosecha - tamaño) / Días Faltan * 7
      fishingProjection.shrimpProjection.mustGrow = ((harvest_weight - size) / daysLeft * 7).toFixed(2)

      // Libras = Cantidad * (% Sup) * Peso Cosecha / 454
      var pounds = fishingProjection.qty * (fishingProjection.shrimpProjection.percent_sup / 100) * harvest_weight / 454

      // Libras / Hectárea = Libras / Hectareas de la piscina
      const poundsHas = (pounds / poolHa).toFixed()



      // Días totales
      // var totalDays = moment(harvestDay).diff(moment(fishingProjection.seedtime), 'days') // Bueno
      var totalDays = moment(harvestDay).diff(moment(fishingProjection.seedtime), 'days') - (1 - daysDifference)// Invento

      fishingProjection.shrimpProjection.pounds = pounds.toFixed()
      fishingProjection.shrimpProjection.daysLeft = daysLeft
      fishingProjection.shrimpProjection.poundsHas = poundsHas
      fishingProjection.shrimpProjection.harvestDay = moment(harvestDay).subtract(1, 'days').format('DD-MM-YYYY')
      fishingProjection.shrimpProjection.totalDays = totalDays.toFixed()
  }
      if (fishingProjection.costProjection !== null) {
        // Libras raleo
        const pounds_raleo = parseFloat(fishingProjection.costProjection.pounds_raleo)


        // Precio del camarón al crear la proyección
        const preShrimpPrice = fishingProjection.costProjection.shrimp_price

        // Precio actual del camarón
        const currentShrimpPrice = (await ShrimpPrice.query().orderBy('date', 'desc').first()).toJSON()

        // Venta $/lb = Peso Cosecha * [Precio actual del camaron]
        const preSalePound = harvest_weight * preShrimpPrice
        const salePound = harvest_weight * currentShrimpPrice

        // Costo larva = Costo Juv * Cantidad / 1000
        const larvaCost = fishingProjection.costProjection.cost_juv * fishingProjection.qty / 1000

        // Saldo Kilos x alim = ([Hectareas de la piscina] * Kg/Ha x Alim dia) * (Día Cosecha - [Fecha de ultimo reporte registrado])
        const balanceKilos = (poolHa * fishingProjection.costProjection.kg_ha_food_day).toFixed(2) * (moment(harvestDay).diff(moment(lastReportDate), 'days') - 1 + (daysDifference)) // Ver explicación de daysDifference

        // Total Alim Kg = Kg Acum + Saldo Kilos x alim
        const totalFoodKg = parseFloat(fishingProjection.costProjection.kg_acum) + parseFloat(balanceKilos)

        // Costo Alim = Total Alim Kg * $ Kilo Alim
        const foodCost = (totalFoodKg * fishingProjection.costProjection.price_kilo_food).toFixed()

        // Costo Opera = [Hectareas de la piscina] * (Dias Totales + Días Seco) * costo has / dias
        const operaCost = poolHa * (totalDays + fishingProjection.costProjection.dry_days) * fishingProjection.costProjection.cost_ha_days

        // Costo = (Costo Larva) + (Costo Alim) + (Costo Operativo)
        const cost = parseFloat(larvaCost) + parseFloat(foodCost) + parseFloat(operaCost)

        // Venta raleo = (Peso Raleo * [Precio actual del camaron]) * Lbs Raleo
        const preThinningSale = (fishingProjection.costProjection.weight_raleo * preShrimpPrice) * pounds_raleo
        const thinningSale = (fishingProjection.costProjection.weight_raleo * currentShrimpPrice) * pounds_raleo

        // Venta = (Libras * Venta $/lb) + Venta Raleo
        const preSale = (pounds * preSalePound) + preThinningSale
        const sale = (pounds * salePound) + thinningSale

        // P&G Ganancias y pérdidas = Venta - Costo
        const preEarningLose = preSale - cost
        const earningLose = sale - cost

        // Ut / ha / dia = P&G / Dias Totales / [Hectareas de la piscina]
        const pre_ut_ha_day = (preEarningLose / totalDays / poolHa).toFixed()
        const ut_ha_day = (earningLose / totalDays / poolHa).toFixed()

        // Util / Ha = P&G / [Hectareas de la piscina]
        const pre_util_ha = preEarningLose / poolHa
        const util_ha = earningLose / poolHa

        // Biomasa inicial = (Cantidad * Peso Siembra) / 454
        const initialBiomass = (fishingProjection.qty * sowing_weight) / 454

        // FC = (Total Alim Kg * 2.2046) / (Libras + Lbs Raleo - Biomasa Inicial)
        const fc = (totalFoodKg * 2.2046) / (parseFloat(pounds) + parseFloat(pounds_raleo) - parseFloat(initialBiomass))

        ////////////////////////Proyección de costos con precio que tenía el camarón en ese momento/////////////////////////////
        fishingProjection.costProjection.preSalePound = preSalePound.toFixed(2)
        fishingProjection.costProjection.preCost = cost.toFixed()
        fishingProjection.costProjection.preSale = preSale.toFixed()
        fishingProjection.costProjection.pre_ut_ha_day = pre_ut_ha_day
        fishingProjection.costProjection.preThinningSale = preThinningSale.toFixed(2)
        fishingProjection.costProjection.preEarningLose = preEarningLose.toFixed(2)
        fishingProjection.costProjection.preBalanceKilos = balanceKilos.toFixed()
        fishingProjection.costProjection.pre_util_ha = pre_util_ha.toFixed()
        fishingProjection.costProjection.preTotalFoodKg = totalFoodKg.toFixed()
        fishingProjection.costProjection.preLarvaCost = larvaCost
        fishingProjection.costProjection.preFoodCost = foodCost
        fishingProjection.costProjection.preOperaCost = operaCost.toFixed()
        fishingProjection.costProjection.preFc = fc.toFixed(2)
        fishingProjection.costProjection.preShrimpPrice = preShrimpPrice
        fishingProjection.costProjection.preDate = moment(fishingProjection.costProjection.created_at).format('DD-MM-YYYY')
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////

        ////////////////////////Proyección de costos con precio actual del camarón/////////////////////////////
        fishingProjection.costProjection.salePound = salePound.toFixed(2)
        fishingProjection.costProjection.cost = cost.toFixed()
        fishingProjection.costProjection.sale = sale.toFixed()
        fishingProjection.costProjection.ut_ha_day = ut_ha_day
        fishingProjection.costProjection.thinningSale = thinningSale.toFixed(2)
        fishingProjection.costProjection.earningLose = earningLose.toFixed(2)
        fishingProjection.costProjection.balanceKilos = balanceKilos.toFixed()
        fishingProjection.costProjection.util_ha = util_ha.toFixed()
        fishingProjection.costProjection.totalFoodKg = totalFoodKg.toFixed()
        fishingProjection.costProjection.larvaCost = larvaCost
        fishingProjection.costProjection.foodCost = foodCost
        fishingProjection.costProjection.operaCost = operaCost.toFixed()
        fishingProjection.costProjection.fc = fc.toFixed(2)
        fishingProjection.costProjection.currentShrimpPrice = currentShrimpPrice
        fishingProjection.costProjection.date = moment().format('DD-MM-YYYY')
      ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    } else {
      fishingProjection.costProjection = {
        preFoodCost: '',
        preLarvaCost: '',
        pre_util_ha: '',
        preSale: '',
        preEarningLose: '',
        preThinningSale: '',
        preCost: '',
        preSalePound: ''
      }
    }


    let previousSize = 0
    let age2 = moment().diff(moment(fishingProjection.seedtime), 'days') + 1
    fishingProjection.seedtime = moment(fishingProjection.seedtime).format('DD-MM-YYYY')
    fishingProjection.growthReports.forEach(element => {
      element.date = moment(element.date).format('DD-MM-YYYY')
      element.growth = (element.size - previousSize).toFixed(3)
      previousSize = element.size
      element.generalGrowth = (((element.size - sowing_weight) / age2 * 7)).toFixed(3)
    })


    fishingProjection.growthReports.forEach(element => {
      date.push([{text: element.date, fontSize: 10}, {text: element.size, fontSize: 10}, {text: element.growth, fontSize: 10}, {text: element.generalGrowth, fontSize: 10}, {text: element.percent, fontSize: 10}, {text: element.estimated, fontSize: 10}, {text:element.mr, fontSize:10}, {text: element.dist, fontSize: 10}, {text: element.mf, fontSize:10}])
    })

    let title ='LABORATORIO'
    let subtitle ='PROYECCIÓN DE PESCA'
    let name = 'Asesor Técnico:'
    this.img = {
      image: 'logo.jpeg',
      fit: [70, 70],
      absolutePosition: {
        'x': 35,
        'y': 35
      }
    },
    this.img2 = {
      image: 'logo.jpeg',
      fit: [70, 70],
      absolutePosition: {
        'x': 20,
        'y': 10
      }
    }
    var docDefinition = {
      pageSize: 'A4',
      fontSize: 10,
      pageMargins: [60, 120, 40, 90],
      header: [
        {
          margin: [20, 40, 20, 20],
          alignment: 'center',
          text: [
            {
              bold: true,
              text: `${title}\n`,
              fontSize: 14
            },
            {
              text: `${subtitle}\n`,
              fontSize: 12
            }
          ]
        },
        this.img
      ],
      content: [
        {
          margin: [0,-12,0,0],
           columns: [
            { text: 'PISCINA: ', bold: true, fontSize: 10},
            { text: fishingProjection.pool.description , fontSize: 10},
            { text: 'HÉCTAREAS: ', bold: true, fontSize: 10},
            { text: fishingProjection.pool.area, fontSize: 10},
            { text: 'TIPO: ', bold: true,  fontSize: 10},
            { text: fishingProjection.type , fontSize: 10}
          ]
        },
        {
          margin: [0,12,0,0],
          columns: [
            { text: 'SIEMBRA: ', bold: true, fontSize: 10},
            { text: fishingProjection.seedtime , fontSize: 10},
            { text: 'CANTIDAD: ', bold: true, fontSize: 10},
            { text: fishingProjection.qty, fontSize: 10},
            { text: 'SIEMBRA: ', bold: true,  fontSize: 10},
            { text: fishingProjection.sowing_weight, fontSize: 10}
          ]
        },
        {
          margin: [0,12,0,0],
          columns: [
            { text: 'DENSIDAD: ', bold: true, fontSize: 10},
            { text: density , fontSize: 10},
            { text: 'BIOMASA: ', bold: true, fontSize: 10},
            { text: InitialBiomas, fontSize: 10},
            { text: 'EDAD: ', bold: true,  fontSize: 10},
            { text: age , fontSize: 10}
          ]
        },
        {
          alignment: 'left',
          bold: true,
          text: [
            { text: '\n\nREPORTES DE CRECIMIENTO:\n\n', fontSize: 10}
          ]
        },
        { // optional
          alignment: 'center',
          margin:  [-25, 10, 0,0],
          table: {
            // headers are automatically repeated if the table spans over multiple pages
            // you can declare how many rows should be treated as headers
            headerRows: 1,
            width: ['*', '*', '*', '*','*', '*', '*', '*','*'],
            body: [
              subtitle1,
              subtitle2,
              ...date
            ]
          }
        },
        {
          alignment: 'left',
          text: [
            { text: '\n\nCRECIMIENTO EN LAS ULTIMAS 4 SEMANAS: ', fontSize: 10},
            { text:  fishingProjection.lastFourGrowth, fontSize: 10, bold: true}
          ]
        },
        {
          alignment: 'left',
          bold: true,
          text: [
            { text: '\nPROYECCIÒN ', fontSize: 10}
          ]
        },
        {
          margin: [0,12,0,0],
          columns: [
            { text: 'Peso: ', bold: true, fontSize: 10},
            { text: fishingProjection.shrimpProjection.harvest_weight, fontSize: 10},
            { text: 'Días Faltantes: ', bold: true, fontSize: 10},
            { text: fishingProjection.shrimpProjection.daysLeft,  fontSize: 10}
          ]
        },
        {
          margin: [0,12,0,0],
          columns: [
            { text: '% Sup: ', bold: true, fontSize: 10},
            { text: fishingProjection.shrimpProjection.percent_sup , fontSize: 10},
            { text: 'Días Totales: ', bold: true, fontSize: 10},
            { text: fishingProjection.shrimpProjection.totalDays, fontSize: 10}
          ]
        },
        {
          margin: [0,12,0,0],
          columns: [
            { text: 'Día Cosecha: ', bold: true, fontSize: 10},
            { text: fishingProjection.shrimpProjection.harvestDay , fontSize: 10},
            { text: 'Libras: ', bold: true, fontSize: 10},
            { text: fishingProjection.shrimpProjection.pounds, fontSize: 10}
          ]
        },
        {
          margin: [0,12,0,0],
          columns: [
            { text: 'Debe Crec: ', fontSize: 10, bold: true},
            { text: fishingProjection.shrimpProjection.mustGrow , fontSize: 10},
            { text: 'Lbs Ha: ', fontSize: 10, bold: true},
            { text: fishingProjection.shrimpProjection.poundsHas,  fontSize: 10}
          ]
        },
        {
          alignment: 'left',
          bold: true,
          text: [
            { text: '\n\nPROYECCIONES DE COSTO ', fontSize: 10}
          ]
        },
        {
          margin: [0,12,0,0],
          columns: [
            { text: 'Ventas $ / Lb: ', bold: true, fontSize: 10},
            { text: fishingProjection.costProjection.preSalePound ? fishingProjection.costProjection.preSalePound: '', fontSize: 10},
            { text: 'Costo: ', bold: true, fontSize: 10},
            { text: fishingProjection.costProjection.preCost ? fishingProjection.costProjection.preCost: '',  fontSize: 10}
          ]
        },
        {
          margin: [0,12,0,0],
          columns: [
            { text: 'Venta Raleo: ', bold: true, fontSize: 10},
            { text: fishingProjection.costProjection.preThinningSale ? fishingProjection.costProjection.preThinningSale: '', fontSize: 10},
            { text: 'P&G: ', bold: true, fontSize: 10},
            { text: fishingProjection.costProjection.preEarningLose ? fishingProjection.costProjection.preEarningLose : '' , fontSize: 10}
          ]
        },
        {
          margin: [0,12,0,0],
          columns: [
            { text: 'Venta: ', bold: true, fontSize: 10},
            { text: fishingProjection.costProjection.preSale ? fishingProjection.costProjection.preSale: '', fontSize: 10},
            { text: 'Util/Ha: ', bold: true, fontSize: 10},
            { text: fishingProjection.costProjection.pre_util_ha ? fishingProjection.costProjection.pre_util_ha :'', fontSize: 10}
          ]
        },
        {
          margin: [0,12,0,0],
          columns: [
            { text: 'Costo Larva: ', fontSize: 10, bold: true},
            { text: fishingProjection.costProjection.preLarvaCost ? fishingProjection.costProjection.preLarvaCost: '', fontSize: 10},
            { text: 'Costo Alim: ', fontSize: 10, bold: true},
            { text: fishingProjection.costProjection.preFoodCost ? fishingProjection.costProjection.preFoodCost: '',  fontSize: 10}
          ]
        },
        //df
        {
          margin: [0,12,0,0],
          columns: [
            { text: 'Ut/Ha/Día: ', bold: true, fontSize: 10},
            { text: fishingProjection.costProjection.pre_ut_ha_day ? fishingProjection.costProjection.pre_ut_ha_day: '', fontSize: 10},
            { text: 'Saldo Kilos x alim: ', bold: true, fontSize: 10},
            { text: fishingProjection.costProjection.preBalanceKilos ? fishingProjection.costProjection.preBalanceKilos : '' , fontSize: 10}
          ]
        },
        {
          margin: [0,12,0,0],
          columns: [
            { text: 'Total Alim Kilos: ', bold: true, fontSize: 10},
            { text: fishingProjection.costProjection.preTotalFoodKg ? fishingProjection.costProjection.preTotalFoodKg: '', fontSize: 10},
            { text: 'Costo Opera: ', bold: true, fontSize: 10},
            { text: fishingProjection.costProjection.preOperaCost ? fishingProjection.costProjection.preOperaCost :'', fontSize: 10}
          ]
        },
        {
          margin: [0,12,0,0],
          alignment: 'left',
          text: [
            { text: 'FC ', fontSize: 10, bold: true},
            { text: fishingProjection.costProjection.fc ? fishingProjection.costProjection.fc: '', fontSize: 10}
          ]
        },
        /*,
        {
          table: {
            // headers are automatically repeated if the table spans over multiple pages
            // you can declare how many rows should be treated as headers
            headerRows: 2,
            width: ['*','*','*','*', '*'],
            body: [
              file1,
              file2,
            ]
          }
        } */
      ],
      footer: {
        columns: [
          this.img2,
          {text: 'Dpto. Técnico\n', bold: true, alignment: 'left', fontSize: 12, margin: [-25, 13, 0, 0] },
          {text: 'División Acuacultura\n', alignment: 'left', fontSize: 10, margin: [-144, 25, 0, 0]},
          {text: 'Guayaquil - Ecuador', bold: true, alignment: 'left', fontSize: 12, margin: [-263, 60, 0, 0] },
          {text: `${name}\n${fishingProjection.technicalAdvisor.full_name ? fishingProjection.technicalAdvisor.full_name : '' }\n`,bold:true, alignment: 'left', fontSize: 10, margin: [5, 30, 0, 0] },
        ]
      },
      style: {
        fontss: {
          fontSize: 8,
          alignment: 'center'
        }
      }
    }

    var options = {
      // ...
    }
    //return docDefinition
    var pdfDoc = printer.createPdfKitDocument(docDefinition, options)
    const dir = Helpers.appRoot('storage/uploads/')
    const filename = `Proyección_Pesca_${fishingProjection.id}.pdf`
    if (!fs.existsSync(dir)){
      fs.mkdirSync(dir);
    }
    pdfDoc.pipe(fs.createWriteStream(`${dir}/${filename}`));
    pdfDoc.end()

    /*
    let to = 'elyohan14@gmail.com'
    if (floorAnalysis.applicant_id === 0) { // si el solicitante es el mismo cliente hay que buscar correo del cliente
      to = floorAnalysis.shrimp.customer ? floorAnalysis.shrimp.customer.electronic_documents_email : ''
    } else {
      let find = (floorAnalysis.shrimp.customer.contacts.find(val => val.id === floorAnalysis.applicant_id))
      to = find ? find.email : 'elyohan14@gmail.com'
    }
    // return to
    console.log('to', to)
    /* Email.send({
      to,
      subject: 'Análisis',
      content: 'Saludos',
      attachments: [
        {
          filename,
          path: `${dir}/${filename}`
        }
      ]
    }) */
    response.send(`Proyección_Pesca_${fishingProjection.id}`)


  }
  // Analisis Patologico
  async printPathologyAnalysis ({ params, response }) {
    var printer = new PdfPrinter(fonts)
    const pathologyAnalysis = (await PathologyAnalysisMaster.query().with('pools.pool').with('shrimp.zone').with('user.technical_advisor')
    .with('shrimp.customer.contacts').with('applicant').where('id', params.id).first()).toJSON()
    const imgs = (await Img.query().with('poolDetail.pool')
    .where({'analisys_id': pathologyAnalysis.id}).orderBy('patologies_analysis_detail_id').fetch()).toJSON()

    moment.locale('es')
    pathologyAnalysis.delivery_date = moment(pathologyAnalysis.delivery_date).format("DD-MMM-YYYY")
    pathologyAnalysis.analysis_date = moment(pathologyAnalysis.analysis_date).format("DD-MMM-YYYY")
    

    let number = []
    number.push({text: 'Piscinas', alignment: 'center', bold: true, fontSize: 10})
    let average_weight = [{text: 'Peso promedio g.', fontSize: 10 }]
    let flaccidity = [{text: 'Flacidez %', fontSize: 10 }]
    let inflameduropods_pool = [{text: 'Urópodos inflamados %', fontSize: 10 }]
    let necrosis = [{text: 'Necrosis %', fontSize: 10 }]
    let rough_antennas_pool =[ {text: 'Antenas rugosas %', fontSize: 10 }]
    let misshapen_dwarfs = [{text: 'deformes-enanos %', fontSize: 10 }]
    let dirty = [{text: 'Sucias (detritus)', fontSize: 10 }]
    let algae =[ {text: 'Algas', fontSize: 10 }]
    let epistylis = [{text: 'Epistylis', fontSize: 10 }]
    let vortex = [{text: 'Vorticela', fontSize: 10 }]
    let zoothamnium = [{text: 'Zoothamnium', fontSize: 10 }]
    let acineta =[ {text: 'Acineta', fontSize: 10 }]
    let ciliated_apostome = [{text: 'Ciliado Apostome', fontSize: 10 }]
    // let chromatophores = [{text: 'cromatoforos inf.', fontSize: 10 }]
    let others = [{text: 'Otros', fontSize: 10 }]
    let lipids = [{text: 'Lípidos', fontSize: 10 }]
    let thickening = [{text: 'Engrosamiento', fontSize: 10 }]
    let deformity = [{text: 'Deformidad', fontSize: 10 }]
    let constriction = [{text: 'Estrangulamiento', fontSize: 10 }]
    let necrosis_in_tubules = [{text: 'Necrosis', fontSize: 10 }]
    let vermiform_bodies = [{text: 'Cuerpos vermiformes', fontSize: 10 }]
    let baculovirus = [{text: 'Baculovirus', fontSize: 10 }]
    let gregarious = [{text: 'Gregarinas', fontSize: 10 }]
    let nemamotods = [{text: 'Nemátodos', fontSize: 10 }]
    let intestine_algae = [{text: 'Algas', fontSize: 10 }]
    let debris =[ {text: 'Detritos', fontSize: 10 }]
    let balanced = [{text: 'Balanceado', fontSize: 10 }]
    let gregarine_eggs = [{text: 'Huevos de gregarinas', fontSize: 10 }]
    let crustacean_remains = [{text: 'Restos crustáceos', fontSize: 10 }]
    let observations = []
    let allObservations = []
    let recommendations = []
    let allRecommendations = []

    let widths = []
    let space1 = []
    let space2 = []
    let space3 = []
    let subtitle1 = []
    let subtitle2 = []
    let subtitle3 = []
    let subtitle4 = []
    let subtitle5 = []
    let tables = []
    let band = false
    let cont = 0
    let brek = ''
    let recomendate = []

    if (pathologyAnalysis.pools.length === 0) {
     // Dentro las validaciones si no hay piscina
      average_weight.push({text: '-', fontSize: 10})
      flaccidity.push({text: '-', fontSize: 10})
      inflameduropods_pool.push({text: '-', fontSize: 10})
      necrosis.push({text: '-', fontSize: 10})
      rough_antennas_pool.push({text: '-', fontSize: 10})
      misshapen_dwarfs.push({text: '-', fontSize: 10})

      dirty.push({text: '-', fontSize: 10})
      epistylis.push({text: '-', fontSize: 10})
      vortex.push({text: '-', fontSize: 10})
      zoothamnium.push({text: '-', fontSize: 10})
      acineta.push({text: '-', fontSize: 10})
      ciliated_apostome.push({text: '-', fontSize: 10})
//      chromatophores.push({text: '-', fontSize: 10})
others.push({text: '-', fontSize: 10})
      algae.push({text: '-', fontSize: 10})

      lipids.push({text: '-', fontSize: 10})
      thickening.push({text: '-', fontSize: 10})
      deformity.push({text: '-', fontSize: 10})
      constriction.push({text: '-', fontSize: 10})
      necrosis_in_tubules.push({text: '-', fontSize: 10})
      vermiform_bodies.push({text: '-', fontSize: 10})

      baculovirus.push({text: '-', fontSize: 10})
      gregarious.push({text: '-', fontSize: 10})
      nemamotods.push({text: '-', fontSize: 10})
      intestine_algae.push({text: '-', fontSize: 10})
      debris.push({text: '-', fontSize: 10})
      balanced.push({text: '-', fontSize: 10})
      gregarine_eggs.push({text: '-', fontSize: 10})
      crustacean_remains.push({text: '-', fontSize: 10})

      allObservations.push({text: 'No se seleccionó piscina', fontSize: 10})
      observations.push('')
      allRecommendations.push({text: 'No se seleccionó piscina', fontSize: 10}) // VAS
      recommendations.push('')
      widths.push('*')
      subtitle4.push({ text: 'Observaciones ', bold: true })
      subtitle5.push({ text: 'Recomendaciones ', bold: true })
      widths.push('*')
      subtitle4.push('')
      subtitle5.push('')
      number.push({text: '-', alignment: 'center'})
      subtitle1.push({text: 'BRANQUIAS', colSpan: 3, alignment: 'left', bold:true, fontSize: 12})
      subtitle2.push({text: 'HEPATOPÁNCREAS', colSpan:3, alignment: 'left', bold:true, fontSize: 12})
      subtitle3.push({text: 'INTESTINO', colSpan: 3, alignment: 'left', bold:true, fontSize: 12})
    } else { // Si hay piscinas
      let pools = pathologyAnalysis.pools
      // Si la cantidad de piscinas es 4 o menos entonces es una sola tabla
      if (pools.length < 5) { // 4 o menos
        let blankColumns = Array(4 - pools.length).fill('')
        pools = pools.concat(blankColumns)
      } else if (pools.length < 9) { // 8 o menos
        let blankColumns = Array(4 - (pools.length - 4)).fill('')
        pools = pools.concat(blankColumns)
      }
      
      widths.push('*')
      for (const i in pools) {
        if (pools[i]) {
          pools[i].flaccidity =  parseInt(pools[i].flaccidity)
          pools[i].inflameduropods_pool = parseInt(pools[i].inflameduropods_pool)
          pools[i].necrosis = parseInt(pools[i].necrosis)
          pools[i].rough_antennas_pool = parseInt(pools[i].rough_antennas_pool)
          pools[i].misshapen_dwarfs = parseInt(pools[i].misshapen_dwarfs)
        }
          cont = cont + 1
          widths.push(50)
          subtitle4.push('')
          subtitle5.push('')
          number.push({text: pools[i].pool ? pools[i].pool.description : '', alignment: 'center', fontSize: 10, bold: true})
          average_weight.push({ text: pools[i].average_weight_pool, alignment: 'center', fontSize: 10 })
          flaccidity.push({ text: pools[i].flaccidity ? `${pools[i].flaccidity}%` : '-', alignment: 'center', fontSize: 10 })
          inflameduropods_pool.push({ text: pools[i].inflameduropods_pool ? `${pools[i].inflameduropods_pool}%` : '-', alignment: 'center', fontSize: 10 })
          necrosis.push({ text: pools[i].necrosis ? `${pools[i].necrosis}%` : '-', alignment: 'center', fontSize: 10 })
          rough_antennas_pool.push({ text: pools[i].rough_antennas_pool ? `${pools[i].rough_antennas_pool}%` : '-', alignment: 'center', fontSize: 10 })
          misshapen_dwarfs.push({ text: pools[i].misshapen_dwarfs ? `${pools[i].misshapen_dwarfs}%` : '-', alignment: 'center', fontSize: 10 })

          dirty.push({ text: pools[i].dirty ? `${numberToPlus(pools[i].dirty)}` : '-', alignment: 'center', fontSize: 10, background: getBackgroundColorMore2OrEqual(pools[i].dirty) })
          epistylis.push({ text: pools[i].epistylis ? `${numberToPlus(pools[i].epistylis)}` : '-', alignment: 'center', fontSize: 10, background: getBackgroundColorMore2OrEqual(pools[i].epistylis) })
          vortex.push({ text: pools[i].vortex ? `${numberToPlus(pools[i].vortex)}` : '-', alignment: 'center', fontSize: 10, background: getBackgroundColorMore1OrEqual(pools[i].vortex) })
          zoothamnium.push({text: pools[i].zoothamnium ? `${numberToPlus(pools[i].zoothamnium)}` : '-', alignment: 'center', fontSize: 10, background: getBackgroundColorMore2OrEqual(pools[i].zoothamnium) })
          acineta.push({ text: pools[i].acineta ? `${numberToPlus(pools[i].acineta)}` : '-', alignment: 'center', fontSize: 10, background: getBackgroundColorMore2OrEqual(pools[i].acineta) })
          ciliated_apostome.push({ text: pools[i].ciliated_apostome ? `${numberToPlus(pools[i].ciliated_apostome)}` : '-', alignment: 'center', fontSize: 10, background: getBackgroundColorMore2OrEqual(pools[i].ciliated_apostome) })
          // chromatophores.push({ text: pools[i].chromatophores ? `${numberToPlus(pools[i].chromatophores)}` : '-', alignment: 'center', fontSize: 10 })
          others.push({ text: pools[i].others ? `${pools[i].others}` : '-', alignment: 'center', fontSize: 10, background: getBackgroundColor(pools[i].others) })
          algae.push({ text: pools[i].algae ? `${numberToPlus(pools[i].algae)}` : '-', alignment: 'center', fontSize: 10, background: getBackgroundColorMore2OrEqual(pools[i].algae) })

          lipids.push({ text: pools[i].lipids ? `${numberToPlus(pools[i].lipids)}` : '-', alignment: 'center', fontSize: 10, background: getBackgroundColor2(pools[i].lipids) })
          thickening.push({ text: pools[i].thickening ? `${numberToPlus(pools[i].thickening)}` : '-', alignment: 'center', fontSize: 10, background: getBackgroundColorMore2OrEqual(pools[i].thickening) })
          deformity.push({ text: pools[i].deformity ? `${numberToPlus(pools[i].deformity)}` : '-', alignment: 'center', fontSize: 10, background: getBackgroundColorMore1OrEqual(pools[i].deformity) })
          constriction.push({ text: pools[i].constriction ? `${numberToPlus(pools[i].constriction)}` : '-', alignment: 'center', fontSize: 10, background: getBackgroundColorMore1OrEqual(pools[i].constriction) })
          necrosis_in_tubules.push({ text: pools[i].necrosis_in_tubules ? `${numberToPlus(pools[i].necrosis_in_tubules)}` : '-', alignment: 'center', fontSize: 10, background: getBackgroundColorMore2OrEqual(pools[i].necrosis_in_tubules) })
          vermiform_bodies.push({ text: pools[i].vermiform_bodies ? `${numberToPlus(pools[i].vermiform_bodies)}` : '-', alignment: 'center', fontSize: 10, background: getBackgroundColorMore2OrEqual(pools[i].vermiform_bodies) })

          baculovirus.push({ text: pools[i].baculovirus ? `${numberToPlus(pools[i].baculovirus)}` : '-', alignment: 'center', fontSize: 10, background: getBackgroundColorMore1OrEqual(pools[i].baculovirus) })
          gregarious.push({ text: pools[i].gregarious ? `${numberToPlus(pools[i].gregarious)}` : '-', alignment: 'center', fontSize: 10, background: getBackgroundColorMore2OrEqual(pools[i].gregarious) })
          nemamotods.push({ text: pools[i].nemamotods ? `${numberToPlus(pools[i].nemamotods)}` : '-', alignment: 'center', fontSize: 10, background: getBackgroundColorMore1OrEqual(pools[i].nemamotods) })
          intestine_algae.push({ text: pools[i].intestine_algae ? `${numberToPlus(pools[i].intestine_algae)}` : '-', alignment: 'center', fontSize: 10, background: null })
          debris.push({ text: pools[i].debris ? `${numberToPlus(pools[i].debris)}` : '-', alignment: 'center', fontSize: 10, background: getBackgroundColorMore2OrEqual(pools[i].debris) })
          balanced.push({ text: pools[i].balanced ? `${numberToPlus(pools[i].balanced)}` : '-', alignment: 'center', fontSize: 10, background: null })
          gregarine_eggs.push({ text: pools[i].gregarine_eggs ? `${numberToPlus(pools[i].gregarine_eggs)}` : '-', alignment: 'center', fontSize: 10, background: getBackgroundColorMore2OrEqual(pools[i].gregarine_eggs) })
          crustacean_remains.push({ text: pools[i].crustacean_remains ? `${numberToPlus(pools[i].crustacean_remains)}` : '-', alignment: 'center', fontSize: 10, background: getBackgroundColorMore1OrEqual(pools[i].crustacean_remains) })
          // if ()
          allObservations.push({text: `${pools[i].observations ? pools[i].observations : ''}\n`, fontSize: 10})
          recomendate.push({text: `${pools[i].pool ? pools[i].pool.description : ''}: `, bold: true, fontSize: 10})
          allRecommendations.push({text: `${pools[i].recommendations ? pools[i].recommendations : ''}\n`, fontSize: 10})
         
          recommendations.push('')
          if (cont == 4) {
            cont = 0
            band = true
            number.push({text: 'Rango Óptimo', bold: true, colSpan: 1, alignment: 'center', fontSize: 10})
            space1.push({text: '', colSpan: 6, fontSize: 11, border: [false, true, false, false]})
            subtitle1.push({text: 'BRANQUIAS', colSpan: 6, alignment: 'center', bold:true, fontSize: 12})
            space2.push({text: '', colSpan: 6, fontSize: 11, border: [false, true, false, false]})
            subtitle2.push({text: 'HEPATOPÁNCREAS', colSpan: 6, alignment: 'center', bold:true, fontSize: 12})
            space3.push({text: '', colSpan: 6, fontSize: 11, border: [false, true, false, false]})
            subtitle3.push({text: 'INTESTINO', colSpan: 6, alignment: 'center', bold:true, fontSize: 12})
            widths.push('*')
            average_weight.push('')
            flaccidity.push('')
            inflameduropods_pool.push('')
            necrosis.push({text:'\n* BAJO **MEDIO ***ALTO\nN: NORMAL E: ENFERMO\n Óptimo: <5%', rowSpan: 6, fontSize: 9 })
            rough_antennas_pool.push('')
            misshapen_dwarfs.push('')
            dirty.push({ text: 'Nivel de presencia patógenos\n\n+ BAJO (1-25% afectación)\n++MEDIO (26-50% afectación)\n+++ ALTO (51-75% afectación)\n++++ MUY ALTO (76-100% afectación)', rowSpan: 9, fontSize: 9 })
            epistylis.push('')
            vortex.push('')
            zoothamnium.push('')
            acineta.push('')
            ciliated_apostome.push('')
            //chromatophores.push('')
            others.push('')
            algae.push('')
            lipids.push({text: 'Lípidos Mayor a 3.5 Óptimo\n\n+ BAJO (1-25% afectación)\n++MEDIO (26-50% afectación)\n+++ ALTO (51-75% afectación)\n++++ MUY ALTO (76-100% afectación)', rowSpan: 5, fontSize: 9 })
            thickening.push('')
            deformity.push('')
            constriction.push('')
            necrosis_in_tubules.push('')
            vermiform_bodies.push('')
            baculovirus.push('')
            gregarious.push({text:'Baculovirus sin presencia\n\nGREGARINAS > 50% tratar\nH = Huevos A = Adulto\n+ BAJO (1-25% afectación)\n++MEDIO (26-50% afectación)\n+++ ALTO (51-75% afectación)\n++++ MUY ALTO (76-100% afectación)', rowSpan: 7, fontSize: 9 })
            nemamotods.push('')
            intestine_algae.push('')
            debris.push('')
            balanced.push('')
            gregarine_eggs.push('')
            crustacean_remains.push('')
            observations.push('')
            recommendations.push('')
            tables.push({
              alignment: 'center',
              pageBreak: i != (pools.length - 1) ? 'after' : undefined, // Salto de página para nueva table si no es la última tabla a mostrar
              margin:  [-10, 10, 0,0],
              table: {
                // headers are automatically repeated if the table spans over multiple pages
                // you can declare how many rows should be treated as headers
                widths,
                body: [
                  number,
                  average_weight,
                  necrosis,
                  flaccidity,
                  inflameduropods_pool,
                  rough_antennas_pool,
                  misshapen_dwarfs,
                  space1,
                  subtitle1,
                  dirty,
                  epistylis,
                  vortex,
                  zoothamnium,
                  acineta,
                  ciliated_apostome,
                  others,
                  algae,
                  space2,
                  subtitle2,
                  lipids,
                  thickening,
                  deformity,
                  constriction,
                  necrosis_in_tubules,
                  vermiform_bodies,
                  baculovirus,
                  space3,
                  subtitle3,
                  gregarious,
                  nemamotods,
                  intestine_algae,
                  debris,
                  balanced,
                  gregarine_eggs,
                  crustacean_remains
                ]
              }
            })
            
            widths = ['*']
            number = [{text: 'Piscinas', alignment: 'center', bold: true, fontSize: 10}]
            necrosis = [{text: 'Necrosis %', fontSize: 10 }]
            average_weight = [{text: 'Peso promedio g.', fontSize: 10 }]
            flaccidity = [{text: 'Flacidez %', fontSize: 10 }]
            inflameduropods_pool = [{text: 'Urópodos inflamados %', fontSize: 10 }]
            rough_antennas_pool =[ {text: 'Antenas rugosas %', fontSize: 10 }]
            misshapen_dwarfs = [{text: 'deformes-enanos %', fontSize: 10 }]
            space1 = []
            subtitle1 = []

            dirty = [{text: 'Sucias (detritus)', fontSize: 10 }]
            algae =[ {text: 'Algas', fontSize: 10 }]
            epistylis = [{text: 'Epistylis', fontSize: 10 }]
            vortex = [{text: 'Vorticela', fontSize: 10 }]
            zoothamnium = [{text: 'Zoothamnium', fontSize: 10 }]
            acineta =[ {text: 'Acineta', fontSize: 10 }]
            ciliated_apostome = [{text: 'Ciliado Apostome', fontSize: 10 }]

            subtitle2 = []
            subtitle3 = []
            space2 = []
            space3 = []


            others = [{text: 'Otros', fontSize: 10 }]
            lipids = [{text: 'Lípidos', fontSize: 10 }]
            thickening = [{text: 'Engrosamiento', fontSize: 10 }]
            deformity = [{text: 'Deformidad', fontSize: 10 }]
            constriction = [{text: 'Estrangulamiento', fontSize: 10 }]
            necrosis_in_tubules = [{text: 'Necrosis', fontSize: 10 }]
            vermiform_bodies = [{text: 'Cuerpos vermiformes', fontSize: 10 }]
            baculovirus = [{text: 'Baculovirus', fontSize: 10 }]
            gregarious = [{text: 'Gregarinas', fontSize: 10 }]
            nemamotods = [{text: 'Nemátodos', fontSize: 10 }]
            intestine_algae = [{text: 'Algas', fontSize: 10 }]
            debris =[ {text: 'Detritos', fontSize: 10 }]
            balanced = [{text: 'Balanceado', fontSize: 10 }]
            gregarine_eggs = [{text: 'Huevos de gregarinas', fontSize: 10 }]
            crustacean_remains = [{text: 'Restos crustáceos', fontSize: 10 }]
          }
      }
    }
    // console.log(allObservations)
    /* observations.unshift(allObservations)
    let more = []
    let more2 = []
    for (let i in recomendate) {
      
      // Si hay observaciones insertar more2
      if (allObservations[i].text != '\n') {
        more2.push(recomendate[i])
        more2.push(allObservations[i])
      }

      if (allRecommendations[i].text != '\n') {
        more.push(recomendate[i])
        more.push(allRecommendations[i])
      }
      
      console.log(allObservations[i])
    } */
    /* let recomendation = {
      text: []
    }
    let observate = {
      text: []
    } */
    /* recomendation.text = more
    observate.text = more2 */
    const observate = pathologyAnalysis.observations
    const recomendation = pathologyAnalysis.recommendations
    //console.log(observate)
    // recommendations.unshift(allRecommendations)

    ///////////////borrar
    //pools.push('')

    // Recorro el array de objetos de las imagenes y agrego la ruta especifica para cada una de ellas
    /* let img_analisys = [] Sustituido por nuevo método para mostrar imágenes
    let temp = []
    imgs.forEach(element => {
      temp.push({ image: 'storage/uploads/Pathology/' + element.dir, fit: [100,100], alignment: 'left', width: '*', margin: [30,0,-10,0]}) // Corrige estos margenes
      temp.push({ text: 'Fecha: \n' + moment(element.date).format("DD-MM-YYYY"), alignment: 'left', fontSize: 10, margin: [-110,100,0,0]}, {text: 'Descripción: \n' +element.description, alignment: 'left', fontSize: 10, margin: [-130,100,0,0]})
      if (temp.length === 6) {
        img_analisys.push({columns: [...temp]})
        temp = []
      }
    })
    if ((temp.length >1) && (temp.length < 6)) {
      temp[1].margin = [-155, 100, 0, 0]
      temp[2].margin = [-220, 100, 0, 0]
    }
    img_analisys.push({columns: [...temp]}) */
    // Fin

    // Insertar imágenes en el PDF
    const bodyImagesTable = []
    let cells = []
    imgs.forEach((element, key) => {
      // Cada celda es una imagen con sus detalles
      cells.push([{
        image: 'storage/uploads/Pathology/' + element.dir,
        // fit: [100,100], // Fit es para ajustarlas proporcionalmente
        alignment: 'left',
        width: 150,
        height: 150,
        margin: [0,0,-10,0]
      },
      {
        text: element.poolDetail ? element.poolDetail.pool.description : 'Piscina eliminada', alignment: 'left', height: 500
      },
      {
        text: moment(element.date).format('DD-MM-YYYY'), alignment: 'left'
      },
      {
        text: element.description, alignment: 'left'
      }])
      // Cada 3 celdas/imágenes hay que crear una nueva fila
      if (cells.length === 3 || key === imgs.length - 1) { // si es una celda fin de fila o la última celda/imagen
        // Si es la última imagen y no hay 3 celdas rellenar con espacios en blanco
        if (cells.length != 3) {
          if (3 - cells.length == 1) { // Si hay 2 imágenes en la última fila
            cells.push('')
          } else { // Si hay una sola imagen
            cells.push('', '')
          }
          
        }
        bodyImagesTable.push(cells)
        cells = []
      }
    })
    /////////////////////////////////////////////////////
    const imagesTitle = bodyImagesTable.length ? {
      pageBreak: 'before',
      alignment: 'left',
      bold: true,
      text: [
        { text: '\n\FOTOGRAFIAS ANALISIS N°: ' + pathologyAnalysis.id + '\n\n', fontSize: 10}
      ]
    } : {}
    const imagesTable = bodyImagesTable.length ?
      {
      layout: 'noBorders',
      table: {
        headerRows: 1,
        widths: [ '*', '*', '*' ],
        body: bodyImagesTable
      }
    } : {}

    let title ='LABORATORIO'
    let subtitle ='ANÁLISIS PATOLÓGICO DE CAMARÓN'
    let name = 'Asesor Técnico:'
    this.img = {
      image: 'logo.jpeg',
      fit: [70, 70],
      absolutePosition: {
        'x': 35,
        'y': 35
      }
    },
    this.img2 = {
      image: 'logo.jpeg',
      fit: [70, 70],
      absolutePosition: {
        'x': 35,
        'y': 10
      }
    }
    var docDefinition = {
      pageSize: 'A4',
      pageMargins: [60, 120, 40, 90],
      header: [
        {
          margin: [20, 40],
          alignment: 'center',
          text: [
            {
              bold: true,
              text: `${title}\n`,
              fontSize: 16
            },
            {
              text: `${subtitle}\n`,
              fontSize: 14
            }
          ]
        },
       /*  {
          alignment: 'center',
          text: [
            { text: 'CAMARONERA: ', bold: true, fontSize: 12},
            { text: pathologyAnalysis.shrimp.name ? pathologyAnalysis.shrimp.name : '-', fontSize: 10},
            { text: '             '},
            { text: 'SOLICITANTE: ', bold: true, fontSize: 10},
            { text: pathologyAnalysis.applicant ? `${pathologyAnalysis.applicant.name} ${pathologyAnalysis.applicant.last_name}` : '', fontSize: 10}
          ]
        },
        {
          alignment: 'center',
          text: [
            { text: 'ZONA: ', bold: true, fontSize: 10, margin:[0,0,50,0]},
            { text: pathologyAnalysis.shrimp.zone.name ? pathologyAnalysis.shrimp.zone.name : '-' , fontSize: 10},
            { text: '                       '},
            { text: 'FECHA DE ANÁLISIS: ', bold: true, fontSize: 10},
            { text: pathologyAnalysis.analysis_date ? pathologyAnalysis.analysis_date : '-' , fontSize: 10},
            { text: '                       '},
            { text: 'FECHA DE ENTREGA: ', bold: true, fontSize: 10},
            { text: pathologyAnalysis.delivery_date ? pathologyAnalysis.delivery_date : '-' , fontSize: 10}
          ]
        }, */
        this.img
      ],
      content: [
        /* {
          alignment: 'left',
          text: [
            { text: 'CAMARONERA: ', bold: true, fontSize: 12},
            { text: pathologyAnalysis.shrimp.name ? pathologyAnalysis.shrimp.name : '-', fontSize: 12},
            { text: '             '},
            { text: 'SOLICITANTE: ', bold: true, fontSize: 12},
            { text: pathologyAnalysis.applicant ? `${pathologyAnalysis.applicant.name} ${pathologyAnalysis.applicant.last_name}` : '', fontSize: 12}
          ]
        },
        {
          alignment: 'center',
          text: [
            { text: 'ZONA: ', bold: true, fontSize: 10, margin:[0,0,50,0]},
            { text: pathologyAnalysis.shrimp.zone.name ? pathologyAnalysis.shrimp.zone.name : '-' , fontSize: 10},
            { text: '                       '},
            { text: 'FECHA DE ANÁLISIS: ', bold: true, fontSize: 10},
            { text: pathologyAnalysis.analysis_date ? pathologyAnalysis.analysis_date : '-' , fontSize: 10},
            { text: '                       '},
            { text: 'FECHA DE ENTREGA: ', bold: true, fontSize: 10},
            { text: pathologyAnalysis.delivery_date ? pathologyAnalysis.delivery_date : '-' , fontSize: 10}
          ]
        }, */
        {
          layout: 'noBorders', // optional
          margin: [-10, -10],
          table: {
            // headers are automatically repeated if the table spans over multiple pages
            // you can declare how many rows should be treated as headers
            headerRows: 1,
            widths: [ 100, '*', 100, '*', 50, '*'],
    
            body: [
              [ 
                { text: 'Camaronera:', bold: true, fontSize: 12 },
                { text: pathologyAnalysis.shrimp.name ? pathologyAnalysis.shrimp.name : '-', fontSize: 12},
                { text: 'Solicitante:', bold: true, fontSize: 12 },
                { text: pathologyAnalysis.applicant ? `${pathologyAnalysis.applicant.name} ${pathologyAnalysis.applicant.last_name}` : '', fontSize: 12},
                { text: 'Zona:', bold: true, fontSize: 12 },
                { text: pathologyAnalysis.shrimp.zone.name ? pathologyAnalysis.shrimp.zone.name : '-' , fontSize: 12}
              ],
              [ 
                { text: 'Fecha de análisis:', bold: true, fontSize: 12 },
                { text: pathologyAnalysis.analysis_date ? pathologyAnalysis.analysis_date : '-' , fontSize: 12 },
                { text: 'Fecha de entrega:', bold: true, fontSize: 12 },
                { text: pathologyAnalysis.delivery_date ? pathologyAnalysis.delivery_date : '-' , fontSize: 12 },
                '',
                ''
              ]
            ]
          }
        },
        ...tables,
        imagesTitle,
        imagesTable,
        {
          pageBreak: 'before',
          alignment: 'left',
          bold: true,
          text: [
            { text: '\n\nOBSERVACIONES:\n\n', fontSize: 12}
          ]
        },
        observate,
        {
          alignment: 'left',
          bold: true,
          text: [
            { text: '\n\RECOMENDACIONES:\n\n', fontSize: 12}
          ]
        },
        recomendation,
      ],
      footer: {
        columns: [
          this.img2,
          {text: 'Dpto. Técnico\n', bold: true, alignment: 'left', fontSize: 12, margin: [-20, 13, 0, 0] },
          {text: 'División Acuacultura\n', alignment: 'left', fontSize: 10, margin: [-139, 25, 0, 0]},
          {text: 'Guayaquil - Ecuador', bold: true, alignment: 'left', fontSize: 12, margin: [-259, 36, 0, 0] },
          {text: `_______________________\n${name}\n${pathologyAnalysis.user.technical_advisor.full_name ? pathologyAnalysis.user.technical_advisor.full_name : ''}\n`,bold:true, alignment: 'center', fontSize: 10, margin: [-70, 15, 0, 0] }
        ]
      }
      /* footer: function(currentPage, pageCount) {
        var vm = this
        return {
          columns: [
            vm.img2,
            {text: 'Dpto. Técnico\n', bold: true, alignment: 'left', fontSize: 12, margin: [-25, 13, 0, 0] },
            {text: 'Division Acuacultura\n', alignment: 'left', fontSize: 10, margin: [-147, 25, 0, 0]},
            {text: 'Guayaquil - Ecuador', bold: true, alignment: 'left', fontSize: 12, margin: [-269, 36, 0, 0] },
            {text: `_______________________\n${name}\n${pathologyAnalysis.user.technical_advisor.full_name ? pathologyAnalysis.user.technical_advisor.full_name : ''}\n`,bold:true, alignment: 'center', fontSize: 10, margin: [-70, 15, 0, 0] }
          ]
        }
  
      } */
    }

    var options = {
      // ...
    }
    // return docDefinition
    console.log('printer', options)
    var pdfDoc = printer.createPdfKitDocument(docDefinition, options)
    const dir = Helpers.appRoot('storage/uploads/pathologyanalysis')
    const filename = `Analisis_de_patologias_${pathologyAnalysis.id}.pdf`
    if (!fs.existsSync(dir)){
      fs.mkdirSync(dir);
    }
    pdfDoc.pipe(fs.createWriteStream(`${dir}/${filename}`));
    pdfDoc.end()


    let to = 'elyohan14@gmail.com'
    if (pathologyAnalysis.applicant_id === 0) { // si el solicitante es el mismo cliente hay que buscar correo del cliente
      to = pathologyAnalysis.shrimp.customer ? pathologyAnalysis.shrimp.customer.electronic_documents_email : ''
    } else {
      let found = (pathologyAnalysis.shrimp.customer.contacts.find(val => val.id === pathologyAnalysis.applicant_id))
      to = found ? found.email : to
    }

    response.send(`pathologyanalysis-Analisis_de_patologias_${pathologyAnalysis.id}`)
  }

}
function numberToPlus (number) {
  let plus
  if (number == 1) {
    plus = '+'
  } else if (number == 2) {
    plus = '++'
  }
  else if (number == 3) {
    plus = '+++'
  } else {
    plus = '++++'
  }
  return plus
}
function colorDissolved (waterType, dissolved) {
  if (waterType === 0) { // Agua salada
    if ((dissolved >= 10 && dissolved <= 300) || (dissolved == null)) {
      return ''
    } else {
      return 'yellow'
    }
  } else {
    if ((dissolved >= 10 && dissolved <= 100) || (dissolved == null)) {
      return ''
    } else {
      return 'yellow'
    }
  }
}
function colorAlkalinity (waterType, alcalinity) {
  if (waterType === 0) { // Agua salada
    if ((alcalinity >= 60 && alcalinity <= 250) || (alcalinity == null)) {
      return ''
    } else {
      return 'yellow'
    }
  } else {
    if ((alcalinity >= 20 && alcalinity <= 60) || (alcalinity == null)) {
      return ''
    } else {
      return 'yellow'
    }
  }
}
function colorAmmonium (waterType, ammonium) {
  if (waterType === 0) { // Agua salada
    if (ammonium >= 0 && ammonium <= 0.26) {
      return ''
    } else {
      return 'yellow'
    }
  } else {
    if (ammonium >= 0 && ammonium <= 1.04) { // agua dulce
      return ''
    } else {
      return 'yellow'
    }
  }
}
function colorNitrite (waterType, nitrite) {
  if (waterType === 0) { // Agua salada. 0.003 - 0.660
    if ((nitrite >= 0.003 && nitrite <= 0.660) || (nitrite == null)) {
      return ''
    } else {
      return 'yellow'
    }
  } else {
    if ((nitrite >= 0.003 && nitrite <= 0.33) || (nitrite == null)) { // agua dulce. 0.003 - 0.33
      return ''
    } else {
      return 'yellow'
    }
  }
}
function colorNitrate (waterType, nitrate) {
  if (waterType === 0) { // Agua salada. 1.7 - 3.10
    if ((nitrate >= 1.7 && nitrate <= 3.10) || (nitrate == null)) {
      return ''
    } else {
      return 'yellow'
    }
  } else {
    if ((nitrate >= 2.2 && nitrate <= 4.4) || (nitrate == null)) { // agua dulce. 2.2 -4.4
      return ''
    } else {
      return 'yellow'
    }
  }
}
function colorPhosphate (waterType, phosphate) {
  if (waterType === 0) { // Agua salada. 0.2 - 2.0
    if ((phosphate >= 0.2 && phosphate <= 2.0) || (phosphate == null)) {
      return ''
    } else {
      return 'yellow'
    }
  } else {
    if ((phosphate >= 0.1 && phosphate <= 1.0) || (phosphate == null)) { // agua dulce. 0.1 - 1.0
      return ''
    } else {
      return 'yellow'
    }
  }
}
function colorMagnesium (waterType, value) {
  if (waterType === 0) { // Agua salada. 800 - 2500
    if ((value >= 800 && value <= 2500) || (value == null)) {
      return ''
    } else {
      return 'yellow'
    }
  } else {
    if ((value >= 30 && value <= 120) || (value == null)) { // agua dulce. 30 -120
      return ''
    } else {
      return 'yellow'
    }
  }
}
function colorCalcium (waterType, value) {
  if (waterType === 0) { // Agua salada. 80 - 450
    if ((value >= 80 && value <= 450) || (value == null)) {
      return ''
    } else {
      return 'yellow'
    }
  } else {
    if ((value >= 60 && value <= 120) || (value == null)){ // agua dulce. 60 -120
      return ''
    } else {
      return 'yellow'
    }
  }
}
function colorPotassium (waterType, value) {
  if (waterType === 0) { // Agua salada. 30 - 375
    if ((value >= 30 && value <= 375) || (value == null)) {
      return ''
    } else {
      return 'yellow'
    }
  } else {
    if ((value >= 10 && value <= 35) || (value == null)) { // agua dulce. 10 - 35
      return ''
    } else {
      return 'yellow'
    }
  }
}

function getBackgroundColorMore2OrEqual (value) {
  return value >= 2 ? 'yellow' : undefined
}

function getBackgroundColorMore1OrEqual (value) {
  return value >= 1 ? 'yellow' : undefined
}

function getBackgroundColor (value) {
  return value == 3 || value == 4 ? 'yellow' : undefined
}

function getBackgroundColor2 (value) {
  return value == 1 || value == 2 ? 'yellow' : undefined
}
function numberWithThousandSeparator(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

module.exports = PdfGeneratorController;

