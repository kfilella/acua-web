'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with sections
 */
const Section = use('App/Models/Section')
const List = use('App/Functions/List')
const { validate } = use('Validator')
class SectionController {
  /**
   * Show a list of all sections.
   * GET sections
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response }) {
    const notAssigned = (request.get()).notAssigned
    let sections
    if (notAssigned) { // si se quieren las secciones que no están asignadas a ningún departamento
      sections = (await Section.query().with('department.area').where('department_id', null).fetch()).toJSON()
    } else {
      sections = (await Section.query().with('department.area').fetch()).toJSON()
    }
    
    sections = List.addRowActions(sections, [{ name: 'edit', route: 'sections/form/', permission: '1.10.3' }, { name: 'delete', permission: '1.10.4' }])
    response.send(sections)
  }

  /**
   * Create/save a new section.
   * POST sections
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const validation = await validate(request.all(), Section.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const body = request.only(Section.fillable)
      const section = await Section.create(body)
      response.send(section);
    }
  }

  /**
   * Display a single section.
   * GET sections/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, response }) {
    const section = (await Section.query().with('department.area').where('id', params.id).first()).toJSON()
    section.area_id = section.department.area.id
    response.send(section)
  }


  /**
   * Update section details.
   * PUT or PATCH sections/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const section = await Section.query().where('id', params.id).first()
    if (section) {
      const validation = await validate(request.all(), Section.fieldValidationRules())

      if (validation.fails()) {
        response.unprocessableEntity(validation.messages())
      } else {
        const body = request.only(Section.fillable)

        await Section.query().where('id', params.id).update(body)
        response.send(section)
      }
    } else {
      response.notFound(validation.messages())
    }
  }

  /**
   * Delete a section with id.
   * DELETE sections/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, response }) {
    const section = await Section.find(params.id)
    section.delete()
    response.send('success')
  }
}

module.exports = SectionController
