'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with shrimpprojections
 */
const { validate } = use('Validator')
const ShrimpProjection = use('App/Models/ShrimpProjection')
class ShrimpProjectionController {
  /**
   * Show a list of all shrimpprojections.
   * GET shrimpprojections
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
  }

  /**
   * Create/save a new shrimpprojection.
   * POST shrimpprojections
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const validation = await validate(request.all(), ShrimpProjection.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const body = request.only(ShrimpProjection.fillable)
      const shrimpProjection = await ShrimpProjection.create(body)
      response.send(shrimpProjection)
    }
  }

  /**
   * Display a single shrimpprojection.
   * GET shrimpprojections/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Update shrimpprojection details.
   * PUT or PATCH shrimpprojections/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const shrimpProjection = await ShrimpProjection.query().where('id', params.id).first()
    if (shrimpProjection) {
      const validation = await validate(request.all(), ShrimpProjection.fieldValidationRules())

      if (validation.fails()) {
        response.unprocessableEntity(validation.messages())
      } else {
        const body = request.only(ShrimpProjection.fillable)

        await ShrimpProjection.query().where('id', params.id).update(body)
        response.send(shrimpProjection)
      }
    } else {
      response.notFound(validation.messages())
    }
  }

  /**
   * Delete a shrimpprojection with id.
   * DELETE shrimpprojections/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = ShrimpProjectionController
