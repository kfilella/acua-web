'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Laboratory = use("App/Models/Laboratory")
const { validate } = use('Validator')
/**
 * Resourceful controller for interacting with laboratories
 */
class LaboratoryController {
  /**
   * Show a list of all laboratories.
   * GET laboratories
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    const date = (await Laboratory.all()).toJSON()
    response.send(date)
  }

  /**
   * Render a form to be used for creating a new laboratory.
   * GET laboratories/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new laboratory.
   * POST laboratories
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const date = request.all()
    const validation = await validate(request.all(), Laboratory.fielValidateRules())
    if (!validation.fails()) {
      const body = request.only(Laboratory.fillable)
      await Laboratory.create(body)
    } else {
      response.unprocessableEntity(validation.messages())
    }
  }

  /**
   * Display a single laboratory.
   * GET laboratories/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    const date = await Laboratory.find(params.id)
    response.send(date)
  }

  /**
   * Render a form to update an existing laboratory.
   * GET laboratories/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update laboratory details.
   * PUT or PATCH laboratories/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const date = await Laboratory.query().where('id', params.id).first()
    if (date) {
      const validation = await validate(request.all(), Laboratory.fielValidateRules())
      if (!validation.fails()) {
        const body = request.only(Laboratory.fillable)
        for (let i in body) {
          date[i] = body[i]
        }
        date.save()
        response.send(date)
      } else {        
        response.unprocessableEntity(validation.messages())
      }
    }
  }

  /**
   * Delete a laboratory with id.
   * DELETE laboratories/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    const date = await Laboratory.find(params.id)
    date.delete()
    response.send('success')
  }
}

module.exports = LaboratoryController
