'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Food = use("App/Models/FoodType")
const { validate } = use('Validator')
/**
 * Resourceful controller for interacting with foods
 */
class FoodController {
  /**
   * Show a list of all foods.
   * GET foods
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view, params }) {
    response.send((await Food.query().where('id', params.id).fetch()).toJSON())
  }

  /**
   * Render a form to be used for creating a new food.
   * GET foods/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
    const date = request.all()
    const validation = await validate(request.all(), Food.fieldValidationRules())
    if (!validation.fails()) {
      let band = true
      console.log(date)
      let verify = (await Food.all()).toJSON()
      for (let i in verify) {
        for (let j = verify[i].from_weight; j<= verify[i].to_weight; j++) {
          for (let k = date.Int_range; k<=date.to_weight; k++) {
            if (j === k) {
              band = false
            }
          }
        }
      }
      if (band) { // verifica si ya estan esos rangos guardados
        let body = request.only(Food.fillable)
        await Food.create(body)
        response.status(201).send({
          body,
          status: false,
          to: '/foods'
        })
      } else {
        return response.status(200).send({
          status: true,
          message: 'Ya existe un alimento con esos Rangos',
          icon: 'warning',
          color: 'negative'
        })
      }
    } else {
      response.unprocessableEntity(validation.messages())
    }
  }

  /**
   * Create/save a new food.
   * POST foods
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single food.
   * GET foods/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    let foods = (await Food.all()).toJSON()
    const List = []
    for (let i in foods) {
      List.push({'id': foods[i].id, 'code': foods[i].code, 'type': foods[i].type, 'price_kg': foods[i].price_kg, 'range': foods[i].Int_range + ' - ' + foods[i].to_weight, 'actions': [{type: 'edit', route: 'foods/form/', permission: '4.23'}, {type: 'delete', permission: '4.24'}]})
    }
    response.send(List)
  }

  /**
   * Render a form to update an existing food.
   * GET foods/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
    const validation = await validate(request.all(), Food.fieldValidationRules())
    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const body = request.only(Food.fillable)
      const id = params.id
      response.send((await Food.query().where('id', id).update(body)))
    }
  }

  /**
   * Update food details.
   * PUT or PATCH foods/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a food with id.
   * DELETE foods/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    response.send(await Food.query().where('id', params.id).delete())
  }
}

module.exports = FoodController
