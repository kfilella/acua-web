'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with companies
 */
const Company = use('App/Models/Company')
const User = use('App/Models/User')
const List = use('App/Functions/List')
const { validate } = use('Validator')
class CompanyController {
  /**
   * Show a list of all companies.
   * GET companies
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    let companies = (await Company.all()).toJSON()
    companies = List.addRowActions(companies, [{name: 'edit', route: 'companies/form/', permission: '1.6.5'}, {name: 'delete', permission: '1.6.4'}])
    response.send(companies)
  }

  /**
   * Create/save a new company.
   * POST companies
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const wholeRequest = request.all()
    const validation = await validate(wholeRequest, Company.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const body = request.only(Company.fillable)
      const company = await Company.create(body)
      await User.create({
        email: wholeRequest.email,
        password: wholeRequest.password,
        company_id: company.id
      })
      // Insertar usuario en la tabla de usuarios con id de empresa
      response.send(company)
    }
  }

  /**
   * Display a single company.
   * GET companies/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, response }) {
    const company = (await Company.query().where('id', params.id).first()).toJSON()
    response.send(company)
  }

  /**
   * Update company details.
   * PUT or PATCH companies/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const company = await Company.query().where('id', params.id).first()
    if (company) {
      const validation = await validate(request.all(), Company.fieldValidationRules())

      if (validation.fails()) {
        response.unprocessableEntity(validation.messages())
      } else {
        const body = request.only(Company.fillable)

        await Company.query().where('id', params.id).update(body)
        response.send(company)
      }
    } else {
      response.notFound(validation.messages())
    }
  }

  /**
   * Delete a company with id.
   * DELETE companies/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, response }) {
    const company = await Company.find(params.id)
    company.delete()
    response.send('success')
  }
}

module.exports = CompanyController
