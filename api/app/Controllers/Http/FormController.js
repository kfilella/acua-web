'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with forms
 */


const Form = use('App/Models/Form')
const FormField = use('App/Models/FormField')
const List = use('App/Functions/List')
const { validate } = use('Validator')

class FormController {
  /**
   * Show a list of all forms.
   * GET forms
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ response }) {
    let forms = await Form.all()
    forms = List.addRowActions(forms, [{ name: 'edit', route: 'forms/form/', permission: '1.12.3' }, { name: 'delete', permission: '1.12.4' }])
    response.send(forms)
  }

  /**
   * Render a form to be used for creating a new form.
   * GET forms/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new form.
   * POST forms
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    /* const validation = await validate(request.all(), Form.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const body = request.only(Section.fillable)
      const section = await Section.create(body)
      response.send(section);
    } */
  }

  /**
   * Display a single form.
   * GET forms/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    /* const section = (await Section.query().with('department.area').where('id', params.id).first()).toJSON()
    section.area_id = section.department.area.id
    response.send(section) */
  }

  /**
   * Render a form to update an existing form.
   * GET forms/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update form details.
   * PUT or PATCH forms/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    /* const section = await Section.query().where('id', params.id).first()
    if (section) {
      const validation = await validate(request.all(), Section.fieldValidationRules())

      if (validation.fails()) {
        response.unprocessableEntity(validation.messages())
      } else {
        const body = request.only(Section.fillable)

        await Section.query().where('id', params.id).update(body)
        response.send(section)
      }
    } else {
      response.notFound(validation.messages())
    } */
  }

  /**
   * Delete a form with id.
   * DELETE forms/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    /* const section = await Section.find(params.id)
    section.delete()
    response.send('success') */
  }
}

module.exports = FormController
