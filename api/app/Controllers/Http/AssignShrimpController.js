'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const AssignShrimp = use('App/Models/AssignShrimp')
const TechnicalAdvisor = use('App/Models/TechnicalAdvisor')
const TechnicalChief = use('App/Models/TechnicalChief')
const Customer = use('App/Models/Customer')
const List = use('App/Functions/List')
const { validate } = use('Validator')
/**
 * Resourceful controller for interacting with assignshrimps
 */
class AssignShrimpController {
  /**
   * Lista de asesores técnicos y sus camaroneras y clientes asignados. El jefe debe ver solo sus asesores
   * GET assign_shrimps
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ auth, response, view }) {
    const logguedUser = await auth.getUser()
    const technicalChief = (await TechnicalChief.query().with('teams').where('user_id', logguedUser.id).first()).toJSON()

    let technicalAdvisorsIds = []
    technicalChief.teams.forEach(element => {
      const technicalAdvisors = JSON.parse(element.technical_advisors)
      technicalAdvisors.forEach(element2 => {
        technicalAdvisorsIds.push(element2)
      });
    })

    let technical_advisors = (await TechnicalAdvisor.query().with(['assignedShrimps.shrimp.customer'])
    .whereIn('id', technicalAdvisorsIds).where({ status: true }).fetch()).toJSON()
    technical_advisors = List.addRowActions(technical_advisors, [{ name: 'assign', permission: '5.3.5', route: 'assign_shrimps/form/' }])
    technical_advisors.forEach(element => {
      let shrimps = []
      let customers = []
      element.assignedShrimps.forEach(element2 => {
        if (element2.shrimp) {
          shrimps.push(element2.shrimp.name)
          customers.push(element2.shrimp.customer.legal_name)
        } 
      })
      customers = [...new Set(customers)] // Eliminar cliente repetidos
      element.shrimps = shrimps.length ? shrimps.join(',') : 'Sin Asignar'
      element.customers = customers.length ? customers.join(',') : 'Sin Asignar'
    })

    response.send(technical_advisors)
  }

  /**
   * Create/save a new assignshrimp.
   * POST assignshrimps
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response, params }) {
    const allRequest = request.all()
    const validation = await validate(allRequest, AssignShrimp.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const body = request.only(AssignShrimp.fillable)
      await AssignShrimp.query().where({technical_advisor_id: params.id}).delete()
      for (let j in body.shrimps) {
        let element = body.shrimps[j]
        var assign_shrimps = await AssignShrimp.create({technical_advisor_id: parseInt(params.id), shrimp_id:element})
      }
      response.send(true)
    }
  }

  /**
   * Display a single assignshrimp.
   * GET assignshrimps/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    let assign_shrimps = (await AssignShrimp.query().where({technical_advisor_id: params.id}).fetch()).toJSON()
    console.log(assign_shrimps)
    let shrimps_ids = []
    for (let j in assign_shrimps) {
      shrimps_ids.push(parseInt(assign_shrimps[j].shrimp_id))
    }
    response.send(shrimps_ids)
  }

  /**
   * Render a form to update an existing assignshrimp.
   * GET assignshrimps/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update assignshrimp details.
   * PUT or PATCH assignshrimps/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a assignshrimp with id.
   * DELETE assignshrimps/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = AssignShrimpController
