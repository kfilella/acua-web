'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with ratingtypes
 */
const RatingType = use('App/Models/RatingType')
const List = use('App/Functions/List')
const { validate } = use('Validator')
class RatingTypeController {
  /**
   * Show a list of all ratingtypes.
   * GET ratingtypes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    let ratingTypes = (await RatingType.all()).toJSON()
    ratingTypes = List.addRowActions(ratingTypes, [{ name: 'edit', route: 'rating_types/form/', permission: '1.5.1.2.3' }, { name: 'delete', permission: '1.5.1.2.5' }])
    response.send(ratingTypes)
  }

  /**
   * Create/save a new ratingtype.
   * POST ratingtypes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const validation = await validate(request.all(), RatingType.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const body = request.only(RatingType.fillable)
      const ratingType = await RatingType.create(body)
      response.send(ratingType);
    }
  }

  /**
   * Display a single ratingtype.
   * GET ratingtypes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, response }) {
    const ratingType = (await RatingType.query().where('id', params.id).first()).toJSON()
    response.send(ratingType)
  }

  /**
   * Update ratingtype details.
   * PUT or PATCH ratingtypes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const ratingType = await RatingType.query().where('id', params.id).first()
    if (ratingType) {
      const validation = await validate(request.all(), RatingType.fieldValidationRules())

      if (validation.fails()) {
        response.unprocessableEntity(validation.messages())
      } else {
        const body = request.only(RatingType.fillable)

        await RatingType.query().where('id', params.id).update(body)
        response.send(ratingType)
      }
    } else {
      response.notFound(validation.messages())
    }
  }

  /**
   * Delete a ratingtype with id.
   * DELETE ratingtypes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    const ratingType = await RatingType.find(params.id)
    try {
      await ratingType.delete()
      response.send('success')
    } catch (error) {
      response.forbidden('El tipo de calificación está siendo usada. No se puede eliminar')
      // response.send('error')
      // response.forbidden(error)
    }
  }
}

module.exports = RatingTypeController
