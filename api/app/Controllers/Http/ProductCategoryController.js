'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const ProductCategory = use('App/Models/ProductCategory')
const List = use('App/Functions/List')
const { validate } = use('Validator')
/**
 * Resourceful controller for interacting with productcategories
 */
class ProductCategoryController {
  /**
   * Show a list of all productcategories.
   * GET productcategories
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    let productCategories = (await ProductCategory.all()).toJSON()
    productCategories = List.addRowActions(productCategories, [{name: 'edit', route: 'categories/form/', permission: '4.10'}, {name: 'delete', permission: '4.9'}])
    response.send(productCategories)
  }

  /**
   * Render a form to be used for creating a new productcategory.
   * GET productcategories/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new productcategory.
   * POST productcategories
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const validation = await validate(request.all(), ProductCategory.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const body = request.only(ProductCategory.fillable)
      const productCategories = await ProductCategory.create(body)
      response.send(productCategories)
    }
  }

  /**
   * Display a single productcategory.
   * GET productcategories/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    const productCategoryId = params.id
    const productCategories = (await ProductCategory.query().where('id', productCategoryId).first()).toJSON()
    response.send(productCategories)
  }

  /**
   * Render a form to update an existing productcategory.
   * GET productcategories/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update productcategory details.
   * PUT or PATCH productcategories/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const validation = await validate(request.all(), ProductCategory.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const body = request.only(ProductCategory.fillable)
      const productCategoryId = params.id
      const productCategories = await ProductCategory.query().where('id', productCategoryId).update(body)
      response.send(productCategories);
    }
  }

  /**
   * Delete a productcategory with id.
   * DELETE productcategories/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    response.send(await ProductCategory.query().where('id', params.id).delete())
  }
}

module.exports = ProductCategoryController
