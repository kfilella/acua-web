'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with users
 */
const { validate } = use('Validator')
const User = use('App/Models/User')
const Event = use('App/Models/Event')
const UserEvent = use('App/Models/UserEvent')
const TechnicalAdvisor = use('App/Models/TechnicalAdvisor')
const TechnicalChief = use('App/Models/TechnicalChief')
const Customer = use('App/Models/Customer')
const List = use('App/Functions/List')
const Email = use('App/Functions/Email')
const Notification = use('App/Functions/Notification')
const Env = use('Env')
class UserController {
  /**
   * Show a list of all users.
   * GET users
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response }) {
    let users = (await User.all()).toJSON()
    users = List.addRowActions(users, [
      {name: 'edit', route: 'users/form/', permission: '1.1.3'},
      {name: 'delete', permission: '1.1.4'}
    ])

    response.send(users)
  }

  /**
   * Create/save a new user.
   * POST users
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const validation = await validate(request.all(), User.fieldValidationRules())
    const wholeRequest = request.all()

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const body = request.only(User.fillable)
      const roles = request.only(['roles']).roles
      // return body
      const user = await User.create(body)
      // return user
      await user.roles().attach(roles)

      // Si es un asesor técnico,jefe técnico o cliente hay que hacer la asignación al asesor cambiando el user_id en la tabla de asesores o jefes
      if (roles.find(val => val === 3) && wholeRequest.technical_advisor) {
        await TechnicalAdvisor.query().where('id', wholeRequest.technical_advisor).update({ user_id: user.id })
      } else if (roles.find(val => val === 2) && wholeRequest.technical_chief) {
        await TechnicalChief.query().where('id', wholeRequest.technical_chief).update({ user_id: user.id })
      } else if (roles.find(val => val === 4) && wholeRequest.customer) {
        await Customer.query().where('id', wholeRequest.customer).update({ user_id: user.id })
      }

      const events = (await Event.all()).toJSON()
      for (const i of events) {
        await UserEvent.create({
          user_id: user.id,
          event_id: i.id,
          subject: i.subject,
          content: i.content
        })
      }
      response.send(user);
    }
  }

  /**
   * Display a single user.
   * GET users/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, response }) {
    // const user = (await User.query().with('roles').where('id', params.id).first()).toJSON()
    const user = (await User.query().with('roles',
      query => {
        query.select('id')
      }).where('id', params.id).first()).toJSON()
    user.roles = user.roles.map(value => value.id)
    response.send(user)
  }

  /**
   * Update user details.
   * PUT or PATCH users/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const user =  await User.query().with('roles').where('id', params.id).first()
    if (user) {
      const validation = await validate(request.all(), User.fieldValidationRulesUpdate())

      if (validation.fails()) {
        response.unprocessableEntity(validation.messages())
      } else {
        const body = request.only(User.fillable)
        const roles = request.only(['roles']).roles
        for (const i in body) {
          user[i] = body[i]
        }
        user.save()
        await user.roles().detach()
        await user.roles().attach(roles)
        response.send(user);
      }
    } else {
      response.unprocessableEntity(validation.messages()) // Aquí debe ir uno de not found 404
    }

  }

  /**
   * Delete a user with id.
   * DELETE users/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    const user = await User.find(params.id)
    user.delete()
    response.send('success')
  }
  async login ({ auth, request, response }) {
    const { email, password, mobile } = request.all();
    let token = await auth.attempt(email, password)
    let user = (await User.query().with('roles.permissions').where('email', email).first()).toJSON()

    token.permissions = User.getPermission(user.roles)
    // Si es móvil mandar datos para que puedan ser accedidos offline
    if (mobile) {
      token.user = user
    }

    const roles = user.roles.map(val => val.id)
    let technicalAdvisor

    // Preguntar si es un asesor técnico o jefe técnico y verificar si está activo para poder iniciar sesión
    let status = true
   
    if (roles.find(val => val === 3)) { // Si es asesor técnico
      technicalAdvisor = await TechnicalAdvisor.query().where('user_id', user.id).first()
      // status = ((await TechnicalAdvisor.query().where('user_id', user.id).first()).toJSON()).status
      status = technicalAdvisor ? technicalAdvisor.toJSON().status : status
    }

    if (roles.find(val => val === 2)) { // Si es jefe técnico
      const chief = (await TechnicalChief.query().where('user_id', user.id).first()).toJSON()
      status = chief ? chief.status : status

    }

    if (!status) {
      response.unprocessableEntity('Usuario inactivo')
    } else {
      Email.send({ // Envío de correo con el evento 1
        to: email,
        event: 1
      })
  
      Notification.send({ // Envío de notificación de campana al usuario con el evento 1
        to: user.id,
        event: 1
      })
      response.send(token)
    }
  }
  async resetPassword ({ auth, request, response }) {
    const email = request.input('email')
    const user = await User.query().where('email', email).first()

    if (!user) { // usuario no registrado
      response.badRequest(false)
    } else {
      const userToken = (await auth.generate(user)).token
      const token = userToken + 'YOHAN' + userToken.split('').reverse().join('')
      const url = Env.get('URL', 'http://localhost:8080') + '/#/change_password/' + token

      await Email.send({
        to: email,
        subject: 'Restablecimiento de contraseña',
        content: `Hola, haz solicitado restablecer tu contraseña.
          Para hacerlo debes hacer click <a href="${url}">Aquí</a>`
      })
      user.reset_password_token = token
      user.save()

      response.send(true)
    }


  }
  async verifyResetToken ({ auth, request, response }) {
    const token = request.input('token')
    const validity = await User.query().where('reset_password_token', token).first()

    if (!validity) {
      response.badRequest(false)
    } else {
      response.send(true)
    }
  }
  async updatePassword ({ auth, request, response }) {
    const form = request.all()
    const user = await User.query().where('reset_password_token', form.token).first()
    if (user) {
      user.password = form.password
      user.reset_password_token = ''
      user.save()
      response.send(true)
    } else {
      response.badRequest(false)
    }
  }
}

module.exports = UserController
