'use strict'

const PatologyMaster = use('App/Models/PatologiesAnalysisPool')
const PatologyDetail = use('App/Models/PatologiesAnalysisDetail')
const Img = use('App/Models/PathologiesImg')
const moment = require('moment')
const Shrimp = use('App/Models/Shrimp')
const { validate } = use('Validator')
const List = use('App/Functions/List')
const Email = use('App/Functions/Email')
const Helpers = use("Helpers")
class PathologyController {

  async uploadPoolImage ({ request, response }) {
    const analisisId = request.input('analisys_id')
    let dir = `storage/uploads/Pathology/`
    let showingDir = `analisys-${analisisId}`

    const image = request.file('file', {
      types: ['image', 'png', 'jpg'],
      size: '16mb'
    })
    let fileName = image.clientName.replace(/\s/g, '')
    console.log('filename', fileName)
    fileName = fileName.split('-').join('')
    console.log('filename2', fileName)
    if (image) {
      await image.move(Helpers.appRoot(dir), {
        name: fileName,
        overwrite: true
      })

      if (!image.moved()) {
        return image.error()
      }
      response.send(`${fileName}`)
    }
  }

  async index ({ request, response }) {
    const params = request.get()
    const shrimp = await Shrimp.find(params.shrimp_id)
    let patologies_analysis = (await PatologyMaster.query().with('user.technical_advisor').with('zone').with('pools.pool')
    .with('shrimp.customer.contacts').where(params).orderBy('created_at', 'desc').fetch()).toJSON()
    patologies_analysis.forEach(element => {
      if (element.applicant_id == 0) {
        element.applicant = 'Cliente'
      } else {
        let contact =  (element.shrimp.customer.contacts.find(val => val.id === element.applicant_id))
        element.applicant = contact ? `${contact.name} ${contact.last_name}` : 'Contacto eliminado'
      }
      element.analysis_date = moment(element.analysis_date).format("DD-MM-YYYY")
      // En cada análisis mostrar en un campo las piscinas del análisis. Filtro primero para evitar errores si se han eliminado piscinas
      element.pools = element.pools.filter(val => val.pool).map(val => val.pool.description).join(',')
    })

    patologies_analysis = List.addRowActions(patologies_analysis, [
      { name: 'show', route: `/customers/show/${shrimp.customer_id}/shrimps/${params.shrimp_id}/pathologies/form/`, permission: '2.1.1.1.12' },
      { name: 'printPathologyAnalysis', permission: '2.1.1.1.32' },
      { name: 'generateLinkPathologyAnalysis', permission: '2.1.1.1.32' },
      { name: 'sendMailPathology', permission: '2.1.1.1.14', route: 'pathologies/send_mail/'}
    ])
    response.send(patologies_analysis)
  }

  async sendMail ({ request, response, params }) {
    const { to, cc, content } = request.all()
    const analysisId = params.analysisId
    const filename = `Analisis_de_patologias_${analysisId}.pdf`
    await Email.send({
      to,
      subject: 'Análisis patológico',
      content,
      cc,
      attachments: [
        {
          filename,
          path: Helpers.appRoot('storage/uploads/pathologyanalysis') + `/${filename}`
        }
      ]
    })
    response.send(true)
  }

  async sendMailInfo ({ request, response, params }) {
    const analysisId = params.analysisId

    const analysis = (await PatologyMaster.query().with('applicant').with('shrimp.customer.contacts').where('id', analysisId).first()).toJSON()

    const form_emails = {
      to: analysis.applicant.email,
      contacts_email: analysis.shrimp.customer.contacts.map(val => (
        {
          email: val.email || 'Sin correo',
          name: `${val.name} ${val.last_name}`,
          disable: val.email == null || val.email == ''
        }
      )),
      email_customer: analysis.shrimp.customer.electronic_documents_email
    }
    response.send(form_emails)
  }

  async store ({ request, response, params, auth }) {
    const user = await auth.getUser()
    const wholeRequest = request.all()
    const pools = wholeRequest.pools
    wholeRequest.technical_advisor_id = user.id
    const validation = await validate(wholeRequest, PatologyMaster.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const body = request.only(PatologyMaster.fillable)
      const patologies_analysis = await PatologyMaster.create(body)

      // Guardar detalles de piscinas
      for (const pool of pools) {
        const images = pool.images ? pool.images : []
        delete pool.description
        delete pool.expanded
        delete pool.images
        delete pool.area
        pool.patologies_analysis_pools_id = patologies_analysis.id
        const createdPool = await PatologyDetail.create(pool)

        for (const image of images) {  
          image.analisys_id = patologies_analysis.id
          image.patologies_analysis_detail_id = createdPool.id
          await Img.create(image)
        }
      }

      response.send(patologies_analysis)
    }
  }

  async show ({ params, request, response }) {
    const patologies_analysis = (await PatologyMaster.query().with('pools.pool').with('pools.images')
    .where('id', params.id).first()).toJSON()
    patologies_analysis.delivery_date = moment(patologies_analysis.delivery_date).format("YYYY-MM-DD")
    patologies_analysis.analysis_date = moment(patologies_analysis.analysis_date).format("YYYY-MM-DD")
    patologies_analysis.pools.forEach(element => {
      element.description = element.pool.description
      element.expanded = true
      element.flaccidity = parseInt(element.flaccidity)
      element.inflameduropods_pool = parseInt(element.inflameduropods_pool)
      element.necrosis = parseInt(element.necrosis)
      element.rough_antennas_pool = parseInt(element.rough_antennas_pool)
      element.misshapen_dwarfs = parseInt(element.misshapen_dwarfs)
    });
    response.send(patologies_analysis)
  }

  async update ({ params, request, response }) {
    const wholeRequest = request.all()
    const analisysId = params.id
    const pools = wholeRequest.pools

    const patologiesAnalysis = await PatologyMaster.query().where('id', analisysId).first()
    if (patologiesAnalysis) {
      const validation = await validate(request.all(), PatologyMaster.fieldValidationRules())

      if (validation.fails()) {
        response.unprocessableEntity(validation.messages())
      } else {
        const body = request.only(PatologyMaster.fillable)

        await PatologyMaster.query().where('id', analisysId).update(body)

        //Actualizar datos de piscinas
        // Eliminar piscinas anteriores
        const poolIds = []
        const activeRecords = []

        for (const pool of pools) {
          const images = pool.images ? pool.images : []
          delete pool.pool
          delete pool.description
          delete pool.expanded
          delete pool.images
          delete pool.area
          
          if (pool.id) {
            await PatologyDetail.query().where('id', pool.id).update(pool)
            poolIds.push(pool.id)
          } else {
            pool.patologies_analysis_pools_id = analisysId
            let detailId = await PatologyDetail.create(pool)
            poolIds.push(detailId.id)
          }

          // Recorro las imágenes recibidas y modifico las existentes o creo nuevas
          for (const image of images) {
            if (image.id) { // Si tiene id ya existía antes entonces modifico el registro
              // return image.id
              await Img.query().where('id', image.id).update(image)
              activeRecords.push(image.id)
            } else { // Si no existía se crea
              image.analisys_id = analisysId
              image.patologies_analysis_detail_id = pool.id
              const record = await Img.create(image)
              activeRecords.push(record.id)
            }
          }
          
        }
        // Eliminar piscinas que ya no están
        await PatologyDetail.query().where('patologies_analysis_pools_id', analisysId).whereNotIn('id', poolIds).delete()
        // Eliminar imágenes de piscinas que ya no están
        await Img.query().where('analisys_id', analisysId).whereNotIn('id', activeRecords).delete()

        response.send(patologiesAnalysis)
      }
    } else {
      response.notFound(validation.messages())
    }
  }

  async destroy ({ params, request, response }) {
    response.send(await Pathology.query().where('id', params.id).delete())
  }
}

module.exports = PathologyController
