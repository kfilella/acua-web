'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const MeasureUnit = use('App/Models/MeasureUnit')
const List = use('App/Functions/List')
const { validate } = use('Validator')
/**
 * Resourceful controller for interacting with measureunits
 */
class MeasureUnitController {
  /**
   * Show a list of all measureunits.
   * GET measureunits
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    let measureUnits = (await MeasureUnit.all()).toJSON()
    measureUnits = List.addRowActions(measureUnits, [{name: 'edit', route: 'measure_units/form/', permission: '4.15'}, {name: 'delete', permission: '4.14'}])
    response.send(measureUnits)
  }

  /**
   * Render a form to be used for creating a new measureunit.
   * GET measureunits/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new measureunit.
   * POST measureunits
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const validation = await validate(request.all(), MeasureUnit.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const body = request.only(MeasureUnit.fillable)
      const measureUnits = await MeasureUnit.create(body)
      response.send(measureUnits)
    }
  }

  /**
   * Display a single measureunit.
   * GET measureunits/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    const measureUnitId = params.id
    const measureUnit = (await MeasureUnit.query().where('id', measureUnitId).first()).toJSON()
    response.send(measureUnit)
  }

  /**
   * Render a form to update an existing measureunit.
   * GET measureunits/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update measureunit details.
   * PUT or PATCH measureunits/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const validation = await validate(request.all(), MeasureUnit.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const body = request.only(MeasureUnit.fillable)
      const measureUnitId = params.id
      const measureUnit = await MeasureUnit.query().where('id', measureUnitId).update(body)
      response.send(measureUnit);
    }
  }

  /**
   * Delete a measureunit with id.
   * DELETE measureunits/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
    response.send(await MeasureUnit.query().where('id', params.id).delete())
  }
}

module.exports = MeasureUnitController
