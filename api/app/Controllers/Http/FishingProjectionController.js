'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with fishingprojections
 */
const typeOptions = [
  {
    id: 1,
    name: 'Directa'
  },
  {
    id: 2,
    name: 'Transferida'
  },
  {
    id: 3,
    name: 'Bifásica'
  },
  {
    id: 4,
    name: 'Trifásica'
  }
]
const moment = require('moment')
const { validate } = use('Validator')
const FishingProjectionDetail = use('App/Models/FishingProjectionDetail')
const FishingProjection = use('App/Models/FishingProjection')
const ExcelJS = require('exceljs')
const User = use('App/Models/User')
const Email = use('App/Functions/Email')
const Helpers = use("Helpers")
const Shrimp = use('App/Models/Shrimp')
const ShrimpPrice = use('App/Models/ShrimpPrice')
const ShrimpPriceFunctions = use('App/Functions/ShrimpPrice')
var shrimpPricesRanges

class FishingProjectionController {
  /**
   * Show a list of all fishingprojections.
   * GET fishingprojections
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response }) {
    const { shrimp_id, pool_id } = request.get()
    let fishingProjections = []
    let query = FishingProjection.query().with('projections').with('technicalAdvisor')
    .with('projections', (query) => {
      query.orderBy('id')
    })

    if (shrimp_id) {
      query = query.with('pool', (query) => {
        query.where('shrimp_id', shrimp_id)
      })
    }
    
    if (pool_id) {
      query = query.where({pool_id})
    }

    fishingProjections = (await query.orderBy('created_at', 'desc').fetch()).toJSON()

    fishingProjections.forEach(element => {
      // element.type = typeOptions.find(val => val.id === element.type).name
      // element.age = moment().diff(moment(element.seedtime), 'days') + 1
      element.seedtime = moment(element.seedtime).format('DD-MM-YYYY')
      element.updated_at = moment(element.updated_at).format('DD-MM-YYYY hh:mm:ss')
      // element.lastReportDate = element.growthReports.length ? moment(element.growthReports[element.growthReports.length - 1].date).format('DD-MM-YYYY') : ''
    })

    response.send(fishingProjections)
  }

  async sendMailInfo ({ request, response, params }) {
    let form_email = {}
    const projectiones = params.projectionId
    const projection =  (await FishingProjection.query().with('pool').with('technicalAdvisor').where('id', projectiones).fetch()).toJSON()
    const shrimps = (await Shrimp.query().with('contact').with('customer').with('assignedContacts').where('id', projection[0].pool.shrimp_id).fetch()).toJSON()
    if (shrimps[0].contact.length > 0) {
      for (let i in shrimps[0].contact) {
        let temp = shrimps[0].contact
        for (let j in temp) {
          let temp2 = temp[j]
          console.log(temp2.email)
          form_email = {
            to: temp2.email ? temp2.email : '',
            contacts_email: temp2.email ? temp2.email : '',
            email_customer: shrimps[0].customer.electronic_documents_email
          }
        }
      }
    } else {
      form_email = {
        to: '',
        contacts_email: '',
        email_customer: shrimps[0].customer.electronic_documents_email
      }
    }
    /* const form_emails = {
      to: analysis.applicant.email,
      contacts_email: analysis.shrimp.customer.contacts.map(val => val.email),
      email_customer: analysis.shrimp.customer.electronic_documents_email
    } */
    response.send(form_email)
  }

  async sendMail ({ request, response, params }) {
    const { to, cc, content } = request.all()
    const analysisId = params.analysisId
    const filename = await generateFishingExcel(analysisId)

    await Email.send({
      to,
      subject: 'Proyección de Pesca',
      content,
      cc,
      attachments: [
        {
          filename,
          path: Helpers.appRoot(`storage/uploads/${filename}`)
        }
      ]
    })
    response.send(true)
  }

  /**
   * Create/save a new fishingprojection.
   * POST fishing_projections
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ auth, request, response }) {
    const logguedUser = await auth.getUser()
    const user = (await User.query().with('technical_advisor').where('id', logguedUser.id).first()).toJSON()
    const validation = await validate(request.all(), FishingProjection.fieldValidationRules())
    const wholeRequest = request.all()

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const body = request.only(FishingProjection.fillable)
      body.technical_advisor_id = user.technical_advisor.id
      const fishingProjection = await FishingProjection.create(body)

      //Guardar detalles de proyección(ellos llaman proyecciones)
      for (const i of wholeRequest.projections) {
        i.fishing_projection_id = fishingProjection.id
        await FishingProjectionDetail.create(i)
      }

      response.send(fishingProjection)
    }
  }

  /**
   * Display a single fishingprojection.
   * GET fishingprojections/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, response }) {
    const fishingProjection = (await FishingProjection.query().with('projections').with('pool')
    .with('projections', (query) => {
      query.orderBy('id')
    })
    .where('id', params.id)
    .first()).toJSON()
    // let age = moment().diff(moment(fishingProjection.seedtime), 'days') + 1
    fishingProjection.report_date = moment(fishingProjection.report_date).format('YYYY-MM-DD')
    fishingProjection.seedtime = moment(fishingProjection.seedtime).format('YYYY-MM-DD')

    /* // Peso de siembra
    const sowing_weight = fishingProjection.sowing_weight

    // Hectareas de la piscina
    const poolHa = fishingProjection.pool.area

    // Tamaño
    const size = fishingProjection.growthReports.length ? fishingProjection.growthReports[fishingProjection.growthReports.length - 1].size : 0 // Tamaño del último reporte registrado, ordenado por ID

    // tamaño de pre-antepenultimo reporte registrado
    const preAntepenultimate = fishingProjection.growthReports.length > 4 ? fishingProjection.growthReports[fishingProjection.growthReports.length - 5].size : 0

    // Crecimiento 4 últimos (Tamaño - ([tamaño de pre-antepenultimo reporte registrado])) / 28 * 7
    const lastFourGrowth = fishingProjection.growthReports.length > 4 ? (size - (preAntepenultimate)) / 28 * 7 : 0

    fishingProjection.lastFourGrowth = lastFourGrowth.toFixed(3)

    if (fishingProjection.shrimpProjection) {
      // Peso de cosecha
      var harvest_weight = fishingProjection.shrimpProjection.harvest_weight

      // Fecha último reporte
      var lastReportDate = moment(fishingProjection.growthReports[fishingProjection.growthReports.length - 1].date)

      // Día de cosecha = Fecha + (((Peso Cosecha - tamaño) / Crecimiento 4 ult) * 7)
      var daysDifference = (((harvest_weight - size) / lastFourGrowth) * 7) // Días a sumar con decimales
      daysDifference = daysDifference - Math.floor(daysDifference) // Guardo los decimales porque la fórmula loca del excel los necesita para que una resta de día de decimales
      var harvestDay = moment(lastReportDate).add((((harvest_weight - size) / lastFourGrowth) * 7), 'days')

      // Días faltan Dia Cosecha - Fecha + 1
      const daysLeft = moment(harvestDay).diff(moment(lastReportDate), 'days') + 1

      // Debe crecer (Peso Cosecha - tamaño) / Días Faltan * 7
      fishingProjection.shrimpProjection.mustGrow = ((harvest_weight - size) / daysLeft * 7).toFixed(2)

      // Libras = Cantidad * (% Sup) * Peso Cosecha / 454
      var pounds = fishingProjection.qty * (fishingProjection.shrimpProjection.percent_sup / 100) * harvest_weight / 454

      // Libras / Hectárea = Libras / Hectareas de la piscina
      const poundsHas = (pounds / poolHa).toFixed()



      // Días totales
      // var totalDays = moment(harvestDay).diff(moment(fishingProjection.seedtime), 'days') // Bueno
      var totalDays = moment(harvestDay).diff(moment(fishingProjection.seedtime), 'days') - (1 - daysDifference)// Invento

      fishingProjection.shrimpProjection.pounds = pounds.toFixed()
      fishingProjection.shrimpProjection.daysLeft = daysLeft
      fishingProjection.shrimpProjection.poundsHas = poundsHas
      fishingProjection.shrimpProjection.harvestDay = moment(harvestDay).subtract(1, 'days').format('DD-MM-YYYY')
      fishingProjection.shrimpProjection.totalDays = totalDays.toFixed()
    }
    if (fishingProjection.costProjection) {
      // Libras raleo
      const pounds_raleo = parseFloat(fishingProjection.costProjection.pounds_raleo)


      // Precio del camarón al crear la proyección
      const preShrimpPrice = fishingProjection.costProjection.shrimp_price

      // Precio actual del camarón
      const currentShrimpPrice = (await ShrimpPrice.query().orderBy('date', 'desc').first()).toJSON().price

      // Venta $/lb = Peso Cosecha * [Precio actual del camaron]
      const preSalePound = harvest_weight * preShrimpPrice
      const salePound = harvest_weight * currentShrimpPrice

      // Costo larva = Costo Juv * Cantidad / 1000
      const larvaCost = fishingProjection.costProjection.cost_juv * fishingProjection.qty / 1000

      // Saldo Kilos x alim = ([Hectareas de la piscina] * Kg/Ha x Alim dia) * (Día Cosecha - [Fecha de ultimo reporte registrado])
      const balanceKilos = (poolHa * fishingProjection.costProjection.kg_ha_food_day).toFixed(2) * (moment(harvestDay).diff(moment(lastReportDate), 'days') - 1 + (daysDifference)) // Ver explicación de daysDifference

      // Total Alim Kg = Kg Acum + Saldo Kilos x alim
      const totalFoodKg = parseFloat(fishingProjection.costProjection.kg_acum) + parseFloat(balanceKilos)

      // Costo Alim = Total Alim Kg * $ Kilo Alim
      const foodCost = (totalFoodKg * fishingProjection.costProjection.price_kilo_food).toFixed()

      // Costo Opera = [Hectareas de la piscina] * (Dias Totales + Días Seco) * costo has / dias
      const operaCost = poolHa * (totalDays + fishingProjection.costProjection.dry_days) * fishingProjection.costProjection.cost_ha_days

      // Costo = (Costo Larva) + (Costo Alim) + (Costo Operativo)
      const cost = parseFloat(larvaCost) + parseFloat(foodCost) + parseFloat(operaCost)

      // Venta raleo = (Peso Raleo * [Precio actual del camaron]) * Lbs Raleo
      const preThinningSale = (fishingProjection.costProjection.weight_raleo * preShrimpPrice) * pounds_raleo
      const thinningSale = (fishingProjection.costProjection.weight_raleo * currentShrimpPrice) * pounds_raleo

      // Venta = (Libras * Venta $/lb) + Venta Raleo
      const preSale = (pounds * preSalePound) + preThinningSale
      const sale = (pounds * salePound) + thinningSale

      // P&G Ganancias y pérdidas = Venta - Costo
      const preEarningLose = preSale - cost
      const earningLose = sale - cost

      // Ut / ha / dia = P&G / Dias Totales / [Hectareas de la piscina]
      const pre_ut_ha_day = (preEarningLose / totalDays / poolHa).toFixed()
      const ut_ha_day = (earningLose / totalDays / poolHa).toFixed()

      // Util / Ha = P&G / [Hectareas de la piscina]
      const pre_util_ha = preEarningLose / poolHa
      const util_ha = earningLose / poolHa

      // Biomasa inicial = (Cantidad * Peso Siembra) / 454
      const initialBiomass = (fishingProjection.qty * sowing_weight) / 454

      // FC = (Total Alim Kg * 2.2046) / (Libras + Lbs Raleo - Biomasa Inicial)
      const fc = (totalFoodKg * 2.2046) / (parseFloat(pounds) + parseFloat(pounds_raleo) - parseFloat(initialBiomass))

      ////////////////////////Proyección de costos con precio que tenía el camarón en ese momento/////////////////////////////
      fishingProjection.costProjection.preSalePound = preSalePound.toFixed(2)
      fishingProjection.costProjection.preCost = cost.toFixed()
      fishingProjection.costProjection.preSale = preSale.toFixed()
      fishingProjection.costProjection.pre_ut_ha_day = pre_ut_ha_day
      fishingProjection.costProjection.preThinningSale = preThinningSale.toFixed(2)
      fishingProjection.costProjection.preEarningLose = preEarningLose.toFixed(2)
      fishingProjection.costProjection.preBalanceKilos = balanceKilos.toFixed()
      fishingProjection.costProjection.pre_util_ha = pre_util_ha.toFixed()
      fishingProjection.costProjection.preTotalFoodKg = totalFoodKg.toFixed()
      fishingProjection.costProjection.preLarvaCost = larvaCost
      fishingProjection.costProjection.preFoodCost = foodCost
      fishingProjection.costProjection.preOperaCost = operaCost.toFixed()
      fishingProjection.costProjection.preFc = fc.toFixed(2)
      fishingProjection.costProjection.preShrimpPrice = preShrimpPrice
      fishingProjection.costProjection.preDate = moment(fishingProjection.costProjection.created_at).format('DD-MM-YYYY')
      ///////////////////////////////////////////////////////////////////////////////////////////////////////////

      ////////////////////////Proyección de costos con precio actual del camarón/////////////////////////////
      fishingProjection.costProjection.salePound = salePound.toFixed(2)
      fishingProjection.costProjection.cost = cost.toFixed()
      fishingProjection.costProjection.sale = sale.toFixed()
      fishingProjection.costProjection.ut_ha_day = ut_ha_day
      fishingProjection.costProjection.thinningSale = thinningSale.toFixed(2)
      fishingProjection.costProjection.earningLose = earningLose.toFixed(2)
      fishingProjection.costProjection.balanceKilos = balanceKilos.toFixed()
      fishingProjection.costProjection.util_ha = util_ha.toFixed()
      fishingProjection.costProjection.totalFoodKg = totalFoodKg.toFixed()
      fishingProjection.costProjection.larvaCost = larvaCost
      fishingProjection.costProjection.foodCost = foodCost
      fishingProjection.costProjection.operaCost = operaCost.toFixed()
      fishingProjection.costProjection.fc = fc.toFixed(2)
      fishingProjection.costProjection.currentShrimpPrice = currentShrimpPrice
      fishingProjection.costProjection.date = moment().format('DD-MM-YYYY')
      ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    } */

    /* let previousSize = 0
    fishingProjection.growthReports.forEach(element => {
      element.growth = (element.size - previousSize).toFixed(3)
      previousSize = element.size

      element.generalGrowth = (((element.size - sowing_weight) / age * 7)).toFixed(3)
      element.date = moment(element.date).format('YYYY-MM-DD')
    })

    // Ordenar los reportes de crecimiento de forma descendente
    fishingProjection.growthReports.sort(function (a, b) {
      if (a.id < b.id) {
        return 1
      }
      if (a.id > b.id) {
        return -1
      }
      // a must be equal to b
      return 0
    }) */
    response.send(fishingProjection)
  }

  /**
   * Update fishingprojection details.
   * PUT or PATCH fishingprojections/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const wholeRequest = request.all()
    const fishingProjection = await FishingProjection.query().where('id', params.id).first()
    if (fishingProjection) {
      const validation = await validate(request.all(), FishingProjection.fieldValidationRules())

      if (validation.fails()) {
        response.unprocessableEntity(validation.messages())
      } else {
        const body = request.only(FishingProjection.fillable)

        await FishingProjection.query().where('id', params.id).update(body)

        // Actualizar detalles
        const activeIds = []
        for (const element of wholeRequest.projections) {
          if (element.id) {
            await FishingProjectionDetail.query().where('id', element.id).update(element)
            activeIds.push(element.id)
          } else {
            element.fishing_projection_id = params.id
            const record = await FishingProjectionDetail.create(element)
            activeIds.push(record.id)
          }
        }

        await FishingProjectionDetail.query().where('fishing_projection_id', params.id).whereNotIn('id', activeIds).delete()

        response.send(fishingProjection)
      }
    } else {
      response.notFound(validation.messages())
    }
  }

  /**
   * Delete a fishingprojection with id.
   * DELETE fishingprojections/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }

  async printFishing ({ params, response }) {
    // const id = params.id
    const filename = await generateFishingExcel(params.id)
    
    response.download(Helpers.appRoot(`storage/uploads/${filename}`))
  }
}
async function getShrimpPriceRanges () {
  const shrimpPricesRanges = ShrimpPriceFunctions.getRanges()
  for (const range of shrimpPricesRanges) {
    const lastPrice = await ShrimpPrice.query().where('range_id', range.id).orderBy('date', 'desc').orderBy('updated_at', 'desc').first()
    range.lastPrice = lastPrice ? lastPrice.price : 0
    range.range_id = lastPrice ? lastPrice.range_id : 0
    range.average_size = lastPrice ? lastPrice.average_size : 1
  }
  return shrimpPricesRanges
}
function calculatePrice (value) {
  //Cálculo de precio de cosecha

  let priceRecord
  if (value >= 8 && value <= 9.5) {
    priceRecord = shrimpPricesRanges.find(val => val.range_id == 1)
  } else if (value >= 10 && value <= 11.5) {
    priceRecord = shrimpPricesRanges.find(val => val.range_id == 2)
  } else if (value >= 12 && value <= 13.5) {
    priceRecord = shrimpPricesRanges.find(val => val.range_id == 3)
  } else if (value >= 14 && value <= 16) {
    priceRecord = shrimpPricesRanges.find(val => val.range_id == 4)
  } else if (value >= 17 && value <= 20) {
    priceRecord = shrimpPricesRanges.find(val => val.range_id == 5)
  } else if (value >= 21 && value <= 25) {
    priceRecord = shrimpPricesRanges.find(val => val.range_id == 6)
  } else if (value >= 26 && value <= 31) {
    priceRecord = shrimpPricesRanges.find(val => val.range_id == 7)
  } else {
    priceRecord = shrimpPricesRanges.find(val => val.range_id == 8)
  }
  // console.log('priceRecord', priceRecord.lastPrice * value)
  const lastPrice = priceRecord ? priceRecord.lastPrice : 0
  const average_size = priceRecord ? priceRecord.average_size : 1
  return ((lastPrice * value / average_size) / 2.2046)
}

async function generateFishingExcel (fishingId) {
  shrimpPricesRanges = await getShrimpPriceRanges()
  const filename = `Pesca_${fishingId}.xlsx`

    const fishingProjection = (await FishingProjection.query().with('projections').with('pool')
    .with('projections', (query) => {
      query.orderBy('id')
    })
    .where('id', fishingId)
    .first()).toJSON()

    const workbook = new ExcelJS.Workbook()
    await workbook.xlsx.readFile('resources/templates/plantilla_reporte_pesca.xlsx').then(function() {
      console.log('cargo archivo')
      const worksheet = workbook.getWorksheet(1)
      worksheet.name = `Pisc ${fishingProjection.pool.description}`

      // Eliminar la pestaña de precios
      const priceSheet = workbook.getWorksheet('$ libra')
      workbook.removeWorksheet(priceSheet.id)

      const letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']

      worksheet.getCell('C7').value = fishingProjection.pool.description
      worksheet.getCell('C8').value = new Date(fishingProjection.report_date)
      worksheet.getCell('C9').value = parseFloat(fishingProjection.pool.area)
      worksheet.getCell('C10').value = new Date(fishingProjection.seedtime)
      worksheet.getCell('C11').value = fishingProjection.density ? parseFloat(fishingProjection.density) : 0
      worksheet.getCell('C12').value = parseFloat(fishingProjection.sowing_weight)
      worksheet.getCell('C14').value = fishingProjection.dry_days ? parseFloat(fishingProjection.dry_days) : 0
      worksheet.getCell('C15').value = fishingProjection.weight ? parseFloat(fishingProjection.weight) : 0
      worksheet.getCell('C16').value = parseFloat(fishingProjection.mp / 100)
      worksheet.getCell('C17').value = fishingProjection.kg_acum ? parseFloat(fishingProjection.kg_acum) : 0
      worksheet.getCell('C18').value = fishingProjection.last_four ? parseFloat(fishingProjection.last_four) : 0
      worksheet.getCell('C20').value = fishingProjection.cur_kg_ha ? parseFloat(fishingProjection.cur_kg_ha) : 0
      worksheet.getCell('C21').model.result = undefined

      let increment = 11
      fishingProjection.projections.forEach((element, key) => {
        console.log({ increment })
        if (key === 0) {
          worksheet.getCell('K6').style =  worksheet.getCell('E6').style

          worksheet.getCell('K6').value = element.thinning ? `Raleo a los ${element.weight} g` : `Sin Raleo a ${element.weight} gramos`
          worksheet.mergeCells(`K6:L6`)
          worksheet.getCell('L6').style =  worksheet.getCell('F6').style

          // Si es con raleo los items serán otros
          if (element.thinning) { // Con raleo
           // Copiar columna de raleo
           // Copiar estilos de labels
           worksheet.getCell('K7').style = worksheet.getCell('H7').style
           worksheet.getCell('K8').style = worksheet.getCell('H8').style
           worksheet.getCell('K9').style = worksheet.getCell('H9').style
           worksheet.getCell('K10').style = worksheet.getCell('H10').style
           worksheet.getCell('K11').style = worksheet.getCell('H11').style
           worksheet.getCell('K12').style = worksheet.getCell('H12').style
           worksheet.getCell('K13').style = worksheet.getCell('H13').style
           worksheet.getCell('K14').style = worksheet.getCell('H14').style
           worksheet.getCell('K15').style = worksheet.getCell('H15').style
           worksheet.getCell('K16').style = worksheet.getCell('H16').style
           worksheet.getCell('K17').style = worksheet.getCell('H17').style
           worksheet.getCell('K18').style = worksheet.getCell('H18').style
           worksheet.getCell('K19').style = worksheet.getCell('H19').style
           worksheet.getCell('K20').style = worksheet.getCell('H20').style
           worksheet.getCell('K21').style = worksheet.getCell('H21').style
           worksheet.getCell('K22').style = worksheet.getCell('H22').style
           worksheet.getCell('K23').style = worksheet.getCell('H23').style
           worksheet.getCell('K24').style = worksheet.getCell('H24').style
           worksheet.getCell('K25').style = worksheet.getCell('H25').style
           worksheet.getCell('K26').style = worksheet.getCell('H26').style
           worksheet.getCell('K27').style = worksheet.getCell('H27').style
           worksheet.getCell('K28').style = worksheet.getCell('H28').style
           worksheet.getCell('K29').style = worksheet.getCell('H29').style
           worksheet.getCell('K30').style = worksheet.getCell('H30').style
           worksheet.getCell('K31').style = worksheet.getCell('H31').style
           worksheet.getCell('K32').style = worksheet.getCell('H32').style
           worksheet.getCell('K33').style = worksheet.getCell('H33').style
           worksheet.getCell('K34').style = worksheet.getCell('H34').style
           worksheet.getCell('K35').style = worksheet.getCell('H35').style
           worksheet.getCell('K36').style = worksheet.getCell('H36').style
           worksheet.getCell('K37').style = worksheet.getCell('H37').style
           worksheet.getCell('K38').style = worksheet.getCell('H38').style
           worksheet.getCell('K39').style = worksheet.getCell('H39').style
           worksheet.getCell('K40').style = worksheet.getCell('H40').style
           worksheet.getCell('K41').style = worksheet.getCell('H41').style
           worksheet.getCell('K42').style = worksheet.getCell('H42').style

           // Copiar labels
           worksheet.getCell('K7').value = 'Fecha Raleo'
           worksheet.getCell('K8').value = 'Libras Raleo'
           worksheet.getCell('K9').value = 'Peso Raleo'
           worksheet.getCell('K10').value = '%Sup Raleo'
           worksheet.getCell('K11').value = 'Lbs/Ha Raleo'
           worksheet.getCell('K12').value = 'Fecha de Cosecha'
           worksheet.getCell('K13').value = 'Libras Cosecha'
           worksheet.getCell('K14').value = 'Peso Cosecha'
           worksheet.getCell('K15').value = '% Sup Cosecha'
           worksheet.getCell('K16').value = '%Sup Total'
           worksheet.getCell('K17').value = 'Peso Prom'
           worksheet.getCell('K18').value = 'Días'
           worksheet.getCell('K19').value = 'Crec'
           worksheet.getCell('K20').value = 'lbs/Ha Cosecha'
           worksheet.getCell('K21').value = 'Libras Totales'
           worksheet.getCell('K22').value = 'lbs/Ha Total'
           worksheet.getCell('K23').value = 'FC'
           worksheet.getCell('K24').value = 'Debe crecer'
           worksheet.getCell('K25').value = 'Días faltan'
           worksheet.getCell('K26').value = 'Kg/Ha/día'
           worksheet.getCell('K27').value = 'Kg x Alim'
           worksheet.getCell('K28').value = 'Kg Totales'
           worksheet.getCell('K29').value = '$ Saco'
           worksheet.getCell('K30').value = '$/Kilo Alim'
           worksheet.getCell('K31').value = 'Costo Alim'
           worksheet.getCell('K32').value = 'Costo Juvenil'
           worksheet.getCell('K33').value = 'Costo Fijo'
           worksheet.getCell('K34').value = 'Costo Total'
           worksheet.getCell('K35').value = 'Precio Prom'
           worksheet.getCell('K36').value = 'Venta'
           worksheet.getCell('K37').value = 'Utilidad'
           worksheet.getCell('K38').value = 'Util/Ha'
           worksheet.getCell('K39').value = 'Util/día/Ha'
           worksheet.getCell('K40').value = 'Costo /lb'
           worksheet.getCell('K41').value = 'margen * lb'
           worksheet.getCell('K42').value = 'ROI'


           // Copiar estilos de datos
           worksheet.getCell('L7').style = worksheet.getCell('I7').style
           worksheet.getCell('L8').style = worksheet.getCell('I8').style
           worksheet.getCell('L9').style = worksheet.getCell('I9').style
           worksheet.getCell('L10').style = worksheet.getCell('I10').style
           worksheet.getCell('L11').style = worksheet.getCell('I11').style
           worksheet.getCell('L12').style = worksheet.getCell('I12').style
           worksheet.getCell('L13').style = worksheet.getCell('I13').style
           worksheet.getCell('L14').style = worksheet.getCell('I14').style
           worksheet.getCell('L15').style = worksheet.getCell('I15').style
           worksheet.getCell('L16').style = worksheet.getCell('I16').style
           worksheet.getCell('L17').style = worksheet.getCell('I17').style
           worksheet.getCell('L18').style = worksheet.getCell('I18').style
           worksheet.getCell('L19').style = worksheet.getCell('I19').style
           worksheet.getCell('L20').style = worksheet.getCell('I20').style
           worksheet.getCell('L21').style = worksheet.getCell('I21').style
           worksheet.getCell('L22').style = worksheet.getCell('I22').style
           worksheet.getCell('L23').style = worksheet.getCell('I23').style
           worksheet.getCell('L24').style = worksheet.getCell('I24').style
           worksheet.getCell('L25').style = worksheet.getCell('I25').style
           worksheet.getCell('L26').style = worksheet.getCell('I26').style
           worksheet.getCell('L27').style = worksheet.getCell('I27').style
           worksheet.getCell('L28').style = worksheet.getCell('I28').style
           worksheet.getCell('L29').style = worksheet.getCell('I29').style
           worksheet.getCell('L30').style = worksheet.getCell('I30').style
           worksheet.getCell('L31').style = worksheet.getCell('I31').style
           worksheet.getCell('L32').style = worksheet.getCell('I32').style
           worksheet.getCell('L33').style = worksheet.getCell('I33').style
           worksheet.getCell('L34').style = worksheet.getCell('I34').style
           worksheet.getCell('L35').style = worksheet.getCell('I35').style
           worksheet.getCell('L36').style = worksheet.getCell('I36').style
           worksheet.getCell('L37').style = worksheet.getCell('I37').style
           worksheet.getCell('L38').style = worksheet.getCell('I38').style
           worksheet.getCell('L39').style = worksheet.getCell('I39').style
           worksheet.getCell('L40').style = worksheet.getCell('I40').style
           worksheet.getCell('L41').style = worksheet.getCell('I41').style
           worksheet.getCell('L42').style = worksheet.getCell('I42').style

           // Copiar datos

           worksheet.getCell('L7').value = { formula: `C8+(L9-C15)/(C18/7)` } // Fecha raleo
           worksheet.getCell('L8').value = { formula: `L11*C9` } // Libras raleo
           worksheet.getCell('L9').value = parseFloat(element.weight_thinning) // Peso raleo
           worksheet.getCell('L10').value = { formula: `(L8*454/L9)/(C11*C9)` } // Sup Raleo
           worksheet.getCell('L11').value = parseFloat(element.pounds_ha_thinning) // Libras ha raleo
           worksheet.getCell('L12').value = { formula: `C8+((L14-C15)/L24*7)` } // Fecha de cosecha
           worksheet.getCell('L13').value = { formula: `L14*L15*C11*C9/454` } // Libras cosecha
           worksheet.getCell('L14').value = parseFloat(element.weight) // Peso cosecha
           worksheet.getCell('L15').value = { formula: `(C16-(L8*454/L9/(C9*C11)))*0.9` } // Sup Cosecha
           worksheet.getCell('L16').value = { formula: `((L13*454/L14)+(L8*454/L9))/(C11*C9)` } // Sup total
           worksheet.getCell('L17').value = { formula: `((L8*454)+(L13*454))/((L8*454/L9)+(L13*454/L14))` } // Peso promedio
           worksheet.getCell('L18').value = { formula: `L12-C10+1` } // Días
           worksheet.getCell('L19').value = { formula: `L14/L18*7` } // Crec
           worksheet.getCell('L20').value = { formula: `L13/C9` } // Libras ha cosecha
           worksheet.getCell('L21').value = { formula: `L13+L8` } // Libras totales
           worksheet.getCell('L22').value = { formula: `(L13+L8)/C9` } // Libras Ha total
           worksheet.getCell('L23').value = { formula: `(L28*2.2046)/(L13+L8)` } // FC
           worksheet.getCell('L24').value = parseFloat(element.must_growth) // Debe crecer
           worksheet.getCell('L25').value = { formula: `L12-C8` } // Días faltan
           worksheet.getCell('L26').value = parseFloat(element.kg_ha_day) // kg ha día
           worksheet.getCell('L27').value = { formula: `((L7-C8)*C20*C9)+(L26*C9*(L12-L7))` } // kg X alim
           worksheet.getCell('L28').value = { formula: `L27+C17` } // kg totales
           worksheet.getCell('L29').value = parseFloat(element.dollar_bag) // $ saco
           worksheet.getCell('L30').value = { formula: `L29/25` } // $kilo alim
           const priceThinning = parseFloat(calculatePrice(element.weight_thinning).toFixed(2))
           worksheet.getCell('L31').value = { formula: `L28*L30` } // Costo alim
           worksheet.getCell('L32').value = parseFloat(element.youth_cost) // Costo juvenil
           worksheet.getCell('L33').value = parseFloat(element.fixed_cost) // Costo fijo
           worksheet.getCell('L34').value = { formula: `((L28*L30)+(C11/1000*L32)+((L18+C14)*L33)*C9)` } // Costo total
           worksheet.getCell('L35').value = { formula: `((L13*${parseFloat(calculatePrice(element.weight).toFixed(2))})+(L8*${priceThinning}))/(L13+L8)` } // Precio prom
           worksheet.getCell('L36').value = { formula: `(((L13*0.95)+(L13*0.05*0.66))*L35)+((L8*0.95)+(L8*0.05*0.66)*${priceThinning})` } // Venta
           worksheet.getCell('L37').value = { formula: `L36-L34` } // Utilidad
           worksheet.getCell('L38').value = { formula: `L37/C9` } // Util/ha
           worksheet.getCell('L39').value = { formula: `L37/L18/C9` } // util/dia/ha
           worksheet.getCell('L40').value = { formula: `L34/L21` } // Costo lb
           worksheet.getCell('L41').value = { formula: `L35-L40` } // margen * lib
           worksheet.getCell('L42').value = { formula: `L37/L34` } // ROI
           // worksheet.getCell('L22').value = { formula:  }


          } else { // Sin raleo
             // Copiar columna de sin raleo
           // Copiar estilos de labels
           worksheet.getCell('K7').style = worksheet.getCell('E7').style
           worksheet.getCell('K8').style = worksheet.getCell('E8').style
           worksheet.getCell('K9').style = worksheet.getCell('E9').style
           worksheet.getCell('K10').style = worksheet.getCell('E10').style
           worksheet.getCell('K11').style = worksheet.getCell('E11').style
           worksheet.getCell('K12').style = worksheet.getCell('E12').style
           worksheet.getCell('K13').style = worksheet.getCell('E13').style
           worksheet.getCell('K14').style = worksheet.getCell('E14').style
           worksheet.getCell('K15').style = worksheet.getCell('E15').style
           worksheet.getCell('K16').style = worksheet.getCell('E16').style
           worksheet.getCell('K17').style = worksheet.getCell('E17').style
           worksheet.getCell('K18').style = worksheet.getCell('E18').style
           worksheet.getCell('K19').style = worksheet.getCell('E19').style
           worksheet.getCell('K20').style = worksheet.getCell('E20').style
           worksheet.getCell('K21').style = worksheet.getCell('E21').style
           worksheet.getCell('K22').style = worksheet.getCell('E22').style
           worksheet.getCell('K23').style = worksheet.getCell('E23').style
           worksheet.getCell('K24').style = worksheet.getCell('E24').style
           worksheet.getCell('K25').style = worksheet.getCell('E25').style
           worksheet.getCell('K26').style = worksheet.getCell('E26').style
           worksheet.getCell('K27').style = worksheet.getCell('E27').style
           worksheet.getCell('K28').style = worksheet.getCell('E28').style
           worksheet.getCell('K29').style = worksheet.getCell('E29').style
           worksheet.getCell('K30').style = worksheet.getCell('E30').style
           worksheet.getCell('K31').style = worksheet.getCell('E31').style
           worksheet.getCell('K32').style = worksheet.getCell('E32').style
           worksheet.getCell('K33').style = worksheet.getCell('E33').style

           // Copiar labels
           worksheet.getCell(`K7`).value = 'Fecha de cosecha'
           worksheet.getCell(`K8`).value = 'Libras de cosecha'
           worksheet.getCell(`K9`).value = 'peso'
           worksheet.getCell(`K10`).value = '% Sup'
           worksheet.getCell(`K11`).value = 'días'
           worksheet.getCell(`K12`).value = 'Crec'
           worksheet.getCell(`K13`).value = 'Lbs/Ha'
           worksheet.getCell(`K14`).value = 'FC'
           worksheet.getCell(`K15`).value = 'Debe crecer'
           worksheet.getCell(`K16`).value = 'Días faltan'
           worksheet.getCell(`K17`).value = 'Kg/Ha/día'
           worksheet.getCell(`K18`).value = 'Kg x Alim'
           worksheet.getCell(`K19`).value = 'Kg Totales'
           worksheet.getCell(`K20`).value = '$ Saco'
           worksheet.getCell(`K21`).value = '$/Kilo Alim'
           worksheet.getCell(`K22`).value = 'Costo Alim'
           worksheet.getCell(`K23`).value = 'Costo Juvenil'
           worksheet.getCell(`K24`).value = 'Costo Fijo'
           worksheet.getCell(`K25`).value = 'Total Costos'
           worksheet.getCell(`K26`).value = 'Precio'
           worksheet.getCell(`K27`).value = 'Venta'
           worksheet.getCell(`K28`).value = 'Utilidad'
           worksheet.getCell(`K29`).value = 'Utilidad/Ha'
           worksheet.getCell(`K30`).value = 'Util/día/Ha'
           worksheet.getCell(`K31`).value = 'Costo /lb'
           worksheet.getCell(`K32`).value = 'margen * lb'
           worksheet.getCell(`K33`).value = 'ROI'

           // EStilos de datos
           worksheet.getCell('L7').style = worksheet.getCell('F7').style
           worksheet.getCell('L8').style = worksheet.getCell('F8').style
           worksheet.getCell('L9').style = worksheet.getCell('F9').style
           worksheet.getCell('L10').style = worksheet.getCell('F10').style
           worksheet.getCell('L11').style = worksheet.getCell('F11').style
           worksheet.getCell('L12').style = worksheet.getCell('F12').style
           worksheet.getCell('L13').style = worksheet.getCell('F13').style
           worksheet.getCell('L14').style = worksheet.getCell('F14').style
           worksheet.getCell('L15').style = worksheet.getCell('F15').style
           worksheet.getCell('L16').style = worksheet.getCell('F16').style
           worksheet.getCell('L17').style = worksheet.getCell('F17').style
           worksheet.getCell('L18').style = worksheet.getCell('F18').style
           worksheet.getCell('L19').style = worksheet.getCell('F19').style
           worksheet.getCell('L20').style = worksheet.getCell('F20').style
           worksheet.getCell('L21').style = worksheet.getCell('F21').style
           worksheet.getCell('L22').style = worksheet.getCell('F22').style
           worksheet.getCell('L23').style = worksheet.getCell('F23').style
           worksheet.getCell('L24').style = worksheet.getCell('F24').style
           worksheet.getCell('L25').style = worksheet.getCell('F25').style
           worksheet.getCell('L26').style = worksheet.getCell('F26').style
           worksheet.getCell('L27').style = worksheet.getCell('F27').style
           worksheet.getCell('L28').style = worksheet.getCell('F28').style
           worksheet.getCell('L29').style = worksheet.getCell('F29').style
           worksheet.getCell('L30').style = worksheet.getCell('F30').style
           worksheet.getCell('L31').style = worksheet.getCell('F31').style
           worksheet.getCell('L32').style = worksheet.getCell('F32').style
           worksheet.getCell('L33').style = worksheet.getCell('F33').style



            // Datos
            // B5 = C9
            // F7 = L7
            // F9 = L9
            // F11 = L15
            // F12 = L10
            // F13 = L17
            // F16 = L20
            // F18 = L11
            // F19 = L16
            // F20 = L18
            // F21 = L19
            // I8 = L8
            // I9 = L9
            worksheet.getCell('L7').value = { formula: `C8+(((L9-C15)/L15)*7)` } // Fecha de cosecha
            worksheet.getCell('L8').value = { formula: `C9*C11*L10*L9/454` } // Libras de cosecha
            worksheet.getCell('L9').value = parseFloat(element.weight) // Peso
            worksheet.getCell('L10').value = parseFloat(element.survival / 100) // Supervivencia
            worksheet.getCell('L11').value = { formula: `L7-C10+1` } // Días
            worksheet.getCell('L12').value = { formula: `(L9-C12)/L11*7` } // Crecimiento
            worksheet.getCell('L13').value = { formula: `L8/C9` } // Libras/Ha
            worksheet.getCell('L14').value = { formula: `(L19*2.2046)/(L8-((C11*C12*C9)/454))` } // FC
            worksheet.getCell('L15').value = parseFloat(element.must_growth) // Debe crecer
            worksheet.getCell('L16').value = { formula: `L7-C8` } // Días faltan
            worksheet.getCell('L17').value = parseFloat(element.kg_ha_day) // Kg ha día
            worksheet.getCell('L18').value = { formula: `L16*L17*C9` } // Kilos por alimento
            worksheet.getCell('L19').value = { formula: `L18+C17` } // Total kilos alim
            worksheet.getCell('L20').value = parseFloat(element.dollar_bag) // Dólar saco
            worksheet.getCell('L21').value = { formula: `L20/25` } // $/Kilo alim
            worksheet.getCell(`L22`).value = { formula: `L21*L19` } // Costo alim
            worksheet.getCell('L23').value = parseFloat(element.youth_cost) // Costo juvenil
            worksheet.getCell('L24').value = parseFloat(element.fixed_cost) // Costo fijo
            worksheet.getCell('L25').value = { formula: `(((((L7-C8)*L17*C9)+C17)*(L20/25))+(C11/1000*L23)+(((L7-C10+1)+C14)*L24)*C9)` } // Total de costos
            worksheet.getCell('L26').value = parseFloat(calculatePrice(element.weight).toFixed(2)) // Precio
            worksheet.getCell('L27').value = { formula: `((L8*0.95)+(L8*0.05*0.66))*L26` } // Venta
            worksheet.getCell('L28').value = { formula: `L27-L25` } // Utilidad
            worksheet.getCell('L29').value = { formula: `L28/C9` } // Utilidad / Ha
            worksheet.getCell('L30').value = { formula: `L28/(L7-C10+1)/C9` } // Utilidad / Dia / Ha
            worksheet.getCell('L31').value = { formula: `L25/L8` } // Costo/Lb
            worksheet.getCell('L32').value = { formula: `L26-L31` } // Margen*lib
            worksheet.getCell('L33').value = { formula: `L28/L25` } // ROI
          }



        } else {
          worksheet.getCell(`${letters[increment]}6`).style = worksheet.getCell(`E6`).style
          worksheet.getCell(`${letters[increment]}6`).value = element.thinning ? `Raleo a los ${element.weight} g` : `Sin Raleo a ${element.weight} gramos`
          worksheet.getColumn(`${letters[increment]}`).width = 16.14
          worksheet.getColumn(`${letters[increment + 1]}`).width = 8.57

          worksheet.mergeCells(`${letters[increment]}6:${letters[increment + 1]}6`)
          worksheet.getCell(`${letters[increment + 1]}6`).style = worksheet.getCell(`F6`).style
          if (element.thinning) { // Con raleo
            // Copiar columna de raleo
           // Copiar estilos de labels
           worksheet.getCell(`${letters[increment]}7`).style = worksheet.getCell('H7').style
           worksheet.getCell(`${letters[increment]}8`).style = worksheet.getCell('H8').style
           worksheet.getCell(`${letters[increment]}9`).style = worksheet.getCell('H9').style
           worksheet.getCell(`${letters[increment]}10`).style = worksheet.getCell('H10').style
           worksheet.getCell(`${letters[increment]}11`).style = worksheet.getCell('H11').style
           worksheet.getCell(`${letters[increment]}12`).style = worksheet.getCell('H12').style
           worksheet.getCell(`${letters[increment]}13`).style = worksheet.getCell('H13').style
           worksheet.getCell(`${letters[increment]}14`).style = worksheet.getCell('H14').style
           worksheet.getCell(`${letters[increment]}15`).style = worksheet.getCell('H15').style
           worksheet.getCell(`${letters[increment]}16`).style = worksheet.getCell('H16').style
           worksheet.getCell(`${letters[increment]}17`).style = worksheet.getCell('H17').style
           worksheet.getCell(`${letters[increment]}18`).style = worksheet.getCell('H18').style
           worksheet.getCell(`${letters[increment]}19`).style = worksheet.getCell('H19').style
           worksheet.getCell(`${letters[increment]}20`).style = worksheet.getCell('H20').style
           worksheet.getCell(`${letters[increment]}21`).style = worksheet.getCell('H21').style
           worksheet.getCell(`${letters[increment]}22`).style = worksheet.getCell('H22').style
           worksheet.getCell(`${letters[increment]}23`).style = worksheet.getCell('H23').style
           worksheet.getCell(`${letters[increment]}24`).style = worksheet.getCell('H24').style
           worksheet.getCell(`${letters[increment]}25`).style = worksheet.getCell('H25').style
           worksheet.getCell(`${letters[increment]}26`).style = worksheet.getCell('H26').style
           worksheet.getCell(`${letters[increment]}27`).style = worksheet.getCell('H27').style
           worksheet.getCell(`${letters[increment]}28`).style = worksheet.getCell('H28').style
           worksheet.getCell(`${letters[increment]}29`).style = worksheet.getCell('H29').style
           worksheet.getCell(`${letters[increment]}20`).style = worksheet.getCell('H30').style
           worksheet.getCell(`${letters[increment]}31`).style = worksheet.getCell('H31').style
           worksheet.getCell(`${letters[increment]}32`).style = worksheet.getCell('H32').style
           worksheet.getCell(`${letters[increment]}33`).style = worksheet.getCell('H33').style
           worksheet.getCell(`${letters[increment]}34`).style = worksheet.getCell('H34').style
           worksheet.getCell(`${letters[increment]}35`).style = worksheet.getCell('H35').style
           worksheet.getCell(`${letters[increment]}36`).style = worksheet.getCell('H36').style
           worksheet.getCell(`${letters[increment]}37`).style = worksheet.getCell('H37').style
           worksheet.getCell(`${letters[increment]}38`).style = worksheet.getCell('H38').style
           worksheet.getCell(`${letters[increment]}39`).style = worksheet.getCell('H39').style
           worksheet.getCell(`${letters[increment]}40`).style = worksheet.getCell('H40').style
           worksheet.getCell(`${letters[increment]}41`).style = worksheet.getCell('H41').style
           worksheet.getCell(`${letters[increment]}42`).style = worksheet.getCell('H42').style

           // Copiar labels
           worksheet.getCell(`${letters[increment]}7`).value = 'Fecha Raleo'
           worksheet.getCell(`${letters[increment]}8`).value = 'Libras Raleo'
           worksheet.getCell(`${letters[increment]}9`).value = 'Peso Raleo'
           worksheet.getCell(`${letters[increment]}10`).value = '%Sup Raleo'
           worksheet.getCell(`${letters[increment]}11`).value = 'Lbs/Ha Raleo'
           worksheet.getCell(`${letters[increment]}12`).value = 'Fecha de Cosecha'
           worksheet.getCell(`${letters[increment]}13`).value = 'Libras Cosecha'
           worksheet.getCell(`${letters[increment]}14`).value = 'Peso Cosecha'
           worksheet.getCell(`${letters[increment]}15`).value = '% Sup Cosecha'
           worksheet.getCell(`${letters[increment]}16`).value = '%Sup Total'
           worksheet.getCell(`${letters[increment]}17`).value = 'Peso Prom'
           worksheet.getCell(`${letters[increment]}18`).value = 'Días'
           worksheet.getCell(`${letters[increment]}19`).value = 'Crec'
           worksheet.getCell(`${letters[increment]}20`).value = 'lbs/Ha Cosecha'
           worksheet.getCell(`${letters[increment]}21`).value = 'Libras Totales'
           worksheet.getCell(`${letters[increment]}22`).value = 'lbs/Ha Total'
           worksheet.getCell(`${letters[increment]}23`).value = 'FC'
           worksheet.getCell(`${letters[increment]}24`).value = 'Debe crecer'
           worksheet.getCell(`${letters[increment]}25`).value = 'Días faltan'
           worksheet.getCell(`${letters[increment]}26`).value = 'Kg/Ha/día'
           worksheet.getCell(`${letters[increment]}27`).value = 'Kg x Alim'
           worksheet.getCell(`${letters[increment]}28`).value = 'Kg Totales'
           worksheet.getCell(`${letters[increment]}29`).value = '$ Saco'
           worksheet.getCell(`${letters[increment]}30`).value = '$/Kilo Alim'
           worksheet.getCell(`${letters[increment]}31`).value = 'Costo Alim'
           worksheet.getCell(`${letters[increment]}32`).value = 'Costo Juvenil'
           worksheet.getCell(`${letters[increment]}33`).value = 'Costo Fijo'
           worksheet.getCell(`${letters[increment]}34`).value = 'Costo Total'
           worksheet.getCell(`${letters[increment]}35`).value = 'Precio Prom'
           worksheet.getCell(`${letters[increment]}36`).value = 'Venta'
           worksheet.getCell(`${letters[increment]}37`).value = 'Utilidad'
           worksheet.getCell(`${letters[increment]}38`).value = 'Util/Ha'
           worksheet.getCell(`${letters[increment]}39`).value = 'Util/día/Ha'
           worksheet.getCell(`${letters[increment]}40`).value = 'Costo /lb'
           worksheet.getCell(`${letters[increment]}41`).value = 'margen * lb'
           worksheet.getCell(`${letters[increment]}42`).value = 'ROI'

            // Copiar estilos de datos
            worksheet.getCell(`${letters[increment + 1]}7`).style = worksheet.getCell('I7').style
            worksheet.getCell(`${letters[increment + 1]}8`).style = worksheet.getCell('I8').style
            worksheet.getCell(`${letters[increment + 1]}9`).style = worksheet.getCell('I9').style
            worksheet.getCell(`${letters[increment + 1]}10`).style = worksheet.getCell('I10').style
            worksheet.getCell(`${letters[increment + 1]}11`).style = worksheet.getCell('I11').style
            worksheet.getCell(`${letters[increment + 1]}12`).style = worksheet.getCell('I12').style
            worksheet.getCell(`${letters[increment + 1]}13`).style = worksheet.getCell('I13').style
            worksheet.getCell(`${letters[increment + 1]}14`).style = worksheet.getCell('I14').style
            worksheet.getCell(`${letters[increment + 1]}15`).style = worksheet.getCell('I15').style
            worksheet.getCell(`${letters[increment + 1]}16`).style = worksheet.getCell('I16').style
            worksheet.getCell(`${letters[increment + 1]}17`).style = worksheet.getCell('I17').style
            worksheet.getCell(`${letters[increment + 1]}18`).style = worksheet.getCell('I18').style
            worksheet.getCell(`${letters[increment + 1]}19`).style = worksheet.getCell('I19').style
            worksheet.getCell(`${letters[increment + 1]}20`).style = worksheet.getCell('I20').style
            worksheet.getCell(`${letters[increment + 1]}21`).style = worksheet.getCell('I21').style
            worksheet.getCell(`${letters[increment + 1]}22`).style = worksheet.getCell('I22').style
            worksheet.getCell(`${letters[increment + 1]}23`).style = worksheet.getCell('I23').style
            worksheet.getCell(`${letters[increment + 1]}24`).style = worksheet.getCell('I24').style
            worksheet.getCell(`${letters[increment + 1]}25`).style = worksheet.getCell('I25').style
            worksheet.getCell(`${letters[increment + 1]}26`).style = worksheet.getCell('I26').style
            worksheet.getCell(`${letters[increment + 1]}27`).style = worksheet.getCell('I27').style
            worksheet.getCell(`${letters[increment + 1]}28`).style = worksheet.getCell('I28').style
            worksheet.getCell(`${letters[increment + 1]}29`).style = worksheet.getCell('I29').style
            worksheet.getCell(`${letters[increment + 1]}30`).style = worksheet.getCell('I30').style
            worksheet.getCell(`${letters[increment + 1]}31`).style = worksheet.getCell('I31').style
            worksheet.getCell(`${letters[increment + 1]}32`).style = worksheet.getCell('I32').style
            worksheet.getCell(`${letters[increment + 1]}33`).style = worksheet.getCell('I33').style
            worksheet.getCell(`${letters[increment + 1]}34`).style = worksheet.getCell('I34').style
            worksheet.getCell(`${letters[increment + 1]}35`).style = worksheet.getCell('I35').style
            worksheet.getCell(`${letters[increment + 1]}36`).style = worksheet.getCell('I36').style
            worksheet.getCell(`${letters[increment + 1]}37`).style = worksheet.getCell('I37').style
            worksheet.getCell(`${letters[increment + 1]}38`).style = worksheet.getCell('I38').style
            worksheet.getCell(`${letters[increment + 1]}39`).style = worksheet.getCell('I39').style
            worksheet.getCell(`${letters[increment + 1]}40`).style = worksheet.getCell('I40').style
            worksheet.getCell(`${letters[increment + 1]}41`).style = worksheet.getCell('I41').style
            worksheet.getCell(`${letters[increment + 1]}42`).style = worksheet.getCell('I42').style


          // Datos
          const priceThinning = parseFloat(calculatePrice(element.weight_thinning).toFixed(2))
            worksheet.getCell(`${letters[increment + 1]}7`).value = { formula: `C8+(${letters[increment + 1]}9-C15)/(C18/7)` } // Fecha raleo
           worksheet.getCell(`${letters[increment + 1]}8`).value = { formula: `${letters[increment + 1]}11*C9` } // Libras raleo
           worksheet.getCell(`${letters[increment + 1]}9`).value = parseFloat(element.weight_thinning) // Peso raleo
           worksheet.getCell(`${letters[increment + 1]}10`).value = { formula: `(${letters[increment + 1]}8*454/${letters[increment + 1]}9)/(C11*C9)` } // Sup Raleo
           worksheet.getCell(`${letters[increment + 1]}11`).value = parseFloat(element.pounds_ha_thinning) // Libras ha raleo
           worksheet.getCell(`${letters[increment + 1]}12`).value = { formula: `C8+((${letters[increment + 1]}14-C15)/${letters[increment + 1]}24*7)` } // Fecha de cosecha
           worksheet.getCell(`${letters[increment + 1]}13`).value = { formula: `${letters[increment + 1]}14*${letters[increment + 1]}15*C11*C9/454` } // Libras cosecha
           worksheet.getCell(`${letters[increment + 1]}14`).value = parseFloat(element.weight) // Peso cosecha
           worksheet.getCell(`${letters[increment + 1]}15`).value = { formula: `(C16-(${letters[increment + 1]}8*454/${letters[increment + 1]}9/(C9*C11)))*0.9` } // Sup Cosecha
           worksheet.getCell(`${letters[increment + 1]}16`).value = { formula: `((${letters[increment + 1]}13*454/${letters[increment + 1]}14)+(${letters[increment + 1]}8*454/${letters[increment + 1]}9))/(C11*C9)` } // Sup total
           worksheet.getCell(`${letters[increment + 1]}17`).value = { formula: `((${letters[increment + 1]}8*454)+(${letters[increment + 1]}13*454))/((${letters[increment + 1]}8*454/${letters[increment + 1]}9)+(${letters[increment + 1]}13*454/${letters[increment + 1]}14))` } // Peso promedio
           worksheet.getCell(`${letters[increment + 1]}18`).value = { formula: `${letters[increment + 1]}12-C10+1` } // Días
           worksheet.getCell(`${letters[increment + 1]}19`).value = { formula: `${letters[increment + 1]}14/${letters[increment + 1]}18*7` } // Crec
           worksheet.getCell(`${letters[increment + 1]}20`).value = { formula: `${letters[increment + 1]}13/C9` } // Libras ha cosecha
           worksheet.getCell(`${letters[increment + 1]}21`).value = { formula: `${letters[increment + 1]}13+${letters[increment + 1]}8` } // Libras totales
           worksheet.getCell(`${letters[increment + 1]}22`).value = { formula: `(${letters[increment + 1]}13+${letters[increment + 1]}8)/C9` } // Libras Ha total
           worksheet.getCell(`${letters[increment + 1]}23`).value = { formula: `(${letters[increment + 1]}28*2.2046)/(${letters[increment + 1]}13+${letters[increment + 1]}8)` } // FC
           worksheet.getCell(`${letters[increment + 1]}24`).value = parseFloat(element.must_growth) // Debe crecer
           worksheet.getCell(`${letters[increment + 1]}25`).value = { formula: `${letters[increment + 1]}12-C8` } // Días faltan
           worksheet.getCell(`${letters[increment + 1]}26`).value = parseFloat(element.kg_ha_day) // kg ha día
           worksheet.getCell(`${letters[increment + 1]}27`).value = { formula: `((${letters[increment + 1]}7-C8)*C20*C9)+(${letters[increment + 1]}26*C9*(${letters[increment + 1]}12-${letters[increment + 1]}7))` } // kg X alim
           worksheet.getCell(`${letters[increment + 1]}28`).value = { formula: `${letters[increment + 1]}27+C17` } // kg totales
           worksheet.getCell(`${letters[increment + 1]}29`).value = parseFloat(element.dollar_bag) // $ saco
           worksheet.getCell(`${letters[increment + 1]}30`).value = { formula: `${letters[increment + 1]}29/25` } // $kilo alim
           worksheet.getCell(`${letters[increment + 1]}31`).value = { formula: `${letters[increment + 1]}28*${letters[increment + 1]}30` } // Costo alim
           worksheet.getCell(`${letters[increment + 1]}32`).value = parseFloat(element.youth_cost) // Costo juvenil
           worksheet.getCell(`${letters[increment + 1]}33`).value = parseFloat(element.fixed_cost) // Costo fijo
           worksheet.getCell(`${letters[increment + 1]}34`).value = { formula: `((${letters[increment + 1]}28*${letters[increment + 1]}30)+(C11/1000*${letters[increment + 1]}32)+((${letters[increment + 1]}18+C14)*${letters[increment + 1]}33)*C9)` } // Costo total
           worksheet.getCell(`${letters[increment + 1]}35`).value = { formula: `((${letters[increment + 1]}13*${parseFloat(calculatePrice(element.weight).toFixed(2))})+(${letters[increment + 1]}8*${priceThinning}))/(${letters[increment + 1]}13+${letters[increment + 1]}8)` } // Precio prom
           worksheet.getCell(`${letters[increment + 1]}36`).value = { formula: `(((${letters[increment + 1]}13*0.95)+(${letters[increment + 1]}13*0.05*0.66))*${letters[increment + 1]}35)+((${letters[increment + 1]}8*0.95)+(${letters[increment + 1]}8*0.05*0.66)*${priceThinning})` } // Venta
           worksheet.getCell(`${letters[increment + 1]}37`).value = { formula: `${letters[increment + 1]}36-${letters[increment + 1]}34` } // Utilidad
           worksheet.getCell(`${letters[increment + 1]}38`).value = { formula: `${letters[increment + 1]}37/C9` } // Util/ha
           worksheet.getCell(`${letters[increment + 1]}39`).value = { formula: `${letters[increment + 1]}37/${letters[increment + 1]}18/C9` } // util/dia/ha
           worksheet.getCell(`${letters[increment + 1]}40`).value = { formula: `${letters[increment + 1]}34/${letters[increment + 1]}21` } // Costo lb
           worksheet.getCell(`${letters[increment + 1]}41`).value = { formula: `${letters[increment + 1]}35-${letters[increment + 1]}40` } // margen * lib
           worksheet.getCell(`${letters[increment + 1]}42`).value = { formula: `${letters[increment + 1]}37/${letters[increment + 1]}34` } // ROI

          } else {

          worksheet.getCell(`${letters[increment]}7`).style = worksheet.getCell(`E7`).style
          worksheet.getCell(`${letters[increment]}8`).style = worksheet.getCell(`E8`).style
          worksheet.getCell(`${letters[increment]}9`).style = worksheet.getCell(`E9`).style
          worksheet.getCell(`${letters[increment]}10`).style = worksheet.getCell(`E10`).style
          worksheet.getCell(`${letters[increment]}11`).style = worksheet.getCell(`E11`).style
          worksheet.getCell(`${letters[increment]}12`).style = worksheet.getCell(`E12`).style
          worksheet.getCell(`${letters[increment]}13`).style = worksheet.getCell(`E13`).style
          worksheet.getCell(`${letters[increment]}14`).style = worksheet.getCell(`E14`).style
          worksheet.getCell(`${letters[increment]}15`).style = worksheet.getCell(`E15`).style
          worksheet.getCell(`${letters[increment]}16`).style = worksheet.getCell(`E16`).style
          worksheet.getCell(`${letters[increment]}17`).style = worksheet.getCell(`E17`).style
          worksheet.getCell(`${letters[increment]}18`).style = worksheet.getCell(`E18`).style
          worksheet.getCell(`${letters[increment]}19`).style = worksheet.getCell(`E19`).style
          worksheet.getCell(`${letters[increment]}20`).style = worksheet.getCell(`E20`).style
          worksheet.getCell(`${letters[increment]}21`).style = worksheet.getCell(`E21`).style
          worksheet.getCell(`${letters[increment]}22`).style = worksheet.getCell(`E22`).style
          worksheet.getCell(`${letters[increment]}23`).style = worksheet.getCell(`E23`).style
          worksheet.getCell(`${letters[increment]}24`).style = worksheet.getCell(`E24`).style
          worksheet.getCell(`${letters[increment]}25`).style = worksheet.getCell(`E25`).style
          worksheet.getCell(`${letters[increment]}26`).style = worksheet.getCell(`E26`).style
          worksheet.getCell(`${letters[increment]}27`).style = worksheet.getCell(`E27`).style
          worksheet.getCell(`${letters[increment]}28`).style = worksheet.getCell(`E28`).style
          worksheet.getCell(`${letters[increment]}29`).style = worksheet.getCell(`E29`).style
          worksheet.getCell(`${letters[increment]}30`).style = worksheet.getCell(`E30`).style
          worksheet.getCell(`${letters[increment]}31`).style = worksheet.getCell(`E31`).style
          worksheet.getCell(`${letters[increment]}32`).style = worksheet.getCell(`E32`).style
          worksheet.getCell(`${letters[increment]}33`).style = worksheet.getCell(`E33`).style

          worksheet.getCell(`${letters[increment]}7`).value = 'Fecha de cosecha'
          worksheet.getCell(`${letters[increment]}8`).value = 'Libras de cosecha'
          worksheet.getCell(`${letters[increment]}9`).value = 'peso'
          worksheet.getCell(`${letters[increment]}10`).value = '% Sup'
          worksheet.getCell(`${letters[increment]}11`).value = 'días'
          worksheet.getCell(`${letters[increment]}12`).value = 'Crec'
          worksheet.getCell(`${letters[increment]}13`).value = 'Lbs/Ha'
          worksheet.getCell(`${letters[increment]}14`).value = 'FC'
          worksheet.getCell(`${letters[increment]}15`).value = 'Debe crecer'
          worksheet.getCell(`${letters[increment]}16`).value = 'Días faltan'
          worksheet.getCell(`${letters[increment]}17`).value = 'Kg/Ha/día'
          worksheet.getCell(`${letters[increment]}18`).value = 'Kg x Alim'
          worksheet.getCell(`${letters[increment]}19`).value = 'Kg Totales'
          worksheet.getCell(`${letters[increment]}20`).value = '$ Saco'
          worksheet.getCell(`${letters[increment]}21`).value = '$/Kilo Alim'
          worksheet.getCell(`${letters[increment]}22`).value = 'Costo Alim'
          worksheet.getCell(`${letters[increment]}23`).value = 'Costo Juvenil'
          worksheet.getCell(`${letters[increment]}24`).value = 'Costo Fijo'
          worksheet.getCell(`${letters[increment]}25`).value = 'Total Costos'
          worksheet.getCell(`${letters[increment]}26`).value = 'Precio'
          worksheet.getCell(`${letters[increment]}27`).value = 'Venta'
          worksheet.getCell(`${letters[increment]}28`).value = 'Utilidad'
          worksheet.getCell(`${letters[increment]}29`).value = 'Utilidad/Ha'
          worksheet.getCell(`${letters[increment]}30`).value = 'Util/día/Ha'
          worksheet.getCell(`${letters[increment]}31`).value = 'Costo /lb'
          worksheet.getCell(`${letters[increment]}32`).value = 'margen * lb'
          worksheet.getCell(`${letters[increment]}33`).value = 'ROI'

          worksheet.getCell(`${letters[increment + 1]}7`).style = worksheet.getCell(`F7`).style
          worksheet.getCell(`${letters[increment + 1]}8`).style = worksheet.getCell(`F8`).style
          worksheet.getCell(`${letters[increment + 1]}9`).style = worksheet.getCell(`F9`).style
          worksheet.getCell(`${letters[increment + 1]}10`).style = worksheet.getCell(`F10`).style
          worksheet.getCell(`${letters[increment + 1]}11`).style = worksheet.getCell(`F11`).style
          worksheet.getCell(`${letters[increment + 1]}12`).style = worksheet.getCell(`F12`).style
          worksheet.getCell(`${letters[increment + 1]}13`).style = worksheet.getCell(`F13`).style
          worksheet.getCell(`${letters[increment + 1]}14`).style = worksheet.getCell(`F14`).style
          worksheet.getCell(`${letters[increment + 1]}15`).style = worksheet.getCell(`F15`).style
          worksheet.getCell(`${letters[increment + 1]}16`).style = worksheet.getCell(`F16`).style
          worksheet.getCell(`${letters[increment + 1]}17`).style = worksheet.getCell(`F17`).style
          worksheet.getCell(`${letters[increment + 1]}18`).style = worksheet.getCell(`F18`).style
          worksheet.getCell(`${letters[increment + 1]}19`).style = worksheet.getCell(`F19`).style
          worksheet.getCell(`${letters[increment + 1]}20`).style = worksheet.getCell(`F20`).style
          worksheet.getCell(`${letters[increment + 1]}21`).style = worksheet.getCell(`F21`).style
          worksheet.getCell(`${letters[increment + 1]}22`).style = worksheet.getCell(`F22`).style
          worksheet.getCell(`${letters[increment + 1]}23`).style = worksheet.getCell(`F23`).style
          worksheet.getCell(`${letters[increment + 1]}24`).style = worksheet.getCell(`F24`).style
          worksheet.getCell(`${letters[increment + 1]}25`).style = worksheet.getCell(`F25`).style
          worksheet.getCell(`${letters[increment + 1]}26`).style = worksheet.getCell(`F26`).style
          worksheet.getCell(`${letters[increment + 1]}27`).style = worksheet.getCell(`F27`).style
          worksheet.getCell(`${letters[increment + 1]}28`).style = worksheet.getCell(`F28`).style
          worksheet.getCell(`${letters[increment + 1]}29`).style = worksheet.getCell(`F29`).style
          worksheet.getCell(`${letters[increment + 1]}30`).style = worksheet.getCell(`F30`).style
          worksheet.getCell(`${letters[increment + 1]}31`).style = worksheet.getCell(`F31`).style
          worksheet.getCell(`${letters[increment + 1]}32`).style = worksheet.getCell(`F32`).style
          worksheet.getCell(`${letters[increment + 1]}33`).style = worksheet.getCell(`F33`).style

         // console.log(`${letters[increment + 1]}7`, '-', `${letters[increment - 2]}7`)


          // Datos
          worksheet.getCell(`${letters[increment + 1]}7`).value = { formula: `C8+(((${letters[increment + 1]}9-C15)/${letters[increment + 1]}15)*7)` } // Fecha de cosecha
          worksheet.getCell(`${letters[increment + 1]}8`).value = { formula: `C9*C11*${letters[increment + 1]}10*${letters[increment + 1]}9/454` } // ${letters[increment + 1]}ibras de cosecha
          worksheet.getCell(`${letters[increment + 1]}9`).value = parseFloat(element.weight) // Peso
          worksheet.getCell(`${letters[increment + 1]}10`).value = parseFloat(element.survival / 100) // Supervivencia
          worksheet.getCell(`${letters[increment + 1]}11`).value = { formula: `${letters[increment + 1]}7-C10+1` } // Días
          worksheet.getCell(`${letters[increment + 1]}12`).value = { formula: `(${letters[increment + 1]}9-C12)/${letters[increment + 1]}11*7` } // Crecimiento
          worksheet.getCell(`${letters[increment + 1]}13`).value = { formula: `${letters[increment + 1]}8/C9` } // ${letters[increment + 1]}ibras/Ha
          worksheet.getCell(`${letters[increment + 1]}14`).value = { formula: `(${letters[increment + 1]}19*2.2046)/(${letters[increment + 1]}8-((C11*C12*C9)/454))` } // FC
          worksheet.getCell(`${letters[increment + 1]}15`).value = parseFloat(element.must_growth) // Debe crecer
          worksheet.getCell(`${letters[increment + 1]}16`).value = { formula: `${letters[increment + 1]}7-C8` } // Días faltan
          worksheet.getCell(`${letters[increment + 1]}17`).value = parseFloat(element.kg_ha_day) // Kg ha día
          worksheet.getCell(`${letters[increment + 1]}18`).value = { formula: `${letters[increment + 1]}16*${letters[increment + 1]}17*C9` } // Kilos por alimento
          worksheet.getCell(`${letters[increment + 1]}19`).value = { formula: `${letters[increment + 1]}18+C17` } // Total kilos alim
          worksheet.getCell(`${letters[increment + 1]}20`).value = parseFloat(element.dollar_bag) // Dólar saco
          worksheet.getCell(`${letters[increment + 1]}21`).value = { formula: `${letters[increment + 1]}20/25` } // $/Kilo alim
          worksheet.getCell(`${letters[increment + 1]}22`).value = { formula: `${letters[increment + 1]}21*${letters[increment + 1]}19` } // Costo alim
          worksheet.getCell(`${letters[increment + 1]}23`).value = parseFloat(element.youth_cost) // Costo juvenil
          worksheet.getCell(`${letters[increment + 1]}24`).value = parseFloat(element.fixed_cost) // Costo fijo
          worksheet.getCell(`${letters[increment + 1]}25`).value = { formula: `(((((${letters[increment + 1]}7-C8)*${letters[increment + 1]}17*C9)+C17)*(${letters[increment + 1]}20/25))+(C11/1000*${letters[increment + 1]}23)+(((${letters[increment + 1]}7-C10+1)+C14)*${letters[increment + 1]}24)*C9)` } // Total de costos
          worksheet.getCell(`${letters[increment + 1]}26`).value = parseFloat(calculatePrice(element.weight).toFixed(2)) // Precio
          worksheet.getCell(`${letters[increment + 1]}27`).value = { formula: `((${letters[increment + 1]}8*0.95)+(${letters[increment + 1]}8*0.05*0.66))*${letters[increment + 1]}26` } // Venta
          worksheet.getCell(`${letters[increment + 1]}28`).value = { formula: `${letters[increment + 1]}27-${letters[increment + 1]}25` } // Utilidad
          worksheet.getCell(`${letters[increment + 1]}29`).value = { formula: `${letters[increment + 1]}28/C9` } // Utilidad / Ha
          worksheet.getCell(`${letters[increment + 1]}30`).value = { formula: `${letters[increment + 1]}28/(${letters[increment + 1]}7-C10+1)/C9` } // Utilidad / Dia / Ha
          worksheet.getCell(`${letters[increment + 1]}31`).value = { formula: `${letters[increment + 1]}25/${letters[increment + 1]}8` } // Costo/${letters[increment + 1]}b
          worksheet.getCell(`${letters[increment + 1]}32`).value = { formula: `${letters[increment + 1]}26-${letters[increment + 1]}31` } // Margen*lib
          worksheet.getCell(`${letters[increment + 1]}33`).value = { formula: `${letters[increment + 1]}28/${letters[increment + 1]}25` } // ROI
          }
        }

        increment += key === 0 ? 2 : 3
      })
      worksheet.mergeCells(`A3:${letters[increment - 1]}3`)
      worksheet.getCell('A3').alignment = { vertical: 'middle', horizontal: 'center' }
      worksheet.getCell('A43').value = 'Observaciones:'
      worksheet.getCell('A44').value = fishingProjection.observations

      return workbook.xlsx.writeFile(Helpers.appRoot(`storage/uploads/${filename}`))
    })
    console.log('se generó')
    return filename
}

module.exports = FishingProjectionController
