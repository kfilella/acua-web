'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with formats
 */
const Format = use('App/Models/Format')
const List = use('App/Functions/List')
const { validate } = use('Validator')
class FormatController {
  /**
   * Show a list of all formats.
   * GET formats
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    let formats = (await Format.all()).toJSON()
    formats = List.addRowActions(formats, [{ name: 'edit', route: 'formats/form/', permission: '1.11.3' }, { name: 'delete', permission: '1.11.4' }])
    response.send(formats)
  }

  /**
   * Create/save a new format.
   * POST formats
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const body = request.all()
    body.config = JSON.stringify(body.config)
    const format = await Format.create(body)
    response.send(format)
  }

  /**
   * Display a single format.
   * GET formats/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, response }) {
    const format = await Format.find(params.id)
    response.send(format)
  }

  /**
   * Update format details.
   * PUT or PATCH formats/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a format with id.
   * DELETE formats/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = FormatController
