'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with costbenefitprojections
 */
const CostBenefitProjection = use('App/Models/CostBenefitProjection')
const CostBenefitProjectionDetail = use('App/Models/CostBenefitProjectionDetail')
const ShrimpPrice = use('App/Models/ShrimpPrice')
const ShrimpPriceFunctions = use('App/Functions/ShrimpPrice')
const ExcelJS = require('exceljs')
const User = use('App/Models/User')
const Email = use('App/Functions/Email')
const { validate } = use('Validator')
const PdfPrinter = use("pdfmake")
const Helpers = use("Helpers")
const fs = use("fs")
const moment = require('moment')

var fonts = {
  Roboto: {
    normal: 'resources/fonts/Roboto-Regular.ttf',
    bold: 'resources/fonts/Roboto-Medium.ttf',
    italics: 'resources/fonts/Roboto-Italic.ttf',
    bolditalics: 'resources/fonts/Roboto-MediumItalic.ttf'
  }
}


async function getShrimpPriceRanges () {
  const shrimpPricesRanges = ShrimpPriceFunctions.getRanges()
  for (const range of shrimpPricesRanges) {
    const lastPrice = await ShrimpPrice.query().where('range_id', range.id).orderBy('date', 'desc').orderBy('updated_at', 'desc').first()
    range.lastPrice = lastPrice ? lastPrice.price : 0
    range.range_id = lastPrice ? lastPrice.range_id : 0
    range.average_size = lastPrice ? lastPrice.average_size : 1
  }
  return shrimpPricesRanges
}
/**
   * Función para calcular los datos de la proyección de costo beneficio
   *
   * @param {array} details Array con las piscinas añadidas a la proyección
   */
async function calculateData (details) {
  // Consulta de precios de camarón
  console.log('shrimpPricesRanges', await getShrimpPriceRanges())
  const shrimpPricesRanges = await getShrimpPriceRanges()
  /* const shrimpPricesRanges = ShrimpPriceFunctions.getRanges()
  for (const range of shrimpPricesRanges) {
     const lastPrice = await ShrimpPrice.query().where('range_id', range.id).orderBy('date', 'desc').orderBy('updated_at', 'desc').first()
     range.lastPrice = lastPrice ? lastPrice.price : 0
     range.range_id = lastPrice ? lastPrice.range_id : 0
     range.average_size = lastPrice ? lastPrice.average_size : 1
  } */

  details.forEach(element => {
    // Cabeza - sección producción

    // Libras
    element.pounds_thinning2 = element.pounds_thinning * element.pool.area
    element.sale_head = element.pounds_thinning2 * 0.95
    element.sale_tail = (element.pounds_thinning2 - element.sale_head) * 0.66

    element.production_days = parseInt(element.production_days)
    element.stopping_days = parseInt(element.stopping_days)
    element.total_days = element.production_days + element.stopping_days
    element.cicles = (365 / element.total_days).toFixed(2)
    element.growth = (((element.size_grs - element.size_planting) / element.production_days) * 7).toFixed(2)
    element.cam_m = (element.density * (element.survival / 100) / 10000).toFixed()

    //Cálculo de precio de cosecha
    let priceRecord
    if (element.size_grs >= 8 && element.size_grs <= 9.5) {
      priceRecord = shrimpPricesRanges.find(val => val.range_id == 1)
    } else if (element.size_grs >= 10 && element.size_grs <= 11.5) {
      priceRecord = shrimpPricesRanges.find(val => val.range_id == 2)
    } else if (element.size_grs >= 12 && element.size_grs <= 13.5) {
      priceRecord = shrimpPricesRanges.find(val => val.range_id == 3)
    } else if (element.size_grs >= 14 && element.size_grs <= 16) {
      priceRecord = shrimpPricesRanges.find(val => val.range_id == 4)
    } else if (element.size_grs >= 17 && element.size_grs <= 20) {
      priceRecord = shrimpPricesRanges.find(val => val.range_id == 5)
    } else if (element.size_grs >= 21 && element.size_grs <= 25) {
      priceRecord = shrimpPricesRanges.find(val => val.range_id == 6)
    } else if (element.size_grs >= 26 && element.size_grs <= 31) {
      priceRecord = shrimpPricesRanges.find(val => val.range_id == 7)
    } else {
      priceRecord = shrimpPricesRanges.find(val => val.range_id == 8)
    }
    // console.log('priceRecord', priceRecord.lastPrice * element.size_grs)
    const lastPrice = priceRecord ? priceRecord.lastPrice : 0
    const average_size = priceRecord ? priceRecord.average_size : 1
    element.harvest_price = ((lastPrice * element.size_grs / average_size) / 2.2046)

    //Cálculo de precio de raleo
    let priceRecordThinning
    if (element.size_thinning >= 8 && element.size_thinning <= 9.5) {
      priceRecordThinning = shrimpPricesRanges.find(val => val.range_id == 1)
    } else if (element.size_thinning >= 10 && element.size_thinning <= 11.5) {
      priceRecordThinning = shrimpPricesRanges.find(val => val.range_id == 2)
    } else if (element.size_thinning >= 12 && element.size_thinning <= 13.5) {
      priceRecordThinning = shrimpPricesRanges.find(val => val.range_id == 3)
    } else if (element.size_thinning >= 14 && element.size_thinning <= 16) {
      priceRecordThinning = shrimpPricesRanges.find(val => val.range_id == 4)
    } else if (element.size_thinning >= 17 && element.size_thinning <= 20) {
      priceRecordThinning = shrimpPricesRanges.find(val => val.range_id == 5)
    } else if (element.size_thinning >= 21 && element.size_thinning <= 25) {
      priceRecordThinning = shrimpPricesRanges.find(val => val.range_id == 6)
    } else if (element.size_thinning >= 26 && element.size_thinning <= 31) {
      priceRecordThinning = shrimpPricesRanges.find(val => val.range_id == 7)
    } else {
      priceRecordThinning = shrimpPricesRanges.find(val => val.range_id == 8)
    }

    const lastPrice2 = priceRecordThinning ? priceRecordThinning.lastPrice : 0
    const average_size2 = priceRecordThinning ? priceRecordThinning.average_size : 1
    element.thinning_price = ((lastPrice2 * element.size_thinning / average_size2) / 2.2046)

    //console.log('element.pounds', element.pounds)
    element.initial_biomass = (element.density * element.size_planting /454)
    if (element.thinning == 1) { // Con raleo
      element.pounds = ((element.survival / 100) - (element.pounds_thinning2 * 454 / element.size_thinning) / (element.density * element.pool.area)) * (element.density * element.pool.area * element.size_grs / 454)
      element.production_head = (element.pounds * 0.95)
      element.production_tail = ((element.pounds - element.production_head) * 0.66)
      element.sale_us_dolar = ((element.sale_head + parseFloat(element.sale_tail)) * element.thinning_price) + ((element.production_head + element.production_tail) * element.harvest_price)
      element.pounds_shrimp_ha = ((element.pounds_thinning2 + element.pounds)/ element.pool.area).toFixed() // Con raleo y sin raleo son distintas
      // element.balanced_kilos = (element.fc * (element.pounds + element.pounds_thinning2 - element.initial_biomass))/2.2046 // depende
      element.balanced_kilos = (element.fc * (element.pounds + element.pounds_thinning2 - element.initial_biomass))/2.2046
    } else { // Sin raleo
      element.pounds = element.density * (element.survival / 100) * element.size_grs * element.pool.area / 454
      element.production_head = (element.pounds * 0.95)
      element.production_tail = ((element.pounds - element.production_head) * 0.66)
      element.sale_us_dolar = ((element.production_head + element.production_tail) * element.harvest_price)
      element.pounds_shrimp_ha = ((element.pounds)/ element.pool.area).toFixed()
      element.balanced_kilos = (element.fc * (element.pounds- element.initial_biomass))/2.2046
    }

    // Precio venta libra
    element.price_pound_sale = element.cent_gr * element.size_grs
    element.production_us_dolar = Math.floor((parseFloat(element.production_head) + parseFloat(element.production_tail)) * element.price_pound_sale)
    // element.pounds_shrimp_ha = ((element.density * (element.survival / 100)) * element.size_grs / 1000 * 2.2046).toFixed()

    element.price = element.size_thinning * element.cent_gr


    // Millares de p larvas
    element.thousands_larvae = (element.density * element.pool.area) / 1000
    element.expenses_us_dolar = element.thousands_larvae * element.cost_per_thounsand

    element.initial_biomass = (element.density * element.size_planting /454)

    // Kilos balanceado
    // element.balanced_kilos = (element.fc * (parseFloat(element.pounds) + parseFloat(element.pounds_thinning))) / 2.2046

    element.balanced_us_dolar = (element.balanced_kilos * element.us_kg_balance).toFixed()

    element.balanced_bag_cost = element.us_kg_balance * 25
    element.operations_us_dolar = element.cost_day_ha * element.total_days * element.pool.area


    element.price_pound_sale = element.price_pound_sale.toFixed(2)
    element.size_thinning = parseFloat(element.size_thinning).toFixed(2)
    element.fc = parseFloat(element.fc).toFixed(2)

    element.balanced_kilos = parseFloat(element.balanced_kilos).toFixed()
    element.cost_day_ha = parseFloat(element.cost_day_ha).toFixed(2)

    element.total_cost = parseFloat(element.expenses_us_dolar) + parseFloat(element.balanced_us_dolar) + parseFloat(element.operations_us_dolar)

    element.us_kg_balance = parseFloat(element.us_kg_balance).toFixed(2)
    element.total_sale = parseFloat(element.sale_us_dolar)
    element.utility = parseFloat(element.sale_us_dolar) - parseFloat(element.total_cost)
    // element.margin_kg = element.utility / ((parseFloat(element.pounds) + parseFloat(element.pounds_thinning)) / 2.2046)

    if (element.thinning == 1) { // Con raleo
      element.margin_kg = element.utility / ((element.pounds + element.pounds_thinning2) / 2.2046)
      element.margin_pounds = (element.margin_kg / 2.2046).toFixed(2)
      element.cost_pound = (element.total_cost / (parseFloat(element.pounds) + parseFloat(element.pounds_thinning2))).toFixed(2)
    } else {
      // =+F36/((F7)/2,2046)
      element.margin_kg = element.utility / (element.pounds / 2.2046)
      element.margin_pounds = (element.margin_kg / 2.2046).toFixed(2)
      element.cost_pound = (element.total_cost / (parseFloat(element.pounds))).toFixed(2)
    }


    element.roi = ((element.utility / element.total_cost) * 100).toFixed()
    element.rofi = element.utility / element.balanced_us_dolar
    element._rofi = (1 / element.rofi).toFixed(2)

    element.utility_ha_day = (element.utility / (element.pool.area * element.total_days)).toFixed(2)
    element.utility_ha = (element.utility / element.pool.area).toFixed()


    element.production_total = (element.density * (element.survival / 100)).toFixed()

    element.margin_kg = parseFloat(element.margin_kg).toFixed(2)
    element.rofi = parseFloat(element.rofi).toFixed(2)
    element.price = element.price.toFixed(2)
    element.pounds = element.pounds.toFixed()
    element.sale_tail = element.sale_tail.toFixed()
    element.production_head = element.production_head.toFixed()
    element.thinning_price = element.thinning_price.toFixed(2)
    element.production_tail = element.production_tail.toFixed()
    element.harvest_price = element.harvest_price.toFixed(2)
    element.sale_us_dolar = element.sale_us_dolar.toFixed()
    element.expenses_us_dolar = element.expenses_us_dolar.toFixed()
    element.initial_biomass = element.initial_biomass.toFixed()
    element.operations_us_dolar = element.operations_us_dolar.toFixed()
    element.total_cost = element.total_cost.toFixed()
    element.cost_per_thounsand = parseFloat(element.cost_per_thounsand).toFixed(2)
  })
  return details
}
class CostBenefitProjectionController {
  /**
   * Show a list of all costbenefitprojections.
   * GET costbenefitprojections
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    const records = (await CostBenefitProjection.query().with('technicalAdvisor').with('details.pool').orderBy('created_at', 'desc').fetch()).toJSON()
    records.forEach(element => {
      element.pools = (element.details.map(val => val.pool.description)).join(',')
    })
    response.send(records)
  }

  /**
   * Create/save a new costbenefitprojection.
   * POST costbenefitprojections
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ auth, request, response }) {
    const req = request.all()
    const logguedUser = await auth.getUser()
    const user = (await User.query().with('technical_advisor').where('id', logguedUser.id).first()).toJSON()

    const validation = await validate(request.all(), CostBenefitProjectionDetail.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const body = request.only(CostBenefitProjectionDetail.fillable)
      // Si no hay proyección se debe crear
      if (!req.projectionId) {
        body.cost_benefit_projection_id = (await CostBenefitProjection.create({
          shrimp_id: req.shrimp_id,
          technical_advisor_id: user.technical_advisor.id
        })).toJSON().id
      } else {
        body.cost_benefit_projection_id = req.projectionId
      }
      // delete body.shrimp_id

      const record = await CostBenefitProjectionDetail.create(body)
      response.send(record)
    }
  }

  /**
   * Display a single costbenefitprojection.
   * GET costbenefitprojections/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    const record = (await CostBenefitProjection.query().with('details.pool')
    .with('details', (query) => {
      query.orderBy('id')
    })
    .where('id', params.id).first()).toJSON()
    record.details = await calculateData(record.details)
    response.send(record)
  }

  /**
   * Update costbenefitprojection details.
   * PUT or PATCH costbenefitprojections/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const section = await CostBenefitProjectionDetail.query().where('id', params.id).first()
    if (section) {
      const validation = await validate(request.all(), CostBenefitProjectionDetail.fieldValidationRules())

      if (validation.fails()) {
        response.unprocessableEntity(validation.messages())
      } else {
        const body = request.only(CostBenefitProjectionDetail.fillable)

        await CostBenefitProjectionDetail.query().where('id', params.id).update(body)
        response.send(section)
      }
    } else {
      response.notFound(validation.messages())
    }
  }

  /**
   * Delete a costbenefitprojection with id.
   * DELETE costbenefitprojections/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
  /**
   * Generar el reporte de la proyección costo-beneficio.
   * GET cost_benefit_projections_print/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async printCostBenefitProjection ({ params, request, response }) {
    var printer = new PdfPrinter(fonts)
    const report = (await CostBenefitProjection.query().with('details.pool').where('id', params.id)
    .with('shrimp.customer').with('technicalAdvisor').with('shrimp.zone').first()).toJSON()




    const customer = report.shrimp.customer.legal_name
    const shrimp = report.shrimp.name
    const zone = report.shrimp.zone ? report.shrimp.zone.name : 'Zona eliminada'
    const poolNames = report.details.map(val => val.pool.description)
    console.log('poolNames', poolNames)



    const title = 'PROYECCIÓN DE COSTO BENEFICIO'
    this.img = {
      image: 'logo.jpeg',
      fit: [65, 65],
//      margin: [31, 25],
      absolutePosition: {
        'x': 31,
        'y': 25
      }
    }
    this.img2 = {
      image: 'logo.jpeg',
      fit: [65, 65],
      absolutePosition: {
        'x': 31,
        'y': 10
      }
    }

    // Contenido
    const content = [
      {
        margin: [-17, 40, 20, 20],
        alignment: 'left',
        text: [
          {
            bold: true,
            text: `PISCINAS ESTUDIADAS: ${poolNames}`,
            fontSize: 14
          }
        ]
      }
    ]
    //return report
    report.details = await calculateData(report.details)
    report.details.forEach((element, key) => {
      let planting_type = ''
      switch(element.planting_type) {
        case 1:
          planting_type = 'Madre hija'
          break;
        case 2:
          planting_type = 'Transferida'
          break;
        case 3:
          planting_type = 'Directa'
          break;
        default:
          planting_type = 'Raceways-transferida'
      }
      content.push(
        {
          margin: [-17, 0, 20, 20],
          alignment: 'left',
          text: [
            {
              bold: true,
              text: `PISCINA ${element.pool.description}`,
              fontSize: 14
            }
          ]
        },
        {
          margin: [-17, 0],
          table: {
            headerRows: 1,
            widths: ['*', '*', '*', '*'],
            body: [
              [
                { text: 'RALEO', bold: true, alignment: 'center' },
                { text: 'TIPO DE SIEMBRA', bold: true, alignment: 'center' },
                { text: 'PISCINA', bold: true, alignment: 'center' },
                { text: 'HECTÁREAS', bold: true, alignment: 'center' },
              ],
              [
                { text: element.thinning, alignment: 'center' },
                { text: element.planting_type, alignment: 'center' },
                { text: element.pool.description, alignment: 'center' },
                { text: element.pool.area, alignment: 'center' }
              ]
            ]
          }
        },
        {
          margin: [-17, 10, 20, 20],
          alignment: 'left',
          text: [
            {
              bold: true,
              text: 'PRODUCCIÓN',
              fontSize: 14
            }
          ]
        },
        {
          margin: [-17, 0],
          table: {
            headerRows: 1,
            widths: ['auto', 'auto', 'auto', 'auto', 'auto', 'auto', 'auto'],
            body: [
              [
                { text: 'DENSIDAD', bold: true, alignment: 'center' },
                { text: 'SUPERVIVENCIA', bold: true, alignment: 'center' },
                { text: 'TAMAÑO (gr)', bold: true, alignment: 'center' },
                { text: 'CICLOS', bold: true, alignment: 'center' },
                { text: 'CRECIMIENTO', bold: true, alignment: 'center' },
                { text: 'TAMAÑO SIEMBRA', bold: true, alignment: 'center' },
                { text: 'TOTAL', bold: true, alignment: 'center' }
              ],
              [
                { text: element.density, alignment: 'center' },
                { text: element.survival, alignment: 'center' },
                { text: element.size_grs, alignment: 'center' },
                { text: element.cicles, alignment: 'center' },
                { text: element.growth, alignment: 'center' },
                { text: element.size_planting, alignment: 'center' },
                { text: element.production_us_dolar, alignment: 'center' }
              ],
              [
                { text: 'COSECHA', alignment: 'center', colSpan: 7, bold: true }
              ],
              [
                { text: 'LIBRAS', alignment: 'center', colSpan: 2, bold: true },
                { text: '' },
                { text: 'CABEZA', alignment: 'center', colSpan: 2, bold: true },
                { text: '' },
                { text: 'COLA', alignment: 'center', colSpan: 2, bold: true },
                { text: '' },
                { text: 'TOTAL', alignment: 'center', bold: true }
              ],
              [
                { text: element.pounds, alignment: 'center', colSpan: 2 },
                { text: '' },
                { text: element.production_head, alignment: 'center', colSpan: 2 },
                { text: '' },
                { text: element.production_tail, alignment: 'center', colSpan: 2 },
                { text: '' },
                { text: element.production_total, alignment: 'center' }
              ]
            ]
          }
        },
        {
          margin: [-17, 10, 20, 20],
          alignment: 'left',
          text: [
            {
              bold: true,
              text: 'VENTAS',
              fontSize: 14
            }
          ]
        },
        {
          margin: [-17, 0],
          table: {
            headerRows: 1,
            widths: ['*', '*', '*', '*', '*', '*'],
            body: [
              [
                { text: 'LIBRAS CAMARÓN / Ha', bold: true, alignment: 'center', colSpan: 2 },
                { text: '' },
                { text: 'PRECIO VENTA / LIBRA', bold: true, alignment: 'center', colSpan: 2 },
                { text: 'CICLOS', bold: true, alignment: 'center' },
                { text: 'CTVS / GRAMO', bold: true, alignment: 'center', colSpan: 2 },
                { text: 'TAMAÑO SIEMBRA', bold: true, alignment: 'center' }
              ],
              [
                { text: element.pounds_shrimp_ha, alignment: 'center', colSpan: 2 },
                { text: '', alignment: 'center' },
                { text: element.price_pound_sale, alignment: 'center', colSpan: 2 },
                { text: '', alignment: 'center' },
                { text: element.cent_gr, alignment: 'center', colSpan: 2 },
                { text: '', alignment: 'center' }
              ],
              [
                { text: 'RALEO', alignment: 'center', colSpan: 6, bold: true }
              ],
              [
                { text: 'TAMAÑO RALEO', alignment: 'center', bold: true },
                { text: 'LIBRAS', alignment: 'center', bold: true },
                { text: 'CABEZA', alignment: 'center', bold: true },
                { text: 'COLA', alignment: 'center', bold: true },
                { text: 'US$', alignment: 'center', bold: true },
                { text: 'PRECIO', alignment: 'center', bold: true }
              ],
              [
                { text: element.size_thinning, alignment: 'center' },
                { text: element.pounds_thinning, alignment: 'center' },
                { text: element.sale_head, alignment: 'center' },
                { text: element.sale_tail, alignment: 'center' },
                { text: element.sale_us_dolar, alignment: 'center' },
                { text: element.price, alignment: 'center' }
              ]
            ]
          }
        },
        {
          pageBreak: 'before',
          margin: [-17, 10, 20, 20],
          alignment: 'left',
          text: [
            {
              bold: true,
              text: 'GASTOS',
              fontSize: 14
            }
          ]
        },
        {
          margin: [-17, 0],
          table: {
            headerRows: 1,
            widths: ['*', '*', '*', '*', '*', '*'],
            body: [
              [
                { text: 'POST-LARVAS', alignment: 'center', colSpan: 6, bold: true },
                { text: '' },
                { text: '' },
                { text: '' },
                { text: '' },
                { text: '' }
              ],
              [
                { text: 'MILLARES DE POST-LARVAS', bold: true, alignment: 'center', colSpan: 2 },
                { text: '' },
                { text: 'COSTO POR MILLAR DE JUVENIL', bold: true, alignment: 'center', colSpan: 2 },
                { text: '' },
                { text: 'PRECIO', bold: true, alignment: 'center', colSpan: 2 },
                { text: '' }
              ],
              [
                { text: element.thousands_larvae, alignment: 'center', colSpan: 2 },
                { text: '', alignment: 'center' },
                { text: element.cost_per_thounsand, alignment: 'center', colSpan: 2 },
                { text: '', alignment: 'center' },
                { text: element.expenses_us_dolar, alignment: 'center', colSpan: 2 },
                { text: '', alignment: 'center' }
              ],
              [
                { text: 'BALANCEADO', alignment: 'center', colSpan: 6, bold: true }
              ],
              [
                { text: 'BIOMASA INICIAL', alignment: 'center', bold: true },
                { text: 'FEED CONVER', alignment: 'center', bold: true },
                { text: 'KILOS BALANCEADO', alignment: 'center', bold: true },
                { text: 'US$ / Kg DE BALANCEADO', alignment: 'center', bold: true },
                { text: 'COSTO DE BALANCEADO', alignment: 'center', bold: true },
                { text: 'PRECIO', alignment: 'center', bold: true }
              ],
              [
                { text: element.initial_biomass, alignment: 'center' },
                { text: element.fc, alignment: 'center' },
                { text: element.balanced_kilos, alignment: 'center' },
                { text: element.us_kg_balance, alignment: 'center' },
                { text: element.balanced_bag_cost, alignment: 'center' },
                { text: element.balanced_us_dolar, alignment: 'center' }
              ],
              [
                { text: 'OPERACIONES', alignment: 'center', colSpan: 6, bold: true }
              ],
              [
                { text: 'DÍAS DE PRODUCCIÓN', alignment: 'center', bold: true },
                { text: 'DÍAS DE PARALIZACIÓN', alignment: 'center', bold: true },
                { text: 'TOTAL', alignment: 'center', bold: true },
                { text: 'COSTOS DÍA / Ha', alignment: 'center', bold: true },
                { text: 'PRECIO', alignment: 'center', bold: true, colSpan: 2 },
                { text: ''}
              ],
              [
                { text: element.production_days, alignment: 'center' },
                { text: element.stopping_days, alignment: 'center' },
                { text: element.total_days, alignment: 'center' },
                { text: element.cost_day_ha, alignment: 'center' },
                { text: element.operations_us_dolar, alignment: 'center', colSpan: 2 },
                { text: ''}
              ]
            ]
          }
        },
        {
          margin: [-17, 10, 20, 20],
          alignment: 'left',
          text: [
            {
              bold: true,
              text: 'RESUMEN DE COSTOS',
              fontSize: 14
            }
          ]
        },
        {
          pageBreak: report.details.length - 1 == key ? undefined : 'after',
          margin: [-17, 0],
          table: {
            headerRows: 1,
            widths: ['*', '*', '*', '*', '*'],
            body: [
              [
                { text: 'TOTAL COSTOS', alignment: 'center', bold: true },
                { text: 'TOTAL VENTAS', alignment: 'center', bold: true },
                { text: 'UTILIDAD', alignment: 'center', bold: true },
                { text: 'MARGEN Kg', alignment: 'center', bold: true },
                { text: 'MARGEN lb', alignment: 'center', bold: true }
              ],
              [
                { text: element.total_cost, alignment: 'center' },
                { text: element.total_sale, alignment: 'center' },
                { text: element.utility, alignment: 'center' },
                { text: element.margin_kg, alignment: 'center' },
                { text: element.margin_pounds, alignment: 'center' }
              ],
              [
                { text: 'COSTOS / lb', alignment: 'center', bold: true },
                { text: 'UTILIDAD $ / DÍA / Ha', alignment: 'center', bold: true },
                { text: 'ROI', alignment: 'center', bold: true },
                { text: 'ROFI', alignment: 'center', bold: true },
                { text: '1 / ROFI', alignment: 'center', bold: true }
              ],
              [
                { text: element.cost_pound, alignment: 'center' },
                { text: element.utility_ha_day, alignment: 'center' },
                { text: element.roi, alignment: 'center' },
                { text: element.rofi, alignment: 'center' },
                { text: element._rofi, alignment: 'center' }
              ]
            ]
          }
        }
      )
    })


    var docDefinition = {
      pageSize: 'letter',
      pageMargins: [60, 120, 40, 90],
      header: [
        {
          margin: [20, 40, 20, 20],
          alignment: 'center',
          text: [
            {
              bold: true,
              text: `${title}\n`,
              fontSize: 14
            }
          ]
        },
        {
          alignment: 'center',
          margin:[0,-10,0,10],
          text: [
            { text: 'CLIENTE: ', bold: true, fontSize: 10},
            { text: customer, fontSize: 10},
            { text: '             '},
            { text: 'CAMARONERA: ', bold: true, fontSize: 10},
            { text: shrimp, fontSize: 10},
            { text: '             '},
            { text: 'Zona: ', bold: true, fontSize: 10},
            { text: zone, fontSize: 10}
          ]
        },
        this.img
      ],
      content,
      footer: {
        columns: [
          this.img2,
          {text: 'Dpto. Técnico\n', bold: true, alignment: 'left', fontSize: 12, margin: [-25, 13, 0, 0] },
          {text: 'División Acuacultura\nTelf. 593 4 2811-61 Ext. 214-240\nhttp://www.agripac.com.ec/\n', alignment: 'left', fontSize: 10, margin: [-144, 25, 0, 0]},
          {text: 'Guayaquil - Ecuador', bold: true, alignment: 'left', fontSize: 12, margin: [-263, 60, 0, 0] },
          {text: `Asesor técnico\n${report.technicalAdvisor ? report.technicalAdvisor.full_name : ''}\n`,bold:true, alignment: 'left', fontSize: 10, margin: [5, 30, 0, 0] },
        ]
      }
    }

    var options = {
      // ...
    }
    var pdfDoc = printer.createPdfKitDocument(docDefinition, options)
    const dir = Helpers.appRoot('storage/uploads/cost_benefit_projections')
    const filename = `Reporte_de_proyeccion_costo_beneficio_${report.id}.pdf`
    if (!fs.existsSync(dir)){
      fs.mkdirSync(dir);
    }
    pdfDoc.pipe(fs.createWriteStream(`${dir}/${filename}`));
    pdfDoc.end()

    response.send(`cost_benefit_projections-Reporte_de_proyeccion_costo_beneficio_${report.id}`)
  }

  async sendMailInfo ({ request, response, params }) {
    const reportId = params.id

    const report = (await CostBenefitProjection.query().with('shrimp.customer.contacts').where('id', reportId).first()).toJSON()

    const contacts_email = report.shrimp.customer.contacts.map(val => (
      {
        email: val.email || 'Sin correo',
        name: `${val.name} ${val.last_name}`,
        disable: val.email == null || val.email == ''
      }
    ))
    const form_emails = {
      to: contacts_email.length ? contacts_email[0] : '',
      contacts_email,
      email_customer: report.shrimp.customer.electronic_documents_email
    }
    response.send(form_emails)
  }

  async sendMail ({ request, response, params }) {
    const { to, cc, content } = request.all()
    const reportId = params.id
    const filename = `Reporte_de_proyeccion_costo_beneficio_${reportId}.pdf`
    await Email.send({
      to,
      subject: 'Reporte de proyección costo-beneficio',
      content,
      cc,
      attachments: [
        {
          filename,
          path: Helpers.appRoot('storage/uploads/cost_benefit_projections') + `/${filename}`
        }
      ]
    })
    response.send(true)
  }

  async generateExcelReport ({ request, response }) {
    const { id } = request.get()
    const record = (await CostBenefitProjection.query().with('details.pool').with('details.food')
    .where('id', id).first()).toJSON()
    record.details = await calculateData(record.details)
    // return record
    var workbook = new ExcelJS.Workbook()
    await workbook.xlsx.readFile('resources/templates/plantilla_proyeccion_costos.xlsx')
    .then(function() {
      console.log('cargo archivo')
      var worksheet = workbook.getWorksheet(1)
      // worksheet.name = 'Costos'
      let increment = 8

      worksheet.getCell('D6').value = moment(record.created_at).format('DD/M/YYYY')
      record.details.forEach((element, key) => {
        if (key > 0) { // Si está después de la primera proyección, copiar el formato anterior
          // Estilos
          worksheet.getCell(`A${increment}`).style = worksheet.getCell(`A${increment - 1}`).style
          worksheet.getCell(`B${increment}`).style = worksheet.getCell(`B${increment - 1}`).style
          worksheet.getCell(`C${increment}`).style = worksheet.getCell(`C${increment - 1}`).style
          worksheet.getCell(`D${increment}`).style = worksheet.getCell(`D${increment - 1}`).style
          worksheet.getCell(`E${increment}`).style = worksheet.getCell(`E${increment - 1}`).style
          worksheet.getCell(`F${increment}`).style = worksheet.getCell(`F${increment - 1}`).style
          worksheet.getCell(`G${increment}`).style = worksheet.getCell(`G${increment - 1}`).style
          worksheet.getCell(`H${increment}`).style = worksheet.getCell(`H${increment - 1}`).style
          worksheet.getCell(`I${increment}`).style = worksheet.getCell(`I${increment - 1}`).style
          worksheet.getCell(`J${increment}`).style = worksheet.getCell(`J${increment - 1}`).style
          worksheet.getCell(`K${increment}`).style = worksheet.getCell(`K${increment - 1}`).style
          worksheet.getCell(`L${increment}`).style = worksheet.getCell(`L${increment - 1}`).style
          worksheet.getCell(`M${increment}`).style = worksheet.getCell(`M${increment - 1}`).style
          worksheet.getCell(`N${increment}`).style = worksheet.getCell(`N${increment - 1}`).style
          worksheet.getCell(`O${increment}`).style = worksheet.getCell(`O${increment - 1}`).style
          worksheet.getCell(`P${increment}`).style = worksheet.getCell(`P${increment - 1}`).style
          worksheet.getCell(`Q${increment}`).style = worksheet.getCell(`Q${increment - 1}`).style
          worksheet.getCell(`R${increment}`).style = worksheet.getCell(`R${increment - 1}`).style
          worksheet.getCell(`S${increment}`).style = worksheet.getCell(`S${increment - 1}`).style
          worksheet.getCell(`T${increment}`).style = worksheet.getCell(`T${increment - 1}`).style
          worksheet.getCell(`U${increment}`).style = worksheet.getCell(`U${increment - 1}`).style
          worksheet.getCell(`V${increment}`).style = worksheet.getCell(`V${increment - 1}`).style
          worksheet.getCell(`W${increment}`).style = worksheet.getCell(`W${increment - 1}`).style
          worksheet.getCell(`X${increment}`).style = worksheet.getCell(`X${increment - 1}`).style
          worksheet.getCell(`Y${increment}`).style = worksheet.getCell(`Y${increment - 1}`).style
          worksheet.getCell(`Z${increment}`).style = worksheet.getCell(`Z${increment - 1}`).style
          worksheet.getCell(`AA${increment}`).style = worksheet.getCell(`AA${increment - 1}`).style
          worksheet.getCell(`AB${increment}`).style = worksheet.getCell(`AB${increment - 1}`).style
          worksheet.getCell(`AC${increment}`).style = worksheet.getCell(`AC${increment - 1}`).style
        }
         // Valores
         worksheet.getCell(`A${increment}`).value = element.pool.description
         worksheet.getCell(`B${increment}`).value = parseFloat(element.pool.area)
         worksheet.getCell(`C${increment}`).value = parseFloat(element.density)
         worksheet.getCell(`D${increment}`).value = `${element.survival}%`
         worksheet.getCell(`E${increment}`).value = parseFloat(element.size_planting)
         worksheet.getCell(`F${increment}`).value = element.production_days
         worksheet.getCell(`G${increment}`).value = element.thinning == 1 ? parseFloat(element.pounds_thinning2) : ''
         worksheet.getCell(`H${increment}`).value = element.thinning == 1 ? parseFloat(element.size_thinning) : ''
         worksheet.getCell(`I${increment}`).value = parseFloat(element.pounds)
         worksheet.getCell(`J${increment}`).value = parseFloat(element.size_grs)
         worksheet.getCell(`K${increment}`).value = element.thinning == 1 ? { formula: `G${increment}+I${increment}` } : { formula: `I${increment}` }
         worksheet.getCell(`M${increment}`).value = parseFloat(element.growth)
         worksheet.getCell(`N${increment}`).value = parseFloat(element.cam_m)
         worksheet.getCell(`O${increment}`).value = parseFloat(element.pounds_shrimp_ha)
         worksheet.getCell(`P${increment}`).value = parseFloat(element.fc)
         worksheet.getCell(`Q${increment}`).value = parseFloat(element.cost_per_thounsand)
         worksheet.getCell(`R${increment}`).value = parseFloat(element.us_kg_balance)
         worksheet.getCell(`L${increment}`).value = element.thinning == 1 ? { formula: `((Q${increment}*454)+(O${increment}*454))/((Q${increment}*454/R${increment})+(O${increment}*454/P${increment}))` } : { formula: `J${increment}` }
         worksheet.getCell(`S${increment}`).value = parseFloat(element.cost_pound)
         worksheet.getCell(`T${increment}`).value = parseFloat(element.cost_day_ha)
         worksheet.getCell(`U${increment}`).value = parseFloat(element.harvest_price)
         worksheet.getCell(`V${increment}`).value = element.utility
         worksheet.getCell(`W${increment}`).value = parseFloat(element.margin_kg)
         worksheet.getCell(`X${increment}`).value = parseFloat(element.margin_pounds)
         worksheet.getCell(`Y${increment}`).value = parseFloat(element.utility_ha)
         worksheet.getCell(`Z${increment}`).value = parseFloat(element.utility_ha_day)
         worksheet.getCell(`AA${increment}`).value =  parseFloat(element.roi) + '%'
         worksheet.getCell(`AB${increment}`).value = parseFloat(element.rofi)
         worksheet.getCell(`AC${increment}`).value = element.food ? element.food.name : ''

         increment++
      })

      return workbook.xlsx.writeFile(`storage/uploads/costos/Costos${id}.xlsx`);
    })
    response.download(Helpers.appRoot(`storage/uploads/costos/Costos${id}.xlsx`))
  }

  /**
   * Guardar una proyección de costos, ésta es diferente porque recibe varias tablas
   * POST cost_benefit_projections2
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
   async store2 ({ auth, request, response }) {
    const req = request.all()
    const logguedUser = await auth.getUser()
    const user = (await User.query().with('technical_advisor').where('id', logguedUser.id).first()).toJSON()

    // Si no hay proyección se debe crear
    const body = {}
    if (!req.projectionId) {
      body.cost_benefit_projection_id = (await CostBenefitProjection.create({
        shrimp_id: req.shrimp_id,
        technical_advisor_id: user.technical_advisor.id
      })).toJSON().id
    } else {
      body.cost_benefit_projection_id = req.projectionId
    }

    for(const element of req.details) {// Recorrer cada tabla enviada para crearla
      element.cost_benefit_projection_id = body.cost_benefit_projection_id
      const record = await CostBenefitProjectionDetail.create(element)
    }
    response.send(body.cost_benefit_projection_id)
    // return tables
    // const validation = await validate(request.all(), CostBenefitProjectionDetail.fieldValidationRules())

    /* if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const body = request.only(CostBenefitProjectionDetail.fillable)
      // Si no hay proyección se debe crear
      if (!req.projectionId) {
        body.cost_benefit_projection_id = (await CostBenefitProjection.create({
          shrimp_id: req.shrimp_id,
          technical_advisor_id: user.technical_advisor.id
        })).toJSON().id
      } else {
        body.cost_benefit_projection_id = req.projectionId
      }
      // delete body.shrimp_id

      const record = await CostBenefitProjectionDetail.create(body)
      response.send(record)
    } */
  }
  /**
   * Actualizar una proyección de costos
   * PUT cost_benefit_projections2/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update2 ({ params, request, response }) {
    const req = request.all()

    const activeIds = []
    for(const element of req.details) {// Recorrer cada tabla enviada para actualizarla o crearla. Guardo las activas para eliminar las eliminadas
      if (element.id) { // Si existe ya, la actualizo
        const body = {
          pool_id: element.pool_id,
          thinning: element.thinning,
          planting_type: element.planting_type,
          density: element.density,
          survival: element.survival,
          size_grs: element.size_grs,
          size_planting: element.size_planting,
          cent_gr: element.cent_gr,
          size_thinning: element.size_thinning,
          pounds_thinning: element.pounds_thinning,
          cost_per_thounsand: element.cost_per_thounsand,
          fc: element.fc,
          us_kg_balance: element.us_kg_balance,
          production_days: element.production_days,
          stopping_days: element.stopping_days,
          cost_day_ha: element.cost_day_ha,
          initial_biomass: element.initial_biomass,
          food_id: element.food_id
        }
        await CostBenefitProjectionDetail.query().where('id', element.id).update(body)
        activeIds.push(element.id)
      } else { // Si no existe crearla
        console.log('aqui', element)
        element.cost_benefit_projection_id = params.id
        const record = await CostBenefitProjectionDetail.create(element)
        activeIds.push(record.id)
      }
    }
    // Eliminar las tablas existentes que no se crearon o modificaron recientemente
    await CostBenefitProjectionDetail.query().where('cost_benefit_projection_id', params.id).whereNotIn('id',activeIds).delete()
    response.send(true)

    /* const section = await CostBenefitProjectionDetail.query().where('id', params.id).first()
    if (section) {
      const validation = await validate(request.all(), CostBenefitProjectionDetail.fieldValidationRules())

      if (validation.fails()) {
        response.unprocessableEntity(validation.messages())
      } else {
        const body = request.only(CostBenefitProjectionDetail.fillable)

        await CostBenefitProjectionDetail.query().where('id', params.id).update(body)
        response.send(section)
      }
    } else {
      response.notFound(validation.messages())
    } */
  }

  async getShrimpRanges () {
    return getShrimpPriceRanges()
  }
}

module.exports = CostBenefitProjectionController
