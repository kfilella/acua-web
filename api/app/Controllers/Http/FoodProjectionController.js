'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with foodprojections
 */
const FoodProjectionMaster = use('App/Models/FoodProjectionMaster')
const FoodProjectionDetail = use('App/Models/FoodProjectionDetail')
const Product = use('App/Models/Product')
const FoodProjectionFoodType = use('App/Models/FoodProjectionFoodType')
const Pool = use('App/Models/Pool')
const User = use('App/Models/User')
const List = use('App/Functions/List')
const Email = use('App/Functions/Email')
const FoodProjectionFunctions = use("App/Functions/FoodProjection")
const { validate } = use('Validator')
const Shrimp = use('App/Models/Shrimp')
const moment = require('moment')
const ExcelJS = require('exceljs')
const Helpers = use("Helpers")

/**
   * Función que devuelve el peso de siembra de una proyección
   *
   *
   * @param harvest_target_weight Peso objetivo
   * @param cultivation_days Días de cultivo
   * @param day Día a buscar dentro de los pesos vivos calculados
   */
function plantingWeigth (harvest_target_weight, cultivation_days, day) {
  // tanteo de tasa de crecimiento para saber con cual tasa el último día le llego cerca al peso objetivo
  let testLiveweight = 0
  let testGrowthRate = 0 // Tasa de crecimiento tanteada
  while (testLiveweight < harvest_target_weight) {
    testGrowthRate++
    testLiveweight = (((0.0231 * Math.pow(cultivation_days, 1.3758)) * (testGrowthRate / 100)))
  }
  const finalWeightPre = (((0.0231 * Math.pow(cultivation_days, 1.3758)) * ((testGrowthRate - 1) / 100)))
  const finalWeightPost = (((0.0231 * Math.pow(cultivation_days, 1.3758)) * ((testGrowthRate) / 100)))

  const diffFinalWeightPre = harvest_target_weight - finalWeightPre
  const diffFinalWeightPost = finalWeightPost - harvest_target_weight

  if (diffFinalWeightPre < diffFinalWeightPost) {
    testGrowthRate-- // Si el peso de abajo es más cercano entonces usar la tasa obtenida menos 1
  }
  const liveweights = []
  for (let i = 1; i <= cultivation_days; i++) {
    // Hasta el día 12 se usa la fórmula lógica tomando datos de la misma fila
    const liveweight = ((0.0231 * Math.pow(i, 1.3758)) * (testGrowthRate / 100)).toFixed(3)
    liveweights.push({ label: liveweight, value: i })
  }
  const found = liveweights.find(val => val.value == day) ? liveweights.find(val => val.value == day).label : 'Sin biomasa inicial'
  return found
}

class FoodProjectionController {
  /**
   * Show a list of all foodprojections.
   * GET foodprojections
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response }) {
    const params = request.get()
    let foodProjection = []
    let query = FoodProjectionMaster.query().with('technicalAdvisor')

    if (Object.keys(params).length) { // si recibe parámetros desde el front se implementa en el where
      const { shrimp_id } = params
      if (shrimp_id) {
        query = query.with('pool', (query) => {
          query.where('shrimp_id', shrimp_id)
        })
        delete params.shrimp_id
      }
      query = query.where(params)
    }

    foodProjection = (await query.orderBy('created_at', 'desc').fetch()).toJSON()

    foodProjection.forEach(element => {
      element.seedtime = moment(element.seedtime).format("DD-MM-YYYY")
    })

    /* foodProjection = List.addRowActions(foodProjection, [
      {name: 'show', route: 'food_projection/form/', permission: '2.1.1.1.29.2'}
    ]) */
    response.send(foodProjection)
  }
  async indexMobile ({ request, response }) {
    const params = request.get()
    let foodProjection = []
    let query = FoodProjectionMaster.query().with('technicalAdvisor')

    const { shrimp_id, page, recordsCount, text } = params
    if (Object.keys(params).length) { // si recibe parámetros desde el front se implementa en el where
      if (shrimp_id) {
        query = query.with('pool', (query) => {
          query.where('shrimp_id', shrimp_id)
        })
        delete params.shrimp_id
      }
      // query = query.where(params)
    }

    foodProjection = (await query.orderBy('created_at', 'desc').paginate(page, recordsCount)).toJSON()

    foodProjection.data.forEach(element => {
      element.seedtime = moment(element.seedtime).format("DD-MM-YYYY")
    })

    response.send(foodProjection)
  }
  async sendMailInfo ({ request, response, params }) {
    const projectionId = params.projectionId

    const analysis = (await FoodProjectionMaster.query().with('pool.shrimp.customer.contacts').where('id', projectionId).first()).toJSON()

    const contacts_email = analysis.pool.shrimp.customer.contacts.map(val => val.email)
    const form_emails = {
      to: contacts_email[0],
      contacts_email,
      email_customer: analysis.pool.shrimp.customer.electronic_documents_email
    }
    response.send(form_emails)
  }

  async sendMail ({ request, response, params }) {
    const { to, cc, content } = request.all()
    const analysisId = params.projectionId
    const filename = `Proyección_Alimentos_${analysisId}.pdf`
    await Email.send({
      to,
      subject: 'Proyección de Alimentos',
      content,
      cc,
      attachments: [
        {
          filename,
          path: Helpers.appRoot(`storage/uploads/${filename}`)
        }
      ]
    })
    response.send(true)
  }

  /**
   * Create/save a new foodprojection.
   * POST foodprojections
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ auth, request, response }) {
    // Implementar transacción
    const logguedUser = await auth.getUser()
    const user = (await User.query().with('technical_advisor').with('technical_chief').where('id', logguedUser.id).first()).toJSON()

    const wholeRequest = request.all()
    const validation = await validate(request.all(), FoodProjectionMaster.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const body = request.only(FoodProjectionMaster.fillable)
      const poolArea = (await Pool.query().where('id', wholeRequest.pool_id).first()).toJSON().area // Área de la piscina al momento de crear la proyección
      body.pool_area = poolArea // wholeRequest.poolArea
      body.technical_advisor_id = user.technical_advisor ? user.technical_advisor.id : 0

      // Si el usuario no es asesor, debe ser jefe, se guarda el id del jefe

      if (!body.technical_advisor_id){
        body.technical_chief_id = user.technical_chief.id
      }
      // tanteo de tasa de crecimiento para saber con cual tasa el último día le llego cerca al peso objetivo
      let testLiveweight = 0
      let testGrowthRate = 0 // Tasa de crecimiento tanteada
      const stockWeight = parseInt(body.stock_weight)
      while (testLiveweight < body.harvest_target_weight) {
        testGrowthRate++
        testLiveweight = (((0.0231 * Math.pow(body.cultivation_days + stockWeight, 1.3758)) * (testGrowthRate / 100)))
      }
      let finalWeightPre = (((0.0231 * Math.pow(body.cultivation_days + stockWeight, 1.3758)) * ((testGrowthRate - 1) / 100)))
      let finalWeightPost = (((0.0231 * Math.pow(body.cultivation_days + stockWeight, 1.3758)) * ((testGrowthRate) / 100)))

      let diffFinalWeightPre = body.harvest_target_weight - finalWeightPre
      let diffFinalWeightPost = finalWeightPost - body.harvest_target_weight

      if (diffFinalWeightPre < diffFinalWeightPost) {
        testGrowthRate-- // Si es peso de abajo es más cercano entonces usar la tasa obtenida menos 1
      }

      body.growth_rate = testGrowthRate
      const foodProjectionMaster = await FoodProjectionMaster.create(body)
      // Guardar en tabla de detalles de la proyección

      //  Peso Vivo = (0.0231 * día^1.3758) * tasa de crecimiento/100
      // Tasa de biomasa = ((4.2847 * Peso Vivo ^ -0.2869) * 1.3337 + 2.3) * tasa de alimentación/100
      // Sobrevivencia primeros 30 días = ((100-Supervivencia < 30 días)/100) - sobrevivencia del día anterior
      // Sobrevivencia después 30 días = ((100-Supervivencia > 30 días)/100) - sobrevivencia del día anterior
      // Biomasa = (Peso Vivo/1000)*(sobrevivencia/100)*Numero de animales
      let previousDaySurvival = 0
      let survivalDay29 = 0 // Sobrevivencia del día 29

      const feeding_rate = foodProjectionMaster.feeding_rate
      const animal_numbers = foodProjectionMaster.density * poolArea

      // Se deben guardar los tipos de alimentos asociados a la proyección recién creada para que los rangos sean independientes
      const foodTypes = (await Product.all()).toJSON()
      for (const element of foodTypes) {
        await FoodProjectionFoodType.create({
          food_projection_id: foodProjectionMaster.id,
          food_type_id: element.id,
          from_weight: element.from_weight,
          to_weight: element.to_weight,
          price_kg: element.price_kg
        })
      }

      const liveweights = ['']
      for (let i = 1; i <= body.cultivation_days; i++) {
        // Hasta el día 12 se usa la fórmula lógica tomando datos de la misma fila
        let liveweight = (((0.0231 * Math.pow(i + stockWeight, 1.3758)) * (testGrowthRate / 100)))
        liveweights[i] = liveweight
        if (i < 13) {
          // var biomass_rate = (4.2847 * Math.pow(liveweight, -0.2869)) * (feeding_rate / 100)
          // Cambiaron nuevamente a J por I. Tasa biomasa = (J35*1.3337)+2.3
          var biomass_rate = ((4.2847 * Math.pow(liveweight, -0.2869)) * 1.3337) + 2.3
        } else {
          var biomass_rate = (4.2847 * Math.pow(liveweights[i-10], -0.2869)) * (feeding_rate / 100)
        }

        //Cálculo de supervivencia
        let survival
        if (i < 30) {
          if (i === 1) {
            survival = 100
          } else {
            survival = previousDaySurvival -((100 - foodProjectionMaster.survival) / 100)
          }

        } else {
          // Si es igual a 30 guardo la sobrevivencia anterior(día 29)
          if (i === 30) {
            survivalDay29 = previousDaySurvival // Una sola vez se ejecuta
          }
          survival = previousDaySurvival - ((survivalDay29 - foodProjectionMaster.survival_after30days) / (body.cultivation_days - 29))
          //survival = previousDaySurvival -((100 - foodProjectionMaster.survival_after30days) / 100) Cálculo viejo
          // La idea es que en el último día dé la supervivencia que se ingresó en supervivencia después de 30
        }
        const biomass = (liveweight / 1000) * (survival/100) * animal_numbers
        const food_day = (biomass_rate/100) * biomass

        // redondeo del peso vivo después de usarlo en las fórmulas
        liveweight = liveweight.toFixed(2)

        // const food_type = await FoodType.query().where("from_weight", '<=',liveweight).where("to_weight", '>=',liveweight).first()
        const food_type = await FoodProjectionFoodType.query().with('foodName')
        .where('food_projection_id', foodProjectionMaster.id).where("from_weight", '<=', liveweight)
        .where("to_weight", '>=', liveweight).first()
        // console.log(food_type, 'Datos')

        await FoodProjectionDetail.create({
          food_projection_master_id: foodProjectionMaster.id,
          day: i,
          food_day,
          biomass_rate,
          liveweight,
          food_type: food_type && food_type.foodName ? food_type.toJSON().foodName.name : 'No hay alimento definido en este rango',
          survival,
          biomass
        })
        previousDaySurvival = survival
      }
      response.send(foodProjectionMaster)
    }
  }

  /**
   * Display a single foodprojection.
   * GET foodprojections/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
    const foodProjection = (await FoodProjectionMaster.query().with('details').where('id', params.id)
    .with('foodTypes.foodName').with('pool')
    .with("details", (builder) => {
      builder.orderBy("id");
    })
    .with("foodTypes", (builder) => {
      builder.orderBy("from_weight");
    })
    .first()).toJSON()
    foodProjection.seedtime = moment(foodProjection.seedtime).format("YYYY-MM-DD")
    foodProjection.chart = {}
    foodProjection.chart.labels = []
    foodProjection.chart.datasets = [
      {
        label: 'Alimento/día proyectado/Ha',
        backgroundColor: '#1976d2',
        borderColor: '#1976d2',
        fill: false,
        data: foodProjection.details.map(val => (val.food_day / foodProjection.pool_area).toFixed(3))
      },
      {
        label: 'Alimento/día Real/Ha',
        backgroundColor: '#21ba45',
        borderColor: '#21ba45',
        fill: false,
        data: foodProjection.details.map(val => (val.real_food_day / foodProjection.pool_area).toFixed(3))
      },
      {
        label: 'Peso vivo proyectado',
        backgroundColor: '#9c27b0',
        borderColor: '#9c27b0',
        fill: false,
        data: foodProjection.details.map(val => val.liveweight)
      },
      {
        label: 'Peso vivo real',
        backgroundColor: '#f2c037',
        borderColor: '#f2c037',
        fill: false,
        data: foodProjection.details.map(val => val.real_liveweight)
      }
    ]
    for (let i = 1; i <= foodProjection.cultivation_days; i++) {
      foodProjection.chart.labels.push(i)
    }

    foodProjection.details.forEach(element => {
      element.food_day = parseFloat(element.food_day).toFixed()
      element.real_food_day = parseFloat(element.real_food_day).toFixed()
    })
    const projectionCalculatedData = FoodProjectionFunctions.calculateFC (foodProjection.details, foodProjection.density, foodProjection.pool_area, foodProjection.cultivation_days, foodProjection.harvest_target_weight)
    foodProjection.fc = projectionCalculatedData.fc
    foodProjection.kilosFood = projectionCalculatedData.kilosFood
    foodProjection.poundsHarvest = projectionCalculatedData.poundsHarvest

    response.send(foodProjection)
  }

  /**
   * Update foodprojection details.
   * PUT or PATCH foodprojections/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const wholeRequest = request.all()
    const validation = await validate(request.all(), FoodProjectionMaster.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const body = request.only(FoodProjectionMaster.fillable)

      // tanteo de tasa de crecimiento para saber con cual tasa el último día le llego cerca al peso objetivo
      let testLiveweight = 0
      let testGrowthRate = 0 // Tasa de crecimiento tanteada
      const stockWeight = parseInt(body.stock_weight)
      while (testLiveweight < body.harvest_target_weight) {
        testGrowthRate++
        testLiveweight = (((0.0231 * Math.pow(body.cultivation_days + stockWeight, 1.3758)) * (testGrowthRate / 100)))
      }
      let finalWeightPre = (((0.0231 * Math.pow(body.cultivation_days + stockWeight, 1.3758)) * ((testGrowthRate - 1) / 100)))
      let finalWeightPost = (((0.0231 * Math.pow(body.cultivation_days + stockWeight, 1.3758)) * ((testGrowthRate) / 100)))

      let diffFinalWeightPre = body.harvest_target_weight - finalWeightPre
      let diffFinalWeightPost = finalWeightPost - body.harvest_target_weight

      if (diffFinalWeightPre < diffFinalWeightPost) {
        testGrowthRate-- // Si es peso de abajo es más cercano entonces usar la tasa obtenida menos 1
      }


      // Se actualiza tabla maestro
      body.growth_rate = testGrowthRate
      await FoodProjectionMaster.query().where('id', params.id).update(body)
      const foodProjectionMaster = await FoodProjectionMaster.query().where('id', params.id).first()
      // Se debe modificar la tabla de detalles de la proyección

      // Eliminar todos los registros del detalle, se deben respaldar los valores reales
      const detailBackup = (await FoodProjectionDetail.query().where('food_projection_master_id', foodProjectionMaster.id).fetch()).toJSON()
      await FoodProjectionDetail.query().where('food_projection_master_id', foodProjectionMaster.id).delete()

      let previousDaySurvival = 0
      let survivalDay29 = 0 //Sobrevivencia del día 29

      // const weeklyGrowth = (foodProjectionMaster.initial_biomass ? (foodProjectionMaster.harvest_target_weight - foodProjectionMaster.stock_weight) / foodProjectionMaster.cultivation_days * 7 : (foodProjectionMaster.harvest_target_weight / foodProjectionMaster.cultivation_days) * 7)
      const feeding_rate = foodProjectionMaster.feeding_rate
      const animal_numbers = foodProjectionMaster.density * foodProjectionMaster.pool_area

      const liveweights = ['']
      for (let i = 1; i <= body.cultivation_days; i++) {
        // Hasta el día 12 se usa la fórmula lógica tomando datos de la misma fila
        let liveweight = (((0.0231 * Math.pow(i + stockWeight, 1.3758)) * (testGrowthRate / 100)))
        liveweights[i] = liveweight
        if (i < 13) {
          // var biomass_rate = (4.2847 * Math.pow(liveweight, -0.2869)) * (feeding_rate / 100)
          // Cambiaron nuevamente a J por I. Tasa biomasa = (J35*1.3337)+2.3
          var biomass_rate = ((4.2847 * Math.pow(liveweight, -0.2869)) * 1.3337) + 2.3
        } else {
          var biomass_rate = (4.2847 * Math.pow(liveweights[i-10], -0.2869)) * (feeding_rate / 100)
        }

        // Cálculo de supervivencia
        let survival
        if (i < 30) {
          if (i === 1) {
            survival = 100
          } else {
            survival = previousDaySurvival -((100 - foodProjectionMaster.survival) / 100)
          }

        } else {
          // Si es igual a 30 guardo la sobrevivencia anterior(día 29)
          if (i === 30) {
            survivalDay29 = previousDaySurvival // Una sola vez se ejecuta
          }
          survival = previousDaySurvival - ((survivalDay29 - foodProjectionMaster.survival_after30days) / (body.cultivation_days - 29))
          // La idea es que en el último día dé la supervivencia que se ingresó en supervivencia después de 30
        }
        const biomass = (liveweight / 1000) * (survival/100) * animal_numbers
        const food_day = (biomass_rate/100) * biomass

        // redondeo del peso vivo después de usarlo en las fórmulas
        liveweight = liveweight.toFixed(2)

        // const food_type = await FoodType.query().where("from_weight", '<=',liveweight).where("to_weight", '>=',liveweight).first()
        const food_type = await FoodProjectionFoodType.query().with('foodName')
        .where('food_projection_id', foodProjectionMaster.id)
        .where("from_weight", '<=',liveweight).where("to_weight", '>=',liveweight).first()
        const real_food_day = detailBackup.find(val => val.day == i) ? detailBackup.find(val => val.day == i).real_food_day : null
        const real_biomass_rate = detailBackup.find(val => val.day == i) ? detailBackup.find(val => val.day == i).real_biomass_rate : null
        const real_liveweight = detailBackup.find(val => val.day == i) ? detailBackup.find(val => val.day == i).real_liveweight : null
        const real_food_type = detailBackup.find(val => val.day == i) ? detailBackup.find(val => val.day == i).real_food_type : null
        const real_survival = detailBackup.find(val => val.day == i) ? detailBackup.find(val => val.day == i).real_survival : null
        await FoodProjectionDetail.create({
          food_projection_master_id: foodProjectionMaster.id,
          day: i,
          food_day,
          biomass_rate,
          liveweight,
          food_type: food_type && food_type.foodName ? food_type.toJSON().foodName.name : 'No hay alimento definido en este rango',
          survival,
          biomass,
          real_food_day,
          real_biomass_rate,
          real_liveweight,
          real_food_type,
          real_survival
        })
        previousDaySurvival = survival
      }
      const foodProjection = await FoodProjectionMaster.query().with('details').where('id', foodProjectionMaster.id).first()
      response.send(foodProjection)
    }
  }

  /**
   * Delete a foodprojection with id.
   * DELETE foodprojections/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }

  /**
   * Consultar los datos de la proyección de un día específico
   * GET food_projections_detail/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async getProjectionDay ({ params, response }) {
    const day = await FoodProjectionDetail.find(params.id)
    response.send(day)
  }

  /**
   * Actualizar los datos de la proyección de un día específico
   * PUT food_projections_detail/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async updateProjectionDay ({ params, request, response }) {
    const { real_food_day, real_biomass_rate, real_liveweight, real_food_type, real_survival } = request.all()
    await FoodProjectionDetail.query().where('id', params.id).update({
      real_food_day,
      real_biomass_rate,
      real_liveweight,
      real_food_type,
      real_survival
    })
    response.send(true)
  }

  async getBulkLoadFormat ({ request, response }) {
    const params = request.get()
    const foodProjection = (await FoodProjectionMaster.query().with('pool.shrimp.customer').with('foodTypes.foodName')
    .with('details', (query) => {
      query.orderBy('day')
    })
    .with('foodTypes', (query) => {
      query.orderBy('from_weight')
    })
    .where('id', params.id).first()).toJSON()
    /* const detailsProjection = (await FoodProjectionDetail.query().where('food_projection_master_id', params.id)
    .orderBy('day').fetch()).toJSON() */
   /*

    const workbook = new ExcelJS.Workbook()
    const worksheet = workbook.addWorksheet("Datos proyectados vs reales")
    // Títulos y valores del encabezado
    worksheet.getCell('B1').value = 'Cliente:'
    worksheet.getCell('B2').value = 'Fecha de Siembra:'
    worksheet.getCell('B3').value = 'Fecha de Cosecha:'

    worksheet.getCell('C1').value = foodProjection.pool.shrimp.customer.legal_name
    worksheet.getCell('C2').value = moment(foodProjection.seedtime).format('DD-MM-YYYY')
    worksheet.getCell('C3').value = moment(foodProjection.seedtime).add(foodProjection.cultivation_days - 1, 'days').format('DD-MM-YYYY')

    worksheet.getCell('D1').value = 'Camaronera:'
    worksheet.getCell('D2').value = 'Peso de Siembra:'
    worksheet.getCell('D3').value = 'Peso objetivo:'

    worksheet.getCell('E1').value = foodProjection.pool.shrimp.name
    worksheet.getCell('E2').value = plantingWeigth(foodProjection.harvest_target_weight, foodProjection.cultivation_days, foodProjection.stock_weight)
    worksheet.getCell('E3').value = foodProjection.harvest_target_weight

    worksheet.getCell('F1').value = 'Piscina'

    worksheet.getCell('G1').value = foodProjection.pool.description

    worksheet.getCell('H1').value = 'Área'

    worksheet.getCell('I1').value = foodProjection.pool.area

    // Header de tabla
    worksheet.insertRow(4,[
      'ID', 'Día', 'Alimento/Día - proyección (kg)', 'Alimento/Día - real (kg)', 'Tasa de biomasa - proyección (%)',
      'Tasa de biomasa - real (%)', 'Peso vivo - proyección (g)', 'Peso vivo - real (g)', 'Sobrevivencia - proyección (%)',
      'Sobrevivencia - real (%)'
    ])
    worksheet.getColumn('B').width = 18
    worksheet.getColumn('C').width = 28
    worksheet.getColumn('D').width = 22
    worksheet.getColumn('E').width = 30
    worksheet.getColumn('F').width = 24
    worksheet.getColumn('G').width = 25
    worksheet.getColumn('H').width = 23
    worksheet.getColumn('I').width = 28
    worksheet.getColumn('J').width = 23
    worksheet.getRow(4).font = { bold: true };

    // Inserción de registros
    detailsProjection.forEach((element, key) => {
      const record = [
        element.id, element.day, parseFloat(element.food_day).toFixed(), element.real_food_day, element.biomass_rate, element.real_biomass_rate,
        element.liveweight, element.real_liveweight, element.survival, element.real_survival
      ]
      worksheet.insertRow(key + 5, record)

      // Relleno amarillo para celdas de valores reales
      worksheet.getCell(`D${key + 5}`).fill = {
        type: 'pattern',
        pattern:'solid',
        fgColor:{argb:'FFFFFF00'},
        bgColor:{argb:'ffff00'}
      }
      worksheet.getCell(`F${key + 5}`).fill = {
        type: 'pattern',
        pattern:'solid',
        fgColor:{argb:'FFFFFF00'},
        bgColor:{argb:'ffff00'}
      }
      worksheet.getCell(`H${key + 5}`).fill = {
        type: 'pattern',
        pattern:'solid',
        fgColor:{argb:'FFFFFF00'},
        bgColor:{argb:'ffff00'}
      }
      worksheet.getCell(`J${key + 5}`).fill = {
        type: 'pattern',
        pattern:'solid',
        fgColor:{argb:'FFFFFF00'},
        bgColor:{argb:'ffff00'}
      }
    });

    // res is a Stream object
    response.header(
      "Content-Type",
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    );
    response.header(
      "Content-Disposition",
      "attachment; filename=" + "Carga masiva - proyeccion alimentacion.xlsx"
    );

    await workbook.xlsx.writeFile(Helpers.appRoot('Carga masiva - proyeccion alimentacion.xlsx'))

    response.download(Helpers.appRoot('Carga masiva - proyeccion alimentacion.xlsx')) */
    var workbook = new ExcelJS.Workbook();

await workbook.xlsx.readFile('resources/templates/plantila_proyeccion_alimentacion.xlsx')
    .then(function() {
      console.log('cargo archivo')
        var worksheet = workbook.getWorksheet(1)
        worksheet.name = foodProjection.pool.description
        // var worksheetFood = workbook.getWorksheet(5)

        const worksheetChartData = workbook.addWorksheet('Datos')

        // Hoja de alimentos
        /* foodProjection.foodTypes.forEach((element, key) => {
          console.log(`B${key + 4}`)
          worksheetFood.getCell(`B${key + 4}`).value = parseFloat(element.from_weight).toFixed(2)
          worksheetFood.getCell(`C${key + 4}`).value = parseFloat(element.to_weight).toFixed(2)

          worksheetFood.getCell(`D${key + 4}`).value = element.foodName.name
          worksheetFood.getCell(`E${key + 4}`).value = element.price_kg ? parseFloat(element.price_kg).toFixed(2) : ''
        }) */

        // Hoja de proyección
        worksheet.getCell('B1').value = `CAMARONERA: ${foodProjection.pool.shrimp.name}`
        worksheet.getCell('C4').value = foodProjection.pool.description
        worksheet.getCell('G4').value =  { formula: `DATEVALUE("${moment(foodProjection.seedtime).format('DD/MM/YYYY')}")`}
        worksheet.getCell('J4').model.result = undefined
        worksheet.getCell('C5').value = parseFloat(foodProjection.pool.area)
        worksheet.getCell('G5').value = foodProjection.density
        worksheet.getCell('L5').value = parseFloat(foodProjection.details[foodProjection.details.length - 1].survival) + '%'
        worksheet.getCell('N4').model.result = undefined
        worksheet.getCell('N5').value = foodProjection.cultivation_days
        worksheet.getCell('P5').value = parseFloat(foodProjection.harvest_target_weight)
        worksheet.getCell('L4').value = { formula: '(P5-M9)/N5*7' }
        worksheet.getCell('J5').value = { formula: '+C5*G5*L5*P5/454' }

        // Insertar registros
        let acumFoodDay = 0
        let totalDollarAcum = 0
        let lastRealLiveWeightCell = 0
        let realLiveweightCounter = []
        foodProjection.details.forEach((element, key) => {
          const food = foodProjection.foodTypes.find(val => parseFloat(val.from_weight) <= parseFloat(element.liveweight) && parseFloat(val.to_weight) >= parseFloat(element.liveweight))
          worksheet.getCell(`A${key + 9}`).value = key + 1 // Número de día
          if (key === 0) { // Si es el primer día
            worksheet.getCell('B9').value = { formula: '+G4' } // El primer día, día de siembra
            //worksheet.getCell(`E${key + 9}`).value = { formula: 'Alimento!E4' }
            worksheet.getCell(`I${key + 9}`).value = { formula: `D${key + 9}` }
            worksheet.getCell(`J${key + 9}`).value = { formula: `G${key + 9}` }
          } else {
            worksheet.getCell(`A${key + 9}`).style = worksheet.getCell(`A${key + 8}`).style // Copiar el estilo de la celda en la fila anterior
            worksheet.getCell(`B${key + 9}`).value = { formula: `+B${key + 8}+1` }
            worksheet.getCell(`B${key + 9}`).style = worksheet.getCell(`B${key + 8}`).style // Copiar el estilo de la celda en la fila anterior
            // worksheet.getCell(`C${key + 9}`).value = { formula: `IF(N${key + 9}>0,VLOOKUP(N${key + 9},Alimento!B$4:D$${foodProjection.foodTypes.length + 3},3,-1),VLOOKUP(M${key + 9},Alimento!B$4:D$${foodProjection.foodTypes.length + 3},3,-1))` } // Fórmula para buscar el alimento correspondiente según peso proyectado o real
            worksheet.getCell(`C${key + 9}`).style = worksheet.getCell(`C${key + 8}`).style // Copiar el estilo de la celda en la fila anterior
            worksheet.getCell(`D${key + 9}`).style = worksheet.getCell(`D${key + 8}`).style // Copiar el estilo de la celda en la fila anterior
            /* worksheet.getCell(`E${key + 9}`).value = food ? parseFloat(food.price_kg) : undefined
            worksheet.getCell(`E${key + 9}`).style = worksheet.getCell(`E${key + 8}`).style
            worksheet.getCell(`F${key + 9}`).style = worksheet.getCell(`F${key + 8}`).style */
            worksheet.getCell(`G${key + 9}`).style = worksheet.getCell(`G${key + 8}`).style
            worksheet.getCell(`H${key + 9}`).style = worksheet.getCell(`H${key + 8}`).style
            worksheet.getCell(`I${key + 9}`).style = worksheet.getCell(`I${key + 8}`).style
            worksheet.getCell(`I${key + 9}`).value = { formula: `D${key + 9}+I${key + 8}` }
            worksheet.getCell(`J${key + 9}`).style = worksheet.getCell(`J${key + 8}`).style
            worksheet.getCell(`J${key + 9}`).value = { formula: `G${key + 9}+J${key + 8}` }
            worksheet.getCell(`K${key + 9}`).style = worksheet.getCell(`K${key + 8}`).style
            worksheet.getCell(`L${key + 9}`).style = worksheet.getCell(`L${key + 8}`).style
            worksheet.getCell(`M${key + 9}`).style = worksheet.getCell(`M${key + 8}`).style
            worksheet.getCell(`N${key + 9}`).style = worksheet.getCell(`N${key + 8}`).style
            worksheet.getCell(`O${key + 9}`).style = worksheet.getCell(`O${key + 8}`).style
            worksheet.getCell(`P${key + 9}`).style = worksheet.getCell(`P${key + 8}`).style
            worksheet.getCell(`Q${key + 9}`).style = worksheet.getCell(`Q${key + 8}`).style
            worksheet.getCell(`R${key + 9}`).style = worksheet.getCell(`R${key + 8}`).style
            worksheet.getCell(`S${key + 9}`).style = worksheet.getCell(`S${key + 8}`).style
          }
          worksheet.getCell(`C${key + 9}`).value = element.food_type
          worksheet.getCell(`D${key + 9}`).value = parseFloat(element.food_day)
          acumFoodDay += parseFloat(element.food_day)
          totalDollarAcum += ((food ? parseFloat(food.price_kg) : 0) * parseFloat(element.food_day))

          //worksheet.getCell(`F${key + 9}`).value = { formula: `E${key + 9}*D${key + 9}` } // Recalcular según fórmula
          worksheet.getCell(`G${key + 9}`).value = !isNaN(parseFloat(element.real_food_day)) ? parseFloat(element.real_food_day) : undefined
          worksheet.getCell(`H${key + 9}`).value = { formula: `G${key + 9}/$C$5` }
          worksheet.getCell(`M${key + 9}`).value = parseFloat(element.liveweight)

          const realLiveweight = parseFloat(element.real_liveweight)
          if (!isNaN(realLiveweight)) { // Si hay peso vivo real hay que registrar crecimiento comparándolo con el último registrado
            realLiveweightCounter.push(`N${key + 9}`)
            worksheet.getCell(`N${key + 9}`).value = realLiveweight

            worksheetChartData.getCell(`E${key + 1}`).value = realLiveweight

            worksheet.getCell(`O${key + 9}`).value = lastRealLiveWeightCell ? { formula: `N${key + 9}-${lastRealLiveWeightCell}`} : ''
            lastRealLiveWeightCell = `N${key + 9}`

            if (realLiveweightCounter.length > 4) { // Si van más de 5 pesos vivos reales se calculan los últimos 4
              worksheet.getCell(`P${key + 9}`).value = { formula: `(N${key + 9}-${realLiveweightCounter[realLiveweightCounter.length - 5]})/4` }
            }

            worksheet.getCell(`Q${key + 9}`).value = { formula: `(N${key + 9}-$M$9)/A${key + 9}*7` }
          }

          worksheet.getCell(`R${key + 9}`).value = element.real_survival > 0 ? element.real_survival / 100 : undefined
          // worksheet.getCell(`S${key + 9}`).value = element.real_survival > 0 && !isNaN(realLiveweight) ? { formula: `$G$5*R${key + 9}*N${key + 9}/454` } : '' // Si hay peso y superviv real se muestra el campo de biomasa
          worksheet.getCell(`S${key + 9}`).value = { formula: `IF(AND(R${key + 9}>0,N${key + 9}>0),$G$5*R${key + 9}*N${key + 9}/454,"")` }

          // Dejar de último porque depende de varios valores del registro
          // worksheet.getCell(`C${key + 9}`).model.result = undefined // Recalcular según fórmula

          // Si estamos en el último día hay que establecer el aŕea de impresión
          if (key == foodProjection.details.length - 1) {
            worksheet.pageSetup.printArea = `A1:S${key + 9}`
          }

          // Valores para la pestaña de datos
          worksheetChartData.getCell(`A${key + 1}`).value = key + 1
          worksheetChartData.getCell(`B${key + 1}`).value = parseFloat(element.food_day)
          worksheetChartData.getCell(`C${key + 1}`).value = !isNaN(parseFloat(element.real_food_day)) ? parseFloat(element.real_food_day) : undefined
          worksheetChartData.getCell(`D${key + 1}`).value = parseFloat(element.liveweight)

        })
        worksheet.getCell(`D${foodProjection.cultivation_days + 9}`).font = {
          color: { argb: 'FFFFFF' }
        }
        worksheet.getCell(`D${foodProjection.cultivation_days + 9}`).value = { formula: `SUM(D9:D${foodProjection.cultivation_days + 8})` } // Suma de kilos proyectados
        // worksheet.getCell(`F${foodProjection.cultivation_days + 9}`).value = totalDollarAcum // Suma de gasto en alimento
        worksheet.getCell('R4').value = { formula: `D${foodProjection.cultivation_days + 9}` }
        worksheet.getCell('R5').value = { formula: `${totalDollarAcum}/D${foodProjection.cultivation_days + 9}`} // Recalcular según fórmula
        worksheet.getCell('U1').value = foodProjection.id
        worksheet.getCell('P4').value = { formula: `(D${foodProjection.cultivation_days + 9}*2.2046)/J5` }


        return workbook.xlsx.writeFile(`Piscina ${foodProjection.pool.description} - ${foodProjection.pool.shrimp.name} - Proyección alimentacion.xlsx`);
    })
    response.download(Helpers.appRoot(`Piscina ${foodProjection.pool.description} - ${foodProjection.pool.shrimp.name} - Proyección alimentacion.xlsx`))
  }

  async uploadBulkLoadFormat ({ request, response }) {
    const projectionId = request.input('projection_id')
    // Consultar proyección
    const projection = (await FoodProjectionMaster.query().where('id', projectionId).first()).toJSON()

    let dir = `storage/uploads/customers`
    let showingDir = Helpers.appRoot(dir) + '/'

    const bulkLoad = request.file('file', {
      types: ['vnd.openxmlformats-officedocument.spreadsheetml.sheet'],
      size: '40mb'
    })
    let fileName = bulkLoad.clientName.replace(/\s/g, '')
    fileName = fileName.split('-').join('')
    if (bulkLoad) {
      await bulkLoad.move(Helpers.appRoot(dir), {
        name: 'ArchivoTemporal.xlsx',
        overwrite: true
      })

      if (!bulkLoad.moved()) {
        return bulkLoad.error()
      }
      showingDir += 'ArchivoTemporal.xlsx'

      // Leer excel
      const workbook = new ExcelJS.Workbook();
      await workbook.xlsx.readFile(showingDir);
      var worksheet = workbook.getWorksheet(1)
      var rows = []

      worksheet.eachRow(row => {
        var cells = []
        row.eachCell({
          includeEmpty: true
        }, (cell, key) => {
          cells.push(cell.value)
        })
        rows.push(cells)
      })
      rows.shift()
      // return rows

      // Recorrer cada fila para hacer la modificación
      let key = 0
      for (const i of rows) {
        key++
        if (key > 3) {
          let real_food_day, real_biomass_rate, real_liveweight, real_survival, liveweight
          // Solo insertar filas debajo del encabezado
          if (typeof i[6] === 'object' && i[6] !== null) { // Si es objeto es porque tiene fórmula
            real_food_day = i[6].result
          } else {
            real_food_day = i[6]
          }

          if (typeof i[5] === 'object' && i[5] !== null) { // Si es objeto es porque tiene fórmula
            real_biomass_rate = i[5].result
          } else {
            real_biomass_rate = i[5]
          }

          if (typeof i[12] === 'object' && i[12] !== null) { // Si es objeto es porque tiene fórmula. Peso vivo
            liveweight = i[12].result
          } else {
            liveweight = i[12]
          }

          if (typeof i[13] === 'object' && i[13] !== null) { // Si es objeto es porque tiene fórmula
            real_liveweight = i[13].result
          } else {
            real_liveweight = i[13]
          }

          if (typeof i[17] === 'object' && i[17] !== null) { // Si es objeto es porque tiene fórmula
            real_survival = i[17].result
          } else {
            real_survival = i[17]
          }
          const day = !isNaN(parseFloat(i[0])) ? parseFloat(i[0]) : undefined
          const record = {
            day,
            real_food_day: !isNaN(parseFloat(real_food_day))? parseFloat(real_food_day) : undefined,
            // real_biomass_rate: !isNaN(parseFloat(real_biomass_rate))? parseFloat(real_biomass_rate) : undefined,
            real_liveweight: !isNaN(parseFloat(real_liveweight))? parseFloat(real_liveweight) : undefined,
            real_survival: !isNaN(parseFloat(real_survival))? parseFloat(real_survival) * 100 : undefined,
            liveweight: !isNaN(parseFloat(liveweight)) && !projection.initial_biomass && day < 31 ? parseFloat(liveweight) : undefined
          }

          // arrayEntendible.push(record)
          if (day) {

            if (day == 90) {
              console.log({record})
            }
            await FoodProjectionDetail.query().where('food_projection_master_id', projectionId).where('day', day).update(record)
          }

        }
      }
    return rows
    }
  }
  async setProjectionFoodTypes ({ params, request }) {
    const requestRecords = request.input('foodTypes')
    const projectionId = params.projectionId

    let activeFoodIds = []
    for (const i of requestRecords) {
      let food = {}
        if (i.id) { // Si es un alimento que ya existía, solo se debe modificar
          await FoodProjectionFoodType.query().where('id', i.id).update({
            from_weight: i.from_weight,
            to_weight: i.to_weight,
            food_type_id: i.food_type_id,
            price_kg: i.price_kg ? i.price_kg : null
          })
          activeFoodIds.push(i.id)
        } else { // Si es un alimento nuevo
          food = await FoodProjectionFoodType.create({
            food_projection_id: projectionId,
            from_weight: i.from_weight,
            to_weight: i.to_weight,
            food_type_id: i.food_type_id,
            price_kg: i.price_kg ? i.price_kg : undefined
          })
          activeFoodIds.push(food.id)
        }
    }

    // Eliminar alimentos de proyección que no sean los que se acaban de modificar o crear
    await FoodProjectionFoodType.query().where('food_projection_id', projectionId).whereNotIn('id', activeFoodIds).delete()
    await updateFoodProjectionDetails(projectionId)
    /* const foodProjectionMaster = await FoodProjectionMaster.query().where('id', projectionId).first()

    for (const element of requestRecords) {
      await FoodProjectionFoodType.query().where('id', element.id).update({
        from_weight: element.from_weight,
        to_weight: element.to_weight
      })
    }

    // Eliminar todos los registros del detalle, se deben respaldar los valores reales
    const detailBackup = (await FoodProjectionDetail.query().where('food_projection_master_id', projectionId).fetch()).toJSON()
    await FoodProjectionDetail.query().where('food_projection_master_id', projectionId).delete()

    for (let i = 1; i <= foodProjectionMaster.cultivation_days; i++) {
      const liveweight = detailBackup.find(val => val.day == i) ? detailBackup.find(val => val.day == i).liveweight : null
      const food_day = detailBackup.find(val => val.day == i) ? detailBackup.find(val => val.day == i).food_day : null
      const biomass_rate = detailBackup.find(val => val.day == i) ? detailBackup.find(val => val.day == i).biomass_rate : null
      const survival = detailBackup.find(val => val.day == i) ? detailBackup.find(val => val.day == i).survival : null
      const biomass = detailBackup.find(val => val.day == i) ? detailBackup.find(val => val.day == i).biomass : null


      const real_food_day = detailBackup.find(val => val.day == i) ? detailBackup.find(val => val.day == i).real_food_day : null
      const real_biomass_rate = detailBackup.find(val => val.day == i) ? detailBackup.find(val => val.day == i).real_biomass_rate : null
      const real_liveweight = detailBackup.find(val => val.day == i) ? detailBackup.find(val => val.day == i).real_liveweight : null
      const real_food_type = detailBackup.find(val => val.day == i) ? detailBackup.find(val => val.day == i).real_food_type : null
      const real_survival = detailBackup.find(val => val.day == i) ? detailBackup.find(val => val.day == i).real_survival : null

      const food_type = await FoodProjectionFoodType.query().with('foodName')
      .where('food_projection_id', projectionId)
      .where("from_weight", '<=',liveweight).where("to_weight", '>=',liveweight).first()
      console.log(food_type)

      await FoodProjectionDetail.create({
        food_projection_master_id: projectionId,
        day: i,
        food_day,
        biomass_rate,
        liveweight,
        food_type: food_type && food_type.foodName ? food_type.toJSON().foodName.name : 'No hay alimento definido en este rango',
        survival,
        biomass,
        real_food_day,
        real_biomass_rate,
        real_liveweight,
        real_food_type,
        real_survival
      })
    } */

    return true
  }

  async updateChart ({ request, params }) {
    const { chart_image } = request.all()
    await FoodProjectionMaster.query().where('id', params.id).update({
      chart_image
    })
  }

  async deleteFoodTypes ({ params, request, response }) {
    const { projectionId } = request.all()
    await FoodProjectionFoodType.query().where('id', params.id).delete()
    // Hay que actualizar la proyección para que se muestren los alimentos según los que estén en dicha proyección
    await updateFoodProjectionDetails(projectionId)
    response.send(true)

  }
}
/**
* Función para actualizar el alimento de cada día en una proyección según los cambios realizados a los alimentos de dicha proyección
*/
async function updateFoodProjectionDetails(projectionId) {
  const projection = (await FoodProjectionMaster.query().with('details').where('id', projectionId).first()).toJSON()
  for (const day of projection.details) {
    const liveweight = day.liveweight
    const foodType = await FoodProjectionFoodType.query().with('foodName')
    .where('food_projection_id', projection.id).where("from_weight", '<=',liveweight)
    .where("to_weight", '>=',liveweight).first()

    const food_type = foodType && foodType.foodName ? foodType.toJSON().foodName.name : 'No hay alimento definido en este rango'
    // Actualizar el alimento de ese día
    await FoodProjectionDetail.query().where('id', day.id).update({food_type})
  }
}

module.exports = FoodProjectionController
