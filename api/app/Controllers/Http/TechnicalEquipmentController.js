'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with technicalequipments
 */

const TechnicalEquipment = use('App/Models/TechnicalEquipment')
const TechnicalChief = use('App/Models/TechnicalChief')
const List = use('App/Functions/List')
const { validate } = use('Validator')

class TechnicalEquipmentController {
  async index ({ request, response, view }) {
    let technical_equipments = (await TechnicalEquipment.query().orderBy('id').fetch()).toJSON()
    technical_equipments = List.addRowActions(technical_equipments, [{name: 'edit', route: 'technical_equipments/form/', permission: '5.3.5'}, {name: 'delete', permission: '5.3.4'}, { name: 'activateTechnicalEquipment', permission: '5.3.6' }])
    for (let j in technical_equipments) {
      let chiefId = technical_equipments[j].technical_chief
      let chiefName = (await TechnicalChief.query().where('id', chiefId).fetch()).toJSON()
      technical_equipments[j].technical_chief_name = chiefName[0] ? chiefName[0].full_name : ''
      if (technical_equipments[j].status) {
        technical_equipments[j].estatus = 'Activo'
        technical_equipments[j].actions[2].title = 'Inactivar'
        technical_equipments[j].actions[2].color = 'negative'
      } else {
        technical_equipments[j].estatus = 'Inactivo'
      }
    }
    response.send(technical_equipments)
  }


  async store ({ request, response }) {
    const allRequest = request.all()
    const validation = await validate(allRequest, TechnicalEquipment.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const body = request.only(TechnicalEquipment.fillable)
      console.log(body, 'bodyyy')
      body.status = true
      body.zone = JSON.stringify(allRequest.zone)
      body.technical_advisors = JSON.stringify(allRequest.technical_advisors)
      body.technical_chief = JSON.stringify(allRequest.technical_chief)
      const technical_equipments = await TechnicalEquipment.create(body)
      response.send(technical_equipments);
    }
  }

  async show ({ params, request, response, view }) {
    let technical_equipments = (await TechnicalEquipment.query().where('id', params.id).first()).toJSON()
    let zone = technical_equipments.zone
    let technical_chief = technical_equipments.technical_chief
    let technical_advisors = technical_equipments.technical_advisors
    technical_equipments.zone = JSON.parse(zone)
    technical_equipments.technical_chief = JSON.parse(technical_chief)
    technical_equipments.technical_advisors = JSON.parse(technical_advisors)
    response.send(technical_equipments)
  }

  async activate ({ request, response, params }) {
    let technical_equipments = await TechnicalEquipment.query().where('id', params.id).update({
      status: params.status == 'true' ? true : false
    })
    response.send(technical_equipments)
  }

  async activateList ({ request, response, params }) {
    let technical_equipments = (await TechnicalEquipment.query().where({
      status: params.status == 'true' ? true : false
    }).fetch()).toJSON()
    technical_equipments = List.addRowActions(technical_equipments, [{name: 'edit', route: 'technical_equipments/form/', permission: '5.4.5'}, {name: 'delete', permission: '5.4.4'}, { name: 'activateTechnicalEquipment', permission: '5.4.6' }])
    for (let j in technical_equipments) {
      let chiefId = technical_equipments[j].technical_chief
      let chiefName = (await TechnicalChief.query().where('id', chiefId).fetch()).toJSON()
      technical_equipments[j].technical_chief_name = chiefName[0] ? chiefName[0].full_name : ''
      if (technical_equipments[j].status) {
        technical_equipments[j].estatus = 'Activo'
        technical_equipments[j].actions[2].title = 'Inactivar'
        technical_equipments[j].actions[2].color = 'negative'
      } else {
        technical_equipments[j].estatus = 'Inactivo'
      }
    }
    response.send(technical_equipments)
  }

  async update ({ params, request, response }) {
    const technical_equipments =  await TechnicalEquipment.query().where('id', params.id).first()
    const allRequest = request.all()
    if (technical_equipments) {
      const validation = await validate(request.all(), TechnicalEquipment.fieldValidationRules())

      if (validation.fails()) {
        response.unprocessableEntity(validation.messages())
      } else {
        const body = request.only(TechnicalEquipment.fillable)
        body.zone = JSON.stringify(allRequest.zone)
        body.technical_advisors = JSON.stringify(allRequest.technical_advisors)
        body.technical_chief = JSON.stringify(allRequest.technical_chief)
        await TechnicalEquipment.query().where('id', params.id).update(body)
        response.send(technical_equipments);
      }
    } else {
      response.unprocessableEntity(validation.messages()) // Aquí debe ir uno de not found 404
    }
  }


  async destroy ({ params, request, response }) {
    (await TechnicalEquipment.find(params.id)).delete()
    response.send('success')
  }
}

module.exports = TechnicalEquipmentController
