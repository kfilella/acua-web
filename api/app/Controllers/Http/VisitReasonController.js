'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with visitreasons
 */
const VisitReason = use('App/Models/VisitReason')
const VisitingPlanning = use('App/Models/VisitingPlanning')
const List = use('App/Functions/List')
const { validate } = use('Validator')
class VisitReasonController {
  /**
   * Show a list of all visitreasons.
   * GET visitreasons
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ response }) {
    let visitReason = (await VisitReason.all()).toJSON()
    visitReason = List.addRowActions(visitReason, [{ name: 'edit', route: 'visit_reasons/form/', permission: '2.23.3' }, { name: 'delete', permission: '2.23.5' }])
    visitReason.forEach(element => {
      element.fullName = `${element.name} ${!element.require_approval ? '- (No requiere aprobación)' : ''}`
    })
    response.send(visitReason)
  }

  /**
   * Create/save a new visitreason.
   * POST visitreasons
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const validation = await validate(request.all(), VisitReason.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const body = request.only(VisitReason.fillable)
      const visitReason = await VisitReason.create(body)
      response.send(visitReason)
    }
  }

  /**
   * Display a single visitreason.
   * GET visitreasons/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, response }) {
    const visitReason = (await VisitReason.query().where('id', params.id).first()).toJSON()
    response.send(visitReason)
  }

  /**
   * Update visitreason details.
   * PUT or PATCH visitreasons/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const visitReason = await VisitReason.query().where('id', params.id).first()
    if (visitReason) {
      const validation = await validate(request.all(), VisitReason.fieldValidationRules())

      if (validation.fails()) {
        response.unprocessableEntity(validation.messages())
      } else {
        const body = request.only(VisitReason.fillable)

        await VisitReason.query().where('id', params.id).update(body)
        response.send(visitReason)
      }
    } else {
      response.notFound(validation.messages())
    }
  }

  /**
   * Delete a visitreason with id.
   * DELETE visitreasons/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, response }) {
    const visitReasonId = params.id
    // Antes de eliminar verificar si ese tipo de visita/evento no está en uso
    const isUsed = await VisitingPlanning.query().where('event_type_id', visitReasonId).first()
    if (isUsed) { // Si ha sido incluido en alguna planificación no se puede eliminar
      response.unprocessableEntity('El tipo de evento se encuentra en uso, no puede ser eliminado!')
    } else {
      const visitReason = await VisitReason.find(visitReasonId)
      visitReason.delete()
      response.send('success')
    }
  }
}

module.exports = VisitReasonController
