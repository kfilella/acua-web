'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with roles
 */
const Role = use('App/Models/Role')
const List = use('App/Functions/List')
const { validate } = use('Validator')
class RoleController {
  /**
   * Show a list of all roles.
   * GET roles
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    let roles = (await Role.all()).toJSON()
    roles = List.addRowActions(roles, [{name: 'edit', route: 'roles/form/', permission: '1.2.3'}, {name: 'delete', permission: '1.2.5'}])
    response.send(roles)
  }

  /**
   * Create/save a new role.
   * POST roles
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const validation = await validate(request.all(), Role.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const body = request.only(Role.fillable)
      const prePermissions = request.only(['permissions']).permissions
      // Esto es porque el componente tree no selecciona los padres como se esperaba
      var permissions = []
      for (const i of prePermissions) {
        const divider = i.split('.')
        permissions.push(i)
        for (const j of divider) {
          divider.pop()
          let newcad = divider.join('.')
          permissions.push(newcad)
        }
      }
      permissions = [...new Set(permissions)];
      const role = await Role.create(body)
      await role.permissions().attach(permissions)
      response.send(role);
    }
  }

  /**
   * Display a single role.
   * GET roles/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, response }) {
    const role = (await Role.query().with('permissions').where('id', params.id).first()).toJSON()
    role.permissions = role.permissions.map(value => value.track)
    response.send(role)
  }

  /**
   * Update role details.
   * PUT or PATCH roles/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const role =  await Role.query().with('permissions').where('id', params.id).first()
    if (role) {
      const validation = await validate(request.all(), Role.fieldValidationRulesUpdate())

      if (validation.fails()) {
        response.unprocessableEntity(validation.messages())
      } else {
        const body = request.only(Role.fillable)
        let permissions = request.only(['permissions']).permissions
        for (const i in body) {
          role[i] = body[i]
        }
        role.save()


        function iteratePermissions (element) {
          // Convertir el permiso en un array
          const permissionArray = element.split('.')

          // Eliminar el último elemento del array
          permissionArray.pop()

          // Unir nuevamente el permiso, de string a array
          const permissionString = permissionArray.join('.')

          // Buscar el nuevo string reducido dentro de los permisos
          const found = permissions.find(val => val == permissionString)

          // Si no existe insertarlo
          if (!found) {
            permissions.push(permissionString)
          }

          if (permissionArray.length > 1) {
            iteratePermissions(permissionString)
          }
        }

        // Recorrer cada permiso y ver si todo su árbol ascendente está completo
        const permissionsCopy = [...permissions]
        permissionsCopy.forEach(element => {
          // Convertir el permiso en un array
          const permissionArray = element.split('.')
          if (permissionArray.length > 1) {
            iteratePermissions(element)
          }
        })

        permissions = [...new Set(permissions)]

        await role.permissions().detach()
        await role.permissions().attach(permissions)

        response.send(role);
      }
    } else {
      response.unprocessableEntity(validation.messages()) // Aquí debe ir uno de not found 404
    }
  }

  /**
   * Delete a role with id.
   * DELETE roles/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, response }) {
    const role = await Role.find(params.id)
    role.delete()
    response.send('success')
  }
}

module.exports = RoleController
