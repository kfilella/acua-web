'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with mailconfigurations
 */
const MailConfiguration = use('App/Models/MailConfiguration')
const { validate } = use('Validator')
const Email = use('App/Functions/Email')
class MailConfigurationController {
  /**
   * Show a list of all mailconfigurations.
   * GET mailconfigurations
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    response.send(await MailConfiguration.query().first())
  }

  /**
   * Render a form to be used for creating a new mailconfiguration.
   * GET mailconfigurations/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new mailconfiguration.
   * POST mailconfigurations
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single mailconfiguration.
   * GET mailconfigurations/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing mailconfiguration.
   * GET mailconfigurations/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update mailconfiguration details.
   * PUT or PATCH mailconfigurations/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const mailConfiguration =  await MailConfiguration.query().first()
    if (mailConfiguration) {
      const validation = await validate(request.all(), MailConfiguration.fieldValidationRules(request.only(['type'])))

      if (validation.fails()) {
        response.unprocessableEntity(validation.messages())
      } else {
        const body = request.only(MailConfiguration.fillable)
        // console.log('body', body)
        for (const i in body) {
          mailConfiguration[i] = body[i]
        }
        mailConfiguration.save()
        const sended = await Email.send({
          to: 'elyohan14@gmail.com',
          subject: 'Testing',
          html: '<a href="wondrix.com">Click Aquí</a>'
        })
        console.log('se', sended)
        if (sended) {
          response.send(mailConfiguration)
        } else {
          response.proxyAuthenticationRequired('Mensaje de correo no enviado')
        }
      }
    } else {
      response.unprocessableEntity(validation.messages()) // Aquí debe ir uno de not found 404
    }
  }

  /**
   * Delete a mailconfiguration with id.
   * DELETE mailconfigurations/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = MailConfigurationController
