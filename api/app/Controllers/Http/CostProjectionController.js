'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with costprojections
 */
const { validate } = use('Validator')
const CostProjection = use('App/Models/CostProjection')
const ShrimpPrice = use('App/Models/ShrimpPrice')
class CostProjectionController {
  /**
   * Show a list of all costprojections.
   * GET costprojections
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
  }

  /**
   * Create/save a new costprojection.
   * POST costprojections
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
    const validation = await validate(request.all(), CostProjection.fieldValidationRules())

    if (validation.fails()) {
      response.unprocessableEntity(validation.messages())
    } else {
      const body = request.only(CostProjection.fillable)
      // Guardar precio del camarón al crear la proyección de costos
      body.shrimp_price = (await ShrimpPrice.query().orderBy('date', 'desc').first()).toJSON().price
      const costProjection = await CostProjection.create(body)
      response.send(costProjection)
    }
  }

  /**
   * Display a single costprojection.
   * GET costprojections/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Update costprojection details.
   * PUT or PATCH costprojections/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const costProjection = await CostProjection.query().where('id', params.id).first()
    if (costProjection) {
      const validation = await validate(request.all(), CostProjection.fieldValidationRules())

      if (validation.fails()) {
        response.unprocessableEntity(validation.messages())
      } else {
        const body = request.only(CostProjection.fillable)

        await CostProjection.query().where('id', params.id).update(body)
        response.send(costProjection)
      }
    } else {
      response.notFound(validation.messages())
    }
  }

  /**
   * Delete a costprojection with id.
   * DELETE costprojections/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = CostProjectionController
