'use strict'

const Task = use('Task')
const moment = require('moment')
const Product = use('App/Models/Product')
const Env = use('Env')
class ProductsSap extends Task {
  static get schedule () {
    return '30 3 * * *' // Primer lote de 40mil productos Cada día a las 3:30am 30 3 * * *
  }

  async handle () {
    console.log('Inicio', moment())
    var hana = require('@sap/hana-client');

    var conn = hana.createConnection();

    var conn_params = {
      serverNode: `${Env.get('HANA_HOST')}:${Env.get('HANA_PORT')}`,
      uid: Env.get('HANA_USER'),
      pwd: Env.get('HANA_PASSWORD'),
      instanceNumber: Env.get('HANA_INSTANCE_NUMBER')
    };

    
    conn.connect(conn_params);
    console.log('Connected');
    const records = conn.exec(`select * from "_SYS_BIC"."sap.bfranco.sd.comercial/AT_MARA_RES" LIMIT 40000`);
    console.log(records.length);
    conn.disconnect();

    for (const i of records) {
      const exist = await Product.query().where('sap_MATNR', i.MATNR).first()
      if (exist) { // Si ya se ha guardado ese producto

      } else { // Si no se ha guardado, lo guardo
        await Product.create({
          sap_MATNR: i.MATNR,
          name: i.MAKTX
        })
      }
    }
    
    
    console.log('Disconnected');
    console.log('Fin', moment())
  }
}

module.exports = ProductsSap
