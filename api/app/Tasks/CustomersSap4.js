'use strict'

const Task = use('Task')
const moment = require('moment')
const Customer = use('App/Models/Customer')
const Env = use('Env')
class CustomersSap extends Task {
  static get schedule () {
    return '0 12 * * *' // Primer lote de 40mil clientes Cada día a las 12pm 0 12 * * *
  }

  async handle () {
    console.log('Inicio', moment())
    var hana = require('@sap/hana-client');

    var conn = hana.createConnection();

    var conn_params = {
      serverNode: `${Env.get('HANA_HOST')}:${Env.get('HANA_PORT')}`,
      uid: Env.get('HANA_USER'),
      pwd: Env.get('HANA_PASSWORD'),
      instanceNumber: Env.get('HANA_INSTANCE_NUMBER')
    };

    
    conn.connect(conn_params);
    console.log('Connected');
    const records = conn.exec(`select * from "_SYS_BIC"."sap.bfranco.sd.comercial/AT_KNA1_RES" LIMIT 40000`);
    console.log(records.length);
    conn.disconnect();

    for (const i of records) {
      const exist = await Customer.query().where('sap_KUNNR', i.KUNNR).first()
      if (exist) { // Si ya se ha guardado ese cliente
        try {
          let isThereChanges = false
          if (exist.legal_name != `${i.NAME1} ${i.NAME2}`) {
            exist.legal_name = `${i.NAME1} ${i.NAME2}`
            isThereChanges = true
          }

          if (exist.document_type != document_type) {
            exist.document_type = document_type
            isThereChanges = true
          }

          if (exist.document_number != document_number) {
            exist.document_number = document_number
            isThereChanges = true
          }

          if (exist.phone != i.TEL_NUMBER) {
            exist.phone = i.TEL_NUMBER
            isThereChanges = true
          }
          if (isThereChanges) {
            await exist.save()
          }

        } catch (error) {
          console.error('Error al modificar registro de cliente', error)
        }
      } else { // Si no se ha guardado, lo guardo
        const document_type = i.STCD1.length ? 2 : i.STCD3.length ? 1 : 3
        const document_number = i.STCD1.length ? i.STCD1 : i.STCD3.length ? i.STCD3 : i.STCD2
        await Customer.create({
          type: 1,
          sap_KUNNR: i.KUNNR,
          legal_name: `${i.NAME1} ${i.NAME2}`,
          document_type,
          document_number,
          country_id: 1,
          phone: i.TEL_NUMBER
        })
      }
    }
    
    
    console.log('Disconnected');
    console.log('Fin', moment())
  }
}

module.exports = CustomersSap
