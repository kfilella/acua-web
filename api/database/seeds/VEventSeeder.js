'use strict'

/*
|--------------------------------------------------------------------------
| EventSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Database = use('Database')
const Event = use('App/Models/Event')
const UserEvent = use('App/Models/UserEvent')
const User = use('App/Models/User')
const eventData = [
  {
    id: 1,
    name: 'Inicio se sesión',
    description: 'Evento que mostrará o enviará una notificación cada vez que se inicie sesión',
    subject: 'Inicio se sesión',
    content: 'Saludos, se acaba de registrar un inicio de sesión'
  },
  {
    id: 2,
    name: 'Aprobación de planificación de visitas',
    description: 'Evento que mostrará o enviará una notificación al asesor técnico cada vez que se apruebe una planificación suya.',
    subject: 'Tu planificación ha sido aprobada',
    content: 'Saludos, se acaba de aprobar tu planificación de visita'
  },
  {
    id: 3,
    name: 'Rechazo de planificación de visitas',
    description: 'Evento que mostrará o enviará una notificación al asesor técnico cada vez que se rechace una planificación suya.',
    subject: 'Tu planificación ha sido rechazada',
    content: 'Saludos, se acaba de rechazar tu planificación de visita'
  },
  {
    id: 4,
    name: 'Solicitud de edición de planificación',
    description: 'Evento que se mostrará cuando un asesor técnico realice una solicitud de edición de una planificación ya aprobada o rechazada',
    subject: 'Has recibido una solicitud de edición de planificación',
    content: 'Saludos, acabas de recibir una solicitud de edición de planificación'
  },
  {
    id: 5,
    name: 'Solicitud de eliminación de planificación',
    description: 'Evento que se mostrará cuando un asesor técnico realice una solicitud de eliminación de una planificación ya aprobada o rechazada',
    subject: 'Has recibido una solicitud de eliminación de planificación',
    content: 'Saludos, acabas de recibir una solicitud de eliminación de planificación'
  }
]

class EventSeeder {
  async run () {
    for (const i of eventData) {
      let event = await Event.findBy('id', i.id)
      if (!event) { // Si no existe ese evento
        const createdEvent = await Event.create({ // Se crea el evento
          id: i.id,
          name: i.name,
          description: i.description,
          subject: i.subject,
          content: i.content
        })
        await Database.raw('select setval(\'events_id_seq\', max(id)) from events')
        const users = (await User.all()).toJSON()
        for (const j of users) {
          await UserEvent.create({ // Se asignan esos eventos a usuarios existentes
            user_id: j.id,
            event_id: createdEvent.id,
            subject: i.subject,
            content: i.content
          })
        }
      }
      else { // si existe ya el evento
        await Event.query().where('id', i.id).update({ // Se actualiza el evento
          name: i.name,
          description: i.description,
          subject: i.subject,
          content: i.content
        })
        const users = (await User.all()).toJSON()
        for (const j of users) {
          const hasEvent = await UserEvent.query().where('user_id', j.id).where('event_id', i.id).first()
          if (!hasEvent) {
            await UserEvent.create({ // Se asigna a usuarios que no lo tengan, el caso de usuarios recién creados
              user_id: j.id,
              event_id: event.id,
              subject: i.subject,
              content: i.content
            })
          }
        }
      }
    }
    console.log('Finished Event')
  }
}

module.exports = EventSeeder
