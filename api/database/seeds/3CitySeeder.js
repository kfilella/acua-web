'use strict'

/*
|--------------------------------------------------------------------------
| CitySeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const City = use('App/Models/City')
const Database = use('Database')
const cityData = [
  {
    id: 1,
    state_id: 1,
    name: 'CUENCA'
  },
  {
    id: 2,
    state_id: 2,
    name: 'GUARANDA'
  },
  {
    id: 3,
    state_id: 3,
    name: 'AZOGUES'
  },
  {
    id: 4,
    state_id: 4,
    name: 'TULCÁN'
  },
  {
    id: 5,
    state_id: 4,
    name: 'SAN GABRIEL'
  },
  {
    id: 6,
    state_id: 5,
    name: 'LATACUNGA'
  },
  {
    id: 7,
    state_id: 6,
    name: 'RIOBAMBA'
  },
  {
    id: 8,
    state_id: 6,
    name: 'ALAUSÍ'
  },
  {
    id: 9,
    state_id: 7,
    name: 'MACHALA'
  },
  {
    id: 10,
    state_id: 7,
    name: 'PIÑAS'
  },
  {
    id: 11,
    state_id: 8,
    name: 'ESMERALDAS'
  },
  {
    id: 12,
    state_id: 8,
    name: 'SAN LORENZO'
  },
  {
    id: 13,
    state_id: 8,
    name: 'MUISNE'
  },
  {
    id: 14,
    state_id: 8,
    name: 'ATACAMES'
  },
  {
    id: 15,
    state_id: 9,
    name: 'GUAYAQUIL'
  },
  {
    id: 16,
    state_id: 9,
    name: 'MILAGRO'
  },
  {
    id: 17,
    state_id: 9,
    name: 'SALINAS'
  },
  {
    id: 18,
    state_id: 10,
    name: 'IBARRA'
  },
  {
    id: 19,
    state_id: 11,
    name: 'LOJA'
  },
  {
    id: 20,
    state_id: 11,
    name: 'MACARÁ'
  },
  {
    id: 21,
    state_id: 12,
    name: 'BABAHOYO'
  },
  {
    id: 22,
    state_id: 13,
    name: 'PORTOVIEJO'
  },
  {
    id: 23,
    state_id: 13,
    name: 'MANTA'
  },
  {
    id: 24,
    state_id: 13,
    name: 'CHONE'
  },
  {
    id: 25,
    state_id: 13,
    name: 'JIPIJAPA'
  },
  {
    id: 26,
    state_id: 14,
    name: 'MACAS'
  },
  {
    id: 27,
    state_id: 15,
    name: 'TENA'
  },
  {
    id: 28,
    state_id: 16,
    name: 'PUYO'
  },
  {
    id: 29,
    state_id: 17,
    name: 'QUITO'
  },
  {
    id: 30,
    state_id: 17,
    name: 'SANGOLQUÍ'
  },
  {
    id: 31,
    state_id: 17,
    name: 'CAYAMBE'
  },
  {
    id: 32,
    state_id: 18,
    name: 'AMBATO'
  },
  {
    id: 33,
    state_id: 19,
    name: 'ZAMORA'
  },
  {
    id: 34,
    state_id: 20,
    name: 'PUERTO BAQUERIZO MORENO'
  },
  {
    id: 35,
    state_id: 20,
    name: 'SANTA CRUZ'
  },
  {
    id: 36,
    state_id: 20,
    name: 'PUERTO VILLAMIL'
  },
  {
    id: 37,
    state_id: 21,
    name: 'NUEVA LOJA'
  },
  {
    id: 38,
    state_id: 22,
    name: 'PUERTO FRANCISCO DE ORELLANA'
  },
  {
    id: 39,
    state_id: 23,
    name: 'SANTO DOMINGO DE LOS COLORADOS'
  },
  {
    id: 40,
    state_id: 24,
    name: 'SANTA ELENA'
  },
  {
    id: 41,
    state_id: 1,
    name: 'GIRÓN'
  },
  {
    id: 42,
    state_id: 1,
    name: 'GUALACEO'
  },
  {
    id: 43,
    state_id: 1,
    name: 'NABÓN'
  },
  {
    id: 44,
    state_id: 1,
    name: 'PAUTE'
  },
  {
    id: 45,
    state_id: 1,
    name: 'PUCARÁ'
  },
  {
    id: 46,
    state_id: 1,
    name: 'SAN FERNANDO'
  },
  {
    id: 47,
    state_id: 1,
    name: 'SANTA ISABEL'
  },
  {
    id: 48,
    state_id: 1,
    name: 'SIGSIG'
  },
  {
    id: 49,
    state_id: 2,
    name: 'CALUMA'
  },
  {
    id: 50,
    state_id: 2,
    name: 'CHILLANES'
  },
  {
    id: 51,
    state_id: 2,
    name: 'CHIMBO'
  },
  {
    id: 52,
    state_id: 2,
    name: 'ECHEANDÍA'
  },
  {
    id: 53,
    state_id: 2,
    name: 'SAN MIGUEL'
  },
  {
    id: 54,
    state_id: 3,
    name: 'BIBLIÁN'
  },
  {
    id: 55,
    state_id: 3,
    name: 'CAÑAR'
  },
  {
    id: 56,
    state_id: 3,
    name: 'LA TRONCAL'
  },
  {
    id: 57,
    state_id: 4,
    name: 'BOLÍVAR'
  },
  {
    id: 58,
    state_id: 4,
    name: 'CARCHI'
  },
  {
    id: 59,
    state_id: 4,
    name: 'EL ÁNGEL'
  },
  {
    id: 60,
    state_id: 5,
    name: 'LA MANÁ'
  },
  {
    id: 61,
    state_id: 5,
    name: 'PANGUA'
  },
  {
    id: 62,
    state_id: 5,
    name: 'PUJILÍ'
  },
  {
    id: 63,
    state_id: 5,
    name: 'SALCEDO'
  },
  {
    id: 64,
    state_id: 5,
    name: 'SAQUISILÍ'
  },
  {
    id: 65,
    state_id: 6,
    name: 'CHAMBO'
  },
  {
    id: 66,
    state_id: 6,
    name: 'CHUNCHI'
  },
  {
    id: 67,
    state_id: 6,
    name: 'COLTA'
  },
  {
    id: 68,
    state_id: 6,
    name: 'GUAMOTE'
  },
  {
    id: 69,
    state_id: 6,
    name: 'GUANO'
  },
  {
    id: 70,
    state_id: 6,
    name: 'PALLATANGA'
  },
  {
    id: 71,
    state_id: 6,
    name: 'PENIPE'
  },
  {
    id: 72,
    state_id: 7,
    name: 'ARENILLAS'
  },
  {
    id: 73,
    state_id: 7,
    name: 'BALSAS'
  },
  {
    id: 74,
    state_id: 7,
    name: 'CHILLA'
  },
  {
    id: 75,
    state_id: 7,
    name: 'EL GUABO'
  },
  {
    id: 76,
    state_id: 7,
    name: 'HUAQUILLAS'
  },
  {
    id: 77,
    state_id: 7,
    name: 'LAS LAJAS'
  },
  {
    id: 78,
    state_id: 7,
    name: 'MARCABELÍ'
  },
  {
    id: 79,
    state_id: 7,
    name: 'PASAJE'
  },
  {
    id: 80,
    state_id: 7,
    name: 'SANTA ROSA'
  },
  {
    id: 81,
    state_id: 7,
    name: 'ZARUMA'
  },
  {
    id: 82,
    state_id: 9,
    name: 'BALAO'
  },
  {
    id: 83,
    state_id: 9,
    name: 'BALZAR'
  },
  {
    id: 84,
    state_id: 9,
    name: 'COLIMES'
  },
  {
    id: 85,
    state_id: 9,
    name: 'DAULE'
  },
  {
    id: 86,
    state_id: 9,
    name: 'DURÁN'
  },
  {
    id: 87,
    state_id: 9,
    name: 'EL EMPALME'
  },
  {
    id: 88,
    state_id: 9,
    name: 'EL TRIUNFO'
  },
  {
    id: 89,
    state_id: 9,
    name: 'NARANJAL'
  },
  {
    id: 90,
    state_id: 9,
    name: 'NARANJITO'
  },
  {
    id: 91,
    state_id: 9,
    name: 'PALESTINA'
  },
  {
    id: 92,
    state_id: 9,
    name: 'PEDRO CARBO'
  },
  {
    id: 93,
    state_id: 9,
    name: 'PLAYAS'
  },
  {
    id: 94,
    state_id: 9,
    name: 'SAMBORONDÓN'
  },
  {
    id: 95,
    state_id: 9,
    name: 'YAGUACHI'
  },
  {
    id: 96,
    state_id: 9,
    name: 'SANTA LUCÍA'
  },
  {
    id: 97,
    state_id: 10,
    name: 'ATUNTAQUI'
  },
  {
    id: 98,
    state_id: 10,
    name: 'COTACACHI'
  },
  {
    id: 99,
    state_id: 10,
    name: 'OTAVALO'
  },
  {
    id: 100,
    state_id: 10,
    name: 'PIMAMPIRO'
  },
  {
    id: 101,
    state_id: 10,
    name: 'URCUQUÍ'
  },
  {
    id: 102,
    state_id: 11,
    name: 'ALAMOR'
  },
  {
    id: 103,
    state_id: 11,
    name: 'CALVAS'
  },
  {
    id: 104,
    state_id: 11,
    name: 'CATACOCHA'
  },
  {
    id: 105,
    state_id: 11,
    name: 'CATAMAYO'
  },
  {
    id: 106,
    state_id: 11,
    name: 'CELICA'
  },
  {
    id: 107,
    state_id: 11,
    name: 'CHAGUARPAMBA'
  },
  {
    id: 108,
    state_id: 11,
    name: 'ESPÍNDOLA'
  },
  {
    id: 109,
    state_id: 11,
    name: 'GONZANAMÁ'
  },
  {
    id: 110,
    state_id: 11,
    name: 'PINDAL'
  },
  {
    id: 111,
    state_id: 11,
    name: 'QUILANGA'
  },
  {
    id: 112,
    state_id: 11,
    name: 'SARAGURO'
  },
  {
    id: 113,
    state_id: 11,
    name: 'SOZORANGA'
  },
  {
    id: 114,
    state_id: 11,
    name: 'URDANETA'
  },
  {
    id: 115,
    state_id: 11,
    name: 'ZAPOTILLO'
  },
  {
    id: 116,
    state_id: 12,
    name: 'BABA'
  },
  {
    id: 117,
    state_id: 12,
    name: 'MONTALVO'
  },
  {
    id: 118,
    state_id: 12,
    name: 'PALENQUE'
  },
  {
    id: 119,
    state_id: 12,
    name: 'PUEBLOVIEJO'
  },
  {
    id: 120,
    state_id: 12,
    name: 'QUEVEDO'
  },
  {
    id: 121,
    state_id: 12,
    name: 'VINCES'
  },
  {
    id: 122,
    state_id: 13,
    name: 'ATAHUALPA'
  },
  {
    id: 123,
    state_id: 13,
    name: 'EL CARMEN'
  },
  {
    id: 124,
    state_id: 13,
    name: 'FLAVIO ALFARO'
  },
  {
    id: 125,
    state_id: 13,
    name: 'JUNÍN'
  },
  {
    id: 126,
    state_id: 13,
    name: 'MONTECRISTI'
  },
  {
    id: 127,
    state_id: 13,
    name: 'PAJÁN'
  },
  {
    id: 128,
    state_id: 13,
    name: 'PEDERNALES'
  },
  {
    id: 129,
    state_id: 13,
    name: 'PICHINCHA'
  },
  {
    id: 130,
    state_id: 13,
    name: 'PUERTO LÓPEZ'
  },
  {
    id: 131,
    state_id: 13,
    name: 'ROCAFUERTE'
  },
  {
    id: 132,
    state_id: 13,
    name: 'SANTA ANA'
  },
  {
    id: 133,
    state_id: 13,
    name: 'SUCRE'
  },
  {
    id: 134,
    state_id: 13,
    name: 'TOSAGUA'
  },
  {
    id: 135,
    state_id: 14,
    name: 'GUALAQUIZA'
  },
  {
    id: 136,
    state_id: 14,
    name: 'LIMÓN INDANZA'
  },
  {
    id: 137,
    state_id: 14,
    name: 'PALORA'
  },
  {
    id: 138,
    state_id: 14,
    name: 'SANTIAGO'
  },
  {
    id: 139,
    state_id: 14,
    name: 'SUCÚA'
  },
  {
    id: 140,
    state_id: 15,
    name: 'ARCHIDONA'
  },
  {
    id: 141,
    state_id: 15,
    name: 'BAEZA'
  },
  {
    id: 142,
    state_id: 15,
    name: 'EL CHACO'
  },
  {
    id: 143,
    state_id: 16,
    name: 'MERA'
  },
  {
    id: 144,
    state_id: 17,
    name: 'ALOAG'
  },
  {
    id: 145,
    state_id: 17,
    name: 'MACHACHI'
  },
  {
    id: 146,
    state_id: 17,
    name: 'SAN MIGUEL DE LOS BANCOS'
  },
  {
    id: 147,
    state_id: 17,
    name: 'TABACUNDO'
  },
  {
    id: 148,
    state_id: 18,
    name: 'ANT. BAQ. MORENO'
  },
  {
    id: 149,
    state_id: 18,
    name: 'BAÑOS'
  },
  {
    id: 150,
    state_id: 18,
    name: 'CEVALLOS'
  },
  {
    id: 151,
    state_id: 18,
    name: 'MOCHA'
  },
  {
    id: 152,
    state_id: 18,
    name: 'PATATE'
  },
  {
    id: 153,
    state_id: 18,
    name: 'PELILEO'
  },
  {
    id: 154,
    state_id: 18,
    name: 'PÍLLARO'
  },
  {
    id: 155,
    state_id: 18,
    name: 'QUERO'
  },
  {
    id: 156,
    state_id: 18,
    name: 'TISALEO'
  },
  {
    id: 157,
    state_id: 19,
    name: 'GUAYZIMI'
  },
  {
    id: 158,
    state_id: 19,
    name: 'YACUAMBÍ'
  },
  {
    id: 159,
    state_id: 19,
    name: 'YANTZAZA'
  },
  {
    id: 160,
    state_id: 21,
    name: 'CASCALES'
  },
  {
    id: 161,
    state_id: 21,
    name: 'LA BONITA'
  },
  {
    id: 162,
    state_id: 21,
    name: 'LUMBAQUÍ'
  },
  {
    id: 163,
    state_id: 21,
    name: 'PUERTO EL CARMEN DE PUTUMAYO'
  },
  {
    id: 164,
    state_id: 21,
    name: 'SHUSHUFINDI'
  },
  {
    id: 165,
    state_id: 22,
    name: 'AGUARICO'
  },
  {
    id: 166,
    state_id: 22,
    name: 'LA JOYA DE LOS SACHAS'
  },
  {
    id: 167,
    state_id: 24,
    name: 'SALINAS'
  },
  {
    id: 168,
    state_id: 8,
    name: 'ELOY ALFARO'
  },
  {
    id: 169,
    state_id: 8,
    name: 'LA CONCORDIA'
  },
  {
    id: 170,
    state_id: 8,
    name: 'QUININDÉ'
  },
  {
    id: 171,
    state_id: 8,
    name: 'RIOVERDE'
  },
  {
    id: 172,
    state_id: 4,
    name: 'MIRA'
  },
  {
    id: 173,
    state_id: 4,
    name: 'ESPEJO'
  },
  {
    id: 174,
    state_id: 4,
    name: 'MONTÚFAR'
  },
  {
    id: 175,
    state_id: 4,
    name: 'SAN PEDRO DE HUACA'
  },
  {
    id: 176,
    state_id: 10,
    name: 'SAN MIGUEL DE URCUQUÍ'
  },
  {
    id: 177,
    state_id: 10,
    name: 'ANTONIO ANTE'
  },
  {
    id: 178,
    state_id: 21,
    name: 'SUCUMBIOS'
  },
  {
    id: 179,
    state_id: 21,
    name: 'GONZALO PIZARRO'
  },
  {
    id: 180,
    state_id: 21,
    name: 'LAGO AGRIO'
  },
  {
    id: 181,
    state_id: 21,
    name: 'CUYABENO'
  },
  {
    id: 182,
    state_id: 13,
    name: 'JAMA'
  },
  {
    id: 183,
    state_id: 13,
    name: 'SAN VICENTE'
  },
  {
    id: 184,
    state_id: 13,
    name: 'BOLÍVAR'
  },
  {
    id: 185,
    state_id: 13,
    name: 'JARAMIJÓ'
  },
  {
    id: 186,
    state_id: 13,
    name: 'VEINTICUATRO DE MAYO'
  },
  {
    id: 187,
    state_id: 13,
    name: 'OLMEDO'
  },
  {
    id: 188,
    state_id: 23,
    name: 'LA CONCORDIA'
  },
  {
    id: 189,
    state_id: 17,
    name: 'PUERTO QUITO'
  },
  {
    id: 190,
    state_id: 17,
    name: 'PEDRO VICENTE MALDONADO'
  },
  {
    id: 191,
    state_id: 17,
    name: 'PEDRO MONCAYO'
  },
  {
    id: 192,
    state_id: 17,
    name: 'RUMIÑAHUI'
  },
  {
    id: 193,
    state_id: 17,
    name: 'MEJÍA'
  },
  {
    id: 194,
    state_id: 15,
    name: 'QUIJOS'
  },
  {
    id: 195,
    state_id: 15,
    name: 'CARLOS JULIO AROSEMENA TOLA'
  },
  {
    id: 196,
    state_id: 22,
    name: 'LORETO'
  },
  {
    id: 197,
    state_id: 16,
    name: 'SANTA CLARA'
  },
  {
    id: 198,
    state_id: 16,
    name: 'ARAJUNO'
  },
  {
    id: 199,
    state_id: 16,
    name: 'PASTAZA'
  },
  {
    id: 200,
    state_id: 12,
    name: 'BUENA FE'
  },
  {
    id: 201,
    state_id: 12,
    name: 'VALENCIA'
  },
  {
    id: 202,
    state_id: 12,
    name: 'QUINSALOMA'
  },
  {
    id: 203,
    state_id: 12,
    name: 'MOCACHE'
  },
  {
    id: 204,
    state_id: 12,
    name: 'VENTANAS'
  },
  {
    id: 205,
    state_id: 12,
    name: 'URDANETA'
  },
  {
    id: 206,
    state_id: 5,
    name: 'SIGCHOS'
  },
  {
    id: 207,
    state_id: 2,
    name: 'LAS NAVES'
  },
  {
    id: 208,
    state_id: 6,
    name: 'CUMANDA'
  },
  {
    id: 209,
    state_id: 6,
    name: 'CHUNCHI'
  },
  {
    id: 210,
    state_id: 14,
    name: 'PABLO SEXTO'
  },
  {
    id: 211,
    state_id: 14,
    name: 'HUAMBOYA'
  },
  {
    id: 212,
    state_id: 14,
    name: 'MORONA'
  },
  {
    id: 213,
    state_id: 14,
    name: 'TAISHA'
  },
  {
    id: 214,
    state_id: 14,
    name: 'LOGROÑO'
  },
  {
    id: 215,
    state_id: 14,
    name: 'TIWINTZA'
  },
  {
    id: 216,
    state_id: 14,
    name: 'SAN JUAN BOSCO'
  },
  {
    id: 217,
    state_id: 9,
    name: 'ISIDRO AYORA'
  },
  {
    id: 218,
    state_id: 9,
    name: 'LOMAS DE SARGENTILLO'
  },
  {
    id: 219,
    state_id: 9,
    name: 'NOBOL'
  },
  {
    id: 220,
    state_id: 9,
    name: 'SALITRE'
  },
  {
    id: 221,
    state_id: 9,
    name: 'ALFREDO BAQUERIZO MORENO'
  },
  {
    id: 222,
    state_id: 9,
    name: 'SIMÓN BOLÍVAR'
  },
  {
    id: 223,
    state_id: 9,
    name: 'GENERAL ANTONIO ELIZALDE'
  },
  {
    id: 224,
    state_id: 9,
    name: 'CORONEL MARCELINO MARIDUEÑA'
  },
  {
    id: 225,
    state_id: 24,
    name: 'LA LIBERTAD'
  },
  {
    id: 226,
    state_id: 3,
    name: 'SUSCAL'
  },
  {
    id: 227,
    state_id: 3,
    name: 'EL TAMBO'
  },
  {
    id: 228,
    state_id: 3,
    name: 'DELEG'
  },
  {
    id: 229,
    state_id: 1,
    name: 'SEVILLA DE ORO'
  },
  {
    id: 230,
    state_id: 1,
    name: 'GUACHAPALA'
  },
  {
    id: 231,
    state_id: 1,
    name: 'EL PAN'
  },
  {
    id: 232,
    state_id: 1,
    name: 'CHORDELEG'
  },
  {
    id: 233,
    state_id: 1,
    name: 'CAMILO PONCE ENRIQUEZ'
  },
  {
    id: 234,
    state_id: 1,
    name: 'OÑA'
  },
  {
    id: 235,
    state_id: 7,
    name: 'PORTOVELO'
  },
  {
    id: 236,
    state_id: 11,
    name: 'OLMEDO'
  },
  {
    id: 237,
    state_id: 11,
    name: 'PALTAS'
  },
  {
    id: 238,
    state_id: 11,
    name: 'PUYANGO'
  },
  {
    id: 239,
    state_id: 19,
    name: 'EL PANGUI'
  },
  {
    id: 240,
    state_id: 19,
    name: 'CENTINELA DEL CÓNDOR'
  },
  {
    id: 241,
    state_id: 19,
    name: 'PAQUISHA'
  },
  {
    id: 242,
    state_id: 19,
    name: 'NANGARITZA'
  },
  {
    id: 243,
    state_id: 19,
    name: 'PALANDA'
  },
  {
    id: 244,
    state_id: 19,
    name: 'CHINCHIPE'
  },
  {
    id: 245,
    state_id: 20,
    name: 'ISABELA'
  },
  {
    id: 246,
    state_id: 20,
    name: 'SAN CRISTÓBAL'
  }
]

class CitySeeder {
  async run () {
    for (let i of cityData) {
      var city = await City.find(i.id)
      if (!city) {
        var city = await City.create(i)
        await Database.raw('select setval(\'cities_id_seq\', max(id)) from cities')
      }
      else {
        for (let j in i) {
          city[j] = i[j]
        }
        await city.save()
      }

    }
    console.log('Finished City')
  }
}

module.exports = CitySeeder
