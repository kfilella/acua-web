'use strict'

/*
|--------------------------------------------------------------------------
| ScheduleSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Schedule = use('App/Models/Schedule')
const Database = use('Database')
const scheduleData = [
  {
    id: 1,
    name: 'Domingo',
    start_time: '09:00:00',
    end_time: '17:00:00'
  },
  {
    id: 2,
    name: 'Lunes',
    start_time: '09:00:00',
    end_time: '17:00:00'
  },
  {
    id: 3,
    name: 'Martes',
    start_time: '09:00:00',
    end_time: '17:00:00'
  },
  {
    id: 4,
    name: 'Miércoles',
    start_time: '09:00:00',
    end_time: '17:00:00'
  },
  {
    id: 5,
    name: 'Jueves',
    start_time: '09:00:00',
    end_time: '17:00:00'
  },
  {
    id: 6,
    name: 'Viernes',
    start_time: '09:00:00',
    end_time: '17:00:00'
  },
  {
    id: 7,
    name: 'Sábado',
    start_time: '09:00:00',
    end_time: '17:00:00'
  }
]

class ScheduleSeeder {
  async run () {
    for (let i of scheduleData) {
      var schedule = await Schedule.find(i.id)
      if (!schedule) {
        var schedule = await Schedule.create(i)
        await Database.raw('select setval(\'schedules_id_seq\', max(id)) from schedules')
      }
      else {
        // Si ya existen registros no hacemos nada porque sobreescriberemos lo que configure el usuario
        /* for (let j in i) {
          schedule[j] = i[j]
        }
        await schedule.save() */

      }

    }
    console.log('Finished Schedule')
  }
}

module.exports = ScheduleSeeder
