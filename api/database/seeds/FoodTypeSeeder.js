'use strict'

/*
|--------------------------------------------------------------------------
| FoodTypeSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const FoodType = use('App/Models/FoodType')

class FoodTypeSeeder {
}

module.exports = FoodTypeSeeder
