'use strict'

/*
|--------------------------------------------------------------------------
| MeasureUnitSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const MeasureUnit = use('App/Models/MeasureUnit')
const Database = use('Database')
const measureUnitData = [
  {
    id: 1,
    full_name: 'Kilogramo',
    slug: 'kg'
  },
  {
    id: 2,
    full_name: 'Litro',
    slug: 'lt'
  },
  {
    id: 3,
    full_name: 'Metro',
    slug: 'mt'
  }
]

class MeasureUnitSeeder {
  async run () {
    for (let i of measureUnitData) {
      var measureUnit = await MeasureUnit.find(i.id)
      if (!measureUnit) {
        var measureUnit = await MeasureUnit.create(i)
        await Database.raw('select setval(\'measure_units_id_seq\', max(id)) from measure_units')
      }
      else {
        for (let j in i) {
          measureUnit[j] = i[j]
        }
        await measureUnit.save()
      }

    }
    console.log('Finished MeasureUnit')
  }
}

module.exports = MeasureUnitSeeder
