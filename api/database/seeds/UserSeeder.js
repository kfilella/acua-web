'use strict'

/*
|--------------------------------------------------------------------------
| UserSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const User = use('App/Models/User')
const usersData = [
  {
    email: 'admin@wondrix.com',
    password: 'wondrix2020',
    roles: [1]
  },
  {
    email: 'jefetecnico@wondrix.com',
    password: 'wondrix2020',
    roles: [2]
  },
  {
    email: 'at1@wondrix.com',
    password: 'wondrix2020',
    roles: [3]
  },
  {
    email: 'jtorbay@wondrix.com',
    password: 'wondrix2020',
    roles: [1]
  }
]

class UserSeeder {
  async run () {
    for (let i of usersData) {
      var user = await User.query().with('roles').where('email', i.email).first()
      let roles = i.roles
      delete i.roles
      if (!user) {
        var user = await User.create(i)
      }
      else {
        for (let j in i) {
          user[j] = i[j]
        }
        await user.save()
        await user.roles().detach()
      }
      await user.roles().attach(roles)
    }
    console.log('Finished User')
  }
}

module.exports = UserSeeder
