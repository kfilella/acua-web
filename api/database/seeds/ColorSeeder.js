'use strict'

/*
|--------------------------------------------------------------------------
| ColorSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Color = use('App/Models/Color')
const Database = use('Database')
const colorData = [
  {
    id: 1,
    name: 'Rojo',
  },
  {
    id: 2,
    name: 'Carmesí',
  },
  {
    id: 3,
    name: 'Bermellón',
  },
  {
    id: 4,
    name: 'Escarlata',
  },
  {
    id: 5,
    name: 'Granate',
  },
  {
    id: 6,
    name: 'Carmín',
  },
  {
    id: 7,
    name: 'Amaranto',
  },
  {
    id: 8,
    name: 'Verde',
  },
  {
    id: 9,
    name: 'Verde Agua',
  },
  {
    id: 10,
    name: 'Verde Veronés',
  },
  {
    id: 11,
    name: 'Verde Oliva',
  },
  {
    id: 12,
    name: 'Verde Esmeralda',
  },
  {
    id: 13,
    name: 'Verde Lima',
  },
  {
    id: 14,
    name: 'Verde Manzana',
  },
  {
    id: 15,
    name: 'Azul',
  },
  {
    id: 16,
    name: 'Azul Cobalto',
  },
  {
    id: 17,
    name: 'Azul Marino',
  },
  {
    id: 18,
    name: 'Azul Petróleo',
  },
  {
    id: 19,
    name: 'Zafiro',
  },
  {
    id: 20,
    name: 'Índigo',
  },
  {
    id: 21,
    name: 'Turquí',
  },
  {
    id: 22,
    name: 'Azul de Prusia',
  },
  {
    id: 23,
    name: 'Azul Majorelle',
  },
  {
    id: 24,
    name: 'Azul Klein',
  },
  {
    id: 25,
    name: 'Magenta',
  },
  {
    id: 26,
    name: 'Fucsia',
  },
  {
    id: 27,
    name: 'Morado',
  },
  {
    id: 28,
    name: 'Malva',
  },
  {
    id: 29,
    name: 'Lila',
  },
  {
    id: 30,
    name: 'Salmón',
  },
  {
    id: 31,
    name: 'Lavanda',
  },
  {
    id: 32,
    name: 'Rosa',
  },
  {
    id: 33,
    name: 'Cian',
  },
  {
    id: 34,
    name: 'Turquesa',
  },
  {
    id: 35,
    name: 'Celeste',
  },
  {
    id: 36,
    name: 'Amarillo',
  },
  {
    id: 37,
    name: 'Amarillo Limón',
  },
  {
    id: 38,
    name: 'Dorado',
  },
  {
    id: 39,
    name: 'Topacio',
  },
  {
    id: 40,
    name: 'Marrón',
  },
  {
    id: 41,
    name: 'Caqui',
  },
  {
    id: 42,
    name: 'Ocre',
  },
  {
    id: 43,
    name: 'Siena',
  },
  {
    id: 44,
    name: 'Siena Pálido',
  },
  {
    id: 45,
    name: 'Borgoña',
  },
  {
    id: 46,
    name: 'Violeta',
  },
  {
    id: 47,
    name: 'Amatista',
  },
  {
    id: 48,
    name: 'Púrpura',
  },
  {
    id: 49,
    name: 'Naranja',
  },
  {
    id: 50,
    name: 'Sésamo',
  },
  {
    id: 51,
    name: 'Beige',
  },
  {
    id: 52,
    name: 'Durazno',
  },
  {
    id: 53,
    name: 'Blanco',
  },
  {
    id: 54,
    name: 'Lino',
  },
  {
    id: 55,
    name: 'Marfil',
  },
  {
    id: 56,
    name: 'Plateado',
  },
  {
    id: 57,
    name: 'Gris',
  },
  {
    id: 58,
    name: 'Negro',
  }
]

class ColorSeeder {
  async run () {
    for (let i of colorData) {
      var color = await Color.find(i.id)
      if (!color) {
        var color = await Color.create(i)
        await Database.raw('select setval(\'colors_id_seq\', max(id)) from colors')
      }
      else {
        for (let j in i) {
          color[j] = i[j]
        }
        await color.save()
      }

    }
    console.log('Finished Color')
  }
}

module.exports = ColorSeeder
