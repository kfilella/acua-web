'use strict'

/*
|--------------------------------------------------------------------------
| FormatCategoryVariableSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const FormatCategoryVariable = use('App/Models/FormatCategoryVariable')
const Database = use('Database')
const formatCategoryVariableData = [
  {
    id: 1,
    format_category_id: 1,
    name: 'VARIABLE_EJEMPLO',
    description: 'descripción de la variable ejemplo'
  }
]

class FormatCategoryVariableSeeder {
  async run () {
    for (let i of formatCategoryVariableData) {
      var formatCategoryVariable = await FormatCategoryVariable.find(i.id)
      if (!formatCategoryVariable) {
        var formatCategoryVariable = await FormatCategoryVariable.create(i)
        await Database.raw('select setval(\'format_category_variables_id_seq\', max(id)) from format_category_variables')
      }
      else {
        await FormatCategoryVariable.query().where('id', i.id).update(i)
      }

    }
    console.log('Finished FormatCategoryVariable')
  }
}

module.exports = FormatCategoryVariableSeeder
