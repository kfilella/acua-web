'use strict'

/*
|--------------------------------------------------------------------------
| MailConfigurationSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const MailConfiguration = use('App/Models/MailConfiguration')
const mailConfigurationData = [
  {
    id: 1,
    host: "softlutionszone.com",
    port: 465,
    secure: true,
    user: 'wondrix@wondrix.com',
    pass: 'KS.BSrOB#p7m',
    from: 'Wondrix BoilerPlate',
    mailgun_domain: 'base.wondrix.com',
    mailgun_apikey: '6baaf87b5640da37352c48fe840332c3-87cdd773-ae3a0c52',
    type: 1
  }
]

class MailConfigurationSeeder {
  async run () {
    for (let i of mailConfigurationData) {
      let mailConfiguration = await MailConfiguration.findBy('id', i.id)
      if (!mailConfiguration) {
        await MailConfiguration.create(i)
      }
      else {
        // await MailConfiguration.query().where('id', i.id).update(i)
        // Si hay registro no debe modificar porque el usuario debe hacer esto
      }
    }
    console.log('Finished MailConfiguration')
  }
}

module.exports = MailConfigurationSeeder
