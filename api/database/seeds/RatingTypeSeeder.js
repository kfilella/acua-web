'use strict'

/*
|--------------------------------------------------------------------------
| RatingTypeSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const RatingType = use('App/Models/RatingType')
const Database = use('Database')
const ratingTypeData = [
  {
    id: 1,
    name: 'Tipo de calificación 1'
  }
]

class RatingTypeSeeder {
  async run () {
    for (let i of ratingTypeData) {
      var ratingType = await RatingType.find(i.id)
      if (!ratingType) {
        var ratingType = await RatingType.create(i)
        await Database.raw('select setval(\'rating_types_id_seq\', max(id)) from rating_types')
      }
      else {
        for (let j in i) {
          ratingType[j] = i[j]
        }
        await ratingType.save()
      }

    }
    console.log('Finished RatingType')
  }
}

module.exports = RatingTypeSeeder
