'use strict'

/*
|--------------------------------------------------------------------------
| CountrySeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Country = use('App/Models/Country')
const Database = use('Database')
const countryData = [
  {
    id: 1,
    name: 'Ecuador'
  },
  {
    id: 2,
    name: 'Argentina'
  },
  {
    id: 3,
    name: 'Bolivia'
  },
  {
    id: 4,
    name: 'Brasil'
  },
  {
    id: 5,
    name: 'Chile'
  },
  {
    id: 6,
    name: 'Colombia'
  },
  {
    id: 7,
    name: 'Estados Unidos'
  },
  {id: 8,
    name: 'Paraguay'
  },
  {
    id: 9,
    name: 'Perú'
  },
  {
    id: 10,
    name: 'Uruguay'
  },
  {
    id: 11,
    name: 'Venezuela'
  }
]

class CountrySeeder {
  async run () {
    for (let i of countryData) {
      var country = await Country.find(i.id)
      if (!country) {
        var country = await Country.create(i)
        await Database.raw('select setval(\'countries_id_seq\', max(id)) from countries')
      }
      else {
        for (let j in i) {
          country[j] = i[j]
        }
        await country.save()
      }

    }
    console.log('Finished Country')
  }
}

module.exports = CountrySeeder
