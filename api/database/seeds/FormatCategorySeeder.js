'use strict'

/*
|--------------------------------------------------------------------------
| FormatCategorySeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const FormatCategory = use('App/Models/FormatCategory')
const Database = use('Database')
const formatCategoryData = [
  {
    id: 1,
    name: 'Reporte ejemplo'
  }
]

class FormatCategorySeeder {
  async run () {
    for (let i of formatCategoryData) {
      var formatCategory = await FormatCategory.find(i.id)
      if (!formatCategory) {
        var formatCategory = await FormatCategory.create(i)
        await Database.raw('select setval(\'format_categories_id_seq\', max(id)) from format_categories')
      }
      else {
        await FormatCategory.query().where('id', i.id).update(i)
      }

    }
    console.log('Finished FormatCategory')
  }
}

module.exports = FormatCategorySeeder
