'use strict'

/*
|--------------------------------------------------------------------------
| RatingCompanySeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const RatingCompany = use('App/Models/RatingCompany')
const Database = use('Database')
const ratingCompanyData = [
  {
    id: 1,
    name: 'Empresa calificadora 1'
  }
]

class RatingCompanySeeder {
  async run () {
    for (let i of ratingCompanyData) {
      var ratingCompany = await RatingCompany.find(i.id)
      if (!ratingCompany) {
        var ratingCompany = await RatingCompany.create(i)
        await Database.raw('select setval(\'rating_companies_id_seq\', max(id)) from rating_companies')
      }
      else {
        for (let j in i) {
          ratingCompany[j] = i[j]
        }
        await ratingCompany.save()
      }

    }
    console.log('Finished RatingCompany')
  }
}

module.exports = RatingCompanySeeder
