'use strict'

/*
|--------------------------------------------------------------------------
| PermissionSeeder
  En la tabla de permisos se guardarán los permisos añadidos en este seeder.
  Los permisos deben ser frases verbales. Ejemplo: Listar usuarios, guardar usuarios, ver usuario específico, actualizar usuario y eliminar usuario
  Un permiso debe incluir todas las acciones, desde ver un item del menú hasta la acción mínima de forma descendente.
  Por lo tanto todos los items del menú deben estar atados a un permiso, y todas las
  acciones relacionadas con base de datos también.
  En el front existirán métodos para verificar si el usuario logueado puede ver cierto
  ítem del menú según su permiso y si puede acceder a cierta ruta.
  En el back existe un Middleware que debe aplicarse a todas las rutas que requieren autenticación,
  este Middleware verificará si el usuario tiene los permisos correspondientes.
  Los permisos se encuentran en App/Functions/Permissions. Se debe agregar o modificar allí



|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Permission = use('App/Models/Permission')
const PermissionF = use('App/Functions/Permissions')
const permissions = PermissionF.getPermissions()
const permissionData = []

function formatRecord (data) {
  for (const i of data) {
    permissionData.push({track: i.track, name: i.name, description: i.description})
    if (i.children) {
      formatRecord(i.children)
    }
  }
}
class PermissionSeeder {
  async run () {
    formatRecord(permissions)
    for (let i of permissionData) {
      let permission = await Permission.findBy('track', i.track)
      if (!permission) {
        await Permission.create(i)
      }
      else {
        await Permission.query().where('track', i.track).update(i)
      }
    }
    console.log('Finished Permission')
  }
}

module.exports = PermissionSeeder
