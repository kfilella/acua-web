'use strict'

/*
|--------------------------------------------------------------------------
| StateSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const State = use('App/Models/State')
const Database = use('Database')
const stateData = [
  {
    id: 1,
    country_id: 1,
    name: "AZUAY"
  },
  {
    id: 2,
    country_id: 1,
    name: "BOLIVAR"
  },
  {
    id: 3,
    country_id: 1,
    name: "CAÑAR"
  },
  {
    id: 4,
    country_id: 1,
    name: "CARCHI"
  },
  {
    id: 5,
    country_id: 1,
    name: "COTOPAXI"
  },
  {
    id: 6,
    country_id: 1,
    name: "CHIMBORAZO"
  },
  {
    id: 7,
    country_id: 1,
    name: "EL ORO"
  },
  {
    id: 8,
    country_id: 1,
    name: "ESMERALDAS"
  },
  {
    id: 9,
    country_id: 1,
    name: "GUAYAS"
  },
  {
    id: 10,
    country_id: 1,
    name: "IMBABURA"
  },
  {
    id: 11,
    country_id: 1,
    name: "LOJA"
  },
  {
    id: 12,
    country_id: 1,
    name: "LOS RIOS"
  },
  {
    id: 13,
    country_id: 1,
    name: "MANABI"
  },
  {
    id: 14,
    country_id: 1,
    name: "MORONA SANTIAGO"
  },
  {
    id: 15,
    country_id: 1,
    name: "NAPO"
  },
  {
    id: 16,
    country_id: 1,
    name: "PASTAZA"
  },
  {
    id: 17,
    country_id: 1,
    name: "PICHINCHA"
  },
  {
    id: 18,
    country_id: 1,
    name: "TUNGURAHUA"
  },
  {
    id: 19,
    country_id: 1,
    name: "ZAMORA CHINCHIPE"
  },
  {
    id: 20,
    country_id: 1,
    name: "GALAPAGOS"
  },
  {
    id: 21,
    country_id: 1,
    name: "SUCUMBIOS"
  },
  {
    id: 22,
    country_id: 1,
    name: "ORELLANA"
  },
  {
    id: 23,
    country_id: 1,
    name: "SANTO DOMINGO DE LOS TSACHILAS"
  },
  {
    id: 24,
    country_id: 1,
    name: "SANTA ELENA"
  }
]

class StateSeeder {
  async run () {
    for (let i of stateData) {
      var state = await State.find(i.id)
      if (!state) {
        var state = await State.create(i)
        await Database.raw('select setval(\'states_id_seq\', max(id)) from states')
      }
      else {
        for (let j in i) {
          state[j] = i[j]
        }
        await state.save()
      }

    }
    console.log('Finished State')
  }
}

module.exports = StateSeeder
