'use strict'

/*
|--------------------------------------------------------------------------
| SegmentSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Segment = use('App/Models/Segment')
const Database = use('Database')
const segmentData = [
  {
    id: 1,
    name: 'Segmento 1'
  }
]

class SegmentSeeder {
  async run () {
    for (let i of segmentData) {
      var segment = await Segment.find(i.id)
      if (!segment) {
        var segment = await Segment.create(i)
        await Database.raw('select setval(\'segments_id_seq\', max(id)) from segments')
      }
      else {
        for (let j in i) {
          segment[j] = i[j]
        }
        await segment.save()
      }

    }
    console.log('Finished Segment')
  }
}

module.exports = SegmentSeeder
