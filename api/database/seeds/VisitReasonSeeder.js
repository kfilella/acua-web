'use strict'

/*
|--------------------------------------------------------------------------
| VisitReasonSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const VisitReason = use('App/Models/VisitReason')
const Database = use('Database')
const visitReasonData = [
  {
    id: 1,
    name: 'Visita'
  },
  {
    id: 2,
    name: 'Llamada'
  },
  {
    id: 3,
    name: 'Visita no planificada'
  }
]

class VisitReasonSeeder {
  async run () {
    for (let i of visitReasonData) {
      var visitReason = await VisitReason.find(i.id)
      if (!visitReason) {
        var visitReason = await VisitReason.create(i)
        await Database.raw('select setval(\'visit_reasons_id_seq\', max(id)) from visit_reasons')
      }
      else {// Solo se crean si no existen. No se modifican
      }

    }
    console.log('Finished VisitReason')
  }
}

module.exports = VisitReasonSeeder
