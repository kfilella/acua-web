'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PoolSchema extends Schema {
  up () {
    this.create('pools', (table) => {
      table.increments()
      table.integer('shrimp_id').references('id').inTable('shrimps').notNullable()
      table.string('description').notNullable()
      table.integer('area').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('pools')
  }
}

module.exports = PoolSchema
