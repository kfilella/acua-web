'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FishingProjectionSchema extends Schema {
  up () {
    this.table('fishing_projections', (table) => {
      table.decimal('mp', 5, 2).alter()
    })
  }

  down () {
    this.table('fishing_projections', (table) => {
      table.integer('mp').alter()
    })
  }
}

module.exports = FishingProjectionSchema
