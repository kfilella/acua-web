'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PoolImageSchema extends Schema {
  up () {
    this.create('pool_images', (table) => {
      table.increments()
      table.integer('pool_id').references('id').inTable('pools').notNullable()
      table.date('date').notNullable()
      table.string('description').notNullable()
      table.string('filename').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('pool_images')
  }
}

module.exports = PoolImageSchema
