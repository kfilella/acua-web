'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PhytoplanktonAnalysisDetailSchema extends Schema {
  up () {
    this.create('phytoplankton_analysis_details', (table) => {
      table.increments().notNullable()
      table.integer('phytoplankton_analysis_pools_id').unsigned().references('id').inTable('phytoplankton_analysis_pools').notNullable()
      table.integer('pool_id').unsigned().references('id').inTable('pools').notNullable()
      table.integer('pcyclotellah')
      table.integer('coscinodiscus')
      table.integer('chaetoceros')
      table.integer('melosira')
      table.integer('nitzschia')
      table.integer('skelotonema')
      table.integer('navicles')
      table.integer('thalassiosira')
      table.integer('ceratium')
      table.integer('peridinium')
      table.integer('chlamydomonas')
      table.integer('scenedesmus')
      table.integer('chlorella')
      table.integer('anabaena')
      table.integer('oscillatory')
      table.integer('nostoc')
      table.integer('anacystis')
      table.integer('protozoa')
      table.integer('euglena')
      table.text('observations')
      table.timestamps()
    })
  }

  down () {
    this.drop('phytoplankton_analysis_details')
  }
}

module.exports = PhytoplanktonAnalysisDetailSchema
