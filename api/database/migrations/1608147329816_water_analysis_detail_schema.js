'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class WaterAnalysisDetailSchema extends Schema {
  up () {
    this.table('water_analysis_details', (table) => {
      table.decimal('nitrite', 15, 3).alter()
    })
  }

  down () {
    this.table('water_analysis_details', (table) => {
      table.decimal('nitrite').alter()
    })
  }
}

module.exports = WaterAnalysisDetailSchema
