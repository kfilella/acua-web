'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PatologiesAnalysisPoolSchema extends Schema {
  up () {
    this.table('patologies_analysis_pools', (table) => {
      table.text('observations')
      table.text('recommendations')
    })
  }

  down () {
    this.table('patologies_analysis_pools', (table) => {
      table.dropColumn('observations')
      table.dropColumn('recommendations')
    })
  }
}

module.exports = PatologiesAnalysisPoolSchema
