'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FoodProjectionSchema extends Schema {
  up () {
    this.table('food_projection_master', (table) => {
      table.integer('stock_weight').alter()
    })
  }

  down () {
    this.table('food_projection_master', (table) => {
      table.decimal('stock_weight').alter()
    })
  }
}

module.exports = FoodProjectionSchema
