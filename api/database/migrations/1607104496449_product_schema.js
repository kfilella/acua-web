'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProductSchema extends Schema {
  up () {
    this.table('products', (table) => {      
      table.decimal('from_weight', 15, 3)
      table.decimal('to_weight', 15, 3)
      table.decimal('price_kg', 15, 3)
    })
  }

  down () {
    this.table('products', (table) => {
      table.dropColumn('from_weight')
      table.dropColumn('to_weight')
      table.dropColumn('price_kg')
    })
  }
}

module.exports = ProductSchema
