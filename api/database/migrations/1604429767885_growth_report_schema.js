'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class GrowthReportSchema extends Schema {
  up () {
    /* this.create('growth_reports', (table) => {
      table.increments()
      table.integer('fishing_projection_id').references('id').inTable('fishing_projections')
      table.date('date')
      table.decimal('size', 15, 3)
      table.decimal('percent', 5, 2)
      table.decimal('estimated', 15, 3)
      table.decimal('dist', 15, 3)
      table.decimal('mr', 15, 3)
      table.decimal('mf', 15, 3)
      table.timestamps()
    }) */
  }

  down () {
    // this.drop('growth_reports')
  }
}

module.exports = GrowthReportSchema
