'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PhytoplanktonAnalysisDetailSchema extends Schema {
  up () {
    this.table('phytoplankton_analysis_details', (table) => {
      table.datetime('deleted_at')
    })
  }

  down () {
    this.table('phytoplankton_analysis_details', (table) => {
      table.dropColumn('deleted_at')
    })
  }
}

module.exports = PhytoplanktonAnalysisDetailSchema
