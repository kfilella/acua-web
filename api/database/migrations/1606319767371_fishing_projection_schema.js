'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FishingProjectionSchema extends Schema {
  up () {
    this.table('fishing_projections', (table) => {
      table.integer('technical_advisor_id')
    })
  }

  down () {
    this.table('fishing_projections', (table) => {
      table.dropColumn('technical_advisor_id')
    })
  }
}

module.exports = FishingProjectionSchema
