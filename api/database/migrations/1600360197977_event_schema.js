'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EventSchema extends Schema {
  up () {
    this.table('events', (table) => {
      table.string('subject')
      table.string('content')
    })
  }

  down () {
    this.table('events', (table) => {
      table.dropColumn('subject')
      table.dropColumn('content')
    })
  }
}

module.exports = EventSchema
