'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CustomerSchema extends Schema {
  up () {
    this.table('customers', (table) => {
      table.string('sap_KUNNR')
    })
  }

  down () {
    this.table('customers', (table) => {
      table.dropColumn('sap_KUNNR')
    })
  }
}

module.exports = CustomerSchema
