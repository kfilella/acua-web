'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CompendiumGrowthReportSchema extends Schema {
  up () {
    this.table('compendium_growth_reports', (table) => {
      table.integer('kg_alim')
      table.integer('mp')
    })
  }

  down () {
    this.table('compendium_growth_reports', (table) => {
      table.dropColumn('kg_alim')
      table.dropColumn('mp')
    })
  }
}

module.exports = CompendiumGrowthReportSchema
