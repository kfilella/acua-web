'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FoodProjectionMasterSchema extends Schema {
  up () {
    this.create('food_projection_master', (table) => {
      table.increments()
      table.integer('pool_id').notNullable()
      table.date('seedtime')
      table.integer('cultivation_days')
      table.integer('density')
      table.boolean('initial_biomass')
      table.decimal('stock_weight')
      table.decimal('harvest_target_weight')
      table.decimal('survival')
      table.decimal('fc')
      table.decimal('survival_after30days')
      table.timestamps()
    })
  }

  down () {
    this.drop('food_projection_master')
  }
}

module.exports = FoodProjectionMasterSchema
