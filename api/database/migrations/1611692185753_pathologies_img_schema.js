'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PathologiesImgSchema extends Schema {
  up () {
    this.table('pathologies_imgs', (table) => {
      table.integer('patologies_analysis_detail_id').references('id').inTable('patologies_analysis_details').nullable()
    })
  }

  down () {
    this.table('pathologies_imgs', (table) => {
      table.dropColumn('patologies_analysis_detail_id')
    })
  }
}

module.exports = PathologiesImgSchema
