'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MailConfigurationSchema extends Schema {
  up () {
    this.create('mail_configurations', (table) => {
      table.increments()
      table.string('host')
      table.integer('port')
      table.boolean('secure')
      table.string('user')
      table.string('pass')
      table.string('from')
      table.timestamps()
    })
  }

  down () {
    this.drop('mail_configurations')
  }
}

module.exports = MailConfigurationSchema
