'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PhytoplanktonAnalysisPoolSchema extends Schema {
  up () {
    this.table('phytoplankton_analysis_pools', (table) => {
      // alter table
      table.text('observations')
      table.text('recommendations')
    })
  }

  down () {
    this.table('phytoplankton_analysis_pools', (table) => {
      // reverse alternations
      table.dropColumn('observations')
      table.dropColumn('recommendations')
    })
  }
}

module.exports = PhytoplanktonAnalysisPoolSchema
