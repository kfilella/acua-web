'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CategorySchema extends Schema {
  up () {
    this.table('categories', (table) => {
      // table.string('name').notNullable().alter()
    })
  }

  down () {
    this.table('categories', (table) => {
      // table.string('name').notNullable().unique().alter()
    })
  }
}

module.exports = CategorySchema
