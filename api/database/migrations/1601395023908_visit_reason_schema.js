'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class VisitReasonSchema extends Schema {
  up () {
    this.create('visit_reasons', (table) => {
      table.increments()
      table.string('name')
      table.timestamps()
    })
  }

  down () {
    this.drop('visit_reasons')
  }
}

module.exports = VisitReasonSchema
