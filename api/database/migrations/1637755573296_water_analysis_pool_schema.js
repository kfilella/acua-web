'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class WaterAnalysisPoolSchema extends Schema {
  up () {
    this.table('water_analysis_master', (table) => {
      table.text('observations')
      table.text('recommendations')
    })
  }

  down () {
    this.table('water_analysis_master', (table) => {
      table.dropColumn('observations')
      table.dropColumn('recommendations')
    })
  }
}

module.exports = WaterAnalysisPoolSchema
