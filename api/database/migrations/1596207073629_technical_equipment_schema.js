'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TechnicalEquipmentSchema extends Schema {
  up () {
    this.create('technical_equipments', (table) => {
      table.increments()
      table.string('code')
      table.string('team_name').notNullable()
      table.string('technical_chief')
      table.string('zone')
      table.string('technical_advisors').notNullable()
      table.boolean('status').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('technical_equipments')
  }
}

module.exports = TechnicalEquipmentSchema
