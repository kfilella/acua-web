'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FoodProjectionDetailSchema extends Schema {
  up () {
    this.create('food_projection_detail', (table) => {
      table.increments()
      table.integer('food_projection_master_id')
      table.integer('day')
      table.decimal('food_day', 15, 3)
      table.decimal('biomass_rate')
      table.decimal('liveweight', 8, 3)
      table.integer('food_type')
      table.decimal('biomass')
      table.timestamps()
    })
  }

  down () {
    this.drop('food_projection_detail')
  }
}

module.exports = FoodProjectionDetailSchema
