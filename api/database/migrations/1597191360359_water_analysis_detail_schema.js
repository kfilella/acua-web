'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class WaterAnalysisDetailSchema extends Schema {
  up () {
    this.create('water_analysis_details', (table) => {
      table.increments().notNullable()
      table.integer('water_type') // 0 Salada, 1 dulce
      table.integer('water_analysis_master_id').unsigned().references('id').inTable('water_analysis_master').notNullable()
      table.integer('pool_id').unsigned().references('id').inTable('pools').notNullable()
      table.integer('average_weight')
      table.integer('planting_density')
      table.integer('climatology')
      table.integer('oxygen')
      table.integer('ph')
      table.integer('temperature')
      table.integer('salinity')
      table.integer('dissolved')
      table.integer('turbidity')
      table.integer('alkalinity')
      table.integer('ammonium')
      table.integer('nitrite')
      table.integer('nitrate')
      table.integer('phosphate')
      table.integer('magnesium')
      table.integer('calcium')
      table.integer('potassium')
      table.text('observations')
      table.timestamps()
    })
  }

  down () {
    this.drop('water_analysis_details')
  }
}

module.exports = WaterAnalysisDetailSchema
