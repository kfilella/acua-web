'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class HarvestReportSchema extends Schema {
  up () {
    this.table('harvest_reports', (table) => {
      table.decimal('dollar_pound_first_thinning', 15 ,2)
      table.decimal('dollar_pound_second_thinning', 15 ,2)
      table.decimal('dollar_pound_third_thinning', 15 ,2)
      table.integer('seed').alter()
      table.decimal('seed_weight', 15, 2).alter()
      table.integer('kg_food').alter()
      table.decimal('us_dollar_day_ha', 15, 2).alter()
      table.decimal('us_dollar_pound', 15, 2).alter()
      table.integer('pounds_first_thinning').alter()
      table.integer('pounds_second_thinning').alter()
      table.integer('pounds_third_thinning').alter()
      table.integer('pounds_fourth_thinning').alter()
      table.decimal('weight_first_thinning', 15, 2).alter()
      table.decimal('weight_second_thinning', 15, 2).alter()
      table.decimal('weight_third_thinning', 15, 2).alter()
      table.decimal('weight_fourth_thinning', 15, 2).alter()
      table.integer('dry_days')
      table.decimal('cost_kilo_food', 15, 2)
      table.decimal('youth_cost', 15, 2)
    })
  }

  down () {
    this.table('harvest_reports', (table) => {
      table.dropColumn('dollar_pound_first_thinning')
      table.dropColumn('dollar_pound_second_thinning')
      table.dropColumn('dollar_pound_third_thinning')
      table.decimal('seed', 15, 3).alter()
      table.decimal('seed_weight', 15, 3).alter()
      table.decimal('kg_food', 15, 3).alter()
      table.decimal('us_dollar_day_ha', 15, 3).alter()
      table.decimal('us_dollar_pound', 15, 3).alter()
      table.decimal('pounds_first_thinning', 15, 3).alter()
      table.decimal('pounds_second_thinning', 15, 3).alter()
      table.decimal('pounds_third_thinning', 15, 3).alter()
      table.decimal('pounds_fourth_thinning', 15, 3).alter()
      table.decimal('weight_first_thinning', 15, 3).alter()
      table.decimal('weight_second_thinning', 15, 3).alter()
      table.decimal('weight_third_thinning', 15, 3).alter()
      table.decimal('weight_fourth_thinning', 15, 3).alter()
      table.dropColumn('dry_days')
      table.dropColumn('cost_kilo_food')
      table.dropColumn('youth_cost')
    })
  }
}

module.exports = HarvestReportSchema
