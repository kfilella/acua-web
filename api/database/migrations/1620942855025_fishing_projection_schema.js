'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FishingProjectionSchema extends Schema {
  up () {
    this.table('fishing_projections', (table) => {
      table.text('observations')
    })
  }

  down () {
    this.table('fishing_projections', (table) => {
      table.dropColumn('observations')
    })
  }
}

module.exports = FishingProjectionSchema
