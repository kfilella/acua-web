'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FloorAnalysisDetailSchema extends Schema {
  up () {
    this.table('floor_analysis_details', (table) => {
      table.decimal('ph', 15,2).alter()
    })
  }

  down () {
    this.table('floor_analysis_details', (table) => {
      table.integer('ph').alter()
    })
  }
}

module.exports = FloorAnalysisDetailSchema
