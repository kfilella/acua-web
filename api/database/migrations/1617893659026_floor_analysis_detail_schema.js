'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FloorAnalysisDetailSchema extends Schema {
  up () {
    this.table('floor_analysis_details', (table) => {
      table.integer('redox_potential')
      table.decimal('sulfides', 15 , 1)
      table.decimal('iron', 15, 1)
    })
  }

  down () {
    this.table('floor_analysis_details', (table) => {
      table.dropColumn('redox_potential')
      table.dropColumn('sulfides')
      table.dropColumn('iron')
    })
  }
}

module.exports = FloorAnalysisDetailSchema
