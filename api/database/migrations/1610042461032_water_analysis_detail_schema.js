'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class WaterAnalysisDetailSchema extends Schema {
  up () {
    this.table('water_analysis_details', (table) => {
      table.datetime('deleted_at')
    })
  }

  down () {
    this.table('water_analysis_details', (table) => {
      table.dropColumn('deleted_at')
    })
  }
}

module.exports = WaterAnalysisDetailSchema
