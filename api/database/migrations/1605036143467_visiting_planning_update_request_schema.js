'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class VisitingPlanningUpdateRequestSchema extends Schema {
  up () {
    this.table('visiting_planning_update_requests', (table) => {
      table.boolean('seen').defaultTo(false)
    })
  }

  down () {
    this.table('visiting_planning_update_requests', (table) => {
      table.dropColumn('seen')
    })
  }
}

module.exports = VisitingPlanningUpdateRequestSchema
