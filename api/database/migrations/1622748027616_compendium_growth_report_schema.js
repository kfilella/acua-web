'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CompendiumGrowthReportSchema extends Schema {
  up () {
    this.table('compendium_growth_reports', (table) => {
      table.dropColumn('compendium_id')
      table.integer('compendium_pool_id').references('id').inTable('compendium_pools')
      table.dropColumn('mp')
      table.dropColumn('size')
      // table.decimal('size', 5, 2)
    })

    this.table('compendium_growth_reports', (table) => {
      table.decimal('size', 5, 2)
    })
  }

  down () {
    this.table('compendium_growth_reports', (table) => {
      table.dropColumn('compendium_pool_id')
      table.decimal('size', 15, 3).alter()
    })
  }
}

module.exports = CompendiumGrowthReportSchema
