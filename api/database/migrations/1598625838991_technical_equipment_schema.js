'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TechnicalEquipmentSchema extends Schema {
  up () {
    this.table('technical_equipments', (table) => {
      table.integer('technical_chief').alter()
    })
  }

  down () {
    this.table('technical_equipments', (table) => {
      table.string('technical_chief').alter()
    })
  }
}

module.exports = TechnicalEquipmentSchema
