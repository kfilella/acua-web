'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PatologiesAnalysisDetailSchema extends Schema {
  up () {
    this.table('patologies_analysis_details', (table) => {
      table.integer('flaccidity')
      table.integer('misshapen_dwarfs')
      table.dropColumn('pool_intestine_id')
      table.integer('baculovirus')
      table.integer('gregarious')
      table.integer('nemamotods')
      table.integer('intestine_algae')
      table.integer('debris')
      table.integer('balanced')
      table.integer('crustacean_remains')
    })
  }

  down () {
    this.table('patologies_analysis_details', (table) => {
      table.dropColumn('flaccidity')
      table.dropColumn('misshapen_dwarfs')
      table.integer('pool_intestine_id').unsigned().references('id').inTable('pools').notNullable().defaultTo(1)
      table.dropColumn('baculovirus')
      table.dropColumn('gregarious')
      table.dropColumn('nemamotods')
      table.dropColumn('intestine_algae')
      table.dropColumn('debris')
      table.dropColumn('balanced')
      table.dropColumn('crustacean_remains')
    })
  }
}

module.exports = PatologiesAnalysisDetailSchema
