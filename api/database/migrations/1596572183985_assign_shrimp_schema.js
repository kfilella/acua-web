'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AssignShrimpSchema extends Schema {
  up () {
    this.create('assign_shrimps', (table) => {
      table.increments()
      table.string('shrimp_id')
      table.string('technical_advisor_id')
      table.timestamps()
    })
  }

  down () {
    this.drop('assign_shrimps')
  }
}

module.exports = AssignShrimpSchema
