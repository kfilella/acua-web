'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SectionSchema extends Schema {
  up () {
    this.table('sections', (table) => {
      table.integer('department_id').nullable().alter()
    })
  }

  down () {
    this.table('sections', (table) => {
      table.integer('department_id').notNullable().alter()
    })
  }
}

module.exports = SectionSchema
