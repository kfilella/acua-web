'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FishingProjectionDetailSchema extends Schema {
  up () {
    this.table('fishing_projection_details', (table) => {
      table.integer('thinning')
      table.decimal('pounds_ha_thinning', 10, 0)
      table.decimal('weight_thinning', 10, 2)
    })
  }

  down () {
    this.table('fishing_projection_details', (table) => {
      table.dropColumn('thinning')
      table.dropColumn('pounds_ha_thinning')
      table.dropColumn('weight_thinning')
    })
  }
}

module.exports = FishingProjectionDetailSchema
