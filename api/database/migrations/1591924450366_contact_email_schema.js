'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContactEmailSchema extends Schema {
  up () {
    this.create('contact_emails', (table) => {
      table.increments()
      table.integer('contact_id')
      table.foreign('contact_id').references('contacts.id').onDelete('cascade')
      table.string('email')
      table.timestamps()
    })
  }

  down () {
    this.drop('contact_emails')
  }
}

module.exports = ContactEmailSchema
