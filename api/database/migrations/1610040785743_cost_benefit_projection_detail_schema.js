'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CostBenefitProjectionDetailSchema extends Schema {
  up () {
    this.table('cost_benefit_projection_details', (table) => {
      table.datetime('deleted_at')
    })
  }

  down () {
    this.table('cost_benefit_projection_details', (table) => {
      table.dropColumn('deleted_at')
    })
  }
}

module.exports = CostBenefitProjectionDetailSchema
