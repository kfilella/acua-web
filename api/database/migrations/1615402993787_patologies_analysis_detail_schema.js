'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PatologiesAnalysisDetailSchema extends Schema {
  up () {
    this.table('patologies_analysis_details', (table) => {
      table.text('others')
      table.integer('vermiform_bodies')
      table.integer('gregarine_eggs')
    })
  }

  down () {
    this.table('patologies_analysis_details', (table) => {
      table.dropColumn('others')
      table.dropColumn('vermiform_bodies')
      table.dropColumn('gregarine_eggs')
    })
  }
}

module.exports = PatologiesAnalysisDetailSchema
