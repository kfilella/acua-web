'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PoolImageSchema extends Schema {
  up () {
    this.table('pool_images', (table) => {
      table.datetime('deleted_at')
    })
  }

  down () {
    this.table('pool_images', (table) => {
      table.dropColumn('deleted_at')
    })
  }
}

module.exports = PoolImageSchema
