'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProviderDeliverAddressSchema extends Schema {
  up () {
    this.create('provider_deliver_addresses', (table) => {
      table.increments()
      table.integer('provider_id').references('id').inTable('providers')
      table.text('address').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('provider_deliver_addresses')
  }
}

module.exports = ProviderDeliverAddressSchema
