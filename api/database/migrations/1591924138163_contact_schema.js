'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContactSchema extends Schema {
  up () {
    this.table('contacts', (table) => {
      table.string('job')
    })
  }

  down () {
    this.table('contacts', (table) => {
      table.dropColumn('job')
    })
  }
}

module.exports = ContactSchema
