'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProviderDocumentSchema extends Schema {
  up () {
    this.create('provider_documents', (table) => {
      table.increments()
      table.integer('provider_id').references('id').inTable('providers').notNullable()
      table.integer('type').comment('1: RUC, 2: Certificado de Calificación, 3: Otros Certificados')
      table.string('filename')
      table.timestamps()
    })
  }

  down () {
    this.drop('provider_documents')
  }
}

module.exports = ProviderDocumentSchema
