'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FoodsSchema extends Schema {
  up () {
    this.create('foods', (table) => {
      table.bigIncrements('id')
      table.string('code')
      table.string('name')
      table.string('coste')
      table.float('Int_range')
      table.float('End_range')
      table.timestamps()
    })
  }

  down () {
    this.drop('foods')
  }
}

module.exports = FoodsSchema
