'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProviderSchema extends Schema {
  up () {
    this.create('providers', (table) => {
      table.increments()
      table.string('type').notNullable().comment('1: Natural, 2: Jurídica') //Persona natural o jurídica
      table.boolean('foreign_company').defaultTo(false)
      table.string('legal_name').notNullable()
      table.string('comercial_name')
      table.integer('category_id').references('id').inTable('categories')
      table.string('document_type').notNullable() // Cédula, RUC o pasaporte
      table.string('document_number')
      table.string('electronic_documents_email')
      table.integer('country_id').references('id').inTable('countries').notNullable()
      table.integer('state_id').references('id').inTable('states')
      table.integer('city_id').references('id').inTable('cities')
      table.text('billing_address')
      table.text('deliver_address')
      table.string('phone')
      table.string('website')
      table.string('tags')
      table.boolean('supplier_qualification')
      table.integer('rating_company_id').references('id').inTable('rating_companies')
      table.integer('rating_type_id').references('id').inTable('rating_types')
      table.integer('segment_id').references('id').inTable('segments')
      table.timestamps()
      table.datetime('deleted_at')
    })
  }

  down () {
    this.drop('providers')
  }
}

module.exports = ProviderSchema
