'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ShrimpGrowthReportSizeSchema extends Schema {
  up () {
    this.create('shrimp_growth_report_sizes', (table) => {
      table.increments()
      table.integer('shrimp_growth_report_id').references('id').inTable('shrimp_growth_reports')
      table.integer('pool_id').references('id').inTable('pools')
      table.date('date')
      table.decimal('value', 15, 3).defaultTo(0)
      table.integer('product_id')
      table.timestamps()
    })
  }

  down () {
    this.drop('shrimp_growth_report_sizes')
  }
}

module.exports = ShrimpGrowthReportSizeSchema
