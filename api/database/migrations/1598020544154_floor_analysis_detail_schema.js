'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FloorAnalysisDetailSchema extends Schema {
  up () {
    this.create('floor_analysis_details', (table) => {
      table.increments().notNullable()
      table.integer('floor_analysis_master_id').unsigned().references('id').inTable('floor_analysis_master').notNullable()
      table.integer('pool_id').unsigned().references('id').inTable('pools').notNullable()
      table.integer('ph')
      table.integer('organic_material')
      table.text('observations')
      table.timestamps()
    })
  }

  down () {
    this.drop('floor_analysis_details')
  }
}

module.exports = FloorAnalysisDetailSchema
