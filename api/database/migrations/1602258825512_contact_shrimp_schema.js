'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContactShrimpSchema extends Schema {
  up () {
    this.table('contact_shrimps', (table) => {
      table.integer('shrimp_id').alter()
      table.integer('contact_id').alter()
    })
  }

  down () {
    this.table('contact_shrimps', (table) => {
      table.string('shrimp_id').alter()
      table.string('contact_id').alter()
    })
  }
}

module.exports = ContactShrimpSchema
