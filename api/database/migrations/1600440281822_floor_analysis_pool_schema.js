'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FloorAnalysisPoolSchema extends Schema {
  up () {
    this.table('floor_analysis_master', (table) => {
      table.dropColumn('zone_id')
    })
  }

  down () {
    this.table('floor_analysis_master', (table) => {
      table.integer('zone_id')
    })
  }
}

module.exports = FloorAnalysisPoolSchema
