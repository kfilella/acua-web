'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FoodProjectionDetailSchema extends Schema {
  up () {
    this.table('food_projection_detail', (table) => {
      table.decimal('biomass', 17, 2).alter()
    })
  }

  down () {
    this.table('food_projection_detail', (table) => {
      table.decimal('biomass').alter()
    })
  }
}

module.exports = FoodProjectionDetailSchema
