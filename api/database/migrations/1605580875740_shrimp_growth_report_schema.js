'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ShrimpGrowthReportSchema extends Schema {
  up () {
    this.create('shrimp_growth_reports', (table) => {
      table.increments()
      table.integer('shrimp_id').references('id').inTable('shrimps')
      table.integer('technical_advisor_id').references('id').inTable('technical_advisors')
      table.date('begin_date')
      table.date('end_date')
      table.timestamps()
    })
  }

  down () {
    this.drop('shrimp_growth_reports')
  }
}

module.exports = ShrimpGrowthReportSchema
