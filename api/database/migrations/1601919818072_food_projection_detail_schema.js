'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FoodProjectionDetailSchema extends Schema {
  up () {
    this.table('food_projection_detail', (table) => {
      table.decimal('real_food_day', 15, 3)
      table.decimal('real_biomass_rate')
      table.decimal('real_liveweight', 8, 3)
      table.string('real_food_type')
      table.decimal('real_survival')
    })
  }

  down () {
    this.table('food_projection_detail', (table) => {
      table.dropColumn('real_food_day')
      table.dropColumn('real_biomass_rate')
      table.dropColumn('real_liveweight')
      table.dropColumn('real_food_type')
      table.dropColumn('real_survival')
    })
  }
}

module.exports = FoodProjectionDetailSchema
