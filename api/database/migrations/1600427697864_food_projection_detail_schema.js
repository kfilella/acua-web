'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FoodProjectionDetailSchema extends Schema {
  up () {
    this.table('food_projection_detail', (table) => {
      table.decimal('survival')
    })
  }

  down () {
    this.table('food_projection_detail', (table) => {
      table.dropColumn('survival')
    })
  }
}

module.exports = FoodProjectionDetailSchema
