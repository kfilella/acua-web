'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserEventSchema extends Schema {
  up () {
    this.create('user_events', (table) => {
      table.increments()
      table.integer('user_id').references('id').inTable('users')
      table.integer('event_id').references('id').inTable('events')
      table.string('subject')
      table.text('content')
      table.boolean('mail').defaultTo(true)
      table.boolean('bell').defaultTo(true)
      table.timestamps()
    })
  }

  down () {
    this.drop('user_events')
  }
}

module.exports = UserEventSchema
