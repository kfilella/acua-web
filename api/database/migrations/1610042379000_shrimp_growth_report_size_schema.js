'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ShrimpGrowthReportSizeSchema extends Schema {
  up () {
    this.table('shrimp_growth_report_sizes', (table) => {
      table.datetime('deleted_at')
    })
  }

  down () {
    this.table('shrimp_growth_report_sizes', (table) => {
      table.dropColumn('deleted_at')
    })
  }
}

module.exports = ShrimpGrowthReportSizeSchema
