'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PatologiesAnalysisPoolSchema extends Schema {
  up () {
    this.create('patologies_analysis_pools', (table) => {
      table.increments()
      table.integer('shrimp_id').unsigned().references('id').inTable('shrimps').notNullable()
      table.date('analysis_date').notNullable()
      table.integer('technical_advisor_id').unsigned().references('id').inTable('users').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('patologies_analysis_pools')
  }
}

module.exports = PatologiesAnalysisPoolSchema
