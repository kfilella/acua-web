'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')
const UserEvent = use('App/Models/UserEvent')

class UserEventSchema extends Schema {
  async up () {
    this.table('user_events', (table) => {
      table.boolean('mail').defaultTo(false).alter()
      table.boolean('bell').defaultTo(false).alter()
    })
    await UserEvent.query().update({ mail: false, bell: false })
  }

  down () {
    this.table('user_events', (table) => {
      table.boolean('mail').defaultTo(true).alter()
      table.boolean('bell').defaultTo(true).alter()
    })
  }
}

module.exports = UserEventSchema
