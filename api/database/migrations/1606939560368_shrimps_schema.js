'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ShrimpsSchema extends Schema {
  up () {
    this.table('shrimps', (table) => {
      table.integer('agripac_atendets')
    })
  }

  down () {
    this.table('shrimps', (table) => {
      table.dropColumn('agripac_atendets')
    })
  }
}

module.exports = ShrimpsSchema
