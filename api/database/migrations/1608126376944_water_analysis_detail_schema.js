'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class WaterAnalysisDetailSchema extends Schema {
  up () {
    this.table('water_analysis_details', (table) => {
      table.text('recommendations').alter()
    })
  }

  down () {
    this.table('water_analysis_details', (table) => {
      table.string('recommendations').alter()
    })
  }
}

module.exports = WaterAnalysisDetailSchema
