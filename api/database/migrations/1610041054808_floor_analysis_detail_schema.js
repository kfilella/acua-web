'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FloorAnalysisDetailSchema extends Schema {
  up () {
    this.table('floor_analysis_details', (table) => {
      table.datetime('deleted_at')
    })
  }

  down () {
    this.table('floor_analysis_details', (table) => {
      table.dropColumn('deleted_at')
    })
  }
}

module.exports = FloorAnalysisDetailSchema
