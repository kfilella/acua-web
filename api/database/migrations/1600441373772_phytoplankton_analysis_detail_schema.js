'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PhytoplanktonAnalysisDetailSchema extends Schema {
  up () {
    this.table('phytoplankton_analysis_details', (table) => {
      table.decimal('pcyclotellah').alter()
      table.decimal('coscinodiscus').alter()
      table.decimal('chaetoceros').alter()
      table.decimal('melosira').alter()
      table.decimal('nitzschia').alter()
      table.decimal('skelotonema').alter()
      table.decimal('navicles').alter()
      table.decimal('thalassiosira').alter()
      table.decimal('ceratium').alter()
      table.decimal('peridinium').alter()
      table.decimal('chlamydomonas').alter()
      table.decimal('scenedesmus').alter()
      table.decimal('chlorella').alter()
      table.decimal('anabaena').alter()
      table.decimal('oscillatory').alter()
      table.decimal('nostoc').alter()
      table.decimal('anacystis').alter()
      table.decimal('protozoa').alter()
      table.decimal('euglena').alter()
    })
  }

  down () {
    this.table('phytoplankton_analysis_details', (table) => {
      table.integer('pcyclotellah').alter()
      table.integer('coscinodiscus').alter()
      table.integer('chaetoceros').alter()
      table.integer('melosira').alter()
      table.integer('nitzschia').alter()
      table.integer('skelotonema').alter()
      table.integer('navicles').alter()
      table.integer('thalassiosira').alter()
      table.integer('ceratium').alter()
      table.integer('peridinium').alter()
      table.integer('chlamydomonas').alter()
      table.integer('scenedesmus').alter()
      table.integer('chlorella').alter()
      table.integer('anabaena').alter()
      table.integer('oscillatory').alter()
      table.integer('nostoc').alter()
      table.integer('anacystis').alter()
      table.integer('protozoa').alter()
      table.integer('euglena').alter()
    })
  }
}

module.exports = PhytoplanktonAnalysisDetailSchema
