'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TechnicalAdvisorSchema extends Schema {
  up () {
    this.table('technical_advisors', (table) => {
      table.integer('user_id').unsigned().defaultTo(0)
    })
  }

  down () {
    this.table('technical_advisors', (table) => {
      table.dropColumn('user_id')
    })
  }
}

module.exports = TechnicalAdvisorSchema
