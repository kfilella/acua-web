'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class HarvestReportFatteningFoodSchema extends Schema {
  up () {
    this.create('harvest_report_fattening_foods', (table) => {
      table.increments()
      table.integer('harvest_report_id').references('id').inTable('harvest_reports')
      table.integer('product_id')
      table.timestamps()
    })
  }

  down () {
    this.drop('harvest_report_fattening_foods')
  }
}

module.exports = HarvestReportFatteningFoodSchema
