'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PhytoplanktonAnalysisDetailSchema extends Schema {
  up () {
    this.table('phytoplankton_analysis_details', (table) => {
      table.integer('amphora')
      table.integer('coelastrum')
      table.integer('diplonei')
      table.integer('nitzschia_long')
      table.integer('other_cianofitas')
      table.integer('other_clorofitas')
      table.integer('other_diatomea')
      table.integer('spirulina')
    })
  }

  down () {
    this.table('phytoplankton_analysis_details', (table) => {
      table.dropColumn('amphora')
      table.dropColumn('coelastrum')
      table.dropColumn('diplonei')
      table.dropColumn('nitzschia_long')
      table.dropColumn('other_cianofitas')
      table.dropColumn('other_clorofitas')
      table.dropColumn('other_diatomea')
      table.dropColumn('spirulina')
    })
  }
}

module.exports = PhytoplanktonAnalysisDetailSchema
