'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContactPhoneSchema extends Schema {
  up () {
    this.create('contact_phones', (table) => {
      table.increments()
      table.integer('contact_id')
      table.foreign('contact_id').references('contacts.id').onDelete('cascade')
      table.string('phone')
      table.timestamps()
    })
  }

  down () {
    this.drop('contact_phones')
  }
}

module.exports = ContactPhoneSchema
