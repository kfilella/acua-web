'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FishingProjectionDetailSchema extends Schema {
  up () {
    this.create('fishing_projection_details', (table) => {
      table.increments()
      table.integer('fishing_projection_id').references('id').inTable('fishing_projections')
      table.decimal('weight', 5, 2)
      table.decimal('must_growth', 5, 2)
      table.decimal('survival', 5, 2)
      table.decimal('kg_ha_day', 5, 2)
      table.decimal('dollar_bag', 5, 2)
      table.decimal('youth_cost', 5, 2)
      table.decimal('fixed_cost', 5, 2)
      table.timestamps()
    })
  }

  down () {
    this.drop('fishing_projection_details')
  }
}

module.exports = FishingProjectionDetailSchema
