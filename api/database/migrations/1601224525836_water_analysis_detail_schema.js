'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class WaterAnalysisDetailSchema extends Schema {
  up () {
    this.table('water_analysis_details', (table) => {
      table.string('recommendations')
    })
  }

  down () {
    this.table('water_analysis_details', (table) => {
      table.dropColumn('recommendations')
    })
  }
}

module.exports = WaterAnalysisDetailSchema
