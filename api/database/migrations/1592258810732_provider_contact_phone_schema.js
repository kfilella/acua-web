'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProviderContactPhoneSchema extends Schema {
  up () {
    this.create('provider_contact_phones', (table) => {
      table.increments()
      table.integer('provider_contact_id')
      table.foreign('provider_contact_id').references('provider_contacts.id').onDelete('cascade')
      table.string('phone').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('provider_contact_phones')
  }
}

module.exports = ProviderContactPhoneSchema
