'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ShrimpSchema extends Schema {
  up () {
    this.create('shrimps', (table) => {
      table.increments()
      table.integer('customer_id').references('id').inTable('customers').notNullable()
      table.integer('state_id').references('id').inTable('states').notNullable()
      table.integer('city_id').references('id').inTable('cities').notNullable()
      table.json('ubication')
      table.timestamps()
    })
  }

  down () {
    this.drop('shrimps')
  }
}

module.exports = ShrimpSchema
