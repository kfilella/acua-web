'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CategorySchema extends Schema {
  up () {
    this.create('categories', (table) => {
      table.increments()
      table.integer('type').comment('1: Clientes, 2: Proveedores, 3: Productos').notNullable()
      table.string('name').notNullable()
      table.timestamps()
      table.datetime('deleted_at')
    })
  }

  down () {
    this.drop('categories')
  }
}

module.exports = CategorySchema
