'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ShrimpsSchema extends Schema {
  up () {
    this.table('shrimps', (table) => {
      table.integer('extended_all')
    })
  }

  down () {
    this.table('shrimps', (table) => {
      table.dropColumn('extended_all')
    })
  }
}

module.exports = ShrimpsSchema
