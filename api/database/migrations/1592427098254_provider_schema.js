'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProviderSchema extends Schema {
  up () {
    this.table('providers', (table) => {
      table.integer('type').notNullable().alter() // modificar a entero
      table.integer('document_type').notNullable().alter()
      table.string('supplier_qualification').notNullable().alter()
    })
  }

  down () {
    this.table('providers', (table) => {
      table.string('type').notNullable().alter() // modificar a string otra vez
      table.string('document_type').notNullable().alter()
      // table.boolean('document_type').notNullable().alter()
    })
  }
}

module.exports = ProviderSchema
