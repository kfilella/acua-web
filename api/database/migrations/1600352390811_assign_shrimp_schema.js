'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AssignShrimpSchema extends Schema {
  up () {
    this.table('assign_shrimps', (table) => {
      table.integer('technical_advisor_id').alter()
      table.integer('shrimp_id').alter()
    })
  }

  down () {
    this.table('assign_shrimps', (table) => {
      table.string('technical_advisor_id').alter()
      table.string('shrimp_id').alter()
    })
  }
}

module.exports = AssignShrimpSchema
