'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PatologiesAnalysisDetailSchema extends Schema {
  up () {
    this.create('patologies_analysis_details', (table) => {
      table.increments().notNullable()
      table.integer('patologies_analysis_pools_id').unsigned().references('id').inTable('patologies_analysis_pools').notNullable()
      table.integer('pool_id').unsigned().references('id').inTable('pools').notNullable()
      table.integer('average_weight_pool')
      table.integer('inflameduropods_pool')
      table.integer('rough_antennas_pool')

      table.integer('dirty')
      table.integer('necrosis')
      table.integer('algae')
      table.integer('vortex')
      table.integer('zoothamnium')
      table.integer('acineta')
      table.integer('ciliated_apostome')
      table.integer('chromatophores')

      table.integer('lipids')
      table.integer('thickening')
      table.integer('deformity')
      table.integer('constriction')
      table.integer('necrosis_in_tubules')

      table.integer('average_intestine_weight')
      table.integer('inflamed_neuropods_intestine')
      table.integer('rough_antennas_intestine')
      table.text('observations')
      table.timestamps()
    })
  }

  down () {
    this.drop('patologies_analysis_details')
  }
}

module.exports = PatologiesAnalysisDetailSchema
