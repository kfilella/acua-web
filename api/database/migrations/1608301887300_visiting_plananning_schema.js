'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class VisitingPlananningSchema extends Schema {
  up () {
    this.table('visiting_plannings', (table) => {
      table.integer('shrimp_distance')
    })
  }

  down () {
    this.table('visiting_plannings', (table) => {
      table.dropColumn('shrimp_distance')
    })
  }
}

module.exports = VisitingPlananningSchema
