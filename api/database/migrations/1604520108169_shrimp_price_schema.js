'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ShrimpPriceSchema extends Schema {
  up () {
    this.create('shrimp_prices', (table) => {
      table.increments()
      table.decimal('price', 15, 2)
      table.date('date')
      table.timestamps()
    })
  }

  down () {
    this.drop('shrimp_prices')
  }
}

module.exports = ShrimpPriceSchema
