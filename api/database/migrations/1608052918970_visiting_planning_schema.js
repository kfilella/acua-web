'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class VisitingPlanningSchema extends Schema {
  up () {
    this.table('visiting_plannings', (table) => {
      table.json('ubication')
      table.date('real_date')
      table.time('real_time')
    })
  }

  down () {
    this.table('visiting_plannings', (table) => {
      table.dropColumn('ubication')
      table.dropColumn('real_date')
      table.dropColumn('real_time')
    })
  }
}

module.exports = VisitingPlanningSchema
