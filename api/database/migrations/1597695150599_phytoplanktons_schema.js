'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PhytoplanktonsSchema extends Schema {
  up () {
    this.table('phytoplanktons', (table) => {
      table.string('shrimp_id')// alter table
    })
  }

  down () {
    this.table('phytoplanktons', (table) => {
      table.dropColumn('shrimp_id')
    })
  }
}

module.exports = PhytoplanktonsSchema
