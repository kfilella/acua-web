'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class WaterAnalysisDetailSchema extends Schema {
  up () {
    this.table('water_analysis_details', (table) => {
      table.decimal('average_weight').alter()
      table.decimal('planting_density').alter()
      table.decimal('climatology').alter()
      table.decimal('oxygen').alter()
      table.decimal('ph').alter()
      table.decimal('temperature').alter()
      table.decimal('salinity').alter()
      table.decimal('dissolved').alter()
      table.decimal('turbidity').alter()
      table.decimal('alkalinity').alter()
      table.decimal('ammonium').alter()
      table.decimal('nitrite').alter()
      table.decimal('nitrate').alter()
      table.decimal('phosphate').alter()
      table.decimal('magnesium').alter()
      table.decimal('calcium').alter()
      table.decimal('potassium').alter()
    })
  }

  down () {
    this.table('water_analysis_details', (table) => {
      table.integer('average_weight').alter()
      table.integer('planting_density').alter()
      table.integer('climatology').alter()
      table.integer('oxygen').alter()
      table.integer('ph').alter()
      table.integer('temperature').alter()
      table.integer('salinity').alter()
      table.integer('dissolved').alter()
      table.integer('turbidity').alter()
      table.integer('alkalinity').alter()
      table.integer('ammonium').alter()
      table.integer('nitrite').alter()
      table.integer('nitrate').alter()
      table.integer('phosphate').alter()
      table.integer('magnesium').alter()
      table.integer('calcium').alter()
      table.integer('potassium').alter()
    })
  }
}

module.exports = WaterAnalysisDetailSchema
