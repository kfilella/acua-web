'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FloorAnalysisPoolSchema extends Schema {
  up () {
    this.table('floor_analysis_master', (table) => {
      table.date('sampling_reception_date')
    })
  }

  down () {
    this.table('floor_analysis_master', (table) => {
      table.dropColumn('sampling_reception_date')
    })
  }
}

module.exports = FloorAnalysisPoolSchema
