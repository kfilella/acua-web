'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RoleSchema extends Schema {
  up () {
    this.create('roles', (table) => {
      table.increments()
      table.string('name').unique().notNullable()
      table.string('description').nullable()
      table.timestamps()
      table.datetime('deleted_at')
    })
  }

  down () {
    this.drop('roles')
  }
}

module.exports = RoleSchema
