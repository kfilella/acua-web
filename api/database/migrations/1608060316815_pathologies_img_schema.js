'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PathologiesImgSchema extends Schema {
  up () {
    this.create('pathologies_imgs', (table) => {
      table.increments()
      table.string('analisys_id')
      table.string('description')
      table.date('date')
      table.string('dir')
      table.timestamps()
    })
  }

  down () {
    this.drop('pathologies_imgs')
  }
}

module.exports = PathologiesImgSchema
