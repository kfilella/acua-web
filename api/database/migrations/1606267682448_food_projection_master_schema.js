'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FoodProjectionMasterSchema extends Schema {
  up () {
    this.table('food_projection_master', (table) => {
      table.integer('technical_advisor_id')
    })
  }

  down () {
    this.table('food_projection_master', (table) => {
      table.dropColumn('technical_advisor_id')
    })
  }
}

module.exports = FoodProjectionMasterSchema
