'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CustomerPhoneSchema extends Schema {
  up () {
    this.create('customer_phones', (table) => {
      table.increments()
      table.integer('customer_id')
      table.string('phone')
      table.timestamps()
    })
  }

  down () {
    this.drop('customer_phones')
  }
}

module.exports = CustomerPhoneSchema
