'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FoodTypeSchema extends Schema {
  up () {
    this.table('food_types', (table) => {
      table.decimal('from_weight', 15, 3).alter()
      table.decimal('to_weight', 15, 3).alter()
    })
  }

  down () {
    this.table('food_types', (table) => {
      table.decimal('from_weight').alter()
      table.decimal('to_weight').alter()
    })
  }
}

module.exports = FoodTypeSchema
