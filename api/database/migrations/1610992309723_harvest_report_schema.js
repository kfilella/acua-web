'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class HarvestReportSchema extends Schema {
  up () {
    this.table('harvest_reports', (table) => {
      table.integer('technical_advisor_id')
    })
  }

  down () {
    this.table('harvest_reports', (table) => {
      table.dropColumn('technical_advisor_id')
    })
  }
}

module.exports = HarvestReportSchema
