'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CostProjectionSchema extends Schema {
  up () {
    this.table('cost_projections', (table) => {
      table.decimal('shrimp_price', 15, 2)
    })
  }

  down () {
    this.table('cost_projections', (table) => {
      table.dropColumn('shrimp_price')
    })
  }
}

module.exports = CostProjectionSchema
