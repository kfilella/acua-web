'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PatologiesAnalysisDetailSchema extends Schema {
  up () {
    this.table('patologies_analysis_details', (table) => {
      table.string('recommendations')
    })
  }

  down () {
    this.table('patologies_analysis_details', (table) => {
      table.dropColumn('recommendations')
    })
  }
}

module.exports = PatologiesAnalysisDetailSchema
