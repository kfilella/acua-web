'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProductSchema extends Schema {
  up () {
    this.table('products', (table) => {
      table.string('color')
      table.string('brand')
    })
  }

  down () {
    this.table('products', (table) => {
      table.dropColumn('color')
      table.dropColumn('brand')
    })
  }
}

module.exports = ProductSchema
