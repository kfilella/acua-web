'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CostBenefitProjectionDetailSchema extends Schema {
  up () {
    this.create('cost_benefit_projection_details', (table) => {
      table.increments()
      table.integer('cost_benefit_projection_id').references('id').inTable('cost_benefit_projections')
      table.integer('pool_id').references('id').inTable('pools')
      table.integer('thinning')
      table.integer('planting_type')
      table.decimal('density', 15, 3)
      table.decimal('survival', 15, 3)
      table.decimal('size_grs', 15, 3)
      table.decimal('size_planting', 15, 3)
      table.decimal('pounds', 15, 3)
      table.decimal('cent_gr', 15, 3)
      table.decimal('size_thinning', 15, 3)
      table.decimal('price', 15, 3)
      table.decimal('pounds_thinning', 15, 3)
      table.decimal('cost_per_thounsand', 15, 3)
      table.decimal('fc', 15, 3)
      table.decimal('us_kg_balance', 15, 3)
      table.decimal('production_days', 15, 3)
      table.decimal('stopping_days', 15, 3)
      table.decimal('cost_day_ha', 15, 3)
      table.timestamps()
    })
  }

  down () {
    this.drop('cost_benefit_projection_details')
  }
}

module.exports = CostBenefitProjectionDetailSchema
