'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class HarvestReportSchema extends Schema {
  up () {
    this.create('harvest_reports', (table) => {
      table.increments()
      table.integer('pool_id').references('id').inTable('pools')
      table.integer('seed_type')
      table.date('seed_time')
      table.date('harvest_date')
      table.integer('laboratory_id')
      table.decimal('seed', 15, 3)
      table.decimal('seed_weight', 15, 3)
      table.decimal('kg_food', 15, 3)
      table.decimal('us_dollar_pound', 15, 3)
      table.decimal('us_dollar_kg', 15, 3)
      table.decimal('cost_pound', 15, 3)
      table.decimal('us_dollar_day_ha', 15, 3)
      table.decimal('pounds_first_thinning', 15, 3)
      table.decimal('pounds_second_thinning', 15, 3)
      table.decimal('pounds_third_thinning', 15, 3)
      table.decimal('pounds_fourth_thinning', 15, 3)
      table.decimal('weight_first_thinning', 15, 3)
      table.decimal('weight_second_thinning', 15, 3)
      table.decimal('weight_third_thinning', 15, 3)
      table.decimal('weight_fourth_thinning', 15, 3)
      table.timestamps()
    })
  }

  down () {
    this.drop('harvest_reports')
  }
}

module.exports = HarvestReportSchema
