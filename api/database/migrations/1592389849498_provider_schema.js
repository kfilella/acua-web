'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProviderSchema extends Schema {
  up () {
    this.table('providers', (table) => {
      table.string('code')
      table.string('zone')
      table.string('sector')
    })
  }

  down () {
    this.table('providers', (table) => {
      table.dropColumn('code')
      table.dropColumn('zone')
      table.dropColumn('sector')
    })
  }
}

module.exports = ProviderSchema
