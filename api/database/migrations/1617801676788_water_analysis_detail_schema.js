'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class WaterAnalysisDetailSchema extends Schema {
  up () {
    this.table('water_analysis_details', (table) => {
      table.decimal('planting_density', 15, 0).alter()
      table.integer('climatology').alter()
    })
  }

  down () {
    this.table('water_analysis_details', (table) => {
      table.decimal('planting_density').alter()
      table.decimal('climatology').alter()
    })
  }
}

module.exports = WaterAnalysisDetailSchema
