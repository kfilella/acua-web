'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PhytoplanktonAnalysisDetailSchema extends Schema {
  up () {
    this.table('phytoplankton_analysis_details', (table) => {
      table.bigInteger('pcyclotellah').alter()
      table.bigInteger('coscinodiscus').alter()
      table.bigInteger('chaetoceros').alter()
      table.bigInteger('melosira').alter()
      table.bigInteger('nitzschia').alter()
      table.bigInteger('skelotonema').alter()
      table.bigInteger('navicles').alter()
      table.bigInteger('thalassiosira').alter()
      table.bigInteger('ceratium').alter()
      table.bigInteger('peridinium').alter()
      table.bigInteger('chlamydomonas').alter()
      table.bigInteger('chlorella').alter()
      table.bigInteger('scenedesmus').alter()
      table.bigInteger('anabaena').alter()
      table.bigInteger('oscillatory').alter()
      table.bigInteger('nostoc').alter()
      table.bigInteger('anacystis').alter()
      table.bigInteger('protozoa').alter()
      table.bigInteger('euglena').alter()
      table.text('chart_image')
    })
  }

  down () {
    this.table('phytoplankton_analysis_details', (table) => {
      table.decimal('pcyclotellah', 15, 3).alter()
      table.decimal('coscinodiscus', 15, 3).alter()
      table.decimal('chaetoceros', 15, 3).alter()
      table.decimal('melosira', 15, 3).alter()
      table.decimal('nitzschia', 15, 3).alter()
      table.decimal('skelotonema', 15, 3).alter()
      table.decimal('navicles', 15, 3).alter()
      table.decimal('thalassiosira', 15, 3).alter()
      table.decimal('ceratium', 15, 3).alter()
      table.decimal('peridinium', 15, 3).alter()
      table.decimal('chlamydomonas', 15, 3).alter()
      table.decimal('chlorella', 15, 3).alter()
      table.decimal('scenedesmus', 15, 3).alter()
      table.decimal('anabaena', 15, 3).alter()
      table.decimal('oscillatory', 15, 3).alter()
      table.decimal('nostoc', 15, 3).alter()
      table.decimal('anacystis', 15, 3).alter()
      table.decimal('protozoa', 15, 3).alter()
      table.decimal('euglena', 15, 3).alter()
      table.dropColumn('chart_image')
    })
  }
}

module.exports = PhytoplanktonAnalysisDetailSchema
