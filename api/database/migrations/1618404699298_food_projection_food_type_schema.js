'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FoodProjectionFoodTypeSchema extends Schema {
  up () {
    this.table('food_projection_food_types', (table) => {
      table.decimal('price_kg', 15, 2).nullable()
    })
  }

  down () {
    this.table('food_projection_food_types', (table) => {
      table.dropColumn('price_kg')
    })
  }
}

module.exports = FoodProjectionFoodTypeSchema
