'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PhytoplanktonAnalysisDetailSchema extends Schema {
  up () {
    this.table('phytoplankton_analysis_details', (table) => {
      table.string('recommendations')
    })
  }

  down () {
    this.table('phytoplankton_analysis_details', (table) => {
      table.dropColumn('recommendations')
    })
  }
}

module.exports = PhytoplanktonAnalysisDetailSchema
