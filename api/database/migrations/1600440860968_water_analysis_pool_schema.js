'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class WaterAnalysisPoolSchema extends Schema {
  up () {
    this.table('water_analysis_master', (table) => {
      table.dropColumn('zone_id')
    })
  }

  down () {
    this.table('water_analysis_master', (table) => {
      table.integer('zone_id')
    })
  }
}

module.exports = WaterAnalysisPoolSchema
