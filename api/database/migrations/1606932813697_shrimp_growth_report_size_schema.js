'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ShrimpGrowthReportSizeSchema extends Schema {
  up () {
    this.table('shrimp_growth_report_sizes', (table) => {
      table.decimal('growth', 15, 3).defaultTo(0)
    })
  }

  down () {
    this.table('shrimp_growth_report_sizes', (table) => {
      table.dropColumn('growth')
    })
  }
}

module.exports = ShrimpGrowthReportSizeSchema
