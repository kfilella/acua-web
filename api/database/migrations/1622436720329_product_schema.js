'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProductSchema extends Schema {
  up () {
    this.table('products', (table) => {
      table.string('sap_MATNR')
    })
  }

  down () {
    this.table('products', (table) => {
      table.dropColumn('sap_MATNR')
    })
  }
}

module.exports = ProductSchema
