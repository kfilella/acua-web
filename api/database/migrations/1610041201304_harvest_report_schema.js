'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class HarvestReportSchema extends Schema {
  up () {
    this.table('harvest_reports', (table) => {
      table.datetime('deleted_at')
    })
  }

  down () {
    this.table('harvest_reports', (table) => {
      table.dropColumn('deleted_at')
    })
  }
}

module.exports = HarvestReportSchema
