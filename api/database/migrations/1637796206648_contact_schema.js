'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContactSchema extends Schema {
  up () {
    this.table('contacts', (table) => {
      // alter table
      table.string('email').nullable().alter()
    })
  }

  down () {
    this.table('contacts', (table) => {
      // reverse alternations
      table.string('email').notNullable().alter()
    })
  }
}

module.exports = ContactSchema
