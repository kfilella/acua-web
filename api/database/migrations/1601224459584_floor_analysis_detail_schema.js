'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FloorAnalysisDetailSchema extends Schema {
  up () {
    this.table('floor_analysis_details', (table) => {
      table.string('recommendations')
    })
  }

  down () {
    this.table('floor_analysis_details', (table) => {
      table.dropColumn('recommendations')
    })
  }
}

module.exports = FloorAnalysisDetailSchema
