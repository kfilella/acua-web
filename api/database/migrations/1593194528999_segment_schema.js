'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SegmentSchema extends Schema {
  up () {
    this.table('segments', (table) => {
      table.integer('type').comment('0: Clientes, 1: Proveedores')
    })
  }

  down () {
    this.table('segments', (table) => {
      table.dropColumn('type')
    })
  }
}

module.exports = SegmentSchema
