'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LaboratoriesSchema extends Schema {
  up () {
    this.create('laboratories', (table) => {
      table.increments()
      table.string('name')
      table.timestamps()
    })
  }

  down () {
    this.drop('laboratories')
  }
}

module.exports = LaboratoriesSchema
