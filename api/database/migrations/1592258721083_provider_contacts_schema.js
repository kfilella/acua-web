'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProviderContactsSchema extends Schema {
  up () {
    this.create('provider_contacts', (table) => {
      table.increments()
      table.integer('provider_id').references('id').inTable('providers')
      table.string('last_name').notNullable()
      table.string('name').notNullable()
      table.string('phone').notNullable()
      table.string('email').notNullable()
      table.string('job')
      table.timestamps()
    })
  }

  down () {
    this.drop('provider_contacts')
  }
}

module.exports = ProviderContactsSchema
