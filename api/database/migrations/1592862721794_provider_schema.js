'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProviderSchema extends Schema {
  up () {
    this.table('providers', (table) => {
      table.string('product_categories')
    })
  }

  down () {
    this.table('providers', (table) => {
      table.dropColumn('product_categories')
    })
  }
}

module.exports = ProviderSchema
