'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ZoneSchema extends Schema {
  up () {
    this.create('zones', (table) => {
      table.increments()
      table.string('name')
      table.boolean('status')
      table.timestamps()
    })
  }

  down () {
    this.drop('zones')
  }
}

module.exports = ZoneSchema
