'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PoolSchema extends Schema {
  up () {
    this.table('pools', (table) => {
      table.datetime('deleted_at')
    })
  }

  down () {
    this.table('pools', (table) => {
      table.dropColumn('deleted_at')
    })
  }
}

module.exports = PoolSchema
