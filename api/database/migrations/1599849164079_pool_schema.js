'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PoolSchema extends Schema {
  up () {
    this.table('pools', (table) => {
      table.decimal('area').alter()
    })
  }

  down () {
    this.table('pools', (table) => {
      table.integer('area').alter()
    })
  }
}

module.exports = PoolSchema
