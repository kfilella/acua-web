'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FoodProjectionMasterSchema extends Schema {
  up () {
    this.table('food_projection_master', (table) => {
      table.datetime('deleted_at')
    })
  }

  down () {
    this.table('food_projection_master', (table) => {
      table.dropColumn('deleted_at')
    })
  }
}

module.exports = FoodProjectionMasterSchema
