'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class VisitingPlanningSchema extends Schema {
  up () {
    this.table('visiting_plannings', (table) => {
      table.string('reject_reason')
    })
  }

  down () {
    this.table('visiting_plannings', (table) => {
      table.dropColumn('reject_reason')
    })
  }
}

module.exports = VisitingPlanningSchema
