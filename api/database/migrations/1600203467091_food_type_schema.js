'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FoodTypeSchema extends Schema {
  up () {
    this.create('food_types', (table) => {
      table.increments()
      table.decimal('from_weight')
      table.decimal('to_weight')
      table.string('type')
      table.decimal('price_kg')
      table.timestamps()
    })
  }

  down () {
    this.drop('food_types')
  }
}

module.exports = FoodTypeSchema
