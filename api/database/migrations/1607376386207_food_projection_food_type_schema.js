'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')
const FoodProjectionFoodType = use('App/Models/FoodProjectionFoodType')

class FoodProjectionFoodTypeSchema extends Schema {
  async up () {
    await FoodProjectionFoodType.query().delete()
    this.table('food_projection_food_types', (table) => {
      table.dropForeign('food_type_id')
      table.integer('food_type_id').references('id').inTable('products').alter()
    })
  }

  down () {
    this.table('food_projection_food_types', (table) => {
      table.dropForeign('food_type_id')
      table.integer('food_type_id').references('id').inTable('food_types').alter()
    })
  }
}

module.exports = FoodProjectionFoodTypeSchema
