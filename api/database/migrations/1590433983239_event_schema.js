'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EventSchema extends Schema {
  up () {
    this.create('events', (table) => {
      table.increments()
      table.string('name').unique()
      table.string('description')
      table.timestamps()
      table.datetime('deleted_at')
    })
  }

  down () {
    this.drop('events')
  }
}

module.exports = EventSchema
