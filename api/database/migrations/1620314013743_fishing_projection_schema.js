'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FishingProjectionSchema extends Schema {
  up () {
    // this.drop('growth_reports')
    this.table('fishing_projections', (table) => {
      table.dropColumn('type')
      table.dropColumn('qty')

      table.date('report_date')
      table.integer('density')
      table.integer('dry_days')
      table.decimal('weight', 6, 2)
      table.integer('mp')
      table.integer('kg_acum')
      table.decimal('last_four', 6, 2)
      table.decimal('cur_kg_ha', 6, 2)
      table.decimal('sowing_weight', 6, 2).alter()
    })
  }

  down () {
    this.table('fishing_projections', (table) => {
      table.dropColumn('report_date')
      table.dropColumn('density')
      table.dropColumn('dry_days')
      table.dropColumn('weight')
      table.dropColumn('mp')
      table.dropColumn('kg_acum')
      table.dropColumn('last_four')
      table.dropColumn('cur_kg_ha')
      table.decimal('sowing_weight', 15, 3).alter()
    })
    
  }
}

module.exports = FishingProjectionSchema
