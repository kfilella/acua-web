'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ShrimpGrowthSchema extends Schema {
  up () {
    this.create('shrimp_growths', (table) => {
      table.increments()
      table.integer('shrimp_id').references('id').inTable('shrimps')
      table.integer('year')
      table.integer('week')
      table.integer('food_id')
      table.decimal('inc', 5, 2)
      table.timestamps()
    })
  }

  down () {
    this.drop('shrimp_growths')
  }
}

module.exports = ShrimpGrowthSchema
