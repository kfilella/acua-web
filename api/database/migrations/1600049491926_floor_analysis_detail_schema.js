'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FloorAnalysisDetailSchema extends Schema {
  up () {
    this.table('floor_analysis_details', (table) => {
      table.integer('ph').nullable().alter()
      table.integer('organic_material').nullable().alter()
    })
  }

  down () {
    this.table('floor_analysis_details', (table) => {
      table.integer('ph').notNullable().alter()
      table.integer('organic_material').notNullable().alter()
    })
  }
}

module.exports = FloorAnalysisDetailSchema
