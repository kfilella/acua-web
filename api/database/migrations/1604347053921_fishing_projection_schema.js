'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FishingProjectionSchema extends Schema {
  up () {
    this.create('fishing_projections', (table) => {
      table.increments()
      table.integer('pool_id').references('id').inTable('pools')
      table.integer('type')
      table.date('seedtime')
      table.integer('qty')
      table.decimal('sowing_weight', 15, 3)
      table.timestamps()
    })
  }

  down () {
    this.drop('fishing_projections')
  }
}

module.exports = FishingProjectionSchema
