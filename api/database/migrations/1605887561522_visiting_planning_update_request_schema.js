'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class VisitingPlanningUpdateRequestSchema extends Schema {
  up () {
    this.table('visiting_planning_update_requests', (table) => {
      table.integer('pre_event_type_id').defaultTo(0)
      table.integer('pre_customer_id').defaultTo(0)
      table.integer('pre_shrimp_id').defaultTo(0)
      table.date('pre_date').nullable()
      table.time('pre_time').nullable()

      table.integer('event_type_id').defaultTo(0)
      table.integer('customer_id').defaultTo(0)
      table.integer('shrimp_id').defaultTo(0)
      table.date('date').nullable()
      table.time('time').nullable()
    })
  }

  down () {
    this.table('visiting_planning_update_requests', (table) => {
      table.dropColumn('pre_event_type_id')
      table.dropColumn('pre_customer_id')
      table.dropColumn('pre_shrimp_id')
      table.dropColumn('pre_date')
      table.dropColumn('pre_time')

      table.dropColumn('event_type_id')
      table.dropColumn('customer_id')
      table.dropColumn('shrimp_id')
      table.dropColumn('date')
      table.dropColumn('time')
    })
  }
}

module.exports = VisitingPlanningUpdateRequestSchema
