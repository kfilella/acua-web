'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProviderProductPriceSchema extends Schema {
  up () {
    this.create('provider_product_prices', (table) => {
      table.increments()
      table.integer('product_id').references('id').inTable('products').notNullable()
      table.integer('provider_id').references('id').inTable('providers').notNullable()
      table.integer('price').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('provider_product_prices')
  }
}

module.exports = ProviderProductPriceSchema
