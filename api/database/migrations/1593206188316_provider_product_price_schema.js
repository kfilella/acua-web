'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProviderProductPriceSchema extends Schema {
  up () {
    this.table('provider_product_prices', (table) => {
      table.datetime('deleted_at')
    })
  }

  down () {
    this.table('provider_product_prices', (table) => {
    })
  }
}

module.exports = ProviderProductPriceSchema
