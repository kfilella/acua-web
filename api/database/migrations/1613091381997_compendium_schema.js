'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CompendiumSchema extends Schema {
  up () {
    this.create('compendiums', (table) => {
      table.increments()
      table.integer('shrimp_id').unsigned().references('id').inTable('shrimps')
      table.integer('pool_id').unsigned().references('id').inTable('pools')
      table.date('seed_date')
      table.decimal('cam', 15, 0)
      table.decimal('sowing_size', 15, 2)
      table.integer('product_id').unsigned().references('id').inTable('products')
      table.timestamps()
      table.datetime('deleted_at')
    })
  }

  down () {
    this.drop('compendiums')
  }
}

module.exports = CompendiumSchema
