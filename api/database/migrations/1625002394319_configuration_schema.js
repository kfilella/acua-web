'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ConfigurationSchema extends Schema {
  up () {
    this.create('configurations', (table) => {
      table.increments()
      table.integer('mts_range_marking')
      table.timestamps()
    })
  }

  down () {
    this.drop('configurations')
  }
}

module.exports = ConfigurationSchema
