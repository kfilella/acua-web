'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class HarvestReportSchema extends Schema {
  up () {
    this.table('harvest_reports', (table) => {
      table.integer('start_food_id')
    })
  }

  down () {
    this.table('harvest_reports', (table) => {
      table.dropColumn('start_food_id')
    })
  }
}

module.exports = HarvestReportSchema
