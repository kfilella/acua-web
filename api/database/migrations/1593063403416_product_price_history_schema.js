'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProductPriceHistorySchema extends Schema {
  up () {
    this.create('product_price_histories', (table) => {
      table.increments()
      table.integer('product_id').references('id').inTable('products').notNullable()
      table.integer('price').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('product_price_histories')
  }
}

module.exports = ProductPriceHistorySchema
