'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ZoneCustomerSchema extends Schema {
  up () {
    this.create('customer_zone', (table) => {
      table.increments()
      table.integer('zone_id')
      table.integer('customer_id')
      table.timestamps()
    })
  }

  down () {
    this.drop('customer_zone')
  }
}

module.exports = ZoneCustomerSchema
