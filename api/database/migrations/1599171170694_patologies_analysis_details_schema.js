'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PatologiesAnalysisDetailsSchema extends Schema {
  up () {
    this.table('patologies_analysis_details', (table) => {
      table.integer('pool_intestine_id').unsigned().references('id').inTable('pools').notNullable()
      table.integer('epistylis')
    })
  }

  down () {
    this.table('patologies_analysis_details', (table) => {
      // reverse alternations
    })
  }
}

module.exports = PatologiesAnalysisDetailsSchema
