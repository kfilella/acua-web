'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FoodTypeSchema extends Schema {
  up () {
    this.table('food_types', (table) => {      
      table.string('code')
    })
  }

  down () {
    this.table('food_types', (table) => {
      table.dropColumn('code')
    })
  }
}

module.exports = FoodTypeSchema
