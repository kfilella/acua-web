'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FoodProjectionFoodTypeSchema extends Schema {
  up () {
    this.create('food_projection_food_types', (table) => {
      table.increments()
      table.integer('food_projection_id').references('id').inTable('food_projection_master')
      table.integer('food_type_id').references('id').inTable('food_types')
      table.decimal('from_weight', 15, 3)
      table.decimal('to_weight', 15, 3)
      table.timestamps()
    })
  }

  down () {
    this.drop('food_projection_food_types')
  }
}

module.exports = FoodProjectionFoodTypeSchema
