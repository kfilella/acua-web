'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FormatCategoryVariableSchema extends Schema {
  up () {
    this.create('format_category_variables', (table) => {
      table.increments()
      table.integer('format_category_id').references('id').inTable('format_categories').notNullable()
      table.string('name').notNullable()
      table.string('description').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('format_category_variables')
  }
}

module.exports = FormatCategoryVariableSchema
