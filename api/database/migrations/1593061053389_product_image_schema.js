'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProductImageSchema extends Schema {
  up () {
    this.create('product_images', (table) => {
      table.increments()
      table.integer('product_id').references('id').inTable('products').notNullable()
      table.string('filename')
      table.timestamps()
    })
  }

  down () {
    this.drop('product_images')
  }
}

module.exports = ProductImageSchema
