'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FishingProjectionSchema extends Schema {
  up () {
    this.table('fishing_projections', (table) => {
      table.datetime('deleted_at')
    })
  }

  down () {
    this.table('fishing_projections', (table) => {
      table.dropColumn('deleted_at')
    })
  }
}

module.exports = FishingProjectionSchema
