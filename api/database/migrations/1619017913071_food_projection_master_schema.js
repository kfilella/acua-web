'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FoodProjectionMasterSchema extends Schema {
  up () {
    this.table('food_projection_master', (table) => {
      table.decimal('growth_rate', 15, 0).alter()
    })
  }

  down () {
    this.table('food_projection_master', (table) => {
      table.decimal('growth_rate').alter()
    })
  }
}

module.exports = FoodProjectionMasterSchema
