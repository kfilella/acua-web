'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PathologySchema extends Schema {
  up () {
    this.create('pathologies', (table) => {
      table.increments()
      table.integer('pool_id')
      table.string('pools')
      table.string('gills')
      table.string('hepatopancreas')
      table.string('intestine')
      table.integer('tubules')
      table.string('observation')
      table.timestamps()
    })
  }

  down () {
    this.drop('pathologies')
  }
}

module.exports = PathologySchema
