'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CustomerSchema extends Schema {
  up () {
    this.table('customers', (table) => {
      table.integer('type').notNullable().alter() // modificar a entero
      table.integer('document_type').notNullable().alter() // modificar a entero
      table.string('deliver_address')
    })
  }

  down () {
    this.table('customers', (table) => {
      table.string('type').notNullable().alter() // modificar a string otra vez
      table.string('document_type').notNullable().alter() // modificar a string otra vez
      table.dropColumn('deliver_address')
    })
  }
}

module.exports = CustomerSchema
