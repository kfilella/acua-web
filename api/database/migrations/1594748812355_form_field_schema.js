'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FormFieldSchema extends Schema {
  up () {
    this.create('form_fields', (table) => {
      table.increments()
      table.string('name').notNullable()
      table.boolean('required').notNullable()
      table.integer('form_id').references('id').inTable('forms').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('form_fields')
  }
}

module.exports = FormFieldSchema
