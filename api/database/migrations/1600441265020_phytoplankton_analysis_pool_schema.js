'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PhytoplanktonAnalysisPoolSchema extends Schema {
  up () {
    this.table('phytoplankton_analysis_pools', (table) => {
      table.dropColumn('zone_id')
    })
  }

  down () {
    this.table('phytoplankton_analysis_pools', (table) => {
      table.integer('zone_id')
    })
  }
}

module.exports = PhytoplanktonAnalysisPoolSchema
