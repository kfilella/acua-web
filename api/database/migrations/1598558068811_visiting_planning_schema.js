'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class VisitingPlanningSchema extends Schema {
  up () {
    this.create('visiting_plannings', (table) => {
      table.increments()
      table.integer('event_type_id').notNullable()
      table.integer('customer_id').notNullable()
      table.integer('shrimp_id').notNullable()
      table.integer('technical_advisor_id').notNullable()
      table.string('title').notNullable().notNullable()
      table.string('details')
      table.date('date').notNullable()
      table.time('time').notNullable()
      table.integer('duration')
      table.string('icon')
      table.integer('days')
      table.integer('status').comment('0: En espera, 1: Aprobado, 2: Rechazado, 3: Reenviado por solicitud de edición').defaultTo(0)
      table.timestamps()
    })
  }

  down () {
    this.drop('visiting_plannings')
  }
}

module.exports = VisitingPlanningSchema
