'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CostBenefitProjectionDetailSchema extends Schema {
  up () {
    this.table('cost_benefit_projection_details', (table) => {
      table.integer('food_id')
    })
  }

  down () {
    this.table('cost_benefit_projection_details', (table) => {
      table.dropColumn('food_id')
    })
  }
}

module.exports = CostBenefitProjectionDetailSchema
