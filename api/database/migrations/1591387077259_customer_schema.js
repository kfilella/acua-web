'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CustomerSchema extends Schema {
  up () {
    this.create('customers', (table) => {
      table.increments()
      table.string('type').notNullable() //Persona natural o jurídica
      table.boolean('foreign_company').defaultTo(false)
      table.string('legal_name').notNullable()
      table.string('comercial_name')
      table.string('document_type').notNullable() // Cédula, RUC o pasaporte
      table.string('document_number')
      table.string('electronic_documents_email')
      table.integer('country_id').references('id').inTable('countries').notNullable()
      table.integer('state_id').references('id').inTable('states')
      table.integer('city_id').references('id').inTable('cities')
      table.text('billing_address')
      table.string('phone')
      table.string('tags')
      table.timestamps()
      table.datetime('deleted_at')

    })
  }

  down () {
    this.drop('customers')
  }
}

module.exports = CustomerSchema
