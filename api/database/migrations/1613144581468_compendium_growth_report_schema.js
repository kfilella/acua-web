'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CompendiumGrowthReportSchema extends Schema {
  up () {
    this.create('compendium_growth_reports', (table) => {
      table.increments()
      table.integer('compendium_id').unsigned().references('id').inTable('compendiums')
      table.date('date')
      table.decimal('size', 15, 3)
      table.decimal('mp', 15, 3)
      table.timestamps()
    })
  }

  down () {
    this.drop('compendium_growth_reports')
  }
}

module.exports = CompendiumGrowthReportSchema
