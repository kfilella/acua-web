'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ShrimpPriceSchema extends Schema {
  up () {
    this.table('shrimp_prices', (table) => {
      table.integer('range_id')
    })
  }

  down () {
    this.table('shrimp_prices', (table) => {
      table.dropColumn('range_id')
    })
  }
}

module.exports = ShrimpPriceSchema
