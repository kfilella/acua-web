'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CompendiumPoolSchema extends Schema {
  up () {
    this.table('compendium_pools', (table) => {
      table.datetime('deleted_at')
    })
  }

  down () {
    this.table('compendium_pools', (table) => {
      table.dropColumn('deleted_at')
    })
  }
}

module.exports = CompendiumPoolSchema
