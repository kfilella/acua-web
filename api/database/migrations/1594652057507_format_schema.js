'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FormatSchema extends Schema {
  up () {
    this.create('formats', (table) => {
      table.increments()
      table.integer('format_category_id').references('id').inTable('format_categories').notNullable()
      table.string('name').notNullable()
      table.string('title').notNullable()
      table.boolean('status')
      table.text('content').notNullable()
      table.string('string_date')
      table.string('footer')
      table.text('config')
      table.string('type')
      table.timestamps()
    })
  }

  down () {
    this.drop('formats')
  }
}

module.exports = FormatSchema
