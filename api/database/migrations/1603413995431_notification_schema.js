'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class NotificationSchema extends Schema {
  up () {
    this.table('notifications', (table) => {
      table.text('params')
    })
  }

  down () {
    this.table('notifications', (table) => {
      table.dropColumn('params')
    })
  }
}

module.exports = NotificationSchema
