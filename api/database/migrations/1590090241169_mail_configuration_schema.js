'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MailConfigurationSchema extends Schema {
  up () {
    this.table('mail_configurations', (table) => {
      table.integer('type').comment('0: SMTP, 1 MailGun')
      table.string('mailgun_domain')
      table.string('mailgun_apikey')
    })
  }

  down () {
    this.table('mail_configurations', (table) => {
      table.dropColumn('type')
      table.dropColumn('mailgun_domain')
      table.dropColumn('mailgun_apikey')
    })
  }
}

module.exports = MailConfigurationSchema
