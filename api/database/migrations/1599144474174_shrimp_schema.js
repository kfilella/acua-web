'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ShrimpSchema extends Schema {
  up () {
    this.table('shrimps', (table) => {
      table.string('name').notNullable().defaultTo('Nombre')
    })
  }

  down () {
    this.table('shrimps', (table) => {
      table.dropColumn('name')
    })
  }
}

module.exports = ShrimpSchema
