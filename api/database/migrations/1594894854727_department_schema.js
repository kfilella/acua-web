'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DepartmentSchema extends Schema {
  up () {
    this.table('departments', (table) => {
      table.integer('area_id').nullable().alter()
    })
  }

  down () {
    this.table('departments', (table) => {
      table.integer('area_id').notNullable().alter()
    })
  }
}

module.exports = DepartmentSchema
