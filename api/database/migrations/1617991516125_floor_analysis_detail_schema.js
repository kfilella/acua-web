'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FloorAnalysisDetailSchema extends Schema {
  up () {
    this.table('floor_analysis_details', (table) => {
      table.decimal('redox_potential').alter()
      table.decimal('sulfides', 15 , 2).alter()
      table.decimal('iron', 15, 2).alter()
    })
  }

  down () {
    this.table('floor_analysis_details', (table) => {
      table.integer('redox_potential').alter()
      table.decimal('sulfides', 15 , 1).alter()
      table.decimal('iron', 15, 1).alter()
    })
  }
}

module.exports = FloorAnalysisDetailSchema
