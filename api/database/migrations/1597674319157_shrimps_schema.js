'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ShrimpsSchema extends Schema {
  up () {
    this.table('shrimps', (table) => {
      table.datetime('deleted_at')
    })
  }

  down () {
    this.table('shrimps', (table) => {
      table.dropColumn('deleted_at')
    })
  }
}

module.exports = ShrimpsSchema
