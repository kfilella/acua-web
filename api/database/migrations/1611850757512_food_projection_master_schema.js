'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FoodProjectionMasterSchema extends Schema {
  up () {
    this.table('food_projection_master', (table) => {
      table.text('chart_image')
    })
  }

  down () {
    this.table('food_projection_master', (table) => {
      table.dropColumn('chart_image')
    })
  }
}

module.exports = FoodProjectionMasterSchema
