'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProductSchema extends Schema {
  up () {
    this.create('products', (table) => {
      table.increments()
      table.string('name').notNullable()
      table.integer('product_type')
      table.integer('product_category_id').references('id')
      table.string('internal_reference')
      table.string('barcode')
      table.float('last_price_purchase')
      table.integer('measure_unit_id').references('id').inTable('measure_units')
      table.string('especifications')
      table.integer('color_id').references('id').inTable('colors')
      table.timestamps()
      table.datetime('deleted_at')
    })
  }

  down () {
    this.drop('products')
  }
}

module.exports = ProductSchema
