'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class VisitingPlanningUpdateRequestSchema extends Schema {
  up () {
    this.create('visiting_planning_update_requests', (table) => {
      table.increments()
      table.integer('visiting_planning_id').references('id').inTable('visiting_plannings')
      table.integer('type').comment('0 edición, 1 eliminación')
      table.integer('status').comment('0 pendiente, 1 aprobado, 2 rechazado').defaultTo(0)
      table.string('reason')
      table.timestamps()
    })
  }

  down () {
    this.drop('visiting_planning_update_requests')
  }
}

module.exports = VisitingPlanningUpdateRequestSchema
