'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PatologiesAnalysisDetailSchema extends Schema {
  up () {
    this.table('patologies_analysis_details', (table) => {
      table.integer('dirty').alter()
      table.integer('necrosis').alter()
      table.integer('algae').alter()
      table.integer('epistylis').alter()
      table.integer('vortex').alter()
      table.integer('zoothamnium').alter()
      table.integer('acineta').alter()
      table.integer('ciliated_apostome').alter()
      table.integer('chromatophores').alter()

      table.integer('lipids').alter()
      table.integer('thickening').alter()
      table.integer('deformity').alter()
      table.integer('constriction').alter()

      table.integer('baculovirus').alter()
      table.integer('gregarious').alter()
      table.integer('nemamotods').alter()
      table.integer('intestine_algae').alter()
      table.integer('debris').alter()
      table.integer('balanced').alter()
      table.integer('crustacean_remains').alter()
    })
  }

  down () {
    this.table('patologies_analysis_details', (table) => {
      table.decimal('dirty').alter()
      table.decimal('necrosis').alter()
      table.decimal('algae').alter()
      table.decimal('epistylis').alter()
      table.decimal('vortex').alter()
      table.decimal('zoothamnium').alter()
      table.decimal('acineta').alter()
      table.decimal('ciliated_apostome').alter()
      table.decimal('chromatophores').alter()

      table.decimal('lipids').alter()
      table.decimal('thickening').alter()
      table.decimal('deformity').alter()
      table.decimal('constriction').alter()

      table.decimal('baculovirus').alter()
      table.decimal('gregarious').alter()
      table.decimal('nemamotods').alter()
      table.decimal('intestine_algae').alter()
      table.decimal('debris').alter()
      table.decimal('balanced').alter()
      table.decimal('crustacean_remains').alter()
    })
  }
}

module.exports = PatologiesAnalysisDetailSchema
