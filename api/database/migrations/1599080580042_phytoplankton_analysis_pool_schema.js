'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PhytoplanktonAnalysisPoolSchema extends Schema {
  up () {
    this.create('phytoplankton_analysis_pools', (table) => {
      table.increments()
      table.integer('shrimp_id').unsigned().references('id').inTable('shrimps').notNullable()
      table.date('analysis_date').notNullable()
      table.integer('technical_advisor_id').unsigned().references('id').inTable('users').notNullable()
      table.integer('zone_id').unsigned().references('id').inTable('zones').notNullable()
      table.integer('applicant_id').unsigned().defaultTo(0).notNullable()// Si es cero el solicitante es el mismo cliente, en caso contrario es algún contacto
      table.date('delivery_date').notNullable()
      table.date('sampling_date').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('phytoplankton_analysis_pools')
  }
}

module.exports = PhytoplanktonAnalysisPoolSchema
