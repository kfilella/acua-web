'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PatologiesAnalysisDetailSchema extends Schema {
  up () {
    this.table('patologies_analysis_details', (table) => {
      table.datetime('deleted_at')
    })
  }

  down () {
    this.table('patologies_analysis_details', (table) => {
      table.dropColumn('deleted_at')
    })
  }
}

module.exports = PatologiesAnalysisDetailSchema
