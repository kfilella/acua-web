'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FoodProjectionSchema extends Schema {
  up () {
    this.table('food_projection_master', (table) => {
      table.decimal('pool_area', 15, 2)
    })
  }

  down () {
    this.table('food_projection_master', (table) => {
      table.dropColumn('pool_area')
    })
  }
}

module.exports = FoodProjectionSchema
