'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FloorAnalysisDetailSchema extends Schema {
  up () {
    this.table('floor_analysis_details', (table) => {
      table.text('recommendations').alter()
    })
  }

  down () {
    this.table('floor_analysis_details', (table) => {
      table.string('recommendations').alter()
    })
  }
}

module.exports = FloorAnalysisDetailSchema
