'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PathologySchema extends Schema {
  up () {
    this.table('pathologies', (table) => {
      table.datetime('deleted_at')
    })
  }

  down () {
    this.table('pathologies', (table) => {
      table.dropColumn('deleted_at')
    })
  }
}

module.exports = PathologySchema
