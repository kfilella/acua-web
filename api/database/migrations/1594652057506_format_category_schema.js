'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FormatCategorySchema extends Schema {
  up () {
    this.create('format_categories', (table) => {
      table.increments()
      table.string('name')
      table.timestamps()
    })
  }

  down () {
    this.drop('format_categories')
  }
}

module.exports = FormatCategorySchema
