'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CustomerSchema extends Schema {
  up () {
    this.table('customers', (table) => {
      table.integer('user_id').unsigned().defaultTo(0)
    })
  }

  down () {
    this.table('customers', (table) => {
      table.dropColumn('user_id')
    })
  }
}

module.exports = CustomerSchema
