'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FoodProjectionMasterSchema extends Schema {
  up () {
    this.table('food_projection_master', (table) => {
      table.decimal('feeding_rate')
    })
  }

  down () {
    this.table('food_projection_master', (table) => {
      table.dropColumn('feeding_rate')
    })
  }
}

module.exports = FoodProjectionMasterSchema
