'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TechnicalAdvisorSchema extends Schema {
  up () {
    this.create('technical_advisors', (table) => {
      table.increments()
      table.string('code')
      table.string('full_name').notNullable()
      table.string('phone')
      table.string('company_mail')
      table.string('personal_mail')
      table.string('zone')
      table.boolean('status')
      table.timestamps()
    })
  }

  down () {
    this.drop('technical_advisors')
  }
}

module.exports = TechnicalAdvisorSchema
