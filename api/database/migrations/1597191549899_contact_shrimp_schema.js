'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContactShrimpSchema extends Schema {
  up () {
    this.create('contact_shrimps', (table) => {
      table.increments()
      table.string('shrimp_id')
      table.string('contact_id')
      table.timestamps()
    })
  }

  down () {
    this.drop('contact_shrimps')
  }
}

module.exports = ContactShrimpSchema
