'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class VisitingPlanningSchema extends Schema {
  up () {
    this.table('visiting_plannings', (table) => {
      table.datetime('deleted_at')
    })
  }

  down () {
    this.table('visiting_plannings', (table) => {
      table.dropColumn('deleted_at')
    })
  }
}

module.exports = VisitingPlanningSchema
