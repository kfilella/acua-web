'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PatologiesAnalysisPoolSchema extends Schema {
  up () {
    this.table('patologies_analysis_pools', (table) => {
      table.dropColumn('zone_id')
    })
  }

  down () {
    this.table('patologies_analysis_pools', (table) => {
      table.integer('zone_id')
    })
  }
}

module.exports = PatologiesAnalysisPoolSchema
