'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PathologiesSchema extends Schema {
  up () {
    this.table('pathologies', (table) => {
      table.string('shrimp_id')
    })
  }

  down () {
    this.table('pathologies', (table) => {
      table.dropColumn('shrimp_id')
    })
  }
}

module.exports = PathologiesSchema
