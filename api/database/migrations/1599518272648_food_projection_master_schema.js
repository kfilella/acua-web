'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FoodProjectionMasterSchema extends Schema {
  up () {
    this.table('food_projection_master', (table) => {
      table.decimal('growth_rate')
    })
  }

  down () {
    this.table('food_projection_master', (table) => {
      table.dropColumn('growth_rate')
    })
  }
}

module.exports = FoodProjectionMasterSchema
