'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FoodProjectionDetailSchema extends Schema {
  up () {
    this.table('food_projection_detail', (table) => {
      table.string('food_type').alter()
    })
  }

  down () {
    this.table('food_projection_detail', (table) => {
      // table.integer('food_type').alter()
    })
  }
}

module.exports = FoodProjectionDetailSchema
