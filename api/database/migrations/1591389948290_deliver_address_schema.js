'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DeliverAddressSchema extends Schema {
  up () {
    this.create('deliver_addresses', (table) => {
      table.increments()
      table.integer('customer_id').references('id').inTable('customers')
      table.text('address')
      table.timestamps()
    })
  }

  down () {
    this.drop('deliver_addresses')
  }
}

module.exports = DeliverAddressSchema
