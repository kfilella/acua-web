'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PermissionSchema extends Schema {
  up () {
    this.create('permissions', (table) => {
      table.increments()
      table.string('name').unique().notNullable()
      table.string('track').unique().notNullable()
      table.string('description').nullable()
      table.timestamps()
      table.datetime('deleted_at')
    })
  }

  down () {
    this.drop('permissions')
  }
}

module.exports = PermissionSchema
