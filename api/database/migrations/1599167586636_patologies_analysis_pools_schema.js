'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PatologiesAnalysisPoolsSchema extends Schema {
  up () {
    this.table('patologies_analysis_pools', (table) => {
      table.integer('applicant_id').unsigned().defaultTo(0).notNullable()// Si es cero el solicitante es el mismo cliente, en caso contrario es algún contacto
      table.date('delivery_date').notNullable()
      table.integer('zone_id').unsigned().references('id').inTable('zones').notNullable()
    })
  }

  down () {
    this.table('patologies_analysis_pools', (table) => {
      // reverse alternations
    })
  }
}

module.exports = PatologiesAnalysisPoolsSchema
