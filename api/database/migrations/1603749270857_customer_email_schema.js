'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CustomerEmailSchema extends Schema {
  up () {
    this.create('customer_emails', (table) => {
      table.increments()
      table.integer('customer_id')
      table.string('email')
      table.timestamps()
    })
  }

  down () {
    this.drop('customer_emails')
  }
}

module.exports = CustomerEmailSchema
