'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserSchema extends Schema {
  up () {
    this.table('users', (table) => {
      table.string('reset_password_token')
    })
  }

  down () {
    this.table('users', (table) => {
      table.dropColumn('reset_password_token')
    })
  }
}

module.exports = UserSchema
