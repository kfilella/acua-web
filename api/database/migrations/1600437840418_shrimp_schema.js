'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ShrimpSchema extends Schema {
  up () {
    this.table('shrimps', (table) => {
      table.integer('zone_id').defaultTo(1)
    })
  }

  down () {
    this.table('shrimps', (table) => {
      table.dropColumn('zone_id')
    })
  }
}

module.exports = ShrimpSchema
