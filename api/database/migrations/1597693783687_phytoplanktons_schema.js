'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PhytoplanktonsSchema extends Schema {
  up () {
    this.table('phytoplanktons', (table) => {
      table.integer('pool_id').alter()
    })
  }

  down () {
    this.table('phytoplanktons', (table) => {
      // reverse alternations
      // table.dropColumn('pool_id')
    })
  }
}

module.exports = PhytoplanktonsSchema
