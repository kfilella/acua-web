'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FloorAnalysisPoolSchema extends Schema {
  up () {
    this.table('floor_analysis_master', (table) => {
      table.text('observations')
      table.text('recommendations')
    })
  }

  down () {
    this.table('floor_analysis_master', (table) => {
      table.dropColumn('observations')
      table.dropColumn('recommendations')
    })
  }
}

module.exports = FloorAnalysisPoolSchema
