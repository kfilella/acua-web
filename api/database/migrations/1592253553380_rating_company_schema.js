'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RatingCompanySchema extends Schema {
  up () {
    this.create('rating_companies', (table) => {
      table.increments()
      table.string('name').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('rating_companies')
  }
}

module.exports = RatingCompanySchema
