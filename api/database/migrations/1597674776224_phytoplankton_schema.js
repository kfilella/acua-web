'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PhytoplanktonSchema extends Schema {
  up () {
    this.create('phytoplanktons', (table) => {
      table.increments()
      table.integer('pool_id').unsigned().references('id').inTable('pools').notNullable()
      table.integer('diatomes')
      table.integer('dinoflagelados')
      table.integer('chlorophytes')
      table.integer('cyanophites')
      table.integer('others')
      table.string('observation')
      table.timestamps()
    })
  }

  down () {
    this.drop('phytoplanktons')
  }
}

module.exports = PhytoplanktonSchema
