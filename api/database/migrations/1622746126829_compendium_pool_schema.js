'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CompendiumPoolSchema extends Schema {
  up () {
    this.create('compendium_pools', (table) => {
      table.increments()
      table.integer('compendium_id').references('id').inTable('compendiums')
      table.integer('pool_id').references('id').inTable('pools')
      table.date('seed_date')
      table.decimal('sowing_size', 5, 2)
      table.integer('product_id').references('id').inTable('products')
      table.integer('mp')
      table.integer('kg_alim')
      table.integer('density')
      table.timestamps()
    })
  }

  down () {
    this.drop('compendium_pools')
  }
}

module.exports = CompendiumPoolSchema
