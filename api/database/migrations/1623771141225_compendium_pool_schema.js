'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CompendiumPoolSchema extends Schema {
  up () {
    this.table('compendium_pools', (table) => {
      table.string('products')
    })
  }

  down () {
    this.table('compendium_pools', (table) => {
      table.dropColumn('products')
    })
  }
}

module.exports = CompendiumPoolSchema
