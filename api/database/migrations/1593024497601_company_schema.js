'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CompanySchema extends Schema {
  up () {
    this.create('companies', (table) => {
      table.increments()
      table.string('legal_name')
      table.string('comercial_name')
      table.timestamps()
      table.datetime('deleted_at')
    })
  }

  down () {
    this.drop('companies')
  }
}

module.exports = CompanySchema
