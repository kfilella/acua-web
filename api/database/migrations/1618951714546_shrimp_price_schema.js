'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ShrimpPriceSchema extends Schema {
  up () {
    this.table('shrimp_prices', (table) => {
      table.decimal('average_size', 15, 2)
    })
  }

  down () {
    this.table('shrimp_prices', (table) => {
      table.dropColumn('average_size')
    })
  }
}

module.exports = ShrimpPriceSchema
