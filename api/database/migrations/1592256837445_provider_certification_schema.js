'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProviderCertificationSchema extends Schema {
  up () {
    this.create('provider_certifications', (table) => {
      table.increments()
      table.integer('provider_id').references('id').inTable('providers')
      table.string('description').notNullable()
      table.string('filename')
      table.timestamps()
    })
  }

  down () {
    this.drop('provider_certifications')
  }
}

module.exports = ProviderCertificationSchema
