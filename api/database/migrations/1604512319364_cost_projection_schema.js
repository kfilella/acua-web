'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CostProjectionSchema extends Schema {
  up () {
    this.create('cost_projections', (table) => {
      table.increments()
      table.integer('fishing_projection_id').references('id').inTable('fishing_projections')
      table.decimal('pounds_raleo', 15, 3)
      table.decimal('weight_raleo', 15, 3)
      table.decimal('cost_juv', 15, 3)
      table.decimal('kg_ha_food_day', 15, 3)
      table.decimal('kg_acum', 15, 3)
      table.decimal('price_kilo_food', 15, 3)
      table.integer('dry_days')
      table.decimal('cost_ha_days', 15, 3)
      table.timestamps()
    })
  }

  down () {
    this.drop('cost_projections')
  }
}

module.exports = CostProjectionSchema
