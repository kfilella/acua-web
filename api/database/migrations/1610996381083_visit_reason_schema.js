'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class VisitReasonSchema extends Schema {
  up () {
    this.table('visit_reasons', (table) => {
      table.boolean('require_approval').defaultTo(true)
    })
  }

  down () {
    this.table('visit_reasons', (table) => {
      table.dropColumn('require_approval')
    })
  }
}

module.exports = VisitReasonSchema
