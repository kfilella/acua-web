'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ShrimpProjectionSchema extends Schema {
  up () {
    this.create('shrimp_projections', (table) => {
      table.increments()
      table.integer('fishing_projection_id').references('id').inTable('fishing_projections')
      table.decimal('harvest_weight', 15, 3)
      table.decimal('percent_sup', 5, 2)
      table.timestamps()
    })
  }

  down () {
    this.drop('shrimp_projections')
  }
}

module.exports = ShrimpProjectionSchema
