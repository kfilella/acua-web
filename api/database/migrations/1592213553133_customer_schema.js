'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CustomerSchema extends Schema {
  up () {
    this.table('customers', (table) => {
      table.string('code')
      table.string('zone')
      table.string('sector')
      table.integer('category_id').references('id').inTable('categories')
    })
  }

  down () {
    this.table('customers', (table) => {
      table.dropColumn('code')
      table.dropColumn('zone')
      table.dropColumn('sector')
      table.dropColumn('category_id')
    })
  }
}

module.exports = CustomerSchema
