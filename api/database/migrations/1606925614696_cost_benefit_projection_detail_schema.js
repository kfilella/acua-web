'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CostBenefitProjectionDetailSchema extends Schema {
  up () {
    this.table('cost_benefit_projection_details', (table) => {
      table.decimal('initial_biomass', 15, 3)
    })
  }

  down () {
    this.table('cost_benefit_projection_details', (table) => {
      table.dropColumn('initial_biomass')
    })
  }
}

module.exports = CostBenefitProjectionDetailSchema
