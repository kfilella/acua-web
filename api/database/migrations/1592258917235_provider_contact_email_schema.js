'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProviderContactEmailSchema extends Schema {
  up () {
    this.create('provider_contact_emails', (table) => {
      table.increments()
      table.integer('provider_contact_id')
      table.foreign('provider_contact_id').references('provider_contacts.id').onDelete('cascade')
      table.string('email').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('provider_contact_emails')
  }
}

module.exports = ProviderContactEmailSchema
