'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PhytoplanktonAnalysisDetailSchema extends Schema {
  up () {
    this.table('phytoplankton_analysis_details', (table) => {
      table.decimal('pcyclotellah', 15, 3).alter()
      table.decimal('coscinodiscus', 15, 3).alter()
      table.decimal('chaetoceros', 15, 3).alter()
      table.decimal('melosira', 15, 3).alter()
      table.decimal('nitzschia', 15, 3).alter()
      table.decimal('skelotonema', 15, 3).alter()
      table.decimal('navicles', 15, 3).alter()
      table.decimal('thalassiosira', 15, 3).alter()
      table.decimal('ceratium', 15, 3).alter()
      table.decimal('peridinium', 15, 3).alter()
      table.decimal('chlamydomonas', 15, 3).alter()
      table.decimal('scenedesmus', 15, 3).alter()
      table.decimal('chlorella', 15, 3).alter()
      table.decimal('anabaena', 15, 3).alter()
      table.decimal('oscillatory', 15, 3).alter()
      table.decimal('nostoc', 15, 3).alter()
      table.decimal('anacystis', 15, 3).alter()
      table.decimal('protozoa', 15, 3).alter()
      table.decimal('euglena', 15, 3).alter()
    })
  }

  down () {
    this.table('phytoplankton_analysis_details', (table) => {
      table.decimal('pcyclotellah').alter()
      table.decimal('coscinodiscus').alter()
      table.decimal('chaetoceros').alter()
      table.decimal('melosira').alter()
      table.decimal('nitzschia').alter()
      table.decimal('skelotonema').alter()
      table.decimal('navicles').alter()
      table.decimal('thalassiosira').alter()
      table.decimal('ceratium').alter()
      table.decimal('peridinium').alter()
      table.decimal('chlamydomonas').alter()
      table.decimal('scenedesmus').alter()
      table.decimal('chlorella').alter()
      table.decimal('anabaena').alter()
      table.decimal('oscillatory').alter()
      table.decimal('nostoc').alter()
      table.decimal('anacystis').alter()
      table.decimal('protozoa').alter()
      table.decimal('euglena').alter()
    })
  }
}

module.exports = PhytoplanktonAnalysisDetailSchema
