'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CostBenefitProjectionSchema extends Schema {
  up () {
    this.create('cost_benefit_projections', (table) => {
      table.increments()
      table.integer('shrimp_id').references('id').inTable('shrimps')
      table.integer('technical_advisor_id')
      table.timestamps()
    })
  }

  down () {
    this.drop('cost_benefit_projections')
  }
}

module.exports = CostBenefitProjectionSchema
