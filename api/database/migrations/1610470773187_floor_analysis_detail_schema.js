'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FloorAnalysisDetailSchema extends Schema {
  up () {
    this.table('floor_analysis_details', (table) => {
      table.decimal('organic_material', 15, 3).alter()
    })
  }

  down () {
    this.table('floor_analysis_details', (table) => {
      table.integer('organic_material').alter()
    })
  }
}

module.exports = FloorAnalysisDetailSchema
