'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RatingTypeSchema extends Schema {
  up () {
    this.create('rating_types', (table) => {
      table.increments()
      table.string('name').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('rating_types')
  }
}

module.exports = RatingTypeSchema
