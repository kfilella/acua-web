'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CostBenefitProjectionDetailSchema extends Schema {
  up () {
    this.table('cost_benefit_projection_details', (table) => {
      table.decimal('density', 15, 0).alter()
      table.decimal('survival', 15, 0).alter()
      table.decimal('size_grs', 15, 2).alter()
      table.decimal('size_planting', 15, 2).alter()
      table.decimal('pounds_thinning', 15, 0).alter()
      table.decimal('cost_per_thounsand', 15, 2).alter()
    })
  }

  down () {
    this.table('cost_benefit_projection_details', (table) => {
      table.decimal('density', 15, 3).alter()
      table.decimal('survival', 15, 3).alter()
      table.decimal('size_grs', 15, 3).alter()
      table.decimal('size_planting', 15, 3).alter()
      table.decimal('pounds_thinning', 15, 3).alter()
      table.decimal('cost_per_thounsand', 15, 3).alter()
    })
  }
}

module.exports = CostBenefitProjectionDetailSchema
