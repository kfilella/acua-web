'use strict'

const { test, trait } = use('Test/Suite')('Format')

const User = use('App/Models/User')
trait('Test/ApiClient')
trait('Auth/Client')

test('Listar compendios', async ({ client, assert }) => {
  const user = await User.query().where('email', 'admin@wondrix.com').first()

  const response = await client
    .get('api/areas')
    .loginVia(user)
    .end()
  response.assertStatus(200)
})
