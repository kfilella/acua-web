'use strict'

const { test, trait } = use('Test/Suite')('UserController')
const User = use('App/Models/User')
// const Mail = use('Mail')

trait('Test/ApiClient')
trait('Auth/Client')


test('Iniciar sesión', async ({ client, assert }) => {
  // Mail.fake()
  const userTest = {
    email: 'user@test.com',
    password: '123456'
  }
  await User.create(userTest)
  
  const response = await client.post('api/login').send(userTest).end()
  response.assertStatus(200)

  assert.isObject(response.body, 'Es un objeto')
  assert.equal(response.body.type, 'bearer')

})

test('No iniciar sesión', async ({ client, assert }) => {
  const userTest = {
    email: 'user2@test.com',
    password: '123456'
  }

  const userLogin = {
    email: 'cualquier@correo.com',
    password: 'x'
  }
  
  await User.create(userTest)

  const response = await client.post('api/login').send(userLogin).end()
  response.assertStatus(401)

})

test('Listar usuarios - usuario autorizado', async ({ client, assert }) => {
  const user = await User.query().where('email', 'admin@wondrix.com').first()

  const response = await client
    .get('api/users')
    .loginVia(user)
    .end()
  response.assertStatus(200)

})

test('Listar usuarios - usuario NO autorizado', async ({ client, assert }) => {
  const response = await client.get('api/users').end()
  response.assertStatus(401)
})

test('Guardar usuario - datos incompletos - usuario autorizado', async ({ client, assert }) => {
  const userCreate = {
  }
  const user = await User.query().where('email', 'admin@wondrix.com').first()
  const response = await client.post('api/users').loginVia(user).send(userCreate).end()
  response.assertStatus(422)

})

test('Guardar usuario - datos completos - usuario autorizado', async ({ client, assert }) => {

})

test('Guardar usuario - usuario NO autorizado', async ({ client, assert }) => {
  const userCreate = {
  }
  const response = await client.post('api/users').send(userCreate).end()
  response.assertStatus(401)
})

test('Consultar usuario - usuario autorizado', async ({ client, assert }) => {
  const user = await User.query().where('email', 'admin@wondrix.com').first()
  const response = await client.get('api/users/1').loginVia(user).send().end()
  response.assertStatus(200)
})

test('Consultar usuario - usuario NO autorizado', async ({ client, assert }) => {
  const response = await client.get('api/users/1').send().end()
  response.assertStatus(401)
})

test('Actualizar usuario - datos incompletos - usuario autorizado', async ({ client, assert }) => {
  const userCreate = {
  }
  const user = await User.query().where('email', 'admin@wondrix.com').first()
  const response = await client.put('api/users/1').loginVia(user).send(userCreate).end()
  response.assertStatus(422)

})

test('Actualizar usuario - datos completos - usuario autorizado', async ({ client, assert }) => {

})

test('Actualizar usuario - usuario NO autorizado', async ({ client, assert }) => {
  const userCreate = {
  }
  const response = await client.put('api/users/1').send(userCreate).end()
  response.assertStatus(401)
})

test('Eliminar usuario - usuario autorizado', async ({ client, assert }) => {
  const user = await User.query().where('email', 'admin@wondrix.com').first()
  const userDelete = (await User.query().where('email', 'user2@test.com').first()).toJSON().id

  const response = await client.delete(`api/users/${userDelete}`).loginVia(user).send().end()
  response.assertStatus(200)

})


test('Eliminar usuario - usuario NO autorizado', async ({ client, assert }) => {
  const response = await client.put('api/users/1').end()
  response.assertStatus(401)
})

