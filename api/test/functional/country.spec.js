'use strict'

const { test, trait } = use('Test/Suite')('CountryController')

trait('Test/ApiClient')

test('Mostrar lista de paises', async ({ client, assert }) => {
  const response = await client.get('api/countries').send().end()
  response.assertStatus(200)
  
})
