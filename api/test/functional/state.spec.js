'use strict'

const { test, trait } = use('Test/Suite')('StateController')

trait('Test/ApiClient')

test('Mostrar lista de estados', async ({ client, assert }) => {
  const response = await client.get('api/states').send().end()
  response.assertStatus(200)

})