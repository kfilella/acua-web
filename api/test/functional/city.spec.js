'use strict'

const { test, trait } = use('Test/Suite')('CityController')

trait('Test/ApiClient')

test('Mostrar lista de ciudades', async ({ client, assert }) => {
  const response = await client.get('api/cities').send().end()
  response.assertStatus(200)

})