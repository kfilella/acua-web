
# WondrixBoilerPlate

Proyecto desarrollado por Wondrix para ser la base de sus futuras aplicaciones web y móviles.

El proyecto combina tecnologías de Frontend y Backend, con una estructura definida para comenzar a desarrollar sin preocuparse por algunas cosas.
## Características generales

# WondrixBoilerPlate (client)

A Quasar Framework app

## Install the dependencies
```bash
yarn
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```

### Lint the files
```bash
yarn run lint
```

### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).







# WondrixBoilerPlate (API)

This is the boilerplate for creating an API server in AdonisJs, it comes pre-configured with.

1. Bodyparser
2. Authentication
3. CORS
4. Lucid ORM
5. Migrations and seeds

## Setup

Use the adonis command to install the blueprint

```bash
adonis new yardstick --api-only
```

or manually clone the repo and then run `npm install`.


### Migrations

Run the following command to run startup migrations.

```js
adonis migration:run
```





# Acerca del desarrollo

## Front

El front está basado en VueJS como Framework Javascript y en Quasar como framework de estilos.

El frontend se encuentra en la carpeta `client`.

La carpeta `src` contiene la lógica de la aplicación.

### Componentes

Los componentes son aquellos fragmentos de código que pueden ser reutilizados cualquier cantidad de veces. Éstos deben definirse dentro de la carpeta `components`. El nombre de los componentes debe ser en formato CamelCase, es decir, cada palabra capitalizada.

#### Listados(Tablas)

Existe un componente llamado `Listable` que muestra una tabla con registros y acciones con tan solo enviarle unos parámetros.

```html
<listable :title="title" :buttons="buttons" :cols="cols" route="permissions"/>
```

Donde:

`title` es el titulo de la tabla. El mensaje que tendrá la tabla.

`buttons` es un array de objetos con las acciones globales que tendrá la tabla. Cada objeto debe tener la propiedad `permission` 
```javascript
[
    {
        label: 'Nuevo', icon: 'add', color: 'primary', url: `${this.$route.path}/form`, action: '', permission: 14
    }
]
```


### Páginas

Las páginas(vistas) de la aplicación deben ubicarse en la carpeta `pages`.
Deberán estar ubicadas según el rol y el módulo al que pertenecen.

### Rutas

Las rutas del front estarán definidas en `src/router/routes.js`

#### Importante

Cada ruta que sea parte del children del menú debe estar atada a un permiso. Esto debe definirse como una propiedad meta que es un objeto que contiene la propiedad `permission`. Se le debe asignar un número de permiso que corresponda con el id del permiso en la base de datos. Esto con el objetivo de que los usuarios solamente accedan a rutas permitidas para su rol. En caso de intentar acceder a una ruta no permitida será redireccionado al login para iniciar sesión nuevamente.
Por ejemplo:

```javascript
{ path: '/permissions/form/:id', component: () => import('pages/admin/permissions/Form.vue'), meta: { permission: 12 } }
```

# Procesos comunes

## Nueva ítem del menú

## Nuevo modelo

## Nuevo CRUD
