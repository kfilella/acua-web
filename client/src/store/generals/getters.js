export function someGetter (/* state */) {
}
export function sessionInfo (state) {
  return JSON.parse(localStorage.getItem('___ATAD___'))
}
export function can (state) {
  return permission => {
    const sessionInfo = JSON.parse(localStorage.getItem('___ATAD___'))
    const found = (sessionInfo !== null) ? sessionInfo.permissions.find(item => {
      return item === permission
    }) : undefined
    if (found) {
      return true
    } else {
      return false
    }
  }
}
