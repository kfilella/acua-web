export function someMutation (/* state */) {
}
export function login (state, data) {
  localStorage.setItem('___ATAD___', JSON.stringify(data))
  state.sessionInfo = data
}
export function logout (state) {
  localStorage.removeItem('___ATAD___')
  state.sessionInfo = undefined
}
