import { Loading } from 'quasar'

export const ListMixin = {
  data () {
    return {
      confirm: false,
      text: '',
      records: [],
      stringRecords: []
    }
  },
  async mounted () {
    this.getRecords()
  },
  methods: {
    deleteRecord (id) {
      this.confirm = true
      this.id = id
    },
    confirmDelete () {
      if (this.route) {
        this.$api.delete(this.route + '/' + this.id).then((res) => {
          if (res) {
            this.getRecords()
            this.$q.notify({
              type: 'positive',
              message: 'Registro eliminado con éxito!'
            })
          }
        })
      }
    },
    getRecords () {
      Loading.show()
      this.$api.get(this.route).then(res => {
        Loading.hide()
        this.records = res
        this.stringRecords = res
      })
    }
  }
}
